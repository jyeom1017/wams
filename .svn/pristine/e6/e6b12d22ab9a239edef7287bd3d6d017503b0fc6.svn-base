package com.usom.model.service.asset;

import com.framework.model.util.Pagging;
import com.usom.model.dao.asset.AssetDao;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;

@Service("assetService")
public class AssetServiceImpl implements AssetService {

	@Resource(name="tiberoSession")
    private SqlSession oracleSession;

    @Resource(name = "pagging")
    private Pagging pagging;

    @Override
    public HashMap<String, Object> getAssetCriteria() {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.getAssetCriteria();
    }

    @Override
    public HashMap<String, Object> getLevelCodeList(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return pagging.runPaging(assetDao.getLevelCodeList(param), param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getLevelCodeListExcel(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.getLevelCodeListExcel(param);
    }

    @Override
    public int updateLevelCode(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.updateLevelCode(param);
    }

    @Override
    public int insertLevelCode(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.insertLevelCode(param);
    }

    @Override
    public int deleteLevelCode(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.deleteLevelCode(param);
    }

    @Override
    public int isAssetClassCdExists(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.isAssetClassCdExists(param);
    }

    @Override
    public int getTotalCount() throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.getTotalCount();
    }

    @Override
    public int isLevelCodeUsed(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.isLevelCodeUsed(param);
    }

    // 자산 기준정보관리
    @Override
    public HashMap<String, Object> getAssetBaseInfoList(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return pagging.runPaging(assetDao.getAssetBaseInfoList(param), param);
    }


    @Override
    public HashMap<String, Object> getAssetBaseInfoDetail(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.getAssetBaseInfoDetail(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getAssetBaseInfoExcel(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.getAssetBaseInfoExcel(param);
    }

    @Override
    public int updateAssetBaseInfo(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.updateAssetBaseInfo(param);
    }

    @Override
    public int insertAssetBaseInfo(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.insertAssetBaseInfo(param);
    }

    @Override
    public int deleteAssetBaseInfo(HashMap<String, Object> param) throws Exception {
        AssetDao assetDao = oracleSession.getMapper(AssetDao.class);

        return assetDao.deleteAssetBaseInfo(param);
    }

}