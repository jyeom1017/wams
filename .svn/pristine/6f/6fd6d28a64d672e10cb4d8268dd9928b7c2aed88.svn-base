package com.usom.web.controller.api;

import com.framework.model.util.CommonUtil;
import com.usom.model.service.asset.ItemService;
import com.usom.model.service.common.CommonService;
import com.usom.model.service.gis.GisService;
import com.usom.model.service.postgis.PostGisService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/gis")
public class GisController {

    @Resource(name = "commonService")
    private CommonService commonService;
    
    @Resource(name = "itemService")
    private ItemService itemService;
    
    @Autowired
    private GisService gisService;

    @Autowired
    private PostGisService postgisService;

    @RequestMapping(value = "/getAssetInfoWithGisInfo.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody Map<String, Object> getAssetInfoWithGisInfo(@RequestParam HashMap<String, Object> param) throws Exception {
        return gisService.getAssetInfoWithGisInfo(param);
    }

    @RequestMapping(value = "/getAssetInfoWithLayer.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> getAssetInfoWithLayer(@RequestParam HashMap<String, Object> param) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        param.put("USER_ID", user.getE_ID());
        return gisService.getAssetInfoWithLayer(param);
    }

    @RequestMapping(value = "/insertAssetGis.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertAssetGis(HttpServletRequest req) throws Exception {
        return gisService.insertAssetGis(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getAssetInfoWithGisList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody List<Map<String, Object>> getAssetInfoWithGisList(@RequestBody List<Map<String, Object>> param) throws Exception {
        return gisService.getAssetInfoWithGisList(param);
    }

    @RequestMapping(value = "/getGISInfoByAssetPath.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public List<Map<String, Object>> getGISInfoByAssetPath(@RequestParam Map<String, Object> param) throws Exception {
        return gisService.getGISInfoByAssetPath(param);
    }

    // 진단 구역 리스트
    @RequestMapping(value = "/getDiagnosticAreaList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody Map<String, Object> getDiagnosticAreaList(@RequestParam HashMap<String, Object> param) throws Exception {
        return gisService.getDiagnosticAreaList(param);
    }

    // 진단 구역 삭제
    @RequestMapping(value = "/deleteDiagnosticArea.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteDiagnosticArea(HttpServletRequest req) throws Exception {
        return gisService.deleteDiagnosticArea(CommonUtil.reqDataParsingToHashMap(req));
    }

    // 진단 구역 등록 리스트
    @RequestMapping(value = "/getDiagnosticAreaDetailNewList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> getDiagnosticAreaDetailNewList(@RequestParam HashMap<String, Object> param) throws Exception {
        return gisService.getDiagnosticAreaDetailNewList(param);
    }

    // 진단 구역 수정 리스트
    @RequestMapping(value = "/getDiagnosticAreaDetailEditList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> getDiagnosticAreaDetailEditList(@RequestBody HashMap<String, Object> param) throws Exception {
        return gisService.getDiagnosticAreaDetailEditList(param);
    }

    @RequestMapping(value = "/getAssetsByAssetPath.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public List<Map<String, Object>> getAssetsByAssetPath(HttpServletRequest req) throws Exception {
        return gisService.getAssetsByAssetPath(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getAssetsByGISProperty.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public List<Map<String, Object>> getAssetsByGISProperty(@RequestBody Map<String, Object> param) {
        return gisService.getAssetsByGISProperty(param);
    }

    // 진단 구역 등록/수정 (팝업) 저장
    @RequestMapping(value = "/saveDiagnosticAreaDetail.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int saveDiagnosticAreaDetail(@RequestParam HashMap<String, Object> param) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        param.put("user_id", user.getE_ID());
        return gisService.saveDiagnosticAreaDetail(param);
    }

    // 진단 구역 등록/수정 자산 삭제
    @RequestMapping(value = "/deleteDiagnosticAreaAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteDiagnosticAreaAsset(HttpServletRequest req) throws Exception {
        return gisService.deleteDiagnosticAreaAsset(CommonUtil.reqDataParsingToHashMap(req));
    }

    // 상태진단 그룹 리스트
    @RequestMapping(value = "/getStateDiagnosisGroupList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody Map<String, Object> getStateDiagnosisGroupList(@RequestParam HashMap<String, Object> param) throws Exception {
        return gisService.getStateDiagnosisGroupList(param);
    }

    // 상태진단 그룹 삭제
    @RequestMapping(value = "/deleteStateDiagnosisGroup.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteStateDiagnosisGroup(HttpServletRequest req) throws Exception {
        return gisService.deleteStateDiagnosisGroup(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getStateDiagnosticGroupDetailPopupInfo.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> getStateDiagnosticGroupDetailPopupInfo(@RequestParam Map<String, Object> param) throws Exception {
        System.out.println("param = " + param);
        return gisService.getStateDiagnosticGroupDetailPopupInfo(param);
    }

    // 상태진단 그룹 등록 리스트
    @RequestMapping(value = "/getStateDiagnosisGroupDetailNewList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> getStateDiagnosisGroupDetailNewList(@RequestParam HashMap<String, Object> param) throws Exception {
        return gisService.getStateDiagnosisGroupDetailNewList(param);
    }

    // 상태진단 그룹 수정 리스트
    @RequestMapping(value = "/getStateDiagnosisGroupDetailEditList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> getStateDiagnosisGroupDetailEditList(@RequestParam HashMap<String, Object> param) throws Exception {
        return gisService.getStateDiagnosisGroupDetailEditList(param);
    }

    // 상태진단 그룹 저장
    @RequestMapping(value = "/saveStateDiagnosisGroup.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int saveStateDiagnosisGroup(@RequestParam HashMap<String, Object> param) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        param.put("user_id", user.getE_ID());
        return gisService.saveStateDiagnosisGroup(param);
    }

    @RequestMapping(value = "/getAssetByAssetCode.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public List<Map<String, Object>> getAssetByAssetCode(@RequestParam Map<String, Object> param) throws Exception {
        return gisService.getAssetByAssetCode(param);
    }

    @RequestMapping(value = "/getBlockInfo.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public ArrayList<HashMap<String, Object>> getBlockInfo(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        return postgisService.getBlockInfo(param);
    }

    @RequestMapping(value = "/getElevationDifferenceAnalysisInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public HashMap<String, Object> getElevationDifferenceAnalysisInfo(HttpServletRequest req) throws Exception {
        HashMap<String, Object> result = new HashMap<String, Object>();
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        HashMap<String, Object> param2 = new HashMap<String, Object>();
        ArrayList<HashMap<String, Object>> list = postgisService.getTempPipeList(param);
        String[] ftrIdnList = new String[list.size()];
        int i = 0;
        for (i = 0; i < list.size(); i++) {
            ftrIdnList[i] = list.get(i).get("ftr_idn").toString();
        }
        param.put("ftrIdnList", ftrIdnList);

        result.put("list", gisService.getPipeInfoListFromFtrIdn(param));
        result.put("nodeInfo", postgisService.getNodeList(param2));

        return result;
    }

    @RequestMapping(value = "/getTempPipeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public ArrayList<HashMap<String, Object>> getTempPipeList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return postgisService.getTempPipeList(param);
    }

    @RequestMapping(value = "/saveTempPipeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int saveTempPipeList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        JSONArray itemList = (JSONArray) param.get("ITEM_LIST");
        System.out.println(param);
        int i = 0;

        try {
            postgisService.deleteList(param);
            HashMap<String, Object> param2 = new HashMap<String, Object>();
            for (i = 0; i < itemList.size(); i++) {
                param2.put("FTR_IDN", itemList.get(i));
                postgisService.insertList(param2);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 1;
    }


    @RequestMapping(value = "/getGoogleMapApi", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public String getGoogleMapApi(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        //CommonUtil.reqDataParsingToHashMap(req);
        String result = "";
        //System.out.println(param);
        param.put("url", "https://maps.googleapis.com/maps/api/elevation/json?locations=");
        param.put("key", "&key=AIzaSyBnV9yDpkCXPci_6qrxYZVwn46N4UVjxBc");
        param.put("locations", req.getParameter("locations").toString());
        //param.put("locations", "35.67425878463393,126.5645754699081");
        try {
            result = gisService.getGoogleMapApi(param);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }

    @RequestMapping(value = "/getVWorldApi", method = RequestMethod.GET, headers = "Accept=application/json")
    void getVWorldApi(HttpServletRequest req,HttpServletResponse res) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        //CommonUtil.reqDataParsingToHashMap(req);
        String result = "";
        
        //System.out.println(param);
        param.put("url", "http://api.vworld.kr/req/wfs?service=WFS&version=1.1.0&request=GetFeature&output=application%2Fjson&srsName=EPSG:4326");
        param.put("key", "&key=B11D097B-16B6-3A2B-AFDE-69C751C0320E&domain=10.0.28.100");
        param.put("locations","&typeName="+ req.getParameter("typeName").toString() + "&bbox="+ req.getParameter("bbox").toString());
        //&typeName=lt_c_wkmstrm&bbox=35.66677518918763%2C126.55928110550921%2C35.66677518918763%2C126.55928110550921
        //param.put("locations", "35.67425878463393,126.5645754699081");
        try {
            result = gisService.getVWorldApi(param);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        
        
		res.setStatus(HttpServletResponse.SC_OK);
		
		// response header
		res.setContentType("applicaton/json");
		res.setCharacterEncoding("utf-8");
		//res.setHeader("Content-Type", "text/plain;charset=utf-8");
		res.setHeader("Cache-Contorl", "no-cache, no-store, must-revalidate");
		res.setHeader("Pragma", "no-cache");
		//res.setHeader("temp-header", "hello world");

		PrintWriter writer = res.getWriter();
		writer.println(new String(result.getBytes(Charset.forName("utf-8")),"utf-8"));
		
    }
    
    @RequestMapping(value = "/deleteDoctor1.json", method = RequestMethod.DELETE, headers = "Accept=application/json")
    @ResponseBody
    public int deleteDoctor1(@RequestParam Map<String, Object> param) throws Exception {
        return postgisService.deleteDoctor1(param);
    }

    @RequestMapping(value = "/insertVerify25310.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int insertVerify25310(@RequestParam Map<String, Object> param) throws Exception {
        return postgisService.insertVerify25310(param);
    }

    @RequestMapping(value = "/updateFeatureCodeWithLastData.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> updateFeatureCodeWithLastData(@RequestBody Map<String, Object> param) throws Exception {
        postgisService.updateFeatureCodeWithLastData(param);
        return param;
    }

    @RequestMapping(value = "/updateBlockInformation.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int updateBlockInformation(@RequestBody Map<String, Object> param) throws Exception {
        return postgisService.updateBlockInformation(param);
    }

    @RequestMapping(value = "/updateAllAssetInformationInTheBlock.json", method = RequestMethod.POST)
    @ResponseBody
    public int updateAllAssetInformationInTheBlock(@RequestBody Map<String, Object> param) throws Exception {
        List<String> blockNames = (List<String>) param.get("blockNames");
        int result = 0;

        for (String blockName : blockNames) {
            List<Map<String, Object>> allAssetInformation = postgisService.getAllAssetInformationInTheBlock(blockName);
            int i = gisService.updateAssetInfoItem(allAssetInformation, blockName);
            result = Math.max(result, i);
        }

        return result;
    }

    @RequestMapping(value = "/under42.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int under42(@RequestBody Map<String, Object> param) throws Exception {
        int a = postgisService.museums19(param);
        int b = gisService.under42(param);
        int result = Math.min(a, b);

        return result;
    }

    @RequestMapping(value = "/deleteTemporaryAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int deleteTemporaryAsset(@RequestBody Map<String, Object> param) throws Exception {
    	/*
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		if(param.get("ASSET_SID") != null) {
    	HashMap <String,Object > param1 = itemService.getAssetGisInfo((HashMap<String, Object>)param);
    	if(param1 != null) {
    	param.putAll(param1);
        param.put("table_nm", param.get("LAYER").toString());
        param.put("ftr_idn", param.get("FTR_IDN").toString());
        param.put("ftr_cde", param.get("FTR_CDE").toString());
        itemService.deleteAssetGis((HashMap<String, Object>)param);
    	}
        int result = itemService.deleteAsset((HashMap<String, Object>)param);
		}*/
        return postgisService.deleteTemporaryAsset(param);
    }

    @RequestMapping(value = "/updatePipeLmLayers.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int updatePipeLmLayers(HttpServletRequest req,HttpServletResponse res) throws Exception {
    	HashMap<String,Object> param = new HashMap<String,Object>();
        return postgisService.updatePipeLmLayers(param);
    }
    
    @RequestMapping(value = "/deleteGisAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int deleteGisAsset(HttpServletRequest req,HttpServletResponse res) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();    	
    	HashMap <String,Object > param = new HashMap<String,Object >();
    	param.put("ASSET_SID",Integer.parseInt(req.getParameter("ASSET_SID").toString()));
    	HashMap <String,Object > param1 = itemService.getAssetGisInfo(param);
    	int result1 = 0;
    	if(param1 != null) {
    	param.putAll(param1);
        param.put("table_nm", param.get("LAYER").toString());
        param.put("ftr_idn", param.get("FTR_IDN").toString());
        param.put("ftr_cde", param.get("FTR_CDE").toString());
        result1 = postgisService.deleteGisAsset(param);
        itemService.deleteAssetGis(param);
    	}
        int result = itemService.deleteAsset(param);
        
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			param2.put("lg_opt", "6");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기
			param2.put("lg_desc", "자산삭제:" + param.get("ASSET_SID") );
			param2.put("login_user_id", user.getE_ID().toString());
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}
		return result1+result;
    }

    @RequestMapping(value = "/setPostgisGeoInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int setPostgisGeoInfo(HttpServletRequest req,HttpServletResponse res) throws Exception {
    	int result =0;
    	HashMap<String,Object> param = CommonUtil.reqDataParsingToHashMap(req);
    		
    	return postgisService.setPostgisGeoInfo(param);
    }
}