package com.usom.model.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

public class Configuration {

	
	/************** 수정금지 **********/
	//운영기 
	
	public static final String WEB_ROOT_PATH = "D:";
	
	public static final String UPLOAD_FILE_PATH = "D:/storage/upload/";	//개발기
	
	public static final String UPLOAD_FILE_URL = "/storage/upload/";

	public static final int UPLOAD_FILE_MAX_UPLOAD_SIZE = 400;

	public static final String DOMAIN = "10.0.28.100:8080";
	
	public static final int MAX_REPORT_PRINT_LINE_CNT = 100000;
	
	public static final String REPORT_FILE_TEMPLATE_PATH = "D:/WAMS/resources/excel/template/";//excel 로컬
	public static final String REPORT_FILE_PATH = "D:/WAMS/resources/excel/";//excel 로컬
	

	/************** 수정금지 **********/
	
	
	//개발(개발자PC) 자신의 pc에 맞게 수정.
/*	
	public static final String WEB_ROOT_PATH = "C:/PJ/Tomcat 7.0/webapps/ROOT/";
	
	public static final String UPLOAD_FILE_PATH = "D:/storage/upload/";	//개발기
	
	public static final String UPLOAD_FILE_URL = "/storage/upload/";

	public static final int UPLOAD_FILE_MAX_UPLOAD_SIZE = 300;

	public static final String DOMAIN = "192.168.1.80:8080";	
	
	public static final int MAX_REPORT_PRINT_LINE_CNT = 500;
	
	public static final String REPORT_FILE_TEMPLATE_PATH = "C:/PJ/Tomcat 7.0/webapps/ROOT/resources/excel/template/";//excel 로컬
//	public static final String REPORT_FILE_TEMPLATE_PATH = "D:/PJ/OMAS/workspace/framework/framework-web/src/main/webapp/resources/excel/template/";//excel 로컬
//	public static final String REPORT_FILE_TEMPLATE_PATH = "C:/USOM/usom/framework/framework-web/src/main/webapp/resources/excel/template/";//excel 로컬
	public static final String REPORT_FILE_PATH = "C:/PJ/Tomcat 7.0/webapps/ROOT/resources/excel/";//excel 로컬
*/	

}


 