package com.omas.model.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

public class Configuration {

	public static final String WEB_ROOT_PATH = "C:/webapp/omas/framework-web";
	
	public static final String UPLOAD_FILE_PATH = "C:/webapp/omas/framework-web/resources/upload/";  //운영기

	//public static final String UPLOAD_FILE_PATH = "D:/PJ/OMAS/workspace/omas/framework/framework-web/src/main/webapp/resources/upload/";	//개발기 
	
	public static final String UPLOAD_FILE_URL = "/resources/upload/";
	
	public static final String DOMAIN = "localhost:8080";
	
	public static final String REPORT_FILE_PATH = "C:/webapp/omas/framework-web/resources/report/";
	
	//지도 관련
	public static final String VWORLD_API_KEY = "03ED9C1F-48D4-3740-A43E-9E4B0C358875";
	public static final String DAUM_API_KEY = "f384dce7a418b155e2613b403e3603381e54e19c";

	
	//O.M.A.S App 공통코드
	public static final String SYSTEM_CODE = "OPSYSTEMSS";//전역 시스템정의코드
	public static final String PREFIX_ID = "eps";//자동생성 아이디 prefix
	public static final String MENU_PART_CODE_BASIC = "01";//일반메뉴코드
	public static final String MENU_PART_CODE_OPERATOR = "02";//운영사메뉴코드   M_PART의 파라미터가 된다. 
//	public static String SITE_CODE = "";//선택 사이트코드
	
	//Mybatis query에서 선택 권한사이트 코드 리턴 - 2017-10-12 안상현
	public static final String getSelectSiteCode() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		//로그인 회원정보
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		System.out.println(">>>>>>>>>>>>>>>>>>>>> 선택사이트코드 : " + user.getSELECT_SITE_CODE());
		return user.getSELECT_SITE_CODE();
	}
	
	/**
	 * <pre>업무별 프로세스 코드 정의 - DB + Client + Server가 항상 동일해야 한다.</pre>
	 */
	//진행상태코드
	public static final String STATUS_CODE_000 = "APP000";//계획미확인          
	public static final String STATUS_CODE_001 = "APP001";//계획 결제요청       
	public static final String STATUS_CODE_002 = "APP002";//계획 보류          
	public static final String STATUS_CODE_003 = "APP003";//계획 취소          
	public static final String STATUS_CODE_004 = "APP004";//계획 수정          
	public static final String STATUS_CODE_010 = "APP010";//계획 승인완료       
	public static final String STATUS_CODE_011 = "APP011";//작업완료 결재요청     
	public static final String STATUS_CODE_012 = "APP012";//작업수정           
	public static final String STATUS_CODE_020 = "APP020";//작업완료
	public static final String STATUS_CODE_021 = "APP021";//사후처리 결재요청     
	public static final String STATUS_CODE_022 = "APP022";//사후처리 수정       
	public static final String STATUS_CODE_023 = "APP023";//사후처리 완료
	
	//작업유형코드
	public static final String TYPE_CODE_010 = "TST010";//계획(정기)
	public static final String TYPE_CODE_020 = "TST020";//긴급(민원)
	public static final String TYPE_CODE_030 = "TST030";//긴급(주무관청지시)
	public static final String TYPE_CODE_040 = "TST040";//긴급(사고)
	public static final String TYPE_CODE_050 = "TST050";//대수선
	public static final String TYPE_CODE_060 = "TST060";//기타(교육,훈련,회의,홍보 ..)
	
	//작업분류코드(작업대구분)
	/*public static final String BOPT_CODE_010 = "TTT010";//교육
	public static final String BOPT_CODE_020 = "TTT020";//훈련
	public static final String BOPT_CODE_030 = "TTT030";//홍보
	public static final String BOPT_CODE_040 = "TTT040";//회의
	public static final String BOPT_CODE_050 = "TTT050";//점검
	public static final String BOPT_CODE_060 = "TTT060";//조사
	public static final String BOPT_CODE_070 = "TTT070";//청소 및 준설
	public static final String BOPT_CODE_080 = "TTT080";//개보수 및 대수선
*/	
	
}

