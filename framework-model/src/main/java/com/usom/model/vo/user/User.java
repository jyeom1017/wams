package com.usom.model.vo.user;

/**
 * TABLE NAME : M2_B_EMP
 * @author 
 *
 */
public class User {
	
	private String E_ID;
	private String E_PW;
	private String E_ENUM;
	private String E_NAME;
	private String E_AFF; //소속명
	private String E_POS; //직급코드
	private String E_DBIRTH;	
	private String E_ADDR;
	private String E_ZIP;
	private String E_JOB;
	private String E_PHONE;
	private String E_MPHONE;
	private String E_FAX;
	private String E_EMAIL;	
	private String E_GCODE1;
	private String E_GNAME1;
	private String E_INOFF; //재직여부 (1-재직,2-휴직,3-퇴사)
	private int E_POTO;	
	private String G_UDATE; //최종수정일
	private String G_UUSER; //수정자
	private String E_TGRADE; //기술등급코드
	private int E_SIGN; //서명파일
	private String E_MEMO; //메모

	private String E_GROLE;//권한코드
	private String E_GROLENM;//권한명
	private String LAST_MENU;//마지막 로그인 메뉴
	private int FAIL_CNT;   // 로그인실패 카운트
	private String E_PW_ENC; //패스워드 암호화
	
	
	public String getLAST_MENU() {
		return LAST_MENU;
	}
	public void setLAST_MENU(String lAST_MENU) {
		LAST_MENU = lAST_MENU;
	}
	public String getE_ID() {
		return E_ID;
	}
	public void setE_ID(String e_ID) {
		E_ID = e_ID;
	}
	public String getE_PW() {
		return E_PW;
	}
	public void setE_PW(String e_PW) {
		E_PW = e_PW;
	}
	public String getE_ENUM() {
		return E_ENUM;
	}
	public void setE_ENUM(String e_ENUM) {
		E_ENUM = e_ENUM;
	}
	public String getE_NAME() {
		return E_NAME;
	}
	public void setE_NAME(String e_NAME) {
		E_NAME = e_NAME;
	}
	public String getE_AFF() {
		return E_AFF;
	}
	public void setE_AFF(String e_AFF) {
		E_AFF = e_AFF;
	}
	public String getE_POS() {
		return E_POS;
	}
	public void setE_POS(String e_POS) {
		E_POS = e_POS;
	}
	public String getE_JOB() {
		return E_JOB;
	}
	public void setE_JOB(String e_JOB) {
		E_JOB = e_JOB;
	}
	public String getE_TGRADE() {
		return E_TGRADE;
	}
	public void setE_TGRADE(String e_TGRADE) {
		E_TGRADE = e_TGRADE;
	}
	public String getE_DBIRTH() {
		return E_DBIRTH;
	}
	public void setE_DBIRTH(String e_DBIRTH) {
		E_DBIRTH = e_DBIRTH;
	}
	public String getE_ZIP() {
		return E_ZIP;
	}
	public void setE_ZIP(String e_ZIP) {
		E_ZIP = e_ZIP;
	}
	public String getE_ADDR() {
		return E_ADDR;
	}
	public void setE_ADDR(String e_ADDR) {
		E_ADDR = e_ADDR;
	}
	public String getE_EMAIL() {
		return E_EMAIL;
	}
	public void setE_EMAIL(String e_EMAIL) {
		E_EMAIL = e_EMAIL;
	}
	public String getE_PHONE() {
		return E_PHONE;
	}
	public void setE_PHONE(String e_PHONE) {
		E_PHONE = e_PHONE;
	}
	public String getE_MPHONE() {
		return E_MPHONE;
	}
	public void setE_MPHONE(String e_MPHONE) {
		E_MPHONE = e_MPHONE;
	}	
	public String getE_FAX() {
		return E_FAX;
	}
	public void setE_FAX(String e_FAX) {
		E_FAX = e_FAX;
	}
	public int getE_SIGN() {
		return E_SIGN;
	}
	public void setE_SIGN(int e_SIGN) {
		E_SIGN = e_SIGN;
	}
	public String getE_INOFF() {
		return E_INOFF;
	}
	public void setE_INOFF(String e_INOFF) {
		E_INOFF = e_INOFF;
	}
	public int getE_POTO() {
		return E_POTO;
	}
	public void setE_POTO(int e_POTO) {
		E_POTO = e_POTO;
	}
	public String getE_MEMO() {
		return E_MEMO;
	}
	public void setE_MEMO(String e_MEMO) {
		E_MEMO = e_MEMO;
	}
	public String getE_GCODE1() {
		return E_GCODE1;
	}
	public void setE_GCODE1(String e_GCODE1) {
		E_GCODE1 = e_GCODE1;
	}
	public String getE_GNAME1() {
		return E_GNAME1;
	}
	public void setE_GNAME1(String e_GNAME1) {
		E_GNAME1 = e_GNAME1;
	}
	public String getG_UDATE() {
		return G_UDATE;
	}
	public void setG_UDATE(String g_UDATE) {
		G_UDATE = g_UDATE;
	}
	public String getG_UUSER() {
		return G_UUSER;
	}
	public void setG_UUSER(String g_UUSER) {
		G_UUSER = g_UUSER;
	}
	public String getE_GROLE() {
		return E_GROLE;
		/*
		if(E_GROLE.equals("G001") || E_GROLE.equals("G002") || E_GROLE.equals("G003") || E_GROLE.equals("G004") || E_GROLE.equals("G011") || E_GROLE.equals("G012")) {
			return E_GROLE;
		}else if(!E_GROLE.equals("") && E_GROLE!=null){
			return "";
		}else {
			return "";	
		}*/
	}
	public void setE_GROLE(String e_GROLE) {
		E_GROLE = e_GROLE;
	}
	public String getE_GROLENM() {
		return E_GROLENM;
	}
	public void setE_GROLENM(String e_GROLENM) {
		E_GROLENM = e_GROLENM;
	}
	public void setFAIL_CNT(int fail_cnt) {
		FAIL_CNT = fail_cnt;
	}
	public int getFAIL_CNT() {
		return FAIL_CNT;
	}
	public void setE_PW_ENC(String pw_enc) {
		E_PW_ENC = pw_enc;
	}
	public String getE_PW_ENC() {
		return E_PW_ENC;
	}	
}