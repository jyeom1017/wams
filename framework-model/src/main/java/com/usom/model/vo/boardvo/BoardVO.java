package com.usom.model.vo.boardvo;

import java.sql.Date;

public class BoardVO {
	private Integer NUM;	
	private Integer ASSET_SID;
	private String ASSET_CD;
	private String ASSET_NM;
	private Integer ASSET_PATH_SID;
	private String USE_YN;
	private String INSERT_ID;
	private Date INSERT_DT;
	
	public Integer getNum() {
		return NUM;
	}
	public void setNum(Integer NUM) {
		this.NUM = NUM;
	}
	public String getAssetCd() {
		return ASSET_CD;
	}
	public void setAssetCd(String AssetCd) {
		this.ASSET_CD = AssetCd;
	}
	public String getAssetNm() {
		return ASSET_NM;
	}
	public void setAssetNm(String AssetNm) {
		this.ASSET_NM = AssetNm;
	}
	public Integer getAssetPathSid() {
		return ASSET_PATH_SID;
	}
	public void setContent(Integer AssetPathSid) {
		this.ASSET_PATH_SID = AssetPathSid;
	}
	public String getUseYn() {
		return USE_YN;
	}
	public void setReadCount(String UseYn) {
		this.USE_YN = UseYn;
	}
	public String getInsertId() {
		return INSERT_ID;
	}
	public void setInsertId(String InsertId) {
		this.INSERT_ID = InsertId;
	}	
	public Date getInsertDt() {
		return INSERT_DT;
	}
	public void setInsertDt(Date InsertDt) {
		this.INSERT_DT = InsertDt;
	}
	@Override
	public String toString() {
		return "BoardVO [num=" + NUM + ", AssetCD=" + ASSET_CD + ", AssetNm=" + ASSET_NM + ", USE_YN="
				+ USE_YN + ", InsertDt=" + INSERT_DT + "]";
	}
	
}
