/**
 * 
 */
package com.usom.model.service.system;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usom.model.dao.basic.EmployeeDao;
import com.usom.model.dao.system.SystemDao;

/**
 * @author Administrator
 *
 */
@Service("SystemService")
public class SystemServiceImpl implements SystemService {
	
	@Resource(name="tiberoSession")
	private SqlSession tiberoSession;
	
	public ArrayList<HashMap<String, Object>> getEquipMasterList(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.getEquipMasterList(param);
	}
	
	public int insertEquipMaster(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.insertEquipMaster(param);
	}
	
	public int updateEquipMaster(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.updateEquipMaster(param);
	}
	
	@Transactional
	public int deleteEquipMaster(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		//M2_EQUIP_MASTER2 삭제
		dao.deleteEquipMaster(param);
		//M2_EQUIP_SPEC
		dao.deleteEquipSpec(param);
		//M2_EQUIP_HIST
		return dao.deleteEquipHist(param);
	}
	
	public ArrayList<HashMap<String, Object>> getEquipSpecList(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.getEquipSpecList(param);
	}
	
	public int insertEquipSpec(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.insertEquipSpec(param);
	}
	
	public int updateEquipSpec(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.updateEquipSpec(param);
	}
	
	public int deleteEquipSpec(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.deleteEquipSpec(param);
	}
	
	public ArrayList<HashMap<String, Object>> getEquipHistList(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.getEquipHistList(param);
	}
	
	public int insertEquipHist(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.insertEquipHist(param);
	}
	
	public int updateEquipHist(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.updateEquipHist(param);
	}
	
	public int deleteEquipHist(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.deleteEquipHist(param);
	}
	
	public ArrayList<HashMap<String, Object>> getEquipMasterPopList(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.getEquipMasterPopList(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getSewerageStatus(HashMap<String, Object> param) throws Exception {
		SystemDao dao = tiberoSession.getMapper(SystemDao.class);
		return dao.getSewerageStatus(param);
	}
	
}
