package com.usom.model.service.risk;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.dao.asset.ItemDao;
import com.usom.model.dao.estimation.EstimationDao;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

@Service("riskService")
public class RiskServiceImpl implements RiskService {

	@Resource(name="tiberoSession")
    private SqlSession oracleSession;

    @Resource(name = "pagging")
    private Pagging pagging;

    @Override
    public HashMap<String, Object> getList(HashMap<String, Object> param) {
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);

    		return pagging.runPaging(tibero.getRiskList(param), param);

    }

    
    @Override
    public int insert(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        int risk_sid = itemDao.getRiskSid();
        if (itemDao.insertRisk(param) == 1)
        	return risk_sid;
        else
        	return 0;
    }
    
    @Override
    public int update(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.updateRisk(param);
    }
    
    @Override
    public int delete(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.deleteRisk(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getExcelList(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskExcelList(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getAssetGroup(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskAssetGroup(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getAssetList(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskAssetList(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getAssetListExcel(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskAssetListExcel(param);
    }
    
    @Override
    public List<HashMap<String, Object>> getAssetListExcel1(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskAssetListExcel1(param);
    }
    
    
    @Override
    public ArrayList<HashMap<String, Object>> getFactorList(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getFactorRiskList(param);
    }
    
    @Override
    @Transactional
    public int insertFactor(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);	
        JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");
        HashMap<String, Object> map = new HashMap<>();
        int sid = 0;
        sid = itemDao.getNextRiskProbSid();
        param.put("sid", sid);
        itemDao.insertRiskProb(param);
        itemDao.deleteRiskProbGrade(param);
        System.out.println( param);
        
        for (int i=0; i< infoItems.size(); i++) {
       	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
       		param2.put("INSERT_ID", param.get("INSERT_ID"));	
       		param2.put("RISK_PROB_SID", sid);
        	itemDao.insertRiskProbGrade(param2);
        }
        return sid;
    }
    
    @Override
    @Transactional
    public int updateFactor(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");
        itemDao.updateRiskProb(param);
        itemDao.deleteRiskProbGrade(param);
        
        System.out.println( param);
        
        for (int i=0; i< infoItems.size(); i++) {
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
        	param2.put("INSERT_ID", param.get("INSERT_ID"));
        	param2.put("RISK_PROB_SID", param.get("RISK_PROB_SID"));
        	itemDao.insertRiskProbGrade(param2);
        }
        
        return 1;
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getGradeList(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getGradeRiskList(param);
    }

    @Override
    @Transactional
    public int insertGrade(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);	
        JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");
        HashMap<String, Object> map = new HashMap<>();
        int sid = 0;
        sid = itemDao.getNextRiskSpecSid();
        param.put("sid", sid);
        itemDao.insertRiskSpec(param);
        itemDao.deleteRiskSpecItem(param);
        System.out.println( param);
        
        for (int i=0; i< infoItems.size(); i++) {
       	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
       		param2.put("INSERT_ID", param.get("INSERT_ID"));	
       		param2.put("RISK_SPEC_SID", sid);
        	itemDao.insertRiskSpecItem(param2);
        }
        return sid;
    }
    
    @Override
    @Transactional
    public int updateGrade(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");
        itemDao.updateRiskSpec(param);
        itemDao.deleteRiskSpecItem(param);
        
        System.out.println( param);
        
        for (int i=0; i< infoItems.size(); i++) {
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
        	param2.put("INSERT_ID", param.get("INSERT_ID"));
        	param2.put("RISK_SPEC_SID", param.get("RISK_SPEC_SID"));
        	itemDao.insertRiskSpecItem(param2);
        }
        
        return 1;
    }

    
    @Override
    public ArrayList<HashMap<String, Object>> getFactorDetail(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.getFactorRiskDetail(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getGradeDetail(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.getRiskGradeDetail(param);
    }  
    
    @Override
    public int saveAsset(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.saveRiskAsset(param);
    }

    @Override
    public int deleteAsset(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    	JSONArray assetList = (JSONArray) param.get("ASSET_LIST");
        System.out.println( param.get("ASSET_LIST"));
        for (int i=0; i< assetList.size(); i++) {
        	HashMap<String,Object> param2 = new HashMap<String,Object>();
        	param2.put("ASSET_SID",assetList.get(i));
        	param2.put("RISK_SID", param.get("RISK_SID"));
            System.out.println(param2);
        	itemDao.deleteRiskAsset(param2);
        }    	
    		return 1;
    }

    @Override
    public int updateAsset(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    	itemDao.updateRiskAsset(param);
    	/*
    	JSONArray assetList = (JSONArray) param.get("ASSET_LIST");
        System.out.println( param.get("ASSET_LIST"));
        for (int i=0; i< assetList.size(); i++) {
        	HashMap<String,Object> param2 = new HashMap<String,Object>();
        	param2.put("ASSET_SID",assetList.get(i));
        	param2.put("RISK_SID", param.get("RISK_SID"));
        	param2.put("SERIOUS_GRADE", param.get("SERIOUS_GRADE"));
        	param2.put("SPARE_RATIO", param.get("SPARE_RATIO"));
        	System.out.println(param2);
        	itemDao.updateRiskAsset(param2);
        }*/    	
    		return 1;
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getRiskResult(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.getRiskResult(param);
    }
}
