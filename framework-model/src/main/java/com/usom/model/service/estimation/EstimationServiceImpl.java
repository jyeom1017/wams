package com.usom.model.service.estimation;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.dao.estimation.EstimationDao;
import net.sf.json.JSONArray;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("estimationService")
public class EstimationServiceImpl implements EstimationService {

	@Resource(name="tiberoSession")
    private SqlSession oracleSession;

    @Resource(name = "pagging")
    private Pagging pagging;

    @Override
    public HashMap<String, Object> getList(HashMap<String, Object> param) {
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
    	if(param.get("category").equals("1"))
    		return pagging.runPaging(tibero.getIndirEstList(param), param);
    	else
    		return pagging.runPaging(tibero.getDirEstList(param), param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getListExcel(HashMap<String, Object> param) {
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
    	if(param.get("category").equals("1"))
    		return tibero.getIndirEstList(param);
    	else
    		return tibero.getDirEstList(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getReportExcel(HashMap<String, Object> param) {
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
        //return tibero.getIndirEstReport(param);
    	if(param.get("category").equals("1"))
    		return tibero.getIndirEstAssetExcelReport(param);
    	else
    		return tibero.getDirEstAssetExcelReport(param);
    }
    
    @Override
    public HashMap<String, Object> getDetail(HashMap<String, Object> param) {
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);

        return tibero.getIndirEst(param);
    }  

    @Override
    public ArrayList<HashMap<String, Object>> getIndirEstAssetGroup(HashMap<String, Object> param){
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
    	return tibero.getIndirEstAssetGroup(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getIndirEstAssetList(HashMap<String, Object> param){
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
    	return tibero.getIndirEstAssetList(param);
    }

    
    @Override
    public ArrayList<HashMap<String, Object>> getDirEstAssetGroup(HashMap<String, Object> param){
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
    	return tibero.getDirEstAssetGroup(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getDirEstAssetList(HashMap<String, Object> param){
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
    	return tibero.getDirEstAssetList(param);
    }

    @Override
    public List<Map<String, Object>> getEstAssetNonePage(Map<String, Object> param) {
        EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);

        if (param.get("category").equals("1")) {
            return tibero.getIndirEstAssetNonePage(param);
        } else {
            return tibero.getDirEstAssetNonePage(param);
        }
    }

    @Override
    public HashMap<String, Object> getEstAssetPage(HashMap<String, Object> param) {
    	System.out.println(param);
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
    	if(param.get("category").equals("1"))
    		return pagging.runPaging(tibero.getIndirEstAssetPage(param), param);
    	else
    		return pagging.runPaging(tibero.getDirEstAssetPage(param), param);
    }    
    
    
    @Override
    public ArrayList<HashMap<String, Object>> getIndirEstAssetResultList(HashMap<String, Object> param){
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
    	return tibero.getIndirEstAssetResultList(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getDirEstAssetResultList(HashMap<String, Object> param){
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
    	return tibero.getDirEstAssetResultList(param);
    }
    
    
    @Override
    @Transactional
    public int insert(HashMap<String, Object> param) throws Exception {
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);

        if (param.get("category").equals("1")) {
        	if(tibero.insertIndirEst(param)==1) {
        		int est_sid = tibero.lastIndirEstSid();
        		param.put("est_sid", est_sid);
        		tibero.deleteIndirEstAsset(param);
        		return  est_sid;
        	}else
        		return 0;
        }else if(param.get("category").equals("2")){
        	if(tibero.insertDirEst(param)==1) {
        		int est_sid = tibero.lastDirEstSid();
        		param.put("est_sid", est_sid);
        		tibero.deleteDirEstAsset(param);
        		return  est_sid;        		
        	}
        	else 
        		return 0;
        }
        return 0;
    }

   
    @Override
    @Transactional
    public int update(HashMap<String, Object> param) throws Exception {
    	EstimationDao tibero = oracleSession.getMapper(EstimationDao.class);
        if (param.get("category").equals("1")) {
        	return tibero.updateIndirEst(param);
        }else if(param.get("category").equals("2")){
        	return tibero.updateDirEst(param);
        }
        return 0;
    }
    
    
    @Override
    public int delete(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        if (param.get("category").equals("1")) {
        	return itemDao.deleteIndirEst(param);
        }else if(param.get("category").equals("2")){
        	return itemDao.deleteDirEst(param);
        }
        return 0;
    }

    
    @Override
    public ArrayList<HashMap<String, Object>> historyList(HashMap<String, Object> param) throws Exception{
    	
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        return itemDao.getHistoryIndirEstList(param);
    }

    @Override
    public int insertIndirEstAsset(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        return itemDao.insertIndirEstAsset(param);    	
    }
    
    @Override
    public int deleteIndirEstAsset(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.deleteIndirEstAsset(param);    	
    }

    @Override
    public int saveIndirEstAsset(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	param.put("est_sid", param.get("EST_SID"));
    	
    	if(param.get("OPTION")!=null) {
    	if(param.get("OPTION").equals("DELETE"))
    		itemDao.deleteIndirEstAsset(param);
    	}
    	
    	System.out.println(param);
        JSONArray assetList = (JSONArray) param.get("ASSET_LIST");
        System.out.println( param.get("ASSET_LIST"));
        for (int i=0; i< assetList.size(); i++) {
        	HashMap<String,Object> param2 = new HashMap<String,Object>();
        	param2.put("asset_sid",assetList.get(i));
        	param2.put("est_sid", param.get("EST_SID"));
            itemDao.insertIndirEstAsset(param2);
            System.out.println(param2);
        }
    	return 1;
    }
    
    @Override
    public int saveDirEstAsset(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	param.put("est_sid", param.get("EST_SID"));
    	
    	if(param.get("OPTION")!=null) {
    	if(param.get("OPTION").equals("DELETE"))
    		itemDao.deleteIndirEstAsset(param);
    	}
    	
    	System.out.println(param);
        JSONArray assetList = (JSONArray) param.get("ASSET_LIST");
        System.out.println( param.get("ASSET_LIST"));
        for (int i=0; i< assetList.size(); i++) {
        	HashMap<String,Object> param2 = new HashMap<String,Object>();
        	param2.put("asset_sid",assetList.get(i));
        	param2.put("est_sid", param.get("EST_SID"));
            itemDao.insertDirEstAsset(param2);
            System.out.println(param2);
        }
    	return 1;
    }
    
    @Override
    public int deleteEstAsset(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	param.put("est_sid", param.get("EST_SID"));
    	
    	System.out.println(param);
        JSONArray assetList = (JSONArray) param.get("ASSET_LIST");
        System.out.println( param.get("ASSET_LIST"));
        for (int i=0; i< assetList.size(); i++) {
        	HashMap<String,Object> param2 = new HashMap<String,Object>();
        	param2.put("asset_sid",assetList.get(i));
        	param2.put("est_sid", param.get("EST_SID"));
        	if(param.get("category").equals("1")) {
        		itemDao.deleteIndirEstAsset(param2);
        	}else {
        		itemDao.deleteDirEstAsset(param2);	
        	}
            System.out.println(param2);
        }
    	return 1;
    }    
    
    @Override
    public int saveIndirEstAssetResult(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	itemDao.saveIndirEstAssetGrade(param);
        return itemDao.insertIndirEstAssetResult(param);    	
    }
    
    @Override
    public int saveDirEstAssetResult(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	

    	JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");
        
        for (int i=0; i< infoItems.size(); i++) { 
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
        	itemDao.insertDirEstAssetResult(param2);
        }
        //if(param.get("ASSET_SID")>0)
        	itemDao.saveDirEstAssetGrade(param);
        return 1;    	
    }

    @Override
    public int saveDirEstAssetResult2(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	

    	JSONArray infoItems = (JSONArray) param.get("ASSET_LIST");
        
    	itemDao.deleteDirEstAssetResult(param);
    	itemDao.deleteDirEstAsset2(param);
    	
        for (int i=0; i< infoItems.size(); i++) { 
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
        	param2.put("ASSET_SID", itemDao.getAssetSid(param2));
        	param2.put("EST_SID", param.get("EST_SID"));
        	param2.put("INSERT_ID", "testid");
        	itemDao.insertDirEstAsset2(param2);
        	itemDao.insertDirEstAssetResult2(param2);
        }
        //if(param.get("ASSET_SID")>0)
        	itemDao.saveDirEstAssetGrade(param);
        return 1;    	
    }
    
    
    /*
     * 
     * (non-Javadoc)
     * @see com.usom.model.service.estimation.estimationService#insert(java.util.HashMap)
     */
    
    @Override
    public ArrayList<HashMap<String, Object>> getFactorListExcel(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getFactorList(param);
    }
    
    @Override
    public HashMap<String, Object> getFactorList(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	
        return pagging.runPaging(itemDao.getFactorList(param), param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getFactorDetail(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    	if(param.get("category").equals("2")) {
    		return itemDao.getDirFactorDetail(param);
    	}else {
    		return itemDao.getIndirFactorDetail(param);
    	}
    }    

    @Override
    public ArrayList<HashMap<String, Object>> getFactorDetailExcel(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    	if(param.get("category").equals("2")) {
    		return itemDao.getDirFactorDetailExcel(param);
    	}else {
    		return itemDao.getIndirFactorDetailExcel(param);
    	}
    }
    
    @Override
    @Transactional
    public int insertFactor(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");
        HashMap<String, Object> map = new HashMap<>();
        int sid = 0;
        if(param.get("category").equals("2")){
        	sid = itemDao.getNextDirEstSpecSid();
            param.put("sid", sid);
            param.put("EST_SPEC_SID", sid);
            itemDao.insertDirEstSpec(param);
            itemDao.deleteDirEstSpecItem(param);
        }else {
        	sid = itemDao.getNextIndirEstSpecSid();
            param.put("sid", sid);
            //param.put("insert_id", );
            itemDao.insertIndirEstSpec(param);
            itemDao.newIndirEstSpecItem(param);
            itemDao.newIndirEstSpecItemCase(param);        	
        }
        

        for (int i=0; i< infoItems.size(); i++) {
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
    		param2.put("EST_SPEC_SID", param.get("EST_SPEC_SID"));
    		param2.put("INSERT_ID", param.get("INSERT_ID"));        	
            if(param.get("category").equals("2")){
            	//직접평가
        		for(int j=0;j<6;j++) {
        			param2.put("PIPE_TYPE", "" + j);
            		if(param2.get("TYP" + (j)).equals("Y")) {
            			param2.put("USE_YN", "Y");
            		}else {
            			param2.put("USE_YN", "N");
            		}
            		itemDao.insertDirEstSpecItem(param2);        			
        		}	
            }else {
            	//간접평가

            }
        	//itemDao.updateIndirEstSpecItemCase(param2);
        }


        return sid;
    }

   
    @Override
    @Transactional
    public int updateFactor(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");
        if(param.get("category").equals("2")){
            itemDao.updateDirEstSpec(param);
            itemDao.deleteDirEstSpecItem(param);
    	}else {
	        itemDao.updateIndirEstSpec(param);
	        itemDao.deleteIndirEstSpecItem(param);
	        itemDao.deleteIndirEstSpecItemCase(param);
    	}
        
        for (int i=0; i< infoItems.size(); i++) {
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
        	param2.put("INSERT_ID", param.get("insert_id"));
        	param2.put("EST_SPEC_SID", param.get("EST_SPEC_SID"));
        	int j=0;
        	//직접평가 항목
        	if(param.get("category").equals("2")){
        		param2.put("EST_SPEC_SID", param.get("EST_SPEC_SID"));
        		for(j=0;j<6;j++) {
        			param2.put("PIPE_TYPE", "" + j);
            		if(param2.get("TYP" + (j)).equals("Y")) {
            			param2.put("USE_YN", "Y");
            		}else {
            			param2.put("USE_YN", "N");
            		}
            		itemDao.insertDirEstSpecItem(param2);        			
        		}
        	}else {
        		//간접평가 항목
        		if(param2.get("RN").equals(param2.get("ITEM_RN"))) {
            		System.out.println("insertIndirEstSpecItem" + param2);        		
            		itemDao.insertIndirEstSpecItem(param2);
            	}
            	param2.put("EST_SPEC_SID", param.get("EST_SPEC_SID"));
            	System.out.println("insertIndirEstSpecItemCase:" + param2);
                itemDao.insertIndirEstSpecItemCase(param2);        		
        	}
        	
        }
        

        return 1;
    }
    
    
    @Override
    public int deleteFactor(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        if(param.get("category").equals("2")) {
            //itemDao.deleteDirEstSpec(param);
            //itemDao.deleteDirEstSpecItem(param);        	
        }else { //간접평가 개량방안 항목
        	//itemDao.deleteIndirSpec(param);
        	//itemDao.deleteIndirSpecItem(param);
        }
        return 1;
    }

    
    @Override
    public ArrayList<HashMap<String, Object>> historyFactorList(HashMap<String, Object> param) throws Exception{
    	
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        return itemDao.getHistoryFactorList(param);
    }
    
    /*
     * (non-Javadoc)
     * @see com.usom.model.service.estimation.estimationService#insert(java.util.HashMap)
     */
    
    @Override
    public ArrayList<HashMap<String, Object>> getGradeListExcel(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	
        return itemDao.getGradeList(param);
    }
    
    @Override
    public HashMap<String, Object> getGradeList(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	
        return pagging.runPaging(itemDao.getGradeList(param), param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getGradeDetail(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        return itemDao.getGradeDetail(param);
    }    
    
    @Override
    @Transactional
    public int insertGrade(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	int est_imp_sid = 0;
        JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");

        //직접평가 개량방안  항목 
        if(param.get("category").equals("2")) {
           	est_imp_sid =  itemDao.getNextDirEstImpSid();
            param.put("EST_IMP_SID", est_imp_sid);
            itemDao.insertDirEstImp(param);
            itemDao.deleteDirEstImpPlan(param);        	
        }else{
	       	est_imp_sid =  itemDao.getNextIndirEstImpSid();
	        param.put("EST_IMP_SID", est_imp_sid);
	        itemDao.insertIndirEstImp(param);
	        itemDao.deleteIndirEstImpPlan(param);
        }
        HashMap<String, Object> map = new HashMap<>();

        for (int i=0; i< infoItems.size(); i++) {
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
        	param2.put("EST_IMP_SID", est_imp_sid);
        	param2.put("INSERT_ID", param.get("insert_id"));

        	if(param.get("category").equals("2")) {
        		//직접평가 개량방안 항목
        		itemDao.insertDirEstImpPlan(param2);
        	}else {
        		//간접평가 개량방안 항목
	        	System.out.println("insertIndirEstImpPlan" + param2);
	        	itemDao.insertIndirEstImpPlan(param2);
        	}
        }
        
        

        return 1;
    }


    @Override
    @Transactional
    public int updateGrade(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");
        //직접평가 개량방안  항목 
        if(param.get("category").equals("2")) {
            itemDao.updateDirEstImp(param);
            itemDao.deleteDirEstImpPlan(param);        	
        }else { //간접평가 개량방안 항목
        	itemDao.updateIndirEstImp(param);
        	itemDao.deleteIndirEstImpPlan(param);
        }
        HashMap<String, Object> map = new HashMap<>();

        for (int i=0; i< infoItems.size(); i++) {
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
        	param2.put("INSERT_ID", param.get("insert_id"));
        	//직접평가 개량방안 plan 저장
        	if(param.get("category").equals("2")) {
        		itemDao.insertDirEstImpPlan(param2);	
        	}else { // 간접평가 개량방안 plan 저장
        		System.out.println("insertIndirEstImpPlan" + param2);
        	itemDao.insertIndirEstImpPlan(param2);
        	}
        }
        
        

        return 1;
    }
    
    
    @Override
    public int deleteGrade(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        if(param.get("category").equals("2")) {
            //itemDao.deleteDirEstImp(param);
            //itemDao.deleteDirEstImpPlan(param);        	
        }else { //간접평가 개량방안 항목
        	//itemDao.deleteIndirEstImp(param);
        	//itemDao.deleteIndirEstImpPlan(param);
        }
        return 1; 
    }

    
    @Override
    public ArrayList<HashMap<String, Object>> historyGradeList(HashMap<String, Object> param) throws Exception{
    	
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        return itemDao.getHistoryGradeList(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getRiskList(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskList(param);
    }

    @Override
    public int deleteRisk(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.deleteRisk(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getRiskExcelList(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskExcelList(param);
    }
    
    @Override
    public List<HashMap<String, Object>> getRiskExcelList1(HashMap<String, Object> param) throws Exception{
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskExcelList1(param);    	
    }

    @Override
    public ArrayList<HashMap<String, Object>> getRiskAssetGroup(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskAssetGroup(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getRiskAssetList(HashMap<String, Object> param) throws Exception {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getRiskAssetList(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getFactorRiskList(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getFactorRiskList(param);
    }
    
    @Override
    @Transactional
    public int insertRiskFactor(HashMap<String, Object> param) throws Exception {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);	
        JSONArray infoItems = (JSONArray) param.get("ITEM_LIST");
        HashMap<String, Object> map = new HashMap<>();
        int sid = 0;
        sid = itemDao.getNextIndirEstSpecSid();
        param.put("sid", sid);
        param.put("insert_id", sid);
        itemDao.insertIndirEstSpec(param);
        itemDao.newIndirEstSpecItem(param);
        itemDao.newIndirEstSpecItemCase(param);
        

        for (int i=0; i< infoItems.size(); i++) {
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
    		param2.put("RISK_PROB_NM", param.get("RISK_PROB_NM"));
    		param2.put("INSERT_ID", param.get("INSERT_ID")); 
        	itemDao.updateIndirEstSpecItemCase(param2);
        }


        return sid;
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getGradeRiskList(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
        return itemDao.getGradeRiskList(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getFactorRiskDetail(HashMap<String, Object> param) {
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.getFactorRiskDetail(param);
    }  
    
    
    
    //통계분석
    
    @Override
    public ArrayList<HashMap<String, Object>> getStatIndirEst(HashMap<String, Object> param){
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		if(param.get("CATEGORY").equals("간접평가")) {
    			return itemDao.getStatIndirEst(param);	
    		}else {
    			return itemDao.getStatDirEst(param);
    		}
    		
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getStatRemainLife(HashMap<String, Object> param){
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.getStatRemainLife(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getStatRisk(HashMap<String, Object> param){
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.getStatRisk(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getStatLoS(HashMap<String, Object> param){
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.getStatLoS(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getStatLcc(HashMap<String, Object> param){
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.getStatLcc(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getStatOIP(HashMap<String, Object> param){
    	EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);
    	System.out.println( param);
    		return itemDao.getStatOIP(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getStatFinance(HashMap<String, Object> param) {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        return itemDao.getStatFinance(param);
    }

    @Override
    public List<Map<String, Object>> getGISInformationAndConditionAssessmentResults(Map<String, Object> param) {
        EstimationDao itemDao = oracleSession.getMapper(EstimationDao.class);

        return itemDao.getGISInformationAndConditionAssessmentResults(param);
    }
}
