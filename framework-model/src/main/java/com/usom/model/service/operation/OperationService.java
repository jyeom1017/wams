package com.usom.model.service.operation;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.framework.model.util.CommonUtil;
import com.usom.model.dao.operation.OperationDao;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

public interface OperationService {
    // 상수운영관리
	ArrayList<HashMap<String, Object>> getConsumerList(HashMap<String, Object> param) throws Exception;
	ArrayList<HashMap<String, Object>> getConsumerGroup(HashMap<String, Object> param) throws Exception;
	
    ArrayList<HashMap<String, Object>> getBlockList(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getData_flux_wp_month(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getData_flux_wp_day(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getData_flux_wp_hour(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getData_flux_wp_min(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getData_waterQuality(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getData_waterQuality_hour(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getData_waterQuality_min(HashMap<String, Object> param) throws Exception;
    
    //조사 탐사
 	//list
    public ArrayList<HashMap<String, Object>> getREPAIR (HashMap<String, Object> param) throws Exception;
    public ArrayList<HashMap<String, Object>> getREPAIR_ASSET(HashMap<String, Object> param) throws Exception;
    public ArrayList<HashMap<String, Object>> getREPAIR_FILE(HashMap<String, Object> param) throws Exception;
    //insert
    public HashMap<String, Object> insertREPAIR(HashMap<String, Object> param) throws Exception;
    public int insertREPAIR_FILE(HashMap<String, Object> param) throws Exception;
    public HashMap<String, Object> insertREPAIR_ASSET(HashMap<String, Object> param) throws Exception;
    //update
    public int updateREPAIR_FILE(HashMap<String, Object> param) throws Exception;
    int updateREPAIR_DEL_YN(HashMap<String, Object> param) throws Exception;
    //delete
    int deleteREPAIR_FILE(HashMap<String, Object> param) throws Exception;
    
    public int saveREPAIR(HashMap<String, Object> param) throws Exception;
    int getREPAIR_SID() throws Exception;
	public int updateREPAIR_ASSET(HashMap<String, Object> param) throws Exception;
	public int deleteREPAIR_ASSET(HashMap<String, Object> param) throws Exception;
	public int cancelREPAIR(HashMap<String, Object> param) throws Exception;
    //조사 탐사
 	//list
    public ArrayList<HashMap<String, Object>> getINSPECT (HashMap<String, Object> param) throws Exception;
    public ArrayList<HashMap<String, Object>> getINSPECT_ASSET(HashMap<String, Object> param) throws Exception;
    public ArrayList<HashMap<String, Object>> getINSPECT_FILE(HashMap<String, Object> param) throws Exception;
    //insert
    public HashMap<String, Object> insertINSPECT(HashMap<String, Object> param) throws Exception;
    public int insertINSPECT_FILE(HashMap<String, Object> param) throws Exception;
    public HashMap<String, Object> insertINSPECT_ASSET(HashMap<String, Object> param) throws Exception;
    //update
    public int updateINSPECT_FILE(HashMap<String, Object> param) throws Exception;
    int updateINSPECT_DEL_YN(HashMap<String, Object> param) throws Exception;
	public int updateREPAIR_EQUAL(HashMap<String, Object> param) throws Exception;
	public int updateREPAIR_RATIO(HashMap<String, Object> param) throws Exception;
	public int updateINSPECT_EQUAL(HashMap<String, Object> param) throws Exception;
	public int updateINSPECT_RATIO(HashMap<String, Object> param) throws Exception;
	public int updateINSPECT_ASSET(HashMap<String, Object> param) throws Exception;
    //delete
    int deleteINSPECT_FILE(HashMap<String, Object> param) throws Exception;
    public int deleteINSPECT_ASSET(HashMap<String, Object> param)throws Exception;
    
    public int saveINSPECT(HashMap<String, Object> param) throws Exception;
	public int cancelINSPECT(HashMap<String, Object> param) throws Exception;
    public int getINSPECT_SID() throws Exception;
    // 민원관리
    ArrayList<HashMap<String, Object>> getMinwonList(HashMap<String, Object> param) throws Exception;
    int insertMinwonTemp(HashMap<String, Object> param) throws Exception;
    int deleteMinwonTemp(HashMap<String, Object> param) throws Exception;
    int saveMinwon(HashMap<String, Object> param) throws Exception;
    int deleteMinwon(HashMap<String, Object> param) throws Exception;
    int insertMinwonFile(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getMinwonFileList(HashMap<String, Object> param) throws Exception;
    int deleteMinwonFileOne(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getMinwonExcelList(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> getMinwonAssetList(HashMap<String, Object> param) throws Exception;
    ArrayList<HashMap<String, Object>> selectMinwonAssetPic(HashMap<String, Object> param) throws Exception;

    //비용수정
    int setCostItem(HashMap<String, Object> param) throws Exception;
    //비용관리    
    HashMap<String, Object> getCostList(HashMap<String, Object> param) throws Exception;
    int insertCost(HashMap<String, Object> param) throws Exception;
    int deleteCost(HashMap<String, Object> param) throws Exception;
    int updateCost(HashMap<String, Object> param) throws Exception;
    
    int saveCostList(HashMap<String, Object> param) throws Exception;
    int updateCostList(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getCostExcelList(HashMap<String, Object> param) throws Exception;
	ArrayList<HashMap<String, Object>> getFormatTYPE(HashMap<String, Object> param)throws Exception;
	
	//통계분석
	ArrayList<HashMap<String, Object>> getStatRenovation(HashMap<String, Object> param)throws Exception; //개보수
	ArrayList<HashMap<String, Object>> getStatMinwon(HashMap<String, Object> param)throws Exception; //민원
	ArrayList<HashMap<String, Object>> getStatInspect(HashMap<String, Object> param)throws Exception; //조사 및 탐사
	ArrayList<HashMap<String, Object>> getStatOIP(HashMap<String, Object> param)throws Exception; //최적투자
	
	//연계정보
	ArrayList<HashMap<String, Object>> getRelayInfo(HashMap<String, Object> param)throws Exception;
	

}
