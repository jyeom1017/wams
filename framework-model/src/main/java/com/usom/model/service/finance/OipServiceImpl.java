package com.usom.model.service.finance;


import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.dao.diagnosis.LccDao;
import com.usom.model.dao.finance.OipDao;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("oipService")
public class OipServiceImpl implements OipService {

	@Resource(name="tiberoSession")
    private SqlSession oracleSession;

    @Autowired
    private Pagging pagging;
    @Override
    public ArrayList<HashMap<String, Object>> getList(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	return dao.getOIP(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getFN_CD(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	return dao.getFN_CD(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getORDM(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	return dao.getORDM(param);
    }
    
    @Override
    public HashMap<String, Object> getORDMAssetList(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	HashMap<String, Object> resultMap = new HashMap<String, Object>();
    	int size = dao.getORDM_SIZE(param);
    	System.out.println(size);
    	String ORDMYYYY_N = "";
    	String ORDMYYYY_S = "";
    	for(int i=1; i < size + 1; i++) {
    		ORDMYYYY_N += i+",";
    		ORDMYYYY_S += "\""+i+"\""+",";
    	}
    	System.out.println(ORDMYYYY_N);
    	ORDMYYYY_N = ORDMYYYY_N.substring(0,ORDMYYYY_N.length()-1);
    	ORDMYYYY_S = ORDMYYYY_S.substring(0,ORDMYYYY_S.length()-1);
    	System.out.println(ORDMYYYY_N);
    	param.put("ORDMYYYY_N", ORDMYYYY_N);
    	param.put("ORDMYYYY_S", ORDMYYYY_S);
    	System.out.println(param);
    	resultMap.put("size", ORDMYYYY_N);
    	resultMap.put("List", dao.getORDM_ASSET(param));
    	return resultMap;
    }
    @Override
    public int updateORDMAssetList(HashMap<String, Object> param) throws Exception {
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	JSONArray List = (JSONArray) param.get("List");
		JSONObject Info = (JSONObject) param.get("Info");
		
		HashMap<String, Object> Infomap = CommonUtil.convertJsonToMap(Info);
		String AStr[] = Infomap.get("select").toString().split("\\*");
		String select ="";
		int count =0;
		HashMap<String, Object> Listmap = new HashMap<String, Object>();
		for (int i = 0; i < List.size(); i++) {
			Listmap = CommonUtil.convertJsonToMap(List.getJSONObject(i));
			select = "";
			for(int j=1; j < AStr.length; j++){
				select += Listmap.get((""+j).toString()) + AStr[j];
			}
			Infomap.put("select", select);
			Infomap.put("ORDM_ITEM_CD1", Listmap.get("ORDM_ITEM_CD1"));
			Infomap.put("C_NAME", Listmap.get("C_NAME"));
			
			count += dao.updateOIP_ORDM_ASSET(Infomap);
			count += dao.updateOIP_ASSET(Infomap);
		}
    	return count;
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getOIP_OPTIMAL(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	return dao.getOIP_OPTIMAL(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getOIP_OPTIMAL_NEW(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	return dao.getOIP_OPTIMAL_NEW(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getOIP_RPN(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	return dao.getOIP_RPN(param);
    }
    
    @Override
    public HashMap<String, Object> insertOIP(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	dao.insertOIP(param);
    	return param;
    }
    
    @Override
    public int analysisOIP(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	//dao.deleteOIP(param)
    	dao.updateOIP(param);
    	return dao.insertOIP_ASSET(param);
    }
    
    @Override
    public HashMap<String, Object> newList(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	//dao.insertOIP(param);
    	dao.insertOIP_ASSET(param);
    	return param;
    }
    
    @Override
    public HashMap<String, Object> getDSCNTList() throws Exception {
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	HashMap<String, Object> map = new HashMap<String, Object>();
    	map.put("Info", dao.getDSCNT());
    	map.put("List", dao.getDSCNT_ITEM());
        return map;
    }
    
    @Override
    public int saveDSCNT(HashMap<String, Object> param) throws Exception {
    	int count = 0;
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	
    	String DSCNT_NM = (String) param.get("DSCNT_NM");
    	String UPDATE_ID = (String)param.get("UPDATE_ID");
    	JSONArray List = (JSONArray) param.get("List");
    	
    	HashMap<String, Object> map = new HashMap<>();
    	map.put("DSCNT_NM", DSCNT_NM);
    	map.put("UPDATE_ID", UPDATE_ID);
    	
 		count = dao.deleteDSCNT_ITEM();
 		count += dao.updateDSCNT(map);
 		
 		for (int i = 0; i < List.size(); i++) {
        	map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
            map.put("UPDATE_ID", UPDATE_ID);
            count += dao.insertDSCNT_ITEM(map);
	    }
        return count;
    }
    
    @Override
    public int applyOipAsset(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	JSONArray List = (JSONArray) param.get("List");
		JSONObject Info = (JSONObject) param.get("Info");
		int count = 0;
		HashMap<String, Object> map = CommonUtil.convertJsonToMap(Info);
		count+=dao.updateOIP(map);
		
		for (int i = 0; i < List.size(); i++) {
			map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
			count+=dao.updateOIP_ASSET_LOS(map);
		}
		
    	return count;
    }
    
/*    @Override
    public int applyOipAssetDT(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	int count=0;
    	count+= dao.updateOIP(param);
    	count+= dao.updateOIP_ASSET_DT(param);
    	return count;
    }*/
    
    @Override
    public int applyOipAssetCost(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	return dao.updateOIP(param);
    }
    @Override
    public int saveOipAsset(HashMap<String, Object> param)throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	dao.updateOIP_ASSET_RPN(param);
    	return dao.updateOIP(param);
    }
    
    @Override
    public int deleteOIP(HashMap<String, Object> param)throws Exception {
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	return dao.deleteOIP(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> report(HashMap<String, Object> param) throws Exception{
    	OipDao dao = oracleSession.getMapper(OipDao.class);
    	return dao.report(param);
    }
}