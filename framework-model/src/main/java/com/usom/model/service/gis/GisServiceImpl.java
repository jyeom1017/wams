package com.usom.model.service.gis;


import com.framework.model.util.Pagging;
import com.usom.model.dao.asset.ItemDao;
import com.usom.model.dao.gis.GisDao;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.nio.charset.*;
import java.time.Duration;

@Service("GisService")
public class GisServiceImpl implements GisService {

	@Resource(name = "tiberoSession")
    private SqlSession tiberoSession;

    @Autowired
    private Pagging pagging;

    @Override
    public List<Map<String, Object>> getPipeInfoListFromFtrIdn(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);
        return gisDao.getPipeInfoListFromFtrIdn(param);
    }

    @Override
    public Map<String, Object> getAssetInfoWithGisInfo(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);
        ItemDao itemDao = tiberoSession.getMapper(ItemDao.class);

        HashMap<String, Object> paramMap = gisDao.getAssetInfoWithGisInfo(param);
        HashMap<String, Object> assetInfo = gisDao.getAssetInfo(paramMap);
        ArrayList<HashMap<String, Object>> assetItemList = itemDao.selectAssetItemInfo(paramMap);

        Map<String, Object> returnMap = new HashMap<>();
        Object returnAssetPathSid = null;
        Object returnAssetSid = null;
        Object returnAssetItemInfo = null;
        Object returnAssetItemList = null;

        if (MapUtils.isNotEmpty(paramMap)) {
            returnAssetPathSid = paramMap.get("ASSET_PATH_SID");
            returnAssetSid = paramMap.get("ASSET_SID");
        }
        if (MapUtils.isNotEmpty(assetInfo)) {
            returnAssetItemInfo = assetInfo;
        }
        if (CollectionUtils.isNotEmpty(assetItemList)) {
            returnAssetItemList = assetItemList;
        }

        returnMap.put("ASSET_PATH_SID", returnAssetPathSid);
        returnMap.put("ASSET_SID", returnAssetSid);
        returnMap.put("AssetItemInfo", returnAssetItemInfo);
        returnMap.put("ItemList", returnAssetItemList);
        return returnMap;
    }

    @Override
    public Map<String, Object> getAssetInfoWithLayer(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);
        ItemDao itemDao = tiberoSession.getMapper(ItemDao.class);

        Map<String, Object> assetInfo = gisDao.getAssetInfoWithLayer(param);
        List<Map<String, Object>> assetItemList = gisDao.getAssetItemInfo(assetInfo);
        itemDao.insertAsset((HashMap<String,Object>) assetInfo);
        
        HashMap<String,Object> gisLayerInfo = gisDao.getAssetPathGisLayerNm((HashMap<String,Object>)assetInfo);
        HashMap<String,Object> param1 = new HashMap<String,Object>();
        param1.putAll(assetInfo);
        param1.put("DATA_TYPE", "");
        param1.put("INSERT_ID", param.get("USER_ID"));
        param1.put("ITEM_CD", "10009");param1.put("ITEM_VAL", param.get("ftr_idn"));
        itemDao.insertAssetInfoItem(param1);
        param1.put("ITEM_CD", "10010");param1.put("ITEM_VAL",  param.get("ftr_cde"));        
        itemDao.insertAssetInfoItem(param1);
        param.put("asset_sid", param1.get("ASSET_SID"));
        param.put("layer_cd",gisLayerInfo.get("LAYER_CD"));
        gisDao.insertAssetGis1(param);
        
        

        Map<String, Object> returnMap = new HashMap<>();
        Object returnAssetPathSid = null;
        Object returnAssetSid = null;
        Object returnAssetItemInfo = null;
        Object returnAssetItemList = null;

        if (MapUtils.isNotEmpty(assetInfo)) {
            returnAssetPathSid = assetInfo.get("ASSET_PATH_SID");
            returnAssetSid = assetInfo.get("ASSET_SID");
            returnAssetItemInfo = assetInfo;
        }
        if (CollectionUtils.isNotEmpty(assetItemList)) {
            returnAssetItemList = assetItemList;
        }

        returnMap.put("ASSET_PATH_SID", returnAssetPathSid);
        returnMap.put("ASSET_SID", returnAssetSid);
        returnMap.put("AssetItemInfo", returnAssetItemInfo);
        returnMap.put("ItemList", returnAssetItemList);
        return returnMap;
    }

    @Override
    public int insertAssetGis(Map<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return gisDao.insertAssetGis(param);
    }

    @Override
    public List<Map<String, Object>> getAssetInfoWithGisList(List<Map<String, Object>> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        StringBuilder sb = new StringBuilder();
        Map<String, Object> map = new HashMap<>();

        if (CollectionUtils.isEmpty(param)) {
            sb.append("('','')");
        } else {
            for (int i = 0, paramSize = param.size(); i < paramSize; i++) {
                Map<String, Object> stringObjectMap = param.get(i);

                String layerCd = stringObjectMap.get("fid") == null ? "''" : "'" + stringObjectMap.get("fid").toString().toUpperCase().substring(0, stringObjectMap.get("fid").toString().lastIndexOf(".")) + "'";
                String ftrIdn = stringObjectMap.get("ftr_idn") == null ? "''" : (String) stringObjectMap.get("ftr_idn");

                sb.append("(" + layerCd + "," + ftrIdn + "),");
            }

            sb.deleteCharAt(sb.lastIndexOf(","));
        }

        map.put("fid", sb);

        return gisDao.getAssetInfoWithGisList(map);
    }

    @Override
    public List<Map<String, Object>> getGISInfoByAssetPath(Map<String, Object> param) {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return gisDao.getGISInfoByAssetPath(param);
    }

    @Override
    public Map<String, Object> getDiagnosticAreaList(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return pagging.runPaging(gisDao.getDiagnosticAreaList(param), param);
    }

    @Override
    public int deleteDiagnosticArea(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return gisDao.deleteDiagnosticArea(param);
    }

    @Override
    public Map<String, Object> getDiagnosticAreaDetailNewList(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return pagging.runPaging(gisDao.getDiagnosticAreaDetailNewList(param), param);
    }

    @Override
    public Map<String, Object> getDiagnosticAreaDetailEditList(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return pagging.runPaging(gisDao.getDiagnosticAreaDetailEditList(param), param);
    }

    @Override
    public int saveDiagnosticAreaDetail(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        String userId = (String) param.get("user_id");
        String itemList = (String) param.get("item_list");
        String[] split = itemList.split(",");
        List<Map<String, Object>> list = new ArrayList<>();

        gisDao.setDiagnosticArea(param);
        Object diagAreaSid = param.get("diag_area_sid");

        gisDao.deleteAllDiagnosticAreaAsset(param);

        for (String assetId : split) {
            Map<String, Object> map = new HashMap<>();
            map.put("diag_area_sid", diagAreaSid);
            map.put("asset_sid", assetId);
            map.put("user_id", userId);
            list.add(map);
        }

        return gisDao.insertDiagnosticAreaDetail(list);
    }

    @Override
    public int deleteDiagnosticAreaAsset(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return gisDao.deleteDiagnosticAreaAsset(param);
    }

    @Override
    public Map<String, Object> getStateDiagnosisGroupList(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return pagging.runPaging(gisDao.getStateDiagnosisGroupList(param), param);
    }

    @Override
    public int deleteStateDiagnosisGroup(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return gisDao.deleteStateDiagnosisGroup(param);
    }

    @Override
    public Map<String, Object> getStateDiagnosticGroupDetailPopupInfo(Map<String, Object> param) {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return gisDao.getStateDiagnosticGroupDetailPopupInfo(param);
    }

    @Override
    public Map<String, Object> getStateDiagnosisGroupDetailNewList(HashMap<String, Object> param) throws Exception {
        ArrayList<HashMap<String, Object>> list = getStateDiagnosisGroupDetailList(param);

        return pagging.runPaging(list, param);
    }

    private ArrayList<HashMap<String, Object>> getStateDiagnosisGroupDetailList(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        String f2f91effb = getF2f91effb(param);

        if (StringUtils.isNotEmpty(f2f91effb)) {
            param.put("dynamic2_val", f2f91effb);
        }

        return gisDao.getStateDiagnosisGroupDetailNewList(param);
    }

    // TODO:
    private String getF2f91effb(HashMap<String, Object> param) {
        String pipeMaterial = (String) param.get("pipe_material");
        String pipeDiameter = (String) param.get("pipe_diameter");
        String pipePurpose = (String) param.get("pipe_purpose");
        String waterPurpose = (String) param.get("water_purpose");
        StringBuilder sb = new StringBuilder();

        for (String s : Arrays.asList(pipeMaterial, pipeDiameter, pipePurpose, waterPurpose)) {
            if (StringUtils.isNotEmpty(s)) {
                if (!sb.toString().equals("")) {
                    sb.append(",");
                }
                sb.append(s);
            }
        }

        return sb.toString();
    }

    @Override
    public Map<String, Object> getStateDiagnosisGroupDetailEditList(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return pagging.runPaging(gisDao.getStateDiagnosisGroupDetailEditList(param), param);
    }

    @Override
    public int saveStateDiagnosisGroup(HashMap<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);
        gisDao.saveStateDiagGroup(param);
        gisDao.deleteStateDiagGroupAsset(param);

        String f2f91effb = getF2f91effb(param);

        if (StringUtils.isNotEmpty(f2f91effb)) {
            param.put("dynamic2_val", f2f91effb);
        }

        return gisDao.insertStateDiagGroupAsset(param);
    }

    @Override
    public List<Map<String, Object>> getAssetsByAssetPath(Map<String, Object> param) {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        List<Map<String, Object>> itemList = (List<Map<String, Object>>) param.get("item_list");
        List<Map<String, Object>> list = new ArrayList<>();

        for (Map<String, Object> map : itemList) {
            list.addAll(gisDao.getAssetsByAssetPath(map));
        }

        return list;
    }

    @Override
    public List<Map<String, Object>> getAssetsByGISProperty(Map<String, Object> param) {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        List<Map<String, Object>> itemList = (List<Map<String, Object>>) param.get("item_list");
        List<Map<String, Object>> list = new ArrayList<>();

        for (Map<String, Object> map : itemList) {
            list.addAll(gisDao.getAssetsByGISProperty(map));
        }

        return list;
    }

    @Override
    public List<Map<String, Object>> getAssetByAssetCode(Map<String, Object> param) {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return gisDao.getAssetByAssetCode(param);
    }
    
    
    @Override
    public String getGoogleMapApi(HashMap<String,Object> param) throws Exception{
    		GisDao gisDao = tiberoSession.getMapper(GisDao.class);
    		
    		HashMap <String,Object> result  = gisDao.selectTempLocationsResponse(param);
    		if(result != null) {
    			return result.get("RESPONSE").toString();
    		}
    		
            // header 설정
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            //headers.add("header_name", "header_value");
            //headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            //headers.add("AuthKey", certAuthKey);
            HttpEntity<String> entity = new HttpEntity<String>(headers);

            // 요청 URL
            String reqUrl = param.get("url").toString();
            reqUrl +=param.get("locations");
            reqUrl +=param.get("key");
            String xmlString = "";//<map id=\"ATTABZAA001R08\"><pubcUserNo/><mobYn>N</mobYn><inqrTrgtClCd>1</inqrTrgtClCd><txprDscmNo>" + param.get("test") + "</txprDscmNo><dongCode>86</dongCode><psbSearch>Y</psbSearch><map id=\"userReqInfoVO\"/></map><nts<nts>nts>14NLS0UHB3pbRg8pfOfr0yM17Iv9OIpUDT1W4vqRaGU03";
            
            
            // http 요청
            RestTemplate restTemplate = new RestTemplate();
            //Create a list for the message converters
            //List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
            //Add the String Message converter
            //messageConverters.add(new StringHttpMessageConverter());
            //Add the message converters to the restTemplate
            //restTemplate.setMessageConverters(messageConverters);

            HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);

            //ResponseEntity<Map> response = restTemplate.exchange(reqUrl, HttpMethod.POST, entity, Map.class);
            //ResponseEntity<String> response = restTemplate.execute(reqUrl, HttpMethod.GET, request,NULL,NULL);
            ResponseEntity<String> response = restTemplate.exchange(reqUrl, HttpMethod.GET, entity, String.class);
            
            //ResponseEntity<String> response = restTemplate.getForEntity(reqUrl, String.class);
            //ResponseEntity<String> response = restTemplate.postForEntity(reqUrl, request, String.class);

            if (response.getBody() == null) {
                //throw new UserException("인증번호가 유효하지 않습니다.\n인증번호를 확인해주세요.");
            	return "";
            }
            param.put("response", response.getBody());
            gisDao.insertTempLocationsResponse(param);
            //log.debug(">>> response:",response.getBody());
            // 결과값
            return response.getBody();

    }
    
    @Override
    public String getVWorldApi(HashMap<String,Object> param) throws Exception{

            // 요청 URL
            String reqUrl = param.get("url").toString();
            reqUrl +=param.get("locations");
            reqUrl +=param.get("key");
            String xmlString = "";
            

            // http 요청
            
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            
            HttpEntity<String> entity = new HttpEntity<String>(xmlString, headers);
            System.out.println(entity.getHeaders().toString());
            
            RestTemplate restTemplate = new RestTemplate();
            
            ResponseEntity<String> response = restTemplate.exchange(reqUrl, HttpMethod.GET, entity, String.class);
            
            if (response.getBody() == null) {
                //throw new UserException("인증번호가 유효하지 않습니다.\n인증번호를 확인해주세요.");
            	return "";
            }
            return response.getBody();

    }
    
    @Override
    public int updateAssetInfoItem(List<Map<String, Object>> param, String blockNm) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        Map<String, List<Map<String, Object>>> groupByLayer = getGroupByLayer(param);

        int result = 0;

        for (Map.Entry<String, List<Map<String, Object>>> layerEntry : groupByLayer.entrySet()) {
            List<Map<String, Object>> groupedLayer = layerEntry.getValue();
            // step1
            Map<String, Object> map = new HashMap<>();
            map.put("layer_nm", layerEntry.getKey());
            map.put("ftr_idn_list", getFtrIdnStringWithComma(groupedLayer));
            map.put("item_cd", getItemCd(blockNm));

            List<Map<String, Object>> assetInformationItemList = gisDao.getAssetInformationItemWithFtrIdn(map);
            // step2
            List<Map<String, Object>> list = penPineappleApplePen(groupedLayer, assetInformationItemList);

            if (list.size() > 0) {
            	ArrayList<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
            	int cnt = 500;
            	int idx = 0;
            	int i = 0;
            	for(idx=0;idx<list.size();idx+=cnt) {
            		for(i=0;i<cnt;i++) {
            			if(idx+i < list.size()) {
            			list2.add(list.get(idx+i));
            			}
            		}
            		gisDao.updateAssetInfoItem(list2);
            		//list2.removeAll(Map<String, Object>);
            		list2.clear();
            	}
                result = Math.max(0, 1);
            }
        }

        return result;
    }

    private List<Map<String, Object>> penPineappleApplePen(List<Map<String, Object>> groupedLayer, List<Map<String, Object>> getAssetInformationItemWithFtrIdn) {
        List<Map<String, Object>> list = new ArrayList<>();

        for (int i = 0; i < groupedLayer.size(); i++) {
            Map<String, Object> map = groupedLayer.get(i);
            int ftrIdn1 = Integer.parseInt(map.get("ftr_idn").toString());

            for (int j = 0; j < getAssetInformationItemWithFtrIdn.size(); j++) {
                Map<String, Object> map2 = getAssetInformationItemWithFtrIdn.get(j);
                int ftrIdn2 = ((BigDecimal) map2.get("FTR_IDN")).intValue();

                if (ftrIdn1 == ftrIdn2) {
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("ITEM_VAL", map.get("block_cd"));
                    map1.putAll(map2);
                    list.add(map1);
                    break;
                }
            }
        }
        return list;
    }

    private String getFtrIdnStringWithComma(List<Map<String, Object>> groupedLayer) {
        List<Object> list = new ArrayList<>();

        for (Map<String, Object> map : groupedLayer) {
            list.add(map.get("ftr_idn"));
        }

        return "'" + StringUtils.join(list, "','") + "'";
    }

    private String getItemCd(String block) {
        String itemCd = null;

        switch (block) {
            case "wtl_blk1_as":
                itemCd = "10016";
                break;
            case "wtl_blk2_as":
                itemCd = "10017";
                break;
            case "wtl_blk3_as":
                itemCd = "10018";
                break;
            case "wtl_blk3_sub_as":
                itemCd = "10019";
                break;
        }

        return itemCd;
    }

    private Map<String, List<Map<String, Object>>> getGroupByLayer(List<Map<String, Object>> param) {
        Map<String, List<Map<String, Object>>> groupByLayer = new HashMap<>();

        for (Map<String, Object> map : param) {
            String layerNm = (String) map.get("layer_nm");

            if (!groupByLayer.containsKey(layerNm)) {
                List<Map<String, Object>> list = new ArrayList<>();
                list.add(map);

                groupByLayer.put(layerNm, list);
            } else {
                groupByLayer.get(layerNm).add(map);
            }
        }

        return groupByLayer;
    }

    @Override
    @Transactional
    public int under42(Map<String, Object> param) throws Exception {
        GisDao gisDao = tiberoSession.getMapper(GisDao.class);

        return gisDao.under42(param);
    }
}