package com.usom.model.service.boardservice;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

public interface BoardService {
	void selectExcelList(HttpServletResponse response,HashMap<String,Object> param) throws Exception;
}
