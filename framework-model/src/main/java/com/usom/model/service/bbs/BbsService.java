package com.usom.model.service.bbs;

import java.util.ArrayList;
import java.util.HashMap;

public interface BbsService {
    public ArrayList<HashMap<String, Object>> getList(HashMap<String, Object> param) throws Exception;
    public int insertBbsTempItem(HashMap<String, Object> param) throws Exception;
    public int insertBbsItem(HashMap<String, Object> param) throws Exception;
    public int updateItem(HashMap<String, Object> param) throws Exception;
    public int deleteItem(HashMap<String, Object> param) throws Exception;
    public int deleteBbsTempItem(HashMap<String, Object> param) throws Exception;
    public int insertBbsFile(HashMap<String, Object> param) throws Exception;
    public int deleteBbsFile(HashMap<String, Object> param) throws Exception;
    public ArrayList<HashMap<String, Object>> getBbsFileList(HashMap<String, Object> param) throws Exception;
    public int deleteBbsFileOne(HashMap<String, Object> param) throws Exception;
    public int replyCount(HashMap<String, Object> param) throws Exception;
    public ArrayList<HashMap<String, Object>> getReplyList(HashMap<String, Object> param) throws Exception;
    public int updateReadCount(HashMap<String, Object> param) throws Exception;
}
