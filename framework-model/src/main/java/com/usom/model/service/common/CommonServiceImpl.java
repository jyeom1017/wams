/**
 * 
 */
package com.usom.model.service.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usom.model.dao.common.CommonDao;

import net.sf.json.JSONObject;
import net.sf.json.JSONArray;


/**
 * @author Administrator
 *
 */
@Service("commonService")
public class CommonServiceImpl implements CommonService {
	
	@Resource(name="tiberoSession")
	private SqlSession tiberoSession;
	
	@Override
	public ArrayList<HashMap<String, Object>> getBlockCodeList(	HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getBlockCodeList(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getCommonCodeGroup(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getCommonCodeGroup(param);
	}
	@Override
	public int insertCommonCodeGroup(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.insertCommonCodeGroup(param);
		//return Integer.parseInt(param.get("C_GCODE").toString() + param.get("C_SCODE").toString());
	}
	@Override
	public int updateCommonCodeGroup(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.updateCommonCodeGroup(param);
		//return Integer.parseInt(param.get("C_GCODE").toString() + param.get("C_SCODE").toString());
	}
	@Override
	public int deleteCommonCodeGroup(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.deleteCommonCodeGroup(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getCommonCodeList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getCommonCodeList(param);
	}
	@Override
	public ArrayList<HashMap<String, Object>> getFacCodeList(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getFacCodeList(param);
	}	
	
	@Override
	public HashMap<String, Object> getCommonCodeInfo(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getCommonCodeInfo(param);
	}	
	
	@Override
	public int insertCommonCodeList(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.insertCommonCodeList(param);
		//return Integer.parseInt(param.get("C_GCODE").toString() + param.get("C_SCODE").toString());
	}
	@Override
	public int updateCommonCodeList(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.updateCommonCodeList(param);
		//return Integer.parseInt(param.get("C_GCODE").toString() + param.get("C_SCODE").toString());
	}
	@Override
	public int deleteCommonCodeList(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.deleteCommonCodeList(param);
	}	
	@Override
	public ArrayList<HashMap<String, Object>> getEmployeeRoleList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getEmployeeRoleList(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getFileList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getFileList(param);
	}
	
	@Override
	public int insertFileInfo(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		System.out.println("파일 업로드 param : " + param);
		dao.insertFileInfo(param);
		return Integer.parseInt(param.get("f_num").toString()); //f_num을 리턴
	}
	
	@Override
	public int deleteFileInfo(int filenumber) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.deleteFileInfo(filenumber);
	}

	@Override
	public HashMap<String, Object> getFileInfo(int filenumber)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getFileInfo(filenumber);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getPlanList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getPlanList(param);
	}
	@Override
	public int getPlanTotalRowCount(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getPlanTotalRowCount(param);
	}
	
	
	@Override
	public HashMap<String, Object> getPlanInfo(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getPlanInfo(param);
	}

	@Override
	public int insertPlan(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.insertPlan(param);
	}

	@Override
	public int updatePlan(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.updatePlan(param);
	}

	@Override
	public int deletePlan(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.deletePlan(param);
	}


	
	@Override
	public ArrayList<HashMap<String, Object>> getMenuList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getMenuList(param);
	}


	@Override
	public int insertMenu(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		System.out.println("param : " + param);
		return dao.insertMenu(param);
	}

	@Override
	public int updateMenu(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.updateMenu(param);
	}

	@Override
	public int deleteMenu(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.deleteMenu(param);
	}

	@Override
	public int getMaxSort(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getMaxSort(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getAuthGroupList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getAuthGroupList(param);
	}
	
	@Override
	public String getNextGroupCode() throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getNextGroupCode();
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getAuthMenuList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getAuthMenuList(param);
	}

	@Override
	public int saveAuthMenu(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		
		ArrayList<HashMap<String, Object>> authGroup = dao.getAuthGroupList(param);
		if(authGroup.size()>0) {
			//그룹명 변경
			dao.updateAuthGroup(param);
		}else {
			//그룹코드 추가
			dao.insertAuthGroup(param);
		}

		//권한삭제
		dao.deleteAuthMenu(param);
		
		System.out.print(param);
		
		JSONArray MENULIST = (JSONArray) param.get("G_MENU_LIST");
		System.out.print(MENULIST);

		//권한저장
		for (int i = 0; i < MENULIST.size(); i++) {
			
			JSONObject parseJson = new JSONObject();
			
			parseJson = (JSONObject) MENULIST.get(i);
			HashMap<String, Object> param1 = new HashMap<String, Object>();
			param1.put("G_GCODE",param.get("G_GCODE"));
			param1.put("M_PART",param.get("M_PART"));
			
			Iterator <String> keys = parseJson.keys();
			while(keys.hasNext()) {
			    String key = keys.next();
			    	param1.put(key, parseJson.get(key).toString());
			}
	    	dao.insertAuthMenu(param1);
		}
		
		
		return 1;
	}	

	@Override
	public int insertAuthGroup(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.insertAuthGroup(param);
	}
	@Override
	public int updateAuthGroup(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.updateAuthGroup(param);
	}
	@Override
	public int deleteAuthGroup(HashMap<String, Object> param)
			throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.deleteAuthGroup(param);
	}
	
	//행정동 코드목록  CMT_ADAR_MA
	@Override
	public ArrayList<HashMap<String, Object>> getHJDCodeList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getHJDCodeList(param);
	}	

	//시설 사진 목록
	@Override
	public ArrayList<HashMap<String, Object>> getPictureList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getPictureList(param);
	}
	
	//이력 사진 목록
	@Override
	public ArrayList<HashMap<String, Object>> getHisPictureList(
			HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getHisPictureList(param);
	}	
	
	//출력,로그인 로그
	@Override
	public int insertSystemLog(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.insertSystemLog(param);
	}
	
	//로그인 실패카운트 증가
	@Override
	public int updateLoginFailCnt(HashMap<String,Object>param) throws Exception{
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.updateLoginFailCnt(param);		
	}
	
	
	// 사용자메뉴 설정
	@Override
	public int setUserMenu(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		if(dao.checkUserMenu(param)>0) {
			return 1;
		}else {
			return dao.insertUserMenu(param);
		}
		
	}
	// 사용자메뉴 설정 해제
	@Override
	public int unSetUserMenu(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.deleteUserMenu(param);
	}
	//로그 리스트 가져오기
	@Override
	public ArrayList<HashMap<String, Object>> getSystemLog(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getSystemLog(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getSystemLogExcel(HashMap<String, Object> param) throws Exception {
		CommonDao dao = tiberoSession.getMapper(CommonDao.class);
		return dao.getSystemLogExcel(param);
	}
}
