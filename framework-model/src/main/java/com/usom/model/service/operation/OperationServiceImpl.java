package com.usom.model.service.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.Resource;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.model.util.CommonUtil;
import com.usom.model.dao.asset.ItemDao;
import com.usom.model.dao.operation.OperationDao;
import com.usom.model.dao.report.ReportDao;
import org.springframework.transaction.annotation.Transactional;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("operationService")
public class OperationServiceImpl implements OperationService {
	
	@Resource(name="tiberoSession")
	private SqlSession oracleSession;
	
    @Resource(name = "pagging")
    private Pagging pagging;

	// 상수운영관리
    @Override
	public ArrayList<HashMap<String, Object>> getConsumerList(HashMap<String, Object> param) throws Exception{
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getConsumerList(param);
	}
    
    @Override
    public ArrayList<HashMap<String, Object>> getConsumerGroup(HashMap<String, Object> param) throws Exception{
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getConsumerGroup(param);
	}
    
	@Override
	public ArrayList<HashMap<String, Object>> getBlockList(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getBlockList(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getData_flux_wp_month(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getData_flux_wp_month(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getData_flux_wp_day(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getData_flux_wp_day(param);
	}
	
	//개보수
	//리스트
   @Override
	public ArrayList<HashMap<String, Object>> getData_flux_wp_hour(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getData_flux_wp_hour(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getData_flux_wp_min(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getData_flux_wp_min(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getData_waterQuality(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getData_waterQuality(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getData_waterQuality_hour(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getData_waterQuality_hour(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getData_waterQuality_min(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getData_waterQuality_min(param);
	}
	//개보수
	 	//list
	    public ArrayList<HashMap<String, Object>> getREPAIR(HashMap<String, Object> param) throws Exception{
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        return dao.getREPAIR(param);
	    }
	    
	    public ArrayList<HashMap<String, Object>> getREPAIR_ASSET(HashMap<String, Object> param) throws Exception{
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        return dao.getREPAIR_ASSET(param);
	    }
	    
	    public ArrayList<HashMap<String, Object>> getREPAIR_FILE(HashMap<String, Object> param) throws Exception{
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        return dao.getREPAIR_FILE(param);
	    }
	    
	    //insert
	    @Override
	    public HashMap<String, Object> insertREPAIR(HashMap<String, Object> param) throws Exception {
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	    	param.put("REPAIR_SID", "");
	    	param.put("REPAIR_NM", "");
	    	param.put("REPAIR_START_DT", "");
	    	param.put("REPAIR_END_DT", "");
	    	param.put("CONTRACT_DT", "");
	    	param.put("COMPANY_NM", "");
	    	param.put("BUSINESS_AMT", "");
	    	param.put("C_MEMO", "");
	    	param.put("INSERT_DT", "");
	    	param.put("UPDATE_DT", "");
	    	param.put("DEL_YN", "N");
	    	param.put("TEMP_YN", "Y");
	    	dao.insertREPAIR(param);
	    	param.put("Popup_SID", param.get("REPAIR_SID"));
	    	param.put("Popup_NM", param.get("REPAIR_NM"));
	    	param.put("Popup_START_DT", param.get("REPAIR_START_DT"));
	    	param.put("Popup_END_DT", param.get("REPAIR_END_DT"));
	    	param.remove("REPAIR_SID");
	    	param.remove("REPAIR_NM");
	    	param.remove("REPAIR_START_DT");
	    	param.remove("REPAIR_END_DT");
	        return param;
	    }
	    
	    @Override
	    public int insertREPAIR_FILE(HashMap<String, Object> param) throws Exception {
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        return dao.insertREPAIR_FILE(param);
	    }
	    
	    @Override
	    public HashMap<String, Object> insertREPAIR_ASSET(HashMap<String, Object> param) throws Exception {
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	    	JSONArray List = (JSONArray) param.get("List");
	    	
	    	int count = 0;
	    	int SID = (int) param.get("SID");
	    	String INSERT_ID = (String)param.get("INSERT_ID");
	    	String UPDATE_ID = (String)param.get("UPDATE_ID");
	    	
			HashMap<String, Object> map = new HashMap<String, Object>();
			for (int i = 0; i < List.size(); i++) {
			   	map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
			   	map.put("REPAIR_SID", SID);
			   	map.put("INSERT_ID", INSERT_ID);
			   	map.put("UPDATE_ID", UPDATE_ID);
			    System.out.println("리스트" + map);
			    count += dao.insertREPAIR_ASSET(map);
			}
			HashMap<String, Object> result = new HashMap<String, Object>();
			result.put("count", count);
			result.put("REPAIR_SID", SID);
	        return result;
	    }
	    @Override
		public int updateREPAIR_ASSET(HashMap<String, Object> param) throws Exception {
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        return dao.updateREPAIR_ASSET(param);
		}
	    @Override
	    public int updateREPAIR_FILE(HashMap<String, Object> param) throws Exception{
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        return dao.updateREPAIR_FILE(param);
	    }
	    
	    //delete
	    @Override
	    public int deleteREPAIR_FILE(HashMap<String, Object> param) throws Exception {
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        return dao.deleteREPAIR_FILE(param);
	    }
	    @Override
		public int deleteREPAIR_ASSET(HashMap<String, Object> param) throws Exception {
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        return dao.deleteREPAIR_ASSET(param);
		}

		@Override
		 public int updateREPAIR_DEL_YN(HashMap<String, Object> param) throws Exception{
			OperationDao dao = oracleSession.getMapper(OperationDao.class);
	       return dao.updateREPAIR_DEL_YN(param);
		}
	    
	    @Override
	    public int saveREPAIR(HashMap<String, Object> param) throws Exception {
	    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        JSONObject Info = (JSONObject) param.get("Info");
	        JSONArray List = (JSONArray) param.get("List");
	        HashMap<String, Object> map = new HashMap<>();
    		map = CommonUtil.convertJsonToMap(Info);
    		int SID = (int)map.get("Popup_SID");
    		map.put("REPAIR_SID", SID);
    		map.put("REPAIR_NM", map.get("Popup_NM"));
    		map.put("REPAIR_START_DT", map.get("Popup_START_DT"));
    		map.put("REPAIR_END_DT", map.get("Popup_END_DT"));
    		map.put("TEMP_YN", "N");
    		map.put("DEL_YN", "N");
    		String INSERT_ID = (String) map.get("INSERT_ID");
		    String ID = (String) param.get("ID");
		    int count = 0;
		    count += dao.updateREPAIR(map);
	        dao.deleteREPAIR_FILE(map);
	        
	        for (int i = 0; i < List.size(); i++) {
	        	map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
	    	    map.put("INSERT_ID", INSERT_ID);
	            map.put("UPDATE_ID", ID);
	            map.put("REPAIR_SID", SID);
	            System.out.println("리스트"+map);
	            count += dao.insertREPAIR_FILE(map);
	        }
	        
	        return count;
	    }

		@Override
		public int cancelREPAIR(HashMap<String, Object> param) throws Exception {
			OperationDao dao = oracleSession.getMapper(OperationDao.class);
			JSONObject Info = (JSONObject) param.get("Info");
		    HashMap<String, Object> map = new HashMap<>();
		    map = CommonUtil.convertJsonToMap(Info);
			param.put("REPAIR_SID", map.get("Popup_SID"));
			System.out.println(param);
			int count = 0;
			count += dao.deleteREPAIR(param);
			count += dao.deleteFILE(param);
			count += dao.deleteREPAIR_FILE(param);
			count += dao.deleteREPAIR_ASSET(param);
			return count;
		}
		
		@Override
		public int updateREPAIR_EQUAL(HashMap<String, Object> param) throws Exception {
			OperationDao dao = oracleSession.getMapper(OperationDao.class);
			dao.updateREPAIR_COST(param);
			return dao.updateREPAIR_EQUAL(param);
		}
		
		@Override
		public int updateREPAIR_RATIO(HashMap<String, Object> param) throws Exception {
			OperationDao dao = oracleSession.getMapper(OperationDao.class);
			dao.updateREPAIR_COST(param);
			return dao.updateREPAIR_RATIO(param);
		}
		
	    @Override
		public int getREPAIR_SID() throws Exception {
			OperationDao dao = oracleSession.getMapper(OperationDao.class);
	        return dao.getREPAIR_SID();
		}
	    
    //조사 탐사
 	//list
    public ArrayList<HashMap<String, Object>> getINSPECT(HashMap<String, Object> param) throws Exception{
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.getINSPECT(param);
    }
    
    public ArrayList<HashMap<String, Object>> getINSPECT_ASSET(HashMap<String, Object> param) throws Exception{
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.getINSPECT_ASSET(param);
    }
    
    public ArrayList<HashMap<String, Object>> getINSPECT_FILE(HashMap<String, Object> param) throws Exception{
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.getINSPECT_FILE(param);
    }
    
    //insert
    @Override
    public HashMap<String, Object> insertINSPECT(HashMap<String, Object> param) throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
    	param.put("INSPECT_SID", "");
    	param.put("INSPECT_NM", "");
    	param.put("INSPECT_START_DT", "");
    	param.put("INSPECT_END_DT", "");
    	param.put("CONTRACT_DT", "");
    	param.put("COMPANY_NM", "");
    	param.put("BUSINESS_AMT", "");
    	param.put("C_MEMO", "");
    	param.put("INSERT_DT", "");
    	param.put("UPDATE_DT", "");
    	param.put("DEL_YN", "N");
    	param.put("TEMP_YN", "Y");
    	dao.insertINSPECT(param);
    	param.put("Popup_SID", param.get("INSPECT_SID"));
    	param.put("Popup_NM", param.get("INSPECT_NM"));
    	param.put("Popup_START_DT", param.get("INSPECT_START_DT"));
    	param.put("Popup_END_DT", param.get("INSPECT_END_DT"));
    	param.remove("INSPECT_SID");
    	param.remove("INSPECT_NM");
    	param.remove("INSPECT_START_DT");
    	param.remove("INSPECT_END_DT");
        return param;
    }
    
    @Override
    public int insertINSPECT_FILE(HashMap<String, Object> param) throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.insertINSPECT_FILE(param);
    }
    
    @Override
    public HashMap<String, Object> insertINSPECT_ASSET(HashMap<String, Object> param) throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
    	JSONArray List = (JSONArray) param.get("List");
    	
    	int count = 0;
    	int SID = (int) param.get("SID");
    	String INSERT_ID = (String)param.get("INSERT_ID");
    	String UPDATE_ID = (String)param.get("UPDATE_ID");
    	
		HashMap<String, Object> map = new HashMap<String, Object>();
		for (int i = 0; i < List.size(); i++) {
		   	map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
		   	map.put("INSPECT_SID", SID);
		   	map.put("INSERT_ID", INSERT_ID);
		   	map.put("UPDATE_ID", UPDATE_ID);
		    System.out.println("리스트" + map);
		    count += dao.insertINSPECT_ASSET(map);
		}
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("count", count);
		result.put("INSPECT_SID", SID);
        return result;
    }
    
    public int updateINSPECT_FILE(HashMap<String, Object> param) throws Exception{
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.updateINSPECT_FILE(param);
    }
    
    //delete
    @Override
    public int deleteINSPECT_FILE(HashMap<String, Object> param) throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.deleteINSPECT_FILE(param);
    }
    public int deleteINSPECT_ASSET(HashMap<String, Object> param)throws Exception{
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.deleteINSPECT_ASSET(param);
    }
    
    public int updateINSPECT_ASSET(HashMap<String, Object> param) throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.updateINSPECT_ASSET(param);
    }
    @Override
	public int updateINSPECT_EQUAL(HashMap<String, Object> param) throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
		dao.updateINSPECT_COST(param);
		return dao.updateINSPECT_EQUAL(param);
	}
    
    @Override
	public int updateINSPECT_RATIO(HashMap<String, Object> param) throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
		dao.updateINSPECT_COST(param);
		return dao.updateINSPECT_RATIO(param);
	}
	
	@Override
	 public int updateINSPECT_DEL_YN(HashMap<String, Object> param) throws Exception{
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
       return dao.updateINSPECT_DEL_YN(param);
	}
    
    @Override
    public int saveINSPECT(HashMap<String, Object> param) throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        JSONObject Info = (JSONObject) param.get("Info");
        JSONArray List = (JSONArray) param.get("List");
        HashMap<String, Object> map = new HashMap<>();
		map = CommonUtil.convertJsonToMap(Info);
		int SID = (int)map.get("Popup_SID");
		map.put("INSPECT_SID", SID);
		map.put("INSPECT_NM", map.get("Popup_NM"));
		map.put("INSPECT_START_DT", map.get("Popup_START_DT"));
		map.put("INSPECT_END_DT", map.get("Popup_END_DT"));
		map.put("TEMP_YN", "N");
		map.put("DEL_YN", "N");
		String INSERT_ID = (String) map.get("INSERT_ID");
	    String ID = (String) param.get("ID");
	    int count = 0;
	    count += dao.updateINSPECT(map);
        dao.deleteINSPECT_FILE(map);
        
        for (int i = 0; i < List.size(); i++) {
        	map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
    	    map.put("INSERT_ID", INSERT_ID);
            map.put("UPDATE_ID", ID);
            map.put("INSPECT_SID", SID);
            System.out.println("리스트"+map);
            count += dao.insertINSPECT_FILE(map);
        }
        
        return count;
    }
    
    public int cancelINSPECT(HashMap<String, Object> param) throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
		JSONObject Info = (JSONObject) param.get("Info");
	    HashMap<String, Object> map = new HashMap<>();
	    map = CommonUtil.convertJsonToMap(Info);
		param.put("INSPECT_SID", map.get("Popup_SID"));
		System.out.println(param);
		int count = 0;
		count += dao.deleteINSPECT(param);
		count += dao.deleteFILE(param);
		count += dao.deleteINSPECT_FILE(param);
		count += dao.deleteINSPECT_ASSET(param);
		return count;
    }
    
    @Override
	public int getINSPECT_SID() throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.getINSPECT_SID();
	}
    @Override
    public ArrayList<HashMap<String, Object>>  getFormatTYPE(HashMap<String, Object> param)throws Exception {
    	OperationDao dao = oracleSession.getMapper(OperationDao.class);
        return dao.getFormatTYPE();
    }
	// 민원관리
	@Override
	public ArrayList<HashMap<String, Object>> getMinwonList(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getMinwonList(param);
	}

	@Override
	public int insertMinwonTemp(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		dao.insertMinwonTemp(param);
		param.put("complain_sid", param.get("COMPLAIN_SID"));
		dao.deleteMinwonAsset(param);
		dao.deleteMinwonFile(param);
		return Integer.parseInt(param.get("COMPLAIN_SID").toString()); // 임시글 글번호
	}

	@Override
	public int deleteMinwonTemp(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		dao.deleteMinwonAsset(param);
		dao.deleteMinwonFile(param);		
		return dao.deleteMinwonTemp(param);
	}

	@Transactional
	@Override
	public int saveMinwon(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		System.out.println("save minwon param??? : " + param);
		JSONArray assetData = (JSONArray) param.get("assetData");

		dao.deleteMinwonAsset(param);

		// 민원 자산 등록
		for (int i = 0; i < assetData.size(); i++) {
			System.out.println("asset data insert");
			HashMap<String, Object> map = new HashMap<>();
			map = CommonUtil.jsonObjectToHashMap(assetData.getJSONObject(i));
			map.put("E_ID", param.get("E_ID"));
			map.put("complain_sid", param.get("complain_sid"));

			System.out.println(map);
			dao.insertMinwonAsset(map);
		}

		//신규
		if ("new".equals(param.get("type").toString())) {
			return dao.insertMinwon(param);
		// 수정
		} else {
			return dao.updateMinwon(param);
		}
	}

	@Transactional
	@Override
	public int deleteMinwon(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		dao.deleteMinwonAsset(param); // 등록된 자산 삭제
		return dao.deleteMinwon(param);
	}

	@Override
	public int insertMinwonFile(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.insertMinwonFile(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getMinwonFileList(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getMinwonFileList(param);
	}

	@Override
	public int deleteMinwonFileOne(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.deleteMinwonFileOne(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getMinwonExcelList(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getMinwonExcelList(param);
	}
	@Override
	public ArrayList<HashMap<String, Object>> getMinwonAssetList(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getMinwonAssetList(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectMinwonAssetPic(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.selectMinwonAssetPic(param);
	}

	//비용수정
	@Override
	public int setCostItem(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.setCostItem(param);
	}
    //비용관리
	@Override
	public HashMap<String, Object> getCostList(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		System.out.print(param);
		//if (param.get("TOTCOUNT") == null || param.get("TOTCOUNT").toString().equals("0") ) { 
		param.put("TOTCOUNT", dao.getCostListCnt(param));
		//}
		ArrayList <HashMap<String, Object>>list = dao.getCostList(param);
		
		return pagging.runPaging(list, param);
	}
	
	@Override
    public int insertCost(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.insertCost(param);
	}
	
	@Override
	public int deleteCost(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.deleteCost(param);
	}
	
	@Override
    public int updateCost(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.updateCost(param);
	}

	@Override
    public int saveCostList(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		//return dao.updateCost(param);
		
		System.out.print(param);
		
		JSONArray InfoItemLIST = (JSONArray) param.get("ITEM_LIST");
		System.out.print(InfoItemLIST);

		//속성값 저장
		for (int i = 0; i < InfoItemLIST.size(); i++) {
			
			JSONObject parseJson = new JSONObject();
			parseJson = (JSONObject) InfoItemLIST.get(i);
			HashMap<String, Object> param1 = new HashMap<String, Object>();
			param1.put("INSERT_ID",param.get("INSERT_ID"));
			Iterator <String> keys = parseJson.keys();
			while(keys.hasNext()) {
			    String key = keys.next();
			    if(key.equals("COST_AMT")||key.equals("PERIOD_MONTH")||key.equals("ASSET_SID")) {
			    	param1.put(key, Integer.parseInt(parseJson.get(key).toString()));
			    }else {
			    	param1.put(key, parseJson.get(key).toString());
			    }
			}
			System.out.print(param1);
			dao.insertCost(param1);
		}
		return 1;
	}
	
	@Override
    public int updateCostList(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		//return dao.updateCost(param);
		
		System.out.print(param);
		
		JSONArray InfoItemLIST = (JSONArray) param.get("ITEM_LIST");
		System.out.print(InfoItemLIST);

		//속성값 저장
		for (int i = 0; i < InfoItemLIST.size(); i++) {
			
			JSONObject parseJson = new JSONObject();
			parseJson = (JSONObject) InfoItemLIST.get(i);
			HashMap<String, Object> param1 = new HashMap<String, Object>();
			param1.put("INSERT_ID",param.get("INSERT_ID"));
			Iterator <String> keys = parseJson.keys();
			while(keys.hasNext()) {
				String key = keys.next();
			    if(key.equals("COST_AMT")||key.equals("PERIOD_MONTH")||key.equals("ASSET_SID")) {
			    	param1.put(key, Integer.parseInt(parseJson.get(key).toString()));
			    }else {
			    	param1.put(key, parseJson.get(key).toString());
			    }
			}
			System.out.print(param1);
			dao.updateCost(param1);
		}
		return 1;
	}
	
	@Override
    public ArrayList<HashMap<String, Object>> getCostExcelList(HashMap<String, Object> param) throws Exception {
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getCostExcelList(param);
	}
	
	//통계분석
	
	//개보수
	@Override
    public ArrayList<HashMap<String, Object>> getStatRenovation(HashMap<String, Object> param)throws Exception{
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getStatRenovation(param);
	}
	
	//민원
	@Override
    public ArrayList<HashMap<String, Object>> getStatMinwon(HashMap<String, Object> param)throws Exception{
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getStatMinwon(param);
	}

	//조사및탐사
	@Override
    public ArrayList<HashMap<String, Object>> getStatInspect(HashMap<String, Object> param)throws Exception{
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getStatInspect(param);
	}
	
	//최적투자
	@Override
    public ArrayList<HashMap<String, Object>> getStatOIP(HashMap<String, Object> param)throws Exception{
		ReportDao dao = oracleSession.getMapper(ReportDao.class);
		return dao.getStatOIP(param);
	}
	
	//연계정보
	@Override
    public ArrayList<HashMap<String, Object>> getRelayInfo(HashMap<String, Object> param)throws Exception{
		OperationDao dao = oracleSession.getMapper(OperationDao.class);
		return dao.getRelayInfo(param);
	}
}