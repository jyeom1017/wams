package com.usom.model.service.asset;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.dao.asset.ItemDao;
import com.usom.model.dao.gis.GisDao;
import com.usom.model.dao.postgis.PostGisDao;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import com.usom.model.service.common.CommonService;

@Service("itemService")
public class ItemServiceImpl implements ItemService {

	@Resource(name="tiberoSession")
    private SqlSession oracleSession;
	
	@Resource(name="postgisSession")
	private SqlSession postgisSession;

    @Resource(name = "pagging")
    private Pagging pagging;

    
    @Override
    public ArrayList<HashMap<String, Object>> selectInfoItemList(HashMap<String,Object > param) throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
    	return itemDao.selectInfoItemList();
    }
    
    @Override
    public HashMap<String, Object> getItemList(HashMap<String, Object> param) {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return pagging.runPaging(itemDao.getItemList(param), param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getLevelNumListExcel(HashMap<String, Object> param) throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getLevelNumListExcel(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getLevelNumList() throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getLevelNumList();
    }

    @Override
    public int deleteAssetPath(HashMap<String, Object> param) throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.deleteAssetPath(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getInfoItemList(HashMap<String, Object> param) {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getInfoItemList(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getCustomItemList(HashMap<String, Object> param) {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getCustomItemList(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getAllAssetLevelList(HashMap<String, Object> param) throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getAllAssetLevelList(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getAllInfoItemList() throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getAllInfoItemList();
    }

    @Override
    public ArrayList<HashMap<String, Object>> getAssetMaxLevelList() throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getAssetMaxLevelList();
    }

    /**
     * 최대 레벨 일괄 저장/갱신
     * 객체 배열을 JSONArray로 파싱한 다음 각 배열 값을 JSONObject에 담아 HahsMap으로 변환 후 저장/갱신.
     */
    @Override
    @Transactional
    public int updateMaxLevelList(HttpServletRequest req) throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
        JSONArray jsonArray = CommonUtil.parseJSONArray(req);
        itemDao.deleteMaxLevelAll();
        for (int i=0; i< jsonArray.size(); i++) {
            itemDao.updateMaxLevelList(CommonUtil.jsonObjectToHashMap(jsonArray.getJSONObject(i)));
        }

        return 1;
    }

    @Override
    public void deleteMaxLevel(HashMap<String, Object> param) throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        itemDao.deleteMaxLevel(param);
    }

    /**
     * 자산 경로 및 항목 저장
     * JSONArray infoItems : 자산 경로
     * JSONObject assetPath : 표준/상태 자산 항목
     * String USER_ID : 현재 로그인 중인 아이디
     * 테이블에 저장되는 순서는 자산 경로 -> 템플릿 생성 -> 표준/상태 자산 항목 -> 사용자 정의 항목
     * 테이블에 저장되는 순서가 중요하기 때문에 순서는 되도록 유지
     */
    @Override
    @Transactional
    public int saveAssets(HashMap<String, Object> param) throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        JSONObject assetPath = (JSONObject) param.get("assetPath");
        JSONArray infoItems = (JSONArray) param.get("infoItems");
        JSONArray customItems = (JSONArray) param.get("customItems");

        HashMap<String, Object> map = new HashMap<>();
        map = CommonUtil.jsonObjectToHashMap(assetPath);

        String USER_ID = (String) map.get("USER_ID");

        itemDao.saveAssetPath(map);
        int result = itemDao.saveNewTemplate(USER_ID);
        if(result < 0) return result;

        for (int i=0; i< infoItems.size(); i++) {
            itemDao.saveNewInfoItems(CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i)));
        }
        for (int i=0; i< customItems.size(); i++) {
            itemDao.saveCustomItems(CommonUtil.jsonObjectToHashMap(customItems.getJSONObject(i)));
        }

        return result;
    }

    /**
     * 자산 경로 및 항목 업데이트
     * saveAssets() 메서드와 순서는 같지만 사용자 정의 항목은 기존의 항목을 전부 삭제하고 다시 입력
     */
    @Override
    @Transactional
    public int updateAssets(HashMap<String, Object> param) throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        JSONObject assetPath = (JSONObject) param.get("assetPath");
        JSONArray infoItems = (JSONArray) param.get("infoItems");
        JSONArray customItems = (JSONArray) param.get("customItems");

        HashMap<String, Object> map = new HashMap<>();
        map = CommonUtil.jsonObjectToHashMap(assetPath);

        String USER_ID = (String) map.get("USER_ID");
        String ASSET_PATH_SID = (String) map.get("ASSET_PATH_SID");

        itemDao.updateAssetPath(map);
        
        //int result = itemDao.saveNewTemplate(USER_ID);
        //if(result < 0) return result;
        int result = itemDao.deleteInfoItems(map);
        
        for (int i=0; i< infoItems.size(); i++) {
        	HashMap<String,Object> param2 = CommonUtil.jsonObjectToHashMap(infoItems.getJSONObject(i));
        	param2.put("TEMPLATE_SID", map.get("TEMPLATE_SID"));
            itemDao.saveInfoItems(param2);
        }

        itemDao.deleteCustomItems(ASSET_PATH_SID);

        for (int i=0; i< customItems.size(); i++) {
            itemDao.updateCustomItems(CommonUtil.jsonObjectToHashMap(customItems.getJSONObject(i)));
        }

        return result;
    }
    
    @Override
    public ArrayList<HashMap<String,Object >> selectAssetLevelPathList(HashMap<String,Object > param) throws Exception{
    	
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.selectAssetLevelPathList(param);
    }
    
    @Override
    public HashMap<String, Object> selectAssetItemList(HashMap<String,Object > param) throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
    	
    	if(param.get("BASE_INFO_LIST")!=null) {
    	String[] list = param.get("BASE_INFO_LIST").toString().split(",");
    	param.put("list",list);
    	}
    	
    	int cnt = itemDao.selectAssetItemListCnt(param);
    	param.put("TOTCOUNT",cnt);
    	return  pagging.runPaging(itemDao.selectAssetItemList(param), param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> selectAssetItemListExcel(HashMap<String,Object > param) throws Exception{
    	ArrayList<HashMap<String,Object >>  l1 ;
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
    	
    	if(param.get("BASE_INFO_LIST")!=null) {
    	String[] list = param.get("BASE_INFO_LIST").toString().split(",");
    	param.put("list",list);
    	}
    	
    	l1 = itemDao.selectInfoItemList();
    	//param.put("list",itemDao.selectInfoItemList());
    	String list1[] = new String [l1.size()]; 
    	for(int i=0;i<l1.size();i++ )
    		list1[i] = l1.get(i).get("ITEM_CD").toString();
    	
    	param.put("list1",list1);
    	if(param.get("PAGE_NO")==null) {
    	param.put("PAGE_NO", "0");
    	param.put("ROW_CNT", "0");
    	}
    	return  itemDao.selectAssetItemListExcel(param);
    }
    
    @Override
    public ArrayList<HashMap<String,Object >> selectAssetItemInfo(HashMap<String,Object > param) throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
    	return itemDao.selectAssetItemInfo(param);
    }
    
    @Override
    public int saveAssetItem(HashMap<String,Object> param) throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
    	GisDao gisDao = oracleSession.getMapper(GisDao.class);
    	PostGisDao postDao = postgisSession.getMapper(PostGisDao.class);
    	
    	int result = itemDao.insertAsset(param);
    	System.out.print(param);
    	if(result<0) return result;
    	
    	int ASSET_SID = 0;
    	String ASSET_CD = "";
    	String ITEM_CD ="";
    	ASSET_SID = itemDao.getCurrentAssetSid();
    	ASSET_CD = itemDao.getAssetCd(ASSET_SID);
		//속성값 삭제
    	itemDao.deleteAssetInfoItem(param);
    	itemDao.deleteAssetInfoCurstomItem(param);
		
		System.out.print(param);
		
		JSONArray InfoItemLIST = (JSONArray) param.get("ASSET_INFO");
		System.out.print(InfoItemLIST);
		HashMap<String,Object> gisLayerInfo = gisDao.getAssetPathGisLayerNm(param);

		//속성값 저장
		for (int i = 0; i < InfoItemLIST.size(); i++) {
			
			JSONObject parseJson = new JSONObject();
			
			parseJson = (JSONObject) InfoItemLIST.get(i);
			HashMap<String, Object> param1 = new HashMap<String, Object>();
			param1.put("ASSET_SID",ASSET_SID);
			param1.put("ASSET_PATH_SID",param.get("ASSET_PATH_SID"));
			param1.put("INSERT_ID",param.get("INSERT_ID"));
			
			Iterator <String> keys = parseJson.keys();
			while(keys.hasNext()) {
			    String key = keys.next();
			    	if(key.equals("ITEM_CD")) {
			    		ITEM_CD = parseJson.get(key).toString();
			    	}
			    	if(ITEM_CD.equals("10001") && key.equals("ITEM_VAL")) {
			    		param1.put(key, ASSET_CD);
			    	} else if(ITEM_CD.equals("10009") && key.equals("ITEM_VAL")) {
			    		if(parseJson.get(key).toString().equals("") || parseJson.get(key) == null) {
			    			param1.put("saa_cde", gisLayerInfo.get("C_CVALUE1"));
			    			param1.put("ftr_cde", gisLayerInfo.get("FTR_CDE"));
			    			param1.put("table_nm", gisLayerInfo.get("LAYER_NM"));
			    			param1.put("geom_text", param.get("GEOM_TEXT"));
			    			postDao.insertGisAsset(param1);
			    			param1.put("layer_cd", gisLayerInfo.get("LAYER_CD"));
			    			param1.put("FTR_IDN", param1.get("ftr_idn"));
			    			param1.put("asset_sid", param1.get("ASSET_SID"));
			    			param1.put("ftr_idn", Integer.parseInt(param1.get("ftr_idn").toString()));
			    			gisDao.insertAssetGis1(param1);
			    			param1.put(key, param1.get("FTR_IDN"));
			    		}else {
			    			param1.put(key, parseJson.get(key).toString());	
			    		}
			    	} else if(ITEM_CD.equals("10010") && key.equals("ITEM_VAL")) {
			    		if(parseJson.get(key).toString().equals("") || parseJson.get(key) == null) {
			    			param1.put(key, gisLayerInfo.get("FTR_CDE"));
			    		}else {
			    			param1.put(key, parseJson.get(key).toString());	
			    		}
			    	}			    	
			    	else if(!key.equals("ASSET_SID")) {
			    		param1.put(key, parseJson.get(key).toString());
			    	}
			}
			System.out.print(param1);
			if(param1.get("ITEM_GB").equals("1") || param1.get("ITEM_GB").equals("2"))
				itemDao.insertAssetInfoItem(param1);
			if(param1.get("ITEM_GB").equals("3"))			
				itemDao.insertAssetInfoCurstomItem(param1);
		}
		//접속로그
/*		HashMap<String, Object> param = new HashMap<String, Object>();
		try {
			param.put("lg_opt", "4");//1:로그인, 2:출력, 3:에러
			param.put("lg_desc", userDetailVO.getUser().getE_ID().toString() + " 로그인");
			param.put("login_user_id", userDetailVO.getUser().getE_ID().toString());
			 
			commonService.insertSystemLog(param);
			
		}catch(Exception e) {
			System.out.print("로그인 성공 후 접속로그 db 에러 param ==>" + param.toString());
		}*/
		//return result;
		return ASSET_SID;
    }

    @Override
    public int updateAssetItem(HashMap<String,Object > param) throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
    	//GisDao gisDao = oracleSession.getMapper(GisDao.class);
    	PostGisDao postDao = postgisSession.getMapper(PostGisDao.class);
    	int result =  itemDao.updateAsset(param);
    	int ASSET_SID = Integer.parseInt(param.get("ASSET_SID").toString());
    	//HashMap<String,Object> gisLayerInfo = gisDao.getAssetPathGisLayerNm(param);
    	HashMap<String,Object> gisLayerInfo = getAssetGisInfo(param);
    	//System.out.print(param);
    	if(result<0) return result;
    	
    	if(param.get("GEOM_TEXT")!=null && ! param.get("GEOM_TEXT").equals(gisLayerInfo.get("GEOM_TEXT"))) {
        	gisLayerInfo.put("geom_text", param.get("GEOM_TEXT"));
        	gisLayerInfo.put("table_nm", gisLayerInfo.get("LAYER"));
        	gisLayerInfo.put("ftr_idn", gisLayerInfo.get("FTR_IDN"));
        	postDao.updateGisAsset(gisLayerInfo);    		
    	}
		//속성값 삭제
    	itemDao.deleteAssetInfoItem(param);
    	itemDao.deleteAssetInfoCurstomItem(param);
		
		System.out.print(param);
		
		JSONArray InfoItemLIST = (JSONArray) param.get("ASSET_INFO");
		System.out.print(InfoItemLIST);

		//속성값 저장
		for (int i = 0; i < InfoItemLIST.size(); i++) {
			
			JSONObject parseJson = new JSONObject();
			
			parseJson = (JSONObject) InfoItemLIST.get(i);
			HashMap<String, Object> param1 = new HashMap<String, Object>();
			param1.put("ASSET_SID",ASSET_SID);
			param1.put("ASSET_PATH_SID",param.get("ASSET_PATH_SID"));
			param1.put("INSERT_ID",param.get("INSERT_ID"));
			Iterator <String> keys = parseJson.keys();
			while(keys.hasNext()) {
			    String key = keys.next();
			    	if(!key.equals("ASSET_SID"))
			    	param1.put(key, parseJson.get(key).toString());
			}
			System.out.print(param1);
			if(param1.get("ITEM_CD").equals("10350") && param1.get("ITEM_VAL").equals("폐기")) {
				param1.put("UseYn", "N");
				itemDao.updateUseYnAsset(param1);
			}
			if(param1.get("ITEM_CD").equals("10350") && !param1.get("ITEM_VAL").equals("폐기")) {
				param1.put("UseYn", "Y");
				itemDao.updateUseYnAsset(param1);
			}

			//초기투자비용 금액 
			if(param1.get("ITEM_CD").equals("10006")) {
				HashMap<String, Object> param2 = new HashMap<String, Object>();
				param2.put("ASSET_SID",ASSET_SID);
				param2.put("COST_AMT",param1.get("ITEM_VAL"));
				param2.put("EXPENSE_ITEM","01");
				itemDao.updateAssetInitCost(param2);
			}
			
			if(param1.get("ITEM_GB").equals("1") || param1.get("ITEM_GB").equals("2"))
				itemDao.insertAssetInfoItem(param1);
			if(param1.get("ITEM_GB").equals("3"))			
				itemDao.insertAssetInfoCurstomItem(param1);
		}
		return result;
    	
    }
    //폐기
    @Override
    public int deleteAssetItem(HashMap<String,Object > param) throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
		//속성값 삭제
    	//itemDao.deleteAssetInfoItem(param);
    	//itemDao.deleteAssetInfoCurstomItem(param);    	
    	//itemDao.deleteAsset(param);
    	param.put("UseYn", "N");
		return itemDao.updateUseYnAsset(param);
    }

    //불용처리
    @Override
    public int deleteAssetItem2(HashMap<String,Object > param) throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
		//속성값 삭제
    	//itemDao.deleteAssetInfoItem(param);
    	//itemDao.deleteAssetInfoCurstomItem(param);    	
    	//itemDao.deleteAsset(param);
    	param.put("UseYn", "N");
		return itemDao.updateUselessAsset(param);
    }
    
    @Override
    public int deleteAsset(HashMap<String,Object > param) throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
		//속성값 삭제
    	itemDao.deleteAssetInfoItem(param);
    	itemDao.deleteAssetInfoCurstomItem(param);    	
    	return itemDao.deleteAsset(param);
    }    
    
    @Override
    public ArrayList<HashMap<String,Object >> getAssetLevelList() throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
    	return itemDao.getAssetLevelList();
    }

    @Override
    public int isAssetPathExists(HashMap<String, Object> param) throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.isAssetPathExists(param);
    }

    @Override
    public int getTotalCount() throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getTotalCount();
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> selectAssetPic(HashMap<String, Object> param) throws Exception{
    	
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getAssetPicture(param);
    }
    
    @Override
    public int updateAssetPic(HashMap<String, Object> param) throws Exception{
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.updateAssetPicture(param);    	
    }
    
    @Override
    public ArrayList<HashMap<String,Object >> selectAssetHistory(HashMap<String,Object > param) throws Exception{
    	
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.getAssetHistory(param);
    }

    @Override
    public int isAssetPathUsed(HashMap<String, Object> param) throws Exception {
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);

        return itemDao.isAssetPathUsed(param);
    }
    
    @Override
    public HashMap<String, Object> getAssetGisInfo(HashMap<String, Object> param) throws Exception{
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
        GisDao gisDao = oracleSession.getMapper(GisDao.class);
        PostGisDao postDao = postgisSession.getMapper(PostGisDao.class);
        
        HashMap<String,Object> result = itemDao.getAssetGisInfo(param);
        
        if(result!=null) {
            HashMap<String,Object> result1 =(postDao.getGisInfo(result));
            if(result1 !=null) {
            result.put("GEOM_TEXT", result1.get("geom_text"));
            result.put("GID", result1.get("gid"));
            return result;
            }
        }
        result = gisDao.getAssetPathGisLayerNm(param);
        if(result!=null)
        	result.put("LAYER", result.get("LAYER_NM"));
        return result;
    }
    @Override
    public ArrayList<HashMap<String,Object>> getAssetFromGisInfo(HashMap<String, Object> param) throws Exception
    {
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
        return itemDao.getAssetFromGisInfo(param);    	
    }
    
    @Override
    public int updateAssetGis(HashMap<String, Object> param) throws Exception{
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
        return itemDao.updateAssetGis(param);    	
    }
    
    @Override
    public int insertAssetGis(HashMap<String, Object> param) throws Exception{
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
        itemDao.deleteAssetGis(param);
        return itemDao.insertAssetGis(param);    	
    }
    
    @Override
    public int deleteAssetGis(HashMap<String, Object> param) throws Exception{
        ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
        return itemDao.deleteAssetGis(param);    	
    }
    
    @Override
    public ArrayList<HashMap<String,Object>> findAssetList(HashMap<String, Object> param) throws Exception
    {
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
        return itemDao.findAssetList(param);    	
    }
    
    @Override
    public ArrayList<HashMap<String,Object>> getStatAsset(HashMap<String, Object> param) throws Exception
    {
    	ItemDao itemDao = oracleSession.getMapper(ItemDao.class);
        return itemDao.getStatAsset(param);    	
    }
}
