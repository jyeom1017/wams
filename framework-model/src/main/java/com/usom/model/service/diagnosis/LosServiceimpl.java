package com.usom.model.service.diagnosis;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.model.util.CommonUtil;
import com.usom.model.dao.diagnosis.LosDao;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("losService")
public class LosServiceimpl implements LosService {
	
	@Resource(name="tiberoSession")
	private SqlSession oracleSession;
	
	@Override
	public ArrayList<HashMap<String, Object>> selectLos(HashMap<String, Object> param) throws Exception{
		LosDao dao = oracleSession.getMapper(LosDao.class);
	    return dao.getLOS(param);
    }
	
	@Override
	public HashMap<String, Object> getEvalueItem(HashMap<String, Object> param)throws Exception{
		LosDao dao = oracleSession.getMapper(LosDao.class);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("EVALUE", dao.getLOS_EVALUE_ITEM(param));
		map.put("PARAM", dao.getLOS_PARAM_ITEM(param));
		return map;
	}
	
	@Override
	public int insertEvalueItem(HashMap<String, Object> param)throws Exception{
		LosDao dao = oracleSession.getMapper(LosDao.class);
		Object Sid = null;
		/*HashMap<String, String> type = new HashMap<>();
		type.put("인력", "01");
		type.put("시설", "02");
		type.put("운영", "03");
		type.put("서비스", "04");
		type.put("환경", "05");
		type.put("재정", "06");*/
		
		JSONArray EvaluList = (JSONArray) param.get("EvaluList");
		JSONArray ParamList = (JSONArray) param.get("ParamList");
		JSONObject Info = (JSONObject) param.get("Info");  
		
		HashMap<String, Object> map = CommonUtil.convertJsonToMap(Info);
		String INSERT_ID = (String)map.get("INSERT_ID");
		String INSERT_DT = (String)map.get("INSERT_DT");
		String UPDATE_ID = (String)map.get("UPDATE_ID");
		String UPDATE_DT = (String)map.get("UPDATE_DT");
		String LOS_YEAR =  (String)map.get("LOS_YEAR");
		//map.put("LOS_EVALUE_SID", 0);
		dao.insertLOS(map);
		Sid = map.get("LOS_EVALUE_SID");
		
		dao.deleteLOS_EVALUE_ITEM(LOS_YEAR);
		dao.deleteLOS_PARAM_ITEM(LOS_YEAR);
		
		for (int i = 0; i < EvaluList.size(); i++) {
		   	map = CommonUtil.convertJsonToMap(EvaluList.getJSONObject(i));
		   	map.put("INSERT_ID", INSERT_ID);
		   	map.put("INSERT_DT", INSERT_DT);
		   	map.put("UPDATE_ID", UPDATE_ID);
		   	map.put("UPDATE_DT", UPDATE_DT);
		   	map.put("LOS_YEAR", LOS_YEAR);
		   //  	System.out.println(map);
		   //	map.put("LOS_ITEM_CD", map.get("LOS_ITEM_CD"));
		   //	System.out.println(map);
		    dao.insertLOS_EVALUE_ITEM(map);
		}
		
		for (int i = 0; i < ParamList.size(); i++) {
		   	map = CommonUtil.convertJsonToMap(ParamList.getJSONObject(i));
		   	map.put("INSERT_ID", INSERT_ID);
		   	map.put("INSERT_DT", INSERT_DT);
		   	map.put("UPDATE_ID", UPDATE_ID);
		   	map.put("UPDATE_DT", UPDATE_DT);
		   	map.put("LOS_YEAR", LOS_YEAR);
		   	//map.put("LOS_ITEM_CD", type.get(map.get("LOS_ITEM_CD")));
		    //System.out.println("리스트"+map);
		    dao.insertLOS_PARAM_ITEM(map);
		}
		
		return Integer.parseInt(Sid.toString());
	}
	
	@Override
	public HashMap<String, Object> getWeightList(HashMap<String, Object> param)throws Exception{
		LosDao dao = oracleSession.getMapper(LosDao.class);
		HashMap<String, Object> map = new HashMap<String, Object>();
		ArrayList<HashMap<String, Object>> List = dao.get_LOS_WEIGHT_DETAIL();
		int index = (int)param.get("index");
		map.put("List", List);
		map.put("Info", List.get(index));
		map.put("WeightOptionList", dao.get_LOS_WEIGHT_OPTION(List.get(index)));
		map.put("WeightInfo", dao.getLOS_WEIGHT());
		map.put("OptionList", dao.get_LOS_OPTION_LIST());
		return map;
	}
	
	@Override
	public int insertWeight(HashMap<String, Object> param)throws Exception{
		LosDao dao = oracleSession.getMapper(LosDao.class);
		
		JSONArray WeightInfo = (JSONArray) param.get("WeightInfo");
		JSONArray WeightOptionList = (JSONArray) param.get("WeightOptionList");
		JSONObject Info = (JSONObject) param.get("Info");
		int count = 0;
		HashMap<String, Object> Infomap = CommonUtil.convertJsonToMap(Info);
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(Infomap.get("CHOICE_YN").toString().equals("Y")) {
			count += dao.changeCHOICE_N();
			
			for (int i = 0; i < WeightInfo.size(); i++) {
				map = CommonUtil.convertJsonToMap(WeightInfo.getJSONObject(i));
				map.put("INSERT_ID", Infomap.get("INSERT_ID"));
				map.put("INSERT_DT", Infomap.get("INSERT_DT"));
				map.put("UPDATE_ID", Infomap.get("UPDATE_ID"));
				map.put("UPDATE_DT", Infomap.get("UPDATE_DT"));
				dao.updateLOS_WEIGHT(map);
			}
			
			dao.insertLOS_WEIGHT_INFO_N();
		}
		
		dao.insertLOS_WEIGHT_INFO(Infomap);
		
		count += dao.deleteLOS_WEIGHT_DETAIL(Infomap);
		for (int i = 0; i < WeightInfo.size(); i++) {
			map = CommonUtil.convertJsonToMap(WeightInfo.getJSONObject(i));
			map.put("LOS_DT", Infomap.get("LOS_DT"));
			map.put("CHOICE_YN", Infomap.get("CHOICE_YN"));
			map.put("INSERT_ID", Infomap.get("INSERT_ID"));
			map.put("INSERT_DT", Infomap.get("INSERT_DT"));
			map.put("UPDATE_ID", Infomap.get("UPDATE_ID"));
			map.put("UPDATE_DT", Infomap.get("UPDATE_DT"));
			dao.insertLOS_WEIGHT_DETAIL(map);
		}
		count += dao.deleteLOS_WEIGHT_OPTION(Infomap);
		for (int i = 0; i < WeightOptionList.size(); i++) {
			map = CommonUtil.convertJsonToMap(WeightOptionList.getJSONObject(i));
			map.put("LOS_DT", Infomap.get("LOS_DT"));
			dao.insertLOS_WEIGHT_OPTION(map);
		}
		
		
		
		
		
		return count;
	}
	
	@Override
	public ArrayList<HashMap<String, Object>>  selectWeight(HashMap<String, Object> param) throws Exception{
		LosDao dao = oracleSession.getMapper(LosDao.class);
	    return dao.select_LOS_WEIGHT_DETAIL(param);
    }
	
	@Override
	public int  insertEvalue(HashMap<String, Object> param) throws Exception{
		LosDao dao = oracleSession.getMapper(LosDao.class);
		JSONArray ReusltArray = (JSONArray) param.get("ReusltArray");
		JSONObject Info = (JSONObject) param.get("Info");  
		HashMap<String, Object> Infomap = CommonUtil.convertJsonToMap(Info);
		 
		int count = 0;
		System.out.println(Infomap);
		count = dao.deleteLOS_EVALUE(Infomap);
		HashMap<String, Object> map = new HashMap<String, Object>();
		for (int i = 0; i < ReusltArray.size(); i++) {
			map = CommonUtil.convertJsonToMap(ReusltArray.getJSONObject(i));
			map.put("LOS_EVALUE_SID", (int)Infomap.get("LOS_EVALUE_SID"));
			map.put("INSERT_ID", Infomap.get("INSERT_ID"));
			map.put("INSERT_DT", Infomap.get("INSERT_DT"));
			map.put("UPDATE_ID", Infomap.get("UPDATE_ID"));
			map.put("UPDATE_DT", Infomap.get("UPDATE_DT"));
			map.put("LOS_YEAR", Infomap.get("LOS_YEAR"));
			dao.insertLOS_EVALUE(map);
		}
		
	    return count;
    }
	
	@Override
	public HashMap<String, Object>  selectEvalue(HashMap<String, Object> param) throws Exception{
		LosDao dao = oracleSession.getMapper(LosDao.class);
		HashMap<String, Object> map = new HashMap<String, Object>();
		ArrayList<HashMap<String, Object>> array = dao.getLOS_EVALUE(param);
		map.put("Array", array);
	
		map.put("Info", dao.getLOSArray(param));
		System.out.println(map);
	    return map;
    }
	@Override
	public int deleteLos(HashMap<String, Object> param) throws Exception{
		int count = 0;
		LosDao dao = oracleSession.getMapper(LosDao.class);
		count += dao.deleteLOS_EVALUE(param);
		count += dao.deleteLOS(param);
		return count;
	}
	/*reevalService
	   selectReEst
	   insertReEst
	   updateReEst
	   DeleteReEst
	   getNextReEstSid
	   insertReEstAsset
	   deleteReEstAsset
	   selectReEstAsset
	   
	   selectPriceIndex
	   insertPriceIndex
	   updatePriceIndex
	   deletePriceIndex
	   
	   selectWeightSpec
	   insertWeightSpec
	   updateWeightSpec
	   deleteWeightSpec
	   
	   insertWeightSpecItem
	   deleteWeightSpecItem
	   selectWeightSpecItem
*/
}