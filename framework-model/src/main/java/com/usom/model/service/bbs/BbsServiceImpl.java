package com.usom.model.service.bbs;

import com.usom.model.dao.bbs.BbsDao;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.annotation.Resource;

@Service("bbsService")
public class BbsServiceImpl implements BbsService {
    Logger logger = Logger.getLogger(BbsServiceImpl.class.getName());

    @Resource(name="tiberoSession")
    private SqlSession oracleSession;

    @Override
    public ArrayList<HashMap<String, Object>> getList(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        return dao.getList(param);
    }

    @Override
    public int insertBbsTempItem(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);

        //질의/응답인 경우 GUBUN에 "질의" 추가
        if("qna".equals(param.get("bbs_group_cd"))){

            if (!"응답".equals(param.get("gubun"))) {
                param.put("gubun", "질의");
            }
//            param.put("GUBUN", "질의");

//            if("answer".equals(param.get("qna_type"))){
//                param.put("GUBUN", "응답");
//            }
        }
        dao.insertBbsTempItem(param);
        param.put("bbs_sid", param.get("BBS_SID"));
        dao.deleteM2BbsFile(param);
        dao.deleteM2File(param);
        return Integer.parseInt(param.get("BBS_SID").toString()); //insert한 임시글의 bbs_sid 리턴
    }

    @Transactional
    @Override
    public int insertBbsItem(HashMap<String, Object> param) throws Exception {
        logger.info("insert bbs item param >>>>> " + param);
        BbsDao dao = oracleSession.getMapper(BbsDao.class);

        //신규
        if ("new".equals(param.get("type").toString())) {
            //신규글 중에서 댓글일 때 (응답)
            if (param.get("gubun") != null && "응답".equals(param.get("gubun").toString())) {
                param.put("subject", param.get("subject_content"));
                logger.info("신규 insert bbs item param >>>>> " + param);
                dao.insertBbsItem(param);
                return dao.updateReplyCount(param); // insert 후 댓글 개수 카운트
            } else {
                return dao.insertBbsItem(param);
            }

            // 수정
        } else {
            if (param.get("gubun") != null && "응답".equals(param.get("gubun").toString())) {
                param.put("subject", param.get("subject_content"));
                logger.info("수정 insert bbs item param >>>>> " + param);
            }
            return dao.updateBbsItem(param);
        }
    }

    @Override
    public int updateItem(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        return dao.updateItem(param);
    }

    @Override
    public int deleteItem(HashMap<String, Object> param) throws Exception {
        logger.info("serviceimpl param : " + param);
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        if ("qna".equals(param.get("bbs_group_cd")) && "응답".equals(param.get("gubun").toString())) {
            dao.deleteItem(param);
            return dao.updateReplyCount(param); // 댓글(응답) 삭제 후 댓글 개수 update
        } else {
            return dao.deleteItem(param);
        }
    }

    @Override
    public int deleteBbsTempItem(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        dao.deleteM2BbsFile(param);
        dao.deleteM2File(param);        
        return dao.deleteBbsTempItem(param);
    }

    @Override
    public int deleteBbsFile(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
//        dao.deleteM2File(param);
        return dao.deleteM2BbsFile(param);
    }

    @Override
    public int insertBbsFile(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        return dao.insertBbsFile(param);
    }
    @Override
    public ArrayList<HashMap<String, Object>> getBbsFileList(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        return dao.getBbsFileList(param);
    }

    @Override
    public int deleteBbsFileOne(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        return dao.deleteBbsFileOne(param);
    }

    @Override
    public int replyCount(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        return dao.replyCount(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getReplyList(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        return dao.getReplyList(param);
    }

    @Override
    public int updateReadCount(HashMap<String, Object> param) throws Exception {
        BbsDao dao = oracleSession.getMapper(BbsDao.class);
        return dao.updateReadCount(param);
    }
}
