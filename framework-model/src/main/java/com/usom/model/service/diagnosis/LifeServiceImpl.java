package com.usom.model.service.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.annotate.JsonTypeInfo.As;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.model.util.CommonUtil;
import com.usom.model.dao.asset.ItemDao;
import com.usom.model.dao.diagnosis.LifeDao;
import org.springframework.transaction.annotation.Transactional;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("diagnosisService")
public class LifeServiceImpl implements LifeService {
	
	@Resource(name="tiberoSession")
	private SqlSession oracleSession;
	
	@Resource(name = "pagging")
    private Pagging pagging;
	
	@Override
	public ArrayList<HashMap<String, Object>> getLIFE(HashMap<String, Object> param) throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
	    return dao.getLIFE(param);
    }
	
	@Override
	public HashMap<String, Object> getFactor(int index) throws Exception {
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ListInfo", dao.getLIFE_SPEC());
		ArrayList<HashMap<String, Object>> List = (ArrayList<HashMap<String, Object>>) map.get("ListInfo");
		HashMap<String, Object> param = List.get(index);
		map.put("Info", param);
		
		param.put("PHYSICAL_LIFE_METHOD", 1);
		map.put("List1", dao.getLIFE_SPEC_CAP1(param));
		param.put("PHYSICAL_LIFE_METHOD", 2);
		map.put("List2", dao.getLIFE_SPEC_CAP1(param));
		param.put("PHYSICAL_LIFE_METHOD", 3);
		map.put("List3", dao.getLIFE_SPEC_CAP2(param));
		param.remove("PHYSICAL_LIFE_METHOD");
		map.put("LIFE_SPEC_ITEM", dao.getLIFE_SPEC_ITEM());
		return map;
	}
	@Override
	public int saveLife_Spec(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		JSONObject Info = (JSONObject) param.get("Info");
		JSONArray CAP1List = (JSONArray) param.get("CAP1List");
	    JSONArray CAP1_1List = (JSONArray) param.get("CAP1_1List");
	    JSONObject CAP2List = (JSONObject) param.get("CAP2List");
	    int count = 0;
	    HashMap<String, Object> hInfo = null;
	    HashMap<String, Object> map = new HashMap<String, Object>();
		map = CommonUtil.convertJsonToMap(Info);
		//Info
		count += dao.insertLIFE_SPEC(map);
		hInfo = map;
		count += dao.deleteLIFE_SPEC_ITEM(map);
		
		//CAP1
		for (int i = 0; i < CAP1List.size(); i++) {
		   	map = CommonUtil.convertJsonToMap(CAP1List.getJSONObject(i));
		   	map.put("PHYSICAL_LIFE_METHOD", 1);
		   	map.put("LIFE_SPEC_SID", hInfo.get("LIFE_SPEC_SID"));
			map.put("INSERT_ID", hInfo.get("INSERT_ID"));
			map.put("INSERT_DT", hInfo.get("INSERT_DT"));
			map.put("UPDATE_ID", hInfo.get("UPDATE_ID"));
			map.put("UPDATE_DT", hInfo.get("UPDATE_DT"));
		   	count += dao.insertLIFE_SPEC_CAP1(map);
		}
		//CAP1_1
		for (int i = 0; i < CAP1_1List.size(); i++) {
		   	map = CommonUtil.convertJsonToMap(CAP1_1List.getJSONObject(i));
		   	map.put("PHYSICAL_LIFE_METHOD", 2);
		   	map.put("LIFE_SPEC_SID", hInfo.get("LIFE_SPEC_SID"));
		   	map.put("INSERT_ID", hInfo.get("INSERT_ID"));
			map.put("INSERT_DT", hInfo.get("INSERT_DT"));
			map.put("UPDATE_ID", hInfo.get("UPDATE_ID"));
			map.put("UPDATE_DT", hInfo.get("UPDATE_DT"));
		   	count += dao.insertLIFE_SPEC_CAP1(map);
		}
		//CAP2
	   	map = CommonUtil.convertJsonToMap(CAP2List);
		map.put("LIFE_SPEC_SID", hInfo.get("LIFE_SPEC_SID"));
		map.put("INSERT_ID", hInfo.get("INSERT_ID"));
		map.put("INSERT_DT", hInfo.get("INSERT_DT"));
		map.put("UPDATE_ID", hInfo.get("UPDATE_ID"));
		map.put("UPDATE_DT", hInfo.get("UPDATE_DT"));
	   	count += dao.insertLIFE_SPEC_CAP2(map);
	    
		return count;
	}
	@Override
	public HashMap<String, Object> newLife(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		dao.insertLIFE(param);
		System.out.println("---------------------"+param);
		dao.insertAsset(param);
		return param;
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET(HashMap<String, Object> param) throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
	    return dao.getLIFE_ASSET(param);
    }
	
	@Override
	public int updateLIFE_ASSET(HashMap<String, Object> param)throws Exception {
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.updateLIFE_ASSET(param);
	}
	
	@Override
	public HashMap<String, Object> getLIFE_ASSET_VARIATION(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		HashMap<String, Object> result = null;
		result = dao.getLIFE_ASSET_VARIATION(param);
		if(result == null ) {
			result = new HashMap<String, Object>();
			result.put("success", true);
		}
			
		return result;
	}
	
	@Override
	public int saveLIFE(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		int count = 0;
		count += dao.updateLIFE(param);
		count += dao.saveLIFE_ASSET(param);
		return dao.updateLIFE(param);
	}
	
	@Override
	public int continueLIFE_ASSET(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		
		return dao.continueLIFE_ASSET(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> outputLIFE_ASSET(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		
		return dao.outputLIFE_ASSET(param);
	}
	
	@Override
	public HashMap<String, Object> getFN_LIST(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		HashMap<String, Object> map = new HashMap<String, Object>();
		param.put("CLASS3_CD", "F");
		System.out.println(param);
		map.put("Flist", dao.getFN_LIST(param));
		param.put("CLASS3_CD", "N");
		System.out.println(param);
		map.put("Nlist", dao.getFN_LIST(param));
		return map;
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_RESULT(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.getLIFE_ASSET_RESULT(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		param.put("YYYY", CommonUtil.getDate("yyyy"));
	    return dao.getLIFE_ASSET_STATUS_GRADE(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE_MODIFY(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		int YCOL = dao.getLIFE_YCOL();
		ArrayList<String> ycolIndex = new ArrayList<String>(); 
		for(int i=0; i<YCOL; i++) {
			ycolIndex.add(i+"");
		}
		param.put("YYYY", CommonUtil.getDate("yyyy"));
		param.put("List", ycolIndex);
		param.put("YCOL", YCOL);
		
		return dao.getLIFE_ASSET_STATUS_GRADE_MODIFY(param);
	}
	
	@Override
	public int getLIFE_YCOL(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.getLIFE_YCOL();
	}
	
	@Override
	public HashMap<String, Object> saveLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.saveLIFE_ASSET_STATUS_GRADE(param);
	}
	
	@Override
	public int updateLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.updateLIFE_ASSET_STATUS_GRADE(param);
	}
	
	@Override
	public int resetLIFE_ASSET_STATUS_GRADE_MODIFY(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		int count = dao.deleteLIFE_ASSET_STATUS_GRADE_MODIFY_TEMP_YN();
		count = dao.resetLIFE_ASSET_STATUS_GRADE_TEMP_RESULT(param);
		return count;
	}
	
	@Override
	public int saveLIFE_ASSET_STATUS_GRADE_MODIFY(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.saveLIFE_ASSET_STATUS_GRADE_MODIFY(param);
	}
	
	@Override
	public HashMap<String, Object> getUPDATE_ID(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.getUPDATE_ID();
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_GRAPH(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.getLIFE_ASSET_GRAPH(param);
	}
	
	@Override
	public int deleteList(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.deleteLIFE(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getAssetReport(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.getLIFE_ASSET_REPORT(param);
	}
	@Override
	public ArrayList<HashMap<String, Object>> getAssetFrom(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.getLIFE_ASSET_FROM(param);
	}
	@Override
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE_REPORT(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		param.put("YYYY", CommonUtil.getDate("yyyy"));
		return dao.getLIFE_ASSET_STATUS_GRADE_REPORT(param);
	}
	
	@Override
	public int OnFileSelectLIFE(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		if((int)param.get("startPos") == 0) {
			dao.deleteLIFE_ASSET(param);
		}
		System.out.println("xxxxxxxxxxxxxxxxx"+param);
		return dao.OnFileSelectLIFE(param);
	}
	
	@Override
	public int getTotal(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.getTotalCount();
	}
	
	@Override
	public int updateLIFE(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		param.put("DEL_YN", "N");
		param.put("TEMP_YN", "Y");
		System.out.println(param);
		return dao.updateLIFE(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_REPORT_FORM(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.getLIFE_ASSET_REPORT_FORM(param);
	}
	
	@Override
	public int Test(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		dao.insertAsset(param);
		return 0;
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_REPORT(HashMap<String, Object> param)throws Exception{
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.getLIFE_ASSET_REPORT(param);
	}

	@Override
	public int refreshLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param) throws Exception {
		LifeDao dao = oracleSession.getMapper(LifeDao.class);
		return dao.refreshLIFE_ASSET_STATUS_GRADE(param);
	}
}