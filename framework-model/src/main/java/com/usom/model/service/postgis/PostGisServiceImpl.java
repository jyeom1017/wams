/**
 *
 */
package com.usom.model.service.postgis;

import com.usom.model.dao.postgis.PostGisDao;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Service("PostGisService")
public class PostGisServiceImpl implements PostGisService {

    @Resource(name = "postgisSession")
    private SqlSession postgisSession;

    public ArrayList<HashMap<String, Object>> getBlockInfo(HashMap<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        return dao.getJqgridTestList(param);
    }

    public int updateBlockInfo(HashMap<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        return dao.getJqgridTestTotalRowCount(param);
    }

    public int insertList(HashMap<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        return dao.insertList(param);
    }

    public int deleteList(HashMap<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        return dao.deleteList(param);
    }

    public ArrayList<HashMap<String, Object>> getTempPipeList(HashMap<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        return dao.getTempPipeList(param);
    }

    public ArrayList<HashMap<String, Object>> getNodeList(HashMap<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        return dao.getNodeList(param);
    }

    @Override
    public int deleteDoctor1(Map<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        return dao.deleteDoctor1(param);
    }

    @Override
    public int insertVerify25310(Map<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        return dao.insertVerify25310(param);
    }

    @Override
    public int updateFeatureCodeWithLastData(Map<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        return dao.updateFeatureCodeWithLastData(param);
    }

    @Override
    public int updateBlockInformation(Map<String, Object> param) throws Exception {
        PostGisDao dao = postgisSession.getMapper(PostGisDao.class);
        String[] split = ((String) param.get("feature_id")).split("\\.");
        String tableNm = split[0];
        int gid = Integer.parseInt(split[1]);
        String methodName = "";

        if (tableNm.equals("wtl_blk1_as")) {
            methodName = "updateLargeBlock";
        } else if (tableNm.equals("wtl_blk2_as")) {
            methodName = "updateMediumBlock";
        } else if (tableNm.equals("wtl_blk3_as")) {
            methodName = "updateSmallBlock";
        } else if (tableNm.equals("wtl_blk3_sub_as")) {
            methodName = "updateManagementBlock";
        }

        Method method = dao.getClass().getMethod(methodName, Map.class);

        Map<String, Object> newParam = new HashMap<>();
        newParam.putAll(param);
        newParam.put("gid", gid);

        return (int) method.invoke(dao, newParam);
    }

    @Override
    public List<Map<String, Object>> getAllAssetInformationInTheBlock(String blockNm) {
        PostGisDao postGisDao = postgisSession.getMapper(PostGisDao.class);

        String[] layerArray = {"wtl_fire_ps", "wtl_flow_ps", "wtl_gain_ps", "wtl_manh_ps", "wtl_meta_ps", "wtl_pipe_lm", "wtl_pres_ps", "wtl_prga_ps", "wtl_puri_as", "wtl_rsrv_ps", "wtl_serv_ps", "wtl_stpi_ps", "wtl_valv_ps"};
        List<Map<String, Object>> list = new ArrayList<>();

        for (int i = 0; i < layerArray.length; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("block_nm", blockNm);
            map.put("layer_nm", layerArray[i]);

            list.addAll(postGisDao.getAllAssetInformationInTheBlock(map));
        }

        return list;
    }

    @Override
    public int deleteTemporaryAsset(Map<String, Object> param) throws Exception {
        PostGisDao postGisDao = postgisSession.getMapper(PostGisDao.class);

        return postGisDao.deleteTemporaryAsset(param);
    }

    @Override
    @Transactional
    public int museums19(Map<String, Object> param) throws Exception {
        PostGisDao postGisDao = postgisSession.getMapper(PostGisDao.class);

        return postGisDao.museums19(param);
    }
    
    @Override
    @Transactional
    public int updatePipeLmLayers(Map<String, Object> param) throws Exception{
        PostGisDao postGisDao = postgisSession.getMapper(PostGisDao.class);
        param.put("TABLE_NM", "WTL_PIPE_LM_P1");
        param.put("N", "1");
        postGisDao.truncatePipeLmLayers(param);
        postGisDao.insertPipeLmLayers(param);
        param.put("TABLE_NM", "WTL_PIPE_LM_P2");
        param.put("N", "2");
        postGisDao.truncatePipeLmLayers(param);
        postGisDao.insertPipeLmLayers(param);
        param.put("TABLE_NM", "WTL_PIPE_LM_P3");
        param.put("N", "3");
        postGisDao.truncatePipeLmLayers(param);
        postGisDao.insertPipeLmLayers(param);
        param.put("TABLE_NM", "WTL_PIPE_LM_P4");
        param.put("N", "4");
        postGisDao.truncatePipeLmLayers(param);
        postGisDao.insertPipeLmLayers(param);
        param.put("TABLE_NM", "WTL_PIPE_LM_P5");
        param.put("N", "5");
        postGisDao.truncatePipeLmLayers(param);
        postGisDao.insertPipeLmLayers(param);        
        return 1;
    }
    
    @Override
    public int deleteGisAsset(Map<String, Object> param) throws Exception {
        PostGisDao postGisDao = postgisSession.getMapper(PostGisDao.class);

        return postGisDao.deleteGisAsset(param);
    }
    
    @Override
    public int insertGisAsset(Map<String, Object> param) throws Exception {
        PostGisDao postGisDao = postgisSession.getMapper(PostGisDao.class);

        return postGisDao.insertGisAsset(param);
    }
    
    @Override
    public int setPostgisGeoInfo(Map<String, Object> param) throws Exception {
    	PostGisDao postGisDao = postgisSession.getMapper(PostGisDao.class);
    	
    	return postGisDao.setPostgisGeoInfo(param);
    }
    
}