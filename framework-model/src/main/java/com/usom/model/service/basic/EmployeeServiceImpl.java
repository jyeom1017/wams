/**
 * 
 */
package com.usom.model.service.basic;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usom.model.dao.basic.EmployeeDao;

/**
 * @author Administrator
 *
 */
@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
	
	@Resource(name="tiberoSession")
	private SqlSession oracleSession;

	@Override
	public int updateUserPassword(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.updateUserPassword(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getEmployeeList(
			HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getEmployeeList(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getEmployeeListExcel(
			HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getEmployeeListExcel(param);
	}
	
	@Override
	public int getEmployeeTotalRowCount(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getEmployeeTotalRowCount(param);
	}

	@Override
	public int insertEmployee(HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.insertEmployee(param);
	}

	@Override
	public int deleteEmployee(HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.deleteEmployee(param);
	}

	@Override
	public boolean checkUsedEmployeeId(String user_id) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		String chkUserId = dao.checkUsedEmployeeId(user_id);
		if(chkUserId != null) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public HashMap<String, Object> getEmployee(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getEmployee(param);
	}

	@Override
	public int updateEmployeeBasicInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.updateEmployeeBasicInfo(param);
	}

	@Override
	public int deleteEmployeePhoto(HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.deleteEmployeePhoto(param);
	}

	@Override
	public int updateEmpLastMenu(HashMap<String, Object> param)throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.updateEmpLastMenu(param);
	}	
	
	@Override
	public ArrayList<HashMap<String, Object>> getEmployeeCareerInfoList(
			HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getEmployeeCareerInfoList(param);
	}

	@Override
	public int insertEmployeeCareerInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.insertEmployeeCareerInfo(param);
	}

	@Override
	public int updateEmployeeCareerInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.updateEmployeeCareerInfo(param);
	}

	@Override
	public int deleteEmployeeCareerInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.deleteEmployeeCareerInfo(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getEmployeeTechInfoList(
			HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getEmployeeTechInfoList(param);
	}

	@Override
	public int insertEmployeeTechInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.insertEmployeeTechInfo(param);
	}

	@Override
	public int updateEmployeeTechInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.updateEmployeeTechInfo(param);
	}

	@Override
	public int deleteEmployeeTechInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.deleteEmployeeTechInfo(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getEmployeeEduInfoList(
			HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getEmployeeEduInfoList(param);
	}

	@Override
	public int insertEmployeeEduInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.insertEmployeeEduInfo(param);
	}

	@Override
	public int updateEmployeeEduInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.updateEmployeeEduInfo(param);
	}

	@Override
	public int deleteEmployeeEduInfo(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.deleteEmployeeEduInfo(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getEmployeeSelectionList(
			HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getEmployeeSelectionList(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getUseEmployeeList(
			HashMap<String, Object> param) throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getUseEmployeeList(param);
	}

	@Override
	public int insertUseEmployee(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.insertUseEmployee(param);
	}

	@Override
	public int deleteUseEmployees(HashMap<String, Object> param)
			throws Exception {
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.deleteUseEmployees(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getEmployeeClass(HashMap<String, Object> param)throws Exception{
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.getEmployeeClass(param);
	}

	@Override
	public int createSSOUser(HashMap<String, Object> param) throws Exception{
		EmployeeDao dao = oracleSession.getMapper(EmployeeDao.class);
		return dao.createSSOUser(param);
	}
}
