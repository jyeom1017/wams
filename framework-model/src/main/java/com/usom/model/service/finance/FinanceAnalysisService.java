package com.usom.model.service.finance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface FinanceAnalysisService {
    // 재정계획 수립 > 재정계획 목록
    ArrayList<HashMap<String, Object>> getFinancialPlanList(HashMap<String, Object> param) throws Exception;

    // 재정계획 수립 > 재정수지분석 관리
    ArrayList<HashMap<String, Object>> getFinancialPlanDetailList(HashMap<String, Object> param) throws Exception;

    // 재정계획 수립 > 재무재표(BS) 관리
    ArrayList<HashMap<String, Object>> getFinancialStatementsList(HashMap<String, Object> param) throws Exception;

    // 재정계획 수립 > 재무재표(BS) 관리
    int setFinancialStatement(HashMap<String, Object> param) throws Exception;

    // 재정계획 수립 > 손익계산서(PL) 관리
    ArrayList<HashMap<String, Object>> getIncomeStatement(HashMap<String, Object> param) throws Exception;

    // 재정계획 수립 > 손익계산서(PL) 관리
    int setIncomeStatement(HashMap<String, Object> param) throws Exception;

    // 재정계획 수립 > 현금흐름표(CF) 관리
    ArrayList<HashMap<String, Object>> getCashFlowStatement(HashMap<String, Object> param) throws Exception;

    // 재정계획 수립 > 현금흐름표(CF) 관리
    int setCashFlowStatement(HashMap<String, Object> param) throws Exception;

    // 재정계획 수립 > 기타 비용 관리
    ArrayList<HashMap<String, Object>> getOtherCostManagement(HashMap<String, Object> param) throws Exception;

    // 재정계획 수립 > 기타비용관리 (팝업) 저장
    int setOtherCostManagement(Map<String, Object> param) throws Exception;

    // 재정계획 수립 > 재정계획 삭제
    int deleteFinancialPlan(HashMap<String, Object> param) throws Exception;

    Map<String, Object> insertTempFinancialPlan(Map<String, Object> param) throws Exception;

    List<Map<String, Object>> getOperationAndManagementExpensesPopupList(HashMap<String, Object> param) throws Exception;

    List<Map<String, Object>> getDepreciationPopupList(HashMap<String, Object> param) throws Exception;

    int setFinancialPlanDetail(HashMap<String, Object> param) throws Exception;

    int updateFinancialPlan(HashMap<String, Object> param) throws Exception;

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가 설정 (팝업)
    Map<String, Object> getUnitPriceSettingInfo(HashMap<String, Object> param) throws Exception;

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가/생산량/구입량 설정 (팝업)
    Map<String, Object> getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo(HashMap<String, Object> param) throws Exception;

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 전기 말 이자율 설정 (팝업)
    Map<String, Object> getFinancialAnalysisChargeUnitPrice() throws  Exception;

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 전기 말 이자율 설정 (팝업) 저장
    int updateInterestRateEndOfLastYearInfo(Map<String, Object> param) throws Exception;

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가, 요금단가/생산량/구입량 설정 저장
    int setFinancialAnalysisChargeUnitPrice(Map<String, Object> param) throws Exception;

    // 자산관리 계획수립 목록
    ArrayList<HashMap<String, Object>> getAssetManagementPlanList(HashMap<String, Object> param) throws Exception;

    // 자산관리 계획수립 상세 목록
    ArrayList<HashMap<String, Object>> getAssetManagementPlanDetailList(HashMap<String, Object> param) throws Exception;

    // 자산관리 계획수립 삭제
    int deleteAssetManagementPlan(HashMap<String, Object> param) throws Exception;
    
    // 자산관리 계획수립 신규
    int insertAssetManagementPlan(HashMap<String, Object> param) throws Exception;
    
    int insertAssetManagementPlanDetail(HashMap<String, Object> param) throws Exception;
    
    int updateAssetManagementPlan(HashMap<String, Object> param) throws Exception;
    
    int updateAssetManagementPlanDetail(HashMap<String, Object> param) throws Exception;
    
    HashMap<String, Object> getFinacePriceRaiseRatio(HashMap<String, Object> param) throws Exception; 
    
    int addComCodeListYr2(HashMap<String, Object> param) throws Exception ;
    
    int addLoanItemYear(HashMap<String, Object> param) throws Exception ;
    
    int initFinancialPlanDetailList(HashMap<String, Object> param) throws Exception ;
    
    ArrayList<HashMap<String, Object>> getLoanInfo(HashMap<String, Object> param) throws Exception ;
}