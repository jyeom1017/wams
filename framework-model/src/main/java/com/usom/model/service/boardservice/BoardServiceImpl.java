package com.usom.model.service.boardservice;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.io.InputStream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usom.model.config.Configuration;
import com.usom.model.vo.boardvo.BoardVO;

import net.sf.jxls.transformer.XLSTransformer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("boardService")
public class BoardServiceImpl implements BoardService {

	@Resource(name="tiberoSessionFactory")
	private SqlSessionFactory sqlSessionFactory;
	
	//@Resource(name="tiberoSession")
   // private SqlSession oracleSession;
	private Sheet s;
	//private SXSSFSheet s;
	private List columnIdList;
	
	int startRowIdx;
	int startColIdx;
	
	public Row getRow(int i){
		Row r = s.getRow(i);
		if(r==null) {
			//try {
			r = s.createRow(i);
			//}catch(Exception e) {
				//e.printStackTrace();
				//r = null;
			//}
		}
		return r;
	}
	
	public Cell getCell(int row,int cell){
		Row r = getRow(row);
		if(r==null) return null;
		
		Cell c = r.getCell(cell);
		if(c==null)
			c = r.createCell(cell);
		return c;
	}
	
	public void setCellValue(int row, int cell, String cellvalue){
		//try {
		Cell c = getCell(row,cell);
			if(c==null) return;
		c.setCellValue(cellvalue);
		//}catch(Exception e) {
			//e.printStackTrace();
		//}
	}
		
	
	@Override
	@Transactional
	public void selectExcelList(HttpServletResponse response,HashMap<String,Object> param) {
		List<String>columnNameList = (List<String>)param.get("columnNameList");
		List<String>columnWidthList = (List<String>)param.get("columnWidthList");
		
		SqlSession sqlSession = sqlSessionFactory.openSession();
		
		InputStream is  = null;
		columnIdList = (List<String>)param.get("columnIdList");
		
		startRowIdx = Integer.parseInt(param.get("startRowIdx").toString());
		startColIdx = Integer.parseInt(param.get("startColIdx").toString());
		String excelDir = Configuration.REPORT_FILE_TEMPLATE_PATH +"\\"; // + "\\new\\";

		Workbook wb = null;
		// 메모리에 100개의 행을 유지합니다. 행의 수가 넘으면 디스크에 적습니다.
		//SXSSFWorkbook wb = new SXSSFWorkbook(100);
		//s =	wb.createSheet();
		
		final XSSFCellStyle cellStyle;
		try {
			 if(param.get("templateFileName") == null || param.get("templateFileName").equals("")) {
				wb = new SXSSFWorkbook(100);
				s = wb.createSheet();
				//XSSFWorkbook wbs = new XSSFWorkbook(is);
				//wb = new SXSSFWorkbook(wbs,100);
		
				XSSFCellStyle headerStyle = (XSSFCellStyle) wb.createCellStyle();
				headerStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(242, 232, 242) ));			
				//headerStyle.setFillBackgroundColor(new XSSFColor(new java.awt.Color(128, 128, 128) ));
				headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				headerStyle.setBorderLeft(BorderStyle.THIN);
				headerStyle.setBorderTop(BorderStyle.MEDIUM);
				headerStyle.setBorderRight(BorderStyle.THIN);
				headerStyle.setBorderBottom(BorderStyle.THIN);
				
				
				int i;
				for(i=0;i<columnWidthList.size();i++) {
					s.setColumnWidth(i + startColIdx, Integer.parseInt(columnWidthList.get(i).toString()));
					//s.setDefaultColumnStyle(i + startColIdx, cellStyle);
				}
				for(i=0;i<columnNameList.size();i++) {
					setCellValue(startRowIdx -1,i + startColIdx,columnNameList.get(i).toString());
					s.getRow(startRowIdx -1).getCell(i + startColIdx).setCellStyle(headerStyle);
				}				
			 }else {
				is = new BufferedInputStream(new FileInputStream(excelDir + File.separator + param.get("templateFileName").toString() + ".xlsx"));
				//wb = new XSSFWorkbook(excelDir + File.separator + param.get("templateFileName").toString() + ".xlsx");
				//s = wb.getSheetAt(0);
	            XLSTransformer xls = new XLSTransformer();
	            HashMap<String,Object> map = new HashMap<String,Object>();
	            //HashMap<String,Object> map = (HashMap) param.get("searchOptions");
	            map.putAll((Map) param.get("searchOptions"));
	            //map.putAll(param.get("searchOptions"));
	            //map.put("EST_NM", "test");
	            //map.put("E_NAME", "test1");
	            //map.put("INSERT_DT", "test2");
           		//map.put("EST_YYYYMM", "test3");
	            XSSFWorkbook workBook = (XSSFWorkbook) xls.transformXLS(is,map);
				//XSSFWorkbook workBook = (XSSFWorkbook)org.apache.poi.ss.usermodel.WorkbookFactory.create(is);
	            
				Row row = null;
				int lastRowNum = workBook.getSheetAt(0).getLastRowNum();
				int i = 0;
				for(i=startRowIdx-1;i<=lastRowNum;i++) {
				row = workBook.getSheetAt(0).getRow(i);
				if(row!=null) workBook.getSheetAt(0).removeRow(row);
				}
				//workBook.wait(0);
				wb = new SXSSFWorkbook(workBook,100);
				//s = wb.createSheet();
				s = wb.getSheetAt(0);
				startRowIdx = startRowIdx -1;
			 }
			 
				cellStyle = (XSSFCellStyle) wb.createCellStyle();
				cellStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 252, 252) ));
				cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				cellStyle.setBorderLeft(BorderStyle.THIN);
				cellStyle.setBorderTop(BorderStyle.THIN);
				cellStyle.setBorderRight(BorderStyle.THIN);
				cellStyle.setBorderBottom(BorderStyle.THIN);
				
			 /*
			//XSSFWorkbook wbs = new XSSFWorkbook(is);
			//wb = new SXSSFWorkbook(wbs,100);
	
			XSSFCellStyle headerStyle = (XSSFCellStyle) wb.createCellStyle();
			headerStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(242, 232, 242) ));			
			//headerStyle.setFillBackgroundColor(new XSSFColor(new java.awt.Color(128, 128, 128) ));
			headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			headerStyle.setBorderLeft(BorderStyle.THIN);
			headerStyle.setBorderTop(BorderStyle.MEDIUM);
			headerStyle.setBorderRight(BorderStyle.THIN);
			headerStyle.setBorderBottom(BorderStyle.THIN);
			
			final XSSFCellStyle cellStyle = (XSSFCellStyle) wb.createCellStyle();
			cellStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 252, 252) ));
			cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cellStyle.setBorderLeft(BorderStyle.THIN);
			cellStyle.setBorderTop(BorderStyle.THIN);
			cellStyle.setBorderRight(BorderStyle.THIN);
			cellStyle.setBorderBottom(BorderStyle.THIN);

			
			int i;
			for(i=0;i<columnWidthList.size();i++) {
				s.setColumnWidth(i + startColIdx, Integer.parseInt(columnWidthList.get(i).toString()));
				//s.setDefaultColumnStyle(i + startColIdx, cellStyle);
			}
			for(i=0;i<columnNameList.size();i++) {
				setCellValue(startRowIdx -1,i + startColIdx,columnNameList.get(i).toString());
				s.getRow(startRowIdx -1).getCell(i + startColIdx).setCellStyle(headerStyle);
			}
			//s.getRow(0).setRowStyle(headerStyle);
			*/
			
		    /*sqlSession.select("selectExcelList", param, new ResultHandler<BoardVO>() {
				@Override
				public void handleResult(ResultContext<? extends BoardVO> context) { 
					BoardVO vo = context.getResultObject();
					//System.out.println(vo.getAssetCd());
					//System.out.println(context.getResultCount());
					int rowIdx =  context.getResultCount();
					setCellValue(rowIdx,0,vo.getAssetCd());

			    }
            });*/
			sqlSession.select(param.get("selectName").toString(), param.get("searchOptions"), new ResultHandler<HashMap<String,Object>>() {
				@Override
				public void handleResult(ResultContext<? extends HashMap<String,Object>> context) { 
					HashMap<String,Object> vo = context.getResultObject();
					int rowIdx =  context.getResultCount() -1;
					int i = 0;
					
					for(i=0;i<columnIdList.size();i++) {
						if(vo.get(columnIdList.get(i).toString())!=null) {
							setCellValue(rowIdx + startRowIdx,i + startColIdx,vo.get(columnIdList.get(i).toString()).toString());
						}else {
							setCellValue(rowIdx + startRowIdx,i + startColIdx,"");	
						}
						s.getRow(rowIdx + startRowIdx).getCell(i + startColIdx).setCellStyle(cellStyle);
					}
					
			    }
            });			
			
			String MY_ENCODING = "MS949";
			String fileName = param.get("outputFileName").toString();
			String header = "attachment; filename=" + fileName + ";";
			if(!fileName.equals(new String(fileName.getBytes(MY_ENCODING),MY_ENCODING))) {
				header = "=?UTF-8?Q?attachment; filename=" + java.net.URLEncoder.encode(fileName,"UTF-8") + ";?=";
				response.setHeader("Content-Disposition",new String(header.getBytes(MY_ENCODING),"8859_1"));
			}else {
				response.setHeader("Content-Disposition", header);
			}
			wb.write(response.getOutputStream());
			/*
			String filename = URLDecoder.decode(param.get("outputFileName").toString(), "UTF8");   
			String encoded_String = new String(filename.getBytes("UTF-8"), StandardCharsets.ISO_8859_1);
			
			response.setHeader("Set-Cookie", "fileDownload=true; path=/");
			response.setHeader("Content-Disposition", String.format("attachment; filename=\""+ encoded_String +"\""));
			wb.write(response.getOutputStream());
			*/
		} catch(Exception e) {
			response.setHeader("Set-Cookie", "fileDownload=false; path=/");
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			response.setHeader("Content-Type","text/html; charset=utf-8");
			
			OutputStream out = null;
			try {
				out = response.getOutputStream();
				byte[] data = new String("fail..").getBytes();
				out.write(data, 0, data.length);
			} catch(Exception ignore) {
				ignore.printStackTrace();
			} finally {
				if(out != null) try { out.close(); } catch(Exception ignore) {}
			}
			
		} finally {
			sqlSession.close();

			if(param.get("templateFileName") == null) {
			// 디스크 적었던 임시파일을 제거합니다.
			((SXSSFWorkbook)wb).dispose();
			}
			
			try { wb.close(); } catch(Exception ignore) {}
		}
		
	}
}