package com.usom.model.service.finance;

import java.util.ArrayList;
import java.util.HashMap;

public interface OipService {
	HashMap<String, Object> getDSCNTList() throws Exception;

	int saveDSCNT(HashMap<String, Object> param) throws Exception;

	ArrayList<HashMap<String, Object>> getList(HashMap<String, Object> param)throws Exception;

	HashMap<String, Object> newList(HashMap<String, Object> param) throws Exception;

	ArrayList<HashMap<String, Object>> getOIP_OPTIMAL(HashMap<String, Object> param) throws Exception;

	ArrayList<HashMap<String, Object>> getOIP_RPN(HashMap<String, Object> param) throws Exception;

	int applyOipAsset(HashMap<String, Object> param) throws Exception;

	/*int applyOipAssetDT(HashMap<String, Object> param) throws Exception;*/

	int applyOipAssetCost(HashMap<String, Object> param) throws Exception;

	HashMap<String, Object> getORDMAssetList(HashMap<String, Object> param) throws Exception;

	int updateORDMAssetList(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getFN_CD(HashMap<String, Object> param) throws Exception;

	ArrayList<HashMap<String, Object>> getORDM(HashMap<String, Object> param) throws Exception;

	int saveOipAsset(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getOIP_OPTIMAL_NEW(HashMap<String, Object> param) throws Exception;

	int deleteOIP(HashMap<String, Object> param) throws Exception;

	ArrayList<HashMap<String, Object>> report(HashMap<String, Object> param) throws Exception;

	HashMap<String, Object> insertOIP(HashMap<String, Object> param)throws Exception;

	int analysisOIP(HashMap<String, Object> param)throws Exception;

}