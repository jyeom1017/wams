package com.usom.model.service.operation;

import java.util.ArrayList;
import java.util.HashMap;

public interface New_OperationService {

	HashMap<String, Object> insertREPAIR(HashMap<String, Object> param) throws Exception;

	int saveREPAIR(HashMap<String, Object> param)throws Exception;

	int insertREPAIR_FILE(HashMap<String, Object> param)throws Exception;

	HashMap<String, Object> getREPAIR_MODIFIED(HashMap<String, Object> param)throws Exception;

	int deleteREPAIR_FILE(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getREPAIR(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getREPAIR_ASSET(HashMap<String, Object> param) throws Exception;

	ArrayList<HashMap<String, Object>> getREPAIR_FILE(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getFormatTYPE(HashMap<String, Object> param)throws Exception;

	int updateREPAIR_DEL_YN(HashMap<String, Object> reqDataParsingToHashMap)throws Exception;

	int updateREPAIR_FILE(HashMap<String, Object> param)throws Exception;
	
	//조사 및 탐사
	HashMap<String, Object> insertINSPECT(HashMap<String, Object> param) throws Exception;

	int saveINSPECT(HashMap<String, Object> param)throws Exception;

	int insertINSPECT_FILE(HashMap<String, Object> param)throws Exception;

	HashMap<String, Object> getINSPECT_MODIFIED(HashMap<String, Object> param)throws Exception;

	int deleteINSPECT_FILE(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getINSPECT(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getINSPECT_ASSET(HashMap<String, Object> param) throws Exception;

	ArrayList<HashMap<String, Object>> getINSPECT_FILE(HashMap<String, Object> param)throws Exception;

	int updateINSPECT_DEL_YN(HashMap<String, Object> reqDataParsingToHashMap)throws Exception;

	int updateINSPECT_FILE(HashMap<String, Object> param)throws Exception;

  
}
