package com.usom.model.service.estimation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface EstimationService {

    HashMap<String, Object> getList(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getListExcel(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getReportExcel(HashMap<String, Object> param);

    HashMap<String, Object> getDetail(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getIndirEstAssetGroup(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getIndirEstAssetList(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getDirEstAssetGroup(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getDirEstAssetList(HashMap<String, Object> param) throws Exception;

    List<Map<String, Object>> getEstAssetNonePage(Map<String, Object> param);

    HashMap<String, Object> getEstAssetPage(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getIndirEstAssetResultList(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getDirEstAssetResultList(HashMap<String, Object> param) throws Exception;
    
    int insert(HashMap<String, Object> param) throws Exception;

    int update(HashMap<String, Object> param) throws Exception;
    
    int delete(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> historyList(HashMap<String, Object> param) throws Exception;
    
    int insertIndirEstAsset(HashMap<String, Object> param) throws Exception;

    int deleteIndirEstAsset(HashMap<String, Object> param) throws Exception;

    int saveIndirEstAsset(HashMap<String, Object> param) throws Exception;
    
    int saveDirEstAsset(HashMap<String, Object> param) throws Exception;
    
    int saveDirEstAssetResult(HashMap<String, Object> param) throws Exception;
    
    int saveDirEstAssetResult2(HashMap<String, Object> param) throws Exception; 
    
    int deleteEstAsset(HashMap<String, Object> param) throws Exception;
    
    int saveIndirEstAssetResult(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getFactorListExcel(HashMap<String, Object> param);
    
    HashMap<String, Object> getFactorList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getFactorDetail(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getFactorDetailExcel(HashMap<String, Object> param);
    
    int insertFactor(HashMap<String, Object> param) throws Exception;

    int updateFactor(HashMap<String, Object> param) throws Exception;
    
    int deleteFactor(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> historyFactorList(HashMap<String, Object> param) throws Exception;    
    
    ArrayList<HashMap<String, Object>> getGradeListExcel(HashMap<String, Object> param);

    HashMap<String, Object> getGradeList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getGradeDetail(HashMap<String, Object> param);
    
    int insertGrade(HashMap<String, Object> param) throws Exception;

    int updateGrade(HashMap<String, Object> param) throws Exception;
    
    int deleteGrade(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> historyGradeList(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getRiskList(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getRiskAssetGroup(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getRiskAssetList(HashMap<String, Object> param) throws Exception;

    int deleteRisk(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getFactorRiskList(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getRiskExcelList(HashMap<String, Object> param) throws Exception;
    List<HashMap<String, Object>> getRiskExcelList1(HashMap<String, Object> param) throws Exception;
    
    int insertRiskFactor(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getGradeRiskList(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getFactorRiskDetail(HashMap<String, Object> param);
    
    //통계분석
    ArrayList<HashMap<String, Object>> getStatIndirEst(HashMap<String, Object> param);
    ArrayList<HashMap<String, Object>> getStatRemainLife(HashMap<String, Object> param);
    ArrayList<HashMap<String, Object>> getStatRisk(HashMap<String, Object> param);
    ArrayList<HashMap<String, Object>> getStatLoS(HashMap<String, Object> param);
    ArrayList<HashMap<String, Object>> getStatLcc(HashMap<String, Object> param);
    ArrayList<HashMap<String, Object>> getStatOIP(HashMap<String, Object> param);
    ArrayList<HashMap<String, Object>> getStatFinance(HashMap<String, Object> param);

    List<Map<String, Object>> getGISInformationAndConditionAssessmentResults(Map<String, Object> param);
}
