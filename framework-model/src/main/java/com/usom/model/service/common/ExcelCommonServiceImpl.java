package com.usom.model.service.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.Resource;

import org.apache.commons.lang.ArrayUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usom.model.dao.common.ExcelCommonDao;

import net.sf.json.JSONObject;
import net.sf.json.JSONArray;


@Service("excelCommonService")
public class ExcelCommonServiceImpl implements ExcelCommonService {
	
	

	@Resource(name="tiberoSession")
	private SqlSession tiberoSession;
	
	@Override
	public ArrayList<HashMap<String, Object>> getList(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getList(param);
	}
	
	@Override
	public HashMap<String, Object> getInfo(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getInfo(param);
	}		
	
	@Override
	public int getInt(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getInt(param);
	}
	
	@Override
	public String getString(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getString(param);
	}	
	
	@Override
	public ArrayList<HashMap<String, Object>> getList_010203(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getList_010203(param);
	}	
	
	@Override
	public HashMap<String, Object> getInfo_010203(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getInfo_010203(param);
	}		
	
	@Override
	public ArrayList<HashMap<String, Object>> getPic_010203(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getPic_010203(param);
	}		
	
	@Override
	public ArrayList<HashMap<String, Object>> getList_010202(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getList_010202(param);
	}	
	
	@Override
	public HashMap<String, Object> getInfo_010202(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getInfo_010202(param);
	}		
	
	@Override
	public ArrayList<HashMap<String, Object>> getPic_010202(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getPic_010202(param);
	}	
	
	@Override
	public ArrayList<HashMap<String, Object>> getList1_010102(HashMap<String, Object> param)throws Exception{
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getList1_010102(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getList2_010102(HashMap<String, Object> param)throws Exception{
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getList2_010102(param);
	}
	
	@Override
	public HashMap<String, Object> getInfo_010102(HashMap<String, Object> param)throws Exception{
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getInfo_010102(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getPic_010102(HashMap<String, Object> param) throws Exception{
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getPic_010102(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getList_010205(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getList_010205(param);
	}	
	
	@Override
	public HashMap<String, Object> getInfo_010205(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getInfo_010205(param);
	}		
	
	@Override
	public ArrayList<HashMap<String, Object>> getPic_010205(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getPic_010205(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> getBBSList(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		
		ArrayList<HashMap<String, Object>> list = null;
		String type = (String) param.get("type");
		
		
		if("1".equals(type)) {
			list = dao.getBBSList_Type1(param);
		}else if("2".equals(type)) {
			list = dao.getBBSList_Type2(param);
		}else if("3".equals(type)) {
			list = dao.getBBSList_Type3(param);
		}
		
		return list;
	}		
	
	@Override
	public HashMap<String, Object> getList1_010306(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("info", (HashMap<String, Object>)dao.getInfo_010306(param));
		result.put("list1", (ArrayList<HashMap<String, Object>>)dao.getList_010306(param));
		
		return result;
	}
	
	@Override
	public HashMap<String, Object> getList2_010306(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("info", (HashMap<String, Object>)dao.getInfo2_010306(param));
		result.put("list1", (ArrayList<HashMap<String, Object>>)dao.getList2_010306(param));
		
		return result;
	}
	
	public HashMap<String, Object> getList1_010306_2(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		param.put("FN_CD", "F");
		result.put("flist", dao.getFN_010306_2(param));
		result.put("ftotal", dao.getFN_TOTAL_010306_2(param));
		
		param.put("FN_CD", "N");
		result.put("nlist", dao.getFN_010306_2(param));
		result.put("ntotal", dao.getFN_TOTAL_010306_2(param));
		
		result.put("ttotal", dao.getASSET_TOTAL_010306_2(param));
		
		return result;
	}
	
	public ArrayList<HashMap<String, Object>> getBig_010306(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		
		return dao.getASSET_010306_2(param);
	}
	
	@Override
	public HashMap<String, Object> getList1_010402(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("info1", dao.getList1_010402_0(param));//info
		
		String[] key1 = dao.getList1_010402_1_KEY(param);
		param.put("YYYY_KEY", arrayToStringWithSingleQuotes(key1));
		result.put("year1", StringToarray(key1));//재정수지분석 년도 30년치 
		result.put("list1", dao.getList1_010402_1(param));//재정수지분석 리스트
		
		
		String[] key2 = dao.getList1_010402_2_KEY(param);
		param.put("YYYY_KEY2", arrayToStringWithSingleQuotes(key2));
		result.put("year2", StringToarray(key2));//나머지 년도 5년치
		
		result.put("list2", dao.getList1_010402_2(param));//재무제표(BS)
		result.put("list3", dao.getList1_010402_3(param));//손익계산서(PL)
		result.put("list4", dao.getList1_010402_4(param));//현금흐름표(CF)
		
		return result;
	}
	
	
    @Override
	public HashMap<String, Object> getList1_010305(HashMap<String, Object> param) throws Exception {
    	ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
    	HashMap<String, Object> result = new HashMap<String, Object>();
    	
    	result.put("Info", dao.getInfo_010305(param));//info
    	
    	result.put("Array", dao.getList1_010305(param));
		return result;
	}

	private String arrayToStringWithSingleQuotes(String[] array) {
        if (ArrayUtils.isNotEmpty(array)) {
            StringBuilder sb = new StringBuilder("'");
            int idx = 1;
            for (String str : array) {
                sb.append(str + "' AS C_" + idx + ",'");
                idx++;
            }

            return sb.delete(sb.lastIndexOf(","), sb.length()).toString();
        }

        return "''";
    }
    private ArrayList<HashMap<String, Object>> StringToarray(String[] array) {
    	ArrayList<HashMap<String, Object>> arrayList = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(array)) 
        {
            for (String str : array) {
            	HashMap<String, Object> temp = new HashMap<String, Object>();
            	temp.put("C_1", str);
        		arrayList.add(temp);
            }
            return arrayList;
        }

        return null;
    }
    
	// TODO 큰 데이터 페이징 처리 후 가져오는 곳
	@Override
	public ArrayList<HashMap<String, Object>> getBig_010303(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getBig_010303(param);
	}

	@Override
	public HashMap<String, Object> getBig_Info_010303(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getBig_Info_010303(param);
	}

	@Override
	public HashMap<String, Object> getBig_Info_010307(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getBig_Info_010307(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getBig_010307(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getBig_010307(param);
	}

	@Override
	public HashMap<String, Object> getBig_Info_010401(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getBig_Info_010401(param);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getBig_010401(HashMap<String, Object> param) throws Exception {
		ExcelCommonDao dao = tiberoSession.getMapper(ExcelCommonDao.class);
		return dao.getBig_010401(param);
	}
    
}
