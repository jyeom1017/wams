package com.usom.model.service.diagnosis;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.model.util.CommonUtil;
import com.usom.model.dao.diagnosis.LifeDao;
import com.usom.model.dao.diagnosis.ReevalDao;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("reevalService")
public class ReevalServiceimpl implements ReevalService {
	
	@Resource(name="tiberoSession")
	private SqlSession oracleSession;
	
	@Override
	public ArrayList<HashMap<String, Object>> selectReEst(HashMap<String, Object> param) throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
	    return dao.getRE_EST(param);
    }

	@Override
	public ArrayList<HashMap<String, Object>> selectReEstAssetGroup(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		return dao.getRE_EST_ASSET_GROUP(param);
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> selectReEstAsset(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		return dao.getRE_EST_ASSET(param);
	}
	
	@Override
	public HashMap<String, Object> insertReEst(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		dao.insertRE_EST(param);
		return param;
	}
	@Override
	public int deleteReEst(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		int count = -1;
		count = dao.deleteRE_EST_ASSET(param);
		count += dao.deleteRE_EST(param);
		return count;
	}
	
	@Override
	public int delete2ReEst(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		return dao.updateRE_EST(param);
	}
	@Override
	public int update(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		int count = -1;
		count = dao.updateRE_EST(param);
		count += dao.updateRE_EST_ASSET(param);
		return count;
	}
	
	@Override
	public int insertReEstAsset(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		int count = 0;
		count = dao.deleteRE_EST_ASSET(param);
		count += dao.insertRE_EST_ASSET_import(param); 
		return count;
	}
	
	@Override
	public int deleteReEstAsset(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		return dao.deleteRE_EST_ASSET(param);
	}
	
	@Override
	public HashMap<String, Object> selectPriceIndex()throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		 HashMap<String, Object> map = new HashMap<String, Object>();
		 map.put("array", dao.getPRICE_INDEX());
		 map.put("basic", dao.getPRICE_INDEX_BASIC());
	    return map; 
	}
	@Override
	public int insertPriceIndex(HashMap<String, Object> param)throws Exception {
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		String ID = (String)param.get("ID");
		JSONArray List = (JSONArray) param.get("List");
	    JSONObject Info = (JSONObject) param.get("Info");
	    
	    HashMap<String, Object> map = new HashMap<String, Object>();
		map = CommonUtil.convertJsonToMap(Info);
		map.put("UPDATE_ID", ID);
		
		int count = -1;
		count = dao.updatePRICE_INDEX_BASIC(map);
		count += dao.deletePRICE_INDEX();
		String[] mm = {"01", "02", "03","04", "05", "06","07", "08", "09","10", "11", "12"};
		/*let list = []
		let arr = [];
		//리스트를 array로 전환
		for(i in this.List){
			let yyyymm = this.List[i]
			for(j in arr)
			list.push({YYYYMM : yyyymm['YYYY'] + arr[j], PRICE_INDEX : yyyymm['M'+arr[j]]})
		}*/
		
		HashMap<String, Object> item = new HashMap<String, Object>();
		 for (int i = 0; i < List.size(); i++) {
			   	map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
			   	System.out.println(map);
			   	for(int j=0; j < mm.length; j++) {
			   		item.put("YYYYMM", map.get("YYYY").toString() + mm[j]);
			   		item.put("PRICE_INDEX", map.get("M" + mm[j]));
			   		item.put("INSERT_ID", ID);
			   		System.out.println(i+"번"+item);
			   		count += dao.insertPRICE_INDEX(item);
			   	}
		}
		 
		return count;
	}
	@Override
	public HashMap<String, Object> getWeightSpec(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		 HashMap<String, Object> map = new HashMap<String, Object>();
		 map.put("WeightList", dao.getWEIGHT_SPEC());
		 ArrayList<HashMap<String, Object>> array = new ArrayList<HashMap<String, Object>>();
		 int index = (int)param.get("index");
		 array = (ArrayList<HashMap<String, Object>>)map.get("WeightList");
		 map.put("Info", array.get(index));
		 map.put("List", dao.getWEIGHT_SPEC_ITEM(array.get(index)));
		 
	    return map; 
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> report(HashMap<String, Object> param) throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		return dao.getRE_EST_ASSET_REPORT(param);
	}
	@Override
	public ArrayList<HashMap<String, Object>> getWeightSpecItem(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		return dao.getWEIGHT_SPEC_ITEM(param); 
	}
	
	@Override
	public int insertWeightSpec(HashMap<String, Object> param)throws Exception{
		ReevalDao dao = oracleSession.getMapper(ReevalDao.class);
		
		JSONArray List = (JSONArray) param.get("List");
	    JSONObject Info = (JSONObject) param.get("Info");
	    
	    HashMap<String, Object> map = new HashMap<String, Object>();
  		map = CommonUtil.convertJsonToMap(Info);
  		int count = -1;
  		count = dao.insertWEIGHT_SPEC(map);
	    
	    int WEIGHT_SPEC_SID = Integer.parseInt(String.valueOf(map.get("WEIGHT_SPEC_SID")));
	    String ID = "";
	    String DT = "";
	    if(map.get("UPDATE_ID") == null) {
	    	ID = (String)map.get("INSERT_ID");
	    }
	    else {
	    	ID = (String)map.get("UPDATE_ID");
	    }
	  
	    for (int i = 0; i < List.size(); i++) {
		   	map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
		   	map.put("WEIGHT_SPEC_SID",WEIGHT_SPEC_SID);
		   	map.put("ID", ID);
		   	map.put("ITEM_SID", i);
		   	System.out.println("2"+map);
		   	count += dao.insertWEIGHT_SPEC_ITEM(map);
	    }
	    
		return count;
	}
	
	/*reevalService
	   selectReEst
	   insertReEst
	   updateReEst
	   DeleteReEst
	   getNextReEstSid
	   insertReEstAsset
	   deleteReEstAsset
	   selectReEstAsset
	   
	   selectPriceIndex
	   insertPriceIndex
	   updatePriceIndex
	   deletePriceIndex
	   
	   selectWeightSpec
	   insertWeightSpec
	   updateWeightSpec
	   deleteWeightSpec
	   
	   insertWeightSpecItem
	   deleteWeightSpecItem
	   selectWeightSpecItem
*/
}