package com.usom.model.service.operation;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.dao.operation.New_OperationDao;
import com.usom.model.dao.operation.OperationDao;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("new_operationService")
public class New_OperationServiceImpl implements New_OperationService {
	
	@Resource(name="tiberoSession")
	private SqlSession oracleSession;
	
    @Resource(name = "pagging")
    private Pagging pagging;
    
    //list
    @Override
    public ArrayList<HashMap<String, Object>> getREPAIR(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.getREPAIR(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getREPAIR_ASSET(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.getREPAIR_ASSET(param);
    }
    
    @Override
    public HashMap<String, Object> insertREPAIR(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
    	dao.insertREPAIR(param);
		HashMap<String, Object> map = new HashMap<String, Object>();
    	map.put("List", dao.getREPAIR_ASSET_NM(param));
    	map.put("FileList", dao.getREPAIR_FILE(param));
    	param.put("POPUP_SID", param.get("REPAIR_SID"));
		param.remove("REPAIR_SID");
    	map.put("Info", param);
    	return map;
    }
    
    @Override
    public int saveREPAIR(HashMap<String, Object> param)throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        
    	JSONObject Info = (JSONObject) param.get("Info");
        JSONArray FileList = (JSONArray) param.get("FileList");
        JSONArray List = (JSONArray) param.get("List");
        
        HashMap<String, Object> map = new HashMap<>();
		map = CommonUtil.convertJsonToMap(Info);
		int SID = (int)map.get("POPUP_SID");
		map.put("REPAIR_SID", SID);
		map.put("REPAIR_NM", map.get("POPUP_NM"));
		map.put("REPAIR_START_DT", map.get("START_DT"));
		map.put("REPAIR_END_DT", map.get("END_DT"));
		map.put("TEMP_YN", "N");
		map.put("DEL_YN", "N");
		String INSERT_ID = (String) map.get("INSERT_ID");
	    String ID = (String) param.get("ID");
	    int count = 0;
	    count += dao.updateREPAIR(map);
	    
	    dao.deleteREPAIR_FILE(map);
	    
        for (int i = 0; i < FileList.size(); i++) {
        	map = CommonUtil.convertJsonToMap(FileList.getJSONObject(i));
    	    map.put("INSERT_ID", INSERT_ID);
            map.put("UPDATE_ID", ID);
            map.put("REPAIR_SID", SID);
            //널 대신
            map.put("PHOTO_NUM", 999);
            map.put("ASSET_SID", 0);
            count += dao.insertREPAIR_FILE(map);
        }
        
	    count += dao.deleteREPAIR_ASSET(map);
        for (int i = 0; i < List.size(); i++) {
        	map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
        	if(map.get("ASSET_CD") == null)continue;
    	    map.put("INSERT_ID", INSERT_ID);
            map.put("UPDATE_ID", ID);
            map.put("REPAIR_SID", SID);
            count += dao.insertREPAIR_ASSET(map);
        }
        
        return count;
    }
    
    @Override
    public int insertREPAIR_FILE(HashMap<String, Object> param) throws Exception {
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.insertREPAIR_FILE(param);
    }
    
    @Override
    public HashMap<String, Object> getREPAIR_MODIFIED(HashMap<String, Object> param)throws Exception {
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
    	HashMap<String, Object> map = new HashMap<String, Object>();
    	map.put("Info", dao.getREPAIR_INFO_NM(param));
    	map.put("List", dao.getREPAIR_ASSET_NM(param));
    	map.put("FileList", dao.getREPAIR_FILE(param));
        return map;
    }
    
    @Override
    public int deleteREPAIR_FILE(HashMap<String, Object> param)throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
    	if(param.get("SID")!= null)
    		param.put("REPAIR_SID", param.get("SID")); 
    	return dao.deleteREPAIR_FILE(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getREPAIR_FILE(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.getREPAIR_FILE(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>>  getFormatTYPE(HashMap<String, Object> param)throws Exception {
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.getFormatTYPE();
    }
    
    @Override
	 public int updateREPAIR_DEL_YN(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
      return dao.updateREPAIR_DEL_YN(param);
	}
    
    @Override
    public int updateREPAIR_FILE(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.updateREPAIR_FILE(param);
    }
    //조사 및 탐사
    @Override
    public ArrayList<HashMap<String, Object>> getINSPECT(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.getINSPECT(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getINSPECT_ASSET(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.getINSPECT_ASSET(param);
    }
    
    @Override
    public HashMap<String, Object> insertINSPECT(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
    	dao.insertINSPECT(param);
		HashMap<String, Object> map = new HashMap<String, Object>();
    	map.put("List", dao.getINSPECT_ASSET_NM(param));
    	map.put("FileList", dao.getINSPECT_FILE(param));
    	param.put("POPUP_SID", param.get("INSPECT_SID"));
		param.remove("INSPECT_SID");
    	map.put("Info", param);
    	return map;
    }
    
    @Override
    public int saveINSPECT(HashMap<String, Object> param)throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        
    	JSONObject Info = (JSONObject) param.get("Info");
        JSONArray FileList = (JSONArray) param.get("FileList");
        JSONArray List = (JSONArray) param.get("List");
        
        HashMap<String, Object> map = new HashMap<>();
		map = CommonUtil.convertJsonToMap(Info);
		int SID = (int)map.get("POPUP_SID");
		map.put("INSPECT_SID", SID);
		map.put("INSPECT_NM", map.get("POPUP_NM"));
		map.put("INSPECT_START_DT", map.get("START_DT"));
		map.put("INSPECT_END_DT", map.get("END_DT"));
		map.put("TEMP_YN", "N");
		map.put("DEL_YN", "N");
		String INSERT_ID = (String) map.get("INSERT_ID");
	    String ID = (String) param.get("ID");
	    int count = 0;
	    count += dao.updateINSPECT(map);
	    
	    dao.deleteINSPECT_FILE(map);
	    
        for (int i = 0; i < FileList.size(); i++) {
        	map = CommonUtil.convertJsonToMap(FileList.getJSONObject(i));
    	    map.put("INSERT_ID", INSERT_ID);
            map.put("UPDATE_ID", ID);
            map.put("INSPECT_SID", SID);
            
            //널 대신
            map.put("PHOTO_NUM", 999);
            map.put("ASSET_SID", 0);
            count += dao.insertINSPECT_FILE(map);
        }
        
	    count += dao.deleteINSPECT_ASSET(map);
        for (int i = 0; i < List.size(); i++) {
        	map = CommonUtil.convertJsonToMap(List.getJSONObject(i));
        	if(map.get("ASSET_CD") == null)continue;
    	    map.put("INSERT_ID", INSERT_ID);
            map.put("UPDATE_ID", ID);
            map.put("INSPECT_SID", SID);
            count += dao.insertINSPECT_ASSET(map);
        }
        
        return count;
    }
    
    @Override
    public int insertINSPECT_FILE(HashMap<String, Object> param) throws Exception {
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.insertINSPECT_FILE(param);
    }
    
    @Override
    public HashMap<String, Object> getINSPECT_MODIFIED(HashMap<String, Object> param)throws Exception {
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
    	HashMap<String, Object> map = new HashMap<String, Object>();
    	map.put("Info", dao.getINSPECT_INFO_NM(param));
    	map.put("List", dao.getINSPECT_ASSET_NM(param));
    	map.put("FileList", dao.getINSPECT_FILE(param));
        return map;
    }
    
    @Override
    public int deleteINSPECT_FILE(HashMap<String, Object> param)throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
    	if(param.get("SID")!= null)
    		param.put("INSPECT_SID", param.get("SID")); 
    	return dao.deleteINSPECT_FILE(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getINSPECT_FILE(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.getINSPECT_FILE(param);
    }
    
    @Override
	 public int updateINSPECT_DEL_YN(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
      return dao.updateINSPECT_DEL_YN(param);
	}
    
    @Override
    public int updateINSPECT_FILE(HashMap<String, Object> param) throws Exception{
    	New_OperationDao dao = oracleSession.getMapper(New_OperationDao.class);
        return dao.updateINSPECT_FILE(param);
    }
}