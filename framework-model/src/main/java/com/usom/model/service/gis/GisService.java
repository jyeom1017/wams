package com.usom.model.service.gis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface GisService {

	List<Map<String, Object>> getPipeInfoListFromFtrIdn(HashMap<String, Object> param) throws Exception;
	
    Map<String, Object> getAssetInfoWithGisInfo(HashMap<String, Object> param) throws Exception;

    Map<String, Object> getAssetInfoWithLayer(HashMap<String, Object> param) throws Exception;

    int insertAssetGis(Map<String, Object> param) throws Exception;

    List<Map<String, Object>> getAssetInfoWithGisList(List<Map<String, Object>> param) throws Exception;

    List<Map<String, Object>> getGISInfoByAssetPath(Map<String, Object> param);

    Map<String, Object> getDiagnosticAreaList(HashMap<String, Object> param) throws Exception;

    int deleteDiagnosticArea(HashMap<String, Object> param) throws Exception;

    Map<String, Object> getDiagnosticAreaDetailNewList(HashMap<String, Object> param) throws Exception;

    Map<String, Object> getDiagnosticAreaDetailEditList(HashMap<String, Object> param) throws Exception;

    int saveDiagnosticAreaDetail(HashMap<String, Object> param) throws Exception;

    int deleteDiagnosticAreaAsset(HashMap<String, Object> param) throws Exception;

    Map<String, Object> getStateDiagnosisGroupList(HashMap<String, Object> param) throws Exception;

    int deleteStateDiagnosisGroup(HashMap<String, Object> param) throws Exception;

    Map<String, Object> getStateDiagnosticGroupDetailPopupInfo(Map<String, Object> param);

    Map<String, Object> getStateDiagnosisGroupDetailNewList(HashMap<String, Object> param) throws Exception;

    Map<String, Object> getStateDiagnosisGroupDetailEditList(HashMap<String, Object> param) throws Exception;

    int saveStateDiagnosisGroup(HashMap<String, Object> param) throws Exception;

    List<Map<String, Object>> getAssetsByAssetPath(Map<String, Object> param);

    List<Map<String, Object>> getAssetsByGISProperty(Map<String, Object> param);

    List<Map<String, Object>> getAssetByAssetCode(Map<String, Object> param);


    String getGoogleMapApi(HashMap<String, Object> param) throws Exception;
    
    String getVWorldApi(HashMap<String, Object> param) throws Exception;

    int updateAssetInfoItem(List<Map<String, Object>> param, String blockNm) throws Exception;

    int under42(Map<String, Object> param) throws Exception;
}