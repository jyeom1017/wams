package com.usom.model.service.asset;

import java.util.ArrayList;
import java.util.HashMap;

public interface AssetService {

    HashMap<String, Object> getAssetCriteria() throws Exception;

    HashMap<String, Object> getLevelCodeList(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getLevelCodeListExcel(HashMap<String, Object> param) throws Exception;

    int updateLevelCode(HashMap<String, Object> param) throws Exception;

    int insertLevelCode(HashMap<String, Object> param) throws Exception;

    int deleteLevelCode(HashMap<String, Object> param) throws Exception;

    int isAssetClassCdExists(HashMap<String, Object> param) throws Exception;

    int getTotalCount() throws Exception;

    int isLevelCodeUsed(HashMap<String, Object> param) throws Exception;

    // 자산 기준정보관리
    HashMap<String, Object> getAssetBaseInfoList(HashMap<String, Object> param) throws Exception;

    HashMap<String, Object> getAssetBaseInfoDetail(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getAssetBaseInfoExcel(HashMap<String, Object> param) throws Exception;

    int updateAssetBaseInfo(HashMap<String, Object> param) throws Exception;

    int insertAssetBaseInfo(HashMap<String, Object> param) throws Exception;

    int deleteAssetBaseInfo(HashMap<String, Object> param) throws Exception;
}