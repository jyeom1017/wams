package com.usom.model.service.test;

import java.util.ArrayList;
import java.util.HashMap;

public interface TestService {

	public ArrayList<HashMap<String, Object>> getJqgridTestList(HashMap<String, Object> param) throws Exception;
	public int getJqgridTestTotalRowCount(HashMap<String, Object> param) throws Exception;
}
