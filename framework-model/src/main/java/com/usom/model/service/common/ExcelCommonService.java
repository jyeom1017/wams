package com.usom.model.service.common;

import java.util.ArrayList;
import java.util.HashMap;

public interface ExcelCommonService {

	public ArrayList<HashMap<String, Object>> getList(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getInfo(HashMap<String, Object> param) throws Exception;
	
	public int getInt(HashMap<String, Object> param) throws Exception;
	
	public String getString(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getList_010203(HashMap<String, Object> param) throws Exception;
	
	public HashMap<String, Object> getInfo_010203(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getPic_010203(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getList_010202(HashMap<String, Object> param) throws Exception;
	
	public HashMap<String, Object> getInfo_010202(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getPic_010202(HashMap<String, Object> param) throws Exception;

	public ArrayList<HashMap<String, Object>> getList1_010102(HashMap<String, Object> param)throws Exception;
	
	public ArrayList<HashMap<String, Object>> getList2_010102(HashMap<String, Object> param)throws Exception;
	
	public ArrayList<HashMap<String, Object>> getPic_010102(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getInfo_010102(HashMap<String, Object> param)throws Exception;
	
	public ArrayList<HashMap<String, Object>> getList_010205(HashMap<String, Object> param) throws Exception;
	
	public HashMap<String, Object> getInfo_010205(HashMap<String, Object> param) throws Exception;

	public ArrayList<HashMap<String, Object>> getPic_010205(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getList1_010306(HashMap<String, Object> param) throws Exception;	
	
	public ArrayList<HashMap<String, Object>> getBBSList(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getList1_010306_2(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getList1_010402(HashMap<String, Object> param)throws Exception;

	public HashMap<String, Object> getList1_010305(HashMap<String, Object> param)throws Exception;

	public ArrayList<HashMap<String, Object>> getBig_010303(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getBig_Info_010303(HashMap<String, Object> param)throws Exception;

	public HashMap<String, Object> getBig_Info_010307(HashMap<String, Object> param)throws Exception;

	public ArrayList<HashMap<String, Object>> getBig_010307(HashMap<String, Object> param)throws Exception;

	public ArrayList<HashMap<String, Object>> getBig_010306(HashMap<String, Object> param)throws Exception;

	public HashMap<String, Object> getBig_Info_010401(HashMap<String, Object> param)throws Exception;

	public ArrayList<HashMap<String, Object>> getBig_010401(HashMap<String, Object> param)throws Exception;

	public HashMap<String, Object> getList2_010306(HashMap<String, Object> param)throws Exception;
}
