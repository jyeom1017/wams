package com.usom.model.service.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;

public interface ReevalService {
	/*reevalService
	   selectReEst
	   insertReEst
	   updateReEst
	   DeleteReEst
	   getNextReEstSid
	   insertReEstAsset
	   deleteReEstAsset
	   selectReEstAsset
	   
	   selectPriceIndex
	   insertPriceIndex
	   updatePriceIndex
	   deletePriceIndex
	   
	   selectWeightSpec
	   insertWeightSpec
	   updateWeightSpec
	   deleteWeightSpec
	   
	   insertWeightSpecItem
	   deleteWeightSpecItem
	   selectWeightSpecItem*/

	ArrayList<HashMap<String, Object>> selectReEst(HashMap<String, Object> param) throws Exception;
	
	ArrayList<HashMap<String, Object>> report(HashMap<String, Object> param) throws Exception;
	
	HashMap<String, Object> selectPriceIndex()throws Exception;
	
	int insertPriceIndex(HashMap<String, Object> param)throws Exception;

	HashMap<String, Object> getWeightSpec(HashMap<String, Object> param)throws Exception;

	int insertWeightSpec(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getWeightSpecItem(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> selectReEstAssetGroup(HashMap<String, Object> param)throws Exception;
	
	ArrayList<HashMap<String, Object>> selectReEstAsset(HashMap<String, Object> param)throws Exception;

	HashMap<String, Object> insertReEst(HashMap<String, Object> param)throws Exception;

	int insertReEstAsset(HashMap<String, Object> param) throws Exception;

	int deleteReEstAsset(HashMap<String, Object> param) throws Exception;

	int deleteReEst(HashMap<String, Object> param)throws Exception;

	int update(HashMap<String, Object> param)throws Exception;

	int delete2ReEst(HashMap<String, Object> param)throws Exception;

}
