package com.usom.model.service.postgis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface PostGisService {

    public ArrayList<HashMap<String, Object>> getBlockInfo(HashMap<String, Object> param) throws Exception;

    public int updateBlockInfo(HashMap<String, Object> param) throws Exception;

    public int insertList(HashMap<String, Object> param) throws Exception;

    public int deleteList(HashMap<String, Object> param) throws Exception;
    
    public ArrayList<HashMap<String, Object>> getTempPipeList(HashMap<String, Object> param) throws Exception;
    
    public ArrayList<HashMap<String, Object>> getNodeList(HashMap<String, Object> param) throws Exception;

    int deleteDoctor1(Map<String, Object> param) throws Exception;

    int insertVerify25310(Map<String, Object> param) throws Exception;

    int updateFeatureCodeWithLastData(Map<String, Object> param) throws Exception;

    int updateBlockInformation(Map<String, Object> param) throws Exception;

    List<Map<String, Object>> getAllAssetInformationInTheBlock(String blockNm);

    int deleteTemporaryAsset(Map<String, Object> param) throws Exception;

    int museums19(Map<String, Object> param) throws Exception;
    
    int updatePipeLmLayers(Map<String, Object> param) throws Exception;
    
    int deleteGisAsset(Map<String, Object> param) throws Exception ;
    
    int insertGisAsset(Map<String, Object> param) throws Exception ;
    
    int setPostgisGeoInfo(Map<String, Object> param) throws Exception ;
}
