package com.usom.model.service.asset;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

public interface ItemService {

	ArrayList<HashMap<String,Object>> selectInfoItemList(HashMap<String, Object> param) throws Exception;
	
    HashMap<String, Object> getItemList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getLevelNumListExcel(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getLevelNumList() throws Exception;

    int deleteAssetPath(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getInfoItemList(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getCustomItemList(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getAllAssetLevelList(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getAllInfoItemList() throws Exception;

    ArrayList<HashMap<String, Object>> getAssetMaxLevelList() throws Exception;

    int updateMaxLevelList(HttpServletRequest req) throws Exception;

    void deleteMaxLevel(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String,Object >> selectAssetLevelPathList(HashMap<String,Object > param) throws Exception;

    int saveAssets(HashMap<String, Object> param) throws Exception;

    int updateAssets(HashMap<String, Object> param) throws Exception;
    
    HashMap<String, Object> selectAssetItemList(HashMap<String,Object > param) throws Exception;
    
    ArrayList<HashMap<String, Object>> selectAssetItemListExcel(HashMap<String,Object > param) throws Exception;
    
    ArrayList<HashMap<String,Object >> selectAssetItemInfo(HashMap<String,Object > param) throws Exception;
    
    int saveAssetItem(HashMap<String,Object > param) throws Exception;

    int updateAssetItem(HashMap<String,Object > param) throws Exception;
    
    //자산 폐기 useYn 'N'으로 업데이트
    int deleteAssetItem(HashMap<String,Object > param) throws Exception;

    //자산 불용처리 상태값을 (item_cd : 10350) '불용' 으로 업데이트
    int deleteAssetItem2(HashMap<String,Object > param) throws Exception;    
    
    //자산삭제 삭제 속성값 까지 모두 삭제
    int deleteAsset(HashMap<String,Object > param) throws Exception;

    ArrayList<HashMap<String,Object >> getAssetLevelList() throws Exception;

    int isAssetPathExists(HashMap<String, Object> param) throws Exception;

    int getTotalCount() throws Exception;
    
    ArrayList<HashMap<String, Object>> selectAssetPic(HashMap<String, Object> param) throws Exception;
    
    int updateAssetPic(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String,Object >> selectAssetHistory(HashMap<String,Object > param) throws Exception;

    int isAssetPathUsed(HashMap<String, Object> param) throws Exception;
    
    HashMap<String,Object > getAssetGisInfo(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String,Object>> getAssetFromGisInfo(HashMap<String, Object> param) throws Exception;
    
    int updateAssetGis(HashMap<String, Object> param) throws Exception;
    
    int insertAssetGis(HashMap<String, Object> param) throws Exception;
    
    int deleteAssetGis(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String,Object>> findAssetList(HashMap<String,Object> param) throws Exception;
    
    ArrayList<HashMap<String,Object>> getStatAsset(HashMap<String,Object> param) throws Exception;
    
}
