package com.usom.model.service.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.usom.model.service.common.CommonService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import org.apache.commons.lang.StringUtils;

public class SigninFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	public static String DEFAULT_TARGET_PARAMETER = "spring-security-redirect-login-failure";
	private String targetUrlParameter = DEFAULT_TARGET_PARAMETER;
	
	@Resource(name = "commonService")
	private CommonService commonService;	
	
	
	public String getTargetUrlParameter() { 
		return targetUrlParameter; 
	} 
	public void setTargetUrlParameter(String targetUrlParameter) {
		this.targetUrlParameter = targetUrlParameter; 
	} @Override public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) 
				throws IOException, ServletException 
	{ 
		String accept = request.getHeader("accept");
		String error = "true"; 
		String message = exception.getMessage();
		String login_id = request.getParameter("user_id");
		String ip_addr = request.getRemoteAddr();
		String lg_desc = exception.getMessage();
		
		
		System.out.println("로그인 실패 " + login_id);
		System.out.println(exception.getMessage());
		//접속로그
		HashMap<String, Object> param = new HashMap<String, Object>();
		try {
			param.put("lg_opt", "F");//1:로그인, 2:출력, 3:에러 , F:로그인실패
			param.put("lg_desc", lg_desc);
			param.put("login_user_id", login_id );
			param.put("E_ID", login_id );
			param.put("ip_addr", ip_addr );
			 
			commonService.insertSystemLog(param);
			
			commonService.updateLoginFailCnt(param);
			
		}catch(Exception e) {
			System.out.print("로그인 성공 후 접속로그 db 에러 param ==>" + param.toString());
		}
		
		if( StringUtils.indexOf(accept, "html") > -1 ) { 
			String redirectUrl = request.getParameter(this.targetUrlParameter); 
			if (redirectUrl != null) { 
				super.logger.debug("Found redirect URL: " + redirectUrl); 
				getRedirectStrategy().sendRedirect(request, response, redirectUrl); 
			} 
			else 
			{ 
				super.onAuthenticationFailure(request, response, exception); 
			} 
		} 
		else if( StringUtils.indexOf(accept, "xml") > -1 ) { 
			response.setContentType("application/xml"); 
			response.setCharacterEncoding("utf-8"); 
			String data = StringUtils.join(new String[] { 
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>", 
					"<response>", 
					"<error>" , error , "</error>", 
					"<message>" , message , "</message>", 
					"</response>" 
					}); 
			PrintWriter out = response.getWriter();
			out.print(data); 
			out.flush(); 
			out.close(); 
		} 
		else if( StringUtils.indexOf(accept, "json") > -1 ) 
		{ 
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8");
		String data = StringUtils.join(new String[] { 
				" { \"response\" : {", 
				" \"error\" : " , error , ", ", 
				" \"message\" : \"", message , "\" ", "} } " 
				});
		PrintWriter out = response.getWriter(); 
		out.print(data); 
		out.flush(); 
		out.close(); 
		} 
	}
				
}