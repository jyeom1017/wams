package com.usom.model.service.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;

import com.usom.model.dao.operation.OperationDao;

public interface LifeService {
	public ArrayList<HashMap<String, Object>> getLIFE(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getFactor(int index) throws Exception;
	public int saveLife_Spec(HashMap<String, Object> param)throws Exception;
	public HashMap<String, Object> newLife(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET(HashMap<String, Object> param) throws Exception;
	public int updateLIFE_ASSET(HashMap<String, Object> param)throws Exception;
	public HashMap<String, Object> getLIFE_ASSET_VARIATION(HashMap<String, Object> param)throws Exception;
	public int saveLIFE(HashMap<String, Object> param)throws Exception;
	public int continueLIFE_ASSET(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> outputLIFE_ASSET(HashMap<String, Object> param)throws Exception;
	public HashMap<String, Object> getFN_LIST(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_RESULT(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE_MODIFY(HashMap<String, Object> param)throws Exception;
	public int getLIFE_YCOL(HashMap<String, Object> param)throws Exception;
	public HashMap<String, Object> saveLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception;
	public int updateLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception;
	public int resetLIFE_ASSET_STATUS_GRADE_MODIFY(HashMap<String, Object> param)throws Exception;
	public int saveLIFE_ASSET_STATUS_GRADE_MODIFY(HashMap<String, Object> param)throws Exception;
	public HashMap<String, Object> getUPDATE_ID(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_GRAPH(HashMap<String, Object> param)throws Exception;
	public int deleteList(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getAssetReport(HashMap<String, Object> param)throws Exception;
	public int OnFileSelectLIFE(HashMap<String, Object> param)throws Exception;
	public int getTotal(HashMap<String, Object> param)throws Exception;
	ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE_REPORT(HashMap<String, Object> param)throws Exception;
	public int updateLIFE(HashMap<String, Object> param)throws Exception;
	ArrayList<HashMap<String, Object>> getLIFE_ASSET_REPORT_FORM(HashMap<String, Object> param) throws Exception;
	public int Test(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getAssetFrom(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_REPORT(HashMap<String, Object> param)throws Exception;
	public int refreshLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception;
	
}
