package com.usom.model.service.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;

public interface LosService {

	ArrayList<HashMap<String, Object>> selectLos(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> selectEvalue(HashMap<String, Object> param) throws Exception;
	
	int insertEvalue(HashMap<String, Object> param) throws Exception;
	/*reevalService
	   selectReEst
	   insertReEst
	   updateReEst
	   DeleteReEst
	   getNextReEstSid
	   insertReEstAsset
	   deleteReEstAsset
	   selectReEstAsset
	   
	   selectPriceIndex
	   insertPriceIndex
	   updatePriceIndex
	   deletePriceIndex
	   
	   selectWeightSpec
	   insertWeightSpec
	   updateWeightSpec
	   deleteWeightSpec
	   
	   insertWeightSpecItem
	   deleteWeightSpecItem
	   selectWeightSpecItem*/
	HashMap<String, Object> getEvalueItem(HashMap<String, Object> param) throws Exception;
	int insertEvalueItem(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> getWeightList(HashMap<String, Object> param) throws Exception;
	int insertWeight(HashMap<String, Object> param) throws Exception;
	ArrayList<HashMap<String, Object>> selectWeight(HashMap<String, Object> param) throws Exception;
	int deleteLos(HashMap<String, Object> param) throws Exception;
}
