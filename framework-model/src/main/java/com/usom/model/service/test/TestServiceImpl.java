/**
 * 
 */
package com.usom.model.service.test;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usom.model.dao.test.TestDao;

/**
 * @author Administrator
 *
 */
@Service("testService")
public class TestServiceImpl implements TestService {
	
	@Resource(name="tiberoSession")
	private SqlSession oracleSession;

	public ArrayList<HashMap<String, Object>> getJqgridTestList(
			HashMap<String, Object> param) throws Exception {
		TestDao dao = oracleSession.getMapper(TestDao.class);
		return dao.getJqgridTestList(param);
	}
	public int getJqgridTestTotalRowCount(HashMap<String, Object> param)
			throws Exception {
		TestDao dao = oracleSession.getMapper(TestDao.class);
		return dao.getJqgridTestTotalRowCount(param);
	}







	
}
