package com.usom.model.service.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usom.model.dao.diagnosis.LccDao;

@Service("lccService")
public class LccServiceimpl implements LccService {
	
	@Resource(name="tiberoSession")
	private SqlSession oracleSession;
	
	@Override
	public ArrayList<HashMap<String, Object>>getLCC(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
		
	    return dao.getLCC(param);
    }
	@Override
	public ArrayList<HashMap<String, Object>>getLCC_ASSET(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
	    return dao.getLCC_ASSET_RESULT(param);
    }
	@Override
	public HashMap<String, Object>getLCC_ASSET_TOTAL(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
		System.out.println("sssssssssssssss" + param);
	    return dao.getLCC_ASSET_RESULT_TOTAL(param);
    }
	@Override
	public HashMap<String, Object> getLCCDetails(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
		HashMap<String, Object> map = new HashMap<String, Object>();
		ArrayList<HashMap<String,Object>> List = null;
		
		switch ((int)param.get("page")) {
		case 2: List = dao.getLCC_ASSET_RESULT_DETAILS(param); break;
		default: List = dao.getLCC_ASSET_RESULT_DETAILS_MULTI(param); break;
		};
		
		map.put("List", List);
		//map.put("ListSum", dao.getLCC_ASSET_RESULT_DETAILS_SUM(param));
	    return map; 
    }
	@Override
	public ArrayList<HashMap<String, Object>>getLCC_RESULT(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
	    return dao.getLCC_RESULT(param);
    }
	@Override
	public HashMap<String, Object>getLCC_RESULT_TOTAL(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
	    return dao.getLCC_RESULT_SUM(param);
    }
	@Override
	public HashMap<String, Object>startLCC(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
		dao.updateLCC(param);
		dao.deleteLCC_ASSET(param);
		dao.deleteLCC_ASSET_RESULT_ALL(param);
		dao.deleteLCC_RESULT(param);
	    return param;
    }
	
	@Override
	public HashMap<String, Object>insertLCC(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
		dao.insertLCC(param);
	    return param;
    }
	
	@Override
	public HashMap<String, Object>insertLCC_ASSET_RESULT(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
		dao.deleteLCC_ASSET_RESULT(param);
		dao.insertLCC_ASSET_RESULT(param);
		param.put("Loading_ins", "asset을 생성 중");
	    return param;
    }
	
	
	@Override
	public HashMap<String, Object> factoryLCC_ASSET_RESULT(HashMap<String, Object> param) throws Exception {
		LccDao dao = oracleSession.getMapper(LccDao.class);
		if((int)param.get("ASSET_A") == 0) dao.insertLCC_ASSET(param);
		dao.insertLCC_ASSET_RESULT(param);
		String str = param.get("PERCENT").toString();
		double per = Double.parseDouble(str);
		param.put("Loading_ins", "ASSET("+ String.format("%.1f", per) + "/100)");
		return param;
	}
	
	@Override
	public HashMap<String, Object> factoryLCC_ASSET_RESULT_FN(HashMap<String, Object> param) throws Exception {
		LccDao dao = oracleSession.getMapper(LccDao.class);
		
		if(param.get("FN_CD").equals("F") && (int)param.get("ASSET_A") == 0) dao.updateLCC_RESULT(param); 
		
		dao.insertLCC_ASSET_RESULT_FN(param);
		String str = param.get("PERCENT").toString();
		double per = Double.parseDouble(str);
		str = param.get("FN_CD").equals("F") ? "시설(":"관망(";
		
		param.put("Loading_ins", str + String.format("%.1f", per) + "/100)");
		
		return param;
	}
	
	@Override
	public int deleteLcc(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
		int count = 0;
		count += dao.deleteLCC(param);
		count += dao.deleteLCC_ASSET(param);
		count += dao.deleteLCC_ASSET_RESULT_ALL(param);
		count += dao.deleteLCC_RESULT(param);
	    return count;
    }
	
	@Override
	public HashMap<String, Object> report(HashMap<String, Object> param) throws Exception{
		LccDao dao = oracleSession.getMapper(LccDao.class);
		
		param.remove("page");
		param.remove("rows");
		param.put("CLASS4_CD", "");
		param.put("CLASS3_CD", "");
		param.put("SQL", "");
		
		HashMap<String, Object> result =  new HashMap<String, Object>();
		param.put("FN_CD", "F");
		result.put("flist", dao.reportFN(param));
		result.put("ftotal", dao.getLCC_RESULT_SUM(param));
		
		param.put("FN_CD", "N");
		result.put("nlist", dao.reportFN(param));
		result.put("ntotal", dao.getLCC_RESULT_SUM(param));
		
		result.put("tlist", dao.reportASSET(param));
		result.put("ttotal", dao.reportASSET_TOTAL(param));
		
	    return result;
    }
}