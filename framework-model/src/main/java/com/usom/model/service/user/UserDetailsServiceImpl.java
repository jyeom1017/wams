package com.usom.model.service.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.usom.model.config.Configuration;
import com.usom.model.dao.user.UserDetailsServiceDAO;
import com.usom.model.vo.user.MenuVO;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

/**
 * * A custom {@link UserDetailsService} where user information * is retrieved
 * from a JPA repository
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Resource(name="tiberoSession")
	private SqlSession oracleSession;

	/**
	 * * Returns a populated {@link UserDetails} object. * The username is first
	 * retrieved from the database and then mapped to * a {@link UserDetails}
	 * object.
	 */
	public UserDetails loadUserByUsername(String USER_ID)
			throws UsernameNotFoundException {
		try {
			//사이트 USER 테이블 조회
			UserDetailsServiceDAO dao = oracleSession.getMapper(UserDetailsServiceDAO.class);

			/* [중요] 전역 시스템 코드 */
			User domainUser = dao.findByUserId(USER_ID);
			if(domainUser == null) {//Exception 방지
				domainUser = new User();
				domainUser.setE_ID(USER_ID);
				domainUser.setE_PW("");
				domainUser.setE_GCODE1("");
				domainUser.setE_GROLE("");
				//domainUser.setE_GCODE2("");
			}
			
			boolean enabled = (domainUser.getE_INOFF().equals("1") && (domainUser.getFAIL_CNT()<5))?true:false;
			boolean accountNonExpired = (domainUser.getE_INOFF().equals("1"))?true:false;
			boolean credentialsNonExpired = true;
			boolean accountNonLocked = (domainUser.getFAIL_CNT()<5)?true:false;
			
			UserDetailsServiceVO user = new UserDetailsServiceVO(
									domainUser.getE_ID(), 
									domainUser.getE_PW_ENC(), 
									//domainUser.getE_PW(),
									enabled, 
									accountNonExpired,
									credentialsNonExpired, 
									accountNonLocked
									,getAuthorities(domainUser.getE_GROLE())
								);
			
			//유저정보
			user.setUser(domainUser);
			
			
			MenuVO menuVO = new MenuVO();
			menuVO.setG_GCODE(domainUser.getE_GCODE1());//일반권한코드
			menuVO.setE_ID(domainUser.getE_ID());
			//권한메뉴정보 - 일반메뉴
			user.setBasicMenuList((ArrayList<MenuVO>) dao.getAuthMenuList(menuVO));
			

			//menuVO.setG_GCODE(domainUser.getE_GCODE2());//운영사권한코드
			//권한메뉴정보 - 운영사메뉴
			user.setOperatorMenuList((ArrayList<MenuVO>) dao.getAuthMenuList(menuVO));
			
			
			return user;
		} catch (Exception e) {
			//e.printStackTrace();//임시
			throw new RuntimeException(e);
		}
	}

	/**
	 * * Retrieves a collection of {@link GrantedAuthority} based on a numerical
	 * role * @param role the numerical role * @return a collection of
	 * {@link GrantedAuthority	
	 */
	public Collection<? extends GrantedAuthority> getAuthorities(String role1) {
		List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role1));
		return authList;
	}

	/**
	 * * Converts a numerical role to an equivalent list of roles * @param role
	 * the numerical role * @return list of roles as as a list of {@link String}
	 */
	public List<String> getRoles(String role1) {
		List<String> roles = new ArrayList<String>();
		if(! role1.equals("") || role1 != null) {
			roles.add(role1);	
		}
		return roles;
	}

	/**
	 * * Wraps {@link String} roles to {@link SimpleGrantedAuthority} objects * @param
	 * roles {@link String} of roles * @return list of granted authorities
	 */
	public static List<GrantedAuthority> getGrantedAuthorities(
			List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}
	
}
