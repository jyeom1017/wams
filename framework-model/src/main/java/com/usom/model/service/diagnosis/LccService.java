package com.usom.model.service.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;

public interface LccService {

	ArrayList<HashMap<String, Object>> getLCC(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> getLCC_ASSET_TOTAL(HashMap<String, Object> param) throws Exception;
	ArrayList<HashMap<String, Object>> getLCC_ASSET(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> getLCCDetails(HashMap<String, Object> param) throws Exception;
	ArrayList<HashMap<String, Object>> getLCC_RESULT(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> getLCC_RESULT_TOTAL(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> insertLCC(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> startLCC(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> insertLCC_ASSET_RESULT(HashMap<String, Object> param) throws Exception;
	int deleteLcc(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> report(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> factoryLCC_ASSET_RESULT(HashMap<String, Object> param)throws Exception;
	HashMap<String, Object> factoryLCC_ASSET_RESULT_FN(HashMap<String, Object> param)throws Exception;
	
	/*reevalService
	   selectReEst
	   insertReEst
	   updateReEst
	   DeleteReEst
	   getNextReEstSid
	   insertReEstAsset
	   deleteReEstAsset
	   selectReEstAsset
	   
	   selectPriceIndex
	   insertPriceIndex
	   updatePriceIndex
	   deletePriceIndex
	   
	   selectWeightSpec
	   insertWeightSpec
	   updateWeightSpec
	   deleteWeightSpec
	   
	   insertWeightSpecItem
	   deleteWeightSpecItem
	   selectWeightSpecItem*/
	
	
	
	

}
