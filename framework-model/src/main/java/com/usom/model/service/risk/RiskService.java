package com.usom.model.service.risk;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

public interface RiskService {

	HashMap<String, Object> getList(HashMap<String, Object> param) throws Exception;
    
	ArrayList<HashMap<String, Object>> getAssetGroup(HashMap<String, Object> param) throws Exception;
	
    ArrayList<HashMap<String, Object>> getAssetList(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getAssetListExcel(HashMap<String, Object> param) throws Exception;
    List <HashMap<String, Object>> getAssetListExcel1(HashMap<String, Object> param) throws Exception;

    int insert(HashMap<String, Object> param) throws Exception;
    int update(HashMap<String, Object> param) throws Exception;
    int delete(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> getFactorList(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getExcelList(HashMap<String, Object> param) throws Exception;
    
    int insertFactor(HashMap<String, Object> param) throws Exception;
    
    int updateFactor(HashMap<String, Object> param) throws Exception;
    
    
    ArrayList<HashMap<String, Object>> getGradeList(HashMap<String, Object> param);
    
    int insertGrade(HashMap<String, Object> param) throws Exception;
    
    int updateGrade(HashMap<String, Object> param) throws Exception;    
    
    ArrayList<HashMap<String, Object>> getFactorDetail(HashMap<String, Object> param);
  
    ArrayList<HashMap<String, Object>> getGradeDetail(HashMap<String, Object> param);
    
    int saveAsset(HashMap<String, Object> param) throws Exception;
    
    int deleteAsset(HashMap<String, Object> param) throws Exception;
    
    int updateAsset(HashMap<String, Object> param) throws Exception;
    
    ArrayList<HashMap<String, Object>> getRiskResult(HashMap<String, Object> param);
    
}
