package com.usom.model.service.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.usom.model.service.common.CommonService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import org.apache.commons.lang.StringUtils;



public class CustomAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	
	@Resource(name = "commonService")
	private CommonService commonService;	
	
	
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {

//		setSessionInfo(request, authentication);
		
		// 로그인 완료후 처리할 내용
		//super.onAuthenticationSuccess(request, response, authentication);
		
		UserDetailsServiceVO userDetailVO = (UserDetailsServiceVO) authentication.getPrincipal();
		WebAuthenticationDetails details  = (WebAuthenticationDetails) authentication.getDetails();
		System.out.print("로그인 성공 userDetailVO==>" + userDetailVO);
		
		//접속로그
		HashMap<String, Object> param = new HashMap<String, Object>();
		try {
			param.put("lg_opt", "1");//1:로그인, 2:출력, 3:에러
			param.put("lg_desc", userDetailVO.getUser().getE_ID().toString() + " 로그인");
			param.put("login_user_id", userDetailVO.getUser().getE_ID().toString());
			param.put("ip_addr", details.getRemoteAddress().toString());
			param.put("E_ID", userDetailVO.getUser().getE_ID().toString());
			param.put("FAIL_CNT", 0);
			commonService.insertSystemLog(param);
			
			commonService.updateLoginFailCnt(param);
			
		}catch(Exception e) {
			System.out.print("로그인 성공 후 접속로그 db 에러 param ==>" + param.toString());
		}
		
		
		
		String accept = request.getHeader("accept"); 
		if( StringUtils.indexOf(accept, "html") > -1 ) 
		{ 
			//super.onAuthenticationSuccess(request, response, authentication);
			 //response.sendRedirect(request.getHeader("referer"));
			 response.sendRedirect("/login/success");
		} 
		else if( StringUtils.indexOf(accept, "xml") > -1 ) 
		{ 
			response.setContentType("application/xml"); 
			response.setCharacterEncoding("utf-8"); 
			String data = StringUtils.join(new String[] { 
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
					"<response>", 
					"<error>false</error>",
					"<message>로그인하였습니다.</message>", 
					"</response>" 
					}); 
			PrintWriter out = response.getWriter(); 
			out.print(data); 
			out.flush(); 
			out.close(); 
		} 
		else if( StringUtils.indexOf(accept, "json") > -1 ) 
		{ 
			response.setContentType("application/json"); 
			response.setCharacterEncoding("utf-8"); 
			String data = StringUtils.join(new String[] { 
					" { \"response\" : {", 
					" \"error\" : false , ",
					" \"message\" : \"로그인하였습니다.\" ",
					",",
					" \"res_code\": 200 ",
					",",
					" \"data\": {",
					" \"user_id\": \"",userDetailVO.getUser().getE_ID().toString(),"\"",
					",",
					" \"user_name\": \"",userDetailVO.getUser().getE_NAME().toString(),"\"",
					",",
					" \"group_code\": \"",userDetailVO.getUser().getE_GCODE1().toString(),"\"",					
					",",
					" \"group_name\": \"",userDetailVO.getUser().getE_GNAME1().toString(),"\"",					
					 "} ",
					"} } " 
					}); 
			PrintWriter out = response.getWriter(); 
			out.print(data); 
			out.flush(); 
			out.close(); 
		}

				
	}

	/**
	 * 회원정보 세션에 저장(추후 필요하면 구현)
	 * @param request
	 * @param authentication
	 */
	public void setSessionInfo(HttpServletRequest request, Authentication authentication) {
		User userVO = new User();
		RequestContextHolder.currentRequestAttributes().setAttribute(
				"session_user_info", userVO, RequestAttributes.SCOPE_SESSION);
	}
	/*
	public String getLoginidname() {        
		return loginidname;    
	}     

	public void setLoginidname(String loginidname) {
		this.loginidname = loginidname;    
	}     
	
	public String getDefaultUrl() {        
		return defaultUrl;    
	}     
	
	public void setDefaultUrl(String defaultUrl) {        
		this.defaultUrl = defaultUrl;    
	}
	*/
}
