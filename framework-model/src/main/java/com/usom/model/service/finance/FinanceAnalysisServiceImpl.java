package com.usom.model.service.finance;


import com.framework.model.util.CommonUtil;
import com.usom.model.dao.common.CommonDao;
import com.usom.model.dao.finance.FinanceAnalysisDao;
import net.sf.json.JSONArray;
import org.apache.commons.lang.ArrayUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Service("FinanceAnalysisService")
public class FinanceAnalysisServiceImpl implements FinanceAnalysisService {

	@Resource(name="tiberoSession")
    private SqlSession tiberoSession;

    @Override
    public ArrayList<HashMap<String, Object>> getFinancialPlanList(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        return financeAnalysisDao.getFinancialPlanList(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getFinancialPlanDetailList(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        param.put("columnList", arrayToStringWithSingleQuotes(getFinancialPlanYearArray(param)));

        return financeAnalysisDao.getFinancialPlanDetailList(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getFinancialStatementsList(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        //add30YearsAfter2021(param);
        
        
        param.put("columnList", arrayToStringWithSingleQuotes(getComCodeYearList2(param)));

        return financeAnalysisDao.getFinancialStatementsList(param);
    }

    @Override
    public int setFinancialStatement(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        List<Map<String, Object>> list = changeColumnToRow2(param);

        if (list.size() > 0) {
        	//System.out.println(list);
            return financeAnalysisDao.setFinancialStatement(list);
        }

        return 0;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getIncomeStatement(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        //add30YearsAfter2021(param);
        param.put("columnList", arrayToStringWithSingleQuotes(getComCodeYearList2(param)));

        return financeAnalysisDao.getIncomeStatement(param);
    }

    @Override
    public int setIncomeStatement(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        List<Map<String, Object>> list = changeColumnToRow2(param);

        if (list.size() > 0) {
            return financeAnalysisDao.setIncomeStatement(list);
        }

        return 0;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getCashFlowStatement(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        //add30YearsAfter2021(param);
        param.put("columnList", arrayToStringWithSingleQuotes(getComCodeYearList2(param)));

        return financeAnalysisDao.getCashFlowStatement(param);
    }

    @Override
    public int setCashFlowStatement(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        List<Map<String, Object>> list = changeColumnToRow2(param);

        if (list.size() > 0) {
            return financeAnalysisDao.setCashFlowStatement(list);
        }

        return 0;
    }

    private void add30YearsAfter2021(HashMap<String, Object> param) {
        param.put("fin_anls_ymd", "2021");
        param.put("columnList", arrayToStringWithSingleQuotes(get30YearsSinceTheDateOfAnalysis(param)));
    }

    @Override
    public ArrayList<HashMap<String, Object>> getOtherCostManagement(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        String[] otherCostManagementArray = financeAnalysisDao.getOtherCostManagementArray(param);

        param.put("columnList", arrayToStringWithSingleQuotes(otherCostManagementArray));
        
        //param.put("columnList", arrayToStringWithSingleQuotes(getComCodeYearList2(param)));

        return financeAnalysisDao.getOtherCostManagement(param);
    }

    @Override
    public int setOtherCostManagement(Map<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        String userId = (String) param.get("user_id");
        List<Map<String, Object>> itemList = (List<Map<String, Object>>) param.get("item_list");
        List<Map<String, Object>> list = new ArrayList<>();

        for (Map<String, Object> map : itemList) {
            for (String key : map.keySet()) {
                if (key.contains("_")) {
                    String[] code = key.split("_");
                    String etcCostCd1 = code[0];
                    String etcCostCd2 = code[1];
                    String yyyy = map.get("YYYY").toString();
                    Object etcCostVal = map.get(etcCostCd1 + "_" + etcCostCd2);

                    if (etcCostVal.equals("null")) {
                        etcCostVal = null;
                    }

                    Map<String, Object> hashMap = new HashMap<>();
                    //hashMap.putAll(map);
                    hashMap.put("yyyy", yyyy);
                    hashMap.put("etc_cost_cd1", etcCostCd1);
                    hashMap.put("etc_cost_cd2", etcCostCd2);
                    hashMap.put("etc_cost_val", etcCostVal);
                    hashMap.put("user_id", userId);
                    list.add(hashMap);
                }
            }
        }

        if (list.size() > 0) {
            int a = financeAnalysisDao.setOtherCostManagement(list);
            // TODO: 합계 재계산
            int b = 0;

            return Math.min(a, b);
        }

        return 0;
    }

    private String[] getFinancialPlanYearArray(HashMap<String, Object> param) {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        return financeAnalysisDao.getFinancialPlanYearArray(param);
    }

    private String[] get30YearsSinceTheDateOfAnalysis(HashMap<String, Object> param) {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        return financeAnalysisDao.get30YearsSinceTheDateOfAnalysis(param);
    }

    private String[] getComCodeYearList2(HashMap<String, Object> param) {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        return financeAnalysisDao.getComCodeYearList2(param);
    }
    
    private String arrayToStringWithSingleQuotes(String[] array) {
        if (ArrayUtils.isNotEmpty(array)) {
            StringBuilder sb = new StringBuilder("'");

            for (String str : array) {
                sb.append(str + "','");
            }

            return sb.delete(sb.lastIndexOf(","), sb.length()).toString();
        }

        return "''";
    }

    @Override
    public int deleteFinancialPlan(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        return financeAnalysisDao.deleteFinancialPlan(param);
    }

    @Override
    public Map<String, Object> insertTempFinancialPlan(Map<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        financeAnalysisDao.insertTempFinancialPlan(param);

        Map<String, Object> map = new HashMap<>();
        map.put("FIN_ANLS_SID", param.get("fin_anls_sid"));

        return map;
    }

    @Override
    public List<Map<String, Object>> getOperationAndManagementExpensesPopupList(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        param.put("columnList", arrayToStringWithSingleQuotes(get30YearsSinceTheDateOfAnalysis(param)));

        return financeAnalysisDao.getOperationAndManagementExpensesPopupList(param);
    }

    @Override
    public List<Map<String, Object>> getDepreciationPopupList(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        param.put("columnList", arrayToStringWithSingleQuotes(get30YearsSinceTheDateOfAnalysis(param)));

        return financeAnalysisDao.getDepreciationPopupList(param);
    }

    @Override
    public int setFinancialPlanDetail(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        String tempYn = (String) param.get("temp_yn");

        if (tempYn.equals("N")) {
            financeAnalysisDao.updateFinancialPlan(param);
        }
        List<Map<String, Object>> list = changeColumnToRow(param);
        if(param.get("fin_anls_type").equals("FN14")) {
        	//System.out.println(list.toString());
        	if (list.size() > 0) {
        		return financeAnalysisDao.setFinancialPlanLoan(list);
        	}
        }else {
	        if (list.size() > 0) {
	            return financeAnalysisDao.setFinancialPlanDetail(list);
	        }
        }

        return 0;
    }

    @Override
    public int updateFinancialPlan(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        return financeAnalysisDao.updateFinancialPlan(param);
    }

    @Override
    public Map<String, Object> getUnitPriceSettingInfo(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        Map<String, Object> map = new HashMap<>();
        map.put("UNIT_PRICE_INFO", unitPricePivot(financeAnalysisDao.getUnitPriceSettingInfo(param)));
        map.put("INFLATION_INFO", inflationPivot(financeAnalysisDao.getInflationRateOverTheLastFiveYearsInfo(param)));

        return map;
    }

    private Map<String, Object> unitPricePivot(List<Map<String, Object>> list) {
        HashMap<String, Object> hashMap = new HashMap<>();

        for (Map<String, Object> stringObjectMap : list) {
            String gcode = (String) stringObjectMap.get("C_GCODE");
            String scode = (String) stringObjectMap.get("C_SCODE");

            hashMap.put("YYYY", stringObjectMap.get("YYYY"));
            hashMap.put(gcode + "_" + scode, stringObjectMap.get("ETC_COST_VAL"));
        }

        return hashMap;
    }

    private Map<String, Object> inflationPivot(List<Map<String, Object>> list) {
        HashMap<String, Object> hashMap = new HashMap<>();

        for (Map<String, Object> stringObjectMap : list) {
            hashMap.put("YYYY" + "_" + stringObjectMap.get("YYYY"), stringObjectMap.get("PRICE_RAISE_RATIO"));
        }

        return hashMap;
    }

    @Override
    public Map<String, Object> getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        return unitPricePivot(financeAnalysisDao.getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo(param));
    }

    @Override
    public Map<String, Object> getFinancialAnalysisChargeUnitPrice() throws Exception {
        CommonDao commonDao = tiberoSession.getMapper(CommonDao.class);
        HashMap<String, Object> param = new HashMap<>();
        param.put("gcode", "FN07");
        ArrayList<HashMap<String, Object>> list = commonDao.getCommonCodeList(param);
        Map<String, Object> map = new HashMap<>();

        for (Map<String, Object> stringObjectMap : list) {
            String gcode = (String) stringObjectMap.get("C_GCODE");
            String scode = (String) stringObjectMap.get("C_SCODE");

            map.put(gcode + "_" + scode, stringObjectMap.get("C_VALUE1"));
        }

        return map;
    }

    @Override
    public int updateInterestRateEndOfLastYearInfo(Map<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        String userId = (String) param.get("user_id");
        Map<String, Object> map = (Map<String, Object>) param.get("item");
        int result = 0;

        for (String key : map.keySet()) {
            if (key.contains("_")) {
                String[] code = key.split("_");
                String gcode = code[0];
                String scode = code[1];

                Map<String, Object> hashMap = new HashMap<>();
                hashMap.put("c_gcode", gcode);
                hashMap.put("c_scode", scode);
                hashMap.put("c_value1", map.get(key));
                hashMap.put("user_id", userId);
                result = financeAnalysisDao.updateBeings(hashMap);
            }
        }

        return result;
    }

    @Override
    public int setFinancialAnalysisChargeUnitPrice(Map<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        int finAnlsSid = (int) param.get("fin_anls_sid");
        String userId = (String) param.get("user_id");
        Map<String, Object> map = (Map<String, Object>) param.get("item");
        int result = 0;

        for (String key : map.keySet()) {
            if (key.contains("_")) {
                String[] etcCostCd = key.split("_");
                String etcCostCd1 = etcCostCd[0];
                String etcCostCd2 = etcCostCd[1];

                Map<String, Object> hashMap = new HashMap<>();
                hashMap.put("fin_anls_sid", finAnlsSid);
                hashMap.put("etc_cost_cd1", etcCostCd1);
                hashMap.put("etc_cost_cd2", etcCostCd2);
                hashMap.put("etc_cost_val", map.get(key));
                hashMap.put("user_id", userId);
                result = financeAnalysisDao.setFinancialAnalysisChargeUnitPrice(hashMap);
            }
        }

        return result;
    }

    private List<Map<String, Object>> changeColumnToRow(HashMap<String, Object> param) {
        List<HashMap<String, Object>> itemList = (List<HashMap<String, Object>>) param.get("item_list");
        List<Map<String, Object>> list = new ArrayList<>();

        for (int i = 0; i < itemList.size(); i++) {
            Map<String, Object> hashMap = itemList.get(i);
            List<Map<String, Object>> yyyyList = new ArrayList();
            Map<String, Object> column = new HashMap<>();

            columnSeparation(hashMap, yyyyList, column);
            fieldSeparation(param, list, yyyyList, column);
        }

        return list;
    }

    private List<Map<String, Object>> changeColumnToRow2(HashMap<String, Object> param) {
        List<HashMap<String, Object>> itemList = (List<HashMap<String, Object>>) param.get("item_list");
        List<Map<String, Object>> list = new ArrayList<>();

        for (int i = 0; i < itemList.size(); i++) {
            Map<String, Object> hashMap = itemList.get(i);
            List<Map<String, Object>> yyyyList = new ArrayList();
            Map<String, Object> column = new HashMap<>();

            columnSeparation(hashMap, yyyyList, column);
            fieldSeparation2(param, list, yyyyList, column);
        }

        return list;
    }

    private void columnSeparation(Map<String, Object> hashMap, List<Map<String, Object>> yyyyList, Map<String, Object> column) {
        for (String key : hashMap.keySet()) {
            if (key.matches("'\\d+'")) {
                String yyyy = key.replaceAll("'", "");
                Map<String, Object> map = new HashMap<>();
                map.put(yyyy, hashMap.get(key));
                yyyyList.add(map);
            } else {
                column.put(key.toLowerCase(), hashMap.get(key));
            }
        }
    }

    private void fieldSeparation(HashMap<String, Object> param, List<Map<String, Object>> list, List<Map<String, Object>> yyyyList, Map<String, Object> column) {
        int finAnlsSid = (int) param.get("fin_anls_sid");
        String userId = (String) param.get("USER_ID");

        for (Map<String, Object> stringObjectMap : yyyyList) {
            Map<String, Object> hashMap = new HashMap<>();
            hashMap.putAll(column);
            hashMap.put("fin_anls_sid", finAnlsSid);
            hashMap.put("user_id", userId);

            for (String yyyy : stringObjectMap.keySet()) {
                String itemValue = String.valueOf(stringObjectMap.get(yyyy));
                hashMap.put("yyyy", yyyy);

                if (itemValue.equals("\"null\"")) {
                    itemValue = null;
                }

                hashMap.put("fin_anls_item_val", itemValue);
            }

            list.add(hashMap);
        }
    }

    private void fieldSeparation2(HashMap<String, Object> param, List<Map<String, Object>> list, List<Map<String, Object>> yyyyList, Map<String, Object> column) {
        String userId = (String) param.get("USER_ID");

        for (Map<String, Object> stringObjectMap : yyyyList) {
            Map<String, Object> hashMap = new HashMap<>();
            hashMap.putAll(column);
            hashMap.put("user_id", userId);

            for (String yyyy : stringObjectMap.keySet()) {
                String itemValue = String.valueOf(stringObjectMap.get(yyyy));
                hashMap.put("yyyy", yyyy);

                if (itemValue.equals("\"null\"")) {
                    itemValue = null;
                }

                hashMap.put("acount_amt", itemValue);
            }

            list.add(hashMap);
        }
    }

    @Override
    public ArrayList<HashMap<String, Object>> getAssetManagementPlanList(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        return financeAnalysisDao.getAssetManagementPlanList(param);
    }

    @Override
    public ArrayList<HashMap<String, Object>> getAssetManagementPlanDetailList(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        return financeAnalysisDao.getAssetManagementPlanDetailList(param);
    }

    @Override
    public int deleteAssetManagementPlan(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        int result2 = financeAnalysisDao.deleteAssetManagementPlanDetail(param);
        int result1 = financeAnalysisDao.deleteAssetManagementPlan(param);

        return Math.max(result1, result2);
    }

    @Override
    public int insertAssetManagementPlan(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);

        int result1 = financeAnalysisDao.insertAssetManagementPlan(param);
        int AMP_SID = financeAnalysisDao.getLastAmpSid();
        param.put("AMP_SID", AMP_SID);
        int result2 = financeAnalysisDao.insertAssetManagementPlanDetail(param);

        return (result1 == 1) ? AMP_SID : 0;
    }

    @Override
    public int insertAssetManagementPlanDetail(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);
        int result1 = financeAnalysisDao.insertAssetManagementPlanDetail(param);
        return (result1);
    }

    @Override
    public int updateAssetManagementPlan(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);
        int result1 = financeAnalysisDao.updateAssetManagementPlan(param);
        System.out.println(param);
        JSONArray itemList = (JSONArray) param.get("ITEM_LIST");
        System.out.println(param.get("ITEM_LIST"));
        for (int i = 0; i < itemList.size(); i++) {
            HashMap<String, Object> param2 = CommonUtil.jsonObjectToHashMap(itemList.getJSONObject(i));
            param2.put("AMP_SID", param.get("AMP_SID"));
            param2.put("INSERT_ID", param.get("INSERT_ID"));
            financeAnalysisDao.updateAssetManagementPlanDetail(param2);
            System.out.println(param2);
        }
        return result1;
    }

    @Override
    public int updateAssetManagementPlanDetail(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);
        int result1 = financeAnalysisDao.updateAssetManagementPlanDetail(param);
        return result1;
    }
    
    @Override
    public HashMap<String, Object> getFinacePriceRaiseRatio(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);
    	return financeAnalysisDao.getFinacePriceRaiseRatio(param);
    }
    
    @Override
    public int addComCodeListYr2(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);
        return financeAnalysisDao.addComCodeListYr2(param);
    }
    
    @Override
    public int addLoanItemYear(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);
        return financeAnalysisDao.addLoanItemYear(param);
    }
    
    @Override
    public int initFinancialPlanDetailList(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);
        
        //param.put("fin_anls_ymd", "2021");
        param.put("columnList", arrayToStringWithSingleQuotes(get30YearsSinceTheDateOfAnalysis(param)));
        
        financeAnalysisDao.deleteFinancialPlanDetailList(param);
        return financeAnalysisDao.initFinancialPlanDetailList(param);
    }
    
    @Override
    public ArrayList<HashMap<String, Object>> getLoanInfo(HashMap<String, Object> param) throws Exception {
        FinanceAnalysisDao financeAnalysisDao = tiberoSession.getMapper(FinanceAnalysisDao.class);
    	return financeAnalysisDao.getLoanInfo(param);
    }
    
     
}