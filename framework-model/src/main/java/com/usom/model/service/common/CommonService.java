package com.usom.model.service.common;

import java.util.ArrayList;
import java.util.HashMap;

/**
* <pre>
* 설명 : 공통적인 전역 기능들을 모아놈(공통코드, 파일, 일정마스터..)
* 상세설명 : .
* com.omas.model.service.common
*   |_ CommonService.java
* </pre>
* 
* @Company : MAGIS
* @Author  : ash
* @Date    : 2017. 1. 4. 오후 5:53:25
* @Version : 1.0
 */
public interface CommonService {
	/**
	* <pre>
	* 설명 : 공통코드 - 조회
	* 상세설명 : ..
	* </pre>
	 */
	public ArrayList<HashMap<String, Object>> getBlockCodeList(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getCommonCodeGroup(HashMap<String, Object> param) throws Exception;
	public int insertCommonCodeGroup(HashMap<String, Object> param) throws Exception;
	public int updateCommonCodeGroup(HashMap<String, Object> param) throws Exception;
	public int deleteCommonCodeGroup(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getCommonCodeList(HashMap<String, Object> param) throws Exception;
	public int insertCommonCodeList(HashMap<String, Object> param) throws Exception;
	public int updateCommonCodeList(HashMap<String, Object> param) throws Exception;
	public int deleteCommonCodeList(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getEmployeeRoleList(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getFileList(HashMap<String, Object> param) throws Exception;
	
	public int insertFileInfo(HashMap<String, Object> param) throws Exception;
	public int deleteFileInfo(int filenumber) throws Exception;
	public HashMap<String, Object> getFileInfo(int filenumber) throws Exception;
	public HashMap<String, Object> getCommonCodeInfo(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getFacCodeList(HashMap<String, Object> param) throws Exception;
	
	/**
	* <pre>
	* 설명 : 플랜 리스트
	* 상세설명 : 작업유형코드에따라 불러온다.
	* </pre>
	 */
	public ArrayList<HashMap<String, Object>> getPlanList(HashMap<String, Object> param) throws Exception;
	public int getPlanTotalRowCount(HashMap<String, Object> param) throws Exception;
	
	/**
	* <pre>
	* 설명 : 플랜 조회
	* 상세설명 : .
	* </pre>
	 */
	public HashMap<String, Object> getPlanInfo(HashMap<String, Object> param) throws Exception;
	
	/**
	* <pre>설명 : 플랜 추가</pre>
	 */
	public int insertPlan(HashMap<String, Object> param) throws Exception;
	/**
	* <pre>설명 : 플랜 수정</pre>
	 */
	public int updatePlan(HashMap<String, Object> param) throws Exception;
	/**
	* <pre>설명 : 플랜 삭제</pre>
	 */
	public int deletePlan(HashMap<String, Object> param) throws Exception;

	
	/**
	* <pre> 설명 : 메뉴 리스트 </pre>
	 */
	public ArrayList<HashMap<String, Object>> getMenuList(HashMap<String, Object> param) throws Exception;
	
	/**
	* <pre>설명 : 메뉴 추가</pre>
	 */
	public int insertMenu(HashMap<String, Object> param) throws Exception;
	/**
	* <pre>설명 : 메뉴 수정</pre>
	 */
	public int updateMenu(HashMap<String, Object> param) throws Exception;
	/**
	* <pre>설명 : 메뉴 삭제</pre>
	 */
	public int deleteMenu(HashMap<String, Object> param) throws Exception;
	/**
	* <pre>설명 : 메뉴 - 메뉴정렬의 최댓값</pre>
	 */
	public int getMaxSort(HashMap<String, Object> param) throws Exception;
	
	
	public ArrayList<HashMap<String, Object>> getAuthGroupList(HashMap<String, Object> param) throws Exception;
	public String getNextGroupCode() throws Exception;
	public ArrayList<HashMap<String, Object>> getAuthMenuList(HashMap<String, Object> param) throws Exception;

	public int saveAuthMenu(HashMap<String, Object> param) throws Exception;
	
	/*	
	public int insertAuthMenu(HashMap<String, Object> param) throws Exception;
	public int deleteAuthMenu(HashMap<String, Object> param) throws Exception;
	*/
	
	public int insertAuthGroup(HashMap<String, Object> param) throws Exception;
	public int updateAuthGroup(HashMap<String, Object> param) throws Exception;
	public int deleteAuthGroup(HashMap<String, Object> param) throws Exception;
	
	//행정동 코드목록  CMT_ADAR_MA
	public ArrayList<HashMap<String, Object>> getHJDCodeList(HashMap<String, Object> param) throws Exception;
	
	//시설사신 목록  M2_FAC_PIC
	public ArrayList<HashMap<String, Object>> getPictureList(HashMap<String, Object> param) throws Exception;
	
	//이력 사진목록  M2_FAC_HIS_PIC
	public ArrayList<HashMap<String, Object>> getHisPictureList(HashMap<String, Object> param) throws Exception;
	
	//출력,로그인 로그
	public int insertSystemLog(HashMap<String, Object> param) throws Exception;

	//로그인 실패카운트 증가
	public int updateLoginFailCnt(HashMap<String,Object>param) throws Exception;
	
	//사용자메뉴 설정
	public int setUserMenu(HashMap<String, Object> param) throws Exception;

	//사용자메뉴 해제
	public int unSetUserMenu(HashMap<String, Object> param) throws Exception;
	
	/*
	* <pre>
	* 설명 : 로그관리
	* 상세설명 : ..
	* </pre>
	 */
	public ArrayList<HashMap<String, Object>> getSystemLog(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getSystemLogExcel(HashMap<String, Object> param) throws Exception;
}
