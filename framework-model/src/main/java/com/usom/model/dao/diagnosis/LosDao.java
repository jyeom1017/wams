package com.usom.model.dao.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface LosDao {
	ArrayList<HashMap<String, Object>> getLOS(HashMap<String, Object> param)throws Exception;
	int updateLOS(HashMap<String, Object> param)throws Exception;
	int insertLOS(HashMap<String, Object> param)throws Exception;
	int insertLOS_EVALUE_ITEM(HashMap<String, Object> param)throws Exception;
	int insertLOS_PARAM_ITEM(HashMap<String, Object> param)throws Exception;
	
	int deleteLOS_EVALUE_ITEM(String param)throws Exception;
	int deleteLOS_PARAM_ITEM(String param)throws Exception;
	
	ArrayList<HashMap<String, Object>> getLOS_WEIGHT()throws Exception;
	ArrayList<HashMap<String, Object>> get_LOS_WEIGHT_DETAIL()throws Exception;
	ArrayList<HashMap<String, Object>> get_LOS_WEIGHT_OPTION(HashMap<String, Object> param)throws Exception;
	ArrayList<HashMap<String, Object>> get_LOS_OPTION_LIST()throws Exception;
	
	int changeCHOICE_N()throws Exception;
	int changeCHOICE_Y(HashMap<String, Object> param)throws Exception;
	int deleteLOS_WEIGHT_DETAIL(HashMap<String, Object> param)throws Exception;
	int insertLOS_WEIGHT_DETAIL(HashMap<String, Object> param)throws Exception;
	ArrayList<HashMap<String, Object>> select_LOS_WEIGHT_DETAIL(HashMap<String, Object> param)throws Exception;
	int deleteLOS_WEIGHT_OPTION(HashMap<String, Object> param)throws Exception;
	int insertLOS_WEIGHT_OPTION(HashMap<String, Object> param)throws Exception;
	int updateLOS_WEIGHT(HashMap<String, Object> param)throws Exception;
	
	ArrayList<HashMap<String, Object>> getLOS_EVALUE_ITEM(HashMap<String, Object> param)throws Exception;
	ArrayList<HashMap<String, Object>> getLOS_PARAM_ITEM(HashMap<String, Object> param)throws Exception;
	
	int deleteLOS_EVALUE(HashMap<String, Object> param)throws Exception;
	int insertLOS_EVALUE(HashMap<String, Object> param)throws Exception;
	
	ArrayList<HashMap<String, Object>> getLOS_EVALUE(HashMap<String, Object> param)throws Exception;
	Object getLOSArray(HashMap<String, Object> param)throws Exception;
	int deleteLOS(HashMap<String, Object> param)throws Exception;
	void insertLOS_WEIGHT_INFO(HashMap<String, Object> infomap)throws Exception;
	void insertLOS_WEIGHT_INFO_N()throws Exception;
}
