package com.usom.model.dao.report;

import java.util.ArrayList;
import java.util.HashMap;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface ReportDao {

	public ArrayList<HashMap<String, Object>> getStatOIP(HashMap<String, Object> param) throws Exception;
}
