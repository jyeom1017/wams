package com.usom.model.dao.basic;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.annotations.Param;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface EmployeeDao {
	
	public int updateUserPassword(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getEmployeeList(HashMap<String, Object> param) throws Exception;
	public int getEmployeeTotalRowCount(HashMap<String, Object> param) throws Exception;
	
	public String checkUsedEmployeeId(@Param("USER_ID") String user_id) throws Exception;
	public HashMap<String, Object> getEmployee(HashMap<String, Object> param) throws Exception;
	public int insertEmployee(HashMap<String, Object> param) throws Exception;
	public int deleteEmployee(HashMap<String, Object> param) throws Exception;
	public int updateEmployeeBasicInfo(HashMap<String, Object> param) throws Exception;

	int deleteEmployeePhoto(HashMap<String, Object> param) throws Exception;
	public int updateEmpLastMenu(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getEmployeeCareerInfoList(HashMap<String, Object> param) throws Exception;
	public int insertEmployeeCareerInfo(HashMap<String, Object> param) throws Exception;
	public int updateEmployeeCareerInfo(HashMap<String, Object> param) throws Exception;
	public int deleteEmployeeCareerInfo(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getEmployeeTechInfoList(HashMap<String, Object> param) throws Exception;
	public int insertEmployeeTechInfo(HashMap<String, Object> param) throws Exception;
	public int updateEmployeeTechInfo(HashMap<String, Object> param) throws Exception;
	public int deleteEmployeeTechInfo(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getEmployeeEduInfoList(HashMap<String, Object> param) throws Exception;
	public int insertEmployeeEduInfo(HashMap<String, Object> param) throws Exception;
	public int updateEmployeeEduInfo(HashMap<String, Object> param) throws Exception;
	public int deleteEmployeeEduInfo(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getEmployeeSelectionList(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getUseEmployeeList(HashMap<String, Object> param) throws Exception;
	public int insertUseEmployee(HashMap<String, Object> param) throws Exception;
	public int deleteUseEmployees(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getEmployeeListExcel(HashMap<String, Object> param) throws Exception;

	public ArrayList<HashMap<String, Object>> getEmployeeClass(HashMap<String, Object> param)throws Exception;
	
	public int createSSOUser(HashMap<String, Object> param) throws Exception;
	
}
