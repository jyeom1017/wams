package com.usom.model.dao.asset;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface AssetDao {

    HashMap<String, Object> getAssetCriteria();

    ArrayList<HashMap<String, Object>> getLevelCodeList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getLevelCodeListExcel(HashMap<String, Object> param);

    int updateLevelCode(HashMap<String, Object> param);

    int insertLevelCode(HashMap<String, Object> param);

    int deleteLevelCode(HashMap<String, Object> param);

    int isAssetClassCdExists(HashMap<String, Object> param);

    int getTotalCount();

    int isLevelCodeUsed(HashMap<String, Object> param);

    //자산기준정보
    ArrayList<HashMap<String, Object>> getAssetBaseInfoList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getAssetBaseInfoExcel(HashMap<String, Object> param);

    HashMap<String, Object> getAssetBaseInfoDetail(HashMap<String, Object> param);

    int updateAssetBaseInfo(HashMap<String, Object> param);

    int insertAssetBaseInfo(HashMap<String, Object> param);

    int deleteAssetBaseInfo(HashMap<String, Object> param);
}
