package com.usom.model.dao.asset;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface ItemDao {

    ArrayList<HashMap<String, Object>> getItemList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getLevelNumListExcel(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getLevelNumList();

    int deleteAssetPath(HashMap<String, Object> param) throws Exception;

    ArrayList<HashMap<String, Object>> selectInfoItemList();
    
    ArrayList<HashMap<String, Object>> getInfoItemList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getCustomItemList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getAllAssetLevelList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getAllInfoItemList();

    ArrayList<HashMap<String, Object>> getAssetMaxLevelList() throws Exception;

    void deleteMaxLevel(HashMap<String, Object> param);
    
    void deleteMaxLevelAll();

    void updateMaxLevelList(HashMap<String, Object> param);

    void saveAssetPath(HashMap<String, Object> param);

    void updateAssetPath(HashMap<String, Object> param);

    int saveNewTemplate(String USER_ID);
    
    void saveNewInfoItems(HashMap<String, Object> param);

    void saveInfoItems(HashMap<String, Object> param);
    
    int deleteInfoItems(HashMap<String, Object> param);

    void saveCustomItems(HashMap<String, Object> param);

    void deleteCustomItems(String ASSET_PATH_SID);

    void updateCustomItems(HashMap<String, Object> param);

    ArrayList<HashMap<String,Object >> selectAssetLevelPathList(HashMap<String,Object > param);
    
    int selectAssetItemListCnt(HashMap<String,Object > param);
    
    ArrayList<HashMap<String, Object>> selectAssetItemList(HashMap<String,Object > param);
    
    ArrayList<HashMap<String, Object>> selectAssetItemListExcel(HashMap<String,Object > param);
    
    ArrayList<HashMap<String,Object >> selectAssetItemInfo(HashMap<String,Object > param);
    
    ArrayList<HashMap<String,Object >> getAssetLevelList();
    
    int getCurrentAssetSid();
    
    int insertAsset(HashMap param);
    
    int updateAsset(HashMap param);
    
    int deleteAsset(HashMap<String,Object > param);
    
    int insertAssetInfoItem(HashMap<String,Object > param);
    
    //int updateAssetInfoItem(HashMap param);
    
    int deleteAssetInfoItem(HashMap<String,Object > param);
    
    int insertAssetInfoCurstomItem(HashMap<String,Object > param);
    
    //int updateAssetInfoCurstomItem(HashMap param);
    
    int deleteAssetInfoCurstomItem(HashMap<String,Object > param); 
    
    int updateUseYnAsset(HashMap<String,Object > param);
    
    int updateUselessAsset(HashMap<String,Object > param);
    
    String getAssetCd(int AssetSid);

    int isAssetPathExists(HashMap<String, Object> param);

    int getTotalCount();
    
    ArrayList<HashMap<String, Object>> getAssetPicture(HashMap<String, Object> param);
    
    int updateAssetPicture(HashMap<String, Object> param);
    
    ArrayList<HashMap<String,Object >> getAssetHistory(HashMap<String,Object > param);

    int isAssetPathUsed(HashMap<String, Object> param);
    
    HashMap<String,Object > getAssetGisInfo(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getAssetFromGisInfo(HashMap<String, Object> param);
    
    int updateAssetGis(HashMap<String, Object> param);
    
    int insertAssetGis(HashMap<String, Object> param);
    
    int deleteAssetGis(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> findAssetList(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getStatAsset(HashMap<String, Object> param);
    
    int updateAssetInitCost(HashMap<String, Object> param);
}
