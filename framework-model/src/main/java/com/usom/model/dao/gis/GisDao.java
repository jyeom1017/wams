package com.usom.model.dao.gis;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Resource(name="tiberoSessionFactory")
public interface GisDao {

	List<Map<String, Object>> getPipeInfoListFromFtrIdn(HashMap<String, Object> param);
	
    HashMap<String, Object> getAssetInfoWithGisInfo(HashMap<String, Object> param);

    HashMap<String, Object> getAssetInfoWithLayer(HashMap<String, Object> param);

    List<Map<String, Object>> getAssetItemInfo(Map<String, Object> assetInfo);

    int insertAssetGis(Map<String, Object> param);
    
    int insertAssetGis1(Map<String, Object> param);

    HashMap<String, Object> getAssetInfo(HashMap<String, Object> paramMap);

    List<Map<String, Object>> getAssetInfoWithGisList(Map<String, Object> param);

    List<Map<String, Object>> getGISInfoByAssetPath(Map<String, Object> param);

    ArrayList<HashMap<String, Object>> getDiagnosticAreaList(HashMap<String, Object> param);

    int deleteDiagnosticArea(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getDiagnosticAreaDetailNewList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getDiagnosticAreaDetailEditList(HashMap<String, Object> param);

    int deleteDiagnosticAreaAsset(HashMap<String, Object> param);

    void setDiagnosticArea(HashMap<String, Object> param);

    void deleteAllDiagnosticAreaAsset(HashMap<String, Object> param);

    int insertDiagnosticAreaDetail(List<Map<String, Object>> param);

    ArrayList<HashMap<String, Object>> getStateDiagnosisGroupList(HashMap<String, Object> param);

    int deleteStateDiagnosisGroup(HashMap<String, Object> param);

    Map<String, Object> getStateDiagnosticGroupDetailPopupInfo(Map<String, Object> param);

    ArrayList<HashMap<String, Object>> getStateDiagnosisGroupDetailNewList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getStateDiagnosisGroupDetailEditList(HashMap<String, Object> param);

    int saveStateDiagGroup(HashMap<String, Object> param);

    int insertStateDiagGroupAsset(HashMap<String, Object> param);

    int deleteStateDiagGroupAsset(HashMap<String, Object> param);

    List<Map<String, Object>> getAssetsByAssetPath(Map<String, Object> map);

    List<Map<String, Object>> getAssetsByGISProperty(Map<String, Object> map);


    List<Map<String, Object>> getAssetByAssetCode(Map<String, Object> param);
    
    //단차분석
    ArrayList<HashMap<String, Object>> getElevationDifferenceAnalysis(HashMap<String, Object> param);
    
    int insertTempLocationsResponse(HashMap<String, Object> param);
    
    HashMap<String, Object> selectTempLocationsResponse(HashMap<String, Object> param);

    List<Map<String, Object>> getAssetInformationItemWithFtrIdn(Map<String, Object> param);

    int updateAssetInfoItem1(List<Map<String, Object>> param) throws Exception;
    
    int updateAssetInfoItem(ArrayList<Map<String, Object>> param) throws Exception;

    int under42(Map<String, Object> param);
    
    HashMap<String,Object> getAssetPathGisLayerNm(HashMap<String, Object> param);
}