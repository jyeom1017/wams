package com.usom.model.dao.Board;

import java.util.HashMap;
import java.util.List;

import com.usom.model.vo.boardvo.BoardVO;

public interface BoardDAO {
	//자산목록 엑셀 다운로드 테스트 
	List<BoardVO> selectExcelList(HashMap<String,Object> param) throws Exception;
	//자산목록 엑셀 다운로드 테스트 
	List<HashMap<String,Object>> selectExcelList2(HashMap<String,Object> param) throws Exception;

	//잔존수명 보고서 
	/*List<HashMap<String,Object>> selectExcelList2(HashMap<String,Object> param) throws Exception;*/
	
}