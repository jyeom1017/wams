package com.usom.model.dao.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface LifeDao {
	public ArrayList<HashMap<String, Object>> getLIFE_SPEC();
	public ArrayList<HashMap<String, Object>> getLIFE_SPEC_CAP1(HashMap<String, Object> param);
	public HashMap<String, Object> getLIFE_SPEC_CAP2(HashMap<String, Object> param);
	public int insertLIFE_SPEC_CAP1(HashMap<String, Object> param) throws Exception;
	public int insertLIFE_SPEC_CAP2(HashMap<String, Object> param) throws Exception;
	public int deleteLIFE_SPEC_ITEM(HashMap<String, Object> param) throws Exception;
	public int insertLIFE(HashMap<String, Object> param) throws Exception;
	public int updateLIFE_ASSET(HashMap<String, Object> param)throws Exception;
	public int updateLIFE(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_SPEC_ITEM()throws Exception;
	public HashMap<String, Object> getLIFE_ASSET_VARIATION(HashMap<String, Object> param)throws Exception;
	public int continueLIFE_ASSET(HashMap<String, Object> param)throws Exception;
	public int saveLIFE_ASSET(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> outputLIFE_ASSET(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getFN_LIST(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_RESULT(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE_MODIFY(HashMap<String, Object> param)throws Exception;
	public int getLIFE_YCOL()throws Exception;
	public HashMap<String, Object> saveLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET(HashMap<String, Object> param)throws Exception;
	public void insertAsset(HashMap<String, Object> param)throws Exception;
	public int insertLIFE_SPEC(HashMap<String, Object> map)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE(HashMap<String, Object> param)throws Exception;
	public int updateLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception;
	public int deleteLIFE_ASSET_STATUS_GRADE_MODIFY_TEMP_YN()throws Exception;
	public int resetLIFE_ASSET_STATUS_GRADE_TEMP_RESULT(HashMap<String, Object> param)throws Exception;
	public int saveLIFE_ASSET_STATUS_GRADE_MODIFY(HashMap<String, Object> param)throws Exception;
	public HashMap<String, Object> getUPDATE_ID()throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_GRAPH(HashMap<String, Object> param)throws Exception;
	public int deleteLIFE(HashMap<String, Object> param)throws Exception;
	public int OnFileSelectLIFE(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_REPORT(HashMap<String, Object> param)throws Exception;
	public int getTotalCount()throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE_REPORT(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_REPORT_FORM(HashMap<String, Object> param)throws Exception;
	public void deleteLIFE_ASSET(HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getLIFE_ASSET_FROM(HashMap<String, Object> param)throws Exception;
	public int refreshLIFE_ASSET_STATUS_GRADE(HashMap<String, Object> param)throws Exception;
}
