package com.usom.model.dao.user;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.usom.model.vo.user.MenuVO;
import com.usom.model.vo.user.User;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface UserDetailsServiceDAO {
	
	public User findByUserId(@Param("USER_ID") String USER_ID);
	public List<MenuVO> getAuthMenuList(MenuVO menuVO) throws Exception;
	
}
