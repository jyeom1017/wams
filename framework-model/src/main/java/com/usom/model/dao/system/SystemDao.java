package com.usom.model.dao.system;

import java.util.ArrayList;
import java.util.HashMap;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface SystemDao {
	
	public ArrayList<HashMap<String, Object>> getEquipMasterList(HashMap<String, Object> param) throws Exception;
	public int insertEquipMaster(HashMap<String, Object> param) throws Exception;
	public int updateEquipMaster(HashMap<String, Object> param) throws Exception;
	public int deleteEquipMaster(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getEquipSpecList(HashMap<String, Object> param) throws Exception;
	public int insertEquipSpec(HashMap<String, Object> param) throws Exception;
	public int updateEquipSpec(HashMap<String, Object> param) throws Exception;
	public int deleteEquipSpec(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getEquipHistList(HashMap<String, Object> param) throws Exception;
	public int insertEquipHist(HashMap<String, Object> param) throws Exception;
	public int updateEquipHist(HashMap<String, Object> param) throws Exception;
	public int deleteEquipHist(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getEquipMasterPopList(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getSewerageStatus(HashMap<String, Object> param) throws Exception;
	
}
