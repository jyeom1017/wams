package com.usom.model.dao.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface ReevalDao {

	ArrayList<HashMap<String, Object>> getRE_EST(HashMap<String, Object> param)throws Exception;
	
	int insertRE_EST(HashMap<String, Object> param)throws Exception;
	
	ArrayList<HashMap<String, Object>> getRE_EST_ASSET_GROUP(HashMap<String, Object> param)throws Exception;
	
	ArrayList<HashMap<String, Object>> getRE_EST_ASSET(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getPRICE_INDEX()throws Exception;
	
	HashMap<String, Object> getPRICE_INDEX_BASIC()throws Exception;
	
	int insertPRICE_INDEX(HashMap<String, Object> param)throws Exception;
	
	int updatePRICE_INDEX_BASIC(HashMap<String, Object> param)throws Exception;
	
	int deletePRICE_INDEX()throws Exception;
	
	ArrayList<HashMap<String, Object>> getWEIGHT_SPEC()throws Exception;
	
	ArrayList<HashMap<String, Object>> getWEIGHT_SPEC_ITEM(HashMap<String, Object> param)throws Exception;
	
	int insertWEIGHT_SPEC(HashMap<String, Object> param)throws Exception;
	
	int insertWEIGHT_SPEC_ITEM(HashMap<String, Object> param)throws Exception;

	int insertRE_EST_ASSET(HashMap<String, Object> param)throws Exception;

	int deleteRE_EST_ASSET(HashMap<String, Object> param)throws Exception;

	int deleteRE_EST(HashMap<String, Object> param)throws Exception;

	int updateRE_EST(HashMap<String, Object> param)throws Exception;

	int updateRE_EST_ASSET(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getRE_EST_ASSET_REPORT(HashMap<String, Object> param)throws Exception;

	int insertRE_EST_ASSET_import(HashMap<String, Object> param)throws Exception;
	
	
}
