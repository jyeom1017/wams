package com.usom.model.dao.test;

import java.util.ArrayList;
import java.util.HashMap;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface TestDao {
	
	public ArrayList<HashMap<String, Object>> getJqgridTestList(HashMap<String, Object> param) throws Exception;
	public int getJqgridTestTotalRowCount(HashMap<String, Object> param) throws Exception;
}
