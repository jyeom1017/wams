package com.usom.model.dao.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface LccDao {
	ArrayList<HashMap<String, Object>> getLCC(HashMap<String, Object> param)throws Exception;
	ArrayList<HashMap<String, Object>> getLCC_ASSET_RESULT(HashMap<String, Object> param)throws Exception;
	HashMap<String, Object> getLCC_ASSET_RESULT_TOTAL(HashMap<String, Object> param)throws Exception;
	ArrayList<HashMap<String, Object>> getLCC_ASSET_RESULT_DETAILS(HashMap<String, Object> param)throws Exception;
	HashMap<String, Object> getLCC_ASSET_RESULT_DETAILS_SUM(HashMap<String, Object> param)throws Exception;
	ArrayList<HashMap<String, Object>> getLCC_RESULT(HashMap<String, Object> param)throws Exception;
	HashMap<String, Object> getLCC_RESULT_SUM(HashMap<String, Object> param)throws Exception;
	int insertLCC(HashMap<String, Object> param)throws Exception;
	int insertLCC_ASSET(HashMap<String, Object> param)throws Exception;
	int insertLCC_ASSET_RESULT(HashMap<String, Object> param)throws Exception;
	int insertLCC_RESULT_F(HashMap<String, Object> param)throws Exception;
	int insertLCC_RESULT_N(HashMap<String, Object> param)throws Exception;
	int deleteLCC(HashMap<String, Object> param)throws Exception;
	int deleteLCC_ASSET(HashMap<String, Object> param)throws Exception;
	int deleteLCC_ASSET_RESULT_ALL(HashMap<String, Object> param)throws Exception;
	int deleteLCC_ASSET_RESULT(HashMap<String, Object> param)throws Exception;
	int deleteLCC_RESULT(HashMap<String, Object> param);
	int updateLCC(HashMap<String, Object> param)throws Exception;
	
	ArrayList<HashMap<String, Object>> reportFN(HashMap<String, Object> param) throws Exception;
	ArrayList<HashMap<String, Object>> reportASSET(HashMap<String, Object> param) throws Exception;
	HashMap<String, Object> reportASSET_TOTAL(HashMap<String, Object> param) throws Exception;
	ArrayList<HashMap<String, Object>> getLCC_ASSET_RESULT_DETAILS_MULTI(HashMap<String, Object> param) throws Exception;
	int insertLCC_ASSET_RESULT_FN(HashMap<String, Object> param);
	int updateLCC_RESULT(HashMap<String, Object> param)throws Exception;
}
