package com.usom.model.dao.operation;

import java.util.ArrayList;
import java.util.HashMap;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface New_OperationDao {

	void insertREPAIR(HashMap<String, Object> param)throws Exception;

	int updateREPAIR(HashMap<String, Object> param)throws Exception;

	int insertREPAIR_ASSET(HashMap<String, Object> param)throws Exception;

	int insertREPAIR_FILE(HashMap<String, Object> map)throws Exception;

	int deleteREPAIR_FILE(HashMap<String, Object> map)throws Exception;

	int deleteREPAIR_ASSET(HashMap<String, Object> map)throws Exception;

	ArrayList<HashMap<String, Object>> getREPAIR_FILE(HashMap<String, Object> param)throws Exception;

	HashMap<String, Object> getREPAIR_INFO_NM(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getREPAIR_ASSET_NM(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getREPAIR(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getREPAIR_ASSET(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getFormatTYPE()throws Exception;

	int updateREPAIR_DEL_YN(HashMap<String, Object> param)throws Exception;

	int updateREPAIR_FILE(HashMap<String, Object> param)throws Exception;
	
	// 조사 및 탐사
	void insertINSPECT(HashMap<String, Object> param)throws Exception;

	int updateINSPECT(HashMap<String, Object> param)throws Exception;

	int insertINSPECT_ASSET(HashMap<String, Object> param)throws Exception;

	int insertINSPECT_FILE(HashMap<String, Object> map)throws Exception;

	int deleteINSPECT_FILE(HashMap<String, Object> map)throws Exception;

	int deleteINSPECT_ASSET(HashMap<String, Object> map)throws Exception;

	ArrayList<HashMap<String, Object>> getINSPECT_FILE(HashMap<String, Object> param)throws Exception;

	HashMap<String, Object> getINSPECT_INFO_NM(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getINSPECT_ASSET_NM(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getINSPECT(HashMap<String, Object> param)throws Exception;

	ArrayList<HashMap<String, Object>> getINSPECT_ASSET(HashMap<String, Object> param)throws Exception;

	int updateINSPECT_DEL_YN(HashMap<String, Object> param)throws Exception;

	int updateINSPECT_FILE(HashMap<String, Object> param)throws Exception;

}
