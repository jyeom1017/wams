package com.usom.model.dao.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.annotations.Param;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface CommonDao {

	public ArrayList<HashMap<String, Object>> getBlockCodeList(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getCommonCodeList(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getFacCodeList(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getCommonCodeGroup (HashMap<String, Object> param) throws Exception;
	public int insertCommonCodeGroup(HashMap<String, Object> param) throws Exception;
	public int updateCommonCodeGroup(HashMap<String, Object> param) throws Exception;
	public int deleteCommonCodeGroup(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getCommonCodeInfo(HashMap<String, Object> param) throws Exception;
	public int insertCommonCodeList(HashMap<String, Object> param) throws Exception;
	public int updateCommonCodeList(HashMap<String, Object> param) throws Exception;
	public int deleteCommonCodeList(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getEmployeeRoleList(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getFileList(HashMap<String, Object> param) throws Exception;
	public int insertFileInfo(HashMap<String, Object> param) throws Exception;
	public int deleteFileInfo(@Param("filenumber") int filenumber) throws Exception;
	public HashMap<String, Object> getFileInfo(@Param("filenumber") int filenumber) throws Exception;
	
	//일정마스터 리스트
	public ArrayList<HashMap<String, Object>> getPlanList(HashMap<String, Object> param) throws Exception;
	public int getPlanTotalRowCount(HashMap<String, Object> param) throws Exception;
	
	
	public HashMap<String, Object> getPlanInfo(HashMap<String, Object> param) throws Exception;
	
	//플랜 - 추가
	public int insertPlan(HashMap<String, Object> param) throws Exception;
	//플랜 - 수정
	public int updatePlan(HashMap<String, Object> param) throws Exception;
	//플랜 - 삭제
	public int deletePlan(HashMap<String, Object> param) throws Exception;

	
	//메뉴 리스트
	public ArrayList<HashMap<String, Object>> getMenuList(HashMap<String, Object> param) throws Exception;
	//메뉴 - 추가
	public int insertMenu(HashMap<String, Object> param) throws Exception;
	//메뉴 - 수정
	public int updateMenu(HashMap<String, Object> param) throws Exception;
	//메뉴 - 삭제
	public int deleteMenu(HashMap<String, Object> param) throws Exception;
	//메뉴 - 메뉴정렬의 최대값
	public int getMaxSort(HashMap<String, Object> param) throws Exception;
	
	
	
	//AUTH GROUP 리스트
	public ArrayList<HashMap<String, Object>> getAuthGroupList(HashMap<String, Object> param) throws Exception;

	public String getNextGroupCode() throws Exception;
	
	//Auth Group Menu 설정값 조회
	public ArrayList<HashMap<String, Object>> getAuthMenuList(HashMap<String, Object> param) throws Exception;
	
	//Auth Group Menu 설정값 저장
	public int insertAuthMenu(HashMap<String, Object> param) throws Exception;

	//Auth Group Menu 설정값 삭제
	public int deleteAuthMenu(HashMap<String, Object> param) throws Exception;
	

	//AuthGroup - 추가
	public int insertAuthGroup (HashMap<String, Object> param) throws Exception;
	//AuthGroup - 수정
	public int updateAuthGroup (HashMap<String, Object> param) throws Exception;
	//AuthGroup - 삭제
	public int deleteAuthGroup (HashMap<String, Object> param) throws Exception;
	
	//행정동 코드목록  CMT_ADAR_MA
	public ArrayList<HashMap<String, Object>> getHJDCodeList(HashMap<String, Object> param) throws Exception;

	//시설사신 목록  M2_FAC_PIC
	public ArrayList<HashMap<String, Object>> getPictureList(HashMap<String, Object> param) throws Exception;
	
	//이력 사진목록  M2_FAC_HIS_PIC
	public ArrayList<HashMap<String, Object>> getHisPictureList(HashMap<String, Object> param) throws Exception;
	
	//출력,로그인 로그
	public int insertSystemLog(HashMap<String, Object> param) throws Exception;
	
	//로그인 실패 카운트 증가
	public int updateLoginFailCnt(HashMap<String,Object>param) throws Exception;
	
	//사용자 메뉴 추가
	public int insertUserMenu(HashMap<String, Object> param) throws Exception;
	
	//사용자 메뉴 삭제
	public int deleteUserMenu(HashMap<String, Object> param) throws Exception;
	
	//사용자 메뉴 확인
	public int checkUserMenu(HashMap<String, Object> param) throws Exception;
	
	// 로그 리스트 가져오기
	public ArrayList<HashMap<String, Object>> getSystemLog(HashMap<String, Object> param);
	public ArrayList<HashMap<String, Object>> getSystemLogExcel(HashMap<String, Object> param);
}
