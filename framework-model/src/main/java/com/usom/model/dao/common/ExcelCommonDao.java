package com.usom.model.dao.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.annotations.Param;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface ExcelCommonDao {

	public ArrayList<HashMap<String, Object>> getList(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getInfo(HashMap<String, Object> param) throws Exception;
	
	public int getInt(HashMap<String, Object> param) throws Exception;
	
	public String getString(HashMap<String, Object> param) throws Exception;	
	
	
	public ArrayList<HashMap<String, Object>> getList_010203(HashMap<String, Object> param) throws Exception;
	
	public HashMap<String, Object> getInfo_010203(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getPic_010203(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getList_010202(HashMap<String, Object> param) throws Exception;
	
	public HashMap<String, Object> getInfo_010202(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getPic_010202(HashMap<String, Object> param) throws Exception;

	public ArrayList<HashMap<String, Object>> getList1_010102(HashMap<String, Object> param) throws Exception;

	public ArrayList<HashMap<String, Object>> getList2_010102(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getInfo_010102(HashMap<String, Object> param) throws Exception;

	public ArrayList<HashMap<String, Object>> getPic_010102(HashMap<String, Object> param) throws Exception;	
	
	public ArrayList<HashMap<String, Object>> getList_010205(HashMap<String, Object> param) throws Exception;
	
	public HashMap<String, Object> getInfo_010205(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getPic_010205(HashMap<String, Object> param) throws Exception;

	//public ArrayList<HashMap<String, Object>> getList1_010102(HashMap<String, Object> param) throws Exception;

	//public ArrayList<HashMap<String, Object>> getList2_010102(HashMap<String, Object> param) throws Exception;

	//public HashMap<String, Object> getInfo_010102(HashMap<String, Object> param) throws Exception;

	//public ArrayList<HashMap<String, Object>> getPic_010102(HashMap<String, Object> param) throws Exception;	
	
	public ArrayList<HashMap<String, Object>> getBBSList_Type1(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getBBSList_Type2(HashMap<String, Object> param) throws Exception;
	
	public ArrayList<HashMap<String, Object>> getBBSList_Type3(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getInfo_010306(HashMap<String, Object> param) throws Exception;

	public ArrayList<HashMap<String, Object>> getList_010306(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getTotal_010306(HashMap<String, Object> param) throws Exception;

	public ArrayList<HashMap<String, Object>> getFN_010306_2(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getFN_TOTAL_010306_2(HashMap<String, Object> param) throws Exception;

	public ArrayList<HashMap<String, Object>> getASSET_010306_2(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getASSET_TOTAL_010306_2(HashMap<String, Object> param) throws Exception;

	
	public String[] getList1_010402_1_KEY(HashMap<String, Object> param) throws Exception;
	public String[] getList1_010402_2_KEY(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getList1_010402_1(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getList1_010402_2(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getList1_010402_3(HashMap<String, Object> param) throws Exception;
	public ArrayList<HashMap<String, Object>> getList1_010402_4(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getList1_010402_0(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getInfo_010305 (HashMap<String, Object> param)throws Exception;
	public ArrayList<HashMap<String, Object>> getList1_010305(HashMap<String, Object> param)throws Exception;

	public ArrayList<HashMap<String, Object>> getBig_010303(HashMap<String, Object> param)throws Exception;

	public HashMap<String, Object> getBig_Info_010303(HashMap<String, Object> param)throws Exception;

	public HashMap<String, Object> getBig_Info_010307(HashMap<String, Object> param)throws Exception;

	public ArrayList<HashMap<String, Object>> getBig_010307(HashMap<String, Object> param)throws Exception;

	public HashMap<String, Object> getBig_Info_010401(HashMap<String, Object> param)throws Exception;

	public ArrayList<HashMap<String, Object>> getBig_010401(HashMap<String, Object> param)throws Exception;

	public ArrayList<HashMap<String, Object>> getList2_010306(HashMap<String, Object> param)throws Exception;

	public HashMap<String, Object> getInfo2_010306(HashMap<String, Object> param)throws Exception;
	
}
