package com.usom.model.dao.finance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
@Resource(name="tiberoSessionFactory")
public interface FinanceAnalysisDao {

    ArrayList<HashMap<String, Object>> getFinancialPlanList(HashMap<String, Object> param);

    String[] getFinancialPlanYearArray(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getFinancialPlanDetailList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getFinancialStatementsList(HashMap<String, Object> param);

    int setFinancialStatement(List<Map<String, Object>> list);

    ArrayList<HashMap<String, Object>> getIncomeStatement(HashMap<String, Object> param);

    int setIncomeStatement(List<Map<String, Object>> list);

    ArrayList<HashMap<String, Object>> getCashFlowStatement(HashMap<String, Object> param);

    int setCashFlowStatement(List<Map<String, Object>> list);

    String[] getOtherCostManagementArray(HashMap<String, Object> param);

    String[] get30YearsSinceTheDateOfAnalysis(Map<String, Object> param);
    
    String[] getComCodeYearList2(Map<String, Object> param);

    ArrayList<HashMap<String, Object>> getOtherCostManagement(HashMap<String, Object> param);

    int setOtherCostManagement(List<Map<String, Object>> list);

    int deleteFinancialPlan(HashMap<String, Object> param);

    int insertTempFinancialPlan(Map<String, Object> param);

    int updateFinancialPlan(HashMap<String, Object> param);

    //급수수익 팝업
    List<Map<String, Object>> getIncomeWaterFN02PopupList(HashMap<String, Object> param);
    
    List<Map<String, Object>> getOperationAndManagementExpensesPopupList(HashMap<String, Object> param);

    List<Map<String, Object>> getDepreciationPopupList(HashMap<String, Object> param);

    int setFinancialPlanDetail(List<Map<String, Object>> list);
    
    int setFinancialPlanLoan(List<Map<String, Object>> list);

    List<Map<String, Object>> getUnitPriceSettingInfo(HashMap<String, Object> param);

    List<Map<String, Object>> getInflationRateOverTheLastFiveYearsInfo(HashMap<String, Object> param);

    List<Map<String, Object>> getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo(HashMap<String, Object> param);

    List<Map<String, Object>> getFinancialAnalysisChargeUnitPrice(HashMap<String, Object> param);

    int setFinancialAnalysisChargeUnitPrice(Map<String, Object> param);

    int updateBeings(Map<String, Object> hashMap);

    ArrayList<HashMap<String, Object>> getAssetManagementPlanList(HashMap<String, Object> param);

    ArrayList<HashMap<String, Object>> getAssetManagementPlanDetailList(HashMap<String, Object> param);

    int deleteAssetManagementPlan(HashMap<String, Object> param);

    int insertAssetManagementPlan(HashMap<String, Object> param);

    int deleteAssetManagementPlanDetail(HashMap<String, Object> param);

    int insertAssetManagementPlanDetail(HashMap<String, Object> param);

    int updateAssetManagementPlan(HashMap<String, Object> param);

    int updateAssetManagementPlanDetail(HashMap<String, Object> param);

    int getLastAmpSid();
    
    HashMap<String, Object> getFinacePriceRaiseRatio(HashMap<String, Object> param); 
    
    int addComCodeListYr2(HashMap<String, Object> param);
    
    int addLoanItemYear(HashMap<String, Object> param);
    
    int deleteFinancialPlanDetailList(HashMap<String, Object> param);
    
    int initFinancialPlanDetailList(HashMap<String, Object> param);
    
    ArrayList<HashMap<String, Object>> getLoanInfo(HashMap<String, Object> param);
    
}