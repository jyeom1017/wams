package com.framework.model.exception;

public class ApplicationException extends java.lang.Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String errMsg;
	
	public ApplicationException(String errMsg) {
		this.errMsg = errMsg;
	}
	@Override
	public String getMessage() {
		return this.errMsg;
	}
}
