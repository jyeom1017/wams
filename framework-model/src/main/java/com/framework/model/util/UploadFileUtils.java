package com.framework.model.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;

import com.usom.model.config.Configuration;

public class UploadFileUtils {

  private static final Logger logger = 
      LoggerFactory.getLogger(UploadFileUtils.class);

//  public static String uploadFile(String uploadPath, 
//      String originalName, 
//      byte[] fileData)throws Exception{
//    
//    return null;
//  }
//  

  
  public static String uploadFile(String uploadPath, 
                              String originalName, 
                              byte[] fileData)throws Exception{
	  System.out.println("**************************************************");	
	  System.out.println("UploadFileUtils /uploadFile 으로 들어오셨네요 ");	
	  System.out.println("**************************************************");
    
	  System.out.println("uploadPath :"+uploadPath);
	  System.out.println("originalName :"+originalName);
	  System.out.println("fileData :"+fileData);
	  
	  
    UUID uid = UUID.randomUUID();  //랜덤 만들어준다.
    
    String savedName = uid.toString() +"_"+originalName;  //폴더에 저장할 이름 만들기 
    
    String savedPath = makeDir(uploadPath);  //밑에 있는 makeDir 을 호출한다.  >>  폴더 만들기
    
    File target = new File(savedPath,savedName);  
    
    FileCopyUtils.copy(fileData, target);   //원본 파일을 저장한다. //java.io.FileNotFoundException 일결우 루트에 파일이 존재하는지 따라가본다.
    
    String formatName = originalName.substring(originalName.lastIndexOf(".")+1); //원본파일의 확장를 의미한다. 이미지일 경우 image/jpeg
    
    String uploadedFileName = null;
    
    
    if(MediaUtils.getMediaType(formatName) != null){                         
      uploadedFileName = makeThumbnail(uploadPath, savedPath, savedName); //썸네일 만든다.
    }else{
      uploadedFileName = makeIcon(uploadPath, savedPath, savedName);  //아니면 그냥 폴더로..
    }
    
    return uploadedFileName;
    
  }
  
  private static  String makeIcon(String uploadPath,     //그냥 피일일 경우 
      String path, 
      String fileName)throws Exception{

    String iconName = 
       path + File.separator+ fileName;
    
    return iconName.substring(
        uploadPath.length()).replace(File.separatorChar, '/');
  }
  
  
  private static  String makeThumbnail( //썸네일 생성
              String uploadPath, 
              String path, 
              String fileName)throws Exception{
	  
	  System.out.println("uploadPath :"+uploadPath);
	  System.out.println("path :"+path);
	  System.out.println("fileName :"+fileName);
	  
    BufferedImage sourceImg = 
        //ImageIO.read(new File(uploadPath + path, fileName));
    		ImageIO.read(new File(path, fileName));
    
    BufferedImage destImg = 
        Scalr.resize(sourceImg, 
            Scalr.Method.AUTOMATIC, 
            Scalr.Mode.FIT_TO_HEIGHT,100);
    
    String thumbnailName = 
         path + File.separator +"thumbnail_"+ fileName;
    
    File newFile = new File(thumbnailName);
    String formatName = 
        fileName.substring(fileName.lastIndexOf(".")+1);
    
    
    ImageIO.write(destImg, formatName.toUpperCase(), newFile);
    return thumbnailName.substring(
        uploadPath.length()).replace(File.separatorChar, '/');
  } 
  
  private static String makeDir(String uploadPath){ //폴더 생성

	  String path= Configuration.UPLOAD_FILE_PATH + "img/" +uploadPath;
    
    
	  System.out.println("uploadPath :"+path);
      
      File dirPath = new File(path);
      
      if(! dirPath.exists() ){
        dirPath.mkdir();
      } 
      
      return path;
    
  }
  
}