package com.framework.model.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.framework.model.util.CommonUtil;

@SuppressWarnings("serial")
public class DataSet implements Serializable, Cloneable {

    class SortObject implements Comparable<Object> {
        int index;
        Object obj;
        SortObject(int index, Object obj) {
            this.index = index;
            this.obj = obj;
        }
        @SuppressWarnings("unchecked")
        public int compareTo(Object o) {
            SortObject other = (SortObject) o;
            if(other.obj == null) return -1;
            else if(this.obj == null) return 1;
            return ((Comparable<Object>) this.obj).compareTo(other.obj);
        }
        public String toString() {
            StringBuffer sb = new StringBuffer()
            .append(index).append(", ").append(obj);
            return sb.toString();
        }
    }

    /**
     * sort method의 오름차순 정렬 지정자
     *
     * @see #sort
     */
    public final static int ASC = 1;
    
    /**
     * DataSet 내에서의 최대 행의 수
     *
     * @see #sort
     */
    public final static int DATASET_MAX_ROW = 200000;

    /**
     * sort method의 내림차순 정렬 지정자
     *
     * @see #sort
     */
    public final static int DESC = -1;
    
    private static Logger logger;

    /**
     * Count of append action
     */
    int appendCount = 0;

    protected int columnCount;

    protected String columnNames[];

    protected ArrayList<Object> data;

    /**
     * 현재 커서의 위치
     */
    protected int row = -1;

    /**
     * <code>DataSet</code> 생성자 입니다.
     */
    public DataSet() {
        super();
    }

    /**
     * <code>DataSet</code> 생성자 입니다.
     * DataSet에 설정할 Colum명들을 배열로 넘겨줍니다.
     * @param columNames DataSet에 설정할 Colum명 String 배열
     * @exception Exception DataSet 생성 실패했을 경우.
     */
    public DataSet(String[] columNames) throws Exception{
        this(columNames, new ArrayList<Object>());
    }

    /**
     * <code>DataSet</code> 생성자 입니다.
     * DataSet에 설정할 Colum명 배열과 Colum값 배열을 담고있는 ArryList를 인자값으로 넘겨줍니다.
     * @param columNames DataSet에 설정할 Colum명 String 배열
     * @param columValueList DataSet에 설정할 Colum값 String 배열을 담고있는 ArryList
     * @exception Exception DataSet 생성 실패했을 경우.
     */
    public DataSet(String[] columNames, ArrayList<Object> columValueList) throws Exception{
    	logger= LoggerFactory.getLogger(this.getClass());
        if(columNames == null) {
            throw new Exception("DataSet 생성 오류입니다. - column name이 null입니다.");
        }
        this.columnNames = columNames;
        this.data = columValueList;
        this.columnCount = this.columnNames.length;
    }

    /**
     * 커서를 절대 위치로 이동합니다.
     * @param  index  절대적인 위치. 1부터 시작합니다.
     * @return  true - 올바른 커서위치, false - 잘 못된 커서 위치
     */
    public boolean absolute(int index) {
        index -= 1;
        if(data == null) return false;
        try {
            if((index >= data.size()) ||
                (index < 0)) return false;
            else {
                row = index;

                return true;
            }
        } catch(Exception e) {
            return false;
        }
    }

    /**
     * 현재 DataSet에 컬럼을 추가하고 각 row마다 해당 컬럼에 대한 디폴트값을 셋팅합니다.
     * 추가되는 컬럼명은 null 또는 공백일 수 없고, 기존 컬럼명과 중복될 수 없습니다.
     *
     * @param columnName 컬럼명
     * @param defaultValue 디폴트값
     */
    public void addColumn(String columnName, Object defaultValue) throws Exception {

        if(columnName==null || (columnName = columnName.trim()).length()<=0) {
            throw new Exception("Column명은 null이거나 공백일 수 없습니다. ===>["+columnName+"]");
        }
        int index = findColumn(columnName);
        if(index != -1) {
            throw new Exception("동일한 Column명이 존재합니다. ===>[" + columnName +"]");
        }
        synchronized(this) {
            int rowCount = this.getRowCount();
            //1. 컬럼 사이즈를 늘이고 컬럼명을 추가한다.
            this.columnCount = columnCount+1;
            String[] newColumnNames = new String[columnCount];
            System.arraycopy(columnNames, 0, newColumnNames, 0, columnCount-1);
            newColumnNames[columnCount-1] = columnName;
            this.columnNames = newColumnNames;
            //2. 각각의 row 데이터에 대해서 추가된 컬럼에 대한 디폴트값을 추가한다.
            //(rowCount가 0보다 큰 경우에만)
            Object[] record = null;
            Object[] newRecord = null;
            for(int i = 0; i<rowCount; i++) {
                record = (Object[])data.get(i);
                if(record==null) continue;
                newRecord  = new Object[columnCount];
                System.arraycopy(record, 0, newRecord, 0, columnCount-1);
                newRecord[columnCount-1] = defaultValue;
                data.set(i, newRecord);
                record = null;
            }
        }
    }

    /**
     * 2006. 07. 19 김성균
     * DataSet에 DataSet을 append 합니다.
     * @param ds    추가하려고 하는 DataSet
     * @return      결과 DataSet
     * @throws Exception
     */
    public DataSet appendData(DataSet ds) throws Exception {
        if(ds == null) throw new Exception("추가하려고 하는 DataSet 객체가 NULL입니다.");
        appendCount++;
        ds.initRow();
        while (ds.next()) {
            this.data.add(ds.getCurrentRow());
        }
        if(data.size() > DATASET_MAX_ROW) {
            logger.debug("추가하려는 Data가 최대 허용건수("+DATASET_MAX_ROW+")를 초과했습니다. (Append 함수 수행  횟수 :"+appendCount+")");
            throw new Exception("System 오류입니다. 추가하려는 Data가 최대 허용건수("+DATASET_MAX_ROW+")를 초과했습니다. (Append 함수 수행 횟수 :"+appendCount+")");
        }
        return this;
    }

    /**
     * 2006. 07. 19 김성균
     * DataSet에 Data를 append 합니다.
     * 2008. 03. 10 오재훈 MapDataSet을 Append할 경우 추가.
     * @param obj   추가하려고 하는 객체
     * @return      결과 DataSet
     * @throws Exception
     */
    public DataSet appendData(Object obj) throws Exception {

        if(obj == null) throw new Exception("추가하려고 하는 Data가 NULL입니다.");

        if(obj instanceof Map) {
            Object[] appendData = new Object[columnCount];

            for (int i = 0; i < columnCount; i++) {
                appendData[i] = ((Map<?, ?>)obj).get(getColumnName(i+1));
            }
            obj = appendData;
        }
        this.data.add(obj);
        if(data.size() > DATASET_MAX_ROW) {
            throw new Exception("System 오류입니다. 추가하려는 Data가 허용된 row를 초과했습니다." );
        }

        return this;
    }

    /**
     * 지정된 컬럼값의 평균을 구합니다.
     * @param columnIndex 컬럼 인뎃스
     * @return  컬럼값들의 평균
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public BigDecimal ave(int columnIndex) throws Exception {
        try {
            BigDecimal temp = sum(columnIndex);
            return temp.divide(new BigDecimal(getRowCount()), 10, BigDecimal.ROUND_HALF_UP);
        } catch(ArithmeticException e) {
            throw new Exception("0 으로 나눌수 없음.");
        }
    }

    /**
     * 지정된 컬럼값의 평균을 구합니다.
     * @param columnName 컬럼 이름
     * @return  컬럼값들의 평균
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public BigDecimal ave(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름::"+columnName);
        }
        return ave(index);
    }

    /**
     * 커서를 초기 위치로 이동합니다.
     */
    public boolean beforeFirst() {
        row = -1;
        return true;
    }

    /**
     * <code>DataSet</code> clone합니다.
     *
     * @return clone한 DataSet
     */
    @SuppressWarnings("unchecked")
    public Object clone() {
        DataSet rs = null;
        try {
        	rs = new DataSet((String[])this.columnNames.clone(), (ArrayList<Object>)this.data.clone());
        } catch(Exception e) {
        }
        return rs;
    }

    /**
     * 현재 커서 위치의 row를 삭제한다.
     * @throws Exception
     */
    public void deleteRow() throws Exception{
        try {
            data.remove(row);
            row--;
        } catch(IndexOutOfBoundsException ex) {
            throw new Exception("부적합한 row 인덱스:"+row);
        }
    }

    /**
     * 지정된 컬럼 이름으로 컬럼 인덱스를 얻습니다.
     *
     * @param columnName
     *            컬럼 이름
     * @return 컬럼 인덱스
     * @exception Exception
     *                잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public int findColumn(String columnName) throws Exception {

        if(columnNames == null)
            throw new Exception("DataSet 생성안됨");
        for(int i = 0; i < columnCount; i++)
            if(columnName.equalsIgnoreCase(columnNames[i]))
                return i + 1;
        return -1;
    }

    public Object[] findObjectArray() throws Exception {
        try {
            return (Object[]) data.get(row);
        } catch(NullPointerException e) {
            throw new Exception("DataSet이 생성되지 않았음");
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("검색결과가 없거나, DataSet.next가 호출되지 않았음");
        }
    }

    /**
     * 현재 커서를 최초 행으로 이동합니다.
     *
     * @return true - 올바른 커서위치, false - 잘 못된 커서 위치
     */
    public boolean first() {
        if(data == null)
            return false;
        row = 0;
        return true;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 BigDecimal형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public BigDecimal getBigDecimal(int index) throws Exception {
        Object temp[] = findObjectArray();
        BigDecimal tempBigDecimal = new BigDecimal((double) 0);
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            tempBigDecimal = new BigDecimal(obj.toString().trim());
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempBigDecimal;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 BigDecimal형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public BigDecimal getBigDecimal(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getBigDecimal(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 InputStream형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public InputStream getBinaryStream(int index) throws Exception {
        Object temp[] = findObjectArray();
        InputStream tempIs = null;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.math.BigDecimal || obj instanceof java.sql.Timestamp)
                    throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            byte tempBytes[];
            if(obj instanceof java.lang.String) {
                tempBytes = ((String) obj).getBytes();
            } else {
                tempBytes = (byte[]) obj;
            }
            tempIs = new ByteArrayInputStream(tempBytes);
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        }
        return tempIs;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 InputStream형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public InputStream getBinaryStream(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름::"+columnName);
        }
        return getBinaryStream(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 Blob형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 데이터 유형이 맞지 않은 경우
     */
    public Blob getBlob(int index) throws Exception {
        Object temp[] = findObjectArray();
        Blob blobObject = null;
        try {
            Object obj = temp[index - 1];
            if( obj instanceof Blob ) {
                blobObject = (Blob)obj;
            }else{
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            }
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        }
        return blobObject;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 Blob형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 데이터 유형이 맞지 않은 경우
     */
    public Blob getBlob(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getBlob(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 boolean형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 포맷
     */
    public boolean getBoolean(int index) throws Exception {
        Object temp[] = findObjectArray();
        Boolean tempBoolean = new Boolean("false");
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            int value = (new BigDecimal(obj.toString().trim())).intValue();
            if(value != 0) tempBoolean = new Boolean("true");
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempBoolean.booleanValue();
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 boolean형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 포맷
     */
    public boolean getBoolean(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getBoolean(index);
    }

	/**
     * 현재 행의 지정된 컬럼의 값을 byte형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
     */
    public byte getByte(int index) throws Exception {
        Object temp[] = findObjectArray();
        byte tempByte = (byte) 0;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            String value = obj.toString().trim();
            double dvalue = (new Double(value)).doubleValue();
            if(Byte.MAX_VALUE < dvalue || Byte.MIN_VALUE > dvalue)
                throw new Exception("숫자 오버플로우");
            tempByte = (new Double(value)).byteValue();
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempByte;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 byte형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
     */
    public byte getByte(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) throw new Exception("부적합한 열 이름===>"+columnName);
        return getByte(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 byte[]형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public byte[] getBytes(int index) throws Exception {
        Object temp[] = findObjectArray();
        byte tempBytes[] = null;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.math.BigDecimal || obj instanceof java.sql.Timestamp)
                    throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            if(obj instanceof java.lang.String) {
                tempBytes = ((String) obj).getBytes();
            } else {
                tempBytes = new byte[((byte[]) obj).length];
                System.arraycopy((byte[]) obj, 0, tempBytes, 0, tempBytes.length);
            }
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        }
        return tempBytes;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 byte[]형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public byte[] getBytes(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름::"+columnName);
        }
        return getBytes(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 Clob형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 데이터 유형이 맞지 않은 경우
     */
    public Clob getClob(int index) throws Exception {
        Object temp[] = findObjectArray();
        Clob clobObject = null;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof Clob ) {
                clobObject = (Clob)obj;
            }else{
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            }
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        }
        return clobObject;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 Clob형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 데이터 유형이 맞지 않은 경우
     */
    public Clob getClob(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getClob(index);
    }

    /**
     * 검색 결과의 컬럼의 수를 구합니다.
     *
     * @return 컬럼의 수
     */
    public int getColumnCount() {
        return columnCount;
    }

    /**
     * 지정된 컬럼의 이름을 구합니다.
     *
     * @param index
     *            컬럼 인덱스
     * @return 지정된 컬럼의 이름
     * @exception Exception
     *                적절하지 않은 열인덱스가 입력되었을 경우
     */
    public String getColumnName(int index) throws Exception {
        if(columnNames == null)
            throw new Exception("DataSet 생성안됨");
        String tempData = null;
        try {
            tempData = columnNames[index - 1];
        } catch (IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        }
        return tempData;
    }

    public String[] getColumnNames() {
        return this.columnNames;
    }

    public Object getCurrentRow() {
        return data.get(row);
    }

    /**
     * @return
     */
    public ArrayList<Object> getData() {
        return data;
    }

    /**
     * 검색 결과의 크기를 구합니다.
     *
     * @return 검색 결과의 크기
     */
    public long getDataSize() {
        if(data == null)
            return 0;
        return data.size();
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 java.sql.Date형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 포맷
     */
    public java.sql.Date getDate(int index) throws Exception {
        Object temp[] = findObjectArray();
        java.sql.Date tempDate = null;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.math.BigDecimal || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            tempDate = java.sql.Date.valueOf(obj.toString().trim());
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        } catch(NumberFormatException e) {
            try {
                java.sql.Timestamp tempTimestamp = java.sql.Timestamp.valueOf(temp[index - 1].toString().trim());
                tempDate = new java.sql.Date(tempTimestamp.getTime());
            } catch(NumberFormatException e2) {
                throw new Exception("내부 표기로 변환할 수 없음");
            }
        }
        return tempDate;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 double형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public double getDouble(int index) throws Exception {
        Object temp[] = findObjectArray();
        Double tempDouble = new Double((double) 0);
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            tempDouble = new Double(obj.toString().trim());
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempDouble.doubleValue();
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 double형으로 얻습니다.
     * 값이 null이면 화면에 default값 리턴
     *
     * @param index 컬럼 인덱스
     * @param defaultValue defaultValue null일경우 기본값
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public double getDouble(int index, double defaultValue) throws Exception {
        Object temp[] = findObjectArray();
        Double tempDouble = new Double((double) 0);
        try {
            Object obj = temp[index - 1];
            if(obj == null) {
                tempDouble = new Double(defaultValue);
            }
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            tempDouble = new Double(obj.toString().trim());
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempDouble.doubleValue();
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 double형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public double getDouble(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getDouble(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 double형으로 얻습니다.
     * 값이 null이면 화면에 default값 리턴
     *
     * @param columnName 컬럼 이름
     * @param defaultValue defaultValue null일경우 기본값
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public double getDouble(String columnName, double defaultValue) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getDouble(index, defaultValue);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 float형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public float getFloat(int index) throws Exception {
        Object temp[] = findObjectArray();
        Float tempFloat = new Float((float) 0);
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            tempFloat = new Float(obj.toString().trim());
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempFloat.floatValue();
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 float형으로 얻습니다.
     * 값이 null이면 화면에 default값 리턴
     *
     * @param index 컬럼 인덱스
     * @param defaultValue defaultValue null일경우 기본값
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public float getFloat(int index, float defaultValue) throws Exception {
        Object temp[] = findObjectArray();
        Float tempFloat = new Float((float) 0);
        try {
            Object obj = temp[index - 1];
            if(obj == null) {
                tempFloat = new Float(defaultValue);
            }
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            tempFloat = new Float(obj.toString().trim());
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempFloat.floatValue();
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 float형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public float getFloat(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getFloat(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 float형으로 얻습니다.
     * 값이 null이면 화면에 default값 리턴
     *
     * @param columnName 컬럼 이름
     * @param defaultValue defaultValue null일경우 기본값
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷
     */
    public float getFloat(String columnName, float defaultValue) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getFloat(index, defaultValue);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 int형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
     */
    public int getInt(int index) throws Exception {
        Object temp[] = findObjectArray();
        int tempInt = 0;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            String value = obj.toString().trim();
            double dvalue = (new Double(value)).doubleValue();
            if(Integer.MAX_VALUE < dvalue || Integer.MIN_VALUE > dvalue)
                throw new Exception("숫자 오버플로우");
            tempInt = (new Double(value)).intValue();
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스: "+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempInt;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 int형으로 얻습니다.
     * 값이 null이면 화면에 default값 리턴
     *
     * @param index 컬럼 인덱스
     * @param defaultValue defaultValue null일경우 기본값
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
     */
    public int getInt(int index, int defaultValue) throws Exception {
        Object temp[] = findObjectArray();
        int tempInt = 0;
        try {
            Object obj = temp[index - 1];
            if(obj == null) {
                tempInt = defaultValue;
            }
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            String value = obj.toString().trim();
            double dvalue = (new Double(value)).doubleValue();
            if(Integer.MAX_VALUE < dvalue || Integer.MIN_VALUE > dvalue)
                throw new Exception("숫자 오버플로우");
            tempInt = (new Double(value)).intValue();
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스: "+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempInt;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 int형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
     */
    public int getInt(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getInt(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 int형으로 얻습니다.
     * 값이 null이면 화면에 default값 리턴
     *
     * @param columnName 컬럼 이름
     * @param defaultValue defaultValue null일경우 기본값
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
     */
    public int getInt(String columnName, int defaultValue) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getInt(index, defaultValue);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 long형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
     */
    public long getLong(int index) throws Exception {
        Object temp[] = findObjectArray();
        long tempLong = (long) 0;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            String value = obj.toString().trim();
            double dvalue = (new Double(value)).doubleValue();
            if(Long.MAX_VALUE < dvalue || Long.MIN_VALUE > dvalue)
                throw new Exception("숫자 오버플로우");
            tempLong = (new Double(value)).longValue();
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempLong;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 long형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
     */
    public long getLong(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getLong(index);
    }

	public String getNVLString(int index) throws Exception{
         Object temp[] = findObjectArray();
         String tempStr = null;
         try {
             Object obj = temp[index - 1];
             if(obj instanceof byte[])
                tempStr = new String((byte[]) obj);
             else
                tempStr = obj.toString();
         } catch(NullPointerException e) {
         } catch(IndexOutOfBoundsException e) {
             throw new Exception("부적합한 열 인덱스::"+e);
         }
         return tempStr;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 String형으로 얻습니다.
     * null일 경우 "" 를 리턴합니다.
     *
     * @param columnName 컬럼 이름
     * @return 컬럼 값
     * @exception Exception
     *                잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public String getNVLString(String columnName) {
        return getNVLString(columnName,"");
    }

	public String getNVLString(String columnName, String nvlString) {
        int index=0;
        try{
            index = findColumn(columnName);
            if(index == -1) {
                logger.debug("부적합한 열 이름===>" + columnName);
                throw new Exception("부적합한 열 이름===>" + columnName);
            }
            String tempStr = getNVLString(index);
            return CommonUtil.nvl(tempStr, nvlString);
        } catch (Exception e) {
            return nvlString;
        }
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 Object형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public Object getObject(int index) throws Exception {
        Object temp[] = findObjectArray();
        try {
            return temp[index - 1];
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        }
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 Object형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   컬럼이름의 index가 없을경우
     */
    public Object getObject(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름::"+columnName);
        }
        return getObject(index);
    }

    /**
     * 현재 커서의 위치를 리턴합니다. 초기위치는 1입니다.
     *
     * @return true - 현재의 커서위치
     */
    public int getRow() {
        return row + 1;
    }

    /**
     * 검색결과 행의 수를 리턴합니다.
     *
     * @return 검색 결과 갯수
     */
    public int getRowCount() {
        if(data == null)
            return 0;
        return data.size();
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 short형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
    */
    public short getShort(int index) throws Exception {
        Object temp[] = findObjectArray();
        short tempShort = (short) 0;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.sql.Timestamp || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            String value = obj.toString().trim();
            double dvalue = (new Double(value)).doubleValue();
            if(Short.MAX_VALUE < dvalue || Short.MIN_VALUE > dvalue)
                throw new Exception("숫자 오버플로우");
            tempShort = (new Double(value)).shortValue();
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스 : "+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }

        return tempShort;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 short형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 숫자 포맷, 오버플로우
     */
    public short getShort(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>"+columnName);
        }
        return getShort(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 String형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public String getString(int index) throws Exception {
        Object temp[] = findObjectArray();
        String tempStr = null;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof byte[])
                tempStr = new String((byte[]) obj);
            else
                tempStr = obj.toString();
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        }
        return tempStr;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 String형으로 얻습니다.
     *
     * @param columnName 컬럼 이름
     * @return 컬럼 값
     * @exception Exception
     *                잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public String getString(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>" + columnName);
        }
        return getString(index);
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 java.sql.Timestamp형으로 얻습니다.
     * @param index 컬럼 인덱스
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 포맷
     */
    public java.sql.Timestamp getTimestamp(int index) throws Exception {
        Object temp[] = findObjectArray();
        java.sql.Timestamp tempTimestamp = null;
        try {
            Object obj = temp[index - 1];
            if(obj instanceof java.math.BigDecimal || obj instanceof byte[])
                throw new Exception("부적합한 열 유형::"+obj.getClass().getName());
            tempTimestamp = java.sql.Timestamp.valueOf(obj.toString().trim());
        } catch(NullPointerException e) {
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스:"+index);
        } catch(NumberFormatException e) {
            throw new Exception("내부 표기로 변환할 수 없음");
        }
        return tempTimestamp;
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 java.sql.Timestamp형으로 얻습니다.
     * @param columnName 컬럼 이름
     * @return  컬럼 값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우, 잘못된 포맷
     */
    public java.sql.Timestamp getTimestamp(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름::"+columnName);
        }
        return getTimestamp(index);
    }

    /**
     * 커서를 초기 위치로 이동합니다.
     */
    public void initRow() {
        row = -1;
    }

    /**
     * 지정된 컬럼이 존재하는지 알려줍니다.
     *
     * @param columnName
     *            컬럼 이름
     * @return 존재 유무
     * @exception Exception
     *                잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public boolean isColumn(String columnName) throws Exception {
        if(columnNames == null)
            throw new Exception("DataSet 생성안됨");
        for(int i = 0; i < columnCount; i++)
            if(columnName.equalsIgnoreCase(columnNames[i]))
                return true;
        return false;
    }

    /**
     * 현재 커서를 마지막 행으로 이동합니다.
     *
     * @return true - 올바른 커서위치, false - 잘 못된 커서 위치
     */
    public boolean last() {
        if(data == null) return false;
        try {
            row = data.size() - 1;
            return true;
        } catch(Exception e) {
            return false;
        }
    }

    /**
     * 지정된 컬럼값의 최대값을 구합니다.
     * @param columnIndex 컬럼 인덱스
     * @return  컬럼값들의 최대값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public BigDecimal max(int columnIndex) throws Exception {
        BigDecimal temp = null;
        try {
            for(int i = 0; i < data.size(); i++) {
                Object values[] = (Object []) data.get(i);
                if(values[columnIndex - 1] == null)
                    continue;
                if(values[columnIndex - 1] instanceof BigDecimal)
                    if(temp == null)
                        temp = (BigDecimal) values[columnIndex - 1];
                    else
                        temp = temp.max((BigDecimal) values[columnIndex - 1]);
                else
                    if(temp == null)
                        temp = new BigDecimal(values[columnIndex - 1].toString());
                    else
                        temp = temp.max(new BigDecimal(values[columnIndex - 1].toString()));
            }
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        } catch(NumberFormatException e) {
            throw new Exception("부적합한 열 타입::"+e);
        }
        return temp;
    }

    /**
     * 지정된 컬럼값의 최대값을 구합니다.
     * @param columnName 컬럼 이름
     * @return  컬럼값들의 최대값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public BigDecimal max(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름::"+columnName);
        }
        return max(index);
    }

    /**
     * 지정된 컬럼값의 최소값을 구합니다.
     * @param  columnIndex 컬럼 인덱스
     * @return  컬럼값들의 최소값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public BigDecimal min(int columnIndex) throws Exception {
        BigDecimal temp = null;
        try {
            for(int i = 0; i < data.size(); i++) {
                Object values[] = (Object []) data.get(i);
                if(values[columnIndex - 1] == null)
                    continue;
                if(values[columnIndex - 1] instanceof BigDecimal)
                    if(temp == null)
                        temp = (BigDecimal) values[columnIndex - 1];
                    else
                        temp = temp.min((BigDecimal) values[columnIndex - 1]);
                else
                    if(temp == null)
                        temp = new BigDecimal(values[columnIndex - 1].toString());
                    else
                        temp = temp.min(new BigDecimal(values[columnIndex - 1].toString()));
            }
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        } catch(NumberFormatException e) {
            throw new Exception("부적합한 열 타입::"+e);
        }
        return temp;
    }

    /**
     * 지정된 컬럼값의 최소값을 구합니다.
     * @param  columnName  컬럼 이름
     * @return  컬럼값들의 최소값
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public BigDecimal min(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름::"+columnName);
        }
        return min(index);
    }

    /**
     * 현재 커서를 다음 위치로 이동합니다.
     *
     * @return true - 올바른 커서위치, false - 잘 못된 커서 위치
     */
    public boolean next() {
        if(data == null)
            return false;
        try {
            if((row + 1) >= data.size())
                return false;
            else {
                row++;
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 현재 커서를 이전 위치로 이동합니다.
     *
     * @return true - 올바른 커서위치, false - 잘 못된 커서 위치
     */
    public boolean previous() {
        if(data == null || row <= 0)
            return false;
        else {
            row--;
            return true;
        }
    }

    /**
     *
     * 현재 커서의 위치에서 상태적인 위치로 이동합니다.
     * @return  index  상태적인 위치(음수 - 위로, 양수 -  아래로)
     * @return  true - 올바른 커서위치, false - 잘 못된 커서 위치
     */
    public boolean relative(int index) {
        if(data == null) return false;
        try {
            if(((row + index) >= data.size()) && ((row + index) < 0))
                return false;
            else {
                row += index;
                return true;
            }
        } catch(Exception e) {
            return false;
        }
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 SET 합니다.
     *
     * @param index
     *            컬럼 인덱스
     * @param value
     *            컬럼 데이타
     * @exception Exception
     *                잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public void setValue(int index, Object value) throws Exception {
        Object temp[] = findObjectArray();
        try {
            temp[index - 1] = value;
        } catch(NullPointerException e) {
        } catch (IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::" + e);
        }
    }

    /**
     * 현재 행의 지정된 컬럼의 값을 SET 합니다.
     *
     * @param columnName
     *            컬럼 이름
     * @param value
     *            컬럼 데이타
     * @exception Exception
     *                잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public void setValue(String columnName, Object value) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름===>" + columnName);
        }
        setValue(index, value);
    }

    /**
     * 검색결과 행의 수를 리턴합니다.
     *
     * @return 검색 결과 갯수
     */
    public int size() {
        if(data == null)
            return 0;
        return data.size();
    }

    /**
     * 지정된 컬럼으로 정렬합니다.
     * @param columnIndex  컬럼 인덱스
     * @param m  ASC 작은 순으로 정렬, DESC 큰 순으로 정렬
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public void sort(int columnIndex, int m) throws Exception {
        try {
            SortObject so[] = new SortObject[data.size()];
            for(int i = 0; i < data.size(); i++) {
                Object values[] = (Object []) data.get(i);
                so[i] = new SortObject(i, values[columnIndex - 1]);
            }
            Arrays.sort(so);
            ArrayList<Object> temp = new ArrayList<Object>(data.size());
            if(m == 1) {
                for(int i = 0; i < so.length; i++)
                    temp.add(data.get(so[i].index));
            } else {
                for(int i = so.length - 1; i >= 0; i--)
                    temp.add(data.get(so[i].index));
            }
            data = temp;
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        } catch(ClassCastException e) {
            throw new Exception("부적합한 열 타입::"+e);
        }
    }

    /**
     * 지정된 컬럼으로 정렬합니다.
     * @param columnName  컬럼 이름
     * @param m  ASC 작은 순으로 정렬, DESC 큰 순으로 정렬
     * @exception Exception   잘못된 컬럼 이름이 입력되었을 경우
     */
    public void sort(String columnName, int m) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름::"+columnName);
        }
        sort(index, m);
    }

    /**
     * 지정된 컬럼값의 총합를 구합니다.
     * @param columnIndex 컬럼 인덱스
     * @return  컬럼값들의 총합
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public BigDecimal sum(int columnIndex) throws Exception {
        BigDecimal temp = new BigDecimal(0);
        try {
            for(int i = 0; i < data.size(); i++) {
                Object values[] = (Object []) data.get(i);
                if(values[columnIndex - 1] == null) continue;
                if(values[columnIndex - 1] instanceof BigDecimal)
                    temp = temp.add((BigDecimal) values[columnIndex - 1]);
                else
                    temp = temp.add(new BigDecimal(values[columnIndex - 1].toString()));
            }
        } catch(IndexOutOfBoundsException e) {
            throw new Exception("부적합한 열 인덱스::"+e);
        } catch(NumberFormatException e) {
            throw new Exception("부적합한 열 타입::"+e);
        }
        return temp;
    }

    /**
     * 지정된 컬럼값의 총합를 구합니다.
     * @param columnName 컬럼 이름
     * @return  컬럼값들의 총합
     * @exception Exception   잘못된 컬럼 인덱스가 입력되었을 경우
     */
    public BigDecimal sum(String columnName) throws Exception {
        int index = findColumn(columnName);
        if(index == -1) {
            throw new Exception("부적합한 열 이름::"+columnName);
        }
        return sum(index);
    }

    public ArrayList<HashMap<String, Object>> toArrayList() throws Exception {
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        if( size() ==0 ) return list;
        HashMap<String, Object> map = null;
        initRow();
        try {
            while(next()) {
                map = new HashMap<String, Object>();
                for(int i = 0;i < getColumnCount(); i++) {
                    map.put(getColumnName(i),getObject(i));
                }
                list.add(map);
                map = null;
            }
        } finally {
            initRow();
        }
        return list;
    }

    /**
     * 현재 커서 위치의 데이터를 DataMap으로 변환하여 리턴한다.
     * @return DataMap
     * @throws Exception
     */
    public DataMap toDataMap() throws Exception{
        int i = 0;
        try {
            Object[] objArr = (Object[]) data.get(row);
            DataMap dataMap = new DataMap();
            for( ; i < this.columnNames.length; i++) {
                dataMap.put(columnNames[i], objArr[i]);
            }
            return dataMap;
        } catch(IndexOutOfBoundsException ex) {
            throw new Exception("부적합한 열 인덱스:["+i+"]");
        }
    }

    /**
     * 현재 커서 위치의 데이터를 인자로 들어오는 DataMap에 담는다.
     * DataMap에 원래 담겨 있던 값들은 모두 clear 된다.
     * @param dataMap
     * @throws Exception
     */
    public void toDataMap(DataMap dataMap) throws Exception{
        int i = 0;
        try {
            Object[] objArr = (Object[]) data.get(row);
            dataMap.clear();
            for( ; i < this.columnNames.length; i++) {
                dataMap.put(columnNames[i], objArr[i]);
            }
        } catch(IndexOutOfBoundsException ex) {
            throw new Exception("부적합한 열 인덱스:["+i+"]");
        }
    }

    /**
     * Key 값을 출력한다.
     * 작성 날짜: (2002-08-28 오전 4:54:41)
     */
    public String toPrintKeys() {
    	StringBuffer keysBuffer = new StringBuffer();
    	String[] keysName = getColumnNames();
    	for(int i = 0; keysName != null && i < keysName.length; i++) {
    		keysBuffer.append(keysName[i]);
    		if(i < keysName.length-1) {
    			keysBuffer.append(", ");
    		}
    	}
		return keysBuffer.toString();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("\n");
        for(int i = 0; i < columnCount; i++)
            sb.append("[" + columnNames[i] + "]");
        sb.append("\n");
        if(data==null){
            data=new ArrayList<Object>();
        }
        for(int i = 0; i < data.size(); i++) {
            Object obj = data.get(i);
            if(obj instanceof Map) {
                for(int j = 0; j < columnCount; j++)
                    sb.append(((Map<?, ?>)obj).get(columnNames[j]) + ((j==columnCount-1)?"\t":",\t "));
            } else {
                Object values[] = (Object []) obj;
                for(int j = 0; j < columnCount; j++)
                    sb.append(values[j] + ((j==columnCount-1)?"\t":",\t "));
            }
            sb.append("\n");
        }
        return sb.toString();
    }

}
