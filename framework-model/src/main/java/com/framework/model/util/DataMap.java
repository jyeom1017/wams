package com.framework.model.util;

import java.io.Reader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;

import com.framework.model.util.CommonUtil;

public class DataMap extends HashMap<Object, Object> implements Serializable {

	private static final long serialVersionUID = 5448171184660135921L;

	protected static Logger logger;

	private HttpServletRequest request = null;

    /**
     * DataMap 생성자 주석.
     */
    public DataMap() {
        super();
    }

    /**
     * DataMap 생성자 주석.
     */
    public DataMap(HttpServletRequest request) {
        super();
        this.request = request;
        init(request);
    }
    
    /**
     * DataMap 생성자 주석.
     */
    public DataMap(ServletRequest request) {
    	super();
    	init(request);
    	this.request = (HttpServletRequest)request;
    }
    
    /**
     * DataMap 생성자 주석.
     */
    public DataMap(Map<?, ?> map) {
    	super();
    	init(map);
    }

    /**
     * DataMap 생성자 주석.
     */
    public DataMap(ResultSet resultSet) {
        super();
        init(resultSet);
    }

    public String get(String key, String value) {
        String retString = "";
        Object o = getObject(key);
        if(o == null) {
        	retString = value;
    	} else if(o instanceof Integer) {
            Integer i = (Integer)getObject(key);
            retString = CommonUtil.nvl(i.toString(), value);
        } else if(o instanceof Long) {
            Long l = (Long)getObject(key);
            retString = CommonUtil.nvl(l.toString(), value);
        } else if(o instanceof Double) {
            Double d = (Double)getObject(key);
            retString = CommonUtil.nvl(d.toString(), value);
        } else if(o instanceof BigDecimal) {
            BigDecimal b = (BigDecimal)getObject(key);
            retString = CommonUtil.nvl(b.toString(), value);
        } else if(o instanceof String) {
            String s = (String)getObject(key);
            retString = CommonUtil.nvl(s, value);
        }
        return retString;
    }

    /**
     * key와 value를 인자로 받아 String을 return 한다. null 이면 기본 value를 return
     * 작성 날짜: (2002-08-26 오전 11:33:27)
     * @return java.lang.BigDecimal
     * @param key java.lang.String
     */
    public BigDecimal getBigDecimal(String key){
        return getBigDecimal(key, "");
    }

    /**
     * key와 value를 인자로 받아 String을 return 한다. null 이면 기본 value를 return
     * 작성 날짜: (2002-08-26 오전 11:33:27)
     * @return java.lang.BigDecimal
     * @param key java.lang.String
     * @param value int
     */
    public BigDecimal getBigDecimal(String key, int value) {
        return getBigDecimal(key, String.valueOf(value));
    }

    /**
     * key와 value를 인자로 받아 String을 return 한다. null 이면 기본 value를 return
     * 작성 날짜: (2002-08-26 오전 11:33:27)
     * @return java.lang.BigDecimal
     * @param key java.lang.String
     * @param value java.lang.String
     */
    public BigDecimal getBigDecimal(String key, String value) {
    	return new BigDecimal(get(key, value));
    }

    /**
     * key와 value를 인자로 받아 double을 return 한다. null 이면 기본 value를 double로 return 한다.
     * 작성 날짜: (2002-11-12 오후 2:25:38)
     * @return double
     * @param key java.lang.String
     */
    public double getDouble(String key) {
    	return getDouble(key, 0.0);
    }

    /**
     * key와 value를 인자로 받아 double을 return 한다. null 이면 기본 value를 double로 return 한다.
     * 작성 날짜: (2002-11-12 오후 2:25:38)
     * @return double
     * @param key java.lang.String
     * @param value double
     */
    public double getDouble(String key, double value) {
    	return getDouble(key, String.valueOf(value));
    }

    /**
     * key와 value를 인자로 받아 double을 return 한다. null 이면 기본 value를 double로 return 한다.
     * 작성 날짜: (2002-11-12 오후 2:25:38)
     * @return double
     * @param key java.lang.String
     * @param value java.lang.String
     */
    public double getDouble(String key, String value) {
        return new Double(get(key, value)).doubleValue();
    }

    /**
     * HTML의 hidden 객체를 생성해서 리턴한다.
     * Desc : 
     * @Method Name : getHiddenParams
     * @param key
     * @return
     */
    public String getHiddenParams(String keys) {
    	String[] arrayKey = keys.split(",");
    	StringBuffer sb = new StringBuffer();
    	for(int i = 0; arrayKey != null && i < arrayKey.length; i++) {
	    	String s = get(arrayKey[i], "");
	    	if(s != null && !s.trim().equals("")) {
	    		sb.append("<input type=\"hidden\" id=\"").append(arrayKey[i]).append("\" name=\"").append(arrayKey[i]).append("\" value=\"").append(s).append("\" />\n");
	    	}
    	}
    	return sb.toString();
    }

    /**
     * key와 value를 인자로 받아 int value을 return 한다. null 이면 기본 value를 int 로 return
     * 작성 날짜: (2002-11-12 오후 2:25:38)
     * @return int
     * @param key java.lang.String
     */
    public int getInt(String key) {
    	return getInt(key, 0);
    }

    /**
     * key와 value를 인자로 받아 int value을 return 한다. null 이면 기본 value를 int 로 return
     * 작성 날짜: (2002-11-12 오후 2:25:38)
     * @return int
     * @param key java.lang.String
     * @param value int
     */
    public int getInt(String key, int value) {
    	return Integer.parseInt(getString(key, value));
    }

    /**
     * key와 value를 인자로 받아 int value을 return 한다. null 이면 기본 value를 int 로 return
     * 작성 날짜: (2002-11-12 오후 2:25:38)
     * @return int
     * @param key java.lang.String
     * @param value java.lang.String
     */
    public int getInt(String key, String value) {
        return Integer.parseInt(getString(key, value));
    }

    public String[] getKeysName() {
        String[] keys = null;
        try {
            Set<Object> _set = keySet();
            keys = new String[_set.size()];
            Iterator<Object> iterator = _set.iterator();
            for(int i = 0; iterator.hasNext(); i++) {
                keys[i] = (String)iterator.next();
            }
            
        }catch(ArrayIndexOutOfBoundsException e){
        	keys = null;
        	
        } catch(Exception e) {
        	keys = null;
//        e.printStackTrace(); 2020
        }
        return keys;
    }

    /**
     * key와 value를 인자로 받아 long을 return 한다. null 이면 기본 value를 long으로 return 한다.
     * 작성 날짜: (2002-11-12 오후 2:25:38)
     * @return long
     * @param key java.lang.String
     */
    public long getLong(String key) {
    	return getLong(key, 0);
    }

    /**
     * key와 value를 인자로 받아 long을 return 한다. null 이면 기본 value를 long으로 return 한다.
     * 작성 날짜: (2002-11-12 오후 2:25:38)
     * @return long
     * @param key java.lang.String
     * @param value long
     */
    public long getLong(String key, long value) {
    	return getLong(key, String.valueOf(value));
    }

    /**
     * key와 value를 인자로 받아 long을 return 한다. null 이면 기본 value를 long으로 return 한다.
     * 작성 날짜: (2002-11-12 오후 2:25:38)
     * @return long
     * @param key java.lang.String
     * @param value java.lang.String
     */
    public long getLong(String key, String value) {
        return new Long(get(key, value)).longValue();
    }

    /**
     * key와 value를 인자로 받아 String을 return 한다. null 이면 기본 value를 return
     * 작성 날짜: (2002-08-26 오전 11:33:27)
     * @return java.lang.String
     * @param key java.lang.String
     * @param value java.lang.String
     */
    public Object getObject(String key) {
        return super.get(key);
    }

    public HttpServletRequest getRequest() {
    	return this.request;
    }

    public String getString(String key) {
        return getString(key, "");
    }

    public String getString(String key, int value) {
        return getString(key, String.valueOf(value));
    }

    /**
     * String key와 String default value을 인자로 받아 String을 return 한다. null 이면 기본 value를 return
     * 작성 날짜: (2002-08-26 오전 11:33:27)
     * @return java.lang.String
     * @param key java.lang.String
     * @param value java.lang.String
     */
    public String getString(String key, String value) {
    	String s = get(key, value);
        return s;
    }


    /**
     * HTML의 hidden 객체를 생성해서 리턴한다.
     * Desc : 
     * @Method Name : getStringHidden
     * @param key
     * @param value
     * @return
     */
    public String getStringHidden(String key) {
    	String s = get(key, "");
    	if(s == null || s.trim().equals("")) {
    		return "";
    	} else {
    		StringBuffer sb = new StringBuffer()
    			.append("<input type=\"hidden\" id=\"").append(key).append("\" name=\"").append(key).append("\" value=\"").append(s).append("\" />");
    		return sb.toString();
    	}
    }

    /**
     * HTML의 hidden 객체를 생성해서 리턴한다.
     * Desc : 
     * @Method Name : getStringHidden
     * @param key
     * @param value
     * @return
     */
    public String getStringHidden(String key, String value) {
    	String s = get(key, value);
    	StringBuffer sb = new StringBuffer();
    	if(s == null || s.trim().equals("")) {
    		sb.append("<input type=\"hidden\" id=\"").append(key).append("\" name=\"").append(key).append("\" value=\"").append(value).append("\" />");
    	} else {
    		sb.append("<input type=\"hidden\" id=\"").append(key).append("\" name=\"").append(key).append("\" value=\"").append(s).append("\" />");
    	}
    	return sb.toString();
    }

    /**
     * String key를 인자로 받아 String[] value를 return 한다.
     * 만약 값이 하나일경우 배열 size는 1로 하여 String[] 를 return 한다.
     * 작성 날짜: (2003-02-05 오후 1:55:11)
     * @return java.lang.String[]
     * @param key java.lang.String
     */
    public String[] getValues(String key) {
        if(super.get(key) != null) {
            try {
                return (String[])super.get(key);
            } catch(java.lang.ClassCastException e) {
                String[] rv = {(String)super.get(key)};
                return rv;
            }
        } else {
            return null;
        }
    }

    /**
     * String key를 인자로 받아 List value를 return 한다.
     * 만약 값이 하나일경우 배열 size는 1로 하여 String[] 를 return 한다.
     * 작성 날짜: (2003-02-05 오후 1:55:11)
     * @return java.lang.String[]
     * @param key java.lang.String
     */
    public List<String> getValuesList(String key) {
        if(super.get(key) != null) {
        	List<String> list = new ArrayList<String>();
        	String[] array = null;
            try {
            	array = (String[])super.get(key);
            } catch(java.lang.ClassCastException e) {
            	array = new String[]{(String)super.get(key)};
            }
            for(int i = 0; array != null && i < array.length; i++) {
            	list.add(array[i]);
            }
            return list;
        } else {
            return null;
        }
    }

    /**
     * 메소드 설명을 삽입하십시오.
     * 작성 날짜: (2002-08-26 오후 11:25:34)
     * @param request HttpServletRequest
     */
    private void init(HttpServletRequest request) {
        Enumeration<?> enumeration = null;
        try {
            enumeration = request.getParameterNames();
            while(enumeration.hasMoreElements()) {
                String key = (String)enumeration.nextElement();
                String[] pv = request.getParameterValues(key);
                if(pv != null) {
                    if(pv.length > 1) {
                        String[] __s = new String[pv.length];
                        for(int i = 0; i < __s.length; i++) __s[i] = pv[i];
                        this.put(key, __s);
                    } else {
                        this.put(key, pv[0]);
                    }
                }
            }
        }catch(ArrayIndexOutOfBoundsException e){
        	enumeration = null;
        } catch(Exception e) {
        	enumeration = null;
//            e.printStackTrace(); 2020
        }
    }
    
    /**
     * 메소드 설명을 삽입하십시오.
     * 작성 날짜: (2002-08-26 오후 11:25:34)
     * @param request ServletRequest
     */
    private void init(ServletRequest request) {
    	Enumeration<?> enumeration = null;
    	try {
    		enumeration = request.getParameterNames();
    		while(enumeration.hasMoreElements()) {
    			String key = (String)enumeration.nextElement();
    			String[] pv = request.getParameterValues(key);
    			if(pv != null) {
    				if(pv.length > 1) {
    					String[] __s = new String[pv.length];
    					for(int i = 0; i < __s.length; i++) __s[i] = pv[i];
    					this.put(key, __s);
    				} else {
    					this.put(key, pv[0]);
    				}
    			}
    		}
    	}catch(ArrayIndexOutOfBoundsException e){
    		enumeration = null;
    	} catch(Exception e) {
    		enumeration = null;
    		//e.printStackTrace();
    	}
    }

    /**
     * 메소드 설명을 삽입하십시오.
     * 작성 날짜: (2002-08-26 오후 11:25:34)
     * @param Map
     */
    private void init(Map<?, ?> map) {
        Set<?> set = null;
        Iterator<?> iterator = null;
        try {
            set = map.keySet();
            iterator = set.iterator();
            while(iterator.hasNext()) {
                String key = (String)iterator.next();
                Object value = CommonUtil.nvlObject(map.get(key));
                this.put(key, value);
            }
        }catch(ArrayIndexOutOfBoundsException e){
        	set = null;
        } catch(Exception e) {
        	set = null;
//            e.printStackTrace();2020

        }
    }

    /**
     * 메소드 설명을 삽입하십시오.
     * 작성 날짜: (2002-08-26 오후 11:25:34)
     * @param ResultSet rs
     */
    private void init(ResultSet rs) {
        ResultSetMetaData rsmd = null;
        try {
            rsmd = rs.getMetaData();
            if(rs.isBeforeFirst())
                rs.next();
            for(int i = 1; i <= rsmd.getColumnCount(); i++) {
                int columnType = rsmd.getColumnType(i);
                String key     = rsmd.getColumnName(i).toLowerCase();
                String value   = "";
                switch(columnType) {
                    case 2005:// clob
                        StringBuffer sb = new StringBuffer();
                        //////////////////////////////////// CLOB Start ////////////////////////////////////
                        Reader input = null;
                        try {
                            input = rs.getCharacterStream(i);
                            char[] buffer = new char[1024];
                            int byteRead;
                            while((byteRead = input.read(buffer, 0, 1024)) != -1) {
                                sb.append(buffer, 0, byteRead);
                            }
                        }catch(ArrayIndexOutOfBoundsException e){
                        	 break;
                            //e.printStackTrace();
                        } finally {
                            if(input != null) try { input.close(); } catch(Exception e1) { }
                        }
                        value = sb.toString();
                        //////////////////////////////////// CLOB End ////////////////////////////////////
                        break;
                    default:
                        value = rs.getString(i);
                }
                this.put(key, CommonUtil.nvl(value));
            }
        }catch(ArrayIndexOutOfBoundsException e){
        	rsmd = null;
        } catch(Exception e) {
        	rsmd = null;
//            e.printStackTrace(); 2020
//            오류 상황 대응 부재
//            Do Something();
        }
    }

    /**
     * Map에 세팅한다.
     * 작성 날짜: (2002-08-28 오전 4:54:41)
     * @param key int
     * @param value int
     */
    public void put(int key, int value) {
        put(String.valueOf(key), String.valueOf(value));
    }

    /**
     * Map에 세팅한다.
     * 작성 날짜: (2002-08-28 오전 4:54:41)
     * @param key int
     * @param value String
     */
   public void put(int key, String value) {
        put(String.valueOf(key), value);
    }

    /**
     * Map에 세팅한다.
     * 작성 날짜: (2002-08-28 오전 4:54:41)
     * @param key String
     * @param value int
     */
    public void put(String key, int value) {
        put(key, String.valueOf(value));
    }

    /**
     * Map에 세팅한다.
     * 작성 날짜: (2002-08-28 오전 4:54:41)
     * @param key String
     * @param value Object
     */
    public void put(String key, Object value) throws Exception {
        if(value == null) {
            super.put(key, "");
        } else {
            super.put(key, value);
        }
    }
    
    /**
     * Map에 세팅한다.
     * 작성 날짜: (2002-08-28 오전 4:54:41)
     * @param key String
     * @param value String
     */
    public void put(String key, String value){
    	super.put(key, value);
    }

    /**
     * 메소드 설명을 삽입하십시오.
     * 작성 날짜: (2003-02-06 오전 10:06:35)
     * @param Map
     */
    @SuppressWarnings("rawtypes")
	public void setMap(Map<?, ?> map) {
        if(map == null || map.isEmpty() || map.size() == 0) {
            map = new HashMap();
        }
        init(map);
    }
    
    /**
     * 메소드 설명을 삽입하십시오.
     * 작성 날짜: (2002-08-26 오후 11:25:34)
     * @param Map
     */
    public void setResultSet(Map<?, ?> map) {
    	Set<?> set = null;
    	Iterator<?> iterator = null;
    	try {
    		set = map.keySet();
    		iterator = set.iterator();
    		while(iterator.hasNext()) {
    			String key = (String)iterator.next();
    			Object value = map.get(key);
    			this.put(key, value);
    		}
    	}catch(ArrayIndexOutOfBoundsException e){
    		set = null;
    	} catch(Exception e) {
    		set = null;
//         e.printStackTrace(); 2020
//         오류 상황 대응 부재
//         Do Something();
    	}
    }

    /**
     * 메소드 설명을 삽입하십시오.
     * 작성 날짜: (2003-02-06 오전 10:06:35)
     * @param ResultSet resultSet
     */
    public void setResultSet(ResultSet rs) {
        init(rs);
    }

    /**
     * HashMap 리턴한다.
     * 작성 날짜: (2002-08-28 오전 4:54:41)
     * @param key String
     * @param value Object
     */
    public HashMap<?, ?> toHashMap() {
    	HashMap<Object, Object> hashMap = null;
    	try {
    		hashMap = new HashMap<Object, Object>();
	    	String[] keys = getKeysName();
	    	for(int i = 0; keys != null && i < keys.length; i++) {
	    		hashMap.put(keys[i], getString(keys[i]));
	    	}
    	}catch(ArrayIndexOutOfBoundsException e){
    		hashMap = null;
    	} catch(Exception e) {
    		//hashMap = new HashMap<Object, Object>();2020
    		hashMap = null;
    	}
        return hashMap;
    }

    /**
     * Key 값을 출력한다.
     * 작성 날짜: (2002-08-28 오전 4:54:41)
     */
    public String toPrintKeys() {
    	StringBuffer keysBuffer = new StringBuffer();
    	String[] keysName = getKeysName();
    	for(int i = 0; keysName != null && i < keysName.length; i++) {
    		keysBuffer.append(keysName[i]);
    		if(i < keysName.length-1) {
    			keysBuffer.append(", ");
    		}
    	}
		return keysBuffer.toString();
    }

    /**
     * Value 값을 출력한다.
     * 작성 날짜: (2002-08-28 오전 4:54:41)
     */
    public String toPrintValues() {
    	StringBuffer valuesBuffer = new StringBuffer();
    	String[] keysName = getKeysName();
    	for(int i = 0; keysName != null && i < keysName.length; i++) {
    		valuesBuffer.append(getString(keysName[i]));
    		if(i < keysName.length-1) {
    			valuesBuffer.append(", ");
    		}
    	}
		return valuesBuffer.toString();
    }

}