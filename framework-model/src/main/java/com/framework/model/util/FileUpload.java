package com.framework.model.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.framework.model.exception.ApplicationException;

@Service(value="fileUpload") 
public class FileUpload {

	private String[] arrFileType;		// 수신 파일 type 지정
	private int fileLimitSize;		    // 수신 파일 제한 용량(MB)
	private String fileName;	 	    // 생성 파일명 지정 [null 이면 default 이름 사용]
	private String uploadDir;		    // upload directory
	
	/**
	 * 파일 upload를 위한 기본 설정
	 * 
	 * @param arrFileType  받아들일 파일 확장자 (ex. JPG, GIF ...)
	 * @param fileName	   생성할 파일명 ( null : default file명 사용 [파이명_년월일시분초밀리세컨드.확장자)
	 */
	public void setFileUpload(String[] arrFileType, int fileLimitSize, String fileName, String uploadDir) {
		this.arrFileType = new String[arrFileType.length];
		for (int i = 0, ARRAY_FILE_TYPE_LENGTH = arrFileType.length; i < ARRAY_FILE_TYPE_LENGTH; i++) {
			this.arrFileType[i] = arrFileType[i];
		}
		this.fileLimitSize = fileLimitSize;
		this.fileName = fileName;
		this.uploadDir = uploadDir;
	}
	
	/**
	 * 업로드 가능 파일확장자 및 파일용량 체크
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public boolean validationFile(MultipartHttpServletRequest request) throws Exception {
		Iterator<String> iter = request.getFileNames();
		while(iter.hasNext()) {
			
			String uploadFileName = iter.next();
			CommonsMultipartFile fileData = (CommonsMultipartFile) request.getFile(uploadFileName);
			boolean imgCheck = false;
			String imgExt = "";								 //확장자
			String orgFileName = "";						 //수신 파일명
			if(fileData != null && ! fileData.isEmpty()) {
				orgFileName = fileData.getOriginalFilename();
				imgExt = orgFileName.substring(orgFileName.lastIndexOf(".") + 1,orgFileName.length());
				// upload 가능한 파일 타입 지정
				for(String str : arrFileType) {
					//설정한 파일 type과 등록하는 파일 type이 일치하는지 체크
					if(imgExt.toUpperCase().equals(str)){
						imgCheck = true;	
					}
				}
				//설정 파일과 등록 파일 type이 일치 하지 않음
				if(!imgCheck) {
					//throw new ApplicationException("업로드 가능한 확장자가 아닙니다.(" + imgExt + ")");
					System.out.println("업로드 가능한 확장자가 아닙니다.(" + imgExt + ")");
					return false;
				}
				
				//파일제한 사이즈 체크
				long fileSize = fileData.getSize() / 1024 / 1024;
				if(fileSize > this.fileLimitSize) {
					//throw new ApplicationException("파일사이즈가 제한사이즈(" + this.fileLimitSize + "MB)를 초과하였습니다.");
					System.out.println("파일사이즈가 제한사이즈(" + this.fileLimitSize + "MB)를 초과하였습니다.");
					return false;
				}
				
			}
		}
		
		return true;
	}
	
	
	/**
	 * 파일 upload 실행
	 * 
	 * @param uploadItem
	 * @param path
	 * @return
	 */
	public String create(CommonsMultipartFile fileData) throws Exception {

		StringBuffer newFileNameBuf = new StringBuffer();//new create file name
		String imgName = "";							 //파일명(확장자 제외)
		String imgExt = "";								 //확장자
		String orgFileName = "";						 //수신 파일명
		
		if (! fileData.isEmpty()) {
//		if (fileData != null) {
			orgFileName = fileData.getOriginalFilename();
			
			imgName = orgFileName.substring(0,orgFileName.lastIndexOf("."));
			imgExt = orgFileName.substring(orgFileName.lastIndexOf(".") + 1,orgFileName.length());

			//파일명 설정
			if(this.fileName!=null && !this.fileName.equals("")){
				//newFileNameBuf.append(this.fileName).append(".").append(imgExt);
				newFileNameBuf.append(CommonUtil.createUniqueIDFromDate()).append(".").append(imgExt);
				
			}else{
				//newFileNameBuf.append(imgName).append("_").
				newFileNameBuf.append(CommonUtil.createUniqueIDFromDate()).append(".").append(imgExt);
			}

			byte[] bytes = fileData.getBytes();
			FileOutputStream lFileOutputStream = null;
			try {
				// 폴더 생성
				File folder = new File(uploadDir);
				if( folder.mkdirs() == false ) {
					folder.mkdirs();
				}
				File lOutFile = new File(this.uploadDir + "/" + newFileNameBuf.toString());
				lFileOutputStream = new FileOutputStream(lOutFile);
				lFileOutputStream.write(bytes);
			} catch (IOException ie) {
				// Exception 처리
				//System.err.println("File writing error! ");
				throw new ApplicationException("파일업로드에 실패하였습니다.");
			} finally {
				if (lFileOutputStream != null) {
					lFileOutputStream.close();
				}
			}
		}

		// Some type of file processing...
//		System.err.println("-------------------------------------------");
////		System.err.println("Test upload: " + uploadItem.getName());
//		System.err.println("Test upload: "+ this.uploadDir + "/" + newFileNameBuf.toString());
//		System.err.println("Test upload: "+ fileData.getOriginalFilename());
//		System.err.println("-------------------------------------------");
		
		return newFileNameBuf.toString();
	}
}
