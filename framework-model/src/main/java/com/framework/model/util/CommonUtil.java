package com.framework.model.util;

import java.awt.Color;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import com.framework.model.exception.ApplicationException;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;



public class CommonUtil {
	protected static Logger logger = Logger.getLogger(CommonUtil.class.getName());

/**
 * 색상 정보 형태 변환
 * @param strColorValue(RGB 16진수) ex)ff,ff,ff -> 255,255,255
 * @return
 */
public static String getColorValueType2(String strColorValue) {
	String[] strColorValues = strColorValue.split(",");
	String strCovColor = String.format("%02x", Integer.parseInt(strColorValues[0], 10))
			+ String.format("%02x", Integer.parseInt(strColorValues[1], 10))
			+ String.format("%02x", Integer.parseInt(strColorValues[2], 10));
	return strCovColor;
}

/**
 * 색상 정보 형태 변환
 * @param strColorValue(RGB 16진수) ex)ffffff -> java.awt.Color
 * @return
 */
public static Color getColorType1(String strColorValue) {
	int r = Integer.parseInt(strColorValue.substring(0,2),16);
	int g = Integer.parseInt(strColorValue.substring(2,4),16);
	int b = Integer.parseInt(strColorValue.substring(4,6),16);
	return new Color(r,g,b);
}

	/**
	 * null 처리
	 * @param value(값)
	 * @return
	 */
	public static String replaceNull(String value) {
		if (value == null || value.length() == 0) {
			return "";
		}
		if(value.trim().equalsIgnoreCase("-")){
			return "";
		}
		return value;
	}
	
	/**
	 * null 처리
	 * @param value(값)
	 * @return
	 */
	public static String replaceNullString(String value) {
		
		if (value == null || value.length() == 0 || "".equals(value.replace("null", ""))) {
			return "";
		}
		if(value.trim().equalsIgnoreCase("-")){
			return "";
		}
		
		return value;
	}	

	/**
	 * strValue(값) 의 유효성 판단      return false -> null, -1, ''
	 * @param strValue(값)
	 * @return
	 */
	public static boolean checkValid(String strValue) {
		if(strValue == null
		|| strValue.equalsIgnoreCase("null")
		|| strValue.equalsIgnoreCase("-1") 
		|| strValue.equalsIgnoreCase("")
		|| strValue.equalsIgnoreCase("NaN")){
			return false;
		}
		return true;	
	}

	/**
	 * 세자리마다 <,> 표시
	 * @param d
	 * @return
	 */
	public static String getNumFromatInstance(double d) {
		  NumberFormat nf = NumberFormat.getInstance();
		  return nf.format(d);
	}
	/**
	 * 세자리마다 <,> 표시
	 * @param 문자열 strNumber
	 * @return
	 */
	public static String getStringCommaFormat(String strNumber) {

		StringBuffer sb = new StringBuffer(strNumber);
		StringBuffer rsb = new StringBuffer();
		sb = sb.reverse();
		int p = 0;

		for (int i = 0; i < strNumber.length(); i++) {
			p = i % 3;

			if (i > 0)
				if (p == 0)
					rsb.append(",");

			rsb.append(sb.substring(i, (i + 1)));
		}

		return (rsb.reverse()).toString();
	}
	/**
	 * 문자열이 숫자형으로 변환이 가능한지 유무판단
	 * @param value
	 * @return
	 */
	public static boolean checkConverterStringtoNumber(String value){
		byte[] c = value.getBytes();
		for(int j=0;j<c.length;j++){
			if(!((c[j] >= 48 && c[j] <= 57) || c[j] == 46)){
				return false;	
			}
		}
		return true;
	}
	
	public static String createUniqueIDFromDate(){
	    Calendar oCalendar = Calendar.getInstance( );  // 현재 날짜/시간 등의 각종 정보 얻기
	    int YEAR = oCalendar.get(Calendar.YEAR);
	    int MONTH = (oCalendar.get(Calendar.MONTH) + 1);
	    int DAY_OF_MONTH = oCalendar.get(Calendar.DAY_OF_MONTH);
	    int HOUR_OF_DAY = oCalendar.get(Calendar.HOUR_OF_DAY);
	    int MINUTE = oCalendar.get(Calendar.MINUTE);
	    int SECOND = oCalendar.get(Calendar.SECOND);
	    int MILLISECOND = oCalendar.get(Calendar.MILLISECOND);
	    
		return "" + YEAR + MONTH + DAY_OF_MONTH + HOUR_OF_DAY + MINUTE + SECOND + MILLISECOND;
	}
	
	public static String getDate(String format) {
		Calendar oCalendar = Calendar.getInstance( );  // 현재 날짜/시간 등의 각종 정보 얻기
		int YEAR = oCalendar.get(Calendar.YEAR);
		int MONTH = (oCalendar.get(Calendar.MONTH) + 1);
		String month = Integer.toString(MONTH);
		if(month.length() == 1) {
			month = "0"+month;
		}
		int DAY_OF_MONTH = oCalendar.get(Calendar.DAY_OF_MONTH);
		String day_of_month = Integer.toString(DAY_OF_MONTH);
		if(day_of_month.length() == 1) {
			day_of_month = "0"+day_of_month;
		}
		int HOUR_OF_DAY = oCalendar.get(Calendar.HOUR_OF_DAY);
		int MINUTE = oCalendar.get(Calendar.MINUTE);
		int SECOND = oCalendar.get(Calendar.SECOND);
		int MILLISECOND = oCalendar.get(Calendar.MILLISECOND);
		
		String rts = "" + YEAR + month + day_of_month + HOUR_OF_DAY + MINUTE + SECOND + MILLISECOND;
		if(! format.isEmpty()) {
			if(format.equals("yyyy-mm-dd")) {
				rts = "" + YEAR +"-"+ month +"-"+ day_of_month;
			}else if(format.equals("yyyy")) {
				rts = "" + YEAR;
			}
		}else {
			rts = "" + YEAR + month + day_of_month + HOUR_OF_DAY + MINUTE + SECOND + MILLISECOND;
		}
		return rts;
	}
	
    /**
     * SQL Injection 방지
     * @param str
     * @param length
     * @return
     * @throws Exception 
     */
    public static void checkSqlInjection(String str, String regx) throws Exception {
    	//허가안된 특수문자 및 DB명령어 정규식
    	Pattern pattern = Pattern.compile(regx);
    	Matcher matcher = pattern.matcher(str);
    	while(matcher.find()) {
    		throw new ApplicationException("허용되지 않는 문자가 들어왔습니다--> " + matcher.group());
    	}
//    	return matcher.replaceAll("");
    }

    /**
    * <pre>
    * 설명 : jsonArray to ArrayList<hashMap>
    * 상세설명 : .
    * </pre>
     */
    public static ArrayList<HashMap<String, Object>> convertJsonArrayToArray(JSONArray jsonArr) throws Exception {
		ArrayList<HashMap<String, Object>> list = new ArrayList<>();
		HashMap<String, Object> map = null;
		for(int i=0; i<jsonArr.size(); i++) {
			if(jsonArr.opt(i) instanceof JSONObject) {
				JSONObject obj = jsonArr.getJSONObject(i);
				Iterator<String> keysItr = obj.keys();
				map = new HashMap<String, Object>();
				while(keysItr.hasNext()) {
					String key = keysItr.next();
			        Object value = obj.get(key);
			        if(value.toString().equals("null")) {//jsonObject가 변환되는 과정에서 null을 스트링으르 인식하여 empty string으로 변환해 준다. - 2018-01-26 안상현
			        	value = "";
			        }
			        map.put(key, value);
				}
				list.add(map);
			}
		}
    	return list;
    }
	
    
    public static HashMap<String, Object> convertJsonToMap(JSONObject jsonObj) throws Exception {    	
    	Iterator<String> joI =  jsonObj.keys();
		HashMap<String, Object>	map = new HashMap<String, Object>();
		while(joI.hasNext()) {
			String key = joI.next();
	        Object value = jsonObj.get(key);
	        if(value.toString().equals("null")) {
	        	value = "";
	        }
	        map.put(key, value);
		}
		
    	return map;
    }

	
	
/*--------------------------------signalpark add--------------------------------------------*/
    /**
     * style의 수치의 값에 구분하여 px을 붙여준다.
     * Desc :
     * @Method Name : addPx
     * @param styleNum(style 수치)
     * @return
     */
    public static String addPx(String styleNum){
		String result = "";
		if(styleNum != null && Integer.parseInt(styleNum) > 0){
			result = styleNum + "px";
		}else{
			result = styleNum;
		}
		return result;
	}

	/**
	 * 영역타입을 받아서 호출할 메소드명으로 변환한다.
	 * Desc :
	 * @Method Name : changeAreaType
	 * @return strResult
	 * @throws
	 */
    public static String changeAreaType(String firstStr, String areaType){
		String strResult = "";
		String[] areaTypeArr = null;
		if(areaType != null && !"".equals(areaType)){
			// 다건
			if(areaType.indexOf("_") > -1){
				areaTypeArr = areaType.split("_");
				// 변환
				for(int i = 0; i < areaTypeArr.length; i++){
					if(areaTypeArr[i].length() > 1){
						areaTypeArr[i] = areaTypeArr[i].substring(0, 1).toUpperCase() + areaTypeArr[i].substring(1).toLowerCase();
					}
				}
				// 조합
				for(int i = 0; i < areaTypeArr.length; i++){
					strResult += areaTypeArr[i];
				}
			// 단건
			}else{
				if(areaType.length() > 1){
					strResult = areaType.substring(0, 1).toUpperCase() + areaType.substring(1).toLowerCase();
				}
			}
		}
		return firstStr + strResult;
	}

	/**
	 * 중복 파일일 경우 끝부분에 (1) 형식의 카운터 삽입.
	 * Desc :
	 * @Method Name : createFileName
	 * @param filePath
	 * @param fileName
	 * @return
	 */
	public static String createFileName(String filePath, String fileName) {
		File fileObject = new File(filePath+"/"+fileName);
		String newName = "";
		String tmpName = "";
		int count = 0;
		int s_pox = 0;
		int e_pox = 0;
		int p_pox = 0;
		if(fileObject.isFile()) {
			s_pox = fileName.lastIndexOf("(");
			e_pox = fileName.lastIndexOf(")");
			p_pox = fileName.lastIndexOf(".");
			tmpName = fileName.substring(p_pox);
			if(s_pox == -1) {
				newName = fileName.substring(0, p_pox)+"(1)"+tmpName;
			} else {
				count = Integer.parseInt(fileName.substring(s_pox+1, e_pox))+1;
				newName = fileName.substring(0, s_pox)+"("+count+")"+tmpName;
			}
			return createFileName(filePath, newName);
		}
		return fileName = fileName.replaceAll(" ","_");
	}

	/**
	 * 파일이 존재할 경우 해당 파일을 삭제한다.
	 * Desc :
	 * @Method Name : deleteFile
	 * @param filePath
	 * @param fileName
	 * @return
	 */
	public static void deleteFile(String filePath, String fileName) {
		File fileObject = new File(filePath+"/"+fileName);
    	if(fileObject.exists()) {
    		fileObject.delete();
    	}
	}

	/**
	 * 수정 시에 초기에 허용될 파일업로드 갯수를 세팅한다.
	 * Desc :
	 * @Method Name : earlyFileMaxCount
	 * @param orginMaxCnt
	 * @param fileSet
	 * @param uploadMode
	 * @return
	 */
	public static int earlyFileMaxCount(int orginMaxCnt, DataSet fileSet, String uploadMode) {
		int earlyMaxCnt = 0;
		// 업로드형식이 URL이라면 earlyMaxCnt는 무조건 1
		if("URL".equals(uploadMode)) {
			earlyMaxCnt = 1;
		} else {
			if(fileSet != null && fileSet.size() > 0) {
				if(orginMaxCnt <= fileSet.size()) {
					earlyMaxCnt = 0;
				} else {
					earlyMaxCnt = orginMaxCnt - fileSet.size();
				}
			} else {
				earlyMaxCnt = orginMaxCnt;
			}
		}
		return earlyMaxCnt;
	}

	/**
	 * 
	 * Desc : 
	 * @Method Name : editerDecode
	 * @param inStr
	 * @return
	 */
    public static String editerDecode(String inStr) {
        if(!nvl(inStr).equals(""))    {
            inStr = inStr.replaceAll("&#39;", "'");
            inStr = inStr.replaceAll("&#34;", "\"");
            inStr = inStr.replaceAll("&#44;", ", ");
            inStr = inStr.replaceAll("&lt;", "<");
            inStr = inStr.replaceAll("&gt;", ">");
            inStr = inStr.replaceAll("\n", "<br />");
        }
        return inStr;
    }

    /**
     * 
     * Desc : 
     * @Method Name : editerEncode
     * @param inStr
     * @return
     */
    public static String editerEncode(String inStr) {
        if(!nvl(inStr).equals(""))    {
            inStr = inStr.replaceAll("'", "&#39;");
            inStr = inStr.replaceAll("\"", "&#34;");
            inStr = inStr.replaceAll(", ", "&#44;");
            inStr = inStr.replaceAll("<", "&lt;");
            inStr = inStr.replaceAll(">", "&gt;");
        }
        return inStr;
    }

	/**
	 * [설명]
	 * 금액의 경우 좌측의 0을 자릿 수 만큼 채운다.
	 * @param String amount 금액
	 * @param int length 채울 자릿 수
	 * @return String 0 채운 결과 값
	 */
	public static String fillLeft(String amount, int length, char character) {
		if(nvl(amount).equals("")) return "";
		if(length < amount.length()) return amount;
		StringBuffer result = new StringBuffer();
		for(int i = 0; i < length-amount.length(); i++) {
			result.append(character);
		}
		return (result.append(amount)).toString();
	}

	/**
	 * [설명]
	 * 금액의 경우 좌측의 0을 자릿 수 만큼 채운다.
	 * @param String amount 금액
	 * @param int length 채울 자릿 수
	 */
	public static String fillLeftZero(String amount, int length) {
		return fillLeft(amount, length, '0');
	}
	
	/**
	 * [설명]
	 * 금액의 경우 좌측의 0을 제거한다.
	 *
	 * @param String amount 금액
	 * @return String 0제거 결과 값
	 */
	public static String trimLeftZero(String amount) {
		String strAmount = (new BigDecimal(amount)).toString();
		return strAmount;
	}	

	/**
	 * "/"(slash)로 시작하지 않으면 "/"(slash)를 추가하여 리턴한다.
	 * Desc :
	 * @Method Name : findSlash
	 * @param s
	 * @return
	 */
	public static String findSlash(String s) {
		if(nvl(s).equals("") || s.startsWith("/")) {
			return s;
		} else {
			return "/".concat(s);
		}
	}

	/**
	 * 쿠키의 키를 조회해서 값을 리턴한다.
	 * Desc : 
	 * @Method Name : getCookieValue
	 * @param cookieName
	 * @return
	 */
	public static String getCookieValue(HttpServletRequest request, String cookieName) {
		String cookieValue = null;
		Cookie[] cookies = request.getCookies();// request.getCookies의 리턴형은 Cookie[]
		if(cookies == null) {
			cookieValue = "";
		} else {
			for(Cookie cookie : cookies) {
				if(cookie.getName().equals(cookieName)) {
					//logger.debug("쿠키 : ["+cookie.getName()+"] ["+cookie.getValue()+"]");
					cookieValue = cookie.getValue();
					break;
				}
			}
		}
		return CommonUtil.nvl(cookieValue, "");
	}

    /**
     * [설명]
     * 금액을 숫자로 변환해 주는 메소드.
     * ex) System.out.println(CommonUtil.getDecimalFormat("100000"));
     *     result : 100,000
     * @param String str 변환할 금액의 String 값.
     * @return String 치환된 결과.
     */
    public static String getDecimalFormat(String str) {
        return getDecimalFormat(str, -1);
    }

    /**
     * [설명]
     * 금액을 숫자로 변환해 주는 메소드. 반올림 처리함
     * ex) System.out.println(CommonUtil.getDecimalFormat("100000"));
     *     result : 100,000
     * @param String str 변환할 금액의 String 값.
     * @param int fraction 형식을 선택할 값.
     * @return String 치환된 결과.
     */
    public static String getDecimalFormat(String str, int fraction) {
        if(nvl(str).length() == 0) {
            return "";
        }
        double doubleNumber = Double.parseDouble(str);
        String result = ""; // return format
        // fraction NumberFormat setting.
        java.text.DecimalFormat numberFormat = new java.text.DecimalFormat();
        switch(fraction) {
	        case  1 : numberFormat.applyPattern("#,###.0"); break;
	        case  2 : numberFormat.applyPattern("#,###.00"); break;
	        case  3 : numberFormat.applyPattern("#,###.000"); break;
	        case  4 : numberFormat.applyPattern("#,###.0000"); break;
	        case  5 : numberFormat.applyPattern("0"); break;
	        case  6 : numberFormat.applyPattern("0.0"); break;
	        case  7 : numberFormat.applyPattern("0.00"); break;
	        case  8 : numberFormat.applyPattern("0.000"); break;
	        case  9 : numberFormat.applyPattern("0.0000"); break;
	        case 10 : numberFormat.applyPattern("#,###.00000"); break;
	        default : numberFormat.applyPattern("#,##0"); break;
        }
        try {
            result = numberFormat.format(doubleNumber);
        } catch(IllegalArgumentException e) {
        	result = "-1";
        }
        return result;
    }
    
    /**
     * [설명]
     * 금액에 세 자리마다 컴마를 찍는다.
     * ex) System.out.println(CardUtil.getMoneyFormat("100000"));
     *     result : 100,000
     * @param String str 변환할 금액의 String 값.
     * @param int fraction 형식을 선택할 값.
     * @return String 치환된 결과.
     */
     public static String getMoneyFormat(String amount) {
         return getMoneyFormat(amount, 0);
     }

     /**
     * [설명]
     * 금액에 세 자리마다 컴마를 찍는다.
     * ex) System.out.println(CardUtil.getMoneyFormat("100000", 2));
     *     result : 1,000.00
     * @param String amount 변환할 금액의 String 값.
     * @param int point 소수점의 위치.
     * @return String 치환된 결과.
     */
     public static String getMoneyFormat(String amount, int point) {
         boolean isminus = false;
         if(amount.startsWith("-")) {
             isminus = true;
             amount = amount.substring(1);
         }
         //LogManager.debug("1 amount ["+amount+"]");
         amount = nvl(amount, "0");
         //LogManager.debug("2 amount ["+amount+"]");
         String o = "";
         String p = "";
         if(amount.indexOf(".") != -1) {
             o = amount.substring(0, amount.indexOf("."));
             p = amount.substring(amount.indexOf(".")+1, amount.length());
         } else {
             if(0 < point && point < amount.length()) {
                 o = amount.substring(0, amount.length()-point);
                 p = amount.substring(amount.length()-point);
             } else {
                 o = amount;
             }
         }
         o = trimLeftZero(o);
         int tLen = o.length();
         String tMoney = "";
         for(int i = 0; i < tLen; i++) {
             if(i != 0 && (i%3 == tLen%3))
                 tMoney += ",";
             if(i < tLen)
                 tMoney += o.charAt(i);
         }
         //LogManager.debug("p.length() ["+p.length()+"] point ["+point+"]");
         // point 보다 소수 자릿 수의 길이가 크면 point로 세팅한다.
         String _p = "";
         if(point < p.length()) {
             for(int i = 0; i < point; i++) {
                 //LogManager.debug("p ["+p.charAt(i)+"]");
                 _p += p.charAt(i);
             }
         // point가 소수 자릿 수 길이 보다 크면 소수 자릿 수 이후부터 point만큼 '0'으로 세팅한다.
         } else {
             _p = p;
             for(int i = p.length(); i < point; i++) {
                 _p += "0";
             }
         }
         if(0 < _p.length()) {
             tMoney += "."+_p;
         }
         if(isminus) {
             tMoney = "-"+tMoney;
         }
         return tMoney;
     }

     /**
     * [설명]
     * 금액에 세 자리마다 컴마를 찍는다. 소수점의 위치, locale, KWR 또는 USD 출력
     * ex) System.out.println(CardUtil.getMoneyFormat("100000", 2, "EN", 2));
     *     result : USD 1,000.00
     * @param String amount 변환할 금액의 String 값.
     * @param int point 소수점의 위치.
     * @param String locale locale
     * @param int no KWR 또는 USD 출력.
     * @return String 치환된 결과.
     */
     public static String getMoneyFormat(String amount, int point, String locale, int no) throws Exception {
         String strAmount = getMoneyFormat(amount, point);
         if(nvl(strAmount).length() == 0)
             return "";
         StringBuffer sbAmount = new StringBuffer();
         if("KO".equals(locale.toUpperCase())) {
             switch(no) {
                 case 1: sbAmount.append(strAmount).append("원"); break;
                 case 2: sbAmount.append("USD ").append(strAmount); break;
             }
         } else {
             switch(no) {
                 case 1: sbAmount.append("KRW ").append(strAmount); break;
                 case 2: sbAmount.append("USD ").append(strAmount); break;
             }
         }
         return sbAmount.toString();
     }
     
     /**
      * [설명]
      * 두 개의 숫자를 비교해서 a < b이면 true를 리턴 하고, a >= b 이면 false를 리턴한다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "1234567891123456789";
      *     String second = "1234567891123456789";
      *     boolean isGreater : CardUtil.isGreaterBigDecimal(first, second);
      *     result : false;
      *
      *     String first  = "1234567891123456789";
      *     String second = "1234567891123456789";
      *     boolean isGreater : CardUtil.isGreaterBigDecimal(first, second);
      *     result : false;
      *
      *     String first  = "1234567891123456789";
      *     String second = "1234567891123456788";
      *     boolean isGreater : CardUtil.isGreaterBigDecimal(first, second);
      *     result : true;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @return boolean
      */
     public static boolean isCompareToBigDecimal(String first, String second, char symbol) {
    	 BigDecimal firstBigDecimal  = new BigDecimal(first);
    	 BigDecimal secondBigDecimal = new BigDecimal(second);
    	 boolean result = false;
    	 switch(symbol) {
    	 case '+':
    		 if(0 <= firstBigDecimal.compareTo(secondBigDecimal)) {
    			 result = true;
    		 } else {
    			 result = false;
    		 }
    		 break;
    	 case '-':
    		 if(firstBigDecimal.compareTo(secondBigDecimal) <= 0) {
    			 result = true;
    		 } else {
    			 result = false;
    		 }
    		 break;
    	 case '<':
    		 if(firstBigDecimal.compareTo(secondBigDecimal) == -1) {
    			 result = true;
    		 } else {
    			 result = false;
    		 }
    		 break;
    	 case '>':
    		 if(firstBigDecimal.compareTo(secondBigDecimal) == 1) {
    			 result = true;
    		 } else {
    			 result = false;
    		 }
    		 break;
    	 case '=':
    		 if(firstBigDecimal.compareTo(secondBigDecimal) == 0) {
    			 result = true;
    		 } else {
    			 result = false;
    		 }
    		 break;
    	 }
    	 return result;
     }

     /**
      * [설명]
      * 두 개의 숫자를 비교해서 a >= b이면 true를 리턴 하고, a < b 이면 false를 리턴한다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "1234567891123456789";
      *     String second = "1234567891123456780";
      *     boolean isGreaterEqualBigDecimal : CardUtil.isGreaterEqualBigDecimal(first, second);
      *     result : true;
      *
      *     String first  = "1234567891123456789";
      *     String second = "1234567891123456789";
      *     boolean isGreaterEqualBigDecimal : CardUtil.isGreaterEqualBigDecimal(first, second);
      *     result : true;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @return boolean
      */
     public static boolean isGreaterEqualBigDecimal(String first, String second) {
    	 return isCompareToBigDecimal(first, second, '+');
     }
     /**
      * [설명]
      * 두 개의 숫자를 비교해서 a > b이면 true를 리턴 하고, a <= b 이면 false를 리턴한다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "1234567891123456789";
      *     String second = "1234567891123456780";
      *     boolean isGreaterBigDecimal : CardUtil.isGreaterBigDecimal(first, second);
      *     result : true;
      *
      *     String first  = "1234567891123456789";
      *     String second = "1234567891123456789";
      *     boolean isGreaterBigDecimal : CardUtil.isGreaterBigDecimal(first, second);
      *     result : false;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @return boolean
      */
     public static boolean isGreaterBigDecimal(String first, String second) {
    	 return isCompareToBigDecimal(first, second, '>');
     }

     /**
      * [설명]
      * 두 개의 숫자를 비교해서 a <= b이면 true를 리턴 하고, a > b 이면 false를 리턴한다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "1234567891123456780";
      *     String second = "1234567891123456789";
      *     boolean isLessEqualBigDecimal : CardUtil.isLessEqualBigDecimal(first, second);
      *     result : true;
      *
      *     String first  = "1234567891123456789";
      *     String second = "1234567891123456789";
      *     boolean isLessEqualBigDecimal : CardUtil.isLessEqualBigDecimal(first, second);
      *     result : true;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @return boolean
      */
     public static boolean isLessEqualBigDecimal(String first, String second) {
    	 return isCompareToBigDecimal(first, second, '-');
     }

     /**
      * [설명]
      * 두 개의 숫자를 비교해서 a < b이면 true를 리턴 하고, a >= b 이면 false를 리턴한다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "1234567891123456789";
      *     String second = "1234567891123456780";
      *     boolean isLessBigDecimal : CardUtil.isLessBigDecimal(first, second);
      *     result : false;
      *
      *     String first  = "1234567891123456780";
      *     String second = "1234567891123456789";
      *     boolean isLess : CardUtil.isGreaterBigDecimal(first, second);
      *     result : true;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @return boolean
      */
     public static boolean isLessBigDecimal(String first, String second) {
    	 return isCompareToBigDecimal(first, second, '<');
     }

     /**
      * [설명]
      * 두 개의 숫자를 비교해서 a = b이면 true를 리턴 하고, a > b or a < b이면 false를 리턴한다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "1234567891123456789";
      *     String second = "1234567891123456780";
      *     boolean isEqualBigDecimal : CardUtil.isEqualBigDecimal(first, second);
      *     result : false;
      *
      *     String first  = "1234567891123456789";
      *     String second = "1234567891123456789";
      *     boolean isEqualBigDecimal : CardUtil.isEqualBigDecimal(first, second);
      *     result : true;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @return boolean
      */
     public static boolean isEqualBigDecimal(String first, String second) {
    	 return isCompareToBigDecimal(first, second, '=');
     }

     /**
      * [설명]
      * 기본 값을 scale 자릿 수 만큼 ROUND한다.
      * ex) System.out.println(CardUtil.scaleBigDecimal("1000", 2, 3));
      *     result : false;
      *
      * @param value String 기본 값(value가 null이면 0으로 세팅)
      * @param scale int 자리수지정
      * @param round int 절삭여부(3-절삭, 4-반올림, 5-반내림)
      * @return String
      */
     public static String scaleBigDecimal(String value, int scale, int round) {
    	 BigDecimal first = new BigDecimal(nvl(value, "0"));
    	 return first.setScale(scale, round).toString();
     }

     /**
      * [설명]
      * 두 개의 숫자를 뺀다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "123456789123456789123456789123456789";
      *     String second = "123456789123456789123456789123456789";
      *     String result = CardUtil.addBigDecimal(first, second);
      *     result : 246913578246913578;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @return String
      */
     public static String addBigDecimal(String first, String second) {
    	 BigDecimal decimal = new BigDecimal(first);
    	 BigDecimal result = decimal.add(new BigDecimal(second));
    	 return result.toString();
     }

     /**
      * [설명]
      * 두 개의 숫자를 뺀다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "123456789123456789123456789123456789";
      *     String second = "123456789123456789123456789123456789";
      *     String result = CardUtil.subtractBigDecimal(first, second);
      *     result : 0;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @return String
      */
     public static String subtractBigDecimal(String first, String second) {
    	 BigDecimal decimal = new BigDecimal(first);
    	 BigDecimal result = decimal.subtract(new BigDecimal(second));
    	 return result.toString();
     }

     /**
      * [설명]
      * 두 개의 숫자를 곱한다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "123456789123456789123456789123456789";
      *     String second = "123456789123456789123456789123456789";
      *     String result = CardUtil.multiplyBigDecimal(first, second);
      *     result : 15241578780673678515622620750190521;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @return String
      * @exception Exception 메소드 수행시 발생한 모든 에러.
      */
     public static String multiplyBigDecimal(String first, String second) {
    	 BigDecimal decimal = new BigDecimal(first);
    	 BigDecimal result = decimal.multiply(new BigDecimal(second));
    	 return result.toString();
     }

     /**
      * [설명]
      * 두 개의 숫자를 나눈다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "123456789123456789123456789123456789";
      *     String second = "123456789123456789123456789123456789";
      *     String result = CardUtil.divideBigDecimal(first, second);
      *     result : 1;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @param int scale 소수이하 자릿 수
      * @return String
      * @exception Exception 메소드 수행시 발생한 모든 에러.
      */
     public static String divideBigDecimal(String first, String second, int scale) {
    	 return divideBigDecimal(first, second, scale, 3);// 기본 값은 절삭으로 세팅
     }

     /**
      * [설명]
      * 두 개의 숫자를 나눈다.
      * 반드시 "문자형"으로 사용해야만 한다.
      * ex) String first  = "10";
      *     String second = "3";
      *     String result = CardUtil.divideBigDecimal(first, second, 3, 4);
      *     result : 1;
      *
      * @param String first 첫 번째 숫자(문자형)
      * @param String second 두 번째 숫자(문자형)
      * @param int scale 소수이하 자릿 수
      * @param int round 절삭여부(3-절삭, 4-반올림, 5-반내림), 기본 값은 절삭으로 세팅
      * @return String
      * @exception Exception 메소드 수행시 발생한 모든 에러.
      */
     public static String divideBigDecimal(String first, String second, int scale, int round) {
    	 BigDecimal decimal = new BigDecimal(first);
    	 BigDecimal result = decimal.divide(new BigDecimal(second), scale, round);
    	 return result.toString();
     }     

	/**
	 * 메뉴 출력 시 여백을 넣어준다.
	 * Desc : 증가 또는 감소 레벨 만큼 공백을 리턴한다.
	 * @Method Name : getLeftMenuTab
	 * @param level
	 * @return
	 */
	public static String getLeftMenuTab(int increaseLevel) {
		StringBuffer buffer = new StringBuffer();
		for(int i = 0; i < increaseLevel; i++) {
			buffer.append("    ");
		}
		return buffer.toString();
	}

	public static DataMap getNvlDataMap(DataMap dataMap) {
		if(dataMap == null) {
			return new DataMap();
		}
		return dataMap;
	}

	/**
	 * DataSet을 받아와서 null 체크를 해서 리턴한다. <br/>
	 * @param dataSet - null 체크할 DataSet
	 * @return DataSet - null 체크 된 DataSet
	 */
	public static DataSet getNvlDataSet(DataSet dataSet) {
		if(dataSet == null) {
			return new DataSet();
		}
		return dataSet;
	}

	/**
	 * Object를 받아와서 null 체크를 해서 리턴한다. <br/>
	 * @param Object - null 체크할 DataSet
	 * @return Object - null 체크 된 DataSet
	 */
	public static Object getNvlObject(Object object, Object returnObject) {
		if(object == null) {
			return returnObject;
		} else {
			return object;
		}
	}

	/**
	 * 업로드 파일의 확장자를 리턴한다.
	 * Desc :
	 * @Method Name : getFileExe
	 * @param fileName
	 * @return
	 */
	public static String getFileExe(String fileName) {
		int pos = fileName.lastIndexOf(".");
		return fileName.substring(pos+1);
	}
	
	/**
	 * 파일의 확장자를 받아 그에 해당하는 아이콘img 태그를 반환한다.
	 * Desc :
	 * @Method Name : getFileIcon
	 * @param exe
	 * @return
	 */
	public static String getFileIcon(String strExe) {
		return getFileIcon(strExe, "");
	}

	/**
	 * 파일의 확장자를 받아 그에 해당하는 아이콘img 태그를 반환한다.
	 * Desc :
	 * @Method Name : getFileIcon
	 * @param exe
	 * @return
	 */
	public static String getFileIcon(String strExe, String pathFolder) {
		String resultTag = null;
		if(strExe != null && !"".equals(strExe)) {
			strExe = strExe.toLowerCase();
			// PDF
			if("pdf".equals(strExe)) {
				resultTag = "<img src='/images"+pathFolder+"/icon/iconPdf.gif' class='iconFile' alt='PDF'/>";
			// DOC
			} else if("doc".equals(strExe) || "docx".equals(strExe)) {
				resultTag = "<img src='/images"+pathFolder+"/icon/iconDoc.gif' class='iconFile' alt='DOC'/>";
			// XLS
			} else if("xls".equals(strExe) || "xlsx".equals(strExe)) {
				resultTag = "<img src='/images"+pathFolder+"/icon/iconXls.gif' class='iconFile' alt='XLS'/>";
			// PPT
			} else if("ppt".equals(strExe) || "pptx".equals(strExe)) {
				resultTag = "<img src='/images"+pathFolder+"/icon/iconPpt.gif' class='iconFile' alt='PPT'/>";
			}
		}
		if(resultTag == null) {
			resultTag = "<img src='/images"+pathFolder+"/icon/iconFile.gif' class='iconFile' alt='FILE' />";
		}
		return resultTag;
	}

	/**
	 *  파일의 파일명을 리턴한다.
	 * Desc :
	 * @Method Name : getFileName
	 * @param fileName
	 * @return
	 */
	public static String getFileName(String fileName) {
		int pos = fileName.lastIndexOf(".");
		return fileName.substring(0, pos);
	}

	/**
	 * 현재의 밀리세컨드를 리턴한다.
	 * Desc :
	 * @Method Name : getTime
	 * @return
	 */
	public static String getTime() {
		return String.valueOf(System.currentTimeMillis());
	}

	/**
	 * 현재의 시간을 pattern에 맞게 리턴한다.
	 * 작성 날짜: (2003-07-25 오후 9:22:10)
	 * @return java.lang.String
	 * @param pattern java.lang.String
	 */
	public static String getTime(String pattern) {
		SimpleDateFormat fmt = new SimpleDateFormat(pattern);
		String time = fmt.format(new java.util.Date(System.currentTimeMillis()));
		return time;
	}

	public static DataMap getSessionInfoMap(HttpServletRequest request, String sessionKey) {
		HttpSession session = request.getSession();
		if(session.getAttribute(sessionKey) == null) {
			return new DataMap();
		} else {
			return (DataMap)session.getAttribute(sessionKey);
		}
	}

	public static String getSessionInfoMap(HttpServletRequest request, String sessionKey, String key) {
		HttpSession session = request.getSession();
		if(session.getAttribute(sessionKey) == null) {
			return "";
		} else {
			DataMap map = (DataMap)session.getAttribute(sessionKey);
			return map.getString(key);
		}
	}

	public static Object getSessionInfoObject(HttpServletRequest request, String sessionKey) {
		HttpSession session = request.getSession();
		return session.getAttribute(sessionKey);
	}

	public static DataSet getSessionInfoSet(HttpServletRequest request, String sessionKey) {
		HttpSession session = request.getSession();
		if(session.getAttribute(sessionKey) == null) {
			return new DataSet();
		} else {
			return (DataSet)session.getAttribute(sessionKey);
		}
	}

	public static String[] getSessionInfoSet(HttpServletRequest request, String sessionKey, String key) {
		HttpSession session = request.getSession();
		if(session.getAttribute(sessionKey) == null) {
			return new String[]{""};
		} else {
			String[] values = null;
			DataSet set = (DataSet)session.getAttribute(sessionKey);
			if(set != null) {
				values = new String[set.size()];
				int idx = 0;
				set.initRow();
				while(set.next()) {
					values[idx++] = set.getNVLString(key);
				}
			} else {
				values = new String[]{""};
			}
			return values;
		}
	}

	public static String getYYYYMMDDPatternDate(String date,String pattern ) {
		pattern = pattern.toUpperCase();
		if(date != null && date.length() == 8) {
			String dateY=date.substring(0, 4);
			String dateM=date.substring(4, 6);
			String dateD=date.substring(6, 8);
			return pattern.replace("YYYY", dateY).replace("MM", dateM).replace("DD", dateD);
		} else {
			return "";
		}
	}

	/**
	 * 빈 문자이면 null을 리턴한다.
	 * Desc :
	 * @Method Name : ifStringEmptyIsNull
	 * @param s
	 * @return
	 */
	public static String ifStringEmptyIsNull(String s) {
		if("".equals(s)) {
			return null;
		}
		return s;

	}

	/**
	 * Null 이면 true 아니면 false를 리턴한다.
	 * Desc :
	 * @Method Name : isEmpty
	 * @param o
	 * @return
	 */
	public static boolean isEmpty(Object o) {
		return (o == null ? true : false);
	}

	/**
	 * List 객체의 값을 String으로 변환하여 리턴한다.
	 * Desc :
	 * @Method Name : listToString
	 * @param list
	 * @return String
	 */
	public static String listToString(List<?> list) {
		return listToString(list, "");
	}

	/**
	 * List 객체의 값을 String으로 변환하여 리턴한다. 이 때 각 데이타는 delim으로 구분하여 리턴한다.
	 * Desc :
	 * @Method Name : listToString
	 * @param list
	 * @param delim : 구분자
	 * @return String
	 */
	public static String listToString(List<?> list, String delim) {
		if(list == null || list.isEmpty()) {
			return "";
		}
		StringBuffer buffer = new StringBuffer();
		for(int i = 0; list != null && !list.isEmpty() && i < list.size(); i++) {
			buffer.append((String)list.get(i)).append(delim);
		}
		return buffer.toString();
	}

	/**
	 * [설명]
	 * Object의 null을 체크하여 백문자로 리턴한다.
	 * ex) Object o = null;
	 *     CommonUtil.nvl(o);
	 * @param String o 문장
	 * @return Object 체크한 결과 값.
	 * @exception Exception 메소드 수행시 발생한 모든 에러.
	 */
	public static Object nvlObject(Object o) {
		return nvlObject(o, new Object());
	}

	/**
	 * [설명]
	 * null을 체크하여 공백문자로 리턴한다.
	 * ex) Object o = null;
	 *     CommonUtil.nvl(o, "string");
	 *     Object o = "test";
	 *     CommonUtil.nvl(o, "string");
	 * @param Object o1 String 문장
	 * @param Object o2 null의 경우에 치환될 값
	 * @return Object 체크한 결과 값.
	 * @exception Exception 메소드 수행시 발생한 모든 에러.
	 */
	public static Object nvlObject(Object o1, Object o2) {
		try {
			if(o1 == null) return o2;
		} catch(Exception e) {
			return o2;
		}
		return o1;
	}

	/**
	 * [설명]
	 * null을 체크하여 공백문자로 리턴한다.
	 * ex) String s = null;
	 *     CommonUtil.nvl(s);
	 * @param String s 문장
	 * @return String null 체크한 결과 값.
	 */
	public static String nvl(String s) {
		return nvl(s, "");
	}

	/**
	 * [설명]
	 * null을 체크하여 공백문자로 리턴한다.
	 * ex) String s = null;
	 *     CommonUtil.nvl(s, "Null value");
	 * @param String String 문장
	 * @param String s2 null의 경우에 치환될 값
	 * @return String null 체크한 결과 값.
	 */
	public static String nvl(String s1, String s2) {
		try {
			if(s1 == null || s1.trim().equals("")) return s2;
		} catch(Exception e) {
			return s2;
		}
		return s1;
	}

	/**
	 * [설명]
	 * 무작위의 영문자 출력 발생 메소드 - 기본 10자리
	 * ex) Log.debug("["+CommonUtil.randomString()+"]");
	 *     result : djfkdjejrj
	 * @param int
	 * @return String
	 */
	public static String random(int length) {
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		int number;
		for(int i = 0; i < length; i++) {
			// 1과 2의 정수를 랜덤하게 출력
			number = random.nextInt(2);
			// number/2의 나머지가 0이면 문자를 세팅
			if((number%2) == 0) {
				sb.append(randomString(1));
				// number/2의 나머지가 0이 아니면 숫자를 세팅한다.
			} else {
				sb.append(randomNumber(1));
			}
		}
		return sb.toString();
	}

	/**
	 * [설명]
	 * 무작위의 숫자 발생 메소드 - length 수 만큼
	 * ex) String s = null;
	 *     Log.debug("["+CommonUtil.randomNumber(5)+"]");
	 *     result : 86521
	 * @param String
	 * @return String
	 */
	public static String randomNumber(int length) {
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		int number;
		for(int i = 0; i < length; i++) {
			// 1~10까지의 정수를 랜덤하게 출력
			number = random.nextInt(10);
			sb.append(number);
		}
		if(length < sb.toString().length()) {
			String s = sb.toString();
			sb.setLength(0);
			sb.append(s.substring(0, s.length()-1));
		}
		return sb.toString();
	}

	/**
	 * [설명]
	 * 무작위의 숫자 발생 메소드 - size는 범위
	 * 1 : 한 개, 2 : 두 개, 3 : 세 개, ...
	 * ex) String s = null;
	 *     Log.debug("["+CommonUtil.randomRange(5)+"]");
	 *     result : 3 (1~5 사이의 어떤 수
	 * @param int
	 * @return int
	 */
	public static int randomRange(int size) {
		Random random = new Random();
		int number = random.nextInt(size);
		return number;
	}

	/**
	 * [설명]
	 * 무작위의 영문자 출력 발생 메소드
	 * ex) Log.debug("["+CommonUtil.randomString(5)+"]");
	 *     result : asdfe
	 * @param int
	 * @return String
	 */
	public static String randomString(int length) {
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		char ch = ' ';
		int number;
		for(int i = 0; i < length; i++) {
			// 1과 2의 정수를 랜덤하게 출력
			number = random.nextInt(2);
			// number/2의 나머지가 0이면 문자를 세팅
			if((number%2) == 0) {
				// 대문자 A-Z 랜덤 알파벳 생성
				ch = (char)((Math.random() * 26) + 65);
				// number/2의 나머지가 0이 아니면 숫자를 세팅한다.
			} else {
				// 소문자 a-z 랜덤 알파벳 생성
				ch = (char)((Math.random() * 26) + 97);
			}
			sb.append(ch);
		}
		return sb.toString();
	}

	/**
	 * 세션에 세팅한다.
	 * Desc :
	 * @Method Name : setSessionInfoMap
	 * @param request
	 * @param sessionKey
	 * @param value
	 */
	public static void setSessionInfoMap(HttpServletRequest request, String sessionKey, DataMap value) {
		setSessionInfoObject(request, sessionKey, value);
	}

	/**
	 * 세션에 세팅한다.
	 * Desc :
	 * @Method Name : setSessionInfoMap
	 * @param request
	 * @param sessionKey
	 * @param key
	 * @param value
	 */
	public static void setSessionInfoMap(HttpServletRequest request, String sessionKey, String key, String value) {
		HttpSession session = request.getSession();
		if(session.getAttribute(sessionKey) != null) {
			DataMap map = (DataMap)session.getAttribute(sessionKey);
			map.put(key, value);
			session.removeAttribute(sessionKey);
			session.setAttribute(sessionKey, map);
		}
	}

	/**
	 * 세션에 세팅한다.
	 * Desc :
	 * @Method Name : setSessionInfoObject
	 * @param request
	 * @param sessionKey
	 * @param value
	 */
	public static void setSessionInfoObject(HttpServletRequest request, String sessionKey, Object value) {
		HttpSession session = request.getSession();
		session.setAttribute(sessionKey, value);
		// 세션 유지시간
		session.setMaxInactiveInterval(60*1440);
	}

	/**
	 * 세션에 세팅한다.
	 * Desc :
	 * @Method Name : setSessionInfoSet
	 * @param request
	 * @param sessionKey
	 * @param value
	 */
	public static void setSessionInfoSet(HttpServletRequest request, String sessionKey, DataSet value) {
		setSessionInfoObject(request, sessionKey, value);
	}

	/**
	 * 글자 수 만큼만 노출하고 그 외에는 "..." 처리한다.
	 * Desc :
	 * @Method Name : shortCutString
	 * @param str
	 * @param limit
	 * @return
	 */
    public static String shortCutString(String str, int limit) {
        return shortCutString(str, limit, "...");
    }

    /**
	 * 글자 수 만큼만 노출하고 그 외에는 endChracter 처리한다.
     * Desc :
     * @Method Name : shortCutString
     * @param str
     * @param limit
     * @param endChracter
     * @return
     */
    public static String shortCutString(String str, int limit, String endChracter) {
	    if (str == null || limit < 4) return str;
	    int len = str.length();
	    int cnt = 0, index = 0;
	    while(index < len && cnt < limit) {
	    	// if one byte character...
	        if(str.charAt(index++) < 256) {
	            cnt++;// increase one
	        } else {
	            cnt += 2;// increase two
	        }
	    }
	    if(index < len) {
	        str = str.substring(0, index) + endChracter;
	    }
	    return str;
    }

	/**
	 * [설명]
	 * s1을split하여 List로 리턴한다.
	 * @param String s1 String 문장
	 * @param String s2 split 기호
	 * @return List
	 */
	public static List<String> stringToList(String s1) {
		return stringToList(s1, ",");
	}

	/**
	 * [설명]
	 * s1을split하여 List로 리턴한다.
	 * @param String s1 String 문장
	 * @param String s2 split 기호
	 * @return List
	 */
	public static List<String> stringToList(String s1, String split) {
		List<String> list = new ArrayList<String>();
		if(s1 == null || s1.trim().equals("")) return list;
		String[] array = s1.split(split);
		for(int i = 0; array != null && i < array.length; i++) { list.add(array[i]); }
		return list;
	}

	/**
	 * 세션에서  제거한다.
	 * Desc :
	 * @Method Name : removeSessionInfoMap
	 * @param request
	 * @param sessionKey
	 */
	public static void removeSessionInfoMap(HttpServletRequest request, String sessionKey) {
		removeSessionInfoObject(request, sessionKey);
	}

	/**
	 * 세션에서  제거한다.
	 * Desc :
	 * @Method Name : removeSessionInfoMap
	 * @param request
	 * @param sessionKey
	 * @param key
	 */
	public static void removeSessionInfoMap(HttpServletRequest request, String sessionKey, String key) {
		HttpSession session = request.getSession();
		if(session.getAttribute(sessionKey) != null) {
			DataMap map = (DataMap)session.getAttribute(sessionKey);
			map.remove(key);
			session.removeAttribute(sessionKey);
			session.setAttribute(sessionKey, map);
		}
	}

	/**
	 * 세션에서  제거한다.
	 * Desc :
	 * @Method Name : removeSessionInfoObject
	 * @param request
	 * @param sessionKey
	 */
	public static void removeSessionInfoObject(HttpServletRequest request, String sessionKey) {
		HttpSession session = request.getSession();
		session.removeAttribute(sessionKey);
	}

	/**
	 * 세션에서  제거한다.
	 * Desc :
	 * @Method Name : removeSessionInfoSet
	 * @param request
	 * @param sessionKey
	 */
	public static void removeSessionInfoSet(HttpServletRequest request, String sessionKey) {
		removeSessionInfoObject(request, sessionKey);
	}

	public static DataMap getLnbMap(DataSet menuSet) throws Exception {
		DataMap lnbMap = new DataMap();
		DataSet lnbSet = null;
		if(menuSet != null && 0 < menuSet.size()) {
			String lnbKey = null;
			String[] columnNames = menuSet.getColumnNames();
			lnbSet = new DataSet(columnNames);
			menuSet.initRow();
			for(int i = 0; menuSet.next(); i++) {
				if("0".equals(menuSet.getString("PARENT_MENU_SEQ"))) {
					if(lnbKey == null) {
						lnbKey = menuSet.getString("MENU_SEQ");
					} else {
						lnbMap.put(lnbKey, lnbSet);
						lnbSet = new DataSet(columnNames);
						lnbKey = menuSet.getString("MENU_SEQ");
					}
				} else {
					lnbSet.appendData(menuSet.toDataMap());
				}
			}
			if(lnbSet != null && 0 < lnbSet.size()) {
				lnbMap.put(lnbKey, lnbSet);
			}
		}
		return lnbMap;
	}

	/**
	 * [설명]
	 * 사용법 : String sConditionName = WebUtils.xssFilter(request.getParameter("xss"));
	 * Cross Site Scrapping 을 막기 위한 유틸리티
	 * @param string
	 * @return valid string
	 */
	public static String xssFilter(String sInvalid) {
		String sValid = sInvalid;
		if (sValid == null || sValid.equals("")) {
			return "";
		}
		sValid = sValid.replaceAll("&", "&amp;");
		sValid = sValid.replaceAll("<", "&lt;");
		sValid = sValid.replaceAll(">", "&gt;");
		sValid = sValid.replaceAll("\"", "&qout;");
		sValid = sValid.replaceAll("\'", "&#039;");
		return sValid;
	}

	/**
	 * [설명]
	 * 좌측에서부터 원하는 길이를 자른다.
	 * ex) String result = CommonUtil.leftString("대한민국", 4);
	 *     result : 대한;
	 *
	 * @param String s 문자열
	 * @param int leng 좌측부터 가져와야 하는 자릿 수
	 * @return String
	 */
	public static String leftString(String s, int leng) {
		if(s == null || s.trim().equals("")) {
			return "";
		}
		byte[] a = s.getBytes();
		byte[] b = new byte[leng];
		for(int i = 0; i < leng; i++) {
			b[i] = a[i];
		}
		return new String(b);
	}

	/**
	 * [설명]
	 * 우측에서부터 원하는 길이를 자른다.
	 * ex) String result = CommonUtil.rightString("대한민국", 4);
	 *     result : 민국;
	 *
	 * @param String s 문자열
	 * @param int leng 우측부터 가져와야 하는 자릿 수
	 * @return String
	 */
	public static String rightString(String s, int leng) {
		if(s == null || s.trim().equals("")) {
			return "";
		}
		byte[] c = s.getBytes();
		byte[] d = new byte[leng];
		int idx = 0;
		for(int i = c.length-leng; i < c.length; i++) {
			d[idx++] = c[i];
		}
		return new String(d);
	}

	/**
	 * [설명]
	 * 시간을 Mask 처리한다.
	 * ex) System.out.println(CommonUtil.maskTimeKor("1250"));
	 *     result : 12시 50분
	 *     System.out.println(CommonUtil.maskTimeKor("125035"));
	 *     result : 12시 50분 35초
	 * @param String time 시간
	 * @param char pattern 형식
	 * @return String 치환된 결과.
	 */
	public static String maskTimeKor(String time) {
		if(time == null || time.trim().length() == 0) return "";
		StringBuffer newTime = new StringBuffer();
		switch(time.length()) {
		case 4:
			newTime.append(time.substring(0,2)).append("시 ").append(time.substring(2)).append("분 ");
			break;
		case 6:
			newTime
				.append(time.substring(0,2)).append("시 ")
				.append(time.substring(2,4)).append("분 ")
				.append(time.substring(4)).append("초");
			break;
		default:
			newTime.append(time);
			break;
		}
		return newTime.toString();
	}

	/**
	 * [설명]
	 * 시간을 Mask 처리한다.
	 * ex) System.out.println(CommonUtil.maskTime("1250", ':'));
	 *     result : 12:50.
	 *     System.out.println(CommonUtil.maskTime("125035", ':'));
	 *     result : 12:50:35.
	 * @param String time 시간
	 * @param char pattern 형식
	 * @return String 치환된 결과.
	 */
	public static String maskTime(String time, char pattern) {
		if(time == null || time.trim().length() == 0) return "";
		StringBuffer newTime = new StringBuffer();
		switch(time.length()) {
		case 4:
			newTime.append(time.substring(0,2)).append(pattern).append(time.substring(2));
			break;
		case 6:
			newTime
				.append(time.substring(0,2)).append(pattern)
				.append(time.substring(2,4)).append(pattern)
				.append(time.substring(4));
			break;
		default:
			newTime.append(time);
			break;
		}
		return newTime.toString();
	}

    /**
     * [설명]
     * Mask 처리한다. '?'와 '*'만 처리함.
     * ex) System.out.println(CommonUtil.mask("1235468790", "??-***-***-??"));
     *     result : 123-***-***-90
     * @param String string 마스킹 대상
     * @param String pattern 패턴
     * @return String 치환된 결과.
     */
    public static String mask(String string, String pattern) {
        char[] account = string.toCharArray();
        ArrayList<String> cntQ = new ArrayList<String>();
        char c = ' ';
        int cnt = 0;
        for(int i = 0; i < pattern.length(); i++) {
            c = pattern.charAt(i);
            if(c == '?' || c == '*') {
                if(c == '?') cntQ.add(String.valueOf(account[cnt]));
                cnt++;
            }
        }
        StringBuffer sb = new StringBuffer();
        c = ' ';
        cnt = 0;
        for(int i = 0; i < pattern.length(); i++) {
            c = pattern.charAt(i);
            if(c == '?') {
                sb.append((String)cntQ.get(cnt++));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

	/**
	 * [설명]
	 * 날짜를 Mask 처리한다. 시, 분, 초의 순서로 데이타로 넘겨야 한다.(12시 30분 25초 -> 123025)
	 * ex) System.out.println(CommonUtil.maskTime("123025", ':');
	 *     result : 12:30:25.
	 *     System.out.println(CommonUtil.maskDate("1230", ':'));
	 *     result : 2004/04/05.
	 * @param cardNo 카드번호
	 * @return String 치환된 결과.
	 */
	public static String maskDate(String date, char pattern) {
		if(date == null || date.trim().length() == 0) return null;
		StringBuffer newDate = new StringBuffer();
		switch(date.length()) {
		case 6:
			newDate
				.append(date.substring(0,2)).append(pattern)
				.append(date.substring(2,4)).append(pattern)
				.append(date.substring(4));
			break;
		case 8:
			newDate
				.append(date.substring(0,4))
				.append(pattern).append(date.substring(4,6)).append(pattern)
				.append(date.substring(6));
			break;
		default:
			newDate.append(date);
			break;
		}
		return newDate.toString();
	}

    /**
    * [설명]
    * 비밀번호 Mask 처리한다.
    * ex) System.out.println(CommonUtil.maskPassword("12344556677890"));
    *     result : 123***********
    * @param String password 비밀번호
    * @return String 치환된 결과.
    */
    public static String maskPassword(String password) {
        if(password == null || password.trim().length() == 0) return "";
        StringBuffer account = new StringBuffer();
        for(int i = 0; i < password.length(); i++) {
            if(2 < i) {
            	account.append("*");
            } else {
            	account.append(password.charAt(i));
            }
        }
        return account.toString();
    }

	/**
	 * [설명]
	 * 전체 문장에서 특정한 범위의 문자를 치환한다.
	 * ex) String s = "There is a different text.";
	 *     System.out.println("1 >>>>>>>>"+s+" : "+s.length());
	 *     s = replace(s, "size", 21, 4);
	 *     System.out.println("2 >>>>>>>>"+s+" : "+s.length());
	 *     result : There is a different size.
	 * @param allString 전체 문장
	 * @param newString 바꿀 문장
	 * @param start     시작위치(0부터 시작하는 것에 주의)
	 * @param end       종료위치(0부터 시작하는 것에 주의)
	 * @return String 치환된 결과.
	 * @exception Exception 메소드 수행시 발생한 모든 에러.
	 */
	public static String replace(String allString, String newString, int start, int end) throws Exception {
		StringBuffer sb = new StringBuffer();
		int idx = 0;
		char[] chars = newString.toCharArray();
		char c;
		for(int i = 0; i < allString.length(); i++) {
			c = allString.charAt(i);
			if(start <= i && i <= start+end-1) {
				sb.append(chars[idx++]);
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	/**
	 * [설명]
	 * 문자열 변환한다.
	 * ex) System.out.println(CommonUtil.replace("statement", "t", "a"));
	 *     result : saaaemena
	 * @param String line 원래 문장.
	 * @param String oldString 바꿀 문장.
	 * @param String newString 바뀔 문장.
	 * @return String 치환된 결과.
	 */
	public static String replace(String line, String oldString, String newString) {
		for(int index = 0; (index = line.indexOf(oldString, index)) >= 0; index += newString.length()) {
			line = line.substring(0, index) + newString + line.substring(index + oldString.length());
		}
		return line;
	}

	public static String replace(String statement, String string) {
		return replace(statement, new String[]{string});
	}

	/**
	 * [설명]
	 * 문장 내의 번역데이타를 치환한다.
	 * ex) String string = "안녕하세요? @1님의 결제일은 @2일 입니다. 결제금액은 @3원 입니다.";
	 *     String[] array = new String[]{"장원규", "25", "1000000"};
	 *     System.out.println(CommonUtil.replace(string, array));
	 *     result : 안녕하세요? 장원규님의 결제일은 25일 입니다. 결제금액은 1000000원 입니다.
	 * @param String statement 전체 문장
	 * @param String[] array 치환될 값.
	 * @return String 치환된 결과.
	 */
	public static String replace(String statement, String[] array) {
		if(statement.indexOf("@") == -1) return statement;
		StringBuffer strWord = new StringBuffer();
		int strLeng = statement.length();
		char strChar;
		StringBuffer bufWord = new StringBuffer();
		String bufString = null;
		int bufLeng;
		char bufChar;
		boolean bBreak = false;
		for(int i = 0; i < strLeng; i++) {
			strChar = statement.charAt(i);
			if(strChar == '@') {
				bufWord.setLength(0);
				bufWord = new StringBuffer();
				bufString = statement.substring(i+1);
				bufLeng = bufString.length();
				for(int j = 0; j < bufLeng; j++) {
					bufChar = ' ';
					bufChar = bufString.charAt(j);
					switch(bufChar) {
					case '1': case '2': case '3': case '4': case '5':
					case '6': case '7': case '8': case '9': case '0':
						bBreak = true;
						strWord.append(array[Integer.parseInt(String.valueOf(bufChar))-1]);
						i++;
						break;
					}
					if(bBreak) break;
				}
			} else {
				strWord.append(strChar);
			}
		}
		return strWord.toString();
	}

	/**
	 * [설명]
	 * 오늘, 어제, 1주전, 2주전, 3주전, 4주전 데이타를 가져옴
	 * 기준일은 항상 오늘임.
	 * yyyy-mm-dd 의 형식임.
	 *
	 * 일 기준으로 조회를 할 경우는 d를 붙인다.
	 * ex) CommonUtil.getTermDate("3d");// 미래
	 *     CommonUtil.getTermDate("-7d");// 과거
	 *
	 * 월 기준으로 조회를 할 경우는 m을 붙인다.
	 * ex) CommonUtil.getTermDate("2m");// 미래
	 *     CommonUtil.getTermDate("-1m");// 과거
	 *
	 * 년 기준으로 조회를 할 경우는 y을 붙인다.
	 * ex) CommonUtil.getTermDate("3y");// 미래
	 *     CommonUtil.getTermDate("-2y");// 과거
	 *
	 * @param String val 요청 값
	 * @return String 결과.
	 */
	public static String getTermDate(String val) {
		return getTermDate(val, ' ');
	}

	/**
	 * [설명]
	 * 오늘, 어제, 1주전, 2주전, 3주전, 4주전 데이타를 가져옴
	 * 기준일은 항상 오늘임.
	 * yyyy-mm-dd 의 형식임.
	 *
	 * 일 기준으로 조회를 할 경우는 d를 붙인다.
	 * ex) CommonUtil.getTermDate("3d", '-');// 미래
	 *     CommonUtil.getTermDate("-7d", '/');// 과거
	 *
	 * 월 기준으로 조회를 할 경우는 m을 붙인다.
	 * ex) CommonUtil.getTermDate("2m", '-');// 미래
	 *     CommonUtil.getTermDate("-1m", '/);// 과거
	 *
	 * 년 기준으로 조회를 할 경우는 y을 붙인다.
	 * ex) CommonUtil.getTermDate("3y", '-');// 미래
	 *     CommonUtil.getTermDate("-2y", '/);// 과거
	 *
	 * @param String val 요청 값
	 * @param char pattern 패턴
	 * @return String 결과.
	 */
	public static String getTermDate(String val, char pattern) {
		if(nvl(val).equals("")) return val;
		int term = 0;
		try {
			String s = val.substring(0, val.length()-1);
			if(s.length() == 0) return val;
			term = Integer.parseInt(s);
		} catch(Exception e) {
			return val;
		}
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		if(val.toLowerCase().endsWith("d")) {
			gregorianCalendar.add(Calendar.DATE, term);
		} else if(val.toLowerCase().endsWith("m")) {
			gregorianCalendar.add(Calendar.MONTH, term);
		} else if(val.toLowerCase().endsWith("y")) {
			gregorianCalendar.add(Calendar.YEAR, term);
		} else {
			return val;
		}
		Date date = new Date(gregorianCalendar.getTimeInMillis());
		if(pattern == ' ') {
			return date.toString();
		} else {
			return maskDate(replace(date.toString(), "-", ""), pattern);
		}
	}

	/**
	 * [설명]
	 * 오늘, 어제, 1주전, 2주전, 3주전, 4주전 데이타를 가져옴
	 * 기준일을 입력받아 해당 기준일을 기준으로 날짜를 체크함 (YYYYMMDD로 반환)
	 * yyyy-mm-dd 의 형식임.
	 *
	 * 일 기준으로 조회를 할 경우는 d를 붙인다.
	 * ex) CommonUtil.getTermDate2("3d", '-');// 미래
	 *     CommonUtil.getTermDate2("-7d", '/');// 과거
	 *
	 * 월 기준으로 조회를 할 경우는 m을 붙인다.
	 * ex) CommonUtil.getTermDate2("2m", '-');// 미래
	 *     CommonUtil.getTermDate2("-1m", '/);// 과거
	 *
	 * 년 기준으로 조회를 할 경우는 y을 붙인다.
	 * ex) CommonUtil.getTermDate2("3y", '-');// 미래
	 *     CommonUtil.getTermDate2("-2y", '/);// 과거
	 *
	 * @param String val 요청 값
	 * @param char pattern 패턴
	 * @return String 결과.
	 */
	public static String getTermDate2(String val, String checkData) {
		if(nvl(val).equals("")) return val;
		int term = 0;
		try {
			String s = val.substring(0, val.length()-1);
			if(s.length() == 0) return val;
			term = Integer.parseInt(s);
		} catch(Exception e) {
			return val;
		}
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		//특정날짜로 설정
		gregorianCalendar.set(Integer.parseInt(checkData.substring(0,4)),Integer.parseInt(checkData.substring(4,6)) -1 ,Integer.parseInt(checkData.substring(6,8)) );
		if(val.toLowerCase().endsWith("d")) {
			gregorianCalendar.add(Calendar.DATE, term);
		} else if(val.toLowerCase().endsWith("m")) {
			gregorianCalendar.add(Calendar.MONTH, term);
		} else if(val.toLowerCase().endsWith("y")) {
			gregorianCalendar.add(Calendar.YEAR, term);
		} else {
			return val;
		}
		Date date = new Date(gregorianCalendar.getTimeInMillis());
		return replace(date.toString(), "-", "");

	}

	/**
	 * [설명]
	 * 오늘, 어제, 1주전, 2주전, 3주전, 4주전 데이타를 가져옴
	 * 기준일은 파라메터로 받은 값.(yyyy-mm-dd형식)
	 * yyyy-mm-dd 의 형식임.
	 *
	 * 일 기준으로 조회를 할 경우는 d를 붙인다.
	 * ex) CommonUtil.getTermDate("3d", '-');// 미래
	 *     CommonUtil.getTermDate("-7d", '/');// 과거
	 *
	 * 월 기준으로 조회를 할 경우는 m을 붙인다.
	 * ex) CommonUtil.getTermDate("2m", '-');// 미래
	 *     CommonUtil.getTermDate("-1m", '/);// 과거
	 *
	 * 년 기준으로 조회를 할 경우는 y을 붙인다.
	 * ex) CommonUtil.getTermDate("3y", '-');// 미래
	 *     CommonUtil.getTermDate("-2y", '/);// 과거
	 *
	 * @param String strDate 기준 날짜
	 * @param String val 요청 값
	 * @param char pattern 패턴
	 * @return String 결과.
	 */
	public static String getTermDate(String strDate, String val, char pattern) {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		if(nvl(strDate).equals("")) {
			return strDate;
		}else{
			String[] sDateArr = strDate.split("-");
			
			if(sDateArr.length != 3) return strDate;
			
			gregorianCalendar.set(GregorianCalendar.YEAR, Integer.parseInt( sDateArr[0] ));
			gregorianCalendar.set(GregorianCalendar.MONTH, Integer.parseInt( sDateArr[1] )-1);
			gregorianCalendar.set(GregorianCalendar.DATE, Integer.parseInt( sDateArr[2] ));
		}
		if(nvl(val).equals("")) return strDate;
		int term = 0;
		try {
			String s = val.substring(0, val.length()-1);
			if(s.length() == 0) return val;
			term = Integer.parseInt(s);
		} catch(Exception e) {
			return val;
		}
		if(val.toLowerCase().endsWith("d")) {
			gregorianCalendar.add(Calendar.DATE, term);
		} else if(val.toLowerCase().endsWith("m")) {
			gregorianCalendar.add(Calendar.MONTH, term);
		} else if(val.toLowerCase().endsWith("y")) {
			gregorianCalendar.add(Calendar.YEAR, term);
		} else {
			return val;
		}
		Date date = new Date(gregorianCalendar.getTimeInMillis());
		if(pattern == ' ') {
			return date.toString();
		} else {
			return maskDate(replace(date.toString(), "-", ""), pattern);
		}
	}
	
	/**
	 * "/"(slash)로 종료하지 않으면 "/"(slash)를 추가하여 리턴한다.
	 * Desc :
	 * @Method Name : lastFindSlash
	 * @param s
	 * @return
	 */
	public static String lastFindSlash(String s) {
		if(nvl(s).equals("") || s.endsWith("/")) {
			return s;
		} else {
			return s.concat("/");
		}
	}

    /**
     * [설명]
     * 테스트 DataSet을 생성
     * @param columnArr[], dataArr[][]
     * @return DataSet
     * @throws BizException
     */
    public static DataSet setTestDataSet(String columnArr[], String dataArr[][]) throws Exception {
        DataSet testSet = new DataSet(columnArr);
        testSet.initRow();
        for(int i = 0; i < dataArr.length; i++) {
            testSet.appendData(dataArr[i]);
        }
        return testSet;
    }

    /**
     * 타입에 해당하는 CSS 리턴한다.
     * Desc :
     * @Method Name : setCSS
     */
    public static String setCSS(String typeName) {
    	StringBuffer cssBuffer = new StringBuffer();
    	if(typeName.indexOf("_") > -1) {
    		typeName = typeName.split("_")[0];
    	}
    	cssBuffer
    		.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/")
    		.append(typeName)
    		.append("/base.css\" />\n")
    		.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/")
    		.append(typeName)
    		.append("/layout01.css\" />")
    	;
    	return cssBuffer.toString();
    }

    /**
     * 특정 클래스의 내용을 invoke
     * @param obj(Method Invoke할 오브젝트)
     * @param methodName(Method Name)
     * @param objList(Parameter Object List)
     * @return
     */
    public static Object invoke(Object obj, String methodName, Object[] objList) throws Exception {
    	Method[] methods = obj.getClass().getMethods();

    	for(int i=0; i<methods.length; i++) {
    		if(methods[i].getName().equals(methodName)) {
    			//try {
    				if (methods[i].getReturnType().getName().equals("void")) {
    					methods[i].invoke(obj, objList);
    				} else {
    					return methods[i].invoke(obj, objList);
    				}
//    			} catch(IllegalAccessException lae) {
//    				//lae.printStackTrace();
//    				throw lae;
//    			} catch(InvocationTargetException ite) {
//    				//ite.printStackTrace();
//    				throw ite;
//    			}
    		}
    	}
    	return null;
    }

    /**
     * 요일 인덱스 값을 받아 요일명을 반환한다.
     *
     * @param dayOfWeekIdx
     * @return strDayOfWeek
     */
    public static String getDayOfWeekName(int dayOfWeekIdx){
    	String strDayOfWeek = "";
    	
    	switch(dayOfWeekIdx){
    		case 1:
    			strDayOfWeek = "일";
    			break;
    		case 2:
    			strDayOfWeek = "월";
    			break;
    		case 3:
    			strDayOfWeek = "화";
    			break;
    		case 4:
    			strDayOfWeek = "수";
    			break;
    		case 5:
    			strDayOfWeek = "목";
    			break;
    		case 6:
    			strDayOfWeek = "금";
    			break;
    		case 7:
    			strDayOfWeek = "토";
    			break;    			
    	}
    	
    	return strDayOfWeek;
    }
    
	/**
	 * 넘겨받은 날짜와 비교하여 오늘로 termDate 전날짜와 비교하여 New 이미지를 반환한다.
	 * Desc :
	 * @Method Name : getNewImage
	 * @return strDate, delayDay
	 * @throws
	 */
    public static String getNewImage(String strDate, String termDate) throws Exception {
    	String newImage = "";
    	int paramDate = Integer.parseInt(strDate.replace(".", ""));					// 넘겨받은 날짜
    	int intDate = Integer.parseInt(getTermDate("-"+termDate).replace("-", ""));	// 오늘에 termDate 날짜
    	if(paramDate > intDate){
    		newImage = "<img src=\"/images/icon/iconNew.gif\" class=\"iconNew\" alt=\"new\" />";
    	}
    	return newImage;
	}
    
	public static Map<?, ?> convertResultSetMap(Map<?, ?> map) throws Exception {
    	DataMap dataMap = new DataMap();
    	if(map != null && !map.isEmpty() && 0 < map.size()) {
    		dataMap.setResultSet(map);
    	}
		return dataMap;
	}    
    
	public static DataSet convertResultDataSet(List<?> list) throws Exception {
		DataSet dataSet = null;
		try {
			for(int i = 0; list != null && !list.isEmpty() && i < list.size(); i++) {
				DataMap map = (DataMap)convertResultSetMap((HashMap<?, ?>)list.get(i));
				if(i == 0) {
					dataSet = new DataSet(map.getKeysName());
				}
				dataSet.appendData(map);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		if(dataSet == null) {
			dataSet = new DataSet();
		}
		return dataSet;
	}

	/**
	 * 넘겨받은 권한코드로 해당 권한명을 반환한다.
	 * Desc :
	 * @Method Name : getAdminAuthName
	 * @return String
	 * @throws
	 */
	public static String getAdminAuthName(HttpServletRequest request, String mgrAuth){
		String authName = "";
		
		if(mgrAuth != null && !"".equals(mgrAuth)){
			switch(Integer.parseInt(mgrAuth)){
				case 1:
					authName = getLocaleText(request, "COMMON.MGR_AUTH1");
					break;
				case 2:
					authName = getLocaleText(request, "COMMON.MGR_AUTH2");
					break;
				case 3:
					authName = getLocaleText(request, "COMMON.MGR_AUTH3");
					break;
			}
		}
		return authName;
	}

	/**
	 * 넘겨받은 REQUEST와 다국어키를 가지고 properties에서 다국어를 반환한다.
	 * Desc :
	 * @Method Name : getLocaleText
	 * @return String
	 * @throws
	 */	
    public static String getLocaleText(HttpServletRequest request, String propKey) {
    	String strResult = "";
    	String LOCALE_CODE = (String)getSessionInfoObject(request, "LOCALE_CODE");
    	String LOCALE_UNDER = "";
    	if(LOCALE_CODE != null && !"".equals(LOCALE_CODE)){
    		LOCALE_UNDER = "_" + LOCALE_CODE;
    	}    	
    	
        Properties p = new Properties(); 
        try{
	        ClassLoader loader = Thread.currentThread().getContextClassLoader();
	        File file = new File(loader.getResource("").getFile());
	        String path = file.getParent() + File.separator + "classes" + File.separator + "locale" + File.separator;
	        
	        p.load(new FileInputStream(path +"locale"+LOCALE_UNDER+".properties"));
	        
	        strResult = new String(nvl(p.getProperty(propKey)).getBytes("ISO-8859-1"), "UTF-8");
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        return strResult;
    }
    
	/**
	 * 넘겨받은 LIST를 다국어에 맞게 PUSH MSG를 포함하여 반환한다.
	 * Desc :
	 * @Method Name : setPushMsgList
	 * @return String
	 * @throws
	 */
    public static List setPushMsgList(HttpServletRequest req, List list) throws Exception{
		List resultList = null;
		
		if(list != null && !list.isEmpty()){
			resultList = new ArrayList();
			for(int i = 0; i < list.size(); i++){
				HashMap hashMap = (HashMap)list.get(i);
				if(hashMap.get("PUSH_STATUS") != null && !"".equals(hashMap.get("PUSH_STATUS"))){
					hashMap.put("PUSH_STATUS", "[<b>" + hashMap.get("PUSH_STATUS") + "</b>] " + CommonUtil.getLocaleText(req, "COMMON.PUSH_CODE_"+hashMap.get("PUSH_STATUS")));
				}
				if(hashMap.get("GCM_STATUS") != null && !"".equals(hashMap.get("GCM_STATUS"))){
					hashMap.put("GCM_STATUS", "[<b>" + hashMap.get("GCM_STATUS") + "</b>] " + CommonUtil.getLocaleText(req, "COMMON.PUSH_CODE_"+hashMap.get("GCM_STATUS")));
				}
				if(hashMap.get("APNS_STATUS") != null && !"".equals(hashMap.get("APNS_STATUS"))){
					hashMap.put("APNS_STATUS", "[<b>" + hashMap.get("APNS_STATUS") + "</b>] " + CommonUtil.getLocaleText(req, "COMMON.PUSH_CODE_"+hashMap.get("APNS_STATUS")));
				}
				resultList.add(hashMap);
			}			
		}else{
			resultList = list;
		}
		
		return resultList;
	}
    
	/**
	 * 넘겨받은 LIST를 기간 범위에 맞게 세팅하여 반환한다.
	 * Desc :
	 * @Method Name : setTermList
	 * @return List
	 * @throws
	 */
    public static List setTermList(List list, int term) throws Exception{
		List resultList = new ArrayList();
		
		for(int i = 0; i <= term; i++){
			HashMap rowMap = new HashMap();
			if(i == 0){
				rowMap.put("CRTE_DATE_TEXT", CommonUtil.getTime("yyyy-MM-dd"));
				rowMap.put("NEAREST7_PUSH_SEND", 0);
				rowMap.put("NEAREST7_GCM_SEND", 0);
				rowMap.put("NEAREST7_APNS_SEND", 0);
			}else{
				rowMap.put("CRTE_DATE_TEXT", CommonUtil.getTermDate("-"+i+"d", '-'));
				rowMap.put("NEAREST7_PUSH_SEND", 0);
				rowMap.put("NEAREST7_GCM_SEND", 0);
				rowMap.put("NEAREST7_APNS_SEND", 0);				
			}
			resultList.add(rowMap);
		}
		
		if(list != null && !list.isEmpty()){
			for(int y = 0; y < resultList.size(); y++){
				HashMap resultMap = (HashMap)resultList.get(y);
				for(int j = 0; j < list.size(); j++){
					HashMap dataMap = (HashMap)list.get(j);
					
					if(resultMap.get("CRTE_DATE_TEXT").equals(dataMap.get("CRTE_DATE_TEXT"))){
						resultMap.put("NEAREST7_PUSH_SEND", addBigDecimal(dataMap.get("PUSH_SUCCESS").toString(), dataMap.get("PUSH_FAIL").toString()));
						resultMap.put("NEAREST7_GCM_SEND", addBigDecimal(dataMap.get("GCM_SUCCESS").toString(), dataMap.get("GCM_FAIL").toString()));
						resultMap.put("NEAREST7_APNS_SEND", addBigDecimal(dataMap.get("APNS_SUCCESS").toString(), dataMap.get("APNS_FAIL").toString()));
					}
				}
			}
		}
		
		return resultList;
	}
    
	/**
	 * 넘겨받은 LIST를 검색일자 범위에 맞게 세팅하여 반환한다.
	 * Desc :
	 * @Method Name : setDateTermList
	 * @return List
	 * @throws
	 */
    public static List setDateTermList(List list, String startDate, String endDate) throws Exception{
    	/* 검색기간 사이에 날짜 구하기 시작 */
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    	Calendar cal = Calendar.getInstance(); 

    	try { 
    		cal.setTime(sdf.parse(startDate)); 
    	} catch (Exception e) { 
    	} 

    	int count = getDiffDayCount(startDate, endDate); 
    	// 시작일부터 
    	cal.add(Calendar.DATE, -1); 
    	// 데이터 저장 
    	ArrayList<String> dateList = new ArrayList<String>(); 

    	for (int i = 0; i <= count; i++) { 
	    	cal.add(Calendar.DATE, 1); 
	    	dateList.add(sdf.format(cal.getTime())); 
    	} 

    	String[] resultArr = new String[dateList.size()]; 
    	dateList.toArray(resultArr);
    	/* 검색기간 사이에 날짜 구하기 종료 */
    	
    	
		List resultList = new ArrayList();
		DataSet dataSet = null;
		
		if(list != null && !list.isEmpty()){
			dataSet = (DataSet)convertResultDataSet(list);
			
			for(int i = 0; resultArr != null && i < resultArr.length; i++){
				HashMap rowMap = new HashMap();
				rowMap.put("CRTE_DATE_TEXT", resultArr[i]);
				rowMap.put("CRTE_DATE", resultArr[i].replaceAll("-", ""));
				rowMap.put("ALL_SUCCESS", 0);
				rowMap.put("ALL_FAIL", 0);
				rowMap.put("PUSH_SUCCESS", 0);
				rowMap.put("PUSH_FAIL", 0);
				rowMap.put("GCM_SUCCESS", 0);
				rowMap.put("GCM_FAIL", 0);
				rowMap.put("APNS_SUCCESS", 0);
				rowMap.put("APNS_FAIL", 0);		
				
				dataSet.initRow();
				while(dataSet.next()){
					if(rowMap.get("CRTE_DATE").equals(dataSet.getString("CRTE_DATE"))){
						rowMap.put("ALL_SUCCESS", dataSet.getNVLString("ALL_SUCCESS", "0"));
						rowMap.put("ALL_FAIL", dataSet.getNVLString("ALL_FAIL", "0"));
						rowMap.put("PUSH_SUCCESS", dataSet.getNVLString("PUSH_SUCCESS", "0"));
						rowMap.put("PUSH_FAIL", dataSet.getNVLString("PUSH_FAIL", "0"));
						rowMap.put("GCM_SUCCESS", dataSet.getNVLString("GCM_SUCCESS", "0"));
						rowMap.put("GCM_FAIL", dataSet.getNVLString("GCM_FAIL", "0"));
						rowMap.put("APNS_SUCCESS", dataSet.getNVLString("APNS_SUCCESS", "0"));
						rowMap.put("APNS_FAIL", dataSet.getNVLString("APNS_FAIL", "0"));						
					}
				}
				resultList.add(rowMap);
			}
		}
		
		return resultList;
	} 
    
    /** 
     * 두날짜 사이의 일수를 리턴 
     * 
     * @param fromDate 
     * yyyyMMdd 형식의 시작일 
     * @param toDate 
     * yyyyMMdd 형식의 종료일 
     * @return 두날짜 사이의 일수 
     */ 
    public static int getDiffDayCount(String fromDate, String toDate) { 
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 

    	try { 
    		return (int) ((sdf.parse(toDate).getTime() - sdf.parse(fromDate) 
    				.getTime()) / 1000 / 60 / 60 / 24); 
    	} catch (Exception e) { 
    		return 0; 
    	} 
    } 

	/**
	 * 백분율을 구하고 파라미터에 맞게 절삭, 반올림, 반내림 하여 반환한다.
	 * Desc :
	 * @Method Name : getPercent
	 * @return String
	 * @throws
	 */    
    public static String getPercent(int total, int data, int scale, int round) {
    	String strResult = "0";
    	double doTotal = 0;
    	double doData = 0;
    	double doPercent = 0;
    	
    	if(total > 0){
        	doTotal = Double.parseDouble(String.valueOf(total));
        	doData = Double.parseDouble(String.valueOf(data));
        	doPercent = (doData/doTotal)*100;    		
        	strResult = scaleBigDecimal(String.valueOf(doPercent), scale, round);
    	}
    	
    	return strResult;
    }
    
    /**
	 * 현재 년월 - YYYYMM
	 */
	public static String getCurrentMonth() {
		String month;

		Calendar cal = Calendar.getInstance();

		StringBuffer buf = new StringBuffer();

		buf.append(Integer.toString(cal.get(Calendar.YEAR)));
		month = Integer.toString(cal.get(Calendar.MONTH) + 1);
		if (month.length() == 1)
			month = "0" + month;

		buf.append(month);

		return buf.toString();
	}

	/**
	 * 현재 년월일 - YYYYMMDD
	 */
	public static String getCurrentDate() {
		String month, day;

		Calendar cal = Calendar.getInstance();

		StringBuffer buf = new StringBuffer();

		buf.append(Integer.toString(cal.get(Calendar.YEAR)));
		month = Integer.toString(cal.get(Calendar.MONTH) + 1);
		if (month.length() == 1)
			month = "0" + month;
		day = Integer.toString(cal.get(Calendar.DATE));
		if (day.length() == 1)
			day = "0" + day;

		buf.append(month);
		buf.append(day);

		return buf.toString();
	}

	/**
	 * 현재 시간 - HHMISS
	 */
	public static String getCurrentTime() {
		String hour, min, sec;

		Calendar cal = Calendar.getInstance();

		StringBuffer buf = new StringBuffer();

		hour = Integer.toString(cal.get(Calendar.HOUR_OF_DAY));
		if (hour.length() == 1)
			hour = "0" + hour;

		min = Integer.toString(cal.get(Calendar.MINUTE));
		if (min.length() == 1)
			min = "0" + min;

		sec = Integer.toString(cal.get(Calendar.SECOND));
		if (sec.length() == 1)
			sec = "0" + sec;

		buf.append(hour);
		buf.append(min);
		buf.append(sec);

		return buf.toString();
	}
	
	public static String gf_FormatNumber(String strNumber, int intPoint, String strNumberType, String ratioStr, String offsetStr)
    {
		float pureData, ratio, offset;
		
		// 데이터가 없거나 NULL 이면...
        if (strNumber==null || strNumber.equals("") || strNumber.equals("null") || !isNumber(strNumber)){
            return "-";
        }else {
        	pureData = Float.valueOf(strNumber);
        	if(ratioStr != null && !ratioStr.equals("")) {
        		pureData *= Float.valueOf(ratioStr);
        	}
        	if(offsetStr != null && !offsetStr.equals("")) {
        		pureData += Float.valueOf(offsetStr);
        	}
        	return gf_FormatNumber(String.valueOf(pureData), intPoint, strNumberType);
        }
		
    }
	/// <summary>
    /// 숫자의 자릿수 마다 표시(콤마와 소숫점)
    /// </summary>
    /// <param name="dclNumber">표시할 숫자</param>
    /// <param name="intPoint">소숫점을 찍을 위치</param>
    /// <param name="strNumberType">올림, 반올림, 버림</param>
    /// <returns></returns>
    public static String gf_FormatNumber(String strNumber, int intPoint, String strNumberType)
    {
        double dblTempNumber = 0;
        double dblNumberType = 0;
        String strReturnNumber = "";
        DecimalFormat fmt = null;

        // 데이터가 없거나 NULL 이면...
        if (strNumber==null || strNumber.equals("") || strNumber.equals("null") || !isNumber(strNumber)){
            return "-";
        }
        else
        {
        	
            switch (intPoint)
            {
                case 0:
                	fmt = new DecimalFormat("#,###,###,##0");
                    dblNumberType = 0;
                    break;

                case 1:
                	fmt = new DecimalFormat("#,###,###,##0.0");
                    dblNumberType = 0.05;
                    break;

                case 2:
                	fmt = new DecimalFormat("#,###,###,##0.00");
                    dblNumberType = 0.005;
                    break;

                case 3:
                	fmt = new DecimalFormat("#,###,###,##0.000");
                    dblNumberType = 0.0005;
                    break;

                case 4:
                	fmt = new DecimalFormat("#,###,###,##0.0000");
                    dblNumberType = 0.00005;
                    break;

                case 5:
                	fmt = new DecimalFormat("#,###,###,##0.00000");
                    dblNumberType = 0.000005;
                    break;

                case 6:
                	fmt = new DecimalFormat("#,###,###,##0.000000");
                    dblNumberType = 0.0000005;
                    break;

                default:
                	fmt = new DecimalFormat("#,###,###,##0");
                    dblNumberType = 0;
                    break;
            }

            switch (strNumberType)
            {
                case "U": // 올림
                    dblTempNumber = Double.parseDouble(strNumber) + dblNumberType;
                    break;

                case "M": // 반올림
                    dblTempNumber = Double.parseDouble(strNumber);
                    break;

                case "D": // 버림
                    dblTempNumber = Double.parseDouble(strNumber) - dblNumberType;
                    break;
            }

            strReturnNumber = fmt.format(dblTempNumber);


            return strReturnNumber;
        }
    }
	
  /// <summary>
    /// 입력된 한계수치보다 낮으면 글자색을 바꿈
    /// </summary>
    /// <param name="strData">수치데이터</param>
    /// <param name="dblLimit">입력된 한계수치</param>
    /// <returns></returns>
    public static String gf_LimitCheck(String strData, String strErrorRange, String strMiddleLimit, String strMaximumLimit)
    {
        String strLimitCheckResult = "";
        
        try
        {
            String strMiddle = "#FFDF24";
            String strMaximum = "#FF9090";
            String strNormal = "#FFFFFF";
            
            // 데이터가 없거나 NULL 이면...
            if (strData==null || strData.equals("") || strData.equals("null")){
                return "-";
            }
            
            //값들 중 하나라도 존재하지 않으면 ...
            if((strErrorRange==null || "".equals(strErrorRange))
        			|| (strMiddleLimit==null || "".equals(strMiddleLimit))
        			|| (strMaximumLimit==null || "".equals(strMaximumLimit))
            			) {
            	return "-";
            }
            
            Float fData = Float.parseFloat(strData);
            Float fLow = Float.parseFloat(strMiddleLimit);
            Float fHigh = Float.parseFloat(strMaximumLimit);
            Float fErrorRange = Float.parseFloat(strErrorRange);
            
            Float fLowest = fLow - fErrorRange;	 //최소 값
            Float fHighest = fHigh + fErrorRange; //최대 값
            
            //최소보다 작거나 최대보다 큰
            if(fData<fLowest || fData>fHighest) {
            	strLimitCheckResult = strMaximum;
            //소보다 작거나 대보다 큰
            }else if(fData<fLow || fData>fHigh) {
            	strLimitCheckResult = strMiddle;
            }else {
            	strLimitCheckResult = strNormal;
            }
            
            // 설정한 수치보다 데이터가 높을 때...
            /*if (strStandart.equals("H"))
            {
                if ((dclData > dclMiddleLimit) && (dclData <= dclMaximumLimit))
                	strLimitCheckResult = strMiddle;
                else if (dclData > dclMaximumLimit)
                	strLimitCheckResult = strMaximum;
                else
                	strLimitCheckResult = strNormal;
            }
            else // 설정한 수치보다 데이터가 낮을 때...
            {
                if ((dclData < dclMiddleLimit) && (dclData >= dclMaximumLimit))
                	strLimitCheckResult = strMiddle;
                else if (dclData < dclMaximumLimit)
                	strLimitCheckResult = strMaximum;
                else
                	strLimitCheckResult = strNormal;
            }*/
            
            
        }
        catch(Exception e)
        {
            strLimitCheckResult = "#FFFFFF";
        }

        return strLimitCheckResult;
    }
    
    /// <summary>
    /// 데이터 변형후 리턴(TD) - 기존 c소스 변환=> result String format: value_with_backcolor
    /// </summary>
    /// <param name="strElementID">관측요소</param>
    /// <param name="strDate">데이터</param>
    /// <param name="strStandart">기준</param>
    /// <param name="strMiddleLimit">중간수치</param>
    /// <param name="strMaximumLimit">최대수치</param>
    /// <param name="strMeasure">단위</param>
    /// <param name="intPoint">소수점위치</param>
    /// <param name="strUMD">올림,반올림,버림 설정</param>
    /// <param name="strMeasureDisplayYN">단위표시여부</param>
    /// <returns></returns>
    public static String getDataTD(String strElementID, String strDate, String strErrorRange, String strMiddleLimit, String strMaximumLimit, String strMeasure, int intPoint, String strUMD, String strMeasureDisplayYN, String strStyle)
    {
       
        String strChangeData = "";
//        HtmlTableCell tdResultData = new HtmlTableCell();
        StringBuilder tdResultStr = new StringBuilder();
       
        // 데이터 표출방식 분류
        switch (strElementID.substring(0, 3))
        {
            case "JYB": // 10m풍향
                if (strMeasureDisplayYN.equals("Y"))
                    strChangeData = gf_FormatNumber(strDate, 0, strUMD) + strMeasure;
                else
                    strChangeData = gf_FormatNumber(strDate, 0, strUMD);
                
                tdResultStr.append(strChangeData);
                tdResultStr.append("_"+strStyle);
                break;

            case "JYD": // 10m돌풍풍향
                if (strMeasureDisplayYN.equals("Y"))
                    strChangeData = gf_FormatNumber(strDate, 0, strUMD) + strMeasure;
                else
                    strChangeData = gf_FormatNumber(strDate, 0, strUMD);
                
                tdResultStr.append(strChangeData);
                tdResultStr.append("_"+strStyle);
                break;
/*
            case "JYL1": // 2m풍향
                if (strMeasureDisplayYN.equals("Y"))
                    strChangeData = gf_FormatNumber(strDate, 0, strUMD) + strMeasure;
                else
                    strChangeData = gf_FormatNumber(strDate, 0, strUMD);

                tdResultStr.append(strChangeData);
                tdResultStr.append("_"+strStyle);
                break;

*/
            case "JYL3": // 2m돌풍풍향
                if (strMeasureDisplayYN.equals("Y"))
                    strChangeData = gf_FormatNumber(strDate, 0, strUMD) + strMeasure;
                else
                    strChangeData = gf_FormatNumber(strDate, 0, strUMD);

                tdResultStr.append(strChangeData);
                tdResultStr.append("_"+strStyle);
                break;
          
            default:
                strChangeData = gf_FormatNumber(strDate, intPoint, strUMD);

                if (strMeasureDisplayYN.equals("Y"))
                	tdResultStr.append(strChangeData + strMeasure);
                else
                	tdResultStr.append(strChangeData);
                
                tdResultStr.append("_" + strStyle); //width
                tdResultStr.append("_" + gf_LimitCheck(strChangeData, strErrorRange, strMiddleLimit, strMaximumLimit)); //color
                break;
        }

        return tdResultStr.toString();
    }
    
    public static boolean isNumber(String str){
        boolean result = false; 
         
         
        try{
            Double.parseDouble(str) ;
            result = true ;
        }catch(Exception e){}
         
         
        return result ;
    }

    public static JSONObject reqDataParsing(HttpServletRequest req) throws Exception{
		
		JSONObject jsonObject = new JSONObject();
		StringBuffer jb = new StringBuffer();
		String line = null;
		
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			jsonObject.put("parseCode", "01");
			return jsonObject;
		}
	  	
		try {
			jsonObject = JSONObject.fromObject(jb.toString()); 
			//restlogger.info(" >>> request jsonObject ===>"+jsonObject.toString());
		    
		}catch (Exception e) {
			 jsonObject.put("parseCode", "02");
			 return jsonObject;
		}	
		
		jsonObject.put("parseCode", "00");
		
		return jsonObject;
	}

	/**
	 * web에서 넘겨 받은 JSON 객체를 HashMap 자료 구조의 객체로 파싱
	 * @param req HTTP 프로토콜로 넘겨 받은 Servlet request
	 * @return param
	 */
	public static HashMap<String, Object> reqDataParsingToHashMap(HttpServletRequest req) throws Exception {
		JSONObject parseJson = null;

		try {
			parseJson = reqDataParsing(req);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		HashMap<String, Object> param = new HashMap<>();
		Iterator keys = parseJson.keys();

		while (keys.hasNext()) {
			String key = (String) keys.next();
			param.put(key, parseJson.get(key));
		}

		return param;
	}

	/**
	 * web에서 넘겨 받은 JSON 객체 리스트를 JSONArray로 변환
	 * @param req HTTP 프로토콜로 넘겨 받은 Servlet request
	 * @return jsonArray
	 */
	public static JSONArray parseJSONArray(HttpServletRequest req) {
		JSONArray jsonArray = null;
		Object line;

		try {
			line = new BufferedReader(req.getReader()).readLine();
			jsonArray = JSONArray.fromObject(line);
		} catch (IOException e) {
			assert false;
			jsonArray.add("error");
		}
		return jsonArray;
	}

	/**
	 * JSON 객체를 HashMap 객체로 변환
	 * @param jsonObject JSON 객체
	 * @return param
	 */
	public static HashMap<String, Object> jsonObjectToHashMap(JSONObject jsonObject) {
		HashMap<String, Object> param;

		Iterator keys = jsonObject.keys();
		param = new HashMap<String, Object>();

		while (keys.hasNext()) {
			String key = (String) keys.next();
			param.put(key, jsonObject.get(key));
		}

		return param;
	}
    
    public static HashMap<String, Object> reqDataParsingToMap(HttpServletRequest req) throws Exception{
		
    	JSONObject parseJson = new JSONObject();
		parseJson = reqDataParsing(req);
    	
        HashMap<String, Object> param = new HashMap<String, Object>();
    	Iterator <String> keys = parseJson.keys();
    	while(keys.hasNext()) {
    	    String key = keys.next();
    	    try {
    	    	param.put(key.toLowerCase(), parseJson.get(key).toString());
    	    }catch(Exception e) {
    	    	e.printStackTrace();
    	    }
    	    	
    	}
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	try{
    		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    		param.put("login_user_id", user.getE_ID());	
    	}catch(Exception e) {
    		param.put("login_user_id", "NOTLOGIN");
    		e.printStackTrace();
    	}
		
		System.out.println("reqDataParsingToMap===>"+param.toString());
		return param;
	}    

    
    /**
     * <pre>
     * 설명 : ArrayList to jsonArray<hashMap>
     * 상세설명 : .
     * </pre>
      */
     @SuppressWarnings("static-access")
   	public static JSONArray ObjectToJson(ArrayList<HashMap<String, Object>> list)throws Exception {
// 		 long start = System.currentTimeMillis();

 		JSONArray jsonArray = new JSONArray();
 		if(list.size() > 0)
 		{
 			for(int i=0; i<list.size(); i++) 
 			{
 				Map<String, Object> map = (Map<String, Object>)list.get(i);
 				Set key = map.keySet();
 				
 				JSONObject json = new JSONObject();

 				for (Iterator iterator = key.iterator(); iterator.hasNext();) 
 				{
 	                 String keyName = (String) iterator.next();
 	                 String valueName = "";
 	                 if(null == map.get(keyName) || "".equals((String) map.get(keyName).toString()) ){
 	                	 valueName = "";
 	                 }else {
 	                	 valueName = (String) map.get(keyName).toString();
 	                 }
 	                 System.out.println("keyName/valueName===>" + keyName+"/"+valueName);
 	                 json.put(keyName, valueName);	 
 				}				
 				jsonArray.add( json );	
 				
 			}
 		}
 		  

// 	  long end = System.currentTimeMillis();

   		
           return jsonArray; 
       }        
     
    @SuppressWarnings("static-access")
 	public static HashMap<String, Object> getParameterMap(HttpServletRequest request) throws Exception {

    	HashMap<String, Object> paramMap = new HashMap<String, Object>();

         Enumeration<String> e = request.getParameterNames();
         while (e.hasMoreElements()) {
             String key = e.nextElement();
             String[] values = request.getParameterValues(key);
             if (values.length == 1) {
                 paramMap.put(key, values[0].trim());
             } else {
                 paramMap.put(key, values);
             }
         }
         
         /** Push Map to Request Attribute **/
		Enumeration<String> attributeNames = (Enumeration<String>)request.getAttributeNames();
		while (attributeNames.hasMoreElements()) {
			String key = attributeNames.nextElement();
			Object obj = request.getAttribute(key);
			if (obj instanceof java.lang.String) {
				paramMap.put(key, obj);
			}
		}
 		
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	try {
    		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    		paramMap.put("login_user_id", user.getE_ID());	
    	} catch (Exception ex) {
    		paramMap.put("login_user_id", "NOTLOGIN");
    		ex.printStackTrace();
    	}

		System.out.println("paramMap===>"+paramMap.toString()); 		

		return paramMap;
     }     
	
    @SuppressWarnings("static-access")
 	public static boolean getIpSatusInfo(HashMap<String, Object> param)throws Exception {
    	
    	HashMap<String, Object> infoMap = new HashMap<String, Object>();
        boolean isStatus = false;
		try {
		    
		    InetAddress pingcheck = InetAddress.getByName((String)param.get("server_ip"));
		    isStatus = pingcheck.isReachable(1000);
		    //System.out.println((String)param.get("server_ip") + " : " + pingcheck.isReachable(2000));
		    
		}catch (Exception e) {
		    System.out.println("핑 테스트 에러");
		    e.printStackTrace();
		}
		return isStatus;
    }
    
}
