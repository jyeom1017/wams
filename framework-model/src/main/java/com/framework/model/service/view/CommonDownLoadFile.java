package com.framework.model.service.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import com.usom.model.config.Configuration;
import com.usom.model.service.common.CommonService;

@Service
public class CommonDownLoadFile extends AbstractView {
	
	@Resource(name = "commonService")
	CommonService commonService;
	
	public CommonDownLoadFile() {
		setContentType("application/octet-stream; charset=UTF-8");
	}
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> param, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		int f_num = Integer.parseInt(param.get("f_num").toString());
		
		// file path/original name select
		HashMap<String, Object> fileMap = commonService.getFileInfo(f_num);
		
		String file_path = (String) fileMap.get("F_PATH");//F_NAME
//		file_path = request.getSession().getServletContext().getRealPath(file_path);
		String SERVER_PATH2 = Configuration.WEB_ROOT_PATH;
		file_path = SERVER_PATH2 + file_path;
		
		String imgExt = (String) fileMap.get("F_NAME");
		imgExt = imgExt.substring(imgExt.lastIndexOf(".") + 1, imgExt.length());
		
		String original_nm = (String) fileMap.get("F_TITLE");//F_NAME은 중복된 파일때문에 시간이 붙음.
		if (!original_nm.contains("." + imgExt)) { // 확장자가 없을 경우 확장자를 붙여서 다운로드 
			original_nm += "." + imgExt;
		}
		
		System.out.println("filePath" + file_path);
		
		File file = new File(file_path);
		
		
//		String userAgent = request.getHeader("User-Agent");
//		if (userAgent.indexOf("MSIE") > -1) {
			original_nm = URLEncoder.encode(original_nm, "utf-8");
//		} else {
//			original_nm = new String(original_nm.getBytes("utf-8"), "iso-8859-1");
//		}		
		
		
		response.setContentType(getContentType());
		response.setContentLength((int) file.length());
		//response.setHeader("Content-Disposition", "attachment; fileName=\""	+ new String(original_nm.getBytes("EUC-KR"),"8859_1") + "\";");
		response.setHeader("Content-Disposition", "attachment; fileName=\"" + original_nm + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		OutputStream out = response.getOutputStream();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			FileCopyUtils.copy(fis, out);
		} catch (IOException ex) {
			fis = null;
		} finally {
			if (fis != null)
				try {
					fis.close();
				} catch (IOException ex) {
					fis = null;
				}
		}
		out.flush();
	}
	
}
