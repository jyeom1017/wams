/*
 * http://javafactory.tistory.com/884 참고 
 */
package com.framework.model.service.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormats;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WritableCellFormat; 

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractJExcelView;

@Service
public class back_ExcelView extends AbstractJExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			WritableWorkbook wb, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

        WritableSheet sheet = wb.createSheet("Spring", 0);
		
        WritableCellFormat celFormat= new WritableCellFormat();  // 셀 서식 생성

        WritableCellFormat integerFormat = new WritableCellFormat (NumberFormats.INTEGER); // 정수형 서식 꼭 써야 sheet.addCell(new Number) 가능 하다.
        
		
        ArrayList<LinkedHashMap<Object, Object>> list = 
        		(ArrayList<LinkedHashMap<Object, Object>>) model.get("wordList");
        for(int i=0; i<list.size(); i++) {
        	
        	LinkedHashMap<Object, Object> map = list.get(i);

    		Iterator iterator = map.entrySet().iterator(); //iterator 컬랙션에 저장되어 있는 요소들을 읽어오늘 방법
    		int j = 0;
        	while (iterator.hasNext()==true ) { //hasNext()==현재위치에서 이동할 항목이 있는지 체크한다.. 즉 이동할 항목이 있으면 true 없으면 false
				Entry entry = (Entry) iterator.next();
				if(i == 0) {//header
					sheet.addCell(new Label(j, 0, entry.getKey().toString()));	
				}
				//contents
				
			/*
			 * 	System.out.println("value "+ entry.getValue());
			 *	System.out.println("자료형은? "+ entry.getValue().getClass().getName());
			 *	System.out.println("--------------------------------------------");
			 **/
	        	//System.out.println("i 번쨰 "+ i);	
				if(entry.getValue() instanceof Integer){					
					sheet.addCell(new Number(j, i+1, (Integer) entry.getValue()));
					// System.out.println("자료형은? "+ entry.getValue().getClass().getName());
					// System.out.println(" Integer 일떄"+entry.getValue());

					// System.out.println("-----------------");
				}else if(entry.getValue()==null || entry.getValue().equals("") ) {
						 sheet.addCell(new Label(j, i+1, "              "));		
						// System.out.println("자료형은? "+ entry.getValue().getClass().getName());
						// System.out.println(" null 이거나 '' 일떄"+entry.getValue());
						// System.out.println("-----------------");
				}else if(entry.getValue() instanceof Double){
					sheet.addCell(new Number(j, i+1, (Double) entry.getValue()));
					 //System.out.println("자료형은? "+ entry.getValue().getClass().getName());
					 //System.out.println(" Double 일떄"+entry.getValue());
					 //System.out.println("-----------------");

				}else if(entry.getValue() instanceof String){
					sheet.addCell(new Label(j, i+1, entry.getValue().toString()));		
					 //System.out.println("자료형은? "+ entry.getValue().getClass().getName());
					 //System.out.println(" String 일떄"+entry.getValue());
					// System.out.println("-----------------");
				}else {
					// System.out.println("자료형은? "+ entry.getValue().getClass().getName());
					// System.out.println(" 아무것도 안일떄"+entry.getValue());
					 //System.out.println("-----------------");
					sheet.addCell(new Label(j, i+1, ""));
				}
					
				j ++;
		         //System.out.println("j 번쨰 "+ j);
        	}//end while
        }
	}
	
}
