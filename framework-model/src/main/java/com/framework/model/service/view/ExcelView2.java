/*package com.framework.model.service.view;

import org.springframework.stereotype.Component;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.List;
import org.springframework.util.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import org.apache.ibatis.cache.decorators.WeakCache;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.ImageUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jfree.chart.imagemap.ImageMapUtilities;
import org.springframework.stereotype.Service;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.*;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.springframework.web.servlet.view.document.AbstractExcelView;
import com.framework.model.service.view.AbstractXlsView;

import com.usom.model.config.Configuration;

import net.sf.json.util.JSONTokener;
import net.sf.jxls.transformer.XLSTransformer;
import com.framework.model.service.excel.ExcelService;

import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

@Component("excelView2")
@RequiredArgsConstructor
public class ExcelView2 extends AbstractXlsView {
    
    private static final int MAX_ROW = 1040000;  
  
    private static final int PAGING_SIZE = 10000;
  
    //private final ExcelService excelService;
	@Resource(name = "ExcelService")
	public ExcelService excelService;

     
    @Override
    protected void buildExcelDocument(Map<String, Object> map, Workbook wb
            , HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> excelMap = (Map<String, Object>) map.get("excelMap");

        List<String> keys = (List<String>) excelMap.get("keys");
        List<String> headers = (List<String>) excelMap.get("headers");
        List<String> widths = (List<String>) excelMap.get("widths");
        List<String> aligns = (List<String>) excelMap.get("aligns");
        long listSize = (Long) excelMap.get("listSize");
        String fileName = (String) excelMap.get("fileName");

        String userAgent = request.getHeader("User-Agent");

        if (userAgent.contains("Trident")
                || (userAgent.indexOf("MSIE") > -1)) {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } else if (userAgent.contains("Chrome")
                || userAgent.contains("Opera")
                || userAgent.contains("Firefox")) {
            fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
        }
      
        SXSSFWorkbook sxssfWorkbook = null;
        
        for (int start = 0; start < listSize; start += PAGING_SIZE) {
            ArrayList<HashMap<String, Object>> list = excelService.getExcelList(start, PAGING_SIZE);
          
            sxssfWorkbook = getWorkbook(fileName, headers, keys, widths
                                        , aligns, list, start, sxssfWorkbook);
          
            list.clear(); // 리스트 페이징 처리 및 메모리 
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".xlsx");

        ServletOutputStream output = reponse.getOutputStream();
        output.flush();
        workbook.write(output);
        output.flush();
        output.close();
    }

    private SXSSFWorkbook getWorkbook(String fileName
            , List<String> headers
            , List<String> keys
            , List<String> widths
            , List<String> aligns
            , ArrayList<Map<String, Object>> list
            , int rowIdx
            , SXSSFWorkbook sxssfWorkbook) {
        // 최초 생성이면 manual flush를 위해 new SXSSFWorkbook(-1)
        // 이어서 작성일 경우 매개변수로 받은 sxssfWorkbook
        SXSSFWorkbook workbook = ObjectUtils.isEmpty(sxssfWorkbook)? sxssfWorkbook : new SXSSFWorkbook(-1);
        // 최초 생성이면 SheetN 생성
        // 이어서 작성일 경우 SheetN에서 이어서
        String sheetName = "Sheet" + (rowIdx / MAX_ROW + 1); // 각 시트 당 1,040,000개씩
        boolean newSheet = ObjectUtils.isEmpty( workbook.getSheet(sheetName));
        Sheet sheet = ObjectUtils.isEmpty( workbook.getSheet(sheetName)) ? workbook.createSheet(sheetName) : workbook.getSheet(sheetName);

        CellStyle headerStyle = createHeaderStyle(workbook);
        CellStyle bodyStyleLeft = createBodyStyle(workbook, "LEFT");
        CellStyle bodyStyleRight = createBodyStyle(workbook, "RIGHT");
        CellStyle bodyStyleCenter = createBodyStyle(workbook, "CENTER");

        // \r\n을 통해 셀 내 개행
        // 개행을 위해 setWrapText 설정
        bodyStyleLeft.setWrapText(true);
        bodyStyleRight.setWrapText(true);
        bodyStyleCenter.setWrapText(true);

        int idx = 0;

        for (String width : widths) {
            sheet.setColumnWidth(idx++, Integer.parseInt(width) * 256);
        }
        
        Row row = null;
        Cell cell = null;
      
        // 매개변수로 받은 rowIdx % MAX_ROW 행부터 이어서 데이터 
        int rowNo = rowIdx % MAX_ROW;
        
        if (newSheet) {
          row = sheet.createRow(rowNo);
          idx = 0;

          for (String columnName : headers) {
              cell = row.createCell(idx++);
              cell.setCellStyle(headerStyle);
              cell.setCellValue(columnName);
          }
        }

        for (Map<String, Object> tempRow : list) {
            idx = 0;
            row = sheet.createRow(++rowNo);

            for (String key : keys) {
                if (StringUtils.isEmpty(key)) {
                    continue;
                }

                cell = row.createCell(idx);

                if (ObjectUtils.isEmpty(aligns)) {
                    // 디폴트 가운데 정렬
                    cell.setCellStyle(bodyStyleCenter);
                } else {
                    String hAlign = aligns.get(idx);

                    if ("LEFT".equals(hAlign)) {
                        cell.setCellStyle(bodyStyleLeft);
                    } else if ("RIGHT".equals(hAlign)) {
                        cell.setCellStyle(bodyStyleRight);
                    } else {
                        cell.setCellStyle(bodyStyleCenter);
                    }
                }

                Object value = tempRow.get(key);

                if (value instanceof BigDecimal) {
                    cell.setCellValue(((BigDecimal) value).toString());
                } else if (value instanceof Double) {
                    cell.setCellValue(((Double) value).toString());
                } else if (value instanceof Long) {
                    cell.setCellValue(((Long) value).toString());
                } else if (value instanceof Integer) {
                    cell.setCellValue(((Integer) value).toString());
                } else {
                    cell.setCellValue((String) value);
                }

                idx++;
              
                // 주기적인 flush 진행
                if (rowNo % 100 == 0) {
                  ((SXSSFSheet) sheet).flushRows(100); 
                }
            }
        }

        return workbook;
    }

    private CellStyle createHeaderStyle(Workbook workbook) {
        CellStyle headerStyle = createBodyStyle(workbook,"");
        // 취향에 따라 설정 가능
        //headerStyle.setFillForegroundColor(HSSFColor.Predefined.LIGHT_YELLOW.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // 가로 세로 정렬 기준
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        return headerStyle;
    }

    private CellStyle createBodyStyle(Workbook workbook, String align) {
        CellStyle bodyStyle = workbook.createCellStyle();
        // 취향에 따라 설정 가능
        bodyStyle.setBorderTop(BorderStyle.THIN);
        bodyStyle.setBorderBottom(BorderStyle.THIN);
        bodyStyle.setBorderLeft(BorderStyle.THIN);
        bodyStyle.setBorderRight(BorderStyle.THIN);
        bodyStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        if (StringUtils.isEmpty(align) == false) {
            if ("LEFT".equals(align)) {
                bodyStyle.setAlignment(HorizontalAlignment.LEFT);
            } else if ("RIGHT".equals(align)) {
                bodyStyle.setAlignment(HorizontalAlignment.RIGHT);
            } else {
                bodyStyle.setAlignment(HorizontalAlignment.CENTER);
            }
        }

        return bodyStyle;
    }
}

*/