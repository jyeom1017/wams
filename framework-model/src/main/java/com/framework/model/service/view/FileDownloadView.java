package com.framework.model.service.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.AbstractView;

@Service
public class FileDownloadView extends AbstractView {
	
	@Autowired
    private ServletContext servletContext;
	
    /**
     * Size of a byte buffer to read/write file
     */
    private static final int BUFFER_SIZE = 4096;
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		System.out.println("---- filePath = " + model.get("filePath").toString());

        String filePath = model.get("filePath").toString();      
        File downloadFile = new File(filePath);
        FileInputStream inputStream = new FileInputStream(downloadFile);
         
        
        	
	        // get MIME type of the file
	        String mimeType = servletContext.getMimeType(filePath);
	        if (mimeType == null) {
	            // set to binary type if MIME mapping not found
	            mimeType = "application/octet-stream";
	        }
	        System.out.println("MIME type: " + mimeType);
	 
	        // set content attributes for the response
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        // set headers for the response
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	        // get output stream of the response
	        OutputStream outStream = response.getOutputStream();
	    try {
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
        } catch (FileNotFoundException fe) {
        	downloadFile = null;
        } catch (IOException ie) {
        	downloadFile = null;
        } finally {
        	
        	if(inputStream != null) {
        		inputStream.close();	
        	}
        	if(outStream != null) {
        		outStream.close();	
        	}
            
		}
        

	}

}
