package com.framework.model.service.view;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jxls.common.Context;
import org.jxls.formula.StandardFormulaProcessor;
import org.jxls.util.JxlsHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.framework.model.util.CommonUtil;
import com.usom.model.config.Configuration;

@Service
public class ExcelReportView extends AbstractExcelView {

	/*@Override
	 * version 1.x
	protected void buildExcelDocument(Map<String, Object> model,
			HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String filename = URLEncoder.encode(model.get("filename").toString(), "UTF-8");
		response.setHeader("Content-Type", "application/octet-stream; charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + filename);
        
        OutputStream os = null;
        InputStream is = null;
        String excel_template = request.getSession().getServletContext().getRealPath("/resources/report/template01.xls");
        
        try {
            //엑셀 템플릿 경로
            is = new FileInputStream(excel_template);
            os = response.getOutputStream();

            XLSTransformer transformer = new XLSTransformer();

            Workbook excel = transformer.transformXLS(is, model);
            excel.write(os);
            os.flush();

        }catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }finally {
            if(os != null) try { os.close(); } catch (IOException e) { }
            if(is != null) try { is.close(); } catch (IOException e) { }
        }
        
	}*/
	
	//version 2.x
	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String filename = model.get("filename").toString();
		String encode_filename = URLEncoder.encode(model.get("filename").toString(), "UTF-8");
		String ext = encode_filename.substring(encode_filename.lastIndexOf(".") + 1, encode_filename.length());
		encode_filename = encode_filename.substring(0, encode_filename.lastIndexOf("."));
		String title = "";
		if(model.get("title") != null) {//보고서 타이틀이 있을경우 파일명 세팅
			title = URLEncoder.encode(model.get("title").toString(), "UTF-8");
			encode_filename = title + "_" + encode_filename + "_" + CommonUtil.createUniqueIDFromDate().substring(0,8) + "." + ext;
		}else {
			encode_filename = encode_filename + "_" + CommonUtil.createUniqueIDFromDate().substring(0,8) + "." + ext;
		}
		//String filename = URLDecoder.decode(model.get("filename").toString(), "UTF-8");

		response.setHeader("Content-Type", "application/octet-stream; charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        
        //String excel_template = request.getSession().getServletContext().getRealPath("/resources/report/template01.xls");
        
//      String excel_template = "C:/workspace/OMAS/framework/framework-web/src/main/webapp/resources/report/template02.xls";
        String excel_template = Configuration.REPORT_FILE_TEMPLATE_PATH + filename;
        try(InputStream is = new FileInputStream(excel_template)) {
            try (OutputStream os = response.getOutputStream()) {
                Context context = new Context(model);
                //JxlsHelper helper = JxlsHelper.getInstance();
                //엑셀 수식적용
                //helper.setFormulaProcessor(new StandardFormulaProcessor());
                //helper.setProcessFormulas(true);
                
                //helper.processTemplate(is, os, context);
            }
        }
        
        /*try(InputStream is = new FileInputStream(excel_template)) {
            try (OutputStream os = response.getOutputStream()) {
            	
            	Transformer transformer = TransformerFactory.createTransformer(is, os);
            	Context context = new Context(model);
            	
            	
            }
        }*/
        
	}
	
	
	
	
	
}
