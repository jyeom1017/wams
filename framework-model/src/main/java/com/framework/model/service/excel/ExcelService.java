package com.framework.model.service.excel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.HashMap;
import com.usom.model.service.asset.ItemService;
import java.text.ParseException;

@Service
@RequiredArgsConstructor
public class ExcelService {

	@Resource(name = "itemService")
	private ItemService itemService;

    public Map<String, Object> getExcel() throws Exception {
    	HashMap<String, Object> param = new HashMap<String, Object>();
        // 엑셀에 저장할 row의 수를 불러옴
        long listSize = 10000;
        

        // 헤더 키 설정
        List<String> headerKeys = Arrays.asList("HEADER1", "HEADER2", "HEADER3");

        // 헤더 명 설정
        List<String> headers = Arrays.asList("헤더명1", "헤더명2", "헤더명3");

   		ArrayList<HashMap<String, Object>> list = itemService.selectAssetItemListExcel(param);

        // 헤더 키에 1:1 매칭
        for (HashMap<String, Object> asset : list) {
            Map<String, Object> tempMap = new HashMap<>();
            tempMap.put("HEADER1", asset.get("ASSET_CD"));
            tempMap.put("HEADER2", asset.get("ASSET_NM"));
            tempMap.put("HEADER3", asset.get("ASSET_SID"));
        }

        // 헤더당 셀 너비 설정
        List<String> widths = Arrays.asList("15", "20", "30");

        /*
         * 셀 당 정렬 기준 설정
         * LEFT: 왼쪽 정렬
         * CENTER: 가운데 정렬
         * RIGHT: 오른쪽 정렬
         */
        List<String> aligns = Arrays.asList("LEFT", "CENTER", "RIGHT");

        // 파일명 설정
        String fileName = "EXAMPLE_EXCEL";

        Map<String, Object> excelMap = new HashMap<>();
        excelMap.put("headers", headers);
        excelMap.put("keys", headerKeys);
        excelMap.put("widths", widths);
        excelMap.put("aligns", aligns);
        excelMap.put("list", list);
        excelMap.put("fileName", fileName);

        return excelMap;
    }
    
    public ArrayList<HashMap<String, Object>> getExcelList( int start, int size) throws Exception {
      // start 인덱스부터 size개의 데이터를 지닌 리스트 반환
    	HashMap<String, Object> param = new HashMap<String, Object>();
      return  itemService.selectAssetItemListExcel(param);
    }
}