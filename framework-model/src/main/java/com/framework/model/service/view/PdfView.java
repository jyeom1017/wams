package com.framework.model.service.view;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;

@Service
public class PdfView extends AbstractPdfView {

	@Override
	protected void buildPdfDocument(Map<String, Object> model,
			Document document, PdfWriter writer, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		// DownLoad 설정
		String userAgent = request.getHeader("User-Agent");
		String fileName = "test.pdf";

		if (userAgent.indexOf("MSIE") > -1) {
			fileName = URLEncoder.encode(fileName, "utf-8");
		} else {
			fileName = new String(fileName.getBytes("utf-8"), "iso-8859-1");
		}

		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		
        ArrayList<LinkedHashMap<Object, Object>> list = (ArrayList<LinkedHashMap<Object, Object>>) model.get("wordList");
        
        Table table = null;
        //	한글 깨짐 방지 BaseFont
        BaseFont baseFont = BaseFont.createFont("HYGoThic-Medium", "UniKS-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font font = new Font(baseFont, 12);
        
        //	header
        for(int i=0; i<list.size(); i++) {
        	
        	LinkedHashMap<Object, Object> map = list.get(i);
        	Iterator iterator = map.entrySet().iterator();
        	//header
        	if(i == 0) {
        		table = new Table(map.size());
        		System.out.println("map.size() : " + map.size());
            	while (iterator.hasNext()) {
    				Entry entry = (Entry) iterator.next();
    				System.out.println("entry.getKey() : " + entry.getKey());
					table.addCell(new Paragraph(entry.getKey().toString(), font));
            	}
            	break;
        	}
        }
        //	Contents
        for(int i=0; i<list.size(); i++) {
        	
        	LinkedHashMap<Object, Object> map = list.get(i);
        	Iterator iterator = map.entrySet().iterator();
        	
        	//contents    		
        	while (iterator.hasNext()) {
        		Entry entry = (Entry) iterator.next();
        		if(entry.getValue() != null && entry.getValue().toString() != null) {//2020
        			if(table != null) {
	        			if(font != null) {
	        				table.addCell(new Paragraph(entry.getValue().toString(), font));	
	        			}
        			}	
        		}else{
        			if(table != null) {
	        			if(font != null) {
	        				table.addCell(new Paragraph("", font));	
	        			}
        			}	
        			
        		}
        	}
        }
        document.add(table);
		
	}

}
