package com.framework.model.service.view;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.cache.decorators.WeakCache;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.ImageUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jfree.chart.imagemap.ImageMapUtilities;
import org.springframework.stereotype.Service;
//import org.springframework.web.servlet.view.document.AbstractExcelView;
import com.framework.model.service.view.AbstractXlsView;
import com.framework.model.util.CommonUtil;
import com.usom.model.config.Configuration;

import net.sf.json.util.JSONTokener;
import net.sf.jxls.transformer.XLSTransformer;





@Service
public class ExcelView extends AbstractXlsView {
//public class ExcelView extends AbstractExcelView {
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
	//protected void buildExcelDocument(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS", new Locale("ko", "KR"));
		String filename = (String) model.get("filename");
		String refilename = (String) model.get("refilename");
	    String sheetName = "Sheet1";
	    String mergeType = (String) model.get("mergeType");
	    
	    //System.out.println(model.get("list"));
	    //System.out.println(model.toString());
		//long start_1 = System.currentTimeMillis();
		
        String mainPath = Configuration.REPORT_FILE_TEMPLATE_PATH;
		InputStream is = null; 
//		XSSFWorkbook workbook;
		try { 
			
			XLSTransformer transformer = new XLSTransformer();
			is = readTemplate(filename, mainPath);
			//workbook = (HSSFWorkbook) transformer.transformXLS(is, model); 
//			workbook = (XSSFWorkbook) transformer.transformXLS(is, model);
			workbook = transformer.transformXLS(is, model);
			
			//merge
			if("011302_3".equals(mergeType)){
				Sheet sheet = workbook.getSheetAt(0);

				// 지역 merge 시작 행번호, 마지막 행번호, 시작 열번호, 마지막 열번호
		        sheet.addMergedRegion(new CellRangeAddress(5, 32, 0,0));// 실제 6 33
		        sheet.addMergedRegion(new CellRangeAddress(33, 60, 0,0));// 실제 34 61
		        sheet.addMergedRegion(new CellRangeAddress(61, 88, 0,0));// 실제 34 61
		        sheet.addMergedRegion(new CellRangeAddress(89, 116, 0,0));// 실제 34 61
		        sheet.addMergedRegion(new CellRangeAddress(117, 144, 0,0));// 실제 34 61
		        
		        for(int i = 5; i<=143; i+=2) 
		        {
		        	sheet.addMergedRegion(new CellRangeAddress(i, i+1, 1,2));// 실제 6 7
		        }
			}else if("010602".equals(mergeType)){
				
				ArrayList<HashMap<String, Object>> mergeList = new ArrayList<HashMap<String, Object>>();
				mergeList = (ArrayList<HashMap<String, Object>>)model.get("merge");
				
				Sheet sheet = workbook.getSheetAt(0);
				
//				
//				 CellStyle headerStyle = workbook.createCellStyle();
////				 headerStyle.setFillBackgroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
////				 headerStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
//				 
//				 
//				 headerStyle.setFillForegroundColor(HSSFColor.BLUE.index); 
//				 headerStyle.setFillBackgroundColor(HSSFColor.BLUE.index);
//				 
//				 Row headers = sheet.getRow(3);
//				 Cell cell = headers.getCell(1);
//			     headers.setRowStyle(headerStyle);
//			     cell.setCellStyle(headerStyle);
				
				int mergeSize = mergeList.size();
				int startRow = 3;
				int endRow = 0;
				if(mergeList != null && mergeSize > 0) {
					
					for(int i=0; i<mergeSize; i++)
		    		{	
						HashMap<String, Object> mergeMap = (HashMap<String, Object>)mergeList.get(i);
						 
						endRow = Integer.parseInt(String.valueOf(mergeMap.get("CNT")));
						
						// 지역 merge 시작 행번호, 마지막 행번호, 시작 열번호, 마지막 열번호
						if(startRow !=  (startRow+(endRow-1))){
							sheet.addMergedRegion(new CellRangeAddress(startRow, (startRow+(endRow-1)), 0,0));	
						}
				        
						
				        startRow = startRow+endRow;
				        
				        
		    		}
				}
				
				 for(int i = startRow; i<=99; i++) 
			        {
					 	endRow = 3;// 무조건 3행씩  
					 	sheet.addMergedRegion(new CellRangeAddress(startRow, (startRow+(endRow-1)), 0,0));
			        	startRow = startRow+endRow;	
			        }
				 
				//스타일 설정
//				 XSSFCellStyle styleOfColor = workbook.createCellStyle();
//				 styleOfColor.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 255)));
//				 styleOfColor.setFillBackgroundColor(new XSSFColor(new Color(255, 255, 255)));
//				 styleOfColorTest.setFillForegroundColor(HSSFColor.AQUA.index); 
				 
				 
//				 Row row = sheet.createRow(3); //0번째 행 생성
//				 row.setRowStyle(style);
//				 
////				 CellStyle style = workbook.createCellStyle();
////				 XSSFCellStyle  style = workbook.createCellStyle();
//////				 style.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
////				 
////				 style.setFillForegroundColor(new XSSFColor(new Color(255, 255, 255)));
//				 
//				 XSSFCellStyle style = (XSSFCellStyle)cell.getCellStyle();
//				 XSSFColor myColor = new XSSFColor(Color.RED);
//				 style.setFillForegroundColor(myColor);
				
				
				 
				
			}
				
				
			
			
			//sheet에 combo 추가하기
			ArrayList<HashMap<String, Object>> addComboList = new ArrayList<HashMap<String, Object>>();
			addComboList = (ArrayList<HashMap<String, Object>>)model.get("addComboList");
			if(addComboList != null && addComboList.size() > 0) {
				//HSSFSheet sheet = workbook.getSheetAt(0);
//				XSSFSheet sheet = workbook.getSheetAt(0);
				Sheet sheet = workbook.getSheetAt(0);
				for(int i=0; i<addComboList.size(); i++)
	    		{	
					HashMap<String, Object> comboMap = (HashMap<String, Object>)addComboList.get(i);
					
					 int firstRow = Integer.parseInt(String.valueOf(comboMap.get("firstRow")));//0부터시작
				     int lastRow = Integer.parseInt(String.valueOf(comboMap.get("lastRow")));
				     int firstCol = Integer.parseInt(String.valueOf(comboMap.get("firstCol")));
				     int lastCol = Integer.parseInt(String.valueOf(comboMap.get("lastCol")));
				     
				     String[] aCategoryValues = ( String[])comboMap.get("comboData");
				     
				     //System.out.println("firstRow=>"+firstRow+"/"+lastRow+"/"+firstCol+"/"+lastCol);
				     //System.out.println(aCategoryValues.length);
				     //System.out.println(aCategoryValues[1]);
				     
				    CellRangeAddressList regions = new CellRangeAddressList(firstRow, lastRow, firstCol, lastCol);  // 적용할 범위 
//					DVConstraint constraintCategory = DVConstraint.createExplicitListConstraint(aCategoryValues);
////					HSSFDataValidation dvCategory = new HSSFDataValidation(regions, constraintCategory );
//					XSSFDataValidation dvCategory = new XSSFDataValidation(regions, constraintCategory );
//					sheet.addValidationData(dvCategory);
				
					
//					if (workbook instanceof HSSFWorkbook) {
//						// HSSF workbook:
//						DVConstraint dvConstraint = DVConstraint.createExplicitListConstraint(aCategoryValues);
//						HSSFDataValidation dataValidation = new HSSFDataValidation(regions, dvConstraint);
//						dataValidation.setSuppressDropDownArrow(false);
//						sheet.addValidationData(dataValidation);
//					} else if (workbook instanceof XSSFWorkbook) {
						// XSSF workbook:
						XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
						XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(aCategoryValues);
						XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint,regions);
						validation.setShowErrorBox(true);
						sheet.addValidationData(validation);
//					}
					
					
					
					
	    		}
			}
			
		/*	ArrayList<HashMap<String, Object>> addImageList = new ArrayList<HashMap<String, Object>>();
			addImageList = (ArrayList<HashMap<String, Object>>)model.get("addImageList");
			if(addImageList != null && addImageList.size() > 0) {
				
				for(int i = 0 ; i<addImageList.size(); i ++) {
					HashMap<String, Object> ImageMap = (HashMap<String, Object>)addImageList.get(i);
					File getFile = new File(ImageMap.get("Path").toString());
					if(getFile.canRead()) {
					BufferedImage img = ImageIO.read(getFile);
					
					
					}
				}
			}*/
			

			//System.out.println("Time3 " + (System.currentTimeMillis() - start_1) + "ms");
		
			//writeWorkbook(encodeFileName(filename + "_" +formatter.format(new Date()).substring(0, 8)) , response, workbook);
			writeWorkbook(encodeFileName(refilename) , response, workbook,sheetName);
			
		}catch(IOException e){
//			e.printStackTrace(); 2020
			throw new RuntimeException(e.getMessage()); 
		}finally{
			if(is != null) try { is.close(); } catch (IOException e) { } 
        }
		
       
        
		
	}
	
    //private void writeWorkbook(String filename, HttpServletResponse response, HSSFWorkbook workbook,String sheetName) throws IOException {
//	private void writeWorkbook(String filename, HttpServletResponse response, XSSFWorkbook workbook,String sheetName) throws IOException {
	private void writeWorkbook(String filename, HttpServletResponse response, Workbook workbook,String sheetName) throws IOException {
		System.out.println("-------------------filename---------------------" + filename);
		/*
    	response.setHeader("Content-disposition", "attachment;filename=" + filename);
//        response.setContentType("application/x-msexcel");//xls
        response.setContentType("application/vnd.ms-excel");
        */
		
		String encode_filename = filename;//URLEncoder.encode(filename, "UTF-8");
		String ext = encode_filename.substring(encode_filename.lastIndexOf(".") + 1, encode_filename.length());
		encode_filename = encode_filename.substring(0, encode_filename.lastIndexOf("."));
		encode_filename = encode_filename + "_" + CommonUtil.createUniqueIDFromDate().substring(0,8) + "." + ext;
		response.setHeader("Content-Type", "application/octet-stream; charset=UTF-8");
		response.setHeader("Content-disposition", "attachment;filename=" + encode_filename);
        workbook.setSheetName(0, sheetName);

        try {
        	workbook.write(response.getOutputStream());
		}catch(IOException e){
//			e.printStackTrace(); 2020
			throw new RuntimeException(e.getMessage());         	
        } finally {
            workbook.close();
        }
        
        
    }
	
	private InputStream readTemplate(String file_name, String mainPath) throws FileNotFoundException {
        return new FileInputStream(mainPath + file_name);
	}
	
    private String encodeFileName(String filename) {
        try {
            return URLEncoder.encode(filename, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
	

