package com.framework.model.service.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.usom.model.config.Configuration;

import jxl.CellType;
import net.sf.jxls.transformer.Sheet;
import net.sf.jxls.transformer.XLSTransformer;

@Service()
public class ExcelRead  {
	
	public ArrayList<HashMap<String, Object>> ExcelReadDocument(HashMap<String, Object> param ) throws Exception {
//	protected void ExcelReadDocument(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {	
		
//        HSSFRow row;
//        HSSFCell cell;
        XSSFRow row;
        XSSFCell cell;

        String filePath = (String)param.get("filePath");// 읽어올 파일
        int targetCellCnt = (Integer)param.get("targetCellCnt");// 엑셀파일의 컬럼수.
        int startRowData = (Integer)param.get("startRowData");// 엑셀파일의 실제 데이터 시작 row
        
        
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
     // xlsx ->> XSSFWorkbook
        // xls  ->> HSSFWorkbook
//      FileInputStream inputStream = new FileInputStream("C:\\INSERT_DB_DATA.xlsx"); 

        FileInputStream inputStream = null;
        XSSFWorkbook workbook = null;

        try {
        	if(filePath == null || "".equals(filePath)) {
        		throw new FileNotFoundException("파일이 없습니다.");
        	}
        	inputStream = new FileInputStream(filePath);
        	workbook = new XSSFWorkbook(inputStream);
        	
//            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
          

            //sheet수 취득
            int sheetCn = workbook.getNumberOfSheets();
            sheetCn = 1; //무조건 첫번째것만.
            
            for(int cn = 0; cn < sheetCn; cn++){

                System.out.println("취득하는 sheet 이름 : " + workbook.getSheetName(cn));
                System.out.println(workbook.getSheetName(cn) + " sheet 데이터 취득 시작");

                //0번째 sheet 정보 취득
//                HSSFSheet sheet = workbook.getSheetAt(cn);
                XSSFSheet sheet = workbook.getSheetAt(cn);

                int rows = sheet.getPhysicalNumberOfRows(); // 행
                int cells = sheet.getRow(cn).getPhysicalNumberOfCells(); // 셀
                cells = targetCellCnt;// 컬럼갯수를 잘못읽어와서 사용자가 직접 컬럼의수를 넘기는 방식으로

                for (int r = startRowData; r < rows; r++) {
                    row = sheet.getRow(r); // row 가져오기
                    
//                    System.out.println("rowrowrowrowrow : " + row);
//                    System.out.println("getCellgetCellgetCell : " + row.getCell(0));
                    
                    if (row != null && row.getCell(0) != null) {
                    	
                    	HashMap<String, Object> rtnMap = new HashMap<String, Object>();
                    	
                        for (int c = 0; c < cells; c++) {
                            cell = row.getCell(c);
                            
                            if (cell != null) {
                                String value = null;

                                value = getCellValue(cell);
                                rtnMap.put("CELL_" + c, value);
                                
//                                System.out.print(value + "\t");
                            } else {
                            	rtnMap.put("CELL_" + c, "");
//                                System.out.print("[null]\t");
                            }
                            
                        } // for(cell) 문
                        System.out.println("excel read rtnMap : " + rtnMap.toString());
                        list.add(rtnMap);
                        
                        System.out.println("\n");
                    }else {
                    	break;
                    }
                    System.out.println("\n                   ");
                    System.out.println("\n                   ");
//                    System.out.print("\n                   ");
//                    System.out.print("\n                   ");
                } // for(row) 문
                
            }
            
//            inputStream.close();
//            workbook.close();
        } catch (FileNotFoundException fe) {
        	list = null;
        } catch (IOException ie) {
        	list = null;
        } finally {
        	if(inputStream != null) {
        		inputStream.close();
        	}
        	if(workbook != null) {
        		 workbook.close();
        	}
             
		}
        
        return list;
    }
	
   private String encodeFileName(String filename) {
        try {
            return URLEncoder.encode(filename, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
   
   private String getCellValue(XSSFCell cell) {
	   
	   String value = null;
	   SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	   SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmm");
//	   System.out.print("cell.getCellTypeEnum()                  " + cell.getCellTypeEnum());
	   
	   switch (cell.getCellTypeEnum()) {
//           case XSSFCell.CELL_TYPE_FORMULA:
       	case FORMULA:
               value = cell.getCellFormula();
//               System.out.println("FORMULA                 " + value +"/n");
               break;
//           case XSSFCell.CELL_TYPE_NUMERIC: // 데이터 타입이 숫자.
           case NUMERIC:
               if (HSSFDateUtil.isCellDateFormatted(cell)){ // 숫자- 날짜 타입이다.
            	   
//            	   System.out.println("NUMERIC    일             " + cell.getDateCellValue().toString().length() +"/n");
            	   
            	   if(cell.getDateCellValue().toString().length() > 8) {
            		   value = formatter2.format(cell.getDateCellValue());   
            	   }else {
            		   value = formatter.format(cell.getDateCellValue());   
            	   }
                   
//                   System.out.println("NUMERIC    일             " + cell.getDateCellValue() +"/n");
                   
//                   System.out.println("NUMERIC    일             " + value +"/n");
               }else{ // 숫자 타입
                   /*
                    * Integer i = (int)cell.getNumericCellValue(); // 리턴타입이 double 임. integer로 형변환 해주고
                   	* value = i.toString(); // integer 로 형변환된 값을 String 으로 받아서 value값에 넣어준다.
                   */
            	   double d = cell.getNumericCellValue(); 
                   value = String.format("%1$,.3f", d); // 소수 3쨋자리까지만
                   
                   value = zeroOut(value);// 0제거
                   
//                   System.out.println("NUMERIC   이              " + value +"/n");
               }
               break;
//           case XSSFCell.CELL_TYPE_STRING:
           case STRING:
               value = "" + cell.getStringCellValue();
//               System.out.println("STRING                 " + value +"/n");
               break;
//           case XSSFCell.CELL_TYPE_BLANK:
           case BLANK:
               value = "";//[null 아닌 공백]
               break;
//           case XSSFCell.CELL_TYPE_ERROR:
           case ERROR:
               //value = "" + cell.getErrorCellValue();
        	   value = "";
               break;
           default:
       }
	   return value;
   }
   
   public String zeroOut(String val ) {
	   //String val = "1000.1";
	   int dotlen = val.indexOf('.');
	   String reval = val ;
		
	   try {
		
		if(val != "" && dotlen > -1) {
			int len = val.length();
			len = len - 1;
			 
//			System.out.println(len);
//			System.out.println(dotlen);
			
			int lastNum = 0;
			int i = len;
			while(i > 0) {
				char oneChar = val.charAt(i);
				String oneString = Character.toString(oneChar);
				
				if("0".equals(oneString)) {
				}else {
					lastNum = i;
					break;
				}
				
				i--;
			}
			
			reval = val.substring(0, (lastNum+1));
			
			int relen = reval.length();
			relen = relen - 1;
			char reoneChar = reval.charAt(relen);
			
			String reoneString = Character.toString(reoneChar);
			if(".".equals(reoneString)) {
				reval = val.substring(0, relen);
			}
			
		}
		
       } catch (NullPointerException fe) {
    	   reval = "";
	   }
	   System.out.println("reval==" + reval);
		return reval;
		
   }
   
}


	

