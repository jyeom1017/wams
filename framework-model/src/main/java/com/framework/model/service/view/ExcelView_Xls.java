package com.framework.model.service.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.usom.model.config.Configuration;

import net.sf.jxls.transformer.Sheet;
import net.sf.jxls.transformer.XLSTransformer;

@Service
public class ExcelView_Xls extends AbstractExcelView {
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS", new Locale("ko", "KR"));
		String filename = (String) model.get("filename");
		String refilename = (String) model.get("refilename");
	    String sheetName = "Sheet1";
	    
	    System.out.println(model.get("list"));
	    System.out.println(model.toString());
	    
        String mainPath = Configuration.REPORT_FILE_TEMPLATE_PATH;
		InputStream is = null; 
		
		try { 
			
			XLSTransformer transformer = new XLSTransformer();
			is = readTemplate(filename, mainPath);
			workbook = (HSSFWorkbook) transformer.transformXLS(is, model); 
			
			//sheet에 combo 추가하기
			ArrayList<HashMap<String, Object>> addComboList = new ArrayList<HashMap<String, Object>>();
			addComboList = (ArrayList<HashMap<String, Object>>)model.get("addComboList");
			if(addComboList != null && addComboList.size() > 0) {
				HSSFSheet sheet = workbook.getSheetAt(0);
				for(int i=0; i<addComboList.size(); i++)
	    		{	
					HashMap<String, Object> comboMap = (HashMap<String, Object>)addComboList.get(i);
					
					 int firstRow = Integer.parseInt(String.valueOf(comboMap.get("firstRow")));//0부터시작
				     int lastRow = Integer.parseInt(String.valueOf(comboMap.get("lastRow")));
				     int firstCol = Integer.parseInt(String.valueOf(comboMap.get("firstCol")));
				     int lastCol = Integer.parseInt(String.valueOf(comboMap.get("lastCol")));
				     
				     String[] aCategoryValues = ( String[])comboMap.get("comboData");
				     
				     System.out.println("firstRow=>"+firstRow+"/"+lastRow+"/"+firstCol+"/"+lastCol);
				     System.out.println(aCategoryValues.length);
				     System.out.println(aCategoryValues[1]);
				     
				    CellRangeAddressList regions = new CellRangeAddressList(firstRow, lastRow, firstCol, lastCol);  // 적용할 범위 
					DVConstraint constraintCategory = DVConstraint.createExplicitListConstraint(aCategoryValues);
					HSSFDataValidation dvCategory = new HSSFDataValidation(regions, constraintCategory );
					sheet.addValidationData(dvCategory);
					
	    		}
			}
			
			
		}catch(IOException e){
//			e.printStackTrace();  2020
			throw new RuntimeException(e.getMessage()); 
		}finally{
			if(is != null) try { is.close(); } catch (IOException e) { } 
        }
		
       
        //writeWorkbook(encodeFileName(filename + "_" +formatter.format(new Date()).substring(0, 8)) , response, workbook);
		writeWorkbook(encodeFileName(refilename) , response, workbook,sheetName);
		
	}
	
    private void writeWorkbook(String filename, HttpServletResponse response, HSSFWorkbook workbook,String sheetName) throws IOException {
    	response.setHeader("Content-disposition", "attachment;filename=" + filename);
        response.setContentType("application/x-msexcel");
        workbook.setSheetName(0, sheetName);

        workbook.write(response.getOutputStream());
    }
	
	private InputStream readTemplate(String file_name, String mainPath) throws FileNotFoundException {
        return new FileInputStream(mainPath + file_name);
	}
	
    private String encodeFileName(String filename) {
        try {
            return URLEncoder.encode(filename, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
	

