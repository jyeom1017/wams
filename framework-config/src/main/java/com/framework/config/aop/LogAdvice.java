package com.framework.config.aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

public class LogAdvice {
	private static final Logger logger = LogManager.getLogger(LogAdvice.class);
	
	//private static Logger logger = Logger.getLogger(LogAdvice.class);
	
	public void printLog() {
		
		logger.info("-------------------------------------------");
		logger.info("-------------------------------------------");
		logger.info("-------------------------------------------");
		logger.info("-------------------------------------------");
	}
	
	public Object printTime(ProceedingJoinPoint joinPoint) throws Throwable {
		
		long start = System.currentTimeMillis();
		
		Object target = joinPoint.getTarget();//실행 Class
//		joinPoint.getArgs();//파라메터 체크
		
		Object result = joinPoint.proceed();//메소드를 실행함
		
		long end = System.currentTimeMillis();
		
		logger.info(target + "time : " + (end - start));
		
		return result;
	}
	
	/**
	 * 서비스 레이어의 정보를 log로 남김
	 * @param joinPoint
	 * @throws Throwable
	 */
	public void serviceLog(JoinPoint joinPoint) throws Throwable {
		
		logger.info("[SERVICE Class] " + joinPoint.getTarget());
		logger.info("[SERVICE Method] " + joinPoint.getSignature().getName());
		
		Object[] signatureArgs = joinPoint.getArgs();
		for (Object signatureArg: signatureArgs) {
			logger.info("[SERVICE parameters] " + signatureArg);
		}
		   
		   
		   
	}

}
