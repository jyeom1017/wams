package com.usom.web.controller.api;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.diagnosis.ReevalService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/reeval")
public class ReevalController {
	@Resource(name="reevalService")
	private ReevalService reevalService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
    @RequestMapping(value = "/getList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getList(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = reevalService.selectReEst(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		//paging start 
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		//paging end 
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getListAssetGroup.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getListAssetGroup(HttpServletRequest req) throws Exception {
        	HashMap<String, Object> param = new HashMap<String, Object>();
            param = CommonUtil.reqDataParsingToHashMap(req);    	
		return reevalService.selectReEstAssetGroup(param);
	}
    
    @RequestMapping(value = "/getListAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getListAsset(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
   		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
   		
   		if(param.get("SQL") == null || param.get("SQL").toString().equals("")) {
   			list = reevalService.selectReEstAsset(param);
		}else {
			JSONObject jsonSql = JSONObject.fromObject(param.get("SQL").toString());
			HashMap<String, Object> SQLPARM = CommonUtil.convertJsonToMap(jsonSql);

			SQLPARM.put("rows", param.get("rows"));
			SQLPARM.put("page", param.get("page"));
			SQLPARM.put("RE_EST_SID", param.get("RE_EST_SID"));
			SQLPARM.put("CLASS4_CD", param.get("CLASS4_CD"));
			SQLPARM.put("CLASS3_CD", param.get("CLASS3_CD"));
	   		list = reevalService.selectReEstAsset(SQLPARM);
		}
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		//paging start 
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		//paging end 
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/insert.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insert(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
     	HashMap<String, Object> param = new HashMap<String, Object>();
     	param.put("INSERT_ID", user.getE_ID());
    	return  reevalService.insertReEst(param);
    }
    
    @RequestMapping(value = "/insertAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertAsset(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
     	param.put("INSERT_ID", user.getE_ID());
    	return  reevalService.insertReEstAsset(param);
    }
    @RequestMapping(value = "/deleteAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteAsset(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  reevalService.deleteReEstAsset(param);
    }
    
    @RequestMapping(value = "/delete.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int delete(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  reevalService.deleteReEst(param);
    }
    
    @RequestMapping(value = "/delete2.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int delete2(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  reevalService.delete2ReEst(param);
    }
    
    @RequestMapping(value = "/update.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int update(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  reevalService.update(param);
    }
    
    @RequestMapping(value = "/getPriceIndex.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getPriceIndex(HttpServletRequest req) throws Exception {
    	return  reevalService.selectPriceIndex();
    }
    
    @RequestMapping(value = "/insertPriceIndex.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertPriceIndex(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    	HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("ID", user.getE_ID());
    	return  reevalService.insertPriceIndex(param);
    }
    
    @RequestMapping(value = "/getWeightSpec.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getWeightSpec(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
    	return  reevalService.getWeightSpec(param);
    }
    
    @RequestMapping(value = "/getWeightSpecItem.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getWeightSpecItem(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
    	return  reevalService.getWeightSpecItem(param);
    }
    
    @RequestMapping(value = "/insertWeightSpec.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertWeightSpec(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
    	return  reevalService.insertWeightSpec(param);
    }
	   /*getList
	   insert
	   update
	   delete
	   report
	   insertAsset
	   deleteAsset
	   
	   getPriceIndex
	   insertPriceIndex
	   updatePriceIndex
	   deletePriceIndex
	   reportPriceIndex
	   
	   getWeightSpec
	   insertWeightSpec
	   updateWeightSpec
	   deleteWeightSpec*/

}
