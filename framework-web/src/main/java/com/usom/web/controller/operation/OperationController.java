package com.usom.web.controller.operation;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.operation.OperationService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/operation")
public class OperationController {
	@Resource(name="operationService")
	private OperationService operationService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
	//개보수
    @RequestMapping(value = "/getREPAIR.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getREPAIR(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println("파랑" + param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = operationService.getREPAIR(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getREPAIR_ASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getREPAIR_ASSET(@RequestParam HashMap<String, Object> param) throws Exception {
    	//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println(param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		
		/*if(param.get("rows").toString().equals("10000")) {
			param.put("rows", param.get("rows2"));
		}*/
		//
		list = operationService.getREPAIR_ASSET(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
		
		
		
   	}
    
    @RequestMapping(value = "/getREPAIR2.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getREPAIR2(HttpServletRequest req) throws Exception {
	   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	    ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
	    param.put("OPTION", 3);
	    list = operationService.getREPAIR(param);
        System.out.println("p" + param);
        System.out.println("l" + list);
        list.get(0).put("Popup_SID",  list.get(0).get("REPAIR_SID"));
        list.get(0).put("Popup_NM", list.get(0).get("REPAIR_NM"));
        list.get(0).put("Popup_END_DT",  list.get(0).get("REPAIR_END_DT"));
        list.get(0).put("Popup_START_DT", list.get(0).get("REPAIR_START_DT"));
        
        list.get(0).remove("REPAIR_SID");
        list.get(0).remove("REPAIR_NM");
        list.get(0).remove("REPAIR_END_DT");
        list.get(0).remove("REPAIR_START_DT");
        HashMap<String, Object> map = new  HashMap<String, Object>();
        map.put("rows", list);
	   	return  map;
   	}
    
    @RequestMapping(value = "/getREPAIR_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody ArrayList<HashMap<String, Object>> getREPAIR_FILE(HttpServletRequest req) throws Exception {
	   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	   	if(param.containsKey("SID")) {
	   		param.put("REPAIR_SID", param.get("SID"));
	   	}
	   	return  operationService.getREPAIR_FILE(param);
   	}
    
    @RequestMapping(value = "/updateREPAIR_FILE", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateREPAIR_FILE(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("UPDATE_ID", user.getE_ID());
        System.out.println(param);
        return operationService.updateREPAIR_FILE(param);
    }
    
    @RequestMapping(value = "/updateREPAIR_ASSET", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateREPAIR_ASSET(@RequestParam HashMap<String, Object> param) throws Exception {
    	if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        param.put("UPDATE_ID", user.getE_ID());
        System.out.println(param);
        return operationService.updateREPAIR_ASSET(param);
    }
    
    @RequestMapping(value = "/updateREPAIR_DEL_YN.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateREPAIR_DEL_YN(HttpServletRequest req) throws Exception {
        return  operationService.updateREPAIR_DEL_YN(CommonUtil.reqDataParsingToHashMap(req));
    }
    
    @Transactional
    @RequestMapping(value = "/deleteREPAIR_ASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteREPAIR_ASSET(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        int cnt = operationService.deleteREPAIR_ASSET(param);
        return cnt;
    }
    
    @Transactional
    @RequestMapping(value = "/deleteREPAIR_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteREPAIR_FILE(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        if(param.containsKey("SID")) {
	   		param.put("REPAIR_SID", param.get("SID"));
	   	}
        int cnt = operationService.deleteREPAIR_FILE(param);
        return cnt;
    }
    
    @Transactional
    @RequestMapping(value = "/saveREPAIR.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int saveREPAIR(HttpServletRequest req) throws Exception {
        System.out.println(req);
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
           
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	    param.put("ID", user.getE_ID());
        System.out.println(param);
    	return operationService.saveREPAIR(param);
   	}
    
    @RequestMapping(value = "/insertREPAIR_FILE", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertREPAIR_FILE(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
    	
        if(param.containsKey("SID")) {
	   		param.put("REPAIR_SID", param.get("SID"));
	   	}
        
        System.out.println(param);
        param.put("INSERT_ID", user.getE_ID());
        param.put("UPDATE_ID", user.getE_ID());
 
        return operationService.insertREPAIR_FILE(param);
    }
    
    @RequestMapping(value = "/insertREPAIR", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insertREPAIR(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("INSERT_ID", user.getE_ID());
        param.put("UPDATE_ID", user.getE_ID());
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list.add(operationService.insertREPAIR(param));
        HashMap<String, Object> map = new  HashMap<String, Object>();
        map.put("rows", list);
        return  map;
    }
    
    @RequestMapping(value = "/insertREPAIR_ASSET", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insertREPAIR_ASSET(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
       
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        System.out.println(param);
    	param.put("INSERT_ID", user.getE_ID());
        param.put("UPDATE_ID", user.getE_ID());
        return operationService.insertREPAIR_ASSET(param);
    }
    
    @Transactional
    @RequestMapping(value = "/cancelREPAIR.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int cancelREPAIR(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return operationService.cancelREPAIR(param);
   	}
    
    @Transactional
    @RequestMapping(value = "/updateREPAIR_EQUAL.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int updateREPAIR_EQUAL(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return operationService.updateREPAIR_EQUAL(param);
   	}
    
    @Transactional
    @RequestMapping(value = "/updateREPAIR_RATIO.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int updateREPAIR_RATIO(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return operationService.updateREPAIR_RATIO(param);
   	}
    
    //조사 탐사
    @RequestMapping(value = "/getINSPECT.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getINSPECT(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println(param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = operationService.getINSPECT(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getINSPECT2.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getINSPECT2(HttpServletRequest req) throws Exception {
	   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	    ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
	    param.put("OPTION", 3);
	    list = operationService.getINSPECT(param);
        System.out.println("p" + param);
        System.out.println("l" + list);
        list.get(0).put("Popup_SID",  list.get(0).get("INSPECT_SID"));
        list.get(0).put("Popup_NM", list.get(0).get("INSPECT_NM"));
        list.get(0).put("Popup_END_DT",  list.get(0).get("INSPECT_END_DT"));
        list.get(0).put("Popup_START_DT", list.get(0).get("INSPECT_START_DT"));
        
        list.get(0).remove("INSPECT_SID");
        list.get(0).remove("INSPECT_NM");
        list.get(0).remove("INSPECT_END_DT");
        list.get(0).remove("INSPECT_START_DT");
        HashMap<String, Object> map = new  HashMap<String, Object>();
        map.put("rows", list);
	   	return  map;
   	}
    
    @RequestMapping(value = "/getINSPECT_ASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getINSPECT_ASSET(@RequestParam HashMap<String, Object> param) throws Exception {
    	//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println(param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = operationService.getINSPECT_ASSET(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
   	}
    
    @RequestMapping(value = "/getINSPECT_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody ArrayList<HashMap<String, Object>> getINSPECT_FILE(HttpServletRequest req) throws Exception {
	   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	 	if(param.containsKey("SID")) {
	   		param.put("INSPECT_SID", param.get("SID"));
	   	}
	   	return  operationService.getINSPECT_FILE(param);
   	}
    
    @RequestMapping(value = "/updateINSPECT_FILE", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateINSPECT_FILE(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("UPDATE_ID", user.getE_ID());
        System.out.println(param);
        return operationService.updateINSPECT_FILE(param);
    }
    
    @RequestMapping(value = "/updateINSPECT_ASSET", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateINSPECT_ASSET(@RequestParam HashMap<String, Object> param) throws Exception {
    	if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        param.put("UPDATE_ID", user.getE_ID());
        System.out.println(param);
        return operationService.updateINSPECT_ASSET(param);
    }
    
    @Transactional
    @RequestMapping(value = "/updateINSPECT_EQUAL.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int updateINSPECT_EQUAL(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return operationService.updateINSPECT_EQUAL(param);
   	}
    
    @Transactional
    @RequestMapping(value = "/updateINSPECT_RATIO.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int updateINSPECT_RATIO(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return operationService.updateINSPECT_RATIO(param);
   	}
    
    @RequestMapping(value = "/updateINSPECT_DEL_YN", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateINSPECT_DEL_YN(HttpServletRequest req) throws Exception {
        return  operationService.updateINSPECT_DEL_YN(CommonUtil.reqDataParsingToHashMap(req));
    }
    
    @Transactional
    @RequestMapping(value = "/deleteINSPECT_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteINSPECT_FILE(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        if(param.containsKey("SID")) {
	   		param.put("INSPECT_SID", param.get("SID"));
	   	}
        int cnt = operationService.deleteINSPECT_FILE(param);
        return cnt;
    }
    
    @Transactional
    @RequestMapping(value = "/deleteINSPECT_ASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteINSPECT_ASSET(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        int cnt = operationService.deleteINSPECT_ASSET(param);
        return cnt;
    }
    
    @Transactional
    @RequestMapping(value = "/saveINSPECT.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int saveINSPECT(HttpServletRequest req) throws Exception {
	   	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
           
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	    param.put("ID", user.getE_ID());
        
    	return operationService.saveINSPECT(param);
   	}
    
    @Transactional
    @RequestMapping(value = "/cancelINSPECT.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int cancelINSPECT(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return operationService.cancelINSPECT(param);
   	}
    
    @RequestMapping(value = "/insertINSPECT_FILE", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertINSPECT_FILE(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        System.out.println(param);
        if(param.containsKey("SID")) {
	   		param.put("INSPECT_SID", param.get("SID"));
	   	}
        param.put("INSERT_ID", user.getE_ID());
        param.put("UPDATE_ID", user.getE_ID());
 
        return operationService.insertINSPECT_FILE(param);
    }
    
    @RequestMapping(value = "/insertINSPECT", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insertINSPECT(HttpServletRequest req) throws Exception {
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("INSERT_ID", user.getE_ID());
        param.put("UPDATE_ID", user.getE_ID());
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list.add(operationService.insertINSPECT(param));
        HashMap<String, Object> map = new  HashMap<String, Object>();
        map.put("rows", list);
        return  map;
    }
    
    @RequestMapping(value = "/insertINSPECT_ASSET", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insertINSPECT_ASSET(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
       
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        System.out.println(param);
    	param.put("INSERT_ID", user.getE_ID());
        param.put("UPDATE_ID", user.getE_ID());
        return operationService.insertINSPECT_ASSET(param);
    }
    
    @RequestMapping(value = "/getFormatTYPE", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>>  getFormatTYPE(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return operationService.getFormatTYPE(param);
    }
    
    @RequestMapping(value = "/getRelayInfo", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getRelayInfo(@RequestParam HashMap<String, Object> param) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        System.out.println(param);
    	param.put("INSERT_ID", user.getE_ID());
        param.put("UPDATE_ID", user.getE_ID());
        return pagging.runPaging(operationService.getRelayInfo(param), param);
    }
    
    @RequestMapping(value = "/getRelayInfoListExcel", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getRelayInfoListExcel(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        System.out.println(param);
    	param.put("INSERT_ID", user.getE_ID());
        param.put("UPDATE_ID", user.getE_ID());
        param.put("page", Integer.parseInt(param.get("PAGE_NO").toString())+1);
        param.put("rows", Integer.parseInt(param.get("ROW_CNT").toString()));
        return operationService.getRelayInfo(param);
    }    
        
    @RequestMapping(value = "/getRelayInfoChart", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getRelayInfoChart(@RequestParam HashMap<String, Object> param) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        System.out.println(param);
        param.put("rows", 20);
        param.put("page", 1);
    	param.put("INSERT_ID", user.getE_ID());
        param.put("UPDATE_ID", user.getE_ID());
        return pagging.runPaging(operationService.getRelayInfo(param), param);
    }
    
}