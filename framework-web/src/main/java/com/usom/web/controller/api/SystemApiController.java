package com.usom.web.controller.api;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.FileUpload;
import com.framework.model.util.Pagging;
import com.framework.model.util.PasswordUtil;
import com.usom.model.config.Configuration;
import com.usom.model.service.basic.EmployeeService;
import com.usom.model.service.system.SystemService;
import com.usom.model.service.bbs.BbsService;
import com.usom.model.service.common.CommonService;
import com.usom.model.vo.user.MenuVO;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 시스템 정보관리
 * 
 * @author jw.kim
 *
 */
@Controller
@RequestMapping("/sys")
public class SystemApiController {
	
	@Resource(name = "SystemService")
	private SystemService systemService;
	
	@Resource(name = "fileUpload")
	private FileUpload fileUpload;
	
	@Resource(name = "commonService")
	private CommonService commonService;
	
	@Resource(name = "employeeService")
	private EmployeeService employeeService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
	/**
	 * 시스템 정보관리 > 시스템현황 > 시스템 현황조회 - 마스터
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getEquipMasterPagerList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	//public @ResponseBody JSONArray getEquipMasterList(@RequestParam HashMap<String, Object> param) throws Exception {
	//	public @ResponseBody ArrayList<HashMap<String, Object>> getEquipMasterList(@RequestParam HashMap<String, Object> param) throws Exception {
	public @ResponseBody HashMap<String, Object> getEquipMasterPagerList(@RequestParam HashMap<String, Object> param) throws Exception {
		
		ArrayList<HashMap<String, Object>> equipMasterList = new ArrayList<HashMap<String, Object>>();
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		if ("RNUM".equals(param.get("sidx"))) {
			param.put("sidx", "ROWNUM");
		}
		equipMasterList = systemService.getEquipMasterList(param);
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		/* paging */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(equipMasterList);
		
		rtnMap.put("rows", equipMasterList);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging */
		
		//		JSONArray jsonEquipMasterList = JSONArray.fromObject(equipMasterList);
		//		JSONArray jsonEquipMasterList = CommonUtil.ObjectToJson(equipMasterList);
		//		return equipMasterList;
		
		return rtnMap;
	}
	
	@RequestMapping(value = "/insertEquipMaster.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertEquipMaster(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.insertEquipMaster(param);
	}
	
	@RequestMapping(value = "/updateEquipMaster.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateEquipMaster(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.updateEquipMaster(param);
	}
	
	@RequestMapping(value = "/deleteEquipMaster.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteEquipMaster(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.deleteEquipMaster(param);
	}
	
	/**
	 * 시스템 정보관리 > 시스템현황 > 시스템 현황조회 - 사양
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getEquipSpecList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getEquipSpecList(HttpServletRequest req) throws Exception {
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		if ("RNUM".equals(param.get("sidx"))) {
			param.put("sidx", "ROWNUM");
		}
		list = systemService.getEquipSpecList(param);
		
		return list;
	}
	
	@RequestMapping(value = "/insertEquipSpec.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertEquipSpec(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.insertEquipSpec(param);
	}
	
	@RequestMapping(value = "/updateEquipSpec.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateEquipSpec(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.updateEquipSpec(param);
	}
	
	@RequestMapping(value = "/deleteEquipSpec.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteEquipSpec(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.deleteEquipSpec(param);
	}
	
	/**
	 * 시스템 정보관리 > 시스템현황 > 시스템 현황조회 - 이력
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getEquipHistList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getEquipHistList(@RequestParam HashMap<String, Object> param) throws Exception {
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		if ("RNUM".equals(param.get("sidx"))) {
			param.put("sidx", "ROWNUM");
		}
		list = systemService.getEquipHistList(param);
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		/* paging */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging */
		
		return rtnMap;
	}
	
	@RequestMapping(value = "/insertEquipHist.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertEquipHist(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.insertEquipHist(param);
	}
	
	@RequestMapping(value = "/updateEquipHist.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateEquipHist(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.updateEquipHist(param);
	}
	
	@RequestMapping(value = "/deleteEquipHist.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteEquipHist(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.deleteEquipHist(param);
	}
	
	/**
	 * 공통 장비선택 팝업창
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getEquipMasterPopList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getEquipMasterPopList(HttpServletRequest req) throws Exception {
		// 1:하드웨어, 2:소프트웨어, 3:네트워크, 4:부대장비
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = systemService.getEquipMasterPopList(param);
		
		return list;
	}
	
	/**
	 * 시스템 정보관리 > 시스템 운영 > 시스템 구축범위 > 하수관로 시설 현황
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getSewerageStatus.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getSewerageStatus(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return systemService.getSewerageStatus(param);
	}
	
	/******************** 아래부터 기존 소스 ********************/
	
	@RequestMapping(value = "/main")
	public ModelAndView main(@RequestParam(required = false) String viewmode) throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("viewmode", viewmode);
		mav.setViewName("omas/basic/employeeManager.jsp");
		
		return mav;
	}
	
	@RequestMapping(value = "/checkMaintenance.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> checkMaintenance(@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		
		//System.out.println("---------점검---------" + commonService.checkMaintenance(param));
		
		//return commonService.checkMaintenance(param);
		return null;
	}
	
	/**
	 * 공통코드 리스트 가져오기
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getCommonCodeList.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getCommonCodeList(@RequestParam HashMap<String, Object> param) throws Exception {
		
		//HashMap<String, Object> param = new HashMap<String, Object>();
		
		return commonService.getCommonCodeList(param);
	}
	
	@RequestMapping(value = "/insertCommonCodeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertCommonCodeList(HttpServletRequest req
	//@RequestParam HashMap<String, Object> param
	) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = reqDataParsing(req);
		System.out.print(parseJson);
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		Iterator<String> keys = parseJson.keys();
		
		while (keys.hasNext()) {
			String key = keys.next();
			if (parseJson.get(key) != null)
				param.put(key, parseJson.get(key).toString());
			else
				param.put(key, "");
		}
		System.out.print("param:");
		System.out.print(param);
		
		return commonService.insertCommonCodeList(param);
	}
	
	@RequestMapping(value = "/updateCommonCodeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateCommonCodeList(HttpServletRequest req
	//@RequestParam HashMap<String, Object> param
	) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = reqDataParsing(req);
		System.out.print(parseJson);
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		Iterator<String> keys = parseJson.keys();
		
		while (keys.hasNext()) {
			String key = keys.next();
			if (parseJson.get(key) != null)
				param.put(key, parseJson.get(key).toString());
			else
				param.put(key, "");
		}
		System.out.print("param:");
		System.out.print(param);
		
		return commonService.updateCommonCodeList(param);
	}
	
	@RequestMapping(value = "/getFileList.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getFileList(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getFileList(param);
	}
	
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> fileUpload(MultipartHttpServletRequest request) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = JSONObject.fromObject(request.getParameter("data").toString());
		
		// file Check
		String[] arrFileType = { "PDF", "ZIP", "XLS", "XLSX", "HWP", "PPT", "PPTX", "DOC", "DOCX", "JPG", "GIF", "PNG" };
		//String folderName = request.getParameter("data"); // 폴더 이름을 받아온다.
		String folderName = parseJson.get("folderName").toString();
		
		// System.out.println("------------폴더 이름"+folderName);
		String uploadDir = Configuration.UPLOAD_FILE_PATH + "file/" + folderName;
		// System.out.println("------------uploadDir 이름"+uploadDir);
		String urlPath = Configuration.UPLOAD_FILE_URL + "file/" + folderName;
		// System.out.println("------------urlPath 이름"+uploadDir);
		HashMap<String, Object> fileInfo = new HashMap<String, Object>();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		/*
		 * 파일 루트 생성
		 */
		// 기본설정 세팅
		fileUpload.setFileUpload(arrFileType, 60, null, uploadDir); // fileUpload.java 이동
		if (fileUpload.validationFile(request)) {// file validation 체크 //fileUpload.java 이동
			CommonsMultipartFile fileData = (CommonsMultipartFile) request.getFile("fileData");
			if (fileData != null && !fileData.isEmpty()) {
				String fileName = fileUpload.create(fileData); /** fileUpload.java 이동 핵심 */
				fileInfo.put("F_NAME", fileName);// F_NAME
				//fileInfo.put("F_TITLE", request.getParameter("fileDesc"));// F_TITLE
				fileInfo.put("F_TITLE", parseJson.get("fileDesc").toString());// F_TITLE
				fileInfo.put("F_PATH", urlPath + "/" + fileName);// F_PATH
				fileInfo.put("userid", user.getE_ID());
				int filenumber = insertFileInfo(fileInfo); // 두번째 루트
				fileInfo.put("filenumber", filenumber);
			}
		}
		// System.out.println("------------fileInfo 이름"+fileInfo);
		return fileInfo;
	}
	
	@RequestMapping(value = "/deleteFileInfo", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteFileInfo(@RequestParam HashMap<String, Object> param) throws Exception {
		int fnum = Integer.parseInt(param.get("f_num").toString());
		return commonService.deleteFileInfo(fnum);
	}
	
	@RequestMapping(value = "/getMenuList.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody JSONArray getMenuList(@RequestParam HashMap<String, Object> param) throws Exception {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		System.out.println("auth:" + auth.getPrincipal());
		ArrayList<MenuVO> basicMenuList = ((UserDetailsServiceVO) auth.getPrincipal()).getBasicMenuList();
		JSONArray jsonBasicMenuList = JSONArray.fromObject(basicMenuList);
		return jsonBasicMenuList;
	}
	
	private int insertFileInfo(HashMap<String, Object> fileInfo) // 파일 번호 생성
			throws Exception {
		commonService.insertFileInfo(fileInfo);
		return Integer.parseInt(fileInfo.get("f_num").toString());
	}
	
	private JSONObject reqDataParsing(HttpServletRequest req) throws Exception {
		
		JSONObject jsonObject = new JSONObject();
		StringBuffer jb = new StringBuffer();
		String line = null;
		
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null) {
				jb.append(line);
			}
		} catch (NullPointerException e) {
			jsonObject.put("parseCode", "01");
			return jsonObject;
		}
		
		try {
			if(jb != null && jb.toString() != null) {
				jsonObject = JSONObject.fromObject(jb.toString());
				//restlogger.info(" >>> request jsonObject ===>"+jsonObject.toString());	
			}
		} catch (NullPointerException e) {
			jsonObject.put("parseCode", "02");
			return jsonObject;
		}
		
		jsonObject.put("parseCode", "00");
		
		return jsonObject;
	}
}
