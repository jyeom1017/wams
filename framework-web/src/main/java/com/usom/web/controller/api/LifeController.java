package com.usom.web.controller.api;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.diagnosis.LifeService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/diag")
public class LifeController {
	@Resource(name="diagnosisService")
	private LifeService diagnosisService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
	//LIFE
    @RequestMapping(value = "/getList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getLIFE(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println(param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = diagnosisService.getLIFE(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	}
    @RequestMapping(value = "/getLIFE_ASSET_VARIATION.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getLIFE_ASSET_VARIATION(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
    	return  diagnosisService.getLIFE_ASSET_VARIATION(param);
    }
    
    
    
    @RequestMapping(value = "/getFactorList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getFactor(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
    	return  diagnosisService.getFactor(Integer.parseInt(param.get("index").toString()));
    }
    
    @RequestMapping(value = "/saveLife_Spec.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int saveLife_Spec(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
    	return  diagnosisService.saveLife_Spec(param);
    }
    
    @RequestMapping(value = "/newLife.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> newLife(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("INSERT_ID", user.getE_ID());
    	return  diagnosisService.newLife(param);
    }
    
    @RequestMapping(value = "/getASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getLIFE_ASSET(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println(param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = diagnosisService.getLIFE_ASSET(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	}
    
    
    @RequestMapping(value = "/updateASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateASSET(@RequestParam HashMap<String, Object> param) throws Exception {
    	if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
    	
    	 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
         
         param.put("UPDATE_ID", user.getE_ID());
    	
    	return diagnosisService.updateLIFE_ASSET(param);
    }
    
    @RequestMapping(value = "/updateASSET2.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateASSET2(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("INSERT_ID", user.getE_ID());
    	return  diagnosisService.updateLIFE_ASSET(param);
    }
    
    @RequestMapping(value = "/saveLIFE.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int saveLIFE(HttpServletRequest req) throws Exception {
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  diagnosisService.saveLIFE(param);
    }
    
    @RequestMapping(value = "/continueLIFE_ASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int continueLIFE_ASSET(HttpServletRequest req) throws Exception {
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  diagnosisService.continueLIFE_ASSET(param);
    }
    
    @RequestMapping(value = "/outputLIFE_ASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String,Object>> outputLIFE_ASSET(HttpServletRequest req) throws Exception {
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  diagnosisService.outputLIFE_ASSET(param);
    }
    
    @RequestMapping(value = "/getFN_LIST.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String,Object> getFN_LIST(HttpServletRequest req) throws Exception {
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  diagnosisService.getFN_LIST(param);
    }
    
    @RequestMapping(value = "/getLIFE_ASSET_RESULT.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getLIFE_ASSET_RESULT(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println(param);
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		if(param.get("SQL") == null || param.get("SQL").toString().equals("")) {
   			param.put("ACLASS4_CD", param.get("CLASS4_CD"));
   			param.put("ACLASS3_CD", param.get("CLASS3_CD"));
   			param.remove("CLASS3_CD");
   			param.remove("CLASS4_CD");
   			list = diagnosisService.getLIFE_ASSET_RESULT(param);
		}else {
			JSONObject jsonSql = JSONObject.fromObject(param.get("SQL").toString());
			HashMap<String, Object> SQLPARM = CommonUtil.convertJsonToMap(jsonSql);

			SQLPARM.put("rows", param.get("rows"));
			SQLPARM.put("page", param.get("page"));
			SQLPARM.put("ACLASS4_CD", param.get("CLASS4_CD"));
			SQLPARM.put("ACLASS3_CD", param.get("CLASS3_CD"));
			SQLPARM.put("LIFE_SID", param.get("LIFE_SID"));
			list = diagnosisService.getLIFE_ASSET_RESULT(SQLPARM);
		};
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getLIFE_ASSET_STATUS_GRADE.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getLIFE_ASSET_STATUS_GRADE(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println(param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = diagnosisService.getLIFE_ASSET_STATUS_GRADE(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getLIFE_ASSET_STATUS_GRADE_MODIFY.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getLIFE_ASSET_STATUS_GRADE_MODIFY(@RequestParam HashMap<String, Object> param) throws Exception {
   		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
   		if (param.get("rules") != null) {
   			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
   		}
   		
   		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
   		System.out.println(param);
   		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
   		list = diagnosisService.getLIFE_ASSET_STATUS_GRADE_MODIFY(param);
   		
   		if (param.get("page") == null) {
   			param.put("page", 1);
   		}
   		if (param.get("rows") == null) {
   			param.put("rows", 10);
   		}
   		
   		/* paging start */
   		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
   		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
   		pagging.setTotalCountArray(list);
   		
   		rtnMap.put("rows", list);//data
   		rtnMap.put("page", pagging.getCurrPage());//현재페이지
   		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
   		rtnMap.put("records", pagging.getTotalCount());//총글갯수
   		/* paging end */
   		
   		return rtnMap;
   	}
    
    @RequestMapping(value = "/getLIFE_YCOL.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int getLIFE_YCOL(HttpServletRequest req) throws Exception {
        
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  diagnosisService.getLIFE_YCOL(param);
    }
    
    @RequestMapping(value = "/saveLIFE_ASSET_STATUS_GRADE.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> saveLIFE_ASSET_STATUS_GRADE(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("INSERT_ID", user.getE_ID());
        return  diagnosisService.saveLIFE_ASSET_STATUS_GRADE(param);
    }
    
    @RequestMapping(value = "/updateLIFE_ASSET_STATUS_GRADE.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateLIFE_ASSET_STATUS_GRADE(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.updateLIFE_ASSET_STATUS_GRADE(param);
    }
    
    @RequestMapping(value = "/resetLIFE_ASSET_STATUS_GRADE_MODIFY.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int resetLIFE_ASSET_STATUS_GRADE_MODIFY(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.resetLIFE_ASSET_STATUS_GRADE_MODIFY(param);
    }
    
    @RequestMapping(value = "/saveLIFE_ASSET_STATUS_GRADE_MODIFY.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int saveLIFE_ASSET_STATUS_GRADE_MODIFY(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.saveLIFE_ASSET_STATUS_GRADE_MODIFY(param);
    }
    
    @RequestMapping(value = "/refreshLIFE_ASSET_STATUS_GRADE.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int refreshLIFE_ASSET_STATUS_GRADE(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.refreshLIFE_ASSET_STATUS_GRADE(param);
    }
    
    @RequestMapping(value = "/getUPDATE_ID.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getUPDATE_ID(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.getUPDATE_ID(param);
    }
    
    @RequestMapping(value = "/getLIFE_ASSET_GRAPH.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getLIFE_ASSET_GRAPH(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.getLIFE_ASSET_GRAPH(param);
    }
    
    @RequestMapping(value = "/deleteList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.deleteList(param);
    }
    
    @RequestMapping(value = "/getAssetReport.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getAssetReport(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.getAssetReport(param);
    }
    @RequestMapping(value = "/getAssetFrom.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getAssetFrom(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.getAssetFrom(param);
    }
    
    @RequestMapping(value = "/getLIFE_ASSET_STATUS_GRADE_REPORT.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getLIFE_ASSET_STATUS_GRADE_REPORT(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.getLIFE_ASSET_STATUS_GRADE_REPORT(param);
    }
    
    @RequestMapping(value = "/OnFileSelectLIFE.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int OnFileSelectLIFE(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.OnFileSelectLIFE(param);
    }
    
    @RequestMapping(value = "/getTotal.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int getTotal(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.getTotal(param);
    }
    
    @RequestMapping(value = "/updateLIFE.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateLIFE(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.updateLIFE(param);
    }
    
    @RequestMapping(value = "/Test.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int Test(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        return  diagnosisService.Test(param);
    }
    
}
