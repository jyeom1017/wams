package com.usom.web.controller.operation;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.operation.New_OperationService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

@Controller
@RequestMapping("/new_operation")
public class New_OperationController {
	@Resource(name="new_operationService")
	private New_OperationService operationService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
	//개보수
    @RequestMapping(value = "/getREPAIR.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getREPAIR(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println("파랑" + param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = operationService.getREPAIR(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getREPAIR_ASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getREPAIR_ASSET(@RequestParam HashMap<String, Object> param) throws Exception {
    	//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println(param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		
		/*if(param.get("rows").toString().equals("10000")) {
			param.put("rows", param.get("rows2"));
		}*/
		//
		list = operationService.getREPAIR_ASSET(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
   	}
    
	@Transactional
	@RequestMapping(value = "/insertREPAIR", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insertREPAIR(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("INSERT_ID", user.getE_ID());
        
        return  operationService.insertREPAIR(param);
    }
	
	@Transactional
    @RequestMapping(value = "/saveREPAIR.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int saveREPAIR(HttpServletRequest req) throws Exception {
        System.out.println(req);
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
           
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	    param.put("ID", user.getE_ID());
        System.out.println(param);
    	return operationService.saveREPAIR(param);
   	}
	
	@Transactional
	@RequestMapping(value = "/insertREPAIR_FILE", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertREPAIR_FILE(HttpServletRequest req) throws Exception {
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
	        
	        HashMap<String, Object> param = new HashMap<String, Object>();
	        param = CommonUtil.reqDataParsingToHashMap(req);
	    	
	        if(param.containsKey("SID")) {
		   		param.put("REPAIR_SID", param.get("SID"));
		   	}
	        
	        System.out.println(param);
	        param.put("INSERT_ID", user.getE_ID());
	        param.put("UPDATE_ID", user.getE_ID());
	 
	        return operationService.insertREPAIR_FILE(param);
	}
	
	@Transactional
	@RequestMapping(value = "/getREPAIR_MODIFIED.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getREPAIR_MODIFIED(HttpServletRequest req) throws Exception {
	   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	   
	   	return  operationService.getREPAIR_MODIFIED(param);
   	} 
	
	@Transactional
	@RequestMapping(value = "/deleteREPAIR_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int deleteREPAIR_FILE(HttpServletRequest req) throws Exception {
	   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	   
	   	return  operationService.deleteREPAIR_FILE(param);
   	} 
	
	@Transactional
	@RequestMapping(value = "/getREPAIR_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getREPAIR_FILE(HttpServletRequest req) throws Exception {
		   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
		   	if(param.containsKey("SID")) {
		   		param.put("REPAIR_SID", param.get("SID"));
		   	}
		   	return  operationService.getREPAIR_FILE(param);
	}
	@Transactional
	@RequestMapping(value = "/getFormatTYPE", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>>  getFormatTYPE(HttpServletRequest req) throws Exception {
	    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	        return operationService.getFormatTYPE(param);
	}
	@Transactional
	@RequestMapping(value = "/updateREPAIR_DEL_YN.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateREPAIR_DEL_YN(HttpServletRequest req) throws Exception {
	        return  operationService.updateREPAIR_DEL_YN(CommonUtil.reqDataParsingToHashMap(req));
	}
	@Transactional
	@RequestMapping(value = "/updateREPAIR_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateREPAIR_FILE(HttpServletRequest req) throws Exception {
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
	        
	        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	        param.put("UPDATE_ID", user.getE_ID());
	        System.out.println(param);
	        return operationService.updateREPAIR_FILE(param);
	}
	
	//조사 및 탐사
    @RequestMapping(value = "/getINSPECT.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getINSPECT(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println("파랑" + param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = operationService.getINSPECT(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getINSPECT_ASSET.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getINSPECT_ASSET(@RequestParam HashMap<String, Object> param) throws Exception {
    	//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		System.out.println(param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		
		/*if(param.get("rows").toString().equals("10000")) {
			param.put("rows", param.get("rows2"));
		}*/
		//
		list = operationService.getINSPECT_ASSET(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
   	}
    
	@Transactional
	@RequestMapping(value = "/insertINSPECT", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insertINSPECT(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("INSERT_ID", user.getE_ID());
        
        return  operationService.insertINSPECT(param);
    }
	
	@Transactional
    @RequestMapping(value = "/saveINSPECT.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int saveINSPECT(HttpServletRequest req) throws Exception {
        System.out.println(req);
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
           
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	    param.put("ID", user.getE_ID());
        System.out.println(param);
    	return operationService.saveINSPECT(param);
   	}
	
	@Transactional
	@RequestMapping(value = "/insertINSPECT_FILE", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertINSPECT_FILE(HttpServletRequest req) throws Exception {
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
	        
	        HashMap<String, Object> param = new HashMap<String, Object>();
	        param = CommonUtil.reqDataParsingToHashMap(req);
	    	
	        if(param.containsKey("SID")) {
		   		param.put("INSPECT_SID", param.get("SID"));
		   	}
	        
	        System.out.println(param);
	        param.put("INSERT_ID", user.getE_ID());
	        param.put("UPDATE_ID", user.getE_ID());
	 
	        return operationService.insertINSPECT_FILE(param);
	}
	
	@Transactional
	@RequestMapping(value = "/getINSPECT_MODIFIED.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getINSPECT_MODIFIED(HttpServletRequest req) throws Exception {
	   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	   
	   	return  operationService.getINSPECT_MODIFIED(param);
   	} 
	
	@Transactional
	@RequestMapping(value = "/deleteINSPECT_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody int deleteINSPECT_FILE(HttpServletRequest req) throws Exception {
	   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	   
	   	return  operationService.deleteINSPECT_FILE(param);
   	} 
	
	@Transactional
	@RequestMapping(value = "/getINSPECT_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getINSPECT_FILE(HttpServletRequest req) throws Exception {
		   	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
		   	if(param.containsKey("SID")) {
		   		param.put("INSPECT_SID", param.get("SID"));
		   	}
		   	return  operationService.getINSPECT_FILE(param);
	}
	
	@Transactional
	@RequestMapping(value = "/updateINSPECT_DEL_YN.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateINSPECT_DEL_YN(HttpServletRequest req) throws Exception {
	        return  operationService.updateINSPECT_DEL_YN(CommonUtil.reqDataParsingToHashMap(req));
	}
	@Transactional
	@RequestMapping(value = "/updateINSPECT_FILE.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateINSPECT_FILE(HttpServletRequest req) throws Exception {
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
	        
	        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
	        param.put("UPDATE_ID", user.getE_ID());
	        System.out.println(param);
	        return operationService.updateINSPECT_FILE(param);
	}   
}