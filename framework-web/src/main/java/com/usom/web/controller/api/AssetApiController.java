package com.usom.web.controller.api;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.asset.AssetService;
import com.usom.model.service.asset.ItemService;
import com.usom.model.service.common.CommonService;
import com.usom.model.service.postgis.PostGisService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

@Controller
@RequestMapping("/asset")
public class AssetApiController {

    @Resource(name = "assetService")
    private AssetService assetService;

    @Resource(name = "itemService")
    private ItemService itemService;
    
    @Resource(name = "commonService")
    private CommonService commonService;
    
    @Autowired
    private PostGisService postgisService;

    @Autowired
    private Pagging pagging;

    // 자산 전체
    @RequestMapping(value = "/getAssetCriteria.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getAssetCriteria() throws Exception {

        return assetService.getAssetCriteria();
    }

    @RequestMapping(value = "/getLevelNumList.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getLevelNumList() throws Exception {

        return itemService.getLevelNumList();
    }

    // 자산 레벨
    @RequestMapping(value = "/getLevelCodeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getLevelCodeList(@RequestParam HashMap<String, Object> param) throws Exception {

        return assetService.getLevelCodeList(param);
    }

    @RequestMapping(value = "/updateLevelCode.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateLevelCode(HttpServletRequest req) throws Exception {

        return assetService.updateLevelCode(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/insertLevelCode.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertLevelCode(HttpServletRequest req) throws Exception {

        return assetService.insertLevelCode(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/deleteLevelCode.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteLevelCode(HttpServletRequest req) throws Exception {

        return assetService.deleteLevelCode(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/isAssetClassCdExists.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int isAssetClassCdExists(HttpServletRequest req) throws Exception {

        return assetService.isAssetClassCdExists(CommonUtil.reqDataParsingToHashMap(req));
    }

    // 자산 인벤토리 경로
    @RequestMapping(value = "/getItemList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getItemList(@RequestParam HashMap<String, Object> param) throws Exception {

        return itemService.getItemList(param);
    }

    @RequestMapping(value = "/deleteAssetPath.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteAssetPath(HttpServletRequest req) throws Exception {
    	//기준정보 삭제 처리 필요 
    	HashMap<String,Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	assetService.deleteAssetBaseInfo(param);
        return itemService.deleteAssetPath(param);
    }

    @RequestMapping(value = "/getInfoItemList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getInfoItemList(HttpServletRequest req) throws Exception {

        return itemService.getInfoItemList(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getCustomItemList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getCustomItemList(HttpServletRequest req) throws Exception {

        return itemService.getCustomItemList(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getAllAssetLevelList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getAllAssetLevelList(HttpServletRequest req) throws Exception {

        return itemService.getAllAssetLevelList(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getAllInfoItemList.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getAllInfoItemList() throws Exception {

        return itemService.getAllInfoItemList();
    }

    @RequestMapping(value = "/getAssetMaxLevelList.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getAssetMaxLevelList() throws Exception {

        return itemService.getAssetMaxLevelList();
    }

    @RequestMapping(value = "/deleteMaxLevel.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public void deleteMaxLevel(HttpServletRequest req) throws Exception {

        itemService.deleteMaxLevel(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/updateMaxLevelList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateMaxLevelList(HttpServletRequest req) throws Exception {

        return itemService.updateMaxLevelList(req);
    }

    @RequestMapping(value = "/saveAssets.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int saveAssets(HttpServletRequest req) throws Exception {
    	HashMap<String,Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	System.out.print(param);
    	if(1==itemService.saveAssets(param))
    	{
    		
            JSONObject assetPath = (JSONObject) param.get("assetPath");
            
            HashMap<String, Object> map = new HashMap<String,Object>();
            map = CommonUtil.jsonObjectToHashMap(assetPath);
    		
    		//HashMap<String,Object> param = CommonUtil.reqDataParsingToHashMap(req);
            param.put("FN_GBN", map.get("CLASS3_CD").toString());
    		if(param.get("FN_GBN").equals("F")) {
	    		param.put("WORK_GBN", map.get("CLASS6_CD"));
	    		param.put("ASSET_GBN1", map.get("CLASS7_CD"));
	    		param.put("ASSET_GBN2", map.get("CLASS8_CD"));
    		}else {
	    		param.put("WORK_GBN", map.get("CLASS5_CD"));
	    		param.put("ASSET_GBN1", map.get("CLASS6_CD"));
	    		param.put("ASSET_GBN2", map.get("CLASS7_CD"));    			
    		}
    		//#{CLASS3_CD}, #{CLASS4_CD}, #{CLASS5_CD}, #{CLASS6_CD}, #{CLASS7_CD}, #{CLASS8_CD},
    		assetService.deleteAssetBaseInfo(param);
    		return assetService.insertAssetBaseInfo(param);
    		
    		//return 1;
		}else {
			return 0;
		}
        
    }

    @RequestMapping(value = "/updateAssets.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateAssets(HttpServletRequest req) throws Exception {

        return itemService.updateAssets(CommonUtil.reqDataParsingToHashMap(req));
    }

    
    @RequestMapping(value = "/getAssetPathInfo.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String,Object >> getAssetPathInfo(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = new HashMap<String,Object >();
    	param.put("ASSET_PATH_SID", req.getParameter("ASSET_PATH_SID"));
        return itemService.selectAssetLevelPathList(param);
    }
    
    @RequestMapping(value = "/getSelectAssetLevelPathList.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String,Object >> getSelectAssetLevelPathList(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = new HashMap<String,Object >();
        return itemService.selectAssetLevelPathList(param);
    }    
    
    @RequestMapping(value = "/getSelectAssetLevelPathList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String,Object >> SelectAssetLevelPathList(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
        return itemService.selectAssetLevelPathList(param);
    }
    
    @RequestMapping(value = "/getSelectAssetItemListExcel.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> SelectAssetItemList(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
    	System.out.println(param);
    	if(param.get("PAGE_NO").equals(0))
    	{
    		String lg_desc ="[메뉴 : 인벤토리관리>자산관리] - 자산목록.xlsx";
    		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    		Object principal = auth.getPrincipal();
    		User user = ((UserDetailsServiceVO) principal).getUser();
    		String user_id =  user.getE_ID();
    		String ip_addr = req.getRemoteAddr();		
    				param.put("lg_opt", "2");//1:로그인, 2:출력, 3:에러
    				param.put("lg_desc", lg_desc);// 화면의 코드를 넘긴다. 예) 유량조사 0502
    				param.put("login_user_id",user_id );
    				param.put("E_ID", user_id );
    				param.put("ip_addr", ip_addr );
    				System.out.println(param);
    				commonService.insertSystemLog(param);	    		
    	}
        return itemService.selectAssetItemListExcel(param);
    }
    
    @RequestMapping(value = "/getSelectAssetItemList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> SelectAssetItemList1(@RequestParam HashMap<String, Object> param) throws Exception {
        return itemService.selectAssetItemList(param);
    }
    
    @RequestMapping(value = "/getFindAssetList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> FindAssetList(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
    	return itemService.findAssetList(param);
    }
    
    @RequestMapping(value = "/getSelectAssetItemInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String,Object >> SelectAssetItemInfo(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
        return itemService.selectAssetItemInfo(param);
    }
    //자산등록 추가
    @RequestMapping(value = "/insertAssetItemInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertAssetItem(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();    	
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());
    	int result = itemService.saveAssetItem(param);
    	//접속로그
    			HashMap<String, Object> param2 = new HashMap<String, Object>();
    			try {
    				String ip_addr = req.getRemoteAddr();
    				param2.put("ip_addr", ip_addr );
    				param2.put("lg_opt", "4");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기
    				param2.put("lg_desc", "자산등록:" + result );
    				param2.put("login_user_id", user.getE_ID().toString());
    				 
    				commonService.insertSystemLog(param2);
    				
    			}catch(Exception e) {
    				//System.out.print(" db 에러 param ==>" + param.toString());
    			}
        return result;
    }
    //자산등록 수정
    @RequestMapping(value = "/AssetItemInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateAssetItem(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();    	
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());
    	int result = itemService.updateAssetItem(param);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			String ip_addr = req.getRemoteAddr();
			param2.put("ip_addr", ip_addr );			
			param2.put("lg_opt", "5");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기
			param2.put("lg_desc", "자산수정:" + param.get("ASSET_SID") );
			param2.put("login_user_id", user.getE_ID().toString());
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		} 	
        return result;
    }
    
    //자산등록 불용
    @RequestMapping(value = "/deleteAssetItemInfo2.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteAssetItem2(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();    	
    	HashMap <String,Object > param = new HashMap<String,Object >();
    	param.put("ASSET_SID",Integer.parseInt(req.getParameter("ASSET_SID").toString()));
        int result =  itemService.deleteAssetItem2(param);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			String ip_addr = req.getRemoteAddr();
			param2.put("ip_addr", ip_addr );			
			param2.put("lg_opt", "8");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기 , 8:불용처리
			param2.put("lg_desc", "자산불용:" + param.get("ASSET_SID") );
			param2.put("login_user_id", user.getE_ID().toString());
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}
		return result;
    }
    
    //자산등록 폐기
    @RequestMapping(value = "/deleteAssetItemInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteAssetItem(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();    	
    	HashMap <String,Object > param = new HashMap<String,Object >();
    	param.put("ASSET_SID",Integer.parseInt(req.getParameter("ASSET_SID").toString()));
        int result =  itemService.deleteAssetItem(param);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			String user_id =  user.getE_ID();
			String ip_addr = req.getRemoteAddr();
			param2.put("ip_addr", ip_addr );
			param2.put("login_user_id",user_id );
			param2.put("E_ID", user_id );
			param2.put("lg_opt", "7");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기
			param2.put("lg_desc", "자산폐기:" + param.get("ASSET_SID") );
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}
		return result;
    }
    
    //잘못 등록된 자산 삭제
    @RequestMapping(value = "/deleteAssetItem.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteAsset(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();    	
    	HashMap <String,Object > param = new HashMap<String,Object >();
    	param.put("ASSET_SID",Integer.parseInt(req.getParameter("ASSET_SID").toString()));
    	HashMap <String,Object > param1 = itemService.getAssetGisInfo(param);
    	int result1 = 0;
    	if(param1 != null) {
    	param.putAll(param1);
        param.put("table_nm", param.get("LAYER").toString());
        param.put("ftr_idn", param.get("FTR_IDN").toString());
        param.put("ftr_cde", param.get("FTR_CDE").toString());
        result1 = postgisService.deleteGisAsset(param);
        itemService.deleteAssetGis(param);
    	}
        int result = itemService.deleteAsset(param);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			String user_id =  user.getE_ID();
			String ip_addr = req.getRemoteAddr();
					param2.put("ip_addr", ip_addr );
					param2.put("login_user_id",user_id );
					param2.put("E_ID", user_id );
			
					param2.put("lg_opt", "6");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기
					param2.put("lg_desc", "자산삭제:" + param.get("ASSET_SID") );
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}  
		return result + result1;
    }
    
    @RequestMapping(value = "/getAssetLevelList.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String,Object >> getAssetLevelList(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
        return itemService.getAssetLevelList();
    }

    @RequestMapping(value = "/isAssetPathExists.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int isAssetPathExists(HttpServletRequest req) throws Exception {

        return itemService.isAssetPathExists(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getLevelPathTotalCount.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody int getLevelPathTotalCount() throws Exception {

        return itemService.getTotalCount();
    }

    @RequestMapping(value = "/getLevelCodeTotalCount.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody int getLevelCodeTotalCount() throws Exception {

        return assetService.getTotalCount();
    }

    @RequestMapping(value = "/isLevelCodeUsed.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int isLevelCodeUsed(HttpServletRequest req) throws Exception {

        return assetService.isLevelCodeUsed(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getSelectAssetPic.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap <String,Object >> SelectAssetPic(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = new HashMap ();
    	param.put("AssetSid",Integer.parseInt(req.getParameter("AssetSid").toString()));
        return itemService.selectAssetPic(param);
    }
    
    @RequestMapping(value = "/updateAssetPic.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateAssetPic(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
    	
        return itemService.updateAssetPic(param);
    }
    
    @RequestMapping(value = "/deleteAssetPic.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteAssetPic(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
    	
        return itemService.updateAssetPic(param);
    }
    
    @RequestMapping(value = "/getSelectAssetHistory.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody HashMap<String,Object > SelectAssetHistory(@RequestParam HashMap<String, Object> param) throws Exception {
    	//HashMap <String,Object > param = new HashMap<String,Object > ();
    	//param.put("AssetSid",Integer.parseInt(req.getParameter("AssetSid").toString()));
    	System.out.println(param);
    	if(param.get("AssetSid").equals("0")) return param;
        return pagging.runPaging(itemService.selectAssetHistory(param),param);
    }
    @RequestMapping(value = "/getSelectAssetHistory.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String,Object > SelectAssetHistory2(@RequestParam HashMap<String, Object> param) throws Exception {
    	//HashMap <String,Object > param = new HashMap<String,Object > ();
    	//param.put("AssetSid",Integer.parseInt(req.getParameter("AssetSid").toString()));
    	//param.put("START_DT",Integer.parseInt(req.getParameter("START_DT").toString()));
    	//param.put("END_DT",Integer.parseInt(req.getParameter("END_DT").toString()));
    	System.out.println(param);
    	if(param.get("AssetSid").equals("0")) return param;    	
        return pagging.runPaging(itemService.selectAssetHistory(param),param);
    }

    @RequestMapping(value = "/isAssetPathUsed.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int isAssetPathUsed(HttpServletRequest req) throws Exception {

        return itemService.isAssetPathUsed(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getAssetGisInfo.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody HashMap<String,Object > getAssetGisInfo(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = new HashMap<String,Object > ();
    	param.put("ASSET_SID",Integer.parseInt(req.getParameter("ASSET_SID").toString()));
    	if(req.getParameter("ASSET_PATH_SID")!=null) {
    		param.put("ASSET_PATH_SID",Integer.parseInt(req.getParameter("ASSET_PATH_SID").toString()));	
    	}
        return itemService.getAssetGisInfo(param);
    }
    
    @RequestMapping(value = "/getAssetFromGisInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getAssetFromGisInfo(HttpServletRequest req) throws Exception {
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
        return itemService.getAssetFromGisInfo(param);
    }
    
    @RequestMapping(value = "/insertAssetGis.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertAssetGis(HttpServletRequest req) throws Exception {
    	
        return itemService.insertAssetGis(CommonUtil.reqDataParsingToHashMap(req));
    }
    
    // 자산 기준정보 관리 

    // 자산 레벨
    
    
    @RequestMapping(value = "/getAssetBaseInfoList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getAssetBaseInfoList(@RequestParam HashMap<String, Object> param) throws Exception {
    	
        return assetService.getAssetBaseInfoList(param);
    }
    
    @RequestMapping(value = "/getAssetBaseInfoDetail.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getAssetBaseInfoDetail(HttpServletRequest req) throws Exception {
    	
        return assetService.getAssetBaseInfoDetail(CommonUtil.reqDataParsingToHashMap(req));
    }
    
    @RequestMapping(value = "/updateAssetBaseInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateAssetBaseInfo(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();    	
    	HashMap <String,Object > param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());
        return assetService.updateAssetBaseInfo(param);
    }
    
    @RequestMapping(value = "/insertAssetBaseInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertAssetBaseInfo(HttpServletRequest req) throws Exception {
    	
        return assetService.insertAssetBaseInfo(CommonUtil.reqDataParsingToHashMap(req));
    }
    
    @RequestMapping(value = "/deleteAssetBaseInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteAssetBaseInfo(HttpServletRequest req) throws Exception {
    	
        return assetService.insertAssetBaseInfo(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/getStatAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getStatAsset(HttpServletRequest req) throws Exception {
    	//
        HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        
        list = itemService.getStatAsset(param);
        } catch (Exception ex) {
        	System.out.println(ex.getMessage());
        }
        return list;    	
        
    }
}