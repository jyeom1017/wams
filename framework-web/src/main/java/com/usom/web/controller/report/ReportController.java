package com.usom.web.controller.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
//import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Arrays;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.jxls.util.Util;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.framework.model.exception.ApplicationException;
import com.framework.model.service.view.ExcelRead;
import com.framework.model.service.view.ExcelReportView;
import com.framework.model.service.view.ExcelView;
import com.framework.model.util.CommonUtil;
import com.usom.model.config.Configuration;
import com.usom.model.dao.asset.ItemDao;
import com.usom.model.service.asset.AssetService;
import com.usom.model.service.asset.ItemService;
import com.usom.model.service.basic.EmployeeService;
import com.usom.model.service.bbs.BbsService;
import com.usom.model.service.common.CommonService;
import com.usom.model.service.diagnosis.LccService;
import com.usom.model.service.diagnosis.LifeService;
import com.usom.model.service.diagnosis.LosService;
import com.usom.model.service.diagnosis.ReevalService;
import com.usom.model.service.estimation.EstimationService;
import com.usom.model.service.finance.FinanceAnalysisService;
import com.usom.model.service.finance.OipService;
import com.usom.model.service.operation.OperationService;
import com.usom.model.service.risk.RiskService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;
import com.usom.model.service.boardservice.BoardService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


@Controller
@RequestMapping("/report")
public class ReportController {
	
//	private static final Logger logger = LoggerFactory.getLogger(ReportController.class);
	@Inject
	private ExcelView excelView;
	
//	@Inject
//	private ExcelRead excelRead;
	
//	@Inject
//	private PdfView pdfView;

	@Resource(name = "commonService")
	private CommonService commonService;
	
	@Resource(name = "employeeService")
	private EmployeeService employeeService;
	
	@Resource(name = "assetService")
	private AssetService assetService;
	
	@Resource(name = "itemService")
	private ItemService itemService;

	@Resource(name = "operationService")
	private OperationService operationService;

	@Resource(name = "excelRead")
	private ExcelRead excelRead;
	
	@Resource(name = "excelReportView")
	private ExcelReportView excelReportView;		
	
	@Resource(name = "bbsService")
    private BbsService bbsService;	

	//@Resource(name = "ExcelService")
	//private ExcelService excelService;
	
    @Resource(name="diagnosisService")
	private LifeService diagnosisService;

	@Resource(name = "estimationService")
	private EstimationService estimationService;
	
	@Resource(name = "riskService")
	private RiskService riskService;
	
	@Resource(name="reevalService")
	private ReevalService reevalService;
	
	@Resource(name="losService")
	private LosService losService;
	
	@Resource(name="lccService")
	private LccService lccService;
	
	@Resource(name="oipService")
	private OipService oipService;
	
	@Resource(name="FinanceAnalysisService")
    private FinanceAnalysisService financeAnalysisService;
	
	@Resource(name="boardService")
    private BoardService boardService;
	
	@RequestMapping(value = "/excel.do")
	public void excel(HttpServletResponse response,HttpServletRequest req) throws Exception {
		
		HashMap<String,Object> param = new HashMap<String,Object> ();
		param = CommonUtil.getParameterMap(req);
		String searchOptions = param.get("searchOptions").toString();
		JSONObject jo  = new JSONObject();
		jo = JSONObject.fromObject(searchOptions);
		System.out.println(">>>>>>>>>>>>>>"+jo);
		HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
		
		if(param.get("selectName").equals("selectAssetItemListExcel")) {
	    	ArrayList<HashMap<String,Object >>  l1 ;
	    	l1 = itemService.selectInfoItemList(new HashMap<String,Object >());
	    	//param.put("list",itemDao.selectInfoItemList());
	    	String list1[] = new String [l1.size()]; 
	    	for(int i=0;i<l1.size();i++ )
	    		list1[i] = l1.get(i).get("ITEM_CD").toString();			
			map.put("list1", list1);
		}
		
		System.out.println(">>>>>>>>>>>>>>"+map);
		
		
//		List <String> columnIdList = new ArrayList<>(Arrays.asList("NUM", "ASSET_CD", "ASSET_NM", "ASSET_PATH_SID"));
//		List <String> columnNameList = new ArrayList<>(Arrays.asList("번호", "자산코드", "자산명", "인벤토리 경로번호"));
		/*
		List <Integer> columnWidthList = new ArrayList<>(Arrays.asList(3000,10000,10000,3000));
		
		List <String> columnIdList = new ArrayList<>(Arrays.asList("RNUM","LIFE_SID","ASSET_SID","RESTRICT_LIFE_YN"
                ,"RESTRICT_LIFE_NUM" ,"VOLUME_LIFE_YN","VOLUME_LIFE_NUM"
                ,"SERVICE_LIFE_YN","SERVICE_LIFE_NUM","ECONOMY_LIFE_YN"
                ,"ECONOMY_LIFE_NUM" ,"PHYSICAL_LIFE_METHOD" ,"PHYSICAL_LIFE_RESULT"
                ,"PHYSICAL_LIFE_NUM","INSERT_ID","INSERT_DT"
                ,"UPDATE_ID","UPDATE_DT","PHYSICAL_LIFE_METHOD_NM"
                ,"ASSET_CD" ,"ASSET_NM","CLASS1_CD"
                ,"CLASS2_CD" ,"CLASS3_CD","CLASS4_CD"
                ,"CLASS5_CD","CLASS6_CD","CLASS7_CD"
                ,"CLASS8_CD","LIFE" ,"YEAR_OF_INSTALLATION"
                ,"YEARS_OF_USE","LIFE_NUM"
				));
		List <String> columnNameList = new ArrayList<>(Arrays.asList("RNUM","LIFE_SID","ASSET_SID","RESTRICT_LIFE_YN"
                ,"RESTRICT_LIFE_NUM" ,"VOLUME_LIFE_YN","VOLUME_LIFE_NUM"
                ,"SERVICE_LIFE_YN","SERVICE_LIFE_NUM","ECONOMY_LIFE_YN"
                ,"ECONOMY_LIFE_NUM" ,"PHYSICAL_LIFE_METHOD" ,"PHYSICAL_LIFE_RESULT"
                ,"PHYSICAL_LIFE_NUM","INSERT_ID","INSERT_DT"
                ,"UPDATE_ID","UPDATE_DT","PHYSICAL_LIFE_METHOD_NM"
                ,"ASSET_CD" ,"ASSET_NM","CLASS1_CD"
                ,"CLASS2_CD" ,"CLASS3_CD","CLASS4_CD"
                ,"CLASS5_CD","CLASS6_CD","CLASS7_CD"
                ,"CLASS8_CD","LIFE" ,"YEAR_OF_INSTALLATION"
                ,"YEARS_OF_USE","LIFE_NUM"));
		
		param.put("LIFE_SID",184);
		param.put("selectName", "selectExcelList2");
		param.put("templateFileName", "test");
		param.put("outputFileName", "test.xlsx");
		param.put("startRowIdx", 3);
		param.put("startColIdx", 0);
		param.put("columnIdList", columnIdList);
		param.put("columnNameList", columnNameList);
		param.put("columnWidthList", columnWidthList);
		boardService.selectExcelList(response,param);
		*/
		List <String> columnWidthList = new ArrayList<>(Arrays.asList((param.get("columnWidthList").toString().split(","))));
		List <String> columnIdList = new ArrayList<>(Arrays.asList((param.get("columnIdList").toString().split(","))));
		List <String> columnNameList = new ArrayList<>(Arrays.asList((param.get("columnNameList").toString().split(","))));
		
		
		//접속,출력로그
		//param.put("LIFE_SID",184);
		param.put("selectName", param.get("selectName").toString());
		param.put("searchOptions", map);
		param.put("templateFileName", param.get("templateFileName").toString());
		param.put("outputFileName", param.get("outputFileName").toString());
		param.put("startRowIdx", param.get("startRowIdx").toString());
		param.put("startColIdx", param.get("startColIdx").toString());
		param.put("columnIdList", columnIdList);
		param.put("columnNameList", columnNameList);
		param.put("columnWidthList", columnWidthList);		
		boardService.selectExcelList(response,param);

		String lg_desc = "";
		switch(param.get("selectName").toString()){
			case "getCommonCodeList" : lg_desc = "[메뉴 : 관리자 > 코드관리 ] - 공통코드.xlsx"; break;
			default : 
				lg_desc = param.get("selectName").toString();break;
		}
		//String user_id = req.getSession().getAttribute("logout_user_id").toString();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object principal = auth.getPrincipal();
		User user = ((UserDetailsServiceVO) principal).getUser();
		String user_id =  user.getE_ID();
		String ip_addr = req.getRemoteAddr();		
				param.put("lg_opt", "2");//1:로그인, 2:출력, 3:에러
				param.put("lg_desc", lg_desc);// 화면의 코드를 넘긴다. 예) 유량조사 0502
				param.put("login_user_id",user_id );
				param.put("E_ID", user_id );
				param.put("ip_addr", ip_addr );
				System.out.println(param);
				commonService.insertSystemLog(param);		
	}
	
    @RequestMapping(value = "/download/excel",method = RequestMethod.GET, produces = "application/vnd.ms-excel")
    public ModelAndView downloadExcel(HttpServletRequest request) throws Exception {
        
    	ModelAndView mav = new ModelAndView("excelView2");
    	//mav.addAttribute("excelMap", excelService.getExcel());

        return mav;
    }
    

	/**
	 * 교육,홍보,훈련,회의 보고서 
	 */
	@RequestMapping(value = "/getTestPic", method = RequestMethod.GET)
	public ModelAndView getTestPic(HttpServletRequest request) throws Exception {
		System.out.println("====================getTestPic");
		ModelAndView mav = new ModelAndView(excelReportView);
		
		String jobType = request.getParameter("jobType");
		String jobParam = request.getParameter("jobParam");
		JSONObject jo  = new JSONObject();
		jo = jo.fromObject(jobParam);
		System.out.println(">>>>>>>>>>"+jobType);
		System.out.println(">>>>>>>>>>"+jobParam);
		System.out.println(">>>>>>>>>>"+jo);
		
		//HashMap<String, Object> info01 = reportService.getEduDetailInfo(param);
		//mav.addObject("info01", info01);
		
		//mav.addObject("employees", employeeService.getUseEmployeeList(param));
		
		//사진대지
		setEmptyImagesHolder(mav);
		HashMap<String, Object> param = new HashMap<String, Object>();
		ArrayList<HashMap<String, Object>> images = null;
		ArrayList<String> sheetNames = new ArrayList<String>();
		
		// 개보수 보고서
		if(jobType != null &&jobType.equals("010202")){
			ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
			ArrayList<HashMap<String, Object>> list2 = new ArrayList<HashMap<String, Object>>();
			param.put("REPAIR_SID", jo.get("REPAIR_SID"));
			param.put("PHOTO_YN", "Y");
			
			images = operationService.getREPAIR_FILE(param);
			System.out.println(">>>>>>>>>>>>>>>>>"+ images );
			sheetNames.add("test");
			
			mav.addObject("images", images);
			mav.addObject("filename", "010203_template.xlsx");	
			mav.addObject("currDate",CommonUtil.getDate("yyyy-mm-dd"));
		
			mav.addObject("sheetNames", sheetNames);
			mav.addObject("IMAGE_CODE","00005");
			mav.addObject("IMAGE_NAME","테스텅");
			
			HashMap<String, Object> REPAIR = new HashMap<String, Object>();
			REPAIR.put("REPAIR_SID",jo.get("REPAIR_SID"));
			REPAIR.put("OPTION",3);
			REPAIR.put("REPAIR_NM", "");
			
			HashMap<String, Object> REPAIR_ASSET = new HashMap<String, Object>();
			REPAIR_ASSET.put("REPAIR_SID",jo.get("REPAIR_SID"));
			
			list = operationService.getREPAIR_ASSET(REPAIR_ASSET);
			list2 = operationService.getREPAIR(REPAIR);
			
			mav.addObject("list", list);
			System.out.println(">>>>>>>>>>>>>>>>>"+ list );
			mav.addObject("list2", list2);
			System.out.println(">>>>>>>>>>>>>>>>>"+ list2 );
			
			for(int i=0; i<images.size(); i++) {
				HashMap<String, Object> image = images.get(i);
				int filenumber = Integer.parseInt(image.get("F_NUM").toString());
				int seq =  (i+1);
				HashMap<String, Object> imageInfo = getFileInfo(filenumber);
				mav.addObject("IMAGE_"+seq, imageInfo.get("IMAGE"));
				mav.addObject("IMAGE_DESC_"+seq, images.get(i).get("F_DESC"));
			}
		}
		else if(jobType != null &&jobType.equals("010203")){
			ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
			ArrayList<HashMap<String, Object>> list2 = new ArrayList<HashMap<String, Object>>();
			param.put("INSPECT_SID", jo.get("INSPECT_SID"));
			param.put("PHOTO_YN", "Y");
			
			images = operationService.getINSPECT_FILE(param);
			System.out.println(">>>>>>>>>>>>>>>>>"+ images );
			sheetNames.add("test");
			
			mav.addObject("images", images);
			mav.addObject("filename", "010203_template.xlsx");	
			mav.addObject("currDate",CommonUtil.getDate("yyyy-mm-dd"));
		
			mav.addObject("sheetNames", sheetNames);
			mav.addObject("IMAGE_CODE","00005");
			mav.addObject("IMAGE_NAME","테스텅");
			
			HashMap<String, Object> INSPECT = new HashMap<String, Object>();
			INSPECT.put("INSPECT_SID",jo.get("INSPECT_SID"));
			INSPECT.put("OPTION",3);
			INSPECT.put("INSPECT_NM", "");
			
			HashMap<String, Object> INSPECT_ASSET = new HashMap<String, Object>();
			INSPECT_ASSET.put("INSPECT_SID",jo.get("INSPECT_SID"));
			
			list = operationService.getINSPECT_ASSET(INSPECT_ASSET);
			list2 = operationService.getINSPECT(INSPECT);
			
			mav.addObject("list", list);
			System.out.println(">>>>>>>>>>>>>>>>>"+ list );
			mav.addObject("list2", list2);
			System.out.println(">>>>>>>>>>>>>>>>>"+ list2 );
			
			for(int i=0; i<images.size(); i++) {
				HashMap<String, Object> image = images.get(i);
				int filenumber = Integer.parseInt(image.get("F_NUM").toString());
				int seq =  (i+1);
				HashMap<String, Object> imageInfo = getFileInfo(filenumber);
				mav.addObject("IMAGE_"+seq, imageInfo.get("IMAGE"));
				mav.addObject("IMAGE_DESC_"+seq, images.get(i).get("F_DESC"));
			}
		}
		/*else if("010305_2".equals(jobType)) {//위험도 평가
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map.remove("page");
			map.remove("rows");
			
			mav.addObject("filename", "010305_2_template.xlsx");	
			mav.addObject("currDate",CommonUtil.getDate("yyyy-mm-dd"));
		
			HashMap<String, Object> list = losService.selectEvalue(map);
			ArrayList<HashMap<String, Object>> array = (ArrayList<HashMap<String, Object>>)list.get("Array");
			ArrayList<ArrayList<HashMap<String, Object>>> item = new ArrayList<ArrayList<HashMap<String, Object>>>();
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			for(int i=0; i < array.size(); i++) {
				int index = Integer.parseInt(array.get(i).get("LOS_ITEM_CD").toString()) - 1;
				item.get(index).add(array.get(i));
				array.get(i).put("ROWNUM", item.get(index).size());
			}
			mav.addObject("LOS_MEMO", map.get("LOS_MEMO"));
			mav.addObject("INSERT_ID", map.get("INSERT_ID"));
			mav.addObject("UPDATE_DT", map.get("UPDATE_DT"));
			mav.addObject("list1", (ArrayList<HashMap<String, Object>>)item.get(0));
			mav.addObject("list2", (ArrayList<HashMap<String, Object>>)item.get(1));
			mav.addObject("list3", (ArrayList<HashMap<String, Object>>)item.get(2));
			mav.addObject("list4", (ArrayList<HashMap<String, Object>>)item.get(3));
			mav.addObject("list5", (ArrayList<HashMap<String, Object>>)item.get(4));
			mav.addObject("list6", (ArrayList<HashMap<String, Object>>)item.get(5));
			
			mav.addObject("sheetNames", sheetNames);
		}*/
		else {
			images = itemService.selectAssetPic(param);
			System.out.println(">>>>>>>>>>>>>>>>>"+ images );
			sheetNames.add("test");
			
			mav.addObject("images", images);
			mav.addObject("filename", "TEST_ALBUM.xlsx");	
			mav.addObject("currDate", CommonUtil.getDate("yyyy-mm-dd"));
		}
		return mav;
	}
	//이미지 없음 
	private void setEmptyImagesHolder(ModelAndView mav) throws Exception {
		InputStream imageInputStream = 
				new FileInputStream(Configuration.WEB_ROOT_PATH + "/resources/images/empty.png");
		byte[] imageBytes = Util.toByteArray(imageInputStream);
		for(int i=0; i<10; i++) {
			int seq = (i+1);
			mav.addObject("IMAGE_"+seq, imageBytes);
			mav.addObject("IMAGE_DESC_"+seq, "");
		}
	}
	
	/*setImagesHolder(mav,filenumber);
	//이미지 있음 
	private void setImagesHolder(ModelAndView mav,int filenumber) throws Exception {
		
		HashMap<String, Object> fileInfo = commonService.getFileInfo(filenumber);
		HashMap<String, Object> imageInfo = new HashMap<String, Object>();
		String f_path = fileInfo.get("F_PATH").toString();
		InputStream imageInputStream = 
				new FileInputStream(Configuration.WEB_ROOT_PATH + f_path);
		byte[] imageBytes = Util.toByteArray(imageInputStream);
		for(int i=0; i<10; i++) {
			int seq = (i+1);
			mav.addObject("IMAGE_"+seq, imageBytes);
			mav.addObject("IMAGE_DESC_"+seq, "");
		}
	}
	*/
	private HashMap<String, Object> getFileInfo(int filenumber) throws Exception {
		HashMap<String, Object> fileInfo = commonService.getFileInfo(filenumber);
		HashMap<String, Object> imageInfo = new HashMap<String, Object>();
		if(fileInfo != null) {
			String f_path = fileInfo.get("F_PATH").toString();
			File f_check = new File(Configuration.WEB_ROOT_PATH + f_path);
			if(f_check.isFile()) {
				InputStream imageInputStream = 
						new FileInputStream(Configuration.WEB_ROOT_PATH + f_path);
				byte[] imageBytes = Util.toByteArray(imageInputStream);
				imageInfo.put("IMAGE", imageBytes);
				imageInfo.put("IMAGE_DESC", fileInfo.get("F_TITLE").toString());
			}else {
				InputStream imageInputStream = 
						new FileInputStream(Configuration.WEB_ROOT_PATH + "/resources/images/empty.png");
				byte[] imageBytes = Util.toByteArray(imageInputStream);
				imageInfo.put("IMAGE", imageBytes);
				imageInfo.put("IMAGE_DESC", fileInfo.get("F_TITLE").toString());
			}	
		}else {
			InputStream imageInputStream = 
					new FileInputStream(Configuration.WEB_ROOT_PATH + "/resources/images/empty.png");
			byte[] imageBytes = Util.toByteArray(imageInputStream);
			imageInfo.put("IMAGE", imageBytes);
			imageInfo.put("IMAGE_DESC", "");
		}
		return imageInfo;
	}
	
	/**
	 * 엑셀 다운로드 공통 2019v
	 *0 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	// http://localhost:8080/report/getExcelDownload
	@RequestMapping(value = "/getExcelDownload")
	public ModelAndView getExcelDownload(HttpServletRequest request) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object principal = auth.getPrincipal();
		User user = ((UserDetailsServiceVO) principal).getUser();
		String user_id =  user.getE_ID();		
		System.out.println("====================getExcelDownload");
		
		ModelAndView mav = new ModelAndView(excelView);
		//ModelAndView mav = new ModelAndView(excelReportView);
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.getParameterMap(request);
		System.out.println("====================param"+param);
		String jobType = (String) param.get("jobType");
		String jnum = (String) param.get("jnum");

		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		ArrayList<HashMap<String, Object>> addComboList = new ArrayList<HashMap<String, Object>>();

		//test data
//		jobType = "011302_3";
//		param.put("s_date", "20190101");
//		param.put("e_date", "20190701");
//		param.put("jnum", "13");//유량 
//		param.put("jnum", "118");//수질
 
		String lg_desc = "";
		if ("010102".equals(jobType)) {//자산관리 목록
			list = itemService.selectAssetItemListExcel(param);
			mav.addObject("list", list);
			mav.addObject("filename", "010102_template.xlsx");
			mav.addObject("refilename", "자산관리.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :정보관리>자산관리] - 자산관리.xlsx ";
		}
		else if ("010301".equals(jobType)) {//기술진단자료 목록
			param.put("page", 0);
			param.put("rows", 10);
			param.put("bbs_group_cd", request.getParameter("bbs_group_cd"));
			param.put("subject", request.getParameter("searchText"));
			param.put("category", request.getParameter("category"));
			param.put("start_dt", request.getParameter("start_dt"));
			param.put("end_dt", request.getParameter("end_dt"));
			param.put("temp_yn", request.getParameter("temp_yn"));
			param.put("delete_yn", request.getParameter("delete_yn"));
			list = bbsService.getList(param);
			mav.addObject("list", list);
			mav.addObject("filename", "010301_template.xlsx");
			mav.addObject("refilename", "기술안전진단 자료관리.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :운영관리] - 기술안전진단 자료관리.xlsx ";
		}
		else if("010302".equals(jobType)) {//간접평가 보고서
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("INSERT_ID","testid");
			map.put("category","1");
			map.put("EST_SID",jnum);
			map.put("MAX_REPORT_PRINT_LINE_CNT", Configuration.MAX_REPORT_PRINT_LINE_CNT);
			HashMap<String,Object> info = estimationService.getListExcel(map).get(0);			
			list = estimationService.getReportExcel(map);
			//System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("EST_NM", info.get("EST_NM"));
			mav.addObject("EST_DT", info.get("EST_START_DT"));
			mav.addObject("INSERT_ID", info.get("INSERT_ID"));
			mav.addObject("INSERT_DT", info.get("INSERT_DT"));						
			mav.addObject("filename", "010302_template.xlsx");
			mav.addObject("refilename", "간접상태평가보고서.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 간접상태평가보고서.xlsx ";
		}
		else if("010302_0".equals(jobType)) {//간접평가 목록 출력
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("INSERT_ID","testid");
			map.put("category","1");
			map.put("START_DT",request.getParameter("start_dt"));
			map.put("END_DT",request.getParameter("end_dt"));
			map.put("searchText",request.getParameter("searchText"));
			System.out.println(">>>>>>>>>>>>>>"+map);
			list = estimationService.getListExcel(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("filename", "010302_0_template.xlsx");
			mav.addObject("refilename", "간접평가 목록.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 간접평가 목록.xlsx ";
		}
		else if("010302_1".equals(jobType)) {//간접평가 항목 출력
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("INSERT_ID","testid");
			map.put("category","1");
			map.put("est_spec_sid",jnum);
			HashMap<String,Object> info = estimationService.getFactorListExcel(map).get(0);			
			System.out.println(">>>>>>>>>>>>>>"+map);
			list = estimationService.getFactorDetailExcel(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("EST_SPEC_NM", info.get("EST_SPEC_NM"));
			mav.addObject("EST_DT", info.get("APPLY_DT"));
			mav.addObject("INSERT_ID", info.get("INSERT_ID"));
			mav.addObject("INSERT_DT", info.get("INSERT_DT"));
			mav.addObject("filename", "010302_1_template.xlsx");
			mav.addObject("refilename", "간접평가 항목.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 간접평가 항목.xlsx ";
		}
		else if("010302_2".equals(jobType)) {//간접평가 개량방안 출력
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("INSERT_ID","testid");
			map.put("category","1");
			map.put("est_imp_sid",jnum);
			System.out.println(">>>>>>>>>>>>>>"+map);
			HashMap<String,Object> info = estimationService.getGradeListExcel(map).get(0);	
			list = estimationService.getGradeDetail(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("EST_IMP_NM", info.get("EST_IMP_NM"));
			mav.addObject("EST_DT", info.get("APPLY_DT"));
			mav.addObject("INSERT_ID", info.get("INSERT_ID"));
			mav.addObject("INSERT_DT", info.get("INSERT_DT"));
			mav.addObject("filename", "010302_2_template.xlsx");
			mav.addObject("refilename", "간접평가개량방안.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 간접평가개량방안.xlsx ";
		}
		else if("011506".equals(jobType)) {//직접평가 보고서
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("INSERT_ID","testid");
			map.put("category","2");
			map.put("EST_SID",jnum);
			map.put("MAX_REPORT_PRINT_LINE_CNT", Configuration.MAX_REPORT_PRINT_LINE_CNT);
			HashMap<String,Object> info = estimationService.getListExcel(map).get(0);		
			list = estimationService.getReportExcel(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("EST_NM", info.get("EST_NM"));
			mav.addObject("EST_DT", info.get("EST_START_DT"));
			mav.addObject("INSERT_ID", info.get("INSERT_ID"));
			mav.addObject("INSERT_DT", info.get("INSERT_DT"));			
			mav.addObject("filename", "011506_template.xlsx");
			mav.addObject("refilename", "직접평가보고서.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 직접평가보고서.xlsx ";
		}
		else if("011506_0".equals(jobType)) {//직접평가 목록 출력
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("INSERT_ID","testid");
			map.put("category","2");
			map.put("START_DT",request.getParameter("start_dt"));
			map.put("END_DT",request.getParameter("end_dt"));
			map.put("searchText",request.getParameter("searchText"));			
			System.out.println(">>>>>>>>>>>>>>"+map);
			list = estimationService.getListExcel(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("filename", "011506_0_template.xlsx");
			mav.addObject("refilename", "직접평가 목록.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 직접평가 목록.xlsx ";
		}
		else if("011506_1".equals(jobType)) {//직접평가 항목 출력
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("INSERT_ID","testid");
			map.put("category","2");
			map.put("est_spec_sid",jnum);
			System.out.println(">>>>>>>>>>>>>>"+map);
			HashMap<String,Object> info = estimationService.getFactorListExcel(map).get(0);	
			list = estimationService.getFactorDetail(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("EST_SPEC_NM", info.get("EST_SPEC_NM"));
			mav.addObject("EST_DT", info.get("APPLY_DT"));
			mav.addObject("INSERT_ID", info.get("INSERT_ID"));
			mav.addObject("INSERT_DT", info.get("INSERT_DT"));			
			mav.addObject("filename", "011506_1_template.xlsx");
			mav.addObject("refilename", "직접평가 항목.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 직접평가 항목.xlsx ";
		}
		else if("011506_2".equals(jobType)) {//직접평가 개량방안 출력
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("category","2");
			map.put("est_imp_sid",jnum);
			System.out.println(">>>>>>>>>>>>>>"+map);
			HashMap<String,Object> info = estimationService.getGradeListExcel(map).get(0);	
			list = estimationService.getGradeDetail(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("EST_IMP_NM", info.get("EST_IMP_NM"));
			mav.addObject("EST_DT", info.get("APPLY_DT"));
			mav.addObject("INSERT_ID", info.get("INSERT_ID"));
			mav.addObject("INSERT_DT", info.get("INSERT_DT"));			
			mav.addObject("filename", "011506_2_template.xlsx");
			mav.addObject("refilename", "직접평가개량방안.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 직접평가개량방안.xlsx ";
		}
		else if("011506_3".equals(jobType)) {//직접평가 개량방안 출력
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("category","2");
			map.put("EST_SID",jnum);
			System.out.println(">>>>>>>>>>>>>>"+map);
			
			list = estimationService.getDirEstAssetList(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("filename", "011506_3_template.xlsx");
			mav.addObject("refilename", "직접평가_결과입력.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 직접평가_결과입력.xlsx ";
		}		
		else if("010303_1".equals(jobType)) {//잔존 수명 예측 평가 - 서치엑셀
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			System.out.println(">>>>>>>>>>>>>>"+jo);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			System.out.println(">>>>>>>>>>>>>>"+map);
			
			list = diagnosisService.getLIFE(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("filename", "010303_1_template.xlsx");
			mav.addObject("refilename", "잔존수명 예측 평가 목록.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 잔존수명 예측 평가 목록.xlsx ";
		}
		else if("010303_2".equals(jobType)) {//잔존 수명 예측 평가 - 평가 관리 (신규)
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			System.out.println(">>>>>>>>>>>>>>"+jo);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			System.out.println(">>>>>>>>>>>>>>"+map);
			list = diagnosisService.getLIFE_ASSET_REPORT_FORM(map);
			mav.addObject("list", list);
			mav.addObject("filename", "010303_2_template.xlsx");
			mav.addObject("refilename", "잔존 수명 예측 평가.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 잔존 수명 예측 평가.xlsx ";
		}
		else if("010303_3".equals(jobType)) {//잔존 수명 예측 평가 - 상태등급(신규)
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			System.out.println(">>>>>>>>>>>>>>"+jo);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			System.out.println(">>>>>>>>>>>>>>"+map);
			
			int YEAR = Integer.parseInt(map.get("EST_YEAR").toString().substring(0,4));
			int count = 1;
			for(int i = YEAR; i > YEAR -10; i--) {
				map.put("YEAR_CD_"+count, i);
				count++;
			}
			map.remove("page");
			map.remove("rows");
			//list = diagnosisService.getLIFE_STATE_ASSET(map);
			System.out.println("??????????????"+list);
			mav.addObject("year", YEAR);
			mav.addObject("list", list);
			mav.addObject("filename", "010303_3_template.xlsx");
			mav.addObject("refilename", "자산 상태등급 진단.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 잔존 수명 예측 평가.xlsx ";
		}
		else if("010303_4".equals(jobType)) {//잔존 수명 예측 평가 - 보고서
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map.put("EST_YYYYMM", map.get("YEAR").toString() + " " + map.get("MONTH").toString());
			map.put("SYS_DT", CommonUtil.getDate("yyyy-mm-dd"));
			
			map.put("MAX_REPORT_PRINT_LINE_CNT", Configuration.MAX_REPORT_PRINT_LINE_CNT);
			
			list = diagnosisService.getLIFE_ASSET_REPORT(map);
			
			mav.addObject("info", map);
			mav.addObject("list", list);
			mav.addObject("filename", "010303_4_template.xlsx");
			mav.addObject("refilename", "잔존수명 예측 보고서.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 잔존수명 예측 보고서.xlsx ";
		}
		else if("010303_5".equals(jobType)) {//잔존 수명 예측 평가 - 상태등급
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			System.out.println(">>>>>>>>>>>>>>"+jo);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			
			
			int YEAR = Integer.parseInt(map.get("YEAR").toString().substring(0,4));
			int count = 1;
			for(int i = YEAR; i > YEAR -10; i--) {
				map.put("YEAR_CD_"+count, i);
				count++;
			}
			map.remove("page");
			map.remove("rows");
			System.out.println(">>>>>>>>>>>>>>"+map);
			//list = diagnosisService.getLIFE_STATE_ASSET(map);
			System.out.println("??????????????"+list);
			mav.addObject("year", YEAR);
			mav.addObject("list", list);
			mav.addObject("filename", "010303_5_template.xlsx");
			mav.addObject("refilename", "자산_상태등급_진단.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 잔존 수명 예측 평가.xlsx ";
		}
		else if("010303_6".equals(jobType)) {//잔존 수명 예측 평가 - 평가항목 관리
			//HashMap<String, Object> map = diagnosisService.getFactor();
			
			//list = map.get("ListInfo");
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("filename", "010303_6_template.xlsx");
			mav.addObject("refilename", "평가항목_관리.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 평가항목 관리.xlsx ";
		}
		else if("010304_0".equals(jobType)) {//위험도 평가
			//list = estimationService.getRiskExcelList(param);
			//mav.addObject("list", list);
			mav.addObject("list", estimationService.getRiskExcelList1(param));
			mav.addObject("filename", "010304_0_template.xlsx");
			mav.addObject("refilename", "위험도평가.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 평가분석 > 위험도 평가 ] - 위험도평가.xlsx";
		}
		else if("010304_1".equals(jobType)) {//위험도 평가
			param.put("RISK_PROB_SID",jnum);
			HashMap<String,Object> info = riskService.getFactorList(param).get(0);
			list = riskService.getFactorDetail(param);
			mav.addObject("list", list);
			mav.addObject("EST_SPEC_NM",info.get("RISK_PROB_NM").toString());
			mav.addObject("INSERT_ID",info.get("INSERT_ID").toString());
			mav.addObject("INSERT_DT",info.get("INSERT_DT").toString());
			mav.addObject("UPDATE_ID",info.get("UPDATE_ID").toString());
			mav.addObject("UPDATE_DT",info.get("UPDATE_DT").toString());
			mav.addObject("filename", "010304_1_template.xlsx");
			mav.addObject("refilename", "위험도평가항목.xlsx");
			lg_desc = "[메뉴 : 평가분석 > 위험도 평가 ] - 평가항목.xlsx";			
		}
		else if("010304_2".equals(jobType)) {//위험도 평가
			param.put("RISK_SPEC_SID",jnum);
			HashMap<String,Object> info = riskService.getGradeList(param).get(0);
			list = riskService.getGradeDetail(param);
			mav.addObject("list", list);
			mav.addObject("EST_SPEC_NM",info.get("EST_SPEC_NM").toString());
			mav.addObject("INSERT_ID",info.get("INSERT_ID").toString());
			mav.addObject("INSERT_DT",info.get("INSERT_DT").toString());
			mav.addObject("UPDATE_ID",info.get("UPDATE_ID").toString());
			mav.addObject("UPDATE_DT",info.get("UPDATE_DT").toString());			
			mav.addObject("filename", "010304_2_template.xlsx");
			mav.addObject("refilename", "파괴심각도.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 평가분석 > 위험도 평가 ] - 파괴심각도.xlsx";
		}
		else if("010304".equals(jobType)) {//위험도 평가
			param.put("RISK_SID",jnum);
			param.put("delete_yn","N");
			param.put("CLASS3_CD","");
			param.put("CLASS4_CD","");
			param.put("MAX_REPORT_PRINT_LINE_CNT", Configuration.MAX_REPORT_PRINT_LINE_CNT);
			HashMap<String,Object> info = estimationService.getRiskExcelList(param).get(0);
			//list = riskService.getAssetListExcel(param);
			//mav.addObject("list", list);
			mav.addObject("list", riskService.getAssetListExcel1(param));
			mav.addObject("EST_NM",info.get("EST_NM").toString());
			mav.addObject("E_NAME",info.get("E_NAME").toString());
			mav.addObject("INSERT_DT",info.get("INSERT_DT").toString());
			mav.addObject("PRINT_DT","2022-05-15");
			mav.addObject("EST_YYYYMM",info.get("EST_YYYYMM").toString());			
			mav.addObject("filename", "010304_template.xlsx");
			mav.addObject("refilename", "위험도평가보고서.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 평가분석 > 위험도 평가 ] - 보고서.xlsx";
		}		
		else if("010305_1".equals(jobType)) {//서비스 평가
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map.remove("page");
			map.remove("rows");
			
			list = losService.selectLos(map);
			mav.addObject("list", list);
			mav.addObject("filename", "010305_1_template.xlsx");
			mav.addObject("refilename", "서비스분석결과.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 평가분석 > 서비스수준분석 ] - 분석결과.xlsx";
		}
		else if("010305_2".equals(jobType)) {//서비스 수준 분석
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			HashMap<String, Object> result = losService.selectEvalue(map); 
			System.out.println("------------------------" + result);
			
			HashMap<String, Object> info = (HashMap<String, Object>)result.get("Info");
			info.put("USER_DT", info.get("UPDATE_DT") == null ? info.get("INSERT_DT") : info.get("UPDATE_DT"));
			info.put("USER_ID", info.get("UPDATE_ID") == null ? info.get("INSERT_ID") : info.get("UPDATE_ID"));
			
			ArrayList<HashMap<String, Object>> array = (ArrayList<HashMap<String, Object>>)result.get("Array");
			ArrayList<ArrayList<HashMap<String, Object>>> item = new ArrayList<ArrayList<HashMap<String, Object>>>();
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			item.add(new ArrayList<HashMap<String, Object>>());
			for(int i=0; i < array.size(); i++) {
				int index = Integer.parseInt(array.get(i).get("LOS_ITEM_CD").toString()) - 1;
				item.get(index).add(array.get(i));
				array.get(i).put("ROWNUM", item.get(index).size());
			}
			
			System.out.println("------------------------" + item);
			//list = 
			mav.addObject("info", result.get("Info"));
			
			mav.addObject("manpower", (ArrayList<HashMap<String, Object>>)item.get(0));
			mav.addObject("facility", (ArrayList<HashMap<String, Object>>)item.get(1));
			mav.addObject("operation", (ArrayList<HashMap<String, Object>>)item.get(2));
			mav.addObject("service", (ArrayList<HashMap<String, Object>>)item.get(3));
			mav.addObject("environment", (ArrayList<HashMap<String, Object>>)item.get(4));
			mav.addObject("finance", (ArrayList<HashMap<String, Object>>)item.get(5));
			
			mav.addObject("filename", "010305_2_template.xlsx");
			mav.addObject("refilename", "서비스분석보고서.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 평가분석 > 서비스수준분석 ] - 서비스분석보고서.xlsx";
		}
		else if("010305_3".equals(jobType)) {//위험도 평가
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			HashMap<String,Object> newMap = losService.getWeightList(map);
			list = (ArrayList<HashMap<String,Object>>)newMap.get("List");
			
			for(int i=0; i < list.size(); i++) {
				list.get(i).put("ROWNUM", i + 1);
			}
			mav.addObject("list", list);
			mav.addObject("filename", "010305_3_template.xlsx");
			mav.addObject("refilename", "서비스분석결과2.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 평가분석 > 서비스수준분석2 ] - 분석결과2.xlsx";
		}
		else if("010306_1".equals(jobType)) {//생애주기
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map.remove("page");
			map.remove("rows");
			
			list = lccService.getLCC(map);
			mav.addObject("list", list);
			mav.addObject("filename", "010306_1_template.xlsx");
			mav.addObject("refilename", "생애주기_비용분석_결과.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 평가분석 > 생애주기 ] - 분석결과.xlsx";
		}
		else if("010306_2".equals(jobType)) {//생애주기 - 보고서1
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map.remove("page");
			map.remove("rows");
			map.put("FN_CD", "F");
			HashMap<String, Object> report = null;
			
			map.put("MAX_REPORT_PRINT_LINE_CNT", Configuration.MAX_REPORT_PRINT_LINE_CNT);
			
			report = lccService.report(map);
			
			mav.addObject("flist", report.get("flist"));
			mav.addObject("ftotal", report.get("ftotal"));
			
			mav.addObject("nlist", report.get("nlist"));
			mav.addObject("ntotal", report.get("ntotal"));
			
			mav.addObject("tlist", report.get("tlist"));
			mav.addObject("ttotal", report.get("ttotal"));
			
			mav.addObject("filename", "010306_2_template.xlsx");
			mav.addObject("refilename", "생애주기_비용분석_보고서.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 평가분석 > 생애주기 ] - 분석보고서.xlsx";
		}
		else if("010306_3".equals(jobType)) {//위험도 평가
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map = lccService.getLCCDetails(map);
			list = (ArrayList<HashMap<String,Object>>)map.get("List");
			System.out.println(list);
			mav.addObject("list", list);
			mav.addObject("filename", "010306_3_template.xlsx");
			mav.addObject("refilename", "생애주기_비용분석_결과.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 평가분석 > 생애주기 ] - 분석결과.xlsx";
		}
		else if("010307_1".equals(jobType)) {//자산가치 재평가
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			System.out.println(">>>>>>>>>>>>>>"+jo);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			System.out.println(">>>>>>>>>>>>>>"+map);
			
			list = reevalService.selectReEst(map);
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("filename", "010303_1_template.xlsx");
			mav.addObject("refilename", "자산가치_재평가_목록.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 자산가치 재평가 목록.xlsx ";
		}
		else if("010307_2".equals(jobType)) {//자산가치 재평가 - 보고서
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			
			map.put("MAX_REPORT_PRINT_LINE_CNT", Configuration.MAX_REPORT_PRINT_LINE_CNT);
			
			list = reevalService.report(map);
			
			mav.addObject("header", map);
			mav.addObject("list", list);
			mav.addObject("filename", "010307_2_template.xlsx");
			mav.addObject("refilename", "자산기치_재평가_보고서.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 자산기치 재평가 보고서.xlsx ";
		}
		else if("010307_3".equals(jobType)) {//위험도 평가
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("index", 0);
			list = (ArrayList<HashMap<String, Object>>)reevalService.getWeightSpec(map).get("WeightList");
			System.out.println("??????????????"+list);
			mav.addObject("list", list);
			mav.addObject("filename", "010307_3_template.xlsx");
			mav.addObject("refilename", "가중치_기준_관리.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :평가분석] - 가중치 기준 관리.xlsx ";
		}
		else if("010401_1".equals(jobType)) {//최적 투자계획 수립
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map.remove("page");
			map.remove("rows");
			
			list = oipService.getList(map);
			
			mav.addObject("list", list);
			mav.addObject("filename", "010401_1_template.xlsx");
			mav.addObject("refilename", "최적_투자계획_수립_결과.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 재무분석 > 최적 투자계획 수립 ] - 분석결과.xlsx";
		}
		else if("010401_2".equals(jobType)) {//최적 투자계획 수립 - 보고서
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map.put("SYS_DT", CommonUtil.getDate("yyyy-mm-dd"));
			
			list = oipService.report(map);
			
			mav.addObject("info", map);
			mav.addObject("list", list);
			mav.addObject("filename", "010401_2_template.xlsx");
			mav.addObject("refilename", "최적_투자계획_수립_보고서.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 :재무분석] - 최적_투자계획_수립_보고서.xlsx ";
		}
		else if("010402_1".equals(jobType)) {//재정계획 수립
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map.remove("page");
			map.remove("rows");
			
			list = financeAnalysisService.getFinancialPlanList(map);
			
			mav.addObject("list", list);
			mav.addObject("filename", "010402_1_template.xlsx");
			mav.addObject("refilename", "재무분석_결과.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 재무분석 > 재무분석 ] - 분석결과.xlsx";
		}
		else if("010403_1".equals(jobType)) {//자산관리계획수립
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			map.remove("page");
			map.remove("rows");
			
			list = financeAnalysisService.getAssetManagementPlanList(map);
			mav.addObject("list", list);
			mav.addObject("filename", "010404_1_template.xlsx");
			mav.addObject("refilename", "자산관리계획수립목록.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 재무분석 > 자산관리계획수립 ] - 목록.xlsx";
		}		
		/*else if("010402_2".equals(jobType)) {//재정수지 총괄
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			//변경
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			
			list = financeAnalysisService.getFinancialPlanList(map);
			// 여기 까지
			mav.addObject("list", list);
			mav.addObject("filename", "010402_2_template.xlsx");
			mav.addObject("refilename", "재무분석_결과.xlsx");
			mav.addObject("addComboList", null);
			lg_desc = "[메뉴 : 재무분석 > 재무분석 ] - 분석결과.xlsx";
		}*/
		//개보수
		else if("010202_1".equals(jobType)) {
			mav.addObject("list", list);
			mav.addObject("filename", "010202_1_template.xlsx");
			mav.addObject("refilename", ""+ jnum + "_개보수 양식.xlsx");
			mav.addObject("addComboList", null);
			
			lg_desc = "[메뉴 : 운영관리 > 개보수 양식] - 개보수 양식.xlsx ";
		}//개보수
		else if("010202_2".equals(jobType)) {
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = jo.fromObject(Param);
			jo.remove("page");
			jo.remove("rows");
			list = operationService.getREPAIR(CommonUtil.convertJsonToMap(jo));
			
			System.out.println(">>>>>>>>>>>>>>"+list);
			
			mav.addObject("list", list);
			mav.addObject("filename", "010202_2_2template.xlsx");
			mav.addObject("refilename", "개보수_리스트.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 개보수 리스트] - 개보수 리스트.xlsx ";
		}
		else if("010202_3".equals(jobType)) {
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			jo.remove("page");
			jo.remove("rows");
			System.out.println(">>>>>>>>>>>>>>"+jo);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			System.out.println(">>>>>>>>>>>>>>"+map);
			map.put("REPORT", 1);			
			list = operationService.getREPAIR_ASSET(map);
			System.out.println("??????????????"+list);
			
			mav.addObject("list", list);
			mav.addObject("filename", "010202_3_template.xlsx");
			mav.addObject("refilename", "개보수_리스트.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 개보수 관리] - 개보수 목록.xlsx ";
		}
	
		//조사 및 탐사 양식
		else if("010203_1".equals(jobType)) {
			mav.addObject("list", list);
			mav.addObject("filename", "010203_1_template.xlsx");
			mav.addObject("refilename", ""+ jnum + "_조사 및 탐사 양식.xlsx");
			mav.addObject("addComboList", null);
			
			lg_desc = "[메뉴 : 운영관리 > 조사 및 탐사 양식] - 조사 및 탐사 양식.xlsx ";
		}//조사 및 탐사 양식
		else if("010203_2".equals(jobType)) {
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = jo.fromObject(Param);
			jo.remove("page");
			jo.remove("rows");
			list = operationService.getINSPECT(CommonUtil.convertJsonToMap(jo));
			
			System.out.println(">>>>>>>>>>>>>>"+list);
			
			mav.addObject("list", list);
			mav.addObject("filename", "010203_2_template.xlsx");
			mav.addObject("refilename", "조사_및_탐사_리스트.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 조사 및 탐사 리스트] - 조사 및 탐사 리스트.xlsx ";
		}
		else if("010203_3".equals(jobType)) {
			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			System.out.println(">>>>>>>>>>>>>>"+jo);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			System.out.println(">>>>>>>>>>>>>>"+map);
			map.put("REPORT", 1);			
			list = operationService.getINSPECT_ASSET(map);
			System.out.println("??????????????"+list);
			
			mav.addObject("list", list);
			mav.addObject("filename", "010203_3_template.xlsx");
			mav.addObject("refilename", "조사_및_탐사관리_리스트.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 조사 및 탐사관리] - 조사 및 탐사관리 리스트.xlsx ";
		}
		else if ("010106".equals(jobType)) { // 사용자관리 
			list = employeeService.getEmployeeListExcel(param);

			mav.addObject("list", list);
			mav.addObject("filename", "010106_template.xlsx");
			mav.addObject("refilename", "사용자관리_다운로드.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 관리자> 사용자관리] - 사용자관리_목록.xlsx";

			
		}  else if ("010003".equals(jobType)) { // 정보 관리 > 레벨 코드 관리
			list = assetService.getLevelCodeListExcel(param);
			mav.addObject("list", list);
			mav.addObject("filename", "010003_template.xlsx");
			mav.addObject("refilename", "레벨코드관리_다운로드.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 정보관리> 레벨코드관리] - 레벨코드관리_목록.xlsx";

		} else if ("010004".equals(jobType)) { // 정보 관리 > 인벤토리 경로 관리
			list = itemService.getLevelNumListExcel(param);
			mav.addObject("list", list);
			mav.addObject("filename", "010004_template.xlsx");
			mav.addObject("refilename", "인벤토리경로_다운로드.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 정보관리> 인벤토리경로] - 인벤토리경로_목록.xlsx";

		} else if ("010201_1".equals(jobType)) { // 운영관리 > 상수운영관리 > 유량 및 수압 데이터 (월 단위)
			list = operationService.getData_flux_wp_month(param);
			mav.addObject("list", list);
			mav.addObject("filename", "010201_1_template.xlsx");
			mav.addObject("refilename", "유량및수압_보고서_월단위.xlsx");
			mav.addObject("addComboList", null);

			System.out.println(mav);
			lg_desc = "[메뉴 : 운영관리 > 상수운영관리 ] - 유량및수압_보고서_월단위.xlsx";

		} else if ("010201_2".equals(jobType)) { // 운영관리 > 상수운영관리 > 유량 및 수압 데이터 (일 단위)
			list = operationService.getData_flux_wp_day(param);
			mav.addObject("list", list);
			mav.addObject("filename", "010201_2_template.xlsx");
			mav.addObject("refilename", "유량및수압_보고서_일단위.xlsx");
			mav.addObject("addComboList", null);

			System.out.println(mav);
			lg_desc = "[메뉴 : 운영관리 > 상수운영관리 ] - 유량및수압_보고서_일단위.xlsx";

		} else if ("010201_3".equals(jobType)) { // 운영관리 > 상수운영관리 > 유량 및 수압 데이터 (시간 단위)
			list = operationService.getData_flux_wp_hour(param);
			mav.addObject("list", list);
			mav.addObject("filename", "010201_3_template.xlsx");
			mav.addObject("refilename", "유량및수압_보고서_시간단위.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 상수운영관리 ] - 유량및수압_보고서_시간단위.xlsx";

		} else if ("010201_4".equals(jobType)) { // 운영관리 > 상수운영관리 > 유량 및 수압 데이터 (분 단위)
			list = operationService.getData_flux_wp_min(param);
			mav.addObject("list", list);
			mav.addObject("filename", "010201_4_template.xlsx");
			mav.addObject("refilename", "유량및수압_보고서_분단위.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 상수운영관리 ] - 유량및수압_보고서_분 단위.xlsx";

		} else if ("010201_5".equals(jobType)) { // 운영관리 > 상수운영관리 > 수질 데이터 (시간 단위)
			list = operationService.getData_waterQuality_hour(param); // 바꿔야 함
			mav.addObject("list", list);
			mav.addObject("filename", "010201_5_template.xlsx");
			mav.addObject("refilename", "수질보고서_시간단위.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 상수운영관리 ] - 수질보고서_시간단위.xlsx";

		} else if ("010201_6".equals(jobType)) { // 운영관리 > 상수운영관리 > 수질 데이터 (분 단위)
			list = operationService.getData_waterQuality_min(param); // 바꿔야 함
			mav.addObject("list", list);
			mav.addObject("filename", "010201_6_template.xlsx");
			mav.addObject("refilename", "수질보고서_분단위.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 상수운영관리 ] - 수질보고서_분단위.xlsx";

		} else if ("010205".equals(jobType)) { // 운영관리 > 상수운영관리 > 민원관리
			list = operationService.getMinwonExcelList(param); // 바꿔야 함
			mav.addObject("list", list);
			mav.addObject("filename", "010205_template.xlsx");
			mav.addObject("refilename", "민원관리_다운로드.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 민원관리 ] - 민원관리_목록.xlsx";

			
		} else if ("010005".equals(jobType)) { // 정보관리 > 자산기준정보 관리 
			list = assetService.getAssetBaseInfoExcel(param); // 바꿔야 함
			mav.addObject("list", list);
			mav.addObject("filename", "010005_template.xlsx");
			mav.addObject("refilename", "자산기준정보.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 정보관리 > 자산기준정보 ] - 자산기준정보.xlsx";

		} else if ("011507".equals(jobType)) { // 정보관리 > 자산기준정보 관리 

			String Param = request.getParameter("PARAM").toString();
			JSONObject jo  = new JSONObject();
			jo = JSONObject.fromObject(Param);
			System.out.println(">>>>>>>>>>>>>>"+jo);
			HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);
			
	        JSONArray assetList = (JSONArray) map.get("list");
	        System.out.println( map.get("list"));
	        for (int i=0; i< assetList.size(); i++) {
	        	HashMap<String, Object> param2 = new HashMap<String, Object>();
	        	JSONObject jo1 =  JSONObject.fromObject(assetList.get(i).toString());
	        	param2 = CommonUtil.convertJsonToMap(jo1);
	        	list.add(param2);
	            //System.out.println(param2);
	        }			
			mav.addObject("list", list);
			mav.addObject("filename", "011507_template.xlsx");
			mav.addObject("refilename", "자산비용_양식.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 운영관리 > 자산비용관리 ] - 자산비용_양식.xlsx";

			
		} else if ("codeManager".equals(jobType)) { // 정보관리 > 자산기준정보 관리 
			list = commonService.getCommonCodeList(param); // 바꿔야 함
			mav.addObject("list", list);
			mav.addObject("filename", "comcode_template.xlsx");
			mav.addObject("refilename", "공통코드목록.xlsx");
			mav.addObject("addComboList", null);

			lg_desc = "[메뉴 : 관리자 > 코드관리 ] - 공통코드.xlsx";

			
		} else {
			lg_desc = "엑셀 다운로드  에러가 발생하였습니다.[jobType null ] ";
			throw new ApplicationException("엑셀 다운로드  에러가 발생하였습니다.[jobType null ] : " + param);
		}
		
		//접속,출력로그
		
		
		String ip_addr = request.getRemoteAddr();
		
		param.put("login_user_id", user_id);
		param.put("lg_opt", "2");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기
		param.put("lg_desc", lg_desc);// 화면의 코드를 넘긴다. 예) 유량조사 0502
		param.put("E_ID", user_id );
		param.put("ip_addr", ip_addr );		  
		commonService.insertSystemLog(param);
		
		return mav;
	}	
	/*
	 * 콤보 셋팅해주기
	 */
   private ArrayList<HashMap<String, Object>> comboList(String type, ArrayList<HashMap<String, Object>> list ) {
       
	   ArrayList<HashMap<String, Object>> addComboList = new ArrayList<HashMap<String, Object>>();
	   
	   try {
		   
		   if("010503".equals(type)) {//수질조사
			   
				int firstRow = 3;
				int lastRow = firstRow;  
				if(list != null && list.size() > 0) {
					lastRow = firstRow + (list.size()-1);
				}
				
				HashMap<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("gcode", "621");
				ArrayList<HashMap<String, Object>> comboList = commonService.getCommonCodeList(paramMap);
				
				String[] aCategoryValues = new String[comboList.size()];
				
				for(int i=0; i<comboList.size(); i++)
	    		{	
					HashMap<String, Object> comboMap = (HashMap<String, Object>)comboList.get(i);
	    			
					String EXCEL_TEXT = (String)comboMap.get("EXCEL_TEXT");
					aCategoryValues[i] = EXCEL_TEXT; 
					
	    		}
				
				HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
				addComboMap.put("firstRow", firstRow+"");
				addComboMap.put("lastRow", lastRow+"");
				addComboMap.put("firstCol", "20");
				addComboMap.put("lastCol", "20");
				addComboMap.put("comboData", aCategoryValues);
				addComboList.add(addComboMap);  
				
		   }else if("010504".equals(type)) {
			   
				int firstRow = 4;
				int lastRow = firstRow;  
				if(list != null && list.size() > 0) {
					lastRow = firstRow + (list.size()-1);
				}
				
				//M2_COMMONCODE 에서 가져오기
				String[][] cmCode = {{"601","5","5"},{"602","10","10"},{"603","18","18"}};//관구분,배수방식,맨홀뚜껑		   
			    HashMap<String, Object> paramMap = new HashMap<String, Object>();
				for(int i=0; i<cmCode.length; i++) {
					paramMap.put("gcode", cmCode[i][0]);
					
					ArrayList<HashMap<String, Object>> comboList = commonService.getCommonCodeList(paramMap);
					String[] aCategoryValues = new String[comboList.size()];
					for(int y=0; y<comboList.size(); y++)
		    		{	
						HashMap<String, Object> comboMap = (HashMap<String, Object>)comboList.get(y);
		    			
						String EXCEL_TEXT = (String)comboMap.get("EXCEL_TEXT");
						aCategoryValues[y] = EXCEL_TEXT; 
						
		    		}
					HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
					addComboMap.put("firstRow", firstRow+"");
					addComboMap.put("lastRow", lastRow+"");
					addComboMap.put("firstCol", cmCode[i][1]);
					addComboMap.put("lastCol", cmCode[i][2]);
					addComboMap.put("comboData", aCategoryValues);
					addComboList.add(addComboMap);
				}
				
				//CMT_CODE_MA 에서 가져오기
				String[][] cmtCode = {{"M2_JOB_EYE","JE_PKIND", "6","6"},{"M2_JOB_EYE","JE_HMKIND", "15","15"},
						{"M2_JOB_EYE","JE_HMQM", "16","16"},{"M2_JOB_EYE","JE_HMLAD", "20","20"}
				};//관종,	맨홀종류,맨홀재질,사다리모양
			    HashMap<String, Object> cmtParamMap = new HashMap<String, Object>();
				for(int i=0; i<cmtCode.length; i++) {
					cmtParamMap.put("tbl_nam", cmtCode[i][0]);
					cmtParamMap.put("att_nam", cmtCode[i][1]);
					
					ArrayList<HashMap<String, Object>> comboList = commonService.getFacCodeList(cmtParamMap);
					String[] aCategoryValues = new String[comboList.size()];
					for(int y=0; y<comboList.size(); y++)
		    		{	
						HashMap<String, Object> comboMap = (HashMap<String, Object>)comboList.get(y);
		    			
						String EXCEL_TEXT = (String)comboMap.get("EXCEL_TEXT");
						aCategoryValues[y] = EXCEL_TEXT; 
						
		    		}
					HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
					addComboMap.put("firstRow", firstRow+"");
					addComboMap.put("lastRow", lastRow+"");
					addComboMap.put("firstCol", cmtCode[i][2]);
					addComboMap.put("lastCol", cmtCode[i][3]);
					addComboMap.put("comboData", aCategoryValues);
					addComboList.add(addComboMap);
				}	
				
				//코드에 없는값
				//맨홀구분
				HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
				addComboMap.put("firstRow", firstRow+"");
				addComboMap.put("lastRow", lastRow+"");
				addComboMap.put("firstCol", 12);
				addComboMap.put("lastCol", 12);
				addComboMap.put("comboData", new String[]{"상류맨홀(H)","하류맨홀(L)"});
				System.out.println("1111111==" + addComboMap.toString());
				addComboList.add(addComboMap);
				addComboList.get(addComboList.size()-1);
				
				
				//조사방향
				HashMap<String, Object> addComboMap2 = new HashMap<String, Object>();	
				addComboMap2.put("firstRow", firstRow+"");
				addComboMap2.put("lastRow", lastRow+"");
				addComboMap2.put("firstCol", 23);
				addComboMap2.put("lastCol", 23);
				addComboMap2.put("comboData", new String[]{"상류->하류(HL)","하류->상류(LH)"});
				System.out.println("222222222222==" + addComboMap2.toString());
				addComboList.add(addComboMap2);				
				
				//구조적, 운영적				
				String[] colNum = new String[]{"27","28","29","30","31","32"};
					
				for(int i=0; i<colNum.length; i++ ) {
					HashMap<String, Object> addComboMap3 = new HashMap<String, Object>();
					addComboMap3.put("firstRow", firstRow+"");
					addComboMap3.put("lastRow", lastRow+"");
					addComboMap3.put("firstCol", colNum[i]);
					addComboMap3.put("lastCol", colNum[i]);
					addComboMap3.put("comboData", new String[]{"1등급","2등급","3등급","4등급","5등급"});
					System.out.println("3333333333==" + addComboMap3.toString());
					addComboList.add(addComboMap3);		
				}
				
				//조치구분
				HashMap<String, Object> addComboMap4 = new HashMap<String, Object>();	
				addComboMap4.put("firstRow", firstRow+"");
				addComboMap4.put("lastRow", lastRow+"");
				addComboMap4.put("firstCol", 33);
				addComboMap4.put("lastCol", 33);
				addComboMap4.put("comboData", new String[]{"이상없음(1)","청소/준설(2)","개보수(3)","기타(9)"});
				System.out.println("44444444444==" + addComboMap4.toString());
				addComboList.add(addComboMap4);		
				
			   
		   }else if("010506".equals(type)) {
			   
				int firstRow = 3;
				int lastRow = firstRow;  
				if(list != null && list.size() > 0) {
					lastRow = firstRow + (list.size()-1);
				}
				
				//체크박스
				for(int i=4; i<18; i++) {
					if(i!=10) {
						HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
						addComboMap.put("firstRow", firstRow+"");
						addComboMap.put("lastRow", lastRow+"");
						addComboMap.put("firstCol", i+"");
						addComboMap.put("lastCol", i+"");
						addComboMap.put("comboData", new String[]{"이상있음(1)","이상없음(0)"});
						addComboList.add(addComboMap);	
					}
				}
				//조치구분
				HashMap<String, Object> addComboMap4 = new HashMap<String, Object>();	
				addComboMap4.put("firstRow", firstRow+"");
				addComboMap4.put("lastRow", lastRow+"");
				addComboMap4.put("firstCol", 20);
				addComboMap4.put("lastCol", 20);
				addComboMap4.put("comboData", new String[]{"이상없음(1)","청소/준설(2)","개보수(3)","기타(9)"});
				addComboList.add(addComboMap4);	
				
		   }else if("010603".equals(type)) {//개보수
			   
			   int firstRow = 2; 
				int lastRow = firstRow;  
				if(list != null && list.size() > 0) {
					lastRow = firstRow + (list.size()-1);
				}
				
				//M2_COMMONCODE 에서 가져오기
				String[][] cmCode = {{"606","4","4"},{"607","5","5"}};//개보수공법, 공법종류		   
			    HashMap<String, Object> paramMap = new HashMap<String, Object>();
				for(int i=0; i<cmCode.length; i++) {
					paramMap.put("gcode", cmCode[i][0]);
					
					ArrayList<HashMap<String, Object>> comboList = commonService.getCommonCodeList(paramMap);
					String[] aCategoryValues = new String[comboList.size()];
					for(int y=0; y<comboList.size(); y++)
		    		{	
						HashMap<String, Object> comboMap = (HashMap<String, Object>)comboList.get(y);
		    			
						String EXCEL_TEXT = (String)comboMap.get("EXCEL_TEXT");
						aCategoryValues[y] = EXCEL_TEXT; 
						
		    		}
					HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
					addComboMap.put("firstRow", firstRow+"");
					addComboMap.put("lastRow", lastRow+"");
					addComboMap.put("firstCol", cmCode[i][1]);
					addComboMap.put("lastCol", cmCode[i][2]);
					addComboMap.put("comboData", aCategoryValues);
					addComboList.add(addComboMap);
				}
		   }
			
        } catch (Exception e) {
        	e.getMessage();
        	e.printStackTrace();
        }
	   return addComboList;
    }	

	
	
	private boolean findFtrIdn_Group(ArrayList<HashMap<String, Object>> orgList, String chk_jg_gnum, String excelFtrIdn) {
		 
		boolean isUse = false;
		
		double D_excelFtrIdn = Double.parseDouble(excelFtrIdn);
		excelFtrIdn = String.valueOf((Math.round(D_excelFtrIdn)));
		excelFtrIdn = chk_jg_gnum + "-" + excelFtrIdn;
		
		if(orgList != null && orgList.size()>0) {
			for(int i=0; i<orgList.size(); i++)
			{	
				HashMap<String, Object> orgMap = (HashMap<String, Object>)orgList.get(i);
				
				String FTR_IDN = (String)orgMap.get("GNUM_IDN_CHK");
				//System.out.println("step FTR_IDN: " +FTR_IDN+"/"+excelFtrIdn);
				if(excelFtrIdn.equals(FTR_IDN)) {
					isUse = true;	
				}
				
			}		
		}
		return isUse;
	 }
	
   private ArrayList<HashMap<String, Object>> columnNameChange(ArrayList<HashMap<String, Object>> excelList, String[] columnNames, int targetCellCnt, String login_user_id, String columnNamesToCode ) throws Exception {
        
	   ArrayList<HashMap<String, Object>> reExcelList = new ArrayList<HashMap<String, Object>>();
	   
	   try {
		   System.out.print("CexcelList.size();     " + excelList.size());
        	for(int i=0; i<excelList.size(); i++)
    		{	
				HashMap<String, Object> excelListMap = (HashMap<String, Object>)excelList.get(i);
				HashMap<String, Object> reExcelListMap = (HashMap<String, Object>)excelList.get(i);
			
				
				
				for(int y=0; y<targetCellCnt; y++)
	    		{	
	       			String value = String.valueOf(excelListMap.get("CELL_" + y));
	       			value = value.trim();
//	       		 System.out.print("CELL_ + y          " + y + "  | " + value +"\n ");
	       			
//	       			System.out.print(columnNamesToCode.indexOf(columnNames[y]) + ", columnNames[y]      =    " + columnNames[y] + " , columnNamesToCode =" + columnNamesToCode +"\n , value = " + value + "\n");
	       			
	       			//if(columnNames[y].equals(columnNamesToCode)) {//코드로 컬럼이면 () 안에 값을 가져온다. 울진하수처리시설(11)
	       			if(columnNamesToCode.indexOf(columnNames[y]) > -1) {
	       				
		       			 if(!"".equals(value) && !"@@@".equals(value)) {
		       				 int sPoint = value.indexOf("(");
		       				 int ePoint = value.indexOf(")");
		       				 
//		       				 System.out.println("sPoint=" + sPoint);
//		       				 System.out.println("ePoint=" + ePoint);
//		       				System.out.println("value=[" + value+"]");
//		       				 System.out.println("target=" + value.substring((sPoint+1),ePoint));
		       				reExcelListMap.put(columnNames[y], value.substring((sPoint+1),ePoint));		 
		       			 }else {
		       				reExcelListMap.put(columnNames[y], "");
		       			 }
	       			
	       				
	       			}else {
	       				reExcelListMap.put(columnNames[y], value);	
	       			}
	       			
	       			
	    		}
//				 System.out.print("\n                   ");
//                 System.out.print("\n                   ");
//                 System.out.print("\n                   ");
//                 System.out.print("\n                   ");
				reExcelListMap.put("login_user_id",login_user_id);
				
				
				System.out.print("reExcelListMap " + reExcelListMap +"\n ");
				
				if(!"".equals(CommonUtil.replaceNullString((String)reExcelListMap.get("cnt_num")))) {
					reExcelList.add(reExcelListMap);	
				}
				
    		}
        	
        	
        } catch (Exception e) {
        	e.getMessage();
        	e.printStackTrace();
        }
	   return reExcelList;
    }	
   

	
}











































