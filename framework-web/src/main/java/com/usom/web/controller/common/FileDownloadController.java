package com.usom.web.controller.common;

import java.util.HashMap;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.framework.model.service.view.CommonDownLoadFile;
import com.framework.model.util.CommonUtil;



@Controller
@RequestMapping("/file")
public class FileDownloadController {

	@Inject
	private CommonDownLoadFile commonDownLoadFileView;
	
	
	@RequestMapping(value = "/file_download", method = RequestMethod.POST)
	public ModelAndView selectCompanyListPage(HttpServletRequest request)throws Exception{
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.getParameterMap(request);
		//param = CommonUtil.reqDataParsingToMap(request);
		
		return new ModelAndView(commonDownLoadFileView, "f_num", param.get("f_num"));
	}
}
