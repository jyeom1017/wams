package com.usom.web.controller.basic;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.DataMap;
import com.framework.model.util.PasswordUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.bbs.BbsService;
import com.usom.model.service.basic.EmployeeService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.json.JSONObject;

import com.usom.model.service.common.CommonService;

// * 기초정보관리 > 인력관리

@Controller
@RequestMapping("/basic/employee")
public class EmployeeController {
	
	@Resource(name = "employeeService")
	private EmployeeService employeeService;
	
    @Resource(name = "commonService")
    private CommonService commonService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
	/*
	 * @RequestMapping(value = "/main") public ModelAndView
	 * main(@RequestParam(required=false) String viewmode) throws Exception {
	 * 
	 * ModelAndView mav = new ModelAndView(); mav.addObject("viewmode", viewmode);
	 * mav.setViewName("omas/basic/employeeManager.jsp");
	 * 
	 * return mav; }
	 */
	
	// * 기존비밀번호 확인
	
	@RequestMapping(value = "/checkEqualUserPassword.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody boolean checkEqualUserPassword(@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		HashMap<String, Object> empInfo = employeeService.getEmployee(param);
		//String e_pw = empInfo.get("E_PW").toString();
		String e_pw = empInfo.get("E_PW_ENC").toString();
		System.out.println(e_pw);
		System.out.println(param.get("chkPassword").toString());
		//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		//		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		//		String e_pw = user.getE_PW();
		boolean chk = PasswordUtil.BCryptPasswordMatch(param.get("chkPassword").toString(), e_pw);
		//boolean chk = PasswordUtil.BCryptPasswordMatch(PasswordUtil.BCryptEncoder(param.get("chkPassword").toString()), e_pw);
		return chk;
	}
	
	// * 비밀번호를 변경시킨다.
	
	@RequestMapping(value = "/updateUserPassword.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateUserPassword(@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();		
		HashMap<String, Object> empInfo = employeeService.getEmployee(param);
		String e_id = empInfo.get("E_ID").toString();
		//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		//		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		param.put("user_id", e_id);
		param.put("password", PasswordUtil.BCryptEncoder(param.get("E_PW_01").toString()));
		
		int result =  employeeService.updateUserPassword(param);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			param2.put("lg_opt", "p");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기 , 8:불용처리 , c:사용자생성,m:사용자수정,d:사용자삭제,v:사용자조회,p:패스워드변경
			param2.put("lg_desc", "패스워드변경:" + param.get("E_ID") );
			param2.put("login_user_id", user.getE_ID().toString());
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}
		return result;
	}
	
	// 로그인사용자의 기존비밀번호 확인
	
	@RequestMapping(value = "/checkEqualLoginUserPassword.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody boolean checkEqualLoginUserPassword(@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		String e_pw = user.getE_PW();
		boolean chk = PasswordUtil.BCryptPasswordMatch(PasswordUtil.BCryptEncoder(param.get("chkPassword").toString()), e_pw);
		return chk;
	}
	
	// * 로그인사용자의 비밀번호를 변경시킨다.
	
	@RequestMapping(value = "/updateLoginUserPassword.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateLoginUserPassword(HttpServletRequest req,@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		param.put("user_id", user.getE_ID());
		param.put("password", PasswordUtil.BCryptEncoder(param.get("E_PW_01").toString()));
		
		int result = employeeService.updateUserPassword(param);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			String user_id =  user.getE_ID();
			String ip_addr = req.getRemoteAddr();		
			param2.put("E_ID", user_id );
			param2.put("ip_addr", ip_addr );
			
			param2.put("lg_opt", "p");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기 , 8:불용처리 , c:사용자생성,m:사용자수정,d:사용자삭제,v:사용자조회,p:패스워드변경
			param2.put("lg_desc", "패스워드변경:" + param.get("E_ID") );
			param2.put("login_user_id", user.getE_ID().toString());
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}		
		return result;
	}
	
	// * 인력목록
	
	@RequestMapping(value = "/getEmployeeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getEmployeeList(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = employeeService.getEmployeeList(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	}
	
	
	@RequestMapping(value = "/getEmployeeList1.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getEmployeeList(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = CommonUtil.reqDataParsingToMap(req);
		
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = employeeService.getEmployeeList(param);
		
		if (param.get("page") == null)
			param.put("page", 1);
		if (param.get("rows") == null)
			param.put("rows", 10);
		
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */
		
		return rtnMap;
	} 
	
	//인력목록 - 기본정보 조회
	@RequestMapping(value = "/getEmployeeInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getEmployeeInfo(@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		
		return employeeService.getEmployee(param);
	}
	
	//인력조회 이력
	@RequestMapping(value = "/updateLogUser.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateLogUser(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			String user_id =  user.getE_ID();
			String ip_addr = req.getRemoteAddr();		
			param2.put("E_ID", user_id );
			param2.put("ip_addr", ip_addr );
			
			param2.put("lg_opt", "v");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기 , 8:불용처리 , c:사용자생성,m:사용자수정,d:사용자삭제,v:사용자조회,p:패스워드변경
			param2.put("lg_desc", "사용자조회:" + param.get("e_id") );
			param2.put("login_user_id", user.getE_ID().toString());
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}
		return 1;
	}
	
	//sso 로그인게정생성 
	/* /basic/employee/CreateUser */
		@RequestMapping(value = "/CreateUser", method = RequestMethod.POST, headers = "Accept=application/json")
		public @ResponseBody int CreateUser(HttpServletRequest req
		//@RequestParam(required=true) HashMap<String, Object> param
		) throws Exception {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
			
			HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
			param.put("E_ID", req.getSession().getAttribute("user_id"));
			int result = employeeService.createSSOUser(param);
	    	//접속로그
			if(result ==1 ) {
			HashMap<String, Object> param2 = new HashMap<String, Object>();
			try {
				String user_id =  user.getE_ID();
				String ip_addr = req.getRemoteAddr();		
				param2.put("E_ID", user_id );
				param2.put("ip_addr", ip_addr );
				
				param2.put("lg_opt", "c");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기 , 8:불용처리 , c:사용자생성,m:사용자수정,d:사용자삭제,v:사용자조회,p:패스워드변경
				param2.put("lg_desc", "사용자생성:" + param.get("E_ID") );
				param2.put("login_user_id", "system");
				 
				commonService.insertSystemLog(param2);
				
			}catch(Exception e) {
				//System.out.print(" db 에러 param ==>" + param.toString());
			}
			}
			return result;
		}
		
	//인력목록 - 추가
	@RequestMapping(value = "/insertEmployee.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertEmployee(HttpServletRequest req
	//@RequestParam(required=true) HashMap<String, Object> param
	) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();		
		JSONObject parseJson = new JSONObject();
		
		parseJson = CommonUtil.reqDataParsing(req);
		
		//System.out.print(parseJson);
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		Iterator<String> keys = parseJson.keys();
		
		while (keys.hasNext()) {
			String key = keys.next();
			
			if (parseJson.get(key) == null)
				param.put(key, "");
			else {
				if(key.equals("E_PW")) {
					param.put("E_PW_ENC", PasswordUtil.BCryptEncoder(parseJson.get("E_PW").toString()));
				}else {
					param.put(key, parseJson.get(key).toString());
				}
			}
			
		}
		//System.out.print("param:");
		//System.out.print(param);
		
		int result = employeeService.insertEmployee(param);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			
			String user_id =  user.getE_ID();
			String ip_addr = req.getRemoteAddr();		
			param2.put("E_ID", user_id );
			param2.put("ip_addr", ip_addr );
		  
					
			param2.put("lg_opt", "c");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기 , 8:불용처리 , c:사용자생성,m:사용자수정,d:사용자삭제,v:사용자조회,p:패스워드변경
			param2.put("lg_desc", "사용자생성:" + param.get("E_ID") );
			param2.put("login_user_id", user.getE_ID().toString());
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}
		return result;
	}
	
	//인력목록 - 삭제
	@RequestMapping(value = "/deleteEmployee.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteEmployee(HttpServletRequest req ) throws Exception {
		HashMap<String,Object> param = CommonUtil.reqDataParsingToHashMap(req);
		System.out.print("param ==>" + param.toString());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();		
		int result= employeeService.deleteEmployee(param);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			String user_id =  user.getE_ID();
			String ip_addr = req.getRemoteAddr();
			param2.put("E_ID", user_id );
			param2.put("ip_addr", ip_addr );

			
			param2.put("lg_opt", "d");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기 , 8:불용처리 , c:사용자생성,m:사용자수정,d:사용자삭제,v:사용자조회,p:패스워드변경
			param2.put("lg_desc", "사용자삭제:" + param.get("E_ID") );
			param2.put("login_user_id", user.getE_ID().toString());
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}
		return result;
	}
	
	//인력 - 기본정보 업데이트
	@RequestMapping(value = "/updateEmployeeBasicInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateEmployeeBasicInfo(HttpServletRequest req
	//@RequestParam(required=true) HashMap<String, Object> param
	) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		//패스워드 인코딩
		/*
		 if(param.get("CHANGE_E_PW") != null && ! param.get("CHANGE_E_PW").equals(""))
		 { param.put("E_PW",
		  PasswordUtil.BCryptEncoder(param.get("CHANGE_E_PW").toString())); 
		 }*/
		 
		int result = employeeService.updateEmployeeBasicInfo(param);
    	//접속로그
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			
			String user_id =  user.getE_ID();
			String ip_addr = req.getRemoteAddr();		
			param2.put("E_ID", user_id );
			param2.put("ip_addr", ip_addr );
			
			param2.put("lg_opt", "m");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기 , 8:불용처리 , c:사용자생성,m:사용자수정,d:사용자삭제,v:사용자조회,p:패스워드변경
			param2.put("lg_desc", "사용자수정:" + param.get("e_id") );
			param2.put("login_user_id", user.getE_ID().toString());
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}
		return result;		
	}

	@RequestMapping(value = "/deleteEmployeePhoto.json", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public int deleteEmployeePhoto(HttpServletRequest req) throws Exception {
		return employeeService.deleteEmployeePhoto(CommonUtil.reqDataParsingToMap(req));
	}
	
	@RequestMapping(value = "/updateEmpLastMenu.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateEmpLastMenu(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return employeeService.updateEmpLastMenu(param);
	}
	
	//인력목록 - 기본정보 조회
	@RequestMapping(value = "/getEmployeeClass.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getEmployeeClass(@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		
		return employeeService.getEmployeeClass(param);
	}
		
	/*
	 * 
	 * //인력목록 - 근무경력정보 - 조회
	 * 
	 * @RequestMapping(value = "/getEmployeeCareerInfoList.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * ArrayList<HashMap<String, Object>>
	 * getEmployeeCareerInfoList(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.getEmployeeCareerInfoList(param); } //인력목록 - 근무경력정보 - 추가
	 * 
	 * @RequestMapping(value = "/insertEmployeeCareerInfo.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int insertEmployeeCareerInfo(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.insertEmployeeCareerInfo(param); } //인력목록 - 근무경력정보 - 수정
	 * 
	 * @RequestMapping(value = "/updateEmployeeCareerInfo.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int updateEmployeeCareerInfo(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.updateEmployeeCareerInfo(param); } //인력목록 - 근무경력정보 - 삭제
	 * 
	 * @RequestMapping(value = "/deleteEmployeeCareerInfo.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int deleteEmployeeCareerInfo(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.deleteEmployeeCareerInfo(param); }
	 * 
	 * //인력목록 - 기술자격 - 조회
	 * 
	 * @RequestMapping(value = "/getEmployeeTechInfoList.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * ArrayList<HashMap<String, Object>>
	 * getEmployeeTechInfoList(@RequestParam(required=true) HashMap<String, Object>
	 * param) throws Exception { return
	 * employeeService.getEmployeeTechInfoList(param); } //인력목록 - 기술자격 - 추가
	 * 
	 * @RequestMapping(value = "/insertEmployeeTechInfo.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int insertEmployeeTechInfo(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.insertEmployeeTechInfo(param); } //인력목록 - 기술자격 - 수정
	 * 
	 * @RequestMapping(value = "/updateEmployeeTechInfo.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int updateEmployeeTechInfo(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.updateEmployeeTechInfo(param); } //인력목록 - 기술자격 - 삭제
	 * 
	 * @RequestMapping(value = "/deleteEmployeeTechInfo.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int deleteEmployeeTechInfo(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.deleteEmployeeTechInfo(param); }
	 * 
	 * //인력목록 - 교육/훈련 - 조회
	 * 
	 * @RequestMapping(value = "/getEmployeeEduInfoList.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * ArrayList<HashMap<String, Object>>
	 * getEmployeeEduInfoList(@RequestParam(required=true) HashMap<String, Object>
	 * param) throws Exception { return
	 * employeeService.getEmployeeEduInfoList(param); } //인력목록 - 교육/훈련 - 추가
	 * 
	 * @RequestMapping(value = "/insertEmployeeEduInfo.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int insertEmployeeEduInfo(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.insertEmployeeEduInfo(param); } //인력목록 - 교육/훈련 - 수정
	 * 
	 * @RequestMapping(value = "/updateEmployeeEduInfo.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int updateEmployeeEduInfo(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.updateEmployeeEduInfo(param); } //인력목록 - 교육/훈련 - 삭제
	 * 
	 * @RequestMapping(value = "/deleteEmployeeEduInfo.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int deleteEmployeeEduInfo(@RequestParam(required=true) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.deleteEmployeeEduInfo(param); }
	 * 
	 * //[공통] 직원선택 팝업
	 * 
	 * @RequestMapping(value = "/getEmployeeSelectionList.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * ArrayList<HashMap<String, Object>>
	 * getEmployeeSelectionList(@RequestParam(required=false) HashMap<String,
	 * Object> param) throws Exception { return
	 * employeeService.getEmployeeSelectionList(param); }
	 * 
	 * //[공통] 대상(참여)인력 정보 가져오기
	 * 
	 * @RequestMapping(value = "/getUseEmployeeList.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * ArrayList<HashMap<String, Object>>
	 * getUseEmployeeList(@RequestParam(required=true) HashMap<String, Object>
	 * param) throws Exception { return employeeService.getUseEmployeeList(param); }
	 * //[공통] 대상(참여)인력 정보 입력
	 * 
	 * @RequestMapping(value = "/insertUseEmployee.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int insertUseEmployee(@RequestParam(required=true) HashMap<String, Object>
	 * param) throws Exception { return employeeService.insertUseEmployee(param); }
	 * //[공통] 대상(참여)인력 정보 삭제
	 * 
	 * @RequestMapping(value = "/deleteUseEmployees.json", method =
	 * RequestMethod.POST, headers="Accept=application/json") public @ResponseBody
	 * int deleteUseEmployees(@RequestParam(required=true) HashMap<String, Object>
	 * param) throws Exception { return employeeService.deleteUseEmployees(param); }
	 */
	
	/*
	 * private JSONObject reqDataParsing(HttpServletRequest req) throws Exception{
	 * 
	 * JSONObject jsonObject = new JSONObject(); StringBuffer jb = new
	 * StringBuffer(); String line = null;
	 * 
	 * try { BufferedReader reader = req.getReader(); while ((line =
	 * reader.readLine()) != null) jb.append(line); } catch (Exception e) {
	 * jsonObject.put("parseCode", "01"); return jsonObject; }
	 * 
	 * try { jsonObject = JSONObject.fromObject(jb.toString());
	 * //restlogger.info(" >>> request jsonObject ===>"+jsonObject.toString());
	 * 
	 * }catch (Exception e) { jsonObject.put("parseCode", "02"); return jsonObject;
	 * }
	 * 
	 * jsonObject.put("parseCode", "00");
	 * 
	 * return jsonObject; }
	 */
	
}
