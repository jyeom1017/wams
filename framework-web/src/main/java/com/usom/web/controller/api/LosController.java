package com.usom.web.controller.api;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.diagnosis.LosService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

@Controller
@RequestMapping("/los")
public class LosController {
	@Resource(name="losService")
	private LosService losService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
    @RequestMapping(value = "/getList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getList(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = losService.selectLos(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		//paging start 
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		//paging end 
		
		return rtnMap;
	}
    @RequestMapping(value = "/getItemList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getItemList(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
     	
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
     	HashMap<String, Object> map = losService.getEvalueItem(param);
     	map.put("INSERT_ID", user.getE_ID());
     	map.put("INSERT_DT", LocalDate.now().toString());
    	return  map;
    }
    
    @RequestMapping(value = "/save.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int save(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  losService.insertEvalueItem(param);
    }
    
    @RequestMapping(value = "/getWeightList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getWeightList(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  losService.getWeightList(param);
    }
    
    @RequestMapping(value = "/insertWeight.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertWeight(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  losService.insertWeight(param);
    }
    
    @RequestMapping(value = "/getWeight.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getWeight(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  losService.selectWeight(param);
    }
    
    @RequestMapping(value = "/Analyze.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insert(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  losService.insertEvalue(param);
    }
    @RequestMapping(value = "/getEvalue.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getEvalue(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  losService.selectEvalue(param);
    }
    
    @RequestMapping(value = "/deleteLos.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteLos(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  losService.deleteLos(param);
    }
	   /*getList
	   insert
	   update
	   delete
	   report
	   insertAsset
	   deleteAsset
	   
	   getPriceIndex
	   insertPriceIndex
	   updatePriceIndex
	   deletePriceIndex
	   reportPriceIndex
	   
	   getWeightSpec
	   insertWeightSpec
	   updateWeightSpec
	   deleteWeightSpec*/

}
