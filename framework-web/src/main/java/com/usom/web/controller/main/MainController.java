package com.usom.web.controller.main;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.codec.binary.Base64;
import org.jxls.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.framework.model.service.view.back_ExcelReportView;
import com.framework.model.service.view.PdfView;
import com.framework.model.util.CommonUtil;
import com.framework.model.util.FileUpload;
import com.usom.model.config.Configuration;
import com.usom.model.service.test.TestService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MainController {
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@Resource(name="testService")
	private TestService testService;
	
	@Inject
	private back_ExcelReportView excelReportView; 
	@Inject
	private PdfView pdfView;
	
	@Resource(name="fileUpload")
	private FileUpload fileUpload;
	
/*	@RequestMapping(value = "/")
	public ModelAndView main(@RequestParam(required=false) String sitecode) throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("view/main");
		
		return mav;
	}
*/
	
	//jqgrid - test paging list
	@RequestMapping(value = "/getJqgridTestList.json", method = RequestMethod.POST, headers="Accept=application/json")
	public @ResponseBody HashMap<String, Object> getJqgridTestList(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만
		if(param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"%|$/()]");
		}
		/*param.get("rules");
		param.get("sidx");
		param.get("sord");*/
		
		int currPage = Integer.parseInt(param.get("page").toString());
		int rowNum = Integer.parseInt(param.get("rows").toString());
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("rows", testService.getJqgridTestList(param));
		map.put("page", currPage);//현재페이지
		int totalCount = testService.getJqgridTestTotalRowCount(param);
		map.put("total", (int) Math.ceil((double) totalCount / rowNum));//총페이지수
		map.put("records", totalCount);//총글갯수
		
		return map;
	}

	
	
	@RequestMapping(value = "/getExcelTest", method = RequestMethod.GET)
	public ModelAndView getExcelTest(HttpServletRequest request) throws Exception {
		
		ModelAndView mav = new ModelAndView(excelReportView);
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("page", 1);
		param.put("rows", 10);
		ArrayList<HashMap<String, Object>> list = testService.getJqgridTestList(param);
		mav.addObject("dataList", list);
		
		//이미지업로드
		String path = "C:/workspace/OMAS/framework/framework-web/src/main/webapp/resources/report";
		param = new HashMap<String, Object>();
		InputStream imageInputStream = 
				new FileInputStream(path + "/pipe_sample01.jpg");
		byte[] imageBytes = Util.toByteArray(imageInputStream);
		param.put("image01", imageBytes);
		param.put("image01_desc", "1번사진대지");
		
		InputStream imageInputStream2 = 
				new FileInputStream(path + "/pipe_sample02.jpg");
		byte[] imageBytes2 = Util.toByteArray(imageInputStream2);
		param.put("image02", imageBytes2);
		param.put("image02_desc", "2번사진대지");
		
		mav.addObject("images", param);
		
		mav.addObject("filename", "보고서테스트.xls");
		
		return mav;
	}
	
	@RequestMapping(value = "/commonImageUploadTest.json", method = RequestMethod.POST, headers="Accept=application/json")
	public @ResponseBody HashMap<String, String> commonImageUploadTest(
			HttpServletRequest request, @RequestParam HashMap<String, Object> param) throws Exception {
		
		HashMap<String, String> responseFileInfo = new HashMap<String, String>();
		
		String path = Configuration.UPLOAD_FILE_PATH + "sewerImage";
		String urlPath = Configuration.UPLOAD_FILE_URL + "sewerImage";
		
		for(int i=0; i<param.size(); i++) {
			String str = param.get("PIC_"+i).toString();
			if(str != null && ! str.equals("")) {
				byte[] imagedata = Base64.decodeBase64(str.substring(str.indexOf(",") + 1));
				//imagedata = null;
				BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imagedata));
				
				String imgName = CommonUtil.createUniqueIDFromDate();
				ImageIO.write(bufferedImage, "png", new File(path + "/img_"+imgName+".png"));
				
				responseFileInfo.put("PIC_PATH_"+i, urlPath + "/img_"+imgName+".png");
			}else {
				responseFileInfo.put("PIC_PATH_"+i, "");
			}
		}
		return responseFileInfo;
	}
	
	@RequestMapping(value = "/insertTest.json", method = RequestMethod.POST, headers="Accept=application/json")
	public @ResponseBody int insertTest(@RequestParam(required=true) HashMap<String, Object> param) throws Exception {
		
		System.out.println("param");
		
		return 1;
	}
	/**
	* <PRE>
	* 설명 : 공통파일업로더
	* 상세설명 : 파일을 해당폴더에 업로드후 file테이블에 insert
	* <PRE>
	 */
	@RequestMapping(value = "/commonFileUpload.json", method = RequestMethod.POST, headers="Accept=application/json")
	public @ResponseBody HashMap<String, String> commonFileUpload(MultipartHttpServletRequest request) throws Exception {
		// file Check
		String[] arrFileType = {"PDF", "ZIP", "XLS", "XLSX", "HWP", "PPT", "PPTX", "DOC", "DOCX"};
		String uploadDir = Configuration.UPLOAD_FILE_PATH + "file";
		String urlPath = Configuration.UPLOAD_FILE_URL + "file";
		HashMap<String, String> responseFileInfo = new HashMap<String, String>();
		//기본설정 세팅
		fileUpload.setFileUpload(arrFileType, 20, null, uploadDir);
		if(fileUpload.validationFile(request)) {//file validation 체크
			Iterator<String> iter = request.getFileNames();
			while(iter.hasNext()) {
				
				String uploadFileName = iter.next();
				CommonsMultipartFile fileData = (CommonsMultipartFile) request.getFile(uploadFileName);
				String fileName = "";
				String filePath = "";
				if(fileData != null && ! fileData.isEmpty()) {
					fileName = fileUpload.create(fileData);
					filePath = urlPath + "/" + fileName;
					responseFileInfo.put(uploadFileName, filePath);
				}else {
					responseFileInfo.put(uploadFileName, "");
				}	
			}
		}
		return responseFileInfo;
	}


	
	
	
}
