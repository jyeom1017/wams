package com.usom.web.controller.api;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.common.CommonService;
import com.usom.model.service.finance.FinanceAnalysisService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/finance")
public class FinanceAnalysisController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private FinanceAnalysisService financeAnalysisService;

    @Autowired
    private Pagging pagging;

    // 재정계획 수립 > 재정계획 목록
    @RequestMapping(value = "/getFinancialPlanList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getFinancialPlanList(@RequestParam HashMap<String, Object> param) throws Exception {
        // SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만
        if (param.get("rules") != null) {
            CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
        }

        HashMap<String, Object> rtnMap = new HashMap<String,Object>();
        ArrayList<HashMap<String, Object>> list = financeAnalysisService.getFinancialPlanList(param);

        if (param.get("page") == null) {
            param.put("page", 1);
        }
        if (param.get("rows") == null) {
            param.put("rows", 10);
        }

        /* paging start */
        pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
        pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
        pagging.setTotalCountArray(list);

        rtnMap.put("rows", list);//data
        rtnMap.put("page", pagging.getCurrPage());//현재페이지
        rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
        rtnMap.put("records", pagging.getTotalCount());//총글갯수
        /* paging end */

        return rtnMap;
    }

    // 재정계획 수립 > 재정수지분석 관리
    @RequestMapping(value = "/getFinancialPlanDetailList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getFinancialPlanDetailList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToMap(req);

        return financeAnalysisService.getFinancialPlanDetailList(param);
    }

    // 재정계획 수립 > 재무재표(BS) 관리
    @RequestMapping(value = "/getFinancialStatementsList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getFinancialStatementsList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToMap(req);

        return financeAnalysisService.getFinancialStatementsList(param);
    }

    // 재정계획 수립 > 재무재표(BS) 관리 (팝업) 저장
    @RequestMapping(value = "/setFinancialStatement.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int setFinancialStatement(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("USER_ID", user.getE_ID());

        return financeAnalysisService.setFinancialStatement(param);
    }

    // 재정계획 수립 > 손익계산서(PL) 관리
    @RequestMapping(value = "/getIncomeStatement.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public ArrayList<HashMap<String, Object>> getIncomeStatement(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToMap(req);

        return financeAnalysisService.getIncomeStatement(param);
    }

    // 재정계획 수립 > 손익계산서(PL) 관리 (팝업) 저장
    @RequestMapping(value = "/setIncomeStatement.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int setIncomeStatement(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("USER_ID", user.getE_ID());

        return financeAnalysisService.setIncomeStatement(param);
    }

    // 재정계획 수립 > 현금흐름표(CF) 관리
    @RequestMapping(value = "/getCashFlowStatement.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public ArrayList<HashMap<String, Object>> getCashFlowStatement(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToMap(req);

        return financeAnalysisService.getCashFlowStatement(param);
    }

    // 재정계획 수립 > 현금흐름표(CF) 관리 (팝업) 저장
    @RequestMapping(value = "/setCashFlowStatement.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int setCashFlowStatement(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("USER_ID", user.getE_ID());

        return financeAnalysisService.setCashFlowStatement(param);
    }

    // 재정계획 수립 > 기타 비용 관리
    @RequestMapping(value = "/getOtherCostManagement.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getOtherCostManagement(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToMap(req);

        return financeAnalysisService.getOtherCostManagement(param);
    }

    // 재정계획 수립 > 기타비용관리 (팝업) 저장
    @RequestMapping(value = "/setOtherCostManagement.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int setOtherCostManagement(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

        Map<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("user_id", user.getE_ID());

        return financeAnalysisService.setOtherCostManagement(param);
    }

    // 재정계획 수립 > 재정계획 삭제
    @RequestMapping(value = "/deleteFinancialPlan.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteFinancialPlan(HttpServletRequest req) throws Exception {
        return financeAnalysisService.deleteFinancialPlan(CommonUtil.reqDataParsingToHashMap(req));
    }

    // 자산관리 계획수립 목록
    @RequestMapping(value = "/getAssetManagementPlanList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getAssetManagementPlanList(@RequestParam HashMap<String, Object> param) throws Exception {
        // SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만
        if (param.get("rules") != null) {
            CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
        }

        HashMap<String, Object> rtnMap = new HashMap<String,Object>();
        ArrayList<HashMap<String, Object>> list = financeAnalysisService.getAssetManagementPlanList(param);

        if (param.get("page") == null) {
            param.put("page", 1);
        }
        if (param.get("rows") == null) {
            param.put("rows", 10);
        }

        /* paging start */
        pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
        pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
        pagging.setTotalCountArray(list);

        rtnMap.put("rows", list);//data
        rtnMap.put("page", pagging.getCurrPage());//현재페이지
        rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
        rtnMap.put("records", pagging.getTotalCount());//총글갯수
        /* paging end */

        return rtnMap;
    }

    // 자산관리 계획수립 상세 목록
    @RequestMapping(value = "/getAssetManagementPlanDetailList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getAssetManagementPlanDetailList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = CommonUtil.reqDataParsingToMap(req);

        return financeAnalysisService.getAssetManagementPlanDetailList(param);
    }

    // 자산관리 계획수립 삭제
    @RequestMapping(value = "/deleteAssetManagementPlan.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteAssetManagementPlan(HttpServletRequest req) throws Exception {
        return financeAnalysisService.deleteAssetManagementPlan(CommonUtil.reqDataParsingToHashMap(req));
    }

    @RequestMapping(value = "/insertTempFinancialPlan.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> insertTempFinancialPlan() throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("user_id", user.getE_ID());

        return financeAnalysisService.insertTempFinancialPlan(param);
    }

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 재정수지 분석 (팝업, TAB-운영관리비)
    @RequestMapping(value = "/getOperationAndManagementExpensesPopupList.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public List<Map<String, Object>> getOperationAndManagementExpensesPopupList(@RequestParam HashMap<String, Object> param) throws Exception {
        return financeAnalysisService.getOperationAndManagementExpensesPopupList(param);
    }

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 재정수지 분석 (팝업, TAB-감가상각비)
    @RequestMapping(value = "/getDepreciationPopupList.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public List<Map<String, Object>> getDepreciationPopupList(@RequestParam HashMap<String, Object> param) throws Exception {
        return financeAnalysisService.getDepreciationPopupList(param);
    }

    @RequestMapping(value = "/updateFinancialPlan.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int updateFinancialPlan(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("USER_ID", user.getE_ID());

        return financeAnalysisService.updateFinancialPlan(param);
    }

    @RequestMapping(value = "/setFinancialPlanDetail.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int setFinancialPlanDetail(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("USER_ID", user.getE_ID());

        return financeAnalysisService.setFinancialPlanDetail(param);
    }

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가 설정 (팝업)
    @RequestMapping(value = "/getUnitPriceSettingInfo.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> getUnitPriceSettingInfo(@RequestParam HashMap<String, Object> param) throws Exception {
        return financeAnalysisService.getUnitPriceSettingInfo(param);
    }

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가/생산량/구입량 설정 (팝업)
    @RequestMapping(value = "/getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo(@RequestParam HashMap<String, Object> param) throws Exception {
        return financeAnalysisService.getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo(param);
    }

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 전기 말 이자율 설정 (팝업)
    @RequestMapping(value = "/getInterestRateEndOfLastYearInfo.json", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> getInterestRateEndOfLastYearInfo() throws Exception {
        return financeAnalysisService.getFinancialAnalysisChargeUnitPrice();
    }

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 전기 말 이자율 설정 (팝업) 저장
    @RequestMapping(value = "/updateInterestRateEndOfLastYearInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int setFinancialAnalysisChargeUn2itPrice(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

        Map<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("user_id", user.getE_ID());

        return financeAnalysisService.updateInterestRateEndOfLastYearInfo(param);
    }

    // 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가, 요금단가/생산량/구입량 설정 저장
    @RequestMapping(value = "/setFinancialAnalysisChargeUnitPrice.json", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public int setFinancialAnalysisChargeUnitPrice(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

        Map<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("user_id", user.getE_ID());

        return financeAnalysisService.setFinancialAnalysisChargeUnitPrice(param);
    }

    // 자산관리 계획수립 신규
    @RequestMapping(value = "/insertAssetManagementPlan.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertAssetManagementPlan(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

    	HashMap<String, Object> param = new HashMap<String, Object>();
    	param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());

        return financeAnalysisService.insertAssetManagementPlan(param);
    }

    // 자산관리 계획수립 수정
    @RequestMapping(value = "/updateAssetManagementPlan.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateAssetManagementPlan(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    	HashMap<String, Object> param = new HashMap<String, Object>();

    	param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());

        return financeAnalysisService.updateAssetManagementPlan(param);
    }

    // 자산관리 계획수립 수정
    @RequestMapping(value = "/updateAssetManagementPlanDetail.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateAssetManagementPlanDetail(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    	HashMap<String, Object> param = new HashMap<String, Object>();

    	param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());

        return financeAnalysisService.updateAssetManagementPlanDetail(param);
    }
    
    // ComCodeListYr2 추가
    @RequestMapping(value = "/addComCodeListYr2.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int addComCodeListYr2(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    	HashMap<String, Object> param = new HashMap<String, Object>();

    	param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());

        return financeAnalysisService.addComCodeListYr2(param);
    }

    // m2_loan_item 추가
    @RequestMapping(value = "/addLoanItemYear.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int addLoanItemYear(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    	HashMap<String, Object> param = new HashMap<String, Object>();

    	param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());

        return financeAnalysisService.addLoanItemYear(param);
    }
    
    //초기화
    @RequestMapping(value = "/initFinancialPlanDetailList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int initFinancialPlanDetailList(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    	HashMap<String, Object> param = new HashMap<String, Object>();

    	param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());

        return financeAnalysisService.initFinancialPlanDetailList(param);
    }
    
    @RequestMapping(value = "/getFinacePriceRaiseRatio.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String,Object> getFinacePriceRaiseRatio(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    	HashMap<String, Object> param = new HashMap<String, Object>();

    	param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());

        return financeAnalysisService.getFinacePriceRaiseRatio(param);
    }
    
    @RequestMapping(value = "/getLoanInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getLoanInfo(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    	HashMap<String, Object> param = new HashMap<String, Object>();

    	param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID",user.getE_ID());

        return financeAnalysisService.getLoanInfo(param);
    }
}