package com.usom.web.controller.api;

import com.framework.model.exception.ApplicationException;
import com.framework.model.util.CommonUtil;
import com.framework.model.util.FileUpload;
import com.framework.model.util.Pagging;
import com.usom.model.service.common.CommonService;
import com.usom.model.service.estimation.EstimationService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;


@Controller
@RequestMapping("/est")
public class EstimationController {
	
	@Resource(name = "estimationService")
	private EstimationService estimationService;
	
	@Resource(name = "fileUpload")
	private FileUpload fileUpload;
	
	@Resource(name = "commonService")
	private CommonService commonService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
	/**
	 * 간접평가 목록
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/getList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getList(@RequestParam HashMap<String, Object> param) throws Exception {
				return estimationService.getList(param);
	}

	/**
	 * 간접평가 자산목록 
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */

	
	@RequestMapping(value = "/assetGroup.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> assetGroup(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		//param = CommonUtil.reqDataParsingToMap(req);
		//param.put("category", req.getParameter("category").toString());
		//param.put("EST_SID", req.getParameter("EST_SID").toString());
	//	param.put("tabId", req.getParameter("tabId").toString());
		//param.put("ASSET_SID", 1000);
		
		if(param.get("category").equals("1")) {
			return estimationService.getIndirEstAssetGroup(param);
		}else {
			return estimationService.getDirEstAssetGroup(param);
		}
		//return estimationService.getDirEstAssetResultList(param);
	}
	
	@RequestMapping(value = "/asset.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> assetList(@RequestParam HashMap<String, Object> param) throws Exception {
		
		if(param.get("category").equals("1")) {
			return estimationService.getIndirEstAssetList(param);
		}else {
			return estimationService.getDirEstAssetList(param);
		}
	}

	@RequestMapping(value = "/assetNonePage.json", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<Map<String, Object>> assetNonePage(@RequestParam Map<String, Object> param) {
		return estimationService.getEstAssetNonePage(param);
	}
	
	@RequestMapping(value = "/assetPage.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> assetPage(@RequestParam HashMap<String, Object> param) throws Exception {
		
		if(param.get("SQL") == null || param.get("SQL").toString().equals("")) {
			return estimationService.getEstAssetPage(param);
		}else {
			JSONObject jsonSql = JSONObject.fromObject(param.get("SQL").toString());
			HashMap<String, Object> SQLPARM = CommonUtil.convertJsonToMap(jsonSql);

			SQLPARM.put("rows", param.get("rows"));
			SQLPARM.put("page", param.get("page"));
			SQLPARM.put("category", param.get("category"));
			SQLPARM.put("EST_SID", param.get("EST_SID"));
			SQLPARM.put("CLASS4_CD", (StringUtils.isNotEmpty(param.get("CLASS4_CD").toString())?param.get("CLASS4_CD").toString():""));
			SQLPARM.put("CLASS3_CD", (StringUtils.isNotEmpty(param.get("CLASS3_CD").toString())?param.get("CLASS3_CD").toString():""));
			SQLPARM.put("BL_BCODE", (StringUtils.isNotEmpty(param.get("BL_BCODE").toString())?param.get("BL_BCODE").toString():""));
			return estimationService.getEstAssetPage(SQLPARM);
		}
	}	

	/**
	 * 간접평가 자산목록 평가결과
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/assetResult.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> assetResultList(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param.put("category", req.getParameter("category").toString());
		param.put("EST_SID", req.getParameter("EST_SID").toString());
		param.put("ASSET_SID", req.getParameter("ASSET_SID").toString());
		if(param.get("category").equals("2")) {
			return estimationService.getDirEstAssetResultList(param);
		}else {
			return estimationService.getIndirEstAssetResultList(param);	
		}
		
	}
	

	@RequestMapping(value = "/insertAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertAssetList(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return estimationService.insertIndirEstAsset(param);
	}
	
	@RequestMapping(value = "/deleteAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteAssetList(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return estimationService.deleteEstAsset(param);
	}
	
	@RequestMapping(value = "/saveIndirEstAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int saveIndirEstAsset(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return estimationService.saveIndirEstAsset(param);
	}

	
	
	@RequestMapping(value = "/saveIndirEstAssetResult.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int saveIndirEstAssetResult(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return estimationService.saveIndirEstAssetResult(param);
	}

	@RequestMapping(value = "/saveDirEstAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int saveDirEstAsset(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return estimationService.saveDirEstAsset(param);
	}
	
	@RequestMapping(value = "/saveDirEstAssetResult.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int saveDirEstAssetResult(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return estimationService.saveDirEstAssetResult(param);
	}

	//직접평가 입력 파일업로드
	@RequestMapping(value = "/saveDirEstAssetResult2.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int saveDirEstAssetResult2(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return estimationService.saveDirEstAssetResult2(param);
	}
	
	/**
	 * 간접평가 변경이력
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/history.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> historyList(@RequestParam HashMap<String, Object> param) throws Exception {
		
		return estimationService.historyList(param);
	}	
	
	/**
	 * 간접평가 신규
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insert.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insert(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		param.put("insert_id",user.getE_ID());
		return estimationService.insert(param);
	}
	
	/**
	 * 간접평가 수정
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> update(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		param.put("update_id",user.getE_ID());
		
		int rtncnt = estimationService.update(param);
		if (rtncnt == 1) {
			return param;
		} else {
			throw new ApplicationException("업데이트 중 에러가 발생하였습니다.");
		}
	}
	
	/**
	 * 간접평가 삭제
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/delete.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int delete(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		int rtncnt = estimationService.delete(param);
		if (rtncnt == 1) {
			/*
			if (param.containsKey("f_num") && !"".equals(CommonUtil.replaceNullString(param.get("f_num").toString()))) {
				int fnum = Integer.parseInt(param.get("f_num").toString());
				if (fnum > 0) {
					rtncnt = commonService.deleteFileInfo(fnum);
				}
			}
			*/
			
		} else {
			throw new ApplicationException("삭제중 에러가 발생하였습니다. : " + param);
		}
		return rtncnt;
	}
	

	/**
	 * 간접평가 항목 목록
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/getFactorList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getFactorList(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return estimationService.getFactorList(param);
	}
	
	/**
	 * 간접평가 항목 변경이력
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/getFactorInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> HistoryFactorList(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return estimationService.getFactorDetail(param);
	}
	
	/**
	 * 간접평가 신규
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertFactor.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertFactor(HttpServletRequest req) throws Exception {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		HashMap<String, Object> param = new HashMap<String, Object>();


		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("insert_id",user.getE_ID());
		param.put("INSERT_ID",user.getE_ID());
		
		System.out.println("insertFactor : " + param);
		return estimationService.insertFactor(param);
	}
	
	/**
	 * 간접평가 수정
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateFactor.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> updateFactor(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("update_id",user.getE_ID());
		param.put("INSERT_ID",user.getE_ID());
		
		int rtncnt = estimationService.updateFactor(param);
		if (rtncnt == 1) {
			HashMap<String, Object> result = new HashMap<String, Object>();
			result.put("STAT","OK");
			return result;
		} else {
			throw new ApplicationException("업데이트 중 에러가 발생하였습니다.");
		}
	}
	
	/**
	 * 간접평가 항목 삭제
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/deleteFactor.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteFactor(HttpServletRequest req) throws Exception {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		int rtncnt = estimationService.deleteFactor(param);
		if (rtncnt == 1) {
			if (param.containsKey("f_num") && !"".equals(CommonUtil.replaceNullString(param.get("f_num").toString()))) {
				int fnum = Integer.parseInt(param.get("f_num").toString());
				if (fnum > 0) {
					rtncnt = commonService.deleteFileInfo(fnum);
				}
			}
			
		} else {
			throw new ApplicationException("삭제중 에러가 발생하였습니다. : " + param);
		}
		return rtncnt;
	}
	
	/**
	 * 간접평가 개량방안 목록
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/getGradeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	//public @ResponseBody HashMap<String, Object> getGradeList(@RequestParam HashMap<String, Object> param) throws Exception {
	public @ResponseBody HashMap<String, Object> getGradeList(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		return estimationService.getGradeList(param);
	}
	
	/**
	 * 간접평가 항목 변경이력
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/getGradeInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> HistoryGradeList(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		
		return estimationService.getGradeDetail(param);
	}
	
	/**
	 * 간접평가 신규
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertGrade.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertGrade(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("update_id",user.getE_ID());
		param.put("INSERT_ID",user.getE_ID());
		
		//HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		
		return estimationService.insertGrade(param);
	}
	
	/**
	 * 간접평가 수정
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateGrade.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> updateGrade(HttpServletRequest req) throws Exception {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("update_id",user.getE_ID());
		param.put("INSERT_ID",user.getE_ID());
		
		//HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		
		int rtncnt = estimationService.updateGrade(param);
		if (rtncnt == 1) {
			return param;
		} else {
			throw new ApplicationException("업데이트 중 에러가 발생하였습니다.");
		}
	}

	/**
	 * 통계분석
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getStatIndirEst.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>>  getStatIndirEst(HttpServletRequest req) throws Exception {

		HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        //ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	//System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	//System.out.println(columnListStr);
        }catch(Exception ex) {

        }		
		return estimationService.getStatIndirEst(param);
		
	}	
	
	@RequestMapping(value = "/getStatRemainLife.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>>  getStatRemainLife(HttpServletRequest req) throws Exception {

		HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        //ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        }catch(Exception ex) {

        }
		return estimationService.getStatRemainLife(param);
		
	}
	
	@RequestMapping(value = "/getStatRisk.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>>  getStatRisk(HttpServletRequest req) throws Exception {

		HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        //ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        }catch(Exception ex) {

        }		
		return estimationService.getStatRisk(param);
		
	}
	
	@RequestMapping(value = "/getStatLoS.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>>  getStatLoS(HttpServletRequest req) throws Exception {

		HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        //ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        }catch(Exception ex) {

        }		
		return estimationService.getStatLoS(param);
		
	}	

	@RequestMapping(value = "/getStatLcc.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>>  getStatLcc(HttpServletRequest req) throws Exception {

		HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        //ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        }catch(Exception ex) {

        }		
		return estimationService.getStatLcc(param);
		
	}	
	
	@RequestMapping(value = "/getStatOIP.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>>  getStatOIP(HttpServletRequest req) throws Exception {

		HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
       // ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        }catch(Exception ex) {

        }		
		return estimationService.getStatOIP(param);
		
	}

	@RequestMapping(value = "/getStatFinance.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getStatFinance(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
		String StartDt = param.get("START_DT").toString();
		String EndDt = param.get("END_DT").toString();
		int start_year = Integer.parseInt(StartDt.substring(0, 4));
		int end_year = Integer.parseInt(EndDt.substring(0, 4));
		int i;
		int year;
		HashMap<String, Object> ColumnList = new HashMap<String,Object>();
//		JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
		String yyyymm = "";
		StringBuilder columnListStr = new StringBuilder();
		StringBuilder monthListStr = new StringBuilder();

		try {
//			for (i = 0; i < chkMonthList.size(); i++) {
//				if (i > 0) {
//					monthListStr.append(",");
//				}
//				monthListStr.append(chkMonthList.get(i).toString());
//			}

			int k = 0;
//			boolean isNotYet = true;
			for (year = start_year; year <= end_year; year++) {
//				if (param.get("UNIT").equals("MONTH")) {
//					int size = chkMonthList.size();
//					System.out.println(chkMonthList);
//					for (i = 0; i < size; i++) {
//						System.out.println("" + year + chkMonthList.get(i));
//						yyyymm = "" + year + chkMonthList.get(i).toString();
//						if (StartDt.substring(0, 6).equals(yyyymm)) isNotYet = false;
//						if (isNotYet && chkMonthList.size() == 12) continue;
//						ColumnList.put("" + k, yyyymm);
//						k++;
//						if (EndDt.substring(0, 6).equals(yyyymm) && chkMonthList.size() == 12) break;
//					}
//				} else {
					yyyymm = String.valueOf(year);
					ColumnList.put(String.valueOf(k), yyyymm);
					k++;
//				}
			}

			Set<String> keys = ColumnList.keySet();
			i = 0;
			for (String key : keys) {
				System.out.println("key: " + key + "/" + ColumnList.get(key));
				if (i > 0) {
					columnListStr.append(",");
				}
				columnListStr.append("'").append(ColumnList.get(key)).append("'");
				i++;
			}

			param.put("columnList", columnListStr.toString());
			param.put("monthList", monthListStr.toString());
			System.out.println(columnListStr);
		} catch (Exception ex) {

		}

		return estimationService.getStatFinance(param);
	}

	@RequestMapping(value = "/getGISInformationAndConditionAssessmentResults.json", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<Map<String, Object>> getGISInformationAndConditionAssessmentResults(@RequestParam Map<String, Object> param) throws Exception {
		return estimationService.getGISInformationAndConditionAssessmentResults(param);
	}
}
