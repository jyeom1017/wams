package com.usom.web.controller.api;

import com.framework.model.exception.ApplicationException;
import com.framework.model.util.CommonUtil;
import com.framework.model.util.FileUpload;
import com.framework.model.util.Pagging;
import com.usom.model.config.Configuration;
import com.usom.model.service.bbs.BbsService;
import com.usom.model.service.common.CommonService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Controller
@RequestMapping("/bbs")
public class BbsApiController {

    //private static Logger logger = Logger.getLogger(BbsApiController.class);
    private static final Logger logger = LogManager.getLogger(BbsApiController.class);

    @Resource(name = "bbsService")
    private BbsService bbsService;

    @Resource(name = "fileUpload")
    private FileUpload fileUpload;

    @Resource(name = "commonService")
    private CommonService commonService;

    @Autowired
    @Qualifier("pagging")
    public Pagging pagging;



    //게시물 리스트 불러오기
    @RequestMapping(value = "/getBBSList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getBBSList(@RequestParam HashMap<String, Object> param) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();    	
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        if (param.get("rules") != null){
            CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
        }
        if ("RNUM".equals(param.get("sidx"))) {
            param.put("sidx", "ROWNUM");
        }
        
        param.put("GCODE", user.getE_GCODE1());
        System.out.println(param);
        list = bbsService.getList(param);

        HashMap<String, Object> rtnMap = new HashMap<String, Object>();

        if (param.get("page") == null)
            param.put("page", 1);
        if (param.get("rows") == null)
            param.put("rows", 10);
        // if(param.get("page")==null) param.put("page", 1);
        // if(param.get("rows")==null) param.put("rows", 10);
        /* paging start */
        pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
        pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
        pagging.setTotalCountArray(list);

        rtnMap.put("rows", list);//data
        rtnMap.put("page", pagging.getCurrPage());//현재페이지
        rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
        rtnMap.put("records", pagging.getTotalCount());//총글갯수
        /* paging end */

        return rtnMap;
    }

    //임시 게시물 삭제
    @RequestMapping(value = "/deleteBbsTempItem", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteBbsTempItem(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        param.put("E_ID", user.getE_ID());

        return bbsService.deleteBbsTempItem(param);
    }

    //글에 첨부된 첨부파일 전부 삭제 (DB)
    @RequestMapping(value = "/deleteBbsFile", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteBbsFile(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);

        return bbsService.deleteBbsFile(param);
    }

    //글 삭제. delete_yn = 'y'으로 업데이트
    @RequestMapping(value = "/deleteBbsItem", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteBBSItem(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        param.put("UPDATEUSER", user.getE_NAME());

        return bbsService.deleteItem(param);
    }

    //신규 클릭 시 임시글 등록. default로 temp_yn = 'y'
    @RequestMapping(value = "/insertBbsTempItem", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertBbsTempItem(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        param.put("E_ID", user.getE_ID());
        param.put("CREATEUSER", user.getE_NAME());
        return bbsService.insertBbsTempItem(param);
    }

    //신규 팝업창에서 저장/수정했을 때 업데이트
    @RequestMapping(value = "/insertBbsItem", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertBbsItem(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        param.put("UPDATEUSER", user.getE_NAME());
        return bbsService.insertBbsItem(param);
    }

    //게시물에 첨부된 파일들 불러오기
    @RequestMapping(value = "/getBbsFileList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getBbsFileList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = bbsService.getBbsFileList(param);
        return list;
    }

    //파일 저장
    @RequestMapping(value = "/saveBbsFile", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int saveBbsFile(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        return bbsService.insertBbsFile(param);
    }

    //글에 첨부된 첨부파일 개별 삭제 (x 버튼 클릭)
    @Transactional
    @RequestMapping(value = "/deleteBbsFileOne.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteBbsFileOne(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);

        //m2_bbs_file 삭제
        int cnt = bbsService.deleteBbsFileOne(param);
        return cnt;
    }

    //파일 저장
    @RequestMapping(value = "/replyCount.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int replyCount(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        return bbsService.replyCount(param);
    }

    @RequestMapping(value = "/getReplyList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getReplyList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = bbsService.getReplyList(param);
        return list;
    }

    @RequestMapping(value = "/updateReadCount.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateReadCount(HttpServletRequest req, HttpServletResponse res) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        String bbs_group_cd = param.get("bbs_group_cd").toString();
        int bbs_sid = Integer.parseInt(param.get("bbs_sid").toString());

        // https://developersoo.tistory.com/14 쿠키를 이용한 조회수 중복 방지

        Cookie[] cookies = req.getCookies();
        Cookie viewCookie = null;

        // 쿠키 있으면 viewCookie에 넣는다
        if (cookies != null && cookies.length > 0) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("cookie" + bbs_group_cd + bbs_sid)) {
                    viewCookie = cookies[i];
                }
            }
        }

        // 쿠키 없으면 조회수 증가 로직을 탄다
        if (viewCookie == null) {
            Cookie newCookie = new Cookie("cookie" + bbs_group_cd + bbs_sid, "|" + bbs_group_cd + bbs_sid + "|"); //ex. |notice5|
            res.addCookie(newCookie);
            return bbsService.updateReadCount(param);

        // 쿠키 있으면 조회수 증가 X
        } else {
            String value = viewCookie.getValue();
            return 0;
        }
    }
}
