package com.usom.web.controller.api;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.diagnosis.LccService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/lcc")
public class LccController {
	@Resource(name="lccService")
	private LccService lccService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
    @RequestMapping(value = "/getList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getList(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = lccService.getLCC(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		//paging start 
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		//paging end 
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getAssetList.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getAssetList(@RequestParam HashMap<String, Object> param) throws Exception {
   		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
   		if (param.get("rules") != null) {
   			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
   		}
   		
   		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
   		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
   		
   		if(param.get("SQL") == null || param.get("SQL").toString().equals("")) {
   			list = lccService.getLCC_ASSET(param);
		}else {
			JSONObject jsonSql = JSONObject.fromObject(param.get("SQL").toString());
			HashMap<String, Object> SQLPARM = CommonUtil.convertJsonToMap(jsonSql);

			SQLPARM.put("rows", param.get("rows"));
			SQLPARM.put("page", param.get("page"));
			SQLPARM.put("LCC_SID", param.get("LCC_SID"));
			SQLPARM.put("CLASS4_CD", param.get("CLASS4_CD"));
			SQLPARM.put("CLASS3_CD", param.get("CLASS3_CD"));
	   		list = lccService.getLCC_ASSET(SQLPARM);
		}
   		
   		if (param.get("page") == null) {
   			param.put("page", 1);
   		}
   		if (param.get("rows") == null) {
   			param.put("rows", 10);
   		}
   		
   		//paging start 
   		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
   		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
   		pagging.setTotalCountArray(list);
   		
   		rtnMap.put("rows", list);//data
   		rtnMap.put("page", pagging.getCurrPage());//현재페이지
   		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
   		rtnMap.put("records", pagging.getTotalCount());//총글갯수
   		//paging end 
   		
   		return rtnMap;
   	}
    @RequestMapping(value = "/getTotal.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getTotal(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  lccService.getLCC_ASSET_TOTAL(param);
    }
    
    @RequestMapping(value = "/getDetails.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getDetails(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  lccService.getLCCDetails(param);
    }
    @RequestMapping(value = "/getResultList.json", method = RequestMethod.POST, headers = "Accept=application/json")
   	public @ResponseBody HashMap<String, Object> getResultList(@RequestParam HashMap<String, Object> param) throws Exception {
   		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
   		if (param.get("rules") != null) {
   			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
   		}
   		
   		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
   		
   		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
   		list = lccService.getLCC_RESULT(param);
   		
   		if (param.get("page") == null) {
   			param.put("page", 1);
   		}
   		if (param.get("rows") == null) {
   			param.put("rows", 10);
   		}
   		
   		//paging start 
   		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
   		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
   		pagging.setTotalCountArray(list);
   		
   		rtnMap.put("rows", list);//data
   		rtnMap.put("page", pagging.getCurrPage());//현재페이지
   		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
   		rtnMap.put("records", pagging.getTotalCount());//총글갯수
   		//paging end 
   		
   		return rtnMap;
   	}
    @RequestMapping(value = "/getResultTotal.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getResultTotal(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  lccService.getLCC_RESULT_TOTAL(param);
    }
    @RequestMapping(value = "/insertList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insertList(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
    	
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	param.put("INSERT_ID", user.getE_ID());
    	param.put("INSERT_DT", LocalDate.now().toString());
    	param.put("LCC_DT", LocalDate.now().toString());
    	
    	return  lccService.insertLCC(param);
    }
    
    @RequestMapping(value = "/insertLccAssetResult.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insertLccAssetResult(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  lccService.insertLCC_ASSET_RESULT(param);
    }
    
    @RequestMapping(value = "/startList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> startList(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  lccService.startLCC(param);
    }
    
    @RequestMapping(value = "/progressbarLcc.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> progressbarLcc(HttpServletRequest req) throws Exception {
    	
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	HashMap<String, Object> result = new HashMap<String, Object>();
    	
    	if(Integer.parseInt(param.get("Loading_index").toString()) == 0) {
    		result = lccService.factoryLCC_ASSET_RESULT(param);	
    	}
    	else if(Integer.parseInt(param.get("Loading_index").toString()) == 1) {
    		result = lccService.factoryLCC_ASSET_RESULT_FN(param);	
    	}
    	else if(Integer.parseInt(param.get("Loading_index").toString()) == 2) {
    		result = lccService.factoryLCC_ASSET_RESULT_FN(param);	
    	}
    	
    	return param;
    }
    
    @RequestMapping(value = "/deleteLcc.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteLcc(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  lccService.deleteLcc(param);
    }
    /*getList
	   insert
	   update
	   delete
	   report
	   insertAsset
	   deleteAsset
	   
	   getPriceIndex
	   insertPriceIndex
	   updatePriceIndex
	   deletePriceIndex
	   reportPriceIndex
	   
	   getWeightSpec
	   insertWeightSpec
	   updateWeightSpec
	   deleteWeightSpec*/

}
