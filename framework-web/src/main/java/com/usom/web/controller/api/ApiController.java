package com.usom.web.controller.api;

import com.framework.model.exception.ApplicationException;
import com.framework.model.util.CommonUtil;
import com.framework.model.util.FileUpload;
import com.framework.model.util.Pagging;
import com.framework.model.util.PasswordUtil;
import com.usom.model.config.Configuration;
import com.usom.model.service.basic.EmployeeService;
import com.usom.model.service.bbs.BbsService;
import com.usom.model.service.common.CommonService;
import com.usom.model.vo.user.MenuVO;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

/*import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
*/

/**
 * 기초정보관리 > 인력관리
 *
 * @author ash
 *
 */
@Controller
@RequestMapping("/api")
public class ApiController {
	
	//private static Logger logger = Logger.getLogger(ApiController.class);
	private static final Logger logger = LogManager.getLogger(ApiController.class);
	
	@Resource(name = "fileUpload")
	private FileUpload fileUpload;
	
	@Resource(name = "commonService")
	private CommonService commonService;
	
	@Resource(name = "employeeService")
	private EmployeeService employeeService;
	
	@Resource(name = "bbsService")
	private BbsService bbsService;	
	
		
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;

	@RequestMapping(value = "/getUserInfo.json")
	public @ResponseBody HashMap<String, Object> getUserInfo() {
		HashMap<String, Object> map = new HashMap<>();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		// 접속 정보
		/*WebAuthenticationDetails details = (WebAuthenticationDetails)authentication.getDetails();
		log.info("접근 IP : " + details.getRemoteAddress() + ",접근 METHOD : " + filterInvocation.getRequest().getMethod() + ",접근 URI : " + filterInvocation.getRequest().getRequestURI());
		*/

		if (auth == null) {
			return map;
		}

		Object principal = auth.getPrincipal();

		if (principal instanceof UserDetailsServiceVO) {
			User user = ((UserDetailsServiceVO) principal).getUser();

			map.put("loginId", user.getE_ID());
			map.put("loginNm", user.getE_NAME());
			map.put("groupNm", user.getE_GROLENM());
			map.put("groupCd", user.getE_GCODE1());
			map.put("lastMenu", user.getLAST_MENU());
		}

		return map;
	}
	
	@RequestMapping(value = "/checkMaintenance.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> checkMaintenance(@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		
		//System.out.println("---------점검---------" + commonService.checkMaintenance(param));
		
		//return commonService.checkMaintenance(param);
		return null;
	}
	

	
	@RequestMapping(value = "/getCommonCodeGroup.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getCommonCodeGroup(@RequestParam HashMap<String, Object> param) throws Exception {
		
		//HashMap<String, Object> param = new HashMap<String, Object>();
		ArrayList<HashMap<String, Object>> list = commonService.getCommonCodeGroup(param);
		return commonService.getCommonCodeGroup(param);
	}

	/**
	 * 공통코드 정보 가져오기
	 *
	 * @param param
	 * @return
	 * @throws Exception
	 */
	/*
	@RequestMapping(value = "/getCommonCodeInfo.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getCommonCodeInfo(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getCommonCodeInfo(param);
	}
*/
	@RequestMapping(value = "/getCommonCodeInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getCommonCodeInfo(HttpServletRequest req) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = CommonUtil.reqDataParsing(req);
		System.out.print(parseJson);
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		Iterator<String> keys = parseJson.keys();
		
		while (keys.hasNext()) {
			String key = keys.next();
			if (parseJson.get(key) != null)
				param.put(key, parseJson.get(key).toString());
			else
				param.put(key, "");
		}
		System.out.print("param:");
		System.out.print(param);
		return commonService.getCommonCodeInfo(param);
	}

	
	@RequestMapping(value = "/getCommonCodeList.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getCommonCodeList(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getCommonCodeList(param);
	}
	
	@RequestMapping(value = "/insertCommonCodeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertCommonCodeList(HttpServletRequest req
	//@RequestParam HashMap<String, Object> param
	) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = CommonUtil.reqDataParsing(req);
		System.out.print(parseJson);
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		Iterator<String> keys = parseJson.keys();
		
		while (keys.hasNext()) {
			String key = keys.next();
			if (parseJson.get(key) != null)
				param.put(key, parseJson.get(key).toString());
			else
				param.put(key, "");
		}
		System.out.print("param:");
		System.out.print(param);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

		param.put("INSERT_ID", user.getE_ID());

		return commonService.insertCommonCodeList(param);
	}
	
	@RequestMapping(value = "/updateCommonCodeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateCommonCodeList(HttpServletRequest req
	//@RequestParam HashMap<String, Object> param
	) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = CommonUtil.reqDataParsing(req);
		System.out.print(parseJson);
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		Iterator<String> keys = parseJson.keys();
		
		while (keys.hasNext()) {
			String key = keys.next();
			if (parseJson.get(key) != null)
				param.put(key, parseJson.get(key).toString());
			else
				param.put(key, "");
		}
		System.out.print("param:");
		System.out.print(param);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

		param.put("UPDATE_ID", user.getE_ID());

		return commonService.updateCommonCodeList(param);
	}
	
	@RequestMapping(value = "/deleteCommonCodeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteCommonCodeList(HttpServletRequest req
	//@RequestParam HashMap<String, Object> param
	) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = CommonUtil.reqDataParsing(req);
		System.out.print(parseJson);
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		Iterator<String> keys = parseJson.keys();
		
		while (keys.hasNext()) {
			String key = keys.next();
			if (parseJson.get(key) != null)
				param.put(key, parseJson.get(key).toString());
			else
				param.put(key, "");
		}
		System.out.print("param:");
		System.out.print(param);
		
		return commonService.deleteCommonCodeList(param);
	}
	
	@RequestMapping(value = "/getFileList.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getFileList(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getFileList(param);
	}
	
	public List<Map<String, Object>> parseInsertFileInfo(
			//Map<String,Object> map,
			String uploadDir, String urlPath, HttpServletRequest request) throws Exception {
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
		Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		String filePath = uploadDir + "/";
		MultipartFile multipartFile = null;
		String originalFileName = null;
		String originalFileExtension = null;
		String storedFileName = null;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> listMap = null;
		
		// String boardIdx = (String)map.get("IDX");
		
		//File file = new File(filePath);2020
		File file = new File(uploadDir + "/");
		if (file.exists() == false) {
			file.mkdirs();
		}
		
		while (iterator.hasNext()) {
			multipartFile = multipartHttpServletRequest.getFile(iterator.next());
			if (multipartFile.isEmpty() == false) {
				originalFileName = multipartFile.getOriginalFilename();
				originalFileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
				 
				storedFileName = CommonUtil.createUniqueIDFromDate() + originalFileExtension;
				
				if (storedFileName.endsWith(".jsp") || storedFileName.endsWith(".js") || storedFileName.endsWith(".html") || storedFileName.endsWith(".htm")) {
					System.out.println("파일 타입을 확인하세요.");           
					return null;       
				} 
				
				 if (storedFileName != null && !"".equals(storedFileName)) {        
					 storedFileName = storedFileName.replaceAll("/", "");        
					 storedFileName = storedFileName.replaceAll("\\", "");        
					 storedFileName = storedFileName.replaceAll(".", "");        
					 storedFileName = storedFileName.replaceAll("&", "");        
			     }				
				
				file = new File(uploadDir + "/" + storedFileName);
				multipartFile.transferTo(file);
				
				listMap = new HashMap<String, Object>();
				// listMap.put("BOARD_IDX", boardIdx);
				listMap.put("ORIGINAL_FILE_NAME", originalFileName);
				listMap.put("STORED_FILE_NAME", storedFileName);
				listMap.put("FILE_SIZE", multipartFile.getSize());
				list.add(listMap);
				
				HashMap<String, Object> fileInfo = new HashMap<String, Object>();
				fileInfo.put("F_NAME", storedFileName);// F_NAME
				fileInfo.put("F_TITLE", originalFileName);// F_TITLE
				fileInfo.put("F_PATH", urlPath + "/" + storedFileName);// F_PATH
				fileInfo.put("userid", user.getE_ID());
				//fileInfo.put("F_BOOK_OPT", "04");//도서구분-F_BOOK_OPT
				int filenumber = insertFileInfo(fileInfo); // 두번째 루트(M2_FILE)
				fileInfo.put("filenumber", filenumber);
				list.add(fileInfo);
			}
		}
		return list;
	}
	
	@RequestMapping(value = "/largeFileUpload", method = RequestMethod.POST, headers = "Accept=application/json")
	public void largeFileUpload(HttpServletRequest request) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = JSONObject.fromObject(request.getParameter("data").toString());
		
		// file Check
		String[] arrFileType = { "PDF", "ZIP", "XLS", "XLSX", "HWP", "PPT", "PPTX", "DOC", "DOCX", "JPG", "GIF", "PNG" };
		//String folderName = request.getParameter("data"); // 폴더 이름을 받아온다.
		String folderName = parseJson.get("folderName").toString();
		
		 if (folderName != null && !"".equals(folderName)) {        
			 folderName = folderName.replaceAll("/", "");
			 folderName = folderName.replaceAll("\\", "");
			 folderName = folderName.replaceAll(".", "");
			 folderName = folderName.replaceAll("&", "");        
		 }
		
		// System.out.println("------------폴더 이름"+folderName);
		String uploadDir = Configuration.UPLOAD_FILE_PATH + "file/" + folderName;
		// System.out.println("------------uploadDir 이름"+uploadDir);
		String urlPath = Configuration.UPLOAD_FILE_URL + "file/" + folderName;
		// System.out.println("------------urlPath 이름"+uploadDir);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);
		if (!isMultipartContent) {
			System.out.println("You are not trying to upload<br/>");
			return;
		}
		// ...
		try {
			List<Map<String, Object>> files = parseInsertFileInfo(uploadDir, urlPath, request);
			System.out.println("Number of fields: " + files.size() + "<br/><br/>");
			System.out.println(files);
			
			/*
			 * FileItemFactory factory = new DiskFileItemFactory(); ServletFileUpload upload
			 * = new ServletFileUpload(factory);
			 *
			 * List<FileItem> fields = upload.parseRequest(request);
			 * System.out.println("Number of fields: " + fields.size() + "<br/><br/>");
			 */
			// ...
			
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> fileUpload(MultipartHttpServletRequest request) throws Exception {
		logger.info("파일 업로드 api controller");
		JSONObject parseJson = new JSONObject();
		parseJson = JSONObject.fromObject(request.getParameter("data").toString());
		// file Check
		String[] arrFileType = { "PDF", "ZIP", "XLS", "XLSX", "HWP", "PPT", "PPTX", "DOC", "DOCX", "JPG", "JPEG", "GIF", "PNG", "MP4" };
		//String folderName = request.getParameter("data"); // 폴더 이름을 받아온다.
		String folderName = parseJson.get("folderName").toString();
		
		// System.out.println("------------폴더 이름"+folderName);
		String uploadDir = Configuration.UPLOAD_FILE_PATH + "file/" + folderName;
		// System.out.println("------------uploadDir 이름"+uploadDir);
		String urlPath = Configuration.UPLOAD_FILE_URL + "file/" + folderName;
		// System.out.println("------------urlPath 이름"+uploadDir);
		HashMap<String, Object> fileInfo = new HashMap<String, Object>();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		/*
		 * 파일 루트 생성
		 */
		// 기본설정 세팅
		// fileLimitSize : mb
//		fileUpload.setFileUpload(arrFileType, 1000, null, uploadDir); // fileUpload.java 이동
		fileUpload.setFileUpload(arrFileType, Configuration.UPLOAD_FILE_MAX_UPLOAD_SIZE, null, uploadDir); // 업로드 최대 용량 20mb으로 변경
		if (fileUpload.validationFile(request)) {// file validation 체크 //fileUpload.java 이동
			CommonsMultipartFile fileData = (CommonsMultipartFile) request.getFile("fileData");

			if (fileData != null && !fileData.isEmpty()) {
//			if (fileData != null) {

				String fileName = fileUpload.create(fileData); /** fileUpload.java 이동 핵심 */
				long fileSize = fileData.getSize(); //byte
				fileInfo.put("F_NAME", fileName);// F_NAME
				//fileInfo.put("F_TITLE", request.getParameter("fileDesc"));// F_TITLE
				fileInfo.put("F_TITLE", parseJson.get("fileDesc").toString());// F_TITLE
				fileInfo.put("F_PATH", urlPath + "/" + fileName);// F_PATH
				fileInfo.put("userid", user.getE_ID());
				//fileInfo.put("F_BOOK_OPT", parseJson.get("bookOpt").toString());//도서구분-F_BOOK_OPT
				fileInfo.put("F_SIZE", fileSize);
				int filenumber = insertFileInfo(fileInfo); // 두번째 루트
				fileInfo.put("filenumber", filenumber);
				System.out.println("m2_file fileInfo : " + fileInfo);
			}
		} else {
			throw new ApplicationException("파일 정보가 잘못되었습니다.[확장자, 파일사이즈]");
		}
		// System.out.println("------------fileInfo 이름"+fileInfo);
		return fileInfo;
	}
	
	/*
	 * @RequestMapping(value = "/cctvFileUpload", method = RequestMethod.POST,
	 * headers = "Accept=application/json") public @ResponseBody HashMap<String,
	 * Object> cctvFileUpload(MultipartHttpServletRequest request) throws Exception
	 * { JSONObject parseJson = new JSONObject(); parseJson =
	 * JSONObject.fromObject(request.getParameter("data").toString()); // file Check
	 * String[] arrFileType = { "MP4" }; String uploadDir = Configuration.CCTV_PATH;
	 * // System.out.println("------------uploadDir 이름"+uploadDir); String urlPath =
	 * Configuration.CCTV_URL; //
	 * System.out.println("------------urlPath 이름"+uploadDir); HashMap<String,
	 * Object> fileInfo = new HashMap<String, Object>(); Authentication auth =
	 * SecurityContextHolder.getContext().getAuthentication(); User user =
	 * ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
	 * 
	 * 파일 루트 생성
	 * 
	 * // 기본설정 세팅 fileUpload.setFileUpload(arrFileType, 1000, null, uploadDir); //
	 * fileUpload.java 이동 if (fileUpload.validationFile(request)) {// file
	 * validation 체크 //fileUpload.java 이동 CommonsMultipartFile fileData =
	 * (CommonsMultipartFile) request.getFile("fileData"); if (fileData != null &&
	 * !fileData.isEmpty()) { String fileName = fileUpload.create(fileData);
	 *//** fileUpload.java 이동 핵심 *//*
									 * fileInfo.put("F_NAME", fileName);// F_NAME fileInfo.put("F_TITLE",
									 * parseJson.get("fileDesc").toString());// F_TITLE fileInfo.put("F_PATH",
									 * urlPath + "/" + fileName);// F_PATH fileInfo.put("userid", user.getE_ID());
									 * fileInfo.put("F_BOOK_OPT",
									 * parseJson.get("bookOpt").toString());//도서구분-F_BOOK_OPT int filenumber =
									 * insertFileInfo(fileInfo); // 두번째 루트 fileInfo.put("filenumber", filenumber); }
									 * } return fileInfo; }
									 */
	/*@RequestMapping(value = "/cctvFileUpload", method = RequestMethod.POST, headers = "Accept=application/json")
	public List<Map<String, Object>> cctvFileUpload(HttpServletRequest request) throws Exception {
		JSONObject parseJson = new JSONObject();
		List<Map<String, Object>> files = null;
		parseJson = JSONObject.fromObject(request.getParameter("data").toString());
		
		// file Check
		String[] arrFileType = { "MP4" };
		String uploadDir = Configuration.CCTV_PATH;
		String urlPath = Configuration.CCTV_URL;
		
		boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);
		if (!isMultipartContent) {
			System.out.println("You are not trying to upload<br/>");
			return null;
		}
		
		try {
			files = parseInsertFileInfo(uploadDir, urlPath, request);
			System.out.println("Number of fields: " + files.size() + "<br/><br/>");
			System.out.println(files);
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
		return files;
	}
	*/
	@RequestMapping(value = "/deleteFileInfo", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteFileInfo(@RequestParam HashMap<String, Object> param) throws Exception {
		int fnum = Integer.parseInt(param.get("f_num").toString());
		System.out.println("삭제할 파일 번호 " + fnum);
		
		HashMap<String, Object> delFileInfo = commonService.getFileInfo(fnum);
		String filepath = delFileInfo.get("F_PATH").toString().replaceAll(Configuration.UPLOAD_FILE_URL, "");// file URL
																														// 경로
																														// 정보를
																														// 지운다.
		filepath = Configuration.UPLOAD_FILE_PATH + filepath;// 실제파일경로로 결합한다.
		
		System.out.println("filepath :"+filepath);
		
		File f = new File(filepath);
		f.delete();// 파일삭제

		return commonService.deleteFileInfo(fnum); //M2_FILE 삭제
	}

	//사용자권한별 메뉴목록
	@RequestMapping(value = "/getMenuList.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody JSONArray getMenuList(@RequestParam HashMap<String, Object> param) throws Exception {
		System.out.println("getMenuList--------------------------------------------------------------");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		System.out.println("auth:" + auth.getPrincipal());
		ArrayList<MenuVO> basicMenuList = ((UserDetailsServiceVO) auth.getPrincipal()).getBasicMenuList();
		JSONArray jsonBasicMenuList = JSONArray.fromObject(basicMenuList);
		return jsonBasicMenuList;
	}
	
	//전체 메뉴목록
	@RequestMapping(value = "/getCommonMenuList.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getCommonMenuList(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getMenuList(param);
	}
	
	@RequestMapping(value = "/insertMenu.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertMenu(HttpServletRequest req
	//@RequestParam HashMap<String, Object> param
	) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = CommonUtil.reqDataParsing(req);
		System.out.print(parseJson);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

		HashMap<String, Object> param = new HashMap<String, Object>();
		Iterator<String> keys = parseJson.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			param.put(key, parseJson.get(key).toString());
		}
		param.put("INSERT_ID", user.getE_ID());
		return commonService.insertMenu(param);
	}
	
	@RequestMapping(value = "/updateMenu.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateMenuList(HttpServletRequest req
	//@RequestParam HashMap<String, Object> param
	) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = CommonUtil.reqDataParsing(req);
		System.out.print(parseJson);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();

		HashMap<String, Object> param = new HashMap<String, Object>();
		Iterator<String> keys = parseJson.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			param.put(key, parseJson.get(key).toString());
		}
		param.put("UPDATE_ID", user.getE_ID());
		return commonService.updateMenu(param);
	}

	@RequestMapping(value = "/deleteMenu", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteMenu(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.deleteMenu(param);
	}

	@RequestMapping(value = "/getMaxSort", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int getMaxSort(HttpServletRequest req) throws Exception {
//	public @ResponseBody int getMaxSort(@RequestParam HashMap<String, Object> param) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);

		logger.info("tsort param?? " + param);
		return commonService.getMaxSort(param);
	}


	private int insertFileInfo(HashMap<String, Object> fileInfo) // 파일 번호 생성
			throws Exception {
		commonService.insertFileInfo(fileInfo);
		return Integer.parseInt(fileInfo.get("f_num").toString());
	}
	
	@RequestMapping(value = "/getAuthGroupList", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getAuthGroupList(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getAuthGroupList(param);
	}
	
	@RequestMapping(value = "/getNextGroupCode", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getNextGroupCode(@RequestParam HashMap<String, Object> param) throws Exception {
		return "{\"GCODE\":\"" + commonService.getNextGroupCode() + "\"}";
	}
	
	
	@RequestMapping(value = "/getAuthMenuList", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getAuthMenuList(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getAuthMenuList(param);
	}
	
	@RequestMapping(value = "/saveAuthMenuList", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int saveAuthMenuList(HttpServletRequest req
	//@RequestParam HashMap<String, Object> param
	) throws Exception {
		JSONObject parseJson = new JSONObject();
		parseJson = CommonUtil.reqDataParsing(req);
		System.out.print(parseJson);
		HashMap<String, Object> param = new HashMap<String, Object>();
		Iterator<String> keys = parseJson.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			param.put(key, parseJson.get(key));
		}
		
		return commonService.saveAuthMenu(param);
	}
	
	@RequestMapping(value = "/insertAuthGroup", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertAuthGroup(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.insertAuthGroup(param);
	}
	
	@RequestMapping(value = "/updateAuthGroup", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateAuthGroup(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.updateAuthGroup(param);
	}
	
	@RequestMapping(value = "/deleteAuthGroup", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteAuthGroup(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.deleteAuthGroup(param);
	}
	
	@RequestMapping(value = "/checkUserId", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody boolean checkUserId(@RequestParam HashMap<String, Object> param) throws Exception {
		String userId = param.get("userId").toString();
		System.out.print("checkUserId:" + userId);
		return employeeService.checkUsedEmployeeId(userId);
	}
	
	@RequestMapping(value = "/getHJDCode", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getHJDCode(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getHJDCodeList(param);
	}
	
	@RequestMapping(value = "/getPictureList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getPictureList(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getPictureList(param);
	}
	
	@RequestMapping(value = "/getHisPictureList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getHisPictureList(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getHisPictureList(param);
	}
	
	

	

	
	//
	
	//사용자메뉴 추가삭제
	@RequestMapping(value = "/setUserMenu", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int setUserMenu(@RequestParam HashMap<String, Object> param) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		param.put("E_ID", user.getE_ID());
		if ("N".equals(param.get("USE_BOOKMARK"))) {
			return commonService.setUserMenu(param);
		} else {
			return commonService.unSetUserMenu(param);
		}
	}
	
	// 로그 리스트 
	@RequestMapping(value = "/getSystemLogExcel.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getSystemLogExcel(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		if (param.get("rules") != null){
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		if ("RNUM".equals(param.get("sidx"))) {
			param.put("sidx", "ROWNUM");
		}
		return commonService.getSystemLogExcel(param);
		
	}
	
	// 로그 리스트 가져오기
	@RequestMapping(value = "/getSystemLog.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getSystemLog(@RequestParam HashMap<String, Object> param) throws Exception {
		//return commonService.getSystemLog(param);
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		if (param.get("rules") != null){
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		if ("RNUM".equals(param.get("sidx"))) {
			param.put("sidx", "ROWNUM");
		}
		list = commonService.getSystemLog(param);

		HashMap<String, Object> rtnMap = new HashMap<String, Object>();

		if (param.get("page") == null)
			param.put("page", 1);
		if (param.get("rows") == null)
			param.put("rows", 10);
		// if(param.get("page")==null) param.put("page", 1);
		// if(param.get("rows")==null) param.put("rows", 10);
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);

		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */

		return rtnMap;		
	}
	
	// 로그 리스트 가져오기
	@RequestMapping(value = "/getBlockCodeList.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getBlockCodeList(@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getBlockCodeList(param);
	
	}

	// 패스워드 암호화
	@RequestMapping(value = "/passwordEncrypt", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String passwordEncrypt(HttpServletRequest request) throws Exception {
		String str = request.getParameter("data").toString();
		System.out.print(str);
		return PasswordUtil.BCryptEncoder(str);
	
	}



}
