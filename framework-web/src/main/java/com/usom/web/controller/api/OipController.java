package com.usom.web.controller.api;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.Pagging;
import com.usom.model.service.finance.OipService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/oip")
public class OipController {
	@Resource(name="oipService")
	private OipService oipService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
	@RequestMapping(value = "/getList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getList(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = oipService.getList(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		//paging start 
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		//paging end 
		return rtnMap;
	}
	
    @RequestMapping(value = "/getORDMList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getORDMList(@RequestParam HashMap<String, Object> param) throws Exception {
    	//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = oipService.getORDM(param);;
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		//paging start 
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		//paging end 
		
		return rtnMap;
    }	
	  
    @RequestMapping(value = "/getFNList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getFNList(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  oipService.getFN_CD(param);
    }	
	 
    @RequestMapping(value = "/getDscntList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getDscntList(HttpServletRequest req) throws Exception {
    	return  oipService.getDSCNTList();
    }	
    
    @RequestMapping(value = "/saveDscnt.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int saveDscnt(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
     	param.put("UPDATE_ID", user.getE_ID());
     	
    	return  oipService.saveDSCNT(param);
    }
    
    @RequestMapping(value = "/insertOIP.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> insertOIP(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
     	param.put("INSERT_ID", user.getE_ID());
    	return  oipService.insertOIP(param);
    }
    
    @RequestMapping(value = "/analysisOIP.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int analysisOIP(HttpServletRequest req) throws Exception {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     	User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
     	param.put("INSERT_ID", user.getE_ID());
    	return  oipService.analysisOIP(param);
    }
    
    @RequestMapping(value = "/saveOipAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int saveOipAssetCost(HttpServletRequest req) throws Exception {
    	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  oipService.saveOipAsset(param);
    }	
    
    @RequestMapping(value = "/getOipOptimal.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getOipOptimal(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
   		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
   		
   		if(param.get("SQL") == null || param.get("SQL").toString().equals("")) {
   			param.put("ACLASS4_CD", param.get("CLASS4_CD"));
   			param.put("ACLASS3_CD", param.get("CLASS3_CD"));
   			param.remove("CLASS3_CD");
   			param.remove("CLASS4_CD");
   			list = oipService.getOIP_OPTIMAL(param);
		}else {
			JSONObject jsonSql = JSONObject.fromObject(param.get("SQL").toString());
			HashMap<String, Object> SQLPARM = CommonUtil.convertJsonToMap(jsonSql);
			SQLPARM.put("rows", param.get("rows"));
			SQLPARM.put("page", param.get("page"));
			SQLPARM.put("OIP_SID", param.get("OIP_SID"));
			SQLPARM.put("ACLASS4_CD", param.get("CLASS4_CD"));
			SQLPARM.put("ACLASS3_CD", param.get("CLASS3_CD"));
			SQLPARM.put("OIP_BUDGET_AMT", param.get("OIP_BUDGET_AMT"));
	   		list = oipService.getOIP_OPTIMAL(SQLPARM);
		}
   		
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		//paging start 
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		//paging end 
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getOipOptimalNew.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getOipOptimalNew(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = oipService.getOIP_OPTIMAL_NEW(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		//paging start 
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		//paging end 
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getOipRPN.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getOipRPN(@RequestParam HashMap<String, Object> param) throws Exception {
		//SQL 인젝션 검증(추후 검증할 값만 넣으면 되도록 구현해 볼 것) - Mybatis 에서 $바인딩돼는 컬럼만		
		if (param.get("rules") != null) {
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = oipService.getOIP_RPN(param);
		
		if (param.get("page") == null) {
			param.put("page", 1);
		}
		if (param.get("rows") == null) {
			param.put("rows", 10);
		}
		
		//paging start 
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);
		
		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		//paging end 
		
		return rtnMap;
	}
    
    @RequestMapping(value = "/getORDMAssetList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getORDMAssetList(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  oipService.getORDMAssetList(param);
    }
    @RequestMapping(value = "/saveORDMAssetList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int saveORDMAssetList(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  oipService.updateORDMAssetList(param);
    }
    
    @RequestMapping(value = "/applyOipAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int applyOipAsset(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  oipService.applyOipAsset(param);
    }
   /* @RequestMapping(value = "/applyOipAssetDT.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int applyOipAssetDT(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  oipService.applyOipAssetDT1(param);
    }*/
    @RequestMapping(value = "/applyOipAssetCost.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int applyOipAssetCost(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  oipService.applyOipAssetCost(param);
    }
    @RequestMapping(value = "/deleteOIP.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteOIP(HttpServletRequest req) throws Exception {
     	HashMap<String, Object> param = CommonUtil.reqDataParsingToHashMap(req);
    	return  oipService.deleteOIP(param);
    }
}
