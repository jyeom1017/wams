package com.usom.web.controller.api;

import com.framework.model.util.CommonUtil;
import com.framework.model.util.FileUpload;
import com.framework.model.util.Pagging;
import com.usom.model.service.bbs.BbsService;
import com.usom.model.service.common.CommonService;
import com.usom.model.service.operation.OperationService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.json.JSONArray;

//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

@Controller
@RequestMapping("/operation")
public class OperationApiController {

    //private static Logger logger = Logger.getLogger(OperationApiController.class);
	private static final Logger logger = LogManager.getLogger(OperationApiController.class);

    @Resource(name = "operationService")
    private OperationService operationService;

    @Resource(name = "commonService")
    private CommonService commonService;

    @Autowired
    @Qualifier("pagging")
    public Pagging pagging;

    // 상수운영관리
    
    @RequestMapping(value = "/getConsumerList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getConsumerList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getConsumerList(param);
        return list;
    }
    
    @RequestMapping(value = "/getConsumerGroup.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getConsumerGroup(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getConsumerGroup(param);
        return list;
    }
    
    @RequestMapping(value = "/getBlockList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getBlockList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getBlockList(param);
        return list;
//        return operationService.getBlockList(CommonUtil.reqDataParsingToMap(req));
    }

    @RequestMapping(value = "/getData_flux_wp_month.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getData_flux_wp_month(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getData_flux_wp_month(param);
        return list;
    }

    @RequestMapping(value = "/getData_flux_wp_day.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getData_flux_wp_day(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getData_flux_wp_day(param);
        return list;
    }

    @RequestMapping(value = "/getData_flux_wp_hour.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getData_flux_wp_hour(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getData_flux_wp_hour(param);
        return list;
    }

    @RequestMapping(value = "/getData_flux_wp_min.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getData_flux_wp_min(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getData_flux_wp_min(param);
        return list;
    }

    @RequestMapping(value = "/getData_waterQuality.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getData_waterQuality(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getData_waterQuality(param);
        return list;
    }

    @RequestMapping(value = "/getData_waterQuality_hour.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getData_waterQuality_hour(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getData_waterQuality_hour(param);
        return list;
    }

    @RequestMapping(value = "/getData_waterQuality_min.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getData_waterQuality_min(HttpServletRequest req) throws Exception {
        logger.info("getData_waterQuality api controller");
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getData_waterQuality_min(param);
        logger.info("list : " + list);
        return list;
    }

    // 민원관리
    // 민원 목록 가져오기
    @RequestMapping(value = "/getMinwonList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getMinwonList(@RequestParam HashMap<String, Object> param) throws Exception {
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        if (param.get("rules") != null){
            CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
        }
        if ("RNUM".equals(param.get("sidx"))) {
            param.put("sidx", "ROWNUM");
        }
        list = operationService.getMinwonList(param);

        HashMap<String, Object> rtnMap = new HashMap<String, Object>();

        if (param.get("page") == null)
            param.put("page", 1);
        if (param.get("rows") == null)
            param.put("rows", 10);
        // if(param.get("page")==null) param.put("page", 1);
        // if(param.get("rows")==null) param.put("rows", 10);
        /* paging start */
        pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
        pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
        pagging.setTotalCountArray(list);

        rtnMap.put("rows", list);//data
        rtnMap.put("page", pagging.getCurrPage());//현재페이지
        rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
        rtnMap.put("records", pagging.getTotalCount());//총글갯수
        /* paging end */

        return rtnMap;
    }

    //신규 클릭 시 임시글 등록. temp_yn은 DB에서 디폴트로 'y' 들어간다
    @RequestMapping(value = "/insertMinwonTemp.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertMinwonTemp(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        param.put("E_ID", user.getE_ID());

        return operationService.insertMinwonTemp(param);
    }

    //임시 게시물 삭제
    @RequestMapping(value = "/deleteMinwonTemp.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteMinwonTemp(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        return operationService.deleteMinwonTemp(param);
    }

    // 민원 등록
    @RequestMapping(value = "/saveMinwon.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int saveMinwon(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("E_ID", user.getE_ID());
        return operationService.saveMinwon(param);
    }

    // 민원 삭제
    @RequestMapping(value = "/deleteMinwon.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteMinwon(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        param.put("E_ID", user.getE_ID());
        return operationService.deleteMinwon(param);
    }

    //파일 저장
    @RequestMapping(value = "/insertMinwonFile.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertMinwonFile(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        param.put("E_ID", user.getE_ID());
        return operationService.insertMinwonFile(param);
    }

    //게시물에 첨부된 파일들 불러오기
    @RequestMapping(value = "/getMinwonFileList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getMinwonFileList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        return operationService.getMinwonFileList(param);
    }

    //글에 첨부된 첨부파일 개별 삭제 (x 버튼 클릭)
    @Transactional
    @RequestMapping(value = "/deleteMinwonFileOne.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteMinwonFileOne(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);

        //m2_complain_file 삭제
        return operationService.deleteMinwonFileOne(param);
    }

    @RequestMapping(value = "/getMinwonAssetList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getMinwonAssetList(HttpServletRequest req) throws Exception {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);

        logger.info("param : " + param);

        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getMinwonAssetList(param);
        return list;
    }

    @RequestMapping(value = "/getSelectMinwonAssetPic.json", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap <String,Object >> SelectAssetPic(HttpServletRequest req) throws Exception {
        HashMap <String,Object > param = new HashMap ();
        param.put("complain_sid",Integer.parseInt(req.getParameter("complain_sid").toString()));
        param.put("AssetSid",Integer.parseInt(req.getParameter("AssetSid").toString()));
        param.put("photo_yn",req.getParameter("photo_yn").toString());
        return operationService.selectMinwonAssetPic(param);
    }


    // 비용 수정
    @RequestMapping(value = "/setCostItem.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int setCostItem(@RequestParam HashMap<String, Object> param) throws Exception {
        return operationService.setCostItem(param);
        
    }
    
    // 비용관리 목록
    @RequestMapping(value = "/getCostList.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody HashMap<String, Object> getCostList(@RequestParam HashMap<String, Object> param) throws Exception {
        return operationService.getCostList(param);
        
    }
    
    // 등록
    @RequestMapping(value = "/insertCost.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int insertCost(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("E_ID", user.getE_ID());
        return operationService.insertCost(param);
    }
    //수정
    @RequestMapping(value = "/updateCost.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int updateCost(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("E_ID", user.getE_ID());
        return operationService.updateCost(param);
    }
    // 삭제
    @RequestMapping(value = "/deleteCost.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody int deleteCost(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToMap(req);
        param.put("E_ID", user.getE_ID());
        return operationService.deleteCost(param);
    }
    
    @RequestMapping(value = "/saveCostList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int saveCostList(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("E_ID", user.getE_ID());
        
		
		return operationService.saveCostList(param);
	}
    
    @RequestMapping(value = "/updateCostList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateCostList(HttpServletRequest req) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
        HashMap<String, Object> param = new HashMap<String, Object>();
        param = CommonUtil.reqDataParsingToHashMap(req);
        param.put("E_ID", user.getE_ID());
		
		return operationService.updateCostList(param);
	}
    
    @RequestMapping(value = "/getCostListExcel.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getCostListExcel(HttpServletRequest req) throws Exception {
        logger.info("getData_waterQuality api controller");
        HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        /*
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list = operationService.getCostExcelList(param);
        logger.info("list : " + list);
        return list;
        */
        return operationService.getCostExcelList(param);
    }

    @RequestMapping(value = "/getStatMinwon.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getStatMinwon(HttpServletRequest req) throws Exception {
        logger.info("getData_waterQuality api controller");
        HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        
        list = operationService.getStatMinwon(param);
        } catch (Exception ex) {
        	System.out.println(ex.getMessage());
        }
        logger.info("list : " + list);
        return list;
    }
    
    @RequestMapping(value = "/getStatRenovation.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getStatRepair(HttpServletRequest req) throws Exception {
        logger.info("getData_waterQuality api controller");
        HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        
        list = operationService.getStatRenovation(param);
        } catch (Exception ex) {
        	System.out.println(ex.getMessage());
        }
        logger.info("list : " + list);
        return list;
    }

    @RequestMapping(value = "/getStatInspect.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getStatInspect(HttpServletRequest req) throws Exception {
        logger.info("getData_waterQuality api controller");
        HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        
        list = operationService.getStatInspect(param);
        } catch (Exception ex) {
        	System.out.println(ex.getMessage());
        }
        logger.info("list : " + list);
        return list;
    }
    
    @RequestMapping(value = "/getStatOIP.json", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody ArrayList<HashMap<String, Object>> getStatOIP(HttpServletRequest req) throws Exception {
        logger.info("getData_waterQuality api controller");
        HashMap<String, Object> param = new HashMap<String, Object>();
        //param = CommonUtil.reqDataParsingToMap(req);
        param = CommonUtil.reqDataParsingToHashMap(req);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        
    	System.out.println(param);
        //System.out.println( param.get("CHK_MONTH_LIST"));
        String StartDt = param.get("START_DT").toString();
        String EndDt = param.get("END_DT").toString();
        int start_year = Integer.parseInt(StartDt.substring(0,4));
        int end_year = Integer.parseInt(EndDt.substring(0,4));
        int i=0 ;
        int year = 0;
        HashMap<String, Object> ColumnList = new HashMap<String, Object>();
        JSONArray chkMonthList = (JSONArray) param.get("CHK_MONTH_LIST");
        String yyyymm  = "";
        String columnListStr = "";
        String monthListStr = "";
        try {
        	
        for (i=0; i< chkMonthList.size(); i++) {
        	if(i>0) monthListStr = monthListStr + ",";
        	monthListStr = monthListStr + chkMonthList.get(i).toString();
        }
            
        int k = 0;
        boolean isNotYet = true;
        for (year = start_year;year <= end_year;year++) {
        	if(param.get("UNIT").equals("MONTH")) {
        		int size = chkMonthList.size();
        		System.out.println(chkMonthList);
	            for (i=0; i< size; i++) {
	            	System.out.println("" + year + chkMonthList.get(i));
	            	yyyymm = "" + year + chkMonthList.get(i).toString();
	            	if(StartDt.substring(0,6).equals(yyyymm)) isNotYet = false;
            		if (isNotYet && chkMonthList.size()==12) continue;
	            	ColumnList.put(""+k,yyyymm);
	            	k++;
	            	if(EndDt.substring(0,6).equals(yyyymm) && chkMonthList.size()==12) break;
	            }
	            
        	}else {
        		yyyymm = "" + year;
        		ColumnList.put(""+k,yyyymm);
        		k++;
        	}
        	
        }
        
        
        Set<String> keys = ColumnList.keySet();
        i=0;
        for (String key : keys) {
        	System.out.println("key: " + key + "/" + ColumnList.get(key));
        	if(i>0) columnListStr = columnListStr + ",";
        	columnListStr = columnListStr +  "'" + ColumnList.get(key) + "'";
        	i++;
        }
        
    	param.put("columnList",columnListStr);
    	param.put("monthList",monthListStr);
    	System.out.println(columnListStr);
        
        list = operationService.getStatOIP(param);
        } catch (Exception ex) {
        	System.out.println(ex.getMessage());
        }
        logger.info("list : " + list);
        return list;
    }        
}
