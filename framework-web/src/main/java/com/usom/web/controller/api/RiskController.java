package com.usom.web.controller.api;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.model.exception.ApplicationException;
import com.framework.model.util.CommonUtil;
import com.framework.model.util.FileUpload;
import com.framework.model.util.Pagging;
import com.usom.model.service.risk.*;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.json.JSONObject;

import com.usom.model.service.common.CommonService;


@Controller
@RequestMapping("/risk")
public class RiskController {
	
	@Resource(name = "riskService")
	private RiskService riskService;
	
	@Resource(name = "fileUpload")
	private FileUpload fileUpload;
	
	@Resource(name = "commonService")
	private CommonService commonService;
	
	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;
	
	/**
	 * 위험도 평가 목록
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getList(@RequestParam HashMap<String, Object> param) throws Exception {
				return riskService.getList(param);
	}

	@RequestMapping(value = "/assetGroup.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> assetGroup(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		//param = CommonUtil.reqDataParsingToMap(req);
		//param.put("category", req.getParameter("category").toString());
		//param.put("EST_SID", req.getParameter("EST_SID").toString());
	//	param.put("tabId", req.getParameter("tabId").toString());
		//param.put("ASSET_SID", 1000);
		
			return riskService.getAssetGroup(param);
		
	}
	
	@RequestMapping(value = "/getAssetList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getRiskList(@RequestParam HashMap<String, Object> param) throws Exception {
		if (param.get("rules") != null){
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		if ("RNUM".equals(param.get("sidx"))) {
			param.put("sidx", "ROWNUM");
		}
		
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
   		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
   		
   		if(param.get("SQL") == null || param.get("SQL").toString().equals("")) {
   			list = riskService.getAssetList(param);
		}else {
			JSONObject jsonSql = JSONObject.fromObject(param.get("SQL").toString());
			HashMap<String, Object> SQLPARM = CommonUtil.convertJsonToMap(jsonSql);

			SQLPARM.put("rows", param.get("rows"));
			SQLPARM.put("page", param.get("page"));
			SQLPARM.put("RISK_SID", param.get("RISK_SID"));
			SQLPARM.put("CLASS4_CD", param.get("CLASS4_CD"));
			SQLPARM.put("CLASS3_CD", param.get("CLASS3_CD"));
	   		list = riskService.getAssetList(SQLPARM);
		}
   		
		if (param.get("page") == null)
			param.put("page", 1);
		if (param.get("rows") == null)
			param.put("rows", 10);
		// if(param.get("page")==null) param.put("page", 1);
		// if(param.get("rows")==null) param.put("rows", 10);
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);

		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */

		return rtnMap;
	}
	
	@RequestMapping(value = "/insert.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insert(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		param.put("insert_id",user.getE_ID());
		
		return riskService.insert(param); 
	}

	@RequestMapping(value = "/update.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> update(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		param.put("update_id",user.getE_ID());
		
		int rtncnt = riskService.update(param);
		if (rtncnt == 1) {
			return param;
		} else {
			throw new ApplicationException("업데이트 중 에러가 발생하였습니다.");
		}
	}
	
	@RequestMapping(value = "/delete.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteRisk(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToMap(req);
		param.put("E_ID", user.getE_ID());
		return riskService.delete(param);
	}
	
	@RequestMapping(value = "/getFactorList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody  ArrayList<HashMap<String, Object>> getRiskAssetList(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return riskService.getFactorList(param);
	}

	@RequestMapping(value = "/insertFactor.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertRiskFactor(HttpServletRequest req) throws Exception {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		HashMap<String, Object> param = new HashMap<String, Object>();


		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("insert_id",user.getE_ID());
		param.put("INSERT_ID",user.getE_ID());
		
		System.out.println("insertRiskFactor : " + param);
		return riskService.insertFactor(param);
	}
	
	@RequestMapping(value = "/updateFactor.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> updateFactor(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("update_id",user.getE_ID());
		param.put("INSERT_ID",user.getE_ID());
		System.out.println( param);
		int rtncnt = riskService.updateFactor(param);
		//int rtncnt=1;
		if (rtncnt == 1) {
			HashMap<String, Object> result = new HashMap<String, Object>();
			result.put("STAT","OK");
			return result;
		} else {
			throw new ApplicationException("업데이트 중 에러가 발생하였습니다.");
		}
	}
	
	@RequestMapping(value = "/getGradeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	//public @ResponseBody HashMap<String, Object> getGradeList(@RequestParam HashMap<String, Object> param) throws Exception {
	public @ResponseBody ArrayList<HashMap<String, Object>> getGradeRiskList(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		return riskService.getGradeList(param);
	}

	@RequestMapping(value = "/insertGrade.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int insertRiskGrade(HttpServletRequest req) throws Exception {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		HashMap<String, Object> param = new HashMap<String, Object>();


		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("insert_id",user.getE_ID());
		param.put("INSERT_ID",user.getE_ID());
		
		System.out.println("insertRiskFactor : " + param);
		return riskService.insertGrade(param);
	}
	
	@RequestMapping(value = "/updateGrade.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> updateGrade(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		//param = CommonUtil.reqDataParsingToMap(req);
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("update_id",user.getE_ID());
		param.put("INSERT_ID",user.getE_ID());
		System.out.println( param);
		int rtncnt = riskService.updateGrade(param);
		//int rtncnt=1;
		if (rtncnt == 1) {
			HashMap<String, Object> result = new HashMap<String, Object>();
			result.put("STAT","OK");
			return result;
		} else {
			throw new ApplicationException("업데이트 중 에러가 발생하였습니다.");
		}
	}
	
	@RequestMapping(value = "/getFactorInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getFactorDetail(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return riskService.getFactorDetail(param);
	}	
	
	@RequestMapping(value = "/getGradeInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getGradeDetail(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return riskService.getGradeDetail(param);
	}
	
	@RequestMapping(value = "/saveAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int saveAsset(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("INSERT_ID",user.getE_ID());
		return riskService.saveAsset(param);
	}

	@RequestMapping(value = "/getRiskResult.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getRiskResult(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return riskService.getRiskResult(param);
	}

	@RequestMapping(value = "/deleteAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteAsset(HttpServletRequest req) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		
		return riskService.deleteAsset(param);
	}

	@RequestMapping(value = "/updateAsset.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateAsset(HttpServletRequest req) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = CommonUtil.reqDataParsingToHashMap(req);
		param.put("INSERT_ID",user.getE_ID());
		return riskService.updateAsset(param);
	}
}
