//package com.usom.web.controller.report;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
//import javax.annotation.Resource;
//import javax.inject.Inject;
//import javax.servlet.http.HttpServletRequest;
//
//import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFRow;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.springframework.stereotype.Controller;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.TransactionStatus;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.support.DefaultTransactionDefinition;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.ModelAndView;
//
//import com.framework.model.exception.ApplicationException;
//import com.framework.model.service.view.ExcelRead;
//import com.framework.model.service.view.ExcelView;
//import com.framework.model.service.view.PdfView;
//import com.framework.model.util.CommonUtil;
//import com.usom.model.config.Configuration;
//import com.usom.model.service.common.CommonService;
//import com.usom.model.service.eu.EuService;
//import com.usom.model.service.facility.FacilityCheckService;
//import com.usom.model.service.info.InfoService;
//import com.usom.model.service.report.ReportService;
//
//import jxl.Cell;
//import jxl.Sheet;
//import jxl.Workbook;
//
//
//
//@Controller
//@RequestMapping("/report")
//public class ReportController2_bak {
//	
////	private static final Logger logger = LoggerFactory.getLogger(ReportController.class);
//	@Inject
//	private ExcelView excelView;
//	
////	@Inject
////	private ExcelRead excelRead;
//	
////	@Inject
////	private PdfView pdfView;
//	
//	@Resource(name = "excelRead")
//	private ExcelRead excelRead;	
//	
//	@Resource(name = "EuService")
//	private EuService euService;	
//	
//	@Resource(name = "FacilityCheckService")
//	private FacilityCheckService facilityCheckService;	
//	
//	@Resource(name = "commonService")
//	private CommonService commonService;
//	
//	@Resource(name = "reportService")
//	private ReportService reportService;	
//	
//	@Resource(name = "InfoService")
//	private InfoService infoService;
//	
//	/**
//	 * 엑셀 다운로드 공통 2019v
//	 *0 
//	 * @param param
//	 * @return
//	 * @throws Exception
//	 */
//	// http://localhost:8080/report/getExcelDownload
//	@RequestMapping(value = "/getExcelDownload")
//	public ModelAndView getExcelDownload(HttpServletRequest request) throws Exception {
//		System.out.println("====================getExcelDownload");
//		
//		ModelAndView mav = new ModelAndView(excelView);
//		
//		HashMap<String, Object> param = new HashMap<String, Object>();
//		param = CommonUtil.getParameterMap(request);
////		param = CommonUtil.reqDataParsingToMap(request);
//		System.out.println("====================param"+param);
//		String jobType = (String) param.get("jobType");
//		String jnum = (String) param.get("jnum");
//		
//		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
//		ArrayList<HashMap<String, Object>> addComboList = new ArrayList<HashMap<String, Object>>();
//		
//		//test data
////		jobType = "011302_3";
////		param.put("s_date", "20190101");
////		param.put("e_date", "20190701");
////		param.put("jnum", "13");//유량 
////		param.put("jnum", "118");//수질
// 
//		String lg_desc = "";
//		
//		if("010502".equals(jobType)){//점검및 조사 유량조사
//
//			list = facilityCheckService.getFcResultFlowExcelList(param);
//			mav.addObject("list", list);
//			mav.addObject("filename", "010502_template.xlsx");
//			mav.addObject("refilename", ""+ jnum + "_유량조사_업로드_템플릿.xlsx");
//			mav.addObject("addComboList", null);
//			
//			lg_desc = "[메뉴 : 점검 및 조사 > 유량조사] - 유량조사_업로드_템플릿.xlsx ";
//			
//		}else if("010503".equals(jobType)){//수질조사
//			
//			list = facilityCheckService.getFcResultWqExcelList(param);
//			
//			//콤보 만들어주기
//			addComboList = comboList("010503", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "010503_template.xlsx");
//			mav.addObject("refilename", ""+ jnum + "_수질조사_업로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);
//			
//			lg_desc = "[메뉴 : 점검 및 조사 > 수질조사] - 수질조사_업로드_템플릿.xlsx ";
//			
//		}else if("010504".equals(jobType) || "010505".equals(jobType)){//육안조사, CCTV조사
//			
//			//99줄 
//			List forSize = new ArrayList();
//			for(int i=1; i<100; i++) {
//				forSize.add(i+"");	
//			}
//			param.put("forSize", forSize);
//			
//			list = facilityCheckService.getFcResultEyeExcelList(param);
//			
//			//콤보 만들어주기 엑셀 변경에 의해 콤보 엑셀에서 셋팅.
////			addComboList = comboList("010504", list);
//			
//			if("010504".equals(jobType) ){//육안조사
//				
//				mav.addObject("refilename", ""+ jnum + "_육안조사_업로드_템플릿.xlsx");	
//				lg_desc = "[메뉴 : 점검 및 조사 > 육안조사] - 육안조사_업로드_템플릿.xlsx ";
//				
//			}else if("010505".equals(jobType)){//육안조사, CCTV조사
//				
//				mav.addObject("refilename", ""+ jnum + "_CCTV조사_업로드_템플릿.xlsx");
//				lg_desc = "[메뉴 : 점검 및 조사 > CCTV조사] - CCTV조사_업로드_템플릿.xlsx ";
//			}
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "010504_template.xlsx");//템플릿 똑같음
//			mav.addObject("addComboList", addComboList);
//			
//		}else if("010506".equals(jobType)){//송연(염료)조사
//			
//			list = facilityCheckService.getFcResultSmokeExcelList(param);
//			
//			//콤보 만들어주기
//			addComboList = comboList("010506", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "010506_template.xlsx");
//			mav.addObject("refilename", ""+ jnum + "_송연(염료)조사_업로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);			
//			
//			
//			lg_desc = "[메뉴 : 점검 및 조사 > 송연(염료)조사] - 송연(염료)조사_업로드_템플릿.xlsx ";
//		}else if("010602".equals(jobType)){//청소및 준설
//			
//			//99줄 
//			List forSize = new ArrayList();
//			for(int i=1; i<100; i++) {
//				forSize.add(i+"");	
//			}
//			param.put("forSize", forSize);
//			
//			list = facilityCheckService.getFcResultCleanExcelList(param);
//			
//			//콤보 만들어주기 엑셀 변경에 의해 콤보 엑셀에서 셋팅.
//			//addComboList = comboList("010602", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "010602_template.xlsx");
//			mav.addObject("refilename", ""+ jnum + "_청소및준설_업로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);			
//			
//			
//			lg_desc = "[메뉴 : 개보수 및 준설 > 청소및준설] - 청소및준설_업로드_템플릿.xlsx ";			
//		}else if("010603".equals(jobType)){//개보수
//			
//			list = facilityCheckService.getFcResultRemodelExcelList(param);
//			
//			//콤보 만들어주기 엑셀 변경에 의해 콤보 엑셀에서 셋팅.
//			addComboList = comboList("010603", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "010603_template.xlsx");
//			mav.addObject("refilename", ""+ jnum + "_개보수_업로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);			
//			
//			
//			lg_desc = "[메뉴 : 개보수 및 준설 > 개보수] - 개보수_업로드_템플릿.xlsx ";		
//		
//		}else if("011104".equals(jobType)){//유량데이터조회
//			
//			//test
//			/*param.put("sm_pcode","11");
//			param.put("s_date","20190201");
//			param.put("e_date","20190201");*/
//			
//			list = euService.getQiMinuteList(param);
//			//param : sm_pcode, s_date, e_date
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011104_template.xlsx");
//			mav.addObject("refilename", "유량데이터조회_보고서.xlsx");
//			mav.addObject("addComboList", null);		
//			
//			
//			lg_desc = "[메뉴 : 유량모니터링 및 I/I분석] - 유량데이터조회_보고서.xlsx";
//			
//		}else if("011105".equals(jobType)){//침입수분석
//			//param : sm_pcode, s_date, e_date
//			list = euService.getItList(param);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011105_template.xlsx");// (차트X)
//			mav.addObject("refilename", "침입수분석_보고서.xlsx");// (차트X)
////			mav.addObject("filename", "011105_template.xlsm");//매크로 (차트O)
////			mav.addObject("refilename", "침입수분석_보고서.xlsm");//매크로 (차트O)
//			mav.addObject("addComboList", null);		
//			
//			
//			lg_desc = "[메뉴 : 유량모니터링 및 I/I분석] - 침입수분석_보고서.xlsm";
//			
//		}else if("011106".equals(jobType)){//유입수분석
//			// param : sm_pcode, s_date, e_date
//			System.out.println("==============================================");
//			System.out.println(param);
//			System.out.println("==============================================");
//			list = euService.getIfEventList(param);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011106_template.xlsx");// (차트X)
//			mav.addObject("refilename", "유입수분석_보고서.xlsx");// (차트X)
////			mav.addObject("filename", "011106_template.xlsm");//매크로 (차트O)
////			mav.addObject("refilename", "유입수분석_보고서.xlsm");//매크로 (차트O)
//			mav.addObject("addComboList", null);		
//			
//			lg_desc = "[메뉴 : 유량모니터링 및 I/I분석] - 유입수분석_보고서.xlsm";
//			
//		} else if ("011302_1".equals(jobType)) {//국가하수도 통계 > 하수관로시설현황 > 시설현황
//			
//			// param : e_date
//			list = infoService.getInfo1List(param);
//			
//			//콤보 만들어주기
//			addComboList = comboList("011302_1", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011302_1_template.xlsx");
//			mav.addObject("refilename", "시설현황_다운로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);
//			
//			lg_desc = "[메뉴 : 국가하수도 정보 시스템연계관리 > 하수관로시설현황 > 시설현황 - 시설현황_다운로드_템플릿.xlsx";
//			
//		} else if ("011302_2".equals(jobType)) {//국가하수도 통계 > 하수관로시설현황 > 하수처리시설별 설치현황
//			
//			// param : e_date
//			list = infoService.getInfo2List(param);
//			
//			//콤보 만들어주기
//			addComboList = comboList("011302_2", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011302_2_template.xlsx");
//			mav.addObject("refilename", "하수처리시설별_설치현황_다운로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);
//			
//			lg_desc = "[메뉴 : 국가하수도 정보 시스템연계관리 > 하수관로시설현황 > 하수처리시설별 설치현황 - 하수처리시설별_설치현황_다운로드_템플릿.xlsx";
//			
//		} else if ("011302_3".equals(jobType)) {//국가하수도 통계 > 하수관로시설현황 > 관종별 설치현황
//			
//			// param : e_date
//			/**********************
//			 * 운영에 반영시 TEST_SWL_PIPE_LM 수정
//			 */
//			long start = System.currentTimeMillis();
//			
//			
//			
//			String e_date = (String) param.get("e_date");
//			
//			if ("".equals(e_date)) {
//				e_date = "19950101";
//			}
//			e_date = e_date.substring(0, 4);
//			int i_e_date = Integer.parseInt(e_date);
//			
//			ArrayList<String> yearList = new ArrayList<String>();
//			for (int i = 1996; i <= i_e_date; i++) {
//				yearList.add(i + "");
//			}
//			param.put("yearList", yearList);
//			System.out.println("Time 1 " + (System.currentTimeMillis() - start) + "ms");
//			
//			long start_1 = System.currentTimeMillis();
//			// param : e_date
//			list = infoService.getInfo3List(param);
//			System.out.println("Time2 " + (System.currentTimeMillis() - start_1) + "ms");
//			//콤보 만들어주기
//			//addComboList = comboList("011302_3", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011302_3_template.xlsx");
//			mav.addObject("refilename", "관종별_매설년도별_현황_다운로드_템플릿.xlsx");
//			mav.addObject("addComboList", null);
//			mav.addObject("mergeType", "011302_3");
//			
//			lg_desc = "[메뉴 : 국가하수도 정보 시스템연계관리 > 하수관로시설현황 > 관종별, 매설년도별 현황 - 관종별,_매설년도별_현황__다운로드_템플릿.xlsx";
//			
//		} else if ("011302_4".equals(jobType)) {//국가하수도 통계 > 하수관로시설현황 > 관경별 설치현황
//			
//			// param : e_date
//			list = infoService.getInfo4List(param);
//			
//			//콤보 만들어주기
//			addComboList = comboList("011302_4", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011302_4_template.xlsx");
//			mav.addObject("refilename", "관경별_설치현황_다운로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);
//			
//			lg_desc = "[메뉴 : 국가하수도 정보 시스템연계관리 > 하수관로시설현황 > 관경별 설치현황 - 관경별_설치현황_다운로드_템플릿.xlsx";
//			
//		} else if ("011303_1".equals(jobType)) {//국가하수도 통계 > 하수관로유지관리현황 > 개보수현황
//			
//			// param : s_date, e_date
//			list = infoService.getRemodelList(param);
//			
//			//콤보 만들어주기
//			addComboList = comboList("011303_1", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011303_1_template.xlsx");
//			mav.addObject("refilename", "하수관로_개·보수 현황_다운로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);
//			
//			lg_desc = "[메뉴 : 국가하수도 정보 시스템연계관리 > 하수관로유지관리현황 > 하수관로 개·보수 현황 - 하수관로 개·보수 현황_다운로드_템플릿.xlsx";
//			
//		} else if ("011303_2".equals(jobType)) {//국가하수도 통계 > 하수관로유지관리현황 > 준설현황
//			
//			// param : e_date
//			list = infoService.getCleanList(param);
//			
//			//콤보 만들어주기
//			addComboList = comboList("011303_2", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011303_2_template.xlsx");
//			mav.addObject("refilename", "하수관로_준설현황_다운로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);
//			
//			lg_desc = "[메뉴 : 국가하수도 정보 시스템연계관리 > 하수관로유지관리현황 > 하수관로_준설현황 - 하수관로_준설현황_다운로드_템플릿.xlsx";
//			
//		} else if ("011304".equals(jobType)) {//국가하수도 통계 > 민원현황
//			
//			// param : s_date, e_date
//			list = infoService.getComplaintList(param);
//			
//			//콤보 만들어주기
//			addComboList = comboList("011304", list);
//			
//			mav.addObject("list", list);
//			mav.addObject("filename", "011304_template.xlsx");
//			mav.addObject("refilename", "민원현황_다운로드_템플릿.xlsx");
//			mav.addObject("addComboList", addComboList);
//			
//			lg_desc = "[메뉴 : 국가하수도 정보 시스템연계관리 > 민원현황 - 민원현황_다운로드_템플릿.xlsx";
//			
//		} else {
//			lg_desc = "엑셀 다운로드  에러가 발생하였습니다.[jobType null ] ";
//			throw new ApplicationException("엑셀 다운로드  에러가 발생하였습니다.[jobType null ] : " + param);
//		}
//		
//		//접속,출력로그
//		param.put("lg_opt", "2");//1:로그인, 2:출력, 3:에러
//		param.put("lg_desc", lg_desc);// 화면의 코드를 넘긴다. 예) 유량조사 0502
//		  
//		commonService.insertSystemLog(param);
//		
//		return mav;
//	}	
//	/*
//	 * 콤보 셋팅해주기
//	 */
//   private ArrayList<HashMap<String, Object>> comboList(String type, ArrayList<HashMap<String, Object>> list ) {
//       
//	   ArrayList<HashMap<String, Object>> addComboList = new ArrayList<HashMap<String, Object>>();
//	   
//	   try {
//		   
//		   if("010503".equals(type)) {//수질조사
//			   
//				int firstRow = 3;
//				int lastRow = firstRow;  
//				if(list != null && list.size() > 0) {
//					lastRow = firstRow + (list.size()-1);
//				}
//				
//				HashMap<String, Object> paramMap = new HashMap<String, Object>();
//				paramMap.put("gcode", "621");
//				ArrayList<HashMap<String, Object>> comboList = commonService.getCommonCodeList(paramMap);
//				
//				String[] aCategoryValues = new String[comboList.size()];
//				
//				for(int i=0; i<comboList.size(); i++)
//	    		{	
//					HashMap<String, Object> comboMap = (HashMap<String, Object>)comboList.get(i);
//	    			
//					String EXCEL_TEXT = (String)comboMap.get("EXCEL_TEXT");
//					aCategoryValues[i] = EXCEL_TEXT; 
//					
//	    		}
//				
//				HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
//				addComboMap.put("firstRow", firstRow+"");
//				addComboMap.put("lastRow", lastRow+"");
//				addComboMap.put("firstCol", "20");
//				addComboMap.put("lastCol", "20");
//				addComboMap.put("comboData", aCategoryValues);
//				addComboList.add(addComboMap);  
//				
//		   }else if("010504".equals(type)) {
//			   
//				int firstRow = 4;
//				int lastRow = firstRow;  
//				if(list != null && list.size() > 0) {
//					lastRow = firstRow + (list.size()-1);
//				}
//				
//				//M2_COMMONCODE 에서 가져오기
//				String[][] cmCode = {{"601","5","5"},{"602","10","10"},{"603","18","18"}};//관구분,배수방식,맨홀뚜껑		   
//			    HashMap<String, Object> paramMap = new HashMap<String, Object>();
//				for(int i=0; i<cmCode.length; i++) {
//					paramMap.put("gcode", cmCode[i][0]);
//					
//					ArrayList<HashMap<String, Object>> comboList = commonService.getCommonCodeList(paramMap);
//					String[] aCategoryValues = new String[comboList.size()];
//					for(int y=0; y<comboList.size(); y++)
//		    		{	
//						HashMap<String, Object> comboMap = (HashMap<String, Object>)comboList.get(y);
//		    			
//						String EXCEL_TEXT = (String)comboMap.get("EXCEL_TEXT");
//						aCategoryValues[y] = EXCEL_TEXT; 
//						
//		    		}
//					HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
//					addComboMap.put("firstRow", firstRow+"");
//					addComboMap.put("lastRow", lastRow+"");
//					addComboMap.put("firstCol", cmCode[i][1]);
//					addComboMap.put("lastCol", cmCode[i][2]);
//					addComboMap.put("comboData", aCategoryValues);
//					addComboList.add(addComboMap);
//				}
//				
//				//CMT_CODE_MA 에서 가져오기
//				String[][] cmtCode = {{"M2_JOB_EYE","JE_PKIND", "6","6"},{"M2_JOB_EYE","JE_HMKIND", "15","15"},
//						{"M2_JOB_EYE","JE_HMQM", "16","16"},{"M2_JOB_EYE","JE_HMLAD", "20","20"}
//				};//관종,	맨홀종류,맨홀재질,사다리모양
//			    HashMap<String, Object> cmtParamMap = new HashMap<String, Object>();
//				for(int i=0; i<cmtCode.length; i++) {
//					cmtParamMap.put("tbl_nam", cmtCode[i][0]);
//					cmtParamMap.put("att_nam", cmtCode[i][1]);
//					
//					ArrayList<HashMap<String, Object>> comboList = commonService.getFacCodeList(cmtParamMap);
//					String[] aCategoryValues = new String[comboList.size()];
//					for(int y=0; y<comboList.size(); y++)
//		    		{	
//						HashMap<String, Object> comboMap = (HashMap<String, Object>)comboList.get(y);
//		    			
//						String EXCEL_TEXT = (String)comboMap.get("EXCEL_TEXT");
//						aCategoryValues[y] = EXCEL_TEXT; 
//						
//		    		}
//					HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
//					addComboMap.put("firstRow", firstRow+"");
//					addComboMap.put("lastRow", lastRow+"");
//					addComboMap.put("firstCol", cmtCode[i][2]);
//					addComboMap.put("lastCol", cmtCode[i][3]);
//					addComboMap.put("comboData", aCategoryValues);
//					addComboList.add(addComboMap);
//				}	
//				
//				//코드에 없는값
//				//맨홀구분
//				HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
//				addComboMap.put("firstRow", firstRow+"");
//				addComboMap.put("lastRow", lastRow+"");
//				addComboMap.put("firstCol", 12);
//				addComboMap.put("lastCol", 12);
//				addComboMap.put("comboData", new String[]{"상류맨홀(H)","하류맨홀(L)"});
//				System.out.println("1111111==" + addComboMap.toString());
//				addComboList.add(addComboMap);
//				addComboList.get(addComboList.size()-1);
//				
//				
//				//조사방향
//				HashMap<String, Object> addComboMap2 = new HashMap<String, Object>();	
//				addComboMap2.put("firstRow", firstRow+"");
//				addComboMap2.put("lastRow", lastRow+"");
//				addComboMap2.put("firstCol", 23);
//				addComboMap2.put("lastCol", 23);
//				addComboMap2.put("comboData", new String[]{"상류->하류(HL)","하류->상류(LH)"});
//				System.out.println("222222222222==" + addComboMap2.toString());
//				addComboList.add(addComboMap2);				
//				
//				//구조적, 운영적				
//				String[] colNum = new String[]{"29","30","31","32"};
//					
//				for(int i=0; i<colNum.length; i++ ) {
//					HashMap<String, Object> addComboMap3 = new HashMap<String, Object>();
//					addComboMap3.put("firstRow", firstRow+"");
//					addComboMap3.put("lastRow", lastRow+"");
//					addComboMap3.put("firstCol", colNum[i]);
//					addComboMap3.put("lastCol", colNum[i]);
//					addComboMap3.put("comboData", new String[]{"1등급","2등급","3등급","4등급","5등급"});
//					System.out.println("3333333333==" + addComboMap3.toString());
//					addComboList.add(addComboMap3);		
//				}
//				
//				//조치구분
//				HashMap<String, Object> addComboMap4 = new HashMap<String, Object>();	
//				addComboMap4.put("firstRow", firstRow+"");
//				addComboMap4.put("lastRow", lastRow+"");
//				addComboMap4.put("firstCol", 33);
//				addComboMap4.put("lastCol", 33);
//				addComboMap4.put("comboData", new String[]{"이상없음(1)","청소/준설(2)","개보수(3)","기타(9)"});
//				System.out.println("44444444444==" + addComboMap4.toString());
//				addComboList.add(addComboMap4);		
//				
//			   
//		   }else if("010506".equals(type)) {
//			   
//				int firstRow = 3;
//				int lastRow = firstRow;  
//				if(list != null && list.size() > 0) {
//					lastRow = firstRow + (list.size()-1);
//				}
//				
//				//체크박스
//				for(int i=4; i<18; i++) {
//					if(i!=10) {
//						HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
//						addComboMap.put("firstRow", firstRow+"");
//						addComboMap.put("lastRow", lastRow+"");
//						addComboMap.put("firstCol", i+"");
//						addComboMap.put("lastCol", i+"");
//						addComboMap.put("comboData", new String[]{"이상있음(1)","이상없음(0)"});
//						addComboList.add(addComboMap);	
//					}
//				}
//				//조치구분
//				HashMap<String, Object> addComboMap4 = new HashMap<String, Object>();	
//				addComboMap4.put("firstRow", firstRow+"");
//				addComboMap4.put("lastRow", lastRow+"");
//				addComboMap4.put("firstCol", 20);
//				addComboMap4.put("lastCol", 20);
//				addComboMap4.put("comboData", new String[]{"이상없음(1)","청소/준설(2)","개보수(3)","기타(9)"});
//				addComboList.add(addComboMap4);	
//				
//		   }else if("010603".equals(type)) {//개보수
//			   
//			   int firstRow = 2; 
//				int lastRow = firstRow;  
//				if(list != null && list.size() > 0) {
//					lastRow = firstRow + (list.size()-1);
//				}
//				
//				//M2_COMMONCODE 에서 가져오기
//				String[][] cmCode = {{"606","4","4"},{"607","5","5"}};//개보수공법, 공법종류		   
//			    HashMap<String, Object> paramMap = new HashMap<String, Object>();
//				for(int i=0; i<cmCode.length; i++) {
//					paramMap.put("gcode", cmCode[i][0]);
//					
//					ArrayList<HashMap<String, Object>> comboList = commonService.getCommonCodeList(paramMap);
//					String[] aCategoryValues = new String[comboList.size()];
//					for(int y=0; y<comboList.size(); y++)
//		    		{	
//						HashMap<String, Object> comboMap = (HashMap<String, Object>)comboList.get(y);
//		    			
//						String EXCEL_TEXT = (String)comboMap.get("EXCEL_TEXT");
//						aCategoryValues[y] = EXCEL_TEXT; 
//						
//		    		}
//					HashMap<String, Object> addComboMap = new HashMap<String, Object>();				
//					addComboMap.put("firstRow", firstRow+"");
//					addComboMap.put("lastRow", lastRow+"");
//					addComboMap.put("firstCol", cmCode[i][1]);
//					addComboMap.put("lastCol", cmCode[i][2]);
//					addComboMap.put("comboData", aCategoryValues);
//					addComboList.add(addComboMap);
//				}
//		   }
//			
//        } catch (Exception e) {
//        	e.getMessage();
//        	e.printStackTrace();
//        }
//	   return addComboList;
//    }	
//
//	
//	/**
//	 * 엑셀 업로드 공통 2019v
//	 * 
//	 * @param param
//	 * @return
//	 * @throws Exception
//	 */	
//	//@RequestMapping(value = "/getExcelRead")
//	@RequestMapping(value = "/getExcelRead.json", method = RequestMethod.POST, headers = "Accept=application/json")
//	public  @ResponseBody ModelAndView getExcelRead(HttpServletRequest request) throws Exception {
//
//    	int rtn = 0;//실패
//    	
//    	ModelAndView mav = new ModelAndView();
//		mav.addObject("errMessage", "");
//
//
//    	
//    	
//		HashMap<String, Object> param = new HashMap<String, Object>();
////		param = CommonUtil.getParameterMap(request);
//		param = CommonUtil.reqDataParsingToMap(request);
//		
//		String jobType = (String) param.get("jobtype");
////		int filenumber = (Integer) param.get("f_num");
//		int filenumber = Integer.parseInt(param.get("f_num").toString());
////		int filenumber = 9; //*********************************************************************************************************** test data
////		param.put("jnum", "118");//*********************************************************************************************************** test data
//		String gJnum = (String) param.get("jnum");
//		String login_user_id = (String) param.get("login_user_id");
//		
//    	//f_num으로 파일경로 가져오기
//    	HashMap<String, Object> fileMap = commonService.getFileInfo(filenumber);
//    	String filePath = (String)fileMap.get("F_PATH");//F_NAME
////	    filePath = "C:/USOM/usom/framework/framework-web/src/main/webapp/resources/excel/template/010603_UP.xlsx"; //********************************************************* test data
//    	
//    	//filePath = request.getSession().getServletContext().getRealPath(filePath);
//    	String SERVER_PATH2 = Configuration.WEB_ROOT_PATH;
//		filePath = SERVER_PATH2 + filePath;
//		
//		System.out.println("filePath" + filePath);
//    	
//	    
//		if(filePath == "") {
//			throw new ApplicationException("에러가 발생하였습니다. filePath null: " + param);
//		}
//		//(test data)
////		gJnum = "26"; //*********************************************************************************************************** test data
////		jobType = "010603"; //*********************************************************************************************************** test data
////		param.put("jobType", "010603"); //*********************************************************************************************************** test data
//		
//		
//		if("010502".equals(jobType)){//점검및 조사 유량조사
//
//			//변수정의
//			param.put("filePath", filePath); 
//			int targetCellCnt = 16;
//			param.put("targetCellCnt", targetCellCnt);//사용자가 올린 엑셀 컬럼수
//			param.put("startRowData", 2);// 실제 data가 시작되는 row
//			
//			//1. 사용자가 올린 엑셀을 읽어 list로 받는다.
//			ArrayList<HashMap<String, Object>> excelList = excelRead.ExcelReadDocument(param);// 사용자가 올린 엑셀을 읽어 list로 받는다.
//			
//			//2. 읽어온 엑셀을 순서대로 컬럼명 변경.
//			//String[] columnNames = new String[]{"ftr_idn", "ftr_idn_type", "jf_sdate","jf_edate","jf_sq","jf_sh","jf_sv","jf_ssdq","jf_srdq","jf_avq","jf_iiw","jf_iiwrate","jf_acrain","jf_iif","jf_iifn","jf_note"};
//			String[] columnNames = new String[]{"cnt_num", "ftr_idn_type", "jf_sdate","jf_edate","jf_sq","jf_sh","jf_sv","jf_ssdq","jf_srdq","jf_avq","jf_iiw","jf_iiwrate","jf_acrain","jf_iif","jf_iifn","jf_note"};
//			ArrayList<HashMap<String, Object>> reExcelList = columnNameChange(excelList, columnNames, targetCellCnt, login_user_id, "" );
//
//			/*** 공사번호 추가 ****/
//			ArrayList<HashMap<String, Object>> cntCheckList = reportService.getCntCheckList(reExcelList);
//
//			StringBuffer chkMsg = new StringBuffer();
//			StringBuffer chkNullMsg = new StringBuffer();
//			StringBuffer chkDupMsg = new StringBuffer();
//			
//			if(cntCheckList.size() > 0) {
//			
//				for(int i=0; i<cntCheckList.size(); i++)
//				{	
//					HashMap<String, Object> cntMap = (HashMap<String, Object>)cntCheckList.get(i);
//					String CNT_NUM = (String)cntMap.get("CNT_NUM");
//					String CNT = (String)cntMap.get("CNT");
//					
//					if("0".equals(CNT)) {//NULL
//						chkNullMsg.append(CNT_NUM + ", ");
//					}else if("1".equals(CNT)) {//정상						
//					}else{//중복
//						chkDupMsg.append(CNT_NUM + ", ");
//					}
//				}
//				if(chkNullMsg.length() > 0) {
//					chkMsg.append("NULL : ").append(chkNullMsg);	
//				}
//				if(chkDupMsg.length() > 0) {
//					chkMsg.append("Duplication : ").append(chkDupMsg);	
//				}
//			}
//			/*** 공사번호 추가 ****/
//			System.out.println("   chkMsg     " + chkMsg.toString());
//			if(chkMsg.length() > 0) {
//				mav.addObject("chkMessage", chkMsg);	
//			}else {
//				rtn = reportService.insertExcelRead(param, reExcelList);	
//			}
//			
//		}else if("010503".equals(jobType)){//수질조사
//			
//			//변수정의
//			param.put("filePath", filePath); 
//			int targetCellCnt = 27;
//			param.put("targetCellCnt", targetCellCnt);
//			param.put("startRowData", 3);
//			
//			//1. 사용자가 올린 엑셀을 읽어 list로 받는다.
//			ArrayList<HashMap<String, Object>> excelList = excelRead.ExcelReadDocument(param);
//			
//			//2. 읽어온 엑셀을 순서대로 컬럼명 변경.
//			String[] columnNames = new String[]{"ftr_idn", "ftr_idn_type","jq_ainst", "jq_sdate","jq_edate",
//					"jq_sbod","jq_xbod","jq_nbod","jq_scod","jq_xcod","jq_ncod","jq_sss","jq_xss","jq_nss","jq_stn","jq_xtn","jq_ntn","jq_stp","jq_xtp","jq_ntp","jq_pftridn","jq_ibod", "jq_icod", "jq_iss", "jq_itn", "jq_itp","jq_note"};
//			String columnNamesToCode = "jq_pftridn";			
//			
//			ArrayList<HashMap<String, Object>> reExcelList = columnNameChange(excelList, columnNames, targetCellCnt, login_user_id, columnNamesToCode );
//			
//			rtn = reportService.insertExcelRead(param, reExcelList);
//			
//			
//		}else if("010504".equals(jobType) || "010505".equals(jobType)){//육안조사, CCTV조사
//			
//			//변수정의
//			param.put("filePath", filePath); 
//			int targetCellCnt = 38;
//			param.put("targetCellCnt", targetCellCnt);//사용자가 올린 엑셀 컬럼수
//			param.put("startRowData", 4);// 실제 data가 시작되는 row
//			
//			//1. 사용자가 올린 엑셀을 읽어 list로 받는다.
//			ArrayList<HashMap<String, Object>> excelList = excelRead.ExcelReadDocument(param);// 사용자가 올린 엑셀을 읽어 list로 받는다.
//			
//			//2. 읽어온 엑셀을 순서대로 컬럼명 변경.
//			String[] columnNames = new String[]{"jg_gnum", "ftr_idn", "ftr_idn_type", "je_sdate","je_edate",
//					"je_ptype","je_pkind","je_pdm","je_pwidth","je_pheight","je_pwsys","je_pleng",//관구분,관관종, 원형관, 사각형관가로,사각형관세로,배수방식, 연장 
//					
//					"manh_type","manh_mlat","manh_mlng","manh_mkind","manh_mqm","manh_mdepth","manh_mcover","manh_msize","manh_mlad",//맨홀구분, 위도, 경도, 맨홀종류, 맨홀재질, 맨홀깊이, 맨홀뚜껑, 맨홀크기,사다리모양
//					
//					"je_pgdist","je_pndist","way_type","je_ocpt",//총주행거리, 미주행거리, 조사방향, 이상발생지점
//					"je_nrs","je_nrsdesc","je_pg_st","je_pg_op","je_hg_st",//미주행사유, 미주행사유설명, 하수관구조적, 하수관운영적,상류맨홀구조적					
//					"je_hg_op","je_lg_st","je_lg_op","je_mopt1","je_mopt2",//상류맨홀운영적,하류맨홀구조적, 하류맨홀운영적, 조치구분이상없음, 조치구분 청소준설
//					"je_mopt3","je_mopt9","je_mdesc"//개보수, 기타, 조치내용
//					};
//			//맨홀구분 : manh_tpe(H 상류, L 하류), 조사방향 : way_type(고정) 			 * 엑셀의 노란색부분만 데이터 받음, 첫줄 관, 두세 맨홀
//
//			String columnNamesToCode = "@jg_gnum@je_ptype@je_pkind@je_pwsys@manh_type@manh_mkind@manh_mqm@manh_mcover@manh_mlad@je_hg_st@je_hg_op@je_lg_st@je_lg_op@je_mopt1@je_mopt2@je_mopt3@je_mopt9";// 코드로 바꿀 항목
//			
//			ArrayList<HashMap<String, Object>> reExcelList = columnNameChange(excelList, columnNames, targetCellCnt, login_user_id, columnNamesToCode );
//
//			//2-1. 현재 대상시설물을 가져온다.
//			ArrayList<HashMap<String, Object>> orgList = facilityCheckService.getFcResultEyeExcelList_CHK(param);
//			
//			
//			//3. 3줄씩 한묶음으로 변형.
//			ArrayList<HashMap<String, Object>> modifylist = new ArrayList<HashMap<String, Object>>();// 3줄 한줄로.
//			HashMap<String, Object> modifyMap = new HashMap<String, Object>();// 3줄 한줄로.
//			int roopCnt = 1;
//			String chk_jg_gnum = "";
//			
//			for(int i=0; i<reExcelList.size(); i++)
//    		{
//				
//				HashMap<String, Object> reExcelListMap = (HashMap<String, Object>)reExcelList.get(i);
//				
//				String ftr_idn = String.valueOf(reExcelListMap.get("ftr_idn"));
//				
//				
//				
//				
//				if(!"".equals(CommonUtil.replaceNullString(ftr_idn))) {
//					
//					if(roopCnt == 1) {//1번째줄
//						chk_jg_gnum = String.valueOf(reExcelListMap.get("jg_gnum"));
//					}
//					
//					boolean chkFtrIdn = findFtrIdn_Group(orgList, chk_jg_gnum , ftr_idn);
//					if(!chkFtrIdn) {
//						throw new ApplicationException("ftr_idn null findFtrIdn_Group 에러가 발생하였습니다. : " + chk_jg_gnum +"-"+ftr_idn);
//					}
//					
//					
//					if(roopCnt == 1) {//1번째줄
//						modifyMap.put("jg_gnum", String.valueOf(reExcelListMap.get("jg_gnum")));//jg_num
//						modifyMap.put("je_pftridn", String.valueOf(reExcelListMap.get("ftr_idn")).replace(".000",""));//관번호
//						modifyMap.put("je_sdate", String.valueOf(reExcelListMap.get("je_sdate")));//시작일자
//						modifyMap.put("je_edate", String.valueOf(reExcelListMap.get("je_edate")));//종료일자
//						//관
//						modifyMap.put("je_ptype", String.valueOf(reExcelListMap.get("je_ptype")));//구분
//						modifyMap.put("je_pkind", String.valueOf(reExcelListMap.get("je_pkind")));//관종
//						modifyMap.put("je_pdm", String.valueOf(reExcelListMap.get("je_pdm")).replace(",",""));//원형관
//						modifyMap.put("je_pwidth", String.valueOf(reExcelListMap.get("je_pwidth")).replace(",",""));//사각형관가로
//						modifyMap.put("je_pheight", String.valueOf(reExcelListMap.get("je_pheight")).replace(",",""));//사각형관세로
//						modifyMap.put("je_pwsys", String.valueOf(reExcelListMap.get("je_pwsys")));//배수방식
//						modifyMap.put("je_pleng", String.valueOf(reExcelListMap.get("je_pleng")).replace(",",""));//연장
//						//조사정보
//						modifyMap.put("je_pgdist", String.valueOf(reExcelListMap.get("je_pgdist")).replace(",",""));//총주행거리
//						modifyMap.put("je_pndist", String.valueOf(reExcelListMap.get("je_pndist")).replace(",",""));//미주행거리
//						modifyMap.put("je_ocpt1", String.valueOf(reExcelListMap.get("je_ocpt")).replace(",",""));//상류 이상발생지점
//						modifyMap.put("je_nrs1", String.valueOf(reExcelListMap.get("je_nrs")));//상류 미주행사유
//						modifyMap.put("je_nrsdesc", String.valueOf(reExcelListMap.get("je_nrsdesc")));//미주행사유설명
//						//조사결과
//						modifyMap.put("je_pg_st", String.valueOf(reExcelListMap.get("je_pg_st")).replace(".000","").replace(",",""));//하수관로 상태등급 구조적
//						modifyMap.put("je_pg_op", String.valueOf(reExcelListMap.get("je_pg_op")).replace(".000","").replace(",",""));//하수관로 상태등급 운영적
//						//조치내용
//						modifyMap.put("je_mopt1", String.valueOf(reExcelListMap.get("je_mopt1")));//이상없음
//						modifyMap.put("je_mopt2", String.valueOf(reExcelListMap.get("je_mopt2")));//청소준설
//						modifyMap.put("je_mopt3", String.valueOf(reExcelListMap.get("je_mopt3")));//개보수
//						modifyMap.put("je_mopt4", "");//예비1
//						modifyMap.put("je_mopt5", "");//예비2
//						modifyMap.put("je_mopt9", String.valueOf(reExcelListMap.get("je_mopt9")));//기타
//						modifyMap.put("je_mdesc", String.valueOf(reExcelListMap.get("je_mdesc")));//조치내용
//						roopCnt++;
//					}else if(roopCnt == 2) {//2번째줄
//						
//						String manh_type = (String.valueOf(reExcelListMap.get("manh_type"))).toLowerCase(); // 맨홀 타입
//						//맨홀
//						modifyMap.put("je_" + manh_type + "mftridn", String.valueOf(reExcelListMap.get("ftr_idn")).replace(".000",""));//맨홀번호
//						modifyMap.put("je_" + manh_type + "mlat", String.valueOf(reExcelListMap.get("manh_mlat")).replace(",",""));//위도
//						modifyMap.put("je_" + manh_type + "mlng", String.valueOf(reExcelListMap.get("manh_mlng")).replace(",",""));//경도
//						modifyMap.put("je_" + manh_type + "mkind", String.valueOf(reExcelListMap.get("manh_mkind")));//맨홀종류
//						modifyMap.put("je_" + manh_type + "mqm", String.valueOf(reExcelListMap.get("manh_mqm")));//맨홀재질
//						modifyMap.put("je_" + manh_type + "mdepth", String.valueOf(reExcelListMap.get("manh_mdepth")).replace(",",""));//맨홀깊이
//						modifyMap.put("je_" + manh_type + "mcover", String.valueOf(reExcelListMap.get("manh_mcover")));//맨홀뚜껑
//						modifyMap.put("je_" + manh_type + "msize", String.valueOf(reExcelListMap.get("manh_msize")).replace(",",""));//맨홀크기
//						modifyMap.put("je_" + manh_type + "mlad", String.valueOf(reExcelListMap.get("manh_mlad")));//사다리모양
//						
//						//조사정보
//						modifyMap.put("je_ocpt2", String.valueOf(reExcelListMap.get("je_ocpt")).replace(",",""));//하류 이상발생지점
//						modifyMap.put("je_nrs2", String.valueOf(reExcelListMap.get("je_nrs")));//하류 미주행사유
//						
//						//조사결과
//						modifyMap.put("je_hg_st", String.valueOf(reExcelListMap.get("je_hg_st")));//상류맨홀구조적
//						modifyMap.put("je_hg_op", String.valueOf(reExcelListMap.get("je_hg_op")));//상류맨홀운영적
//						
//						
//						roopCnt++;
//					}else if(roopCnt == 3) {//2번째줄
//						
//						String manh_type = (String.valueOf(reExcelListMap.get("manh_type"))).toLowerCase(); // 맨홀 타입
//						//맨홀
//						modifyMap.put("je_" + manh_type + "mftridn", String.valueOf(reExcelListMap.get("ftr_idn")).replace(".000",""));//맨홀번호
//						modifyMap.put("je_" + manh_type + "mlat", String.valueOf(reExcelListMap.get("manh_mlat")).replace(",",""));//위도
//						modifyMap.put("je_" + manh_type + "mlng", String.valueOf(reExcelListMap.get("manh_mlng")).replace(",",""));//경도
//						modifyMap.put("je_" + manh_type + "mkind", String.valueOf(reExcelListMap.get("manh_mkind")));//맨홀종류
//						modifyMap.put("je_" + manh_type + "mqm", String.valueOf(reExcelListMap.get("manh_mqm")));//맨홀재질
//						modifyMap.put("je_" + manh_type + "mdepth", String.valueOf(reExcelListMap.get("manh_mdepth")).replace(",",""));//맨홀깊이
//						modifyMap.put("je_" + manh_type + "mcover", String.valueOf(reExcelListMap.get("manh_mcover")));//맨홀뚜껑
//						modifyMap.put("je_" + manh_type + "msize", String.valueOf(reExcelListMap.get("manh_msize")).replace(",",""));//맨홀크기
//						modifyMap.put("je_" + manh_type + "mlad", String.valueOf(reExcelListMap.get("manh_mlad")));//사다리모양
//						
//						//조사결과
//						modifyMap.put("je_lg_st", String.valueOf(reExcelListMap.get("je_lg_st")));//하류맨홀구조적
//						modifyMap.put("je_lg_op", String.valueOf(reExcelListMap.get("je_lg_op")));//하류맨홀운영적
//						
//						modifyMap.put("login_user_id",login_user_id);
//						
//						modifylist.add(modifyMap);
//						modifyMap = new HashMap<String, Object>();
//						roopCnt = 1;//초기화
//					}
//					
//				}else {
////					throw new ApplicationException("ftr_idn null 에러가 발생하였습니다. : " + reExcelListMap);
//				}
//				
//    		}
//
//			
//			
//			rtn = reportService.insertExcelRead(param, modifylist);			
//			
//			
//		}else if("010506".equals(jobType)){//송연(염료)조사
//			
//			//변수정의
//			param.put("filePath", filePath); 
//			int targetCellCnt = 25;
//			param.put("targetCellCnt", targetCellCnt);
//			param.put("startRowData", 3);
//			
//			//1. 사용자가 올린 엑셀을 읽어 list로 받는다.
//			ArrayList<HashMap<String, Object>> excelList = excelRead.ExcelReadDocument(param);
//			
//			//2. 읽어온 엑셀을 순서대로 컬럼명 변경."jg_gnum", 
//			String[] columnNames = new String[]{"ftr_idn", "ftr_idn_type","js_sdate","js_edate","js_st1",
//					"js_st2", "js_st3","js_st4","js_st5","js_st6",
//					"js_st7_desc","js_pos1","js_pos2","js_pos3","js_pos4",
//					"js_pos5","js_pos6","js_pos7","js_pos8_desc","js_note",
//					"js_mopt1","js_mopt2","js_mopt3","js_mopt9", "js_mdesc"
//					};
//			
//			String columnNamesToCode = "@js_st1@js_st2@js_st3@js_st4@js_st5@js_st6@js_pos1@js_pos2@js_pos3@js_pos4@js_pos5@js_pos6@js_pos7@js_mopt1@js_mopt2@js_mopt3@js_mopt9";// 코드로 바꿀 항목
//			
//			ArrayList<HashMap<String, Object>> reExcelList = columnNameChange(excelList, columnNames, targetCellCnt, login_user_id, columnNamesToCode );
//			
//			
//			rtn = reportService.insertExcelRead(param, reExcelList);
//			
//		}else if("010602".equals(jobType)){//청소및 준설
//			
//			//변수정의
//			param.put("filePath", filePath); 
//			int targetCellCnt = 13;
//			param.put("targetCellCnt", targetCellCnt);//사용자가 올린 엑셀 컬럼수
//			param.put("startRowData", 3);// 실제 data가 시작되는 row -1
//			
//			//1. 사용자가 올린 엑셀을 읽어 list로 받는다.
//			ArrayList<HashMap<String, Object>> excelList = excelRead.ExcelReadDocument(param);// 사용자가 올린 엑셀을 읽어 list로 받는다.
//			
//			//2. 읽어온 엑셀을 순서대로 컬럼명 변경.
//			String[] columnNames = new String[]{"jg_gnum", "ftr_idn", "ftr_idn_type", "jc_sdate","jc_edate",
//					"jc_volume","jc_worker","jc_cost","jc_dmopt","jc_dmethod",//준설량, 준설업체명, 준설비용, 준설토사처리방법, 준설토사처리방법설명
//					"jc_dvolume","jc_dcost","jc_note"//처리량, 준설토사처리비용, 특이사항
//					};
//
//			String columnNamesToCode = "@jg_gnum@jc_dmopt@";// 코드로 바꿀 항목
//			
//			ArrayList<HashMap<String, Object>> reExcelList = columnNameChange(excelList, columnNames, targetCellCnt, login_user_id, columnNamesToCode );
//
//			//2-1. 현재 대상시설물을 가져온다.
//			ArrayList<HashMap<String, Object>> orgList = facilityCheckService.getFcResultCleanExcelList_CHK(param);
//			
//			
//			//3. 3줄씩 한묶음으로 변형.
//			ArrayList<HashMap<String, Object>> modifylist = new ArrayList<HashMap<String, Object>>();// 3줄 한줄로.
//			HashMap<String, Object> modifyMap = new HashMap<String, Object>();// 3줄 한줄로.
//			int roopCnt = 1;
//			String chk_jg_gnum = "";
//			
//			for(int i=0; i<reExcelList.size(); i++)
//    		{
//				
//				HashMap<String, Object> reExcelListMap = (HashMap<String, Object>)reExcelList.get(i);
//				
//				String ftr_idn = String.valueOf(reExcelListMap.get("ftr_idn"));
//				
//				
//				
//				
//				if(!"".equals(CommonUtil.replaceNullString(ftr_idn))) {
//					
//					if(roopCnt == 1) {//1번째줄
//						chk_jg_gnum = String.valueOf(reExcelListMap.get("jg_gnum"));
//					}
//					
//					boolean chkFtrIdn = findFtrIdn_Group(orgList, chk_jg_gnum , ftr_idn);
//					if(!chkFtrIdn) {
//						throw new ApplicationException("ftr_idn null findFtrIdn_Group 에러가 발생하였습니다. : " + chk_jg_gnum +"-"+ftr_idn);
//					}
//					
//					
//					if(roopCnt == 1) {//1번째줄
//						modifyMap.put("jg_gnum", String.valueOf(reExcelListMap.get("jg_gnum")));//jg_num
//						//modifyMap.put("je_pftridn", String.valueOf(reExcelListMap.get("ftr_idn")).replace(".000",""));//관번호
//						modifyMap.put("jc_sdate", String.valueOf(reExcelListMap.get("jc_sdate")));//시작일자
//						modifyMap.put("jc_edate", String.valueOf(reExcelListMap.get("jc_edate")));//종료일자
//
//						modifyMap.put("jc_volume", String.valueOf(reExcelListMap.get("jc_volume")).replace(",",""));//준설량
//						modifyMap.put("jc_worker", String.valueOf(reExcelListMap.get("jc_worker")));//준설업체명
//						modifyMap.put("jc_cost", String.valueOf(reExcelListMap.get("jc_cost")).replace(",",""));//준설비용
//						modifyMap.put("jc_dmopt", String.valueOf(reExcelListMap.get("jc_dmopt")).replace(".000","").replace(",",""));//준설토사처리방법
//						modifyMap.put("jc_dmethod", String.valueOf(reExcelListMap.get("jc_dmethod")));//준설토사처리방법설명
//						
//						modifyMap.put("jc_dvolume", String.valueOf(reExcelListMap.get("jc_dvolume")).replace(",",""));//처리량
//						modifyMap.put("jc_dcost", String.valueOf(reExcelListMap.get("jc_dcost")).replace(",",""));//준설토사처리비용
//						modifyMap.put("jc_note", String.valueOf(reExcelListMap.get("jc_note")));//특이사항
//						
//						modifyMap.put("login_user_id",login_user_id);
//						
//						modifylist.add(modifyMap);
//						modifyMap = new HashMap<String, Object>();
//						
//						roopCnt++;
//					}else if(roopCnt == 2) {//2번째줄
//						roopCnt++;
//					}else if(roopCnt == 3) {//3번째줄
//						roopCnt = 1;//초기화
//					}
//					
//				}else {
////					throw new ApplicationException("ftr_idn null 에러가 발생하였습니다. : " + reExcelListMap);
//				}
//				
//    		}
//
//			
//			
//			rtn = reportService.insertExcelRead(param, modifylist);
//			
//			
//		}else if("010603".equals(jobType)){//개보수
//
//				//변수정의
//				param.put("filePath", filePath); 
//				int targetCellCnt = 12;
//				param.put("targetCellCnt", targetCellCnt);//사용자가 올린 엑셀 컬럼수
//				param.put("startRowData", 2);// 실제 data가 시작되는 row
//				
//				//1. 사용자가 올린 엑셀을 읽어 list로 받는다.
//				ArrayList<HashMap<String, Object>> excelList = excelRead.ExcelReadDocument(param);// 사용자가 올린 엑셀을 읽어 list로 받는다.
//				
//				//2. 읽어온 엑셀을 순서대로 컬럼명 변경.
//				String[] columnNames = new String[]{"ftr_idn", "ftr_idn_type", "jr_sdate","jr_edate",
//						"jr_method","jr_type","jr_cnt","jr_leng","jr_worker",//개보수공법, 공법종류, 개보수개소, 개보수연장, 시공자
//						"jr_phone","jr_cost","jr_note"//연락처, 소요사업지, 특이사항
//				};
//				
//				String columnNamesToCode = "@jr_method@jr_type@";// 코드로 바꿀 항목
//				
//				ArrayList<HashMap<String, Object>> reExcelList = columnNameChange(excelList, columnNames, targetCellCnt, login_user_id, columnNamesToCode );
//				
//
//				
//				rtn = reportService.insertExcelRead(param, reExcelList);			
//			
//		}else {
//			throw new ApplicationException("엑셀 업로드  에러가 발생하였습니다.[jobType null ] : " + param);
//		}
//    	
//		System.out.println("step 성공성공성공성공성공성공성공: ");
//        //return rtn;
//		return mav;
//		
//    }
//	private boolean findFtrIdn_Group(ArrayList<HashMap<String, Object>> orgList, String chk_jg_gnum, String excelFtrIdn) {
//		 
//		boolean isUse = false;
//		
//		double D_excelFtrIdn = Double.parseDouble(excelFtrIdn);
//		excelFtrIdn = String.valueOf((Math.round(D_excelFtrIdn)));
//		excelFtrIdn = chk_jg_gnum + "-" + excelFtrIdn;
//		
//		if(orgList != null && orgList.size()>0) {
//			for(int i=0; i<orgList.size(); i++)
//			{	
//				HashMap<String, Object> orgMap = (HashMap<String, Object>)orgList.get(i);
//				
//				String FTR_IDN = (String)orgMap.get("GNUM_IDN_CHK");
//				//System.out.println("step FTR_IDN: " +FTR_IDN+"/"+excelFtrIdn);
//				if(excelFtrIdn.equals(FTR_IDN)) {
//					isUse = true;	
//				}
//				
//			}		
//		}
//		return isUse;
//	 }
//	
//   private ArrayList<HashMap<String, Object>> columnNameChange(ArrayList<HashMap<String, Object>> excelList, String[] columnNames, int targetCellCnt, String login_user_id, String columnNamesToCode ) throws Exception {
//        
//	   ArrayList<HashMap<String, Object>> reExcelList = new ArrayList<HashMap<String, Object>>();
//	   
//	   try {
//		   
//        	for(int i=0; i<excelList.size(); i++)
//    		{	
//				HashMap<String, Object> excelListMap = (HashMap<String, Object>)excelList.get(i);
//				HashMap<String, Object> reExcelListMap = (HashMap<String, Object>)excelList.get(i);
//				
//				for(int y=0; y<targetCellCnt; y++)
//	    		{	
//	       			String value = String.valueOf(excelListMap.get("CELL_" + y));
////	       		 System.out.print("CELL_ + y          " + y + "  | " + value +"\n ");
//	       			
////	       			System.out.print("columnNames[y]          " + columnNames[y] + "  columnNamesToCode " + columnNamesToCode +"\n ");
//	       			
//	       			//if(columnNames[y].equals(columnNamesToCode)) {//코드로 컬럼이면 () 안에 값을 가져온다. 울진하수처리시설(11)
//	       			if(columnNamesToCode.indexOf(columnNames[y]) > 0) {
//	       				
//		       			 if(!"".equals(value) && !"@@@".equals(value)) {
//		       				 int sPoint = value.indexOf("(");
//		       				 int ePoint = value.indexOf(")");
//		       				 
////		       				 System.out.println("sPoint=" + sPoint);
////		       				 System.out.println("ePoint=" + ePoint);
////		       				System.out.println("value=[" + value+"]");
////		       				 System.out.println("target=" + value.substring((sPoint+1),ePoint));
//		       				reExcelListMap.put(columnNames[y], value.substring((sPoint+1),ePoint));		 
//		       			 }else {
//		       				reExcelListMap.put(columnNames[y], "");
//		       			 }
//	       			
//	       				
//	       			}else {
//	       				reExcelListMap.put(columnNames[y], value);	
//	       			}
//	       			
//	       			
//	    		}
////				 System.out.print("\n                   ");
////                 System.out.print("\n                   ");
////                 System.out.print("\n                   ");
////                 System.out.print("\n                   ");
//				reExcelListMap.put("login_user_id",login_user_id);
//				
//				
////				System.out.print("reExcelListMap " + reExcelListMap +"\n ");
//				reExcelList.add(reExcelListMap);
//    		}
//        	
//        	
//        } catch (Exception e) {
//        	e.getMessage();
//        	e.printStackTrace();
//        }
//	   return reExcelList;
//    }	
//   
//
//	/**
//	 * 엑셀 읽어와서 list로
//	 * 
//	 * @param param
//	 * @return
//	 * @throws Exception
//	 */	
//	@RequestMapping(value = "/getExcelReadToList.json", method = RequestMethod.POST, headers = "Accept=application/json")
//	public @ResponseBody ArrayList<HashMap<String, Object>> getExcelReadToList(HttpServletRequest req) throws Exception {
//		
//		ArrayList<HashMap<String, Object>> reExcelList = new ArrayList<HashMap<String, Object>>();
//		ArrayList<HashMap<String, Object>> modifyExcelList = new ArrayList<HashMap<String, Object>>();
//		HashMap<String, Object> param = new HashMap<String, Object>();
//		param = CommonUtil.reqDataParsingToMap(req);
//		
//		
//		//param.put("jobtype", "off1");//test 1
//		//param.put("f_num", "12");//test 2 
//		
//		String jobtype = (String) param.get("jobtype");
//		String login_user_id = (String) param.get("login_user_id");
//		int filenumber = Integer.parseInt(String.valueOf(param.get("f_num")));
//		
//		HashMap<String, Object> fileMap = commonService.getFileInfo(filenumber);
//		String filePath = (String)fileMap.get("F_PATH");//F_NAME
//		
//		//filePath = req.getSession().getServletContext().getRealPath(filePath);
//		String SERVER_PATH2 = Configuration.WEB_ROOT_PATH;
//		filePath = SERVER_PATH2 + filePath;
//		
//		System.out.println("filePath" + filePath);
//		
//		
//		
//		//filePath = "D:/PJ/OMAS/workspace/omas/framework/framework-web/src/main/webapp/resources/upload/file/excel/off1.xlsx";//test 3
//
//	    
//		if(filePath == "") {
//			throw new ApplicationException("에러가 발생하였습니다. filePath null: " + param);
//		}
//		
//		if("impfc".equals(jobtype)){//수리계산
//	
//			//변수정의
//			param.put("filePath", filePath); 
//			int targetCellCnt = 3;
//			param.put("targetCellCnt", targetCellCnt);//사용자가 올린 엑셀 컬럼수
//			param.put("startRowData", 2);// 실제 data가 시작되는 row
//			
//			//1. 사용자가 올린 엑셀을 읽어 list로 받는다.
//			ArrayList<HashMap<String, Object>> excelList = excelRead.ExcelReadDocument(param);// 사용자가 올린 엑셀을 읽어 list로 받는다.
//			
//			//2. 읽어온 엑셀을 순서대로 컬럼명 변경.
//			String[] columnNames = new String[]{"ftr_cde", "ftr_idn", "sm_v"};
//			reExcelList = columnNameChange(excelList, columnNames, targetCellCnt, login_user_id, "" );
//			
//		}else if("off1".equals(jobtype)){// I/I분석 > 오프라인 데이터 등록 > 유량 데이터
//				
//				//변수정의
//				param.put("filePath", filePath); 
//				int targetCellCnt = 4;
//				param.put("targetCellCnt", targetCellCnt);//사용자가 올린 엑셀 컬럼수
//				param.put("startRowData", 1);// 실제 data가 시작되는 row
//				
//				String ma_q_unit = (String) param.get("ma_q_unit");//유량단위
//				String ma_h_unit = (String) param.get("ma_h_unit");//수위단위
//				String ma_v_unit = (String) param.get("ma_v_unit");//유속단위
//				
//				//1. 사용자가 올린 엑셀을 읽어 list로 받는다.
//				ArrayList<HashMap<String, Object>> excelList = excelRead.ExcelReadDocument(param);// 사용자가 올린 엑셀을 읽어 list로 받는다.
//				
//				//2. 읽어온 엑셀을 순서대로 컬럼명 변경.
//				String[] columnNames = new String[]{"ma_date_all", "ma_q", "ma_h", "ma_v"};
//				modifyExcelList = columnNameChange(excelList, columnNames, targetCellCnt, login_user_id, "" );
//				
//				//3. 읽어온 데이터 포맷에 맞게 변경.
//				//ma_date, ma_time, ma_q, ma_h, ma_v
//				
//				HashMap<String, Object> excelListMap = new HashMap<String, Object>();// 3줄 한줄로.
//				String date = "";
//				String date_date = "";
//				String date_time = "";
//				 
//				for(int i=0; i<modifyExcelList.size(); i++)
//	    		{	
//					HashMap<String, Object> modifyMap = (HashMap<String, Object>)modifyExcelList.get(i);
//					
//					date = String.valueOf(modifyMap.get("ma_date_all"));
//					excelListMap.put("ma_date_all", date);//일자+시간
//					
//					date = date.replace("-","").replace(":","").replace(" ","").replace("	","").replace("/","");
//					
//				    date_date = date.substring(0, 8);
//				    date_time = date.substring(8, 12);
//					
//				    
//					excelListMap.put("ma_date", date_date);//일자.
//					excelListMap.put("ma_time", date_time);//시간
//					
//				
//					String temp_ma_q = String.valueOf(modifyMap.get("ma_q"));//유량
//					String temp_ma_h = String.valueOf(modifyMap.get("ma_h"));//수위
//					String temp_ma_v = String.valueOf(modifyMap.get("ma_v"));//유속
//					
//					// 단위 변환
//				    excelListMap.put("ma_q", resetTrans("1", ma_q_unit, temp_ma_q));//유량
//				    excelListMap.put("ma_v", resetTrans("2", ma_v_unit, temp_ma_v));//유속
//					excelListMap.put("ma_h", resetTrans("3", ma_h_unit, temp_ma_h));//수위
//					
//					
//					reExcelList.add(excelListMap);
//					
//					excelListMap = new HashMap<String, Object>();
//					date = "";
//					date_date = "";
//					date_time = "";
//	    		}
//				
//		}else if("off2".equals(jobtype)){// I/I분석 > 오프라인 데이터 등록 > 강우량 데이터
//			
//			//변수정의
//			param.put("filePath", filePath); 
//			int targetCellCnt = 2;
//			param.put("targetCellCnt", targetCellCnt);//사용자가 올린 엑셀 컬럼수
//			param.put("startRowData", 1);// 실제 data가 시작되는 row
//			
//			String rm_rain_unit = (String) param.get("rm_rain_unit");//강우단위
//			
//			
//			//1. 사용자가 올린 엑셀을 읽어 list로 받는다.
//			ArrayList<HashMap<String, Object>> excelList = excelRead.ExcelReadDocument(param);// 사용자가 올린 엑셀을 읽어 list로 받는다.
//			
//			//2. 읽어온 엑셀을 순서대로 컬럼명 변경.
//			String[] columnNames = new String[]{"rm_date_all", "rm_rain"};
//			modifyExcelList = columnNameChange(excelList, columnNames, targetCellCnt, login_user_id, "" );
//			
//			//3. 읽어온 데이터 포맷에 맞게 변경.
//			//rm_date, rm_time, rm_rain
//			
//			HashMap<String, Object> excelListMap = new HashMap<String, Object>();// 3줄 한줄로.
//			String date = "";
//			String date_date = "";
//			String date_time = "";
//			 
//			for(int i=0; i<modifyExcelList.size(); i++)
//    		{	
//				HashMap<String, Object> modifyMap = (HashMap<String, Object>)modifyExcelList.get(i);
//				
//				date = String.valueOf(modifyMap.get("rm_date_all"));
//				excelListMap.put("rm_date_all", date);//일자+시간
//				
//				date = date.replace("-","").replace(":","").replace(" ","").replace("	","").replace("/","");
//				
//			    date_date = date.substring(0, 8);
//			    date_time = date.substring(8, 12);
//				
//			    
//				excelListMap.put("rm_date", date_date);//일자.
//				excelListMap.put("rm_time", date_time);//시간
//				
//				
//				String temp_rm_rain = String.valueOf(modifyMap.get("rm_rain"));//강우
//				// 단위 변환
//			    excelListMap.put("rm_rain", resetTrans("4", rm_rain_unit, temp_rm_rain));//강우
//				
//				reExcelList.add(excelListMap);
//				
//				excelListMap = new HashMap<String, Object>();
//				date = "";
//				date_date = "";
//				date_time = "";
//    		}
//			
//			
//				
//			
//		}else {
//			throw new ApplicationException("엑셀 업로드  에러가 발생하였습니다.[jobType null ] : " + param);
//		}
//		
//		System.out.println("step 성공성공성공성공성공성공성공: ");
//	    return reExcelList;
//	}
//	
//	
//	public double resetTrans(String div_type, String unit_type, String temp_val) {
//		
//		//double i_temp_val = Integer.parseInt(temp_val);
//		double i_temp_val = Double.parseDouble(temp_val);
//		
//		double i_rtn_val = -1;
//		String s_rtn_val = "";
//		
//		if("1".equals(div_type))//유량
//		{
//			if("1".equals(unit_type))//m³/d -> m³/d
//			{
//				
//				i_rtn_val = i_temp_val;
//				
//			}else if("2".equals(unit_type)){//m³/h -> m³/d
//				
//				i_rtn_val = i_temp_val * 24;
//				
//			}else if("3".equals(unit_type)){//m³/s -> m³/d
//				
//				i_rtn_val = i_temp_val * 3600 * 24;
//				
//			}else if("4".equals(unit_type)){//l/h -> m³/d
//				
//				i_rtn_val = (i_temp_val * 24) / 1000;
//				
//			}else if("5".equals(unit_type)){//l/s -> m³/d
//				
//				i_rtn_val = (i_temp_val * 3600 * 24) / 1000;
//				
//			}else {//error
//				i_rtn_val = -1;
//			}
//		}else if("2".equals(div_type)){//유속
//		
//			if("1".equals(unit_type))//m/s -> m/s
//			{
//				
//				i_rtn_val = i_temp_val;
//				
//			}else if("2".equals(unit_type)){//cm/s -> m/s
//				
//				i_rtn_val = i_temp_val / 100;
//				
//			//}else if("".equals(unit_type)){//cm/h -> m/s 공통코드에 없음 현재 
//			//i_rtn_val = i_temp_val / (3600 * 100);
//			}else {//error
//				i_rtn_val = -1;
//			}
//			
//		}else if("3".equals(div_type)) {//수위
//			
//			if("1".equals(unit_type))//m -> cm
//			{
//				
//				i_rtn_val = i_temp_val * 100;
//				
//			}else if("2".equals(unit_type)){//cm -> cm
//				
//				i_rtn_val = i_temp_val;
//				
//			}else if("3".equals(unit_type)){//mm -> cm
//				
//				i_rtn_val = i_temp_val / 10;
//				
//			}else {//error
//				i_rtn_val = -1;
//			}
//		}else if("4".equals(div_type)) {//강우
//			
//			if("1".equals(unit_type))//cm -> mm
//			{
//				
//				i_rtn_val = i_temp_val * 10;
//				
//			}else if("2".equals(unit_type)){//mm -> mm
//				
//				i_rtn_val = i_temp_val;
//				
//			}else {//error
//				i_rtn_val = -1;
//			}
//			
//		}
//		
//		s_rtn_val = i_rtn_val + "";
//
//		return i_rtn_val;
//	}
//}
