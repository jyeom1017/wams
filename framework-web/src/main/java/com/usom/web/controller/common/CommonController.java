package com.usom.web.controller.common;

import com.framework.model.util.*;
//import org.apache.commons.io.IOUtils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.framework.model.service.view.back_ExcelView;
import com.framework.model.service.view.PdfView;
import com.usom.model.config.Configuration;
import com.usom.model.service.common.CommonService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

/**
 * 기초정보관리 > 인력관리
 * 
 * @author ash
 *
 */
@Controller
@RequestMapping("/common")
public class CommonController {
	// 전역변수...테스트 차원
	HashMap<String, Object> dragInfo = new HashMap<String, Object>();
	String dragFolderName;

	// @Resource(name = "uploadPath")
	// private String uploadPath;

	@Resource(name = "fileUpload")
	private FileUpload fileUpload;

	@Resource(name = "commonService")
	private CommonService commonService;

	@Inject
	private PdfView pdfView;

	@Inject
	private back_ExcelView excelView;

	@Autowired
	@Qualifier("pagging")
	public Pagging pagging;

	private static final Logger logger = LoggerFactory.getLogger(CommonController.class);
	
	@RequestMapping(value = "/setLogout.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int setLogout(HttpServletRequest request) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		//request.getSession().setAttribute("logout_user_id",user.getE_ID().toString());
		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			String ip_addr = request.getRemoteAddr();
			String user_id = user.getE_ID().toString();
			param2.put("lg_opt", "o");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기
			param2.put("lg_desc", user_id + "로그아웃");
			param2.put("login_user_id", user_id);
			param2.put("ip_addr", ip_addr );
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}		
		return 1;
	}
	
	@RequestMapping(value = "/getLogin.json", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody boolean getLogin(@RequestParam HashMap<String, Object> param) throws Exception {

//		System.out.println("getLogin.json start");
		boolean isLogin = false;
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//			System.out.print("getLogin.json auth   " + auth);
			User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
//			System.out.println("getLogin.json user  " + user);
//			System.out.println("getLogin.json user.getE_ID()  " + user.getE_ID());
			if(user != null && !"".equals(user.getE_ID())) {
				isLogin = true;
			}	
		}catch(NullPointerException e) {
			isLogin = false;
		}catch(Exception e) {
			isLogin = false;
//			e.printStackTrace();2020
		}
			
		return isLogin;
	}	
	
	
	/**
	 * 공통코드 리스트 가져오기
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
/*
	@RequestMapping(value = "/getCommonCodeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getCommonCodeList(
			@RequestParam HashMap<String, Object> param) throws Exception {

		return commonService.getCommonCodeList(param);
	}
*/
	/**
	 * 공통코드 리스트 가져오기 페이징 적용
	 *
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getCommonCodeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getCommonCodeList(@RequestParam HashMap<String, Object> param) throws Exception {
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		if (param.get("rules") != null){
			CommonUtil.checkSqlInjection(param.get("rules").toString(), "[-;\"|$/()]");
		}
		if ("RNUM".equals(param.get("sidx"))) {
			param.put("sidx", "ROWNUM");
		}
		list = commonService.getCommonCodeList(param);

		HashMap<String, Object> rtnMap = new HashMap<String, Object>();

		if (param.get("page") == null)
			param.put("page", 1);
		if (param.get("rows") == null)
			param.put("rows", 10);
		// if(param.get("page")==null) param.put("page", 1);
		// if(param.get("rows")==null) param.put("rows", 10);
		/* paging start */
		pagging.setCurrPage(Integer.parseInt(param.get("page").toString()));
		pagging.setMaxPage(Integer.parseInt(param.get("rows").toString()));
		pagging.setTotalCountArray(list);

		rtnMap.put("rows", list);//data
		rtnMap.put("page", pagging.getCurrPage());//현재페이지
		rtnMap.put("total", pagging.getTotalPageSize());//총페이지수
		rtnMap.put("records", pagging.getTotalCount());//총글갯수
		/* paging end */

		return rtnMap;
	}
	/**
	 * 공통코드 리스트 info
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getCommonCodeInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getCommonCodeInfo(
			@RequestParam HashMap<String, Object> param) throws Exception {
		return commonService.getCommonCodeInfo(param);
	}
	/**
	 * 시설공통코드 공통코드 리스트 
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getFacCodeList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getFacCodeList(@RequestParam HashMap<String, Object> param) throws Exception {

		return commonService.getFacCodeList(param);
	}		
	
	/**
	 * 시스템권한테이블 조회
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getEmployeeRoleList.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> getEmployeeRoleList(
			@RequestParam HashMap<String, Object> param) throws Exception {

		return commonService.getEmployeeRoleList(param);
	}

	/**
	 * 공통 이미지 업로드
	 * 
	 * @param request
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/imageUpload.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> imageUpload(HttpServletRequest request,
			@RequestParam HashMap<String, Object> param) throws Exception {

		ArrayList<HashMap<String, Object>> resImageInfoList = new ArrayList<HashMap<String, Object>>();

		String jsondata = param.get("imageInfoList").toString();
		String foldername = param.get("foldername").toString();
		JSONArray imageInfoList = (JSONArray) JSONSerializer.toJSON(jsondata);
		for (int i = 0; i < imageInfoList.size(); i++) {
			JSONObject imageInfo = (JSONObject) imageInfoList.get(i);
			String imageobject = imageInfo.get("imageobject").toString();
			String imagename = imageInfo.get("imagename").toString();
			String imagedesc = imageInfo.get("imagedesc").toString();

			HashMap<String, Object> resInfo = new HashMap<String, Object>();
			if (imageobject != null && !imageobject.equals("")) {
				int filenumber = saveImage(imageobject, imagename, imagedesc, foldername);
				resInfo.put("fieldname", imagename);
				resInfo.put("filenumber", filenumber);
			} else {// 삭제한 이미지 -> 서버에서도 삭제해줄 것
				resInfo.put("fieldname", imagename);
				resInfo.put("filenumber", "");
			}
			resImageInfoList.add(resInfo);
		}
		return resImageInfoList;
	}

	private int saveImage(String imageobject, String imagename, String imagedesc, String foldername)
			throws IOException, Exception {

		HashMap<String, Object> fileInfo = new HashMap<String, Object>();

		String path = Configuration.UPLOAD_FILE_PATH + "img/" + foldername;
		// 폴더 생성
		File folder = new File(path);
		if (folder.mkdirs() == false) {
			folder.mkdirs();
		}
		String url = Configuration.UPLOAD_FILE_URL + "img/" + foldername;

		byte[] imagedata = Base64.decodeBase64(imageobject.substring(imageobject.indexOf(",") + 1));
		// imagedata = null;
		BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imagedata));

		String imgName = imagename + "_" + CommonUtil.createUniqueIDFromDate();
		String urlPath = url + "/" + imgName + ".png";
		String filePath = path + "/" + imgName + ".png";
		ImageIO.write(bufferedImage, "png", new File(filePath));

		fileInfo.put("F_NAME", imgName + ".png");// F_NAME
		fileInfo.put("F_TITLE", imagedesc);// F_TITLE
		fileInfo.put("F_PATH", urlPath);// F_PATH

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		fileInfo.put("userid", user.getE_ID());

		int filenumber = insertFileInfo(fileInfo);

		return filenumber;
	}

	/**
	 * <pre>
	* 설명 : 공통파일업로더
	* 상세설명 : file 객체이름이 "fileData"를 저장소에 업로드후 M1_FILE테이블에 기록한후 파일정보를 리턴한다.
	 * </pre>
	 * 
	 */
	// 파일 업로드 첫번째 루트
	@RequestMapping(value = "/fileUpload.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> fileUpload(MultipartHttpServletRequest request) throws Exception {
		// file Check
		String[] arrFileType = { "PDF", "ZIP", "XLS", "XLSX", "HWP", "PPT", "PPTX", "DOC", "DOCX" };
		String folderName = request.getParameter("folderName"); // 폴더 이름을 받아온다.
		// System.out.println("------------폴더 이름"+folderName);
		String uploadDir = Configuration.UPLOAD_FILE_PATH + "file/" + folderName;
		// System.out.println("------------uploadDir 이름"+uploadDir);
		String urlPath = Configuration.UPLOAD_FILE_URL + "file/" + folderName;
		// System.out.println("------------urlPath 이름"+uploadDir);
		HashMap<String, Object> fileInfo = new HashMap<String, Object>();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		/*
		 * 파일 루트 생성
		 */
		// 기본설정 세팅
		fileUpload.setFileUpload(arrFileType, 60, null, uploadDir); // fileUpload.java 이동
		if (fileUpload.validationFile(request)) {// file validation 체크 //fileUpload.java 이동
			CommonsMultipartFile fileData = (CommonsMultipartFile) request.getFile("fileData");
			if (fileData != null && !fileData.isEmpty()) {
				String fileName = fileUpload.create(fileData); /** fileUpload.java 이동 핵심 */
				fileInfo.put("F_NAME", fileName);// F_NAME
				fileInfo.put("F_TITLE", request.getParameter("fileDesc"));// F_TITLE
				fileInfo.put("F_PATH", urlPath + "/" + fileName);// F_PATH
				fileInfo.put("userid", user.getE_ID());
				int filenumber = insertFileInfo(fileInfo); // 두번째 루트
				fileInfo.put("filenumber", filenumber);
			}
		}
		// System.out.println("------------fileInfo 이름"+fileInfo);
		return fileInfo;
	}
	
	
	/**
	 * <pre>
	* 설명 : 다중 파일 업로드 2018-11월
	* 상세설명 : file 객체이름이 "fileData"를 저장소에 업로드후 M1_FILE테이블에 기록한후 파일정보를 리턴한다.
	 * </pre>
	 * 
	 */
	// 파일 업로드 첫번째 루트
	@RequestMapping(value = "/fileMultiUpload.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> fileMultiUpload(MultipartHttpServletRequest request) throws Exception {
		
		List<MultipartFile> fileDataList = request.getFiles("fileData");

		// file Check
		String[] arrFileType = { "PDF", "ZIP", "XLS", "XLSX", "HWP", "PPT", "PPTX", "DOC", "DOCX" };
		String folderName = request.getParameter("folderName"); // 폴더 이름을 받아온다.
		//System.out.println("------------폴더 이름"+folderName);
		String uploadDir = Configuration.UPLOAD_FILE_PATH + "file/" + folderName;
		// System.out.println("------------uploadDir 이름"+uploadDir);
		String urlPath = Configuration.UPLOAD_FILE_URL + "file/" + folderName;
		// System.out.println("------------urlPath 이름"+uploadDir);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		/*
		 * 파일 루트 생성
		 */
		
		HashMap<String, Object> fileKingInfo = new HashMap<String, Object>(); //fileInfo를 담을 생성
		
		// 기본설정 세팅
		for (int i=0; i<fileDataList.size(); i++) { //size() ==length()
			HashMap<String, Object> fileInfo = new HashMap<String, Object>();
			fileUpload.setFileUpload(arrFileType, 60, null, uploadDir); // fileUpload.java 이동
			
			if (fileUpload.validationFile(request)) {// file validation 체크 //fileUpload.java 이동
				CommonsMultipartFile fileData = (CommonsMultipartFile) fileDataList.get(i);
				if (fileData != null && !fileData.isEmpty()) {
					String fileName = fileUpload.create(fileData); /** fileUpload.java 이동 핵심 */
					fileInfo.put("F_NAME", fileName);// F_NAME
					fileInfo.put("F_TITLE", request.getParameter("fileDesc_"+i));   //request.getParameter("fileDesc"));// F_TITLE  //
					fileInfo.put("F_LOCATION", request.getParameter("tab3_fileUpload_"+i));   //request.getParameter("fileDesc"));// F_TITLE  //
					fileInfo.put("F_PATH", urlPath + "/" + fileName);// F_PATH
					fileInfo.put("userid", user.getE_ID());
					int filenumber = insertFileInfo(fileInfo); // 두번째 루트
					fileInfo.put("filenumber", filenumber);
					// System.out.println("------------fileInfo 이름------------"+fileInfo);
				}
			}
			fileKingInfo.put("fileInfo_"+i,fileInfo);	
			 //System.out.println("------------fileKingInfo 이름------------"+fileKingInfo);
		}

		
		return fileKingInfo; //fileInfo
	}

	@RequestMapping(value = "/imageFileUpload.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> imageFileUpload(
			@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		HashMap<String, Object> fileInfo = new HashMap<String, Object>();
		String foldername = param.get("folderName").toString();
		String imagename = param.get("imageName").toString();
		String imageobject = param.get("imageObject").toString();
		String imagedesc = param.get("fileDesc").toString();

		String path = Configuration.UPLOAD_FILE_PATH + "img/" + foldername;

		// 폴더 생성
		File folder = new File(path);
		if (folder.mkdirs() == false) {
			folder.mkdirs();
		}
		String url = Configuration.UPLOAD_FILE_URL + "img/" + foldername;

		byte[] imagedata = Base64.decodeBase64(imageobject.substring(imageobject.indexOf(",") + 1));
		// imagedata = null;
		BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imagedata));

		String imgName = imagename + "_" + CommonUtil.createUniqueIDFromDate();
		String urlPath = url + "/" + imgName + ".png";
		String filePath = path + "/" + imgName + ".png";
		ImageIO.write(bufferedImage, "png", new File(filePath));

		fileInfo.put("F_NAME", imgName + ".png");// F_NAME
		fileInfo.put("F_TITLE", imagedesc);// F_TITLE
		fileInfo.put("F_PATH", urlPath);// F_PATH

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		fileInfo.put("userid", user.getE_ID());

		int filenumber = insertFileInfo(fileInfo); // DB 저장
		fileInfo.put("F_NUM", filenumber);

		return fileInfo;
	}

	/**
	 * <pre>
	* 설명 : 파일정보 가져오기
	* 상세설명 : .
	 * </pre>
	 */
	@RequestMapping(value = "/getFileInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> getFileInfo(@RequestParam("filenumber") int filenumber)
			throws Exception {
		return commonService.getFileInfo(filenumber);
	}

	private int insertFileInfo(HashMap<String, Object> fileInfo) // 파일 번호 생성
			throws Exception {
		commonService.insertFileInfo(fileInfo);
		return Integer.parseInt(fileInfo.get("f_num").toString());
	}

	@RequestMapping(value = "/deleteFileInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deleteFileInfo(@RequestParam("filenumber") int filenumber) throws Exception {

		System.out.println("**************************************************");
		System.out.println("CommonController /deleteFileInfo.json 으로 들어오셨네요 ");
		System.out.println("**************************************************");

		HashMap<String, Object> delFileInfo = commonService.getFileInfo(filenumber);
		String filepath = delFileInfo.get("F_PATH").toString().replaceAll(Configuration.UPLOAD_FILE_URL, "");// file URL
																												// 경로
																												// 정보를
																												// 지운다.
		filepath = Configuration.UPLOAD_FILE_PATH + filepath;// 실제파일경로로 결합한다.

		// System.out.println("filepath :"+filepath);

		File f = new File(filepath);
		f.delete();// 파일삭제

		return commonService.deleteFileInfo(filenumber);
	}

	/**
	 * <pre>
	* 설명 : 일정마스터 - 진행상태코드 업데이트
	 * </pre>
	 */
	@RequestMapping(value = "/updateSchduleMasterStateCode.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int updateSchduleMasterStateCode(@RequestParam(required = true) HashMap<String, Object> param)
			throws Exception {
		//return commonService.updateSchduleMasterStateCode(param);
		return 0;
	}

	/**
	 * 엑셀/PDF로 저장
	 * 
	 * @param data
	 * @param format
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getExportFile", method = RequestMethod.POST)
	public ModelAndView getExportFile(@RequestParam("jsondata") String data, @RequestParam("format") String format)
			throws Exception {
		// public ModelAndView getExportFile(@RequestParam(value= "jsondata"
		// ,required=false) String data, @RequestParam(value="format",required=false)
		// String format) throws Exception {
		ModelAndView mav = null;
		if (format.equals("excel")) {
			mav = new ModelAndView(excelView);
		} else if (format.equals("pdf")) {
			mav = new ModelAndView(pdfView);
		}

		/*
		 * 값의 부재 혹은 데이터가 밀러서 들어오는경우 보내는 DATA 의 JSON 허용하는 값인지 확인
		 */
		JSONArray array = (JSONArray) JSONSerializer.toJSON(data);

		ArrayList<LinkedHashMap<String, Object>> arrList = new ArrayList<LinkedHashMap<String, Object>>();
		LinkedHashMap<String, Object> put = null;

		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonObject = (JSONObject) array.get(i);
			Iterator keys = jsonObject.entrySet().iterator();
			put = new LinkedHashMap<String, Object>();
			while (keys.hasNext()) {
				Entry entry = (Entry) keys.next();
				put.put(entry.getKey().toString(), entry.getValue());
			}
			arrList.add(put);
		}
		if(arrList != null) {
			if(mav != null) {
				mav.addObject("wordList", arrList);
			}	
		}
		

		return mav;
	}

	//

	@RequestMapping(value = "/researchMaintenance.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> researchMaintenance(
			@RequestParam(required = true) HashMap<String, Object> param) throws Exception {

		//System.out.println("--------조사---------" + commonService.researchMaintenance(param));

		//return commonService.researchMaintenance(param);
		return null;
	}

	@RequestMapping(value = "/checkMaintenance.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> checkMaintenance(
			@RequestParam(required = true) HashMap<String, Object> param) throws Exception {

		//System.out.println("---------점검---------" + commonService.checkMaintenance(param));

		//return commonService.checkMaintenance(param);
		return null;
	}

	@RequestMapping(value = "/facilityMaintenance.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> facilityMaintenance(
			@RequestParam(required = true) HashMap<String, Object> param) throws Exception {

		//System.out.println("---------시설---------" + commonService.facilityMaintenance(param));

		//return commonService.facilityMaintenance(param);
		return null;
	}

	@RequestMapping(value = "/cleningMaintenance.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> cleningMaintenance(
			@RequestParam(required = true) HashMap<String, Object> param) throws Exception {

		//System.out.println("---------청소---------" + commonService.cleningMaintenance(param));

		//return commonService.cleningMaintenance(param);
		return null;
	}

	@RequestMapping(value = "/emergencyMaintenance.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ArrayList<HashMap<String, Object>> emergencyMaintenance(
			@RequestParam(required = true) HashMap<String, Object> param) throws Exception {

		//System.out.println("---------긴급---------" + commonService.emergencyMaintenance(param));

		//return commonService.emergencyMaintenance(param);
		return null;
	}

	@RequestMapping(value = "/imageDragInfo.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> imageDragInfo(
			@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		System.out.println("param :" + param);
		dragFolderName = param.get("folderName").toString();

		dragInfo.put("folderName", dragFolderName);

		return dragInfo;
	}

	@ResponseBody
	@RequestMapping(value = "/uploadAjax", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public ResponseEntity<String> uploadAjax(MultipartFile file) throws Exception {

		System.out.println("**************************************************");
		System.out.println("CommonController /uploadAjax 으로 들어오셨네요 ");
		System.out.println("**************************************************");

		String folderName = dragInfo.get("folderName").toString();
		String uploadPath = folderName;

		return new ResponseEntity<String>(
				UploadFileUtils.uploadFile(uploadPath, file.getOriginalFilename(), file.getBytes()),
				HttpStatus.CREATED);
	}
/*
	@ResponseBody
	@RequestMapping("/displayFile")
	public ResponseEntity<byte[]> displayFile(String fileName) throws Exception {

		System.out.println("**************************************************");
		System.out.println("CommonController /displayFile 으로 들어오셨네요 ");
		System.out.println("**************************************************");

		InputStream in = null;
		ResponseEntity<byte[]> entity = null;

		logger.info("fileName NAME: " + fileName);
		logger.info("dragFolderName NAME: " + dragFolderName);

		String[] array = fileName.split("/");
		String thumbnail;

		thumbnail = array[array.length - 1]; // 썸네일 이름만 추출 하기 위해서

		// System.out.println( "테스트 입니다."+thumbnail);

		// for(int i=0;i<array.length;i++) {
		// System.out.println( "테스트 입니다."+array[array.length - 1]);

		// }

		try {

			String formatName = fileName.substring(fileName.lastIndexOf(".") + 1);

			MediaType mType = MediaUtils.getMediaType(formatName);

			HttpHeaders headers = new HttpHeaders();

			// System.out.println("fileName "+fileName);

			in = new FileInputStream(Configuration.UPLOAD_FILE_PATH + "img/" + dragFolderName + "/" + thumbnail); // 파일
																													// 읽어오기
																													// (사진
																													// 경로);
																													// 호출한
																													// ajax
																													// 에서
																													// data
																													// 값.

			if (mType != null) {
				headers.setContentType(mType);
			} else {

				fileName = fileName.substring(fileName.indexOf("_") + 1);
				headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
				headers.add("Content-Disposition",
						"attachment; filename=\"" + new String(fileName.getBytes("UTF-8"), "ISO-8859-1") + "\"");
			}

			entity = new ResponseEntity<byte[]>(IOUtils.toByteArray(in), headers, HttpStatus.CREATED);
		} catch (FileNotFoundException e) {
			entity = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
//			e.printStackTrace(); 2020
			entity = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		} finally {
			if(in != null) {
				in.close();	
			}
		}
		return entity;
	}
*/
	@ResponseBody
	@RequestMapping(value = "/deleteFile", method = RequestMethod.POST)
	public ResponseEntity<String> deleteFile(String fileName) throws Exception {

		System.out.println("**************************************************");
		System.out.println("CommonController /deleteFile 으로 들어오셨네요 ");
		System.out.println("**************************************************");

		System.out.println(" fileName?? : " + fileName);

		logger.info("delete: " + fileName);

		String formatName = fileName.substring(fileName.lastIndexOf(".") + 1);

		MediaType mType = MediaUtils.getMediaType(formatName);

		String[] array = fileName.split("/");
		String thumbnail;
		thumbnail = array[array.length - 1]; // 썸네일 이름만 추출 하기 위해서
		System.out.println("thumbnail :" + thumbnail);

		String[] array2 = thumbnail.split("thumbnail_");
		String orginFile;
		orginFile = array2[array2.length - 1]; // 원본 이름만 추출 하기 위해서
		System.out.println("orginFile :" + orginFile);

		if (mType != null) {
			new File(Configuration.UPLOAD_FILE_PATH + "img/" + dragFolderName + "/" + orginFile).delete(); // 원본 파일 삭제
		}

		new File(Configuration.UPLOAD_FILE_PATH + "img/" + dragFolderName + "/" + thumbnail).delete(); // 썸네일 삭제

		return new ResponseEntity<String>("deleted", HttpStatus.OK);
	}

	@RequestMapping(value = "/dragFileUpload.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, Object> dragFileUpload(
			@RequestParam(required = true) HashMap<String, Object> param) throws Exception {
		HashMap<String, Object> fileInfo = new HashMap<String, Object>();

		System.out.println("**************************************************");
		System.out.println("CommonController /dragFileUpload.json 으로 들어오셨네요 ");
		System.out.println("**************************************************");

		String folderName = param.get("folderName").toString();
		String imageName = param.get("imageName").toString();
		String fileDesc = param.get("fileDesc").toString();
		String orgFileName = param.get("orgFileName").toString();

		System.out.println("folderName :" + folderName);
		System.out.println("imageName :" + imageName);
		System.out.println("fileDesc :" + fileDesc);
		System.out.println("orgFileName :" + orgFileName);

		String urlPath = Configuration.UPLOAD_FILE_URL + "img/" + folderName + "/" + orgFileName;
		System.out.println("url :" + urlPath);

		fileInfo.put("F_NAME", orgFileName);// F_NAME
		fileInfo.put("F_TITLE", fileDesc);// F_TITLE
		fileInfo.put("F_PATH", urlPath);// F_PATH

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
		fileInfo.put("userid", user.getE_ID());

		int filenumber = insertFileInfo(fileInfo); // DB 저장
		fileInfo.put("F_NUM", filenumber);

		System.out.println("fileInfo  :" + fileInfo);

		return fileInfo;
	}

	@RequestMapping(value = "/dragFileRemove.json", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int dragFileRemove(@RequestParam("filenumber") int filenumber) throws Exception {
		HashMap<String, Object> delFileInfo = commonService.getFileInfo(filenumber);
		System.out.println("**************************************************");
		System.out.println("CommonController /dragFileRemove.json 으로 들어오셨네요 ");
		System.out.println("**************************************************");
		System.out.println("delFileInfo  :" + delFileInfo);

		String filepath = delFileInfo.get("F_PATH").toString().replaceAll(Configuration.UPLOAD_FILE_URL, "");// file URL
																												// 경로
																												// 정보를
																												// 지운다.

		String[] array = filepath.split("/");
		String thumbnail;
		thumbnail = array[0] + "/" + array[1] + "/" + "thumbnail_" + array[array.length - 1]; // 썸네일 이름만 추출 하기 위해서

		filepath = Configuration.UPLOAD_FILE_PATH + filepath;// 실제파일경로로 결합한다.

		String thumbnailPath = Configuration.UPLOAD_FILE_PATH + thumbnail;// 실제썸네일경로로 결합한다.

		File f = new File(filepath);
		f.delete();// 원본 삭제

		File f2 = new File(thumbnailPath);
		f2.delete();// 썸네일 삭제

		return commonService.deleteFileInfo(filenumber);
	}
	//관리번호 업데이트
	@RequestMapping(value = "/updateS_ccum.json", method = RequestMethod.POST, headers="Accept=application/json")
	public @ResponseBody int updateS_ccum(@RequestParam(required=true) HashMap<String, Object> param) throws Exception {
		//return commonService.updateS_ccum(param);
		return 0;
	}
	
	//로그인 실패 잠금계정 초기화
	@RequestMapping(value = "/initLoginFailCnt.json", method = RequestMethod.POST, headers="Accept=application/json")
	public @ResponseBody int initLoginFailCnt(@RequestParam(required=true) HashMap<String, Object> param) throws Exception {
		//param.put("E_ID",param.get(""));
		param.put("FAIL_CNT",0);
		return commonService.updateLoginFailCnt(param);
	}	
}
