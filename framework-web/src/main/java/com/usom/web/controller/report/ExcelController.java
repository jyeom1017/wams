package com.usom.web.controller.report;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Struct;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Chart;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTAxDataSource;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBarChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBarSer;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBoolean;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTCatAx;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTLegend;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTLineChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTLineSer;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTNumDataSource;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTNumRef;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPlotArea;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTScaling;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTSerTx;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTStrRef;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTTitle;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTValAx;
import org.openxmlformats.schemas.drawingml.x2006.chart.STAxPos;
import org.openxmlformats.schemas.drawingml.x2006.chart.STBarDir;
import org.openxmlformats.schemas.drawingml.x2006.chart.STCrosses;
import org.openxmlformats.schemas.drawingml.x2006.chart.STLegendPos;
import org.openxmlformats.schemas.drawingml.x2006.chart.STOrientation;
import org.openxmlformats.schemas.drawingml.x2006.chart.STTickLblPos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.usom.model.config.Configuration;
import com.usom.model.service.common.CommonService;
import com.usom.model.service.common.ExcelCommonService;
import com.usom.model.vo.user.User;
import com.usom.model.vo.user.UserDetailsServiceVO;

import net.sf.jxls.transformer.XLSTransformer;

@Controller
@RequestMapping("/excel")
public class ExcelController {

	@Resource(name = "commonService")
	private CommonService commonService;
	
    @Autowired
    private Environment environment;

    @Autowired
    private ExcelCommonService excelCommonService;

    @RequestMapping("/downloadExcel")//http://localhost:8080/excel/downloadExcel.do
    public void excelDownLoad(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException, Exception {
//        HashMap<String, Object> paramMap = new HashMap<String, Object>();
        HashMap<String, Object> map = new HashMap<String, Object>();

        HashMap<String, Object> param = new HashMap<String, Object>();
		param = getParameterMap(req);
		
		
		//각 보고서별 파라메터
		//param.put("REPAIR_SID", "608");		param.put("EXCEL_ID", "010202");//개보수 사업관리
		//param.put("INSPECT_SID", "97");		param.put("EXCEL_ID", "010203");//조사 및 탐사
		//param.put("COMPLAIN_SID", "175");		param.put("EXCEL_ID", "010205");//민원관리
		//param.put("BBS_GROUP_CD", "notice");param.put("BBS_SID", "64");	param.put("EXCEL_ID", "011501");//공지사항
		//param.put("BBS_GROUP_CD", "qna");param.put("BBS_SID", "64");	param.put("EXCEL_ID", "011502");//질의응답
		//param.put("BBS_GROUP_CD", "pbb");param.put("BBS_SID", "64");	param.put("EXCEL_ID", "011503");//자료실
		//param.put("FIN_ANLS_SID", "1012");param.put("EXCEL_ID", "010402");//재정계획 수립
		
		String lg_desc = "";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object principal = auth.getPrincipal();
		User user = ((UserDetailsServiceVO) principal).getUser();
		String user_id =  user.getE_ID();
		String ip_addr = req.getRemoteAddr();
		switch(param.get("EXCEL_ID").toString()){
		case "010202" : lg_desc = "[메뉴 : 운영관리 > 개보수사업 관리 ] - 개보수사업 보고서.xlsx"; break;
		case "010203" : lg_desc = "[메뉴 : 운영관리 > 조사및 탐사 관리 ] - 조사및탐사 보고서.xlsx"; break;
		case "010205" : lg_desc = "[메뉴 : 운영관리 > 민원관리 ] - 민원관리 보고서.xlsx"; break;
		case "010301" : lg_desc = "[메뉴 : 운영관리 > 기술자료관리 ] - 기술자료관리.xlsx"; break;
		case "011501" : lg_desc = "[메뉴 : 게시판 > 공지사항 ] - 공지사항.xlsx"; break;
		case "011502" : lg_desc = "[메뉴 : 게시판 > 질의응답 ] - 질의응답.xlsx"; break;
		case "011503" : lg_desc = "[메뉴 : 게시판 > 자료실 ] - 자료실.xlsx"; break;
		case "010110" : lg_desc = "[메뉴 : 관리자 > 로그관리 ] - 로그관리.xlsx"; break;
		case "010102" : lg_desc = "[메뉴 : 인벤토리관리> 자산관리 ] - 자산대장.xlsx"; break;
		case "010302" : lg_desc = "[메뉴 : 평가분석 > 간접평가 ] - 간접평가 .xlsx"; break;
		case "011506" : lg_desc = "[메뉴 : 평가분석 > 직접평가 ] - 직접평가 .xlsx"; break;
		case "010303" : lg_desc = "[메뉴 : 평가분석 > 잔존수명 예측평가] - 잔존수명예측평가 보고서.xlsx"; break;
		case "010306" : lg_desc = "[메뉴 : 평가분석 > 생애주기비용분석 ] - 생애주기비용분석.xlsx"; break;
		case "010305" : lg_desc = "[메뉴 : 평가분석  > 서비스수준분석 ] - 서비스수준분석.xlsx"; break;
		case "010402" : lg_desc = "[메뉴 : 재무부석 > 재정계획수립 ] - 재정계획수립보고서.xlsx"; break;
		default : 
			lg_desc = param.get("EXCEL_ID").toString();break;
		}		
				param.put("lg_opt", "2");//1:로그인, 2:출력, 3:에러
				param.put("lg_desc", lg_desc);// 화면의 코드를 넘긴다. 예) 유량조사 0502
				param.put("login_user_id",user_id );
				param.put("E_ID", user_id );
				param.put("ip_addr", ip_addr );
				System.out.println(param);
				commonService.insertSystemLog(param);		
		
		
		
        String excelDir = Configuration.REPORT_FILE_TEMPLATE_PATH + "\\new\\";
        
        InputStream is = null;
        Workbook workbook = null;
        FileOutputStream fos = null;
        FileInputStream fileInputStream = null;

        try {

//            String sqlIds = req.getParameter("sqlId");
//            String paramName = req.getParameter("paramName");
//            String paramData = req.getParameter("paramData");
//            String mapInputId = req.getParameter("mapInputId");
            String templateFileName = (String)param.get("EXCEL_ID");
            String templateFileOrgName = "excel";
            
//    		JSONObject jo  = new JSONObject();
//    		jo = JSONObject.fromObject(Param);
//    		HashMap<String, Object> map = CommonUtil.convertJsonToMap(jo);            

            SimpleDateFormat time = new SimpleDateFormat("yyyyMMddhhmmss");
            Date today = new Date();
            String date = time.format(today);

            


            if("010202".equals(templateFileName)) {//개보수 사업관리
            	 ArrayList<HashMap<String, Object>> list1 = excelCommonService.getList_010202(param);
            	 map.put("list1", list1);
            	 
            	 HashMap<String, Object> info1 = excelCommonService.getInfo_010202(param);
            	 map.put("info1", info1);
            	 
            	 templateFileOrgName = "개보수_사업관리";
            }else if("010203".equals(templateFileName)) {//조사 및 탐사
                	 
            	ArrayList<HashMap<String, Object>> list1 = excelCommonService.getList_010203(param);
           	 	map.put("list1", list1);
           	 	
	           	 HashMap<String, Object> info1 = excelCommonService.getInfo_010203(param);
	        	 map.put("info1", info1);
            	
                templateFileOrgName = "조사_및_탐사";
                	 
            }else if("010205".equals(templateFileName)) {//민원관리
            	
            	ArrayList<HashMap<String, Object>> list1 = excelCommonService.getList_010205(param);
           	 	map.put("list1", list1);
           	 	
	           	HashMap<String, Object> info1 = excelCommonService.getInfo_010205(param);
	        	map.put("info1", info1);
	        	
           	 	templateFileOrgName = "민원관리";                	 
                	 
            }else if("010301".equals(templateFileName)) {//기술_자료_관리
           	 
            	templateFileOrgName = "기술_자료_관리";
           	 
            }else if("011501".equals(templateFileName)) {//공지사항
            	
            	param.put("type", "1");
            	ArrayList<HashMap<String, Object>> list1 = excelCommonService.getBBSList(param);
           	 	map.put("list1", list1);
            	
             	templateFileOrgName = "공지사항";
              	 
            }else if("011502".equals(templateFileName)) {//질의 응답

            	param.put("type", "2");
            	ArrayList<HashMap<String, Object>> list1 = excelCommonService.getBBSList(param);
           	 	map.put("list1", list1);
            	
              	templateFileOrgName = "질의_응답";
              	 
            }else if("011503".equals(templateFileName)) {//자료실
            	
            	param.put("type", "3");
            	ArrayList<HashMap<String, Object>> list1 = excelCommonService.getBBSList(param);
           	 	map.put("list1", list1);            	
             	 
             	templateFileOrgName = "자료실";
             	 
            }else if("010110".equals(templateFileName)) {//로그관리
            	 
            	 templateFileOrgName = "로그관리";
            	 
            }else if("010302".equals(templateFileName)) {//간접평가
           	 
           	 	templateFileOrgName = "간접평가";
           	 
            }else if("011506".equals(templateFileName)) {//직접평가
           	 
           	 	templateFileOrgName = "직접평가";
           	 	
            }else if("010303".equals(templateFileName)) {//잔존수명
           	 
           	 	templateFileOrgName = "잔존수명";
           }else if("010102".equals(templateFileName)) {//자산관리
        	   ArrayList<HashMap<String, Object>> list1 = excelCommonService.getList1_010102(param);
          	   map.put("list1", list1);
          	   
          	   ArrayList<HashMap<String, Object>> list2 = excelCommonService.getList2_010102(param);
        	   map.put("list2", list2);
        	   
	           HashMap<String, Object> info1 = excelCommonService.getInfo_010102(param);
	           map.put("info1", info1);
	        	 
        	   templateFileOrgName = "자산관리";
           }else if("010306".equals(templateFileName)) {//생애주기 보고서 그래프
        	   
        	   HashMap<String, Object> asset = null;
        	   if(param.get("ASSET_SID") != null)
        		   asset = excelCommonService.getList1_010306(param);
        	   else 
        		   asset = excelCommonService.getList2_010306(param);
        	   
	      	   map.put("info1", asset.get("info"));
	    	   map.put("list1", asset.get("list1"));
	    	   templateFileOrgName = "생애주기_자산_비용분석";
           }/*else if("010306_2".equals(templateFileName)) {//생애주기 보고서
        	   
        	   param.put("MAX_REPORT_PRINT_LINE_CNT", Configuration.MAX_REPORT_PRINT_LINE_CNT);
	    	   HashMap<String, Object> asset = excelCommonService.getList1_010306_2(param);
	    	   
	    	   map.put("flist", asset.get("flist"));
	    	   map.put("ftotal", asset.get("ftotal"));
	    	   
	    	   map.put("nlist", asset.get("nlist"));
	    	   map.put("ntotal", asset.get("ntotal"));
	    	   
	    	   map.put("tlist", asset.get("tlist"));
	    	   map.put("ttotal", asset.get("ttotal"));
				
	    	   templateFileOrgName = "생애주기_보고서";
           }*/else if("010402".equals(templateFileName)) {//재정계획 수립

        	   HashMap<String, Object> list = excelCommonService.getList1_010402(param);
        	   map.putAll(list);
	    	   
	    	   templateFileOrgName = "재정계획_수립_보고서";
	        }
           else if ("010305".equals(templateFileName)) {// 서비스 수준 분석
        	   HashMap<String, Object> result = excelCommonService.getList1_010305(param);
        	   
	   			System.out.println("------------------------" + result);
	   			
	   			HashMap<String, Object> info = (HashMap<String, Object>)result.get("Info");
	   			info.put("USER_DT", info.get("UPDATE_DT") == null ? info.get("INSERT_DT") : info.get("UPDATE_DT"));
	   			info.put("USER_ID", info.get("UPDATE_ID") == null ? info.get("INSERT_ID") : info.get("UPDATE_ID"));
	   			
	   			ArrayList<HashMap<String, Object>> array = (ArrayList<HashMap<String, Object>>)result.get("Array");
	   			ArrayList<ArrayList<HashMap<String, Object>>> item = new ArrayList<ArrayList<HashMap<String, Object>>>();
	   			item.add(new ArrayList<HashMap<String, Object>>());
	   			item.add(new ArrayList<HashMap<String, Object>>());
	   			item.add(new ArrayList<HashMap<String, Object>>());
	   			item.add(new ArrayList<HashMap<String, Object>>());
	   			item.add(new ArrayList<HashMap<String, Object>>());
	   			item.add(new ArrayList<HashMap<String, Object>>());
	   			for(int i=0; i < array.size(); i++) {
	   				int index = Integer.parseInt(array.get(i).get("LOS_ITEM_CD").toString()) - 1;
	   				item.get(index).add(array.get(i));
	   				array.get(i).put("ROWNUM", item.get(index).size());
	   			}
	   			
	   			System.out.println("------------------------" + item);
	   			//list = 
	   			map.put("info", result.get("Info"));
	   			
	   			map.put("manpower", (ArrayList<HashMap<String, Object>>)item.get(0));
	   			map.put("facility", (ArrayList<HashMap<String, Object>>)item.get(1));
	   			map.put("operation", (ArrayList<HashMap<String, Object>>)item.get(2));
	   			map.put("service", (ArrayList<HashMap<String, Object>>)item.get(3));
	   			map.put("environment", (ArrayList<HashMap<String, Object>>)item.get(4));
	   			map.put("finance", (ArrayList<HashMap<String, Object>>)item.get(5));
	   			
        	   templateFileOrgName = "서비스_수준_분석_보고서";
           }
   
            String excelFileName = templateFileOrgName + "-" + date + ".xlsx";
//excelDir = "D:\\Pj\\workspace\\framework\\framework-web\\src\\main\\webapp\\resources\\excel\\template\\new";//test
            is = new BufferedInputStream(new FileInputStream(excelDir + File.separator + templateFileName + ".xlsx"));
            XLSTransformer xls = new XLSTransformer();
            workbook = xls.transformXLS(is, map);


            setImage(templateFileName, workbook, map);//이미지 있는 템플릿 추가 부분
            setGraph(templateFileName, workbook, map);//그래프 양식 수정 부분
            
            
            res.setHeader("Content-Disposition", getDisposition(excelFileName, getBrowser(req)));
            res.setHeader("Content-Transfer-Encoding", "binary");
            res.setContentType("application/x-msexcel");
            res.setContentType("application/octet-stream; charset=utf-8");
            workbook.write(res.getOutputStream());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) is.close();
            if (workbook != null) workbook.close();
            if (fos != null) fos.close();
            if (fileInputStream != null) fileInputStream.close();
        }
    }
    
   
    private void setGraph(String templateFileName, Workbook workbook, HashMap<String, Object> map) throws Exception {
    	String imageDir = Configuration.WEB_ROOT_PATH;
    	InputStream ips = null; 
    	try {
    		 switch (templateFileName) {
    		 case "010306": {
    			 ArrayList<HashMap<String, Object>>array = (ArrayList<HashMap<String, Object>>)map.get("list1");
    			 final int start = 34;
    			 final int end  = start + array.size() - 2;
    			 
    			 XSSFSheet sheet = null;
    	         sheet = (XSSFSheet) workbook.getSheetAt(0);
    	         
		       	 Drawing drawing = sheet.createDrawingPatriarch();
	   	         ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 1, 8, 14, 28);
	
	   	         Chart chart = drawing.createChart(anchor);
	
	   	         CTChart ctChart = ((XSSFChart)chart).getCTChart();  
	   	         CTPlotArea ctPlotArea = ctChart.getPlotArea();
	   	     
	   	         //the bar chart
	   	         CTBarChart ctBarChart = ctPlotArea.addNewBarChart();
	   	         CTBoolean ctBoolean = ctBarChart.addNewVaryColors();
	   	         ctBoolean.setVal(true);
	   	         ctBarChart.addNewBarDir().setVal(STBarDir.COL);
	
	   	         //the bar series
	   	         CTBarSer ctBarSer = ctBarChart.addNewSer();
	   	         CTSerTx ctSerTx = ctBarSer.addNewTx();
	   	         CTStrRef ctStrRef = ctSerTx.addNewStrRef();
	   	         ctStrRef.setF("생애주기비용분석!$M$31");
	   	         ctBarSer.addNewIdx().setVal(0);  
	   	         CTAxDataSource cttAxDataSource = ctBarSer.addNewCat();
	   	         ctStrRef = cttAxDataSource.addNewStrRef();
	   	         ctStrRef.setF("생애주기비용분석!$B$" + start + ":$B$" + end); 
	   	         CTNumDataSource ctNumDataSource = ctBarSer.addNewVal();
	   	         CTNumRef ctNumRef = ctNumDataSource.addNewNumRef();
	   	         ctNumRef.setF("생애주기비용분석!$M$"+ start +":$M$" + end);
	
	   	         //at least the border lines in Libreoffice Calc ;-)
	   	         ctBarSer.addNewSpPr().addNewLn().addNewSolidFill().addNewSrgbClr().setVal(new byte[] {0,0,0});   
	
	   	         //telling the BarChart that it has axes and giving them Ids
	   	         ctBarChart.addNewAxId().setVal(123456); //cat axis 1 (bars)
	   	         ctBarChart.addNewAxId().setVal(123457); //val axis 1 (left)
	
	   	         //the line chart
	   	         CTLineChart ctLineChart = ctPlotArea.addNewLineChart();
	   	         ctBoolean = ctLineChart.addNewVaryColors();
	   	         ctBoolean.setVal(true);
	
	   	         //the line series
	   	         CTLineSer ctLineSer = ctLineChart.addNewSer();
	   	         ctSerTx = ctLineSer.addNewTx();
	   	         ctStrRef = ctSerTx.addNewStrRef();
	   	         ctStrRef.setF("생애주기비용분석!$N$31");
	   	         ctLineSer.addNewIdx().setVal(1);  
	   	         cttAxDataSource = ctLineSer.addNewCat();
	   	         ctStrRef = cttAxDataSource.addNewStrRef();
	   	         ctStrRef.setF("생애주기비용분석!$B$" + start + ":$B$" + end); 
	   	         ctNumDataSource = ctLineSer.addNewVal();
	   	         ctNumRef = ctNumDataSource.addNewNumRef();
	   	         ctNumRef.setF("생애주기비용분석!$N$" + start + ":$N$" + end);
	
	   	         //at least the border lines in Libreoffice Calc ;-)
	   	         ctLineSer.addNewSpPr().addNewLn().addNewSolidFill().addNewSrgbClr().setVal(new byte[] {0,0,0});   
	
	   	         //telling the LineChart that it has axes and giving them Ids
	   	         ctLineChart.addNewAxId().setVal(123458); //cat axis 2 (lines)
	   	         ctLineChart.addNewAxId().setVal(123459); //val axis 2 (right)
	
	   	         //cat axis 1 (bars)
	   	         CTCatAx ctCatAx = ctPlotArea.addNewCatAx(); 
	   	         ctCatAx.addNewAxId().setVal(123456); //id of the cat axis
	   	         CTScaling ctScaling = ctCatAx.addNewScaling();
	   	         ctScaling.addNewOrientation().setVal(STOrientation.MIN_MAX);
	   	         ctCatAx.addNewDelete().setVal(false);
	   	         ctCatAx.addNewAxPos().setVal(STAxPos.B);
	   	         ctCatAx.addNewCrossAx().setVal(123457); //id of the val axis
	   	         ctCatAx.addNewTickLblPos().setVal(STTickLblPos.NEXT_TO);
	
	   	         //val axis 1 (left)
	   	         CTValAx ctValAx = ctPlotArea.addNewValAx(); 
	   	         ctValAx.addNewAxId().setVal(123457); //id of the val axis
	   	         ctScaling = ctValAx.addNewScaling();
	   	         ctScaling.addNewOrientation().setVal(STOrientation.MIN_MAX);
	   	         ctValAx.addNewDelete().setVal(false);
	   	         ctValAx.addNewAxPos().setVal(STAxPos.L);
	   	         ctValAx.addNewCrossAx().setVal(123456); //id of the cat axis
	   	         ctValAx.addNewCrosses().setVal(STCrosses.AUTO_ZERO); //this val axis crosses the cat axis at zero
	   	         ctValAx.addNewTickLblPos().setVal(STTickLblPos.NEXT_TO);
	   	         
	   	         //cat axis 2 (lines)
	   	         ctCatAx = ctPlotArea.addNewCatAx(); 
	   	         ctCatAx.addNewAxId().setVal(123458); //id of the cat axis
	   	         ctScaling = ctCatAx.addNewScaling();
	   	         ctScaling.addNewOrientation().setVal(STOrientation.MIN_MAX);
	   	         ctCatAx.addNewDelete().setVal(true); //this cat axis is deleted
	   	         ctCatAx.addNewAxPos().setVal(STAxPos.B);
	   	         ctCatAx.addNewCrossAx().setVal(123459); //id of the val axis
	   	         ctCatAx.addNewTickLblPos().setVal(STTickLblPos.NEXT_TO);
	   	      
	   	          //val axis 2 (right)
	   	         ctValAx = ctPlotArea.addNewValAx(); 
	   	         ctValAx.addNewAxId().setVal(123459); //id of the val axis
	   	         ctScaling = ctValAx.addNewScaling();
	   	         ctScaling.addNewOrientation().setVal(STOrientation.MIN_MAX);
	   	         ctValAx.addNewDelete().setVal(false);
	   	         ctValAx.addNewAxPos().setVal(STAxPos.R);
	   	         ctValAx.addNewCrossAx().setVal(123458); //id of the cat axis
	   	         ctValAx.addNewCrosses().setVal(STCrosses.MAX); //this val axis crosses the cat axis at max value
	   	         ctValAx.addNewTickLblPos().setVal(STTickLblPos.NEXT_TO);
	   	         
	   	         //legend
	   	         CTLegend ctLegend = ctChart.addNewLegend();
	   	         ctLegend.addNewLegendPos().setVal(STLegendPos.B);
	   	         ctLegend.addNewOverlay().setVal(false);
    		 }break;
			 }
    		 
    	} catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        if (ips != null) ips.close();
	    }
    }
    
    private void setImage(String templateFileName, Workbook workbook, HashMap<String, Object> map) throws Exception {
    	 
    	String imageDir = Configuration.WEB_ROOT_PATH;
    	//String imageDir = "D:\\PJ2\\framework\\framework-web\\src\\main\\webapp\\resources\\excel\\template\\new\\";//test
    	InputStream ips = null; 
    	try {
    		if(templateFileName.indexOf("010202") >= 0 || templateFileName.indexOf("010203") >= 0  || templateFileName.indexOf("010205") >= 0) {
	
	           //1. 사진대장 시트 갯수만큼 복사하기
	           ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>)map.get("list1");
	           if(list.size() > 0) {
	        	   for(int i=1; i<list.size(); i++) {
		        	   Sheet sh = workbook.cloneSheet(1);
		        	   workbook.setSheetName(1+i, "사진대장-"+(i+1));
		           }   
	           }
	           
	           int pictureIdx = 0;
	           if(list.size() > 0) {
	        	   for(int i=0; i<list.size(); i++) {

	    	           XSSFSheet sheet = null;
	    	           sheet = (XSSFSheet) workbook.getSheetAt(i+1);
	    	           
	        		   HashMap<String, Object> imgInfo = (HashMap<String, Object>)list.get(i);
	        		   
	        		   ArrayList<HashMap<String, Object>> picList = null;
	        		   if(templateFileName.indexOf("010202") >= 0) {
	        			   picList = excelCommonService.getPic_010202(imgInfo);
	        		   }else if(templateFileName.indexOf("010203") >= 0) {
	        			   picList = excelCommonService.getPic_010203(imgInfo);	        			   
	        		   }else if(templateFileName.indexOf("010205") >= 0) {
	        			   picList = excelCommonService.getPic_010205(imgInfo);	        			   
	        		   }
	        		   int odd1 = 2;
	   		           int odd2 = 11;
	   		           int odd3 = 11;
	   		           int even1 = 2; 
	   		           int even2 = 11;
	   		           int even3 = 11;
	   	       	 	   
	   	       	 	   if(picList.size() > 0) {
	   	       	 		   for(int y=0; y<picList.size(); y++) {
	   	       	 			   
	   	       	 			   HashMap<String, Object> picInfo = null;
	   	       	 		       picInfo = (HashMap<String, Object>)picList.get(y);
			   		           
	   	       	 		       String F_DESC = (String)picInfo.get("F_DESC");//이미지 설명
	   	       	 		       String ASSET_CD = (String)picInfo.get("ASSET_CD");//자산코드
	   	       	 		       String ASSET_NM = (String)picInfo.get("ASSET_NM");//자산명
	   	       	 		       String F_PATH = (String)picInfo.get("F_PATH");//path
	   	       	 		
		   	       	 		   //자산코드
		       	 			   sheet.getRow(1).getCell(2).setCellValue(ASSET_CD);
		       	 			   //자산명
		    	 			   sheet.getRow(1).getCell(5).setCellValue(ASSET_NM);
    	 			  
		    	 			  if(!"".equals(F_PATH)) {
		    	 				  //이미지 불러오기
			   	       	 		   ips = new FileInputStream(imageDir + File.separator + picInfo.get("F_PATH")+"");
				   		           byte[] bytes = IOUtils.toByteArray(ips);
				   		          
				   		           //이미지 add
				        		   if("jpg".equals((String)picInfo.get("FILE_EXT_NM"))) {
				        			   workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_JPEG);
				        		   }else if("bmp".equals((String)picInfo.get("FILE_EXT_NM"))) {
				        			   workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_BMP);
				        		   }else if("gif".equals((String)picInfo.get("FILE_EXT_NM"))) {
				        			   workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_GIF);
				        		   }else if("png".equals((String)picInfo.get("FILE_EXT_NM"))) {
				        			   workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_PNG);
				        		   }
				   		           ips.close();
				   		           ips = null;
				   		           bytes = null;
				   		
				   		           //이미지 위치선정
				   		           XSSFCreationHelper helper = (XSSFCreationHelper) workbook.getCreationHelper();
				   		           XSSFDrawing drawing = (XSSFDrawing) sheet.createDrawingPatriarch();
				   		           XSSFClientAnchor anchor = (XSSFClientAnchor) helper.createClientAnchor();
				   		
				   		         
				   		           if (y % 2 == 1) {//홀
				   		        	
				   		        	   anchor.setCol1(3);
				   		        	   anchor.setCol2(6);
				   		        	   anchor.setRow1(odd1);
									   anchor.setRow2(odd2);
									   
									   sheet.getRow(odd3).getCell(3).setCellValue(F_DESC);
									   odd1 = odd1 + 10;
									   odd2 = odd2 + 10;
									   odd3 = odd3 + 10;
				   		           }else {
				   		        	   anchor.setCol1(0);//0부터
					   		           anchor.setCol2(3);
					   		           anchor.setRow1(even1);
					   		           anchor.setRow2(even2);
					   		           
					   		           sheet.getRow(even3).getCell(0).setCellValue(F_DESC);
					   		        
					   		           even1 = even1 + 10;
					   		           even2 = even2 + 10;
					   		           even3 = even3 + 10;
				   		           }
				   		           anchor.setDx1(0); 
				   		           anchor.setDx2(1000);  
				   		           anchor.setDy1(0); 
				   		           anchor.setDy2(1000);
				   		           
					   		        //이미지 넣기   
					   		        XSSFPicture picture = (XSSFPicture)  drawing.createPicture(anchor,pictureIdx);
					   		        pictureIdx++;
		    	 			   }
	   	       	 		   }
	   	       	 	   }
	   	       	 	   
	        	   }
	           }
	           	
       	 	
       	 	
	          
	           
	           
	
	/*  엑셀 이미지2 추가 부분 시작        
	           InputStream ips = new FileInputStream(imgInfo.get("FILE_PATH")+"");
	           InputStream ips2 = new FileInputStream(imageDir + File.separator + "kjw.jpg");
	           byte[] bytes2 = IOUtils.toByteArray(ips2);
	           int pictureIdx2 = workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_JPEG);
	           ips2.close();
	
	           XSSFCreationHelper helper2 = (XSSFCreationHelper) workbook.getCreationHelper();
	           XSSFDrawing drawing2 = (XSSFDrawing) sheet.createDrawingPatriarch();
	           XSSFClientAnchor anchor2 = (XSSFClientAnchor) helper2.createClientAnchor();
	
	           anchor2.setCol1(3);//0부터
	           anchor2.setCol2(6);
	           anchor2.setRow1(2);
	           anchor2.setRow2(11);
	
	           anchor2.setDx1(0); 
	           anchor2.setDx2(1000);  
	           anchor2.setDy1(0); 
	           anchor2.setDy2(1000);
	
	
	           
	           drawing.createPicture(anchor2,pictureIdx2);
	
	           //picture.resize(1.075,1.032);
	           //picture.resize(3.6, 1.10);//조사및 탐사
	           // 엑셀 이미지 추가 부분 끝
	*/
	         }else if("010102".equals(templateFileName)) {
	        	 
  	           XSSFSheet sheet = null;
  	           sheet = (XSSFSheet) workbook.getSheetAt(1);
      		   
      		   HashMap<String, Object> imgInfo = (HashMap<String, Object>)map.get("info1");
      		   ArrayList<HashMap<String, Object>> picList = null;
      		   picList = excelCommonService.getPic_010102(imgInfo);	
  		   	   int odd1 = 2;
 		           int odd2 = 11;
 		           int odd3 = 11;
 		           int even1 = 2; 
 		           int even2 = 11;
 		           int even3 = 11;
 		          int pictureIdx = 0;
 	       	 	   if(picList.size() > 0) {
 	       	 		   for(int y=0; y<picList.size(); y++) {
 	       	 			   
 	       	 			   HashMap<String, Object> picInfo = null;
 	       	 		       picInfo = (HashMap<String, Object>)picList.get(y);
 	       	 		       
 	       	 		       if(picInfo.get("F_PATH") == null ) continue;
 	       	 		  
 	       	 		       String F_DESC = (String)picInfo.get("DESCRIPTION");//이미지 설명
 	       	 		       String ASSET_CD = (String)picInfo.get("ASSET_CD");//자산코드
 	       	 		       String ASSET_NM = (String)picInfo.get("ASSET_NM");//자산명
	   	       	 		   //자산코드
	       	 			   sheet.getRow(1).getCell(2).setCellValue(ASSET_CD);
	       	 			   //자산명
	    	 			   sheet.getRow(1).getCell(5).setCellValue(ASSET_NM);
	 			  
 	       	 		       //이미지 불러오기
	    	 			   try {
	   	       	 		   ips = new FileInputStream(imageDir + File.separator + picInfo.get("F_PATH")+"");
		   		           byte[] bytes = IOUtils.toByteArray(ips);
		   		           //이미지 add
		        		   if("jpg".equals((String)picInfo.get("FILE_EXT_NM"))) {
		        			   workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_JPEG);
		        		   }else if("bmp".equals((String)picInfo.get("FILE_EXT_NM"))) {
		        			   workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_BMP);
		        		   }else if("gif".equals((String)picInfo.get("FILE_EXT_NM"))) {
		        			   workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_GIF);
		        		   }else if("png".equals((String)picInfo.get("FILE_EXT_NM"))) {
		        			   workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_PNG);
		        		   }
		   		           ips.close();
		   		           ips = null;
		   		           bytes = null;
	    	 			   }catch(Exception ex) {
		   	       	 		   ips = new FileInputStream(imageDir + File.separator + "/WAMS/resources/images/empty.png" +"");
			   		           byte[] bytes = IOUtils.toByteArray(ips);
			   		           workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_PNG);
			   		           ips.close();
			   		           ips = null;
			   		           bytes = null;			   		           
	    	 			   }
		   		
		   		           //이미지 위치선정
		   		           XSSFCreationHelper helper = (XSSFCreationHelper) workbook.getCreationHelper();
		   		           XSSFDrawing drawing = (XSSFDrawing) sheet.createDrawingPatriarch();
		   		           XSSFClientAnchor anchor = (XSSFClientAnchor) helper.createClientAnchor();
		   		
		   		         
		   		           if (y % 2 == 1) {//홀
		   		        	
		   		        	   anchor.setCol1(3);
		   		        	   anchor.setCol2(6);
		   		        	   anchor.setRow1(odd1);
							   anchor.setRow2(odd2);
							   
							   sheet.getRow(odd3).getCell(3).setCellValue(F_DESC);
							   odd1 = odd1 + 10;
							   odd2 = odd2 + 10;
							   odd3 = odd3 + 10;
		   		           }else {
		   		        	   anchor.setCol1(0);//0부터
			   		           anchor.setCol2(3);
			   		           anchor.setRow1(even1);
			   		           anchor.setRow2(even2);
			   		           
			   		           sheet.getRow(even3).getCell(0).setCellValue(F_DESC);
			   		        
			   		           even1 = even1 + 10;
			   		           even2 = even2 + 10;
			   		           even3 = even3 + 10;
		   		           }
		   		           anchor.setDx1(0); 
		   		           anchor.setDx2(1000);  
		   		           anchor.setDy1(0); 
		   		           anchor.setDy2(1000);
		   		           
			   		       //이미지 넣기   
			   		       XSSFPicture picture = (XSSFPicture)  drawing.createPicture(anchor,pictureIdx);
			   		    pictureIdx++;
 	       	 		   }
 	       	 	   }
 	       	 	   
		           
	         }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ips != null) ips.close();
        }

    }
    
    private String getDisposition(String filename, String browser) throws UnsupportedEncodingException {
        String dispositionPrefix = "attachment;filename=";
        String encodedFilename = null;
        if (browser.equals("MSIE")) {
            encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
        } else if (browser.equals("Firefox")) {
            encodedFilename = "\"" + new String(filename.getBytes(StandardCharsets.UTF_8), "8859_1") + "\"";
        } else if (browser.equals("Opera")) {
            encodedFilename = "\"" + new String(filename.getBytes(StandardCharsets.UTF_8), "8859_1") + "\"";
        } else if (browser.equals("Chrome")) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < filename.length(); i++) {
                char c = filename.charAt(i);
                if (c > '~') {
                    sb.append(URLEncoder.encode("" + c, "UTF-8"));
                } else {
                    sb.append(c);
                }
            }
            encodedFilename = sb.toString();
        }
        
        String rtn = "";
    	if (browser.equals("Chrome")) {
    		rtn = "attachment;filename=\"" + encodedFilename+ "\"";
    	}else {
    		rtn = dispositionPrefix + encodedFilename;
    	}
        
        return rtn;
    }    
    
    private String getBrowser(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        if (header.indexOf("MSIE") > -1 || header.indexOf("Trident") > -1)
            return "MSIE";
        else if (header.indexOf("Chrome") > -1)
            return "Chrome";
        else if (header.indexOf("Opera") > -1)
            return "Opera";
        return "Firefox";
    }    

    public XSSFCellStyle createHeaderCellStyle(Workbook workbook) {
        XSSFCellStyle xssfCellStyle = (XSSFCellStyle) workbook.createCellStyle();
        XSSFColor xssfColor = new XSSFColor(new Color(217,217,217));
        Font font = workbook.createFont();
        font.setBold(true);

        xssfCellStyle.setBorderTop(BorderStyle.MEDIUM);
        xssfCellStyle.setBorderBottom(BorderStyle.THIN);
        xssfCellStyle.setBorderLeft(BorderStyle.THIN);
        xssfCellStyle.setBorderRight(BorderStyle.THIN);

        xssfCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        xssfCellStyle.setAlignment(HorizontalAlignment.CENTER);
        xssfCellStyle.setFillForegroundColor(xssfColor);
        xssfCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        xssfCellStyle.setFont(font);

        return xssfCellStyle;
    }

    public XSSFCellStyle createDefaultCellStyle(Workbook workbook) {
        XSSFCellStyle xssfCellStyle = (XSSFCellStyle) workbook.createCellStyle();

        xssfCellStyle.setBorderTop(BorderStyle.THIN);
        xssfCellStyle.setBorderBottom(BorderStyle.THIN);
        xssfCellStyle.setBorderLeft(BorderStyle.THIN);
        xssfCellStyle.setBorderRight(BorderStyle.THIN);

        xssfCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        xssfCellStyle.setAlignment(HorizontalAlignment.CENTER);

        return xssfCellStyle;
    }
    @SuppressWarnings("static-access")
 	public static HashMap<String, Object> getParameterMap(HttpServletRequest request) throws Exception {

    	HashMap<String, Object> paramMap = new HashMap<String, Object>();

         Enumeration<String> e = request.getParameterNames();
         while (e.hasMoreElements()) {
             String key = e.nextElement();
             String[] values = request.getParameterValues(key);
             if (values.length == 1) {
                 paramMap.put(key, values[0].trim());
             } else {
                 paramMap.put(key, values);
             }
         }
         
         /** Push Map to Request Attribute **/
		Enumeration<String> attributeNames = (Enumeration<String>)request.getAttributeNames();
		while (attributeNames.hasMoreElements()) {
			String key = attributeNames.nextElement();
			Object obj = request.getAttribute(key);
			if (obj instanceof java.lang.String) {
				paramMap.put(key, obj);
			}
		}
 		
//    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//    	try {
//    		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
//    		paramMap.put("login_user_id", user.getE_ID());	
//    	} catch (Exception ex) {
//    		paramMap.put("login_user_id", "NOTLOGIN");
//    		ex.printStackTrace();
//    	}

		System.out.println("paramMap===>"+paramMap.toString()); 		

		return paramMap;
     } 
}
