package com.usom.web.controller.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.framework.model.exception.ApplicationException;

/**
 * 각 상황별 Exception을 catch하여 원하는 에러페이지를 호출하는 통합 Exception 관리 Controller Class(over Spring 3.2 version)
 * @author ash
 * @note e.getMessage()를 리턴하는경우 query노출 및 보안상 문제가 되어 삭제함
 */
@ControllerAdvice
public class ExceptionControllerAdvice {
	
	@ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception e) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("errName", e.getClass().getSimpleName());
//		mav.addObject("errMessage", e.getMessage());
		mav.addObject("errMessage", "에러가 발생되었습니다.");
		//mav.setViewName("omas/errors/exception.jsp");
//		e.printStackTrace();2020
		return mav;
    }
	
    //Ajax 에러도 이쪽으로 오는듯..
    @ExceptionHandler(RuntimeException.class)
    public ModelAndView handleRuntimeException(RuntimeException e) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("errName", e.getClass().getSimpleName());
//		mav.addObject("errMessage", e.getMessage());
		mav.addObject("errMessage", "런타임 에러가 발생되었습니다.");
		//mav.setViewName("omas/errors/exception.jsp");
//		e.printStackTrace();2020
		return mav;
    }
    
    //사용자 App Exception
    @ExceptionHandler(ApplicationException.class)
    public ModelAndView applicationException(ApplicationException e) {
    	ModelAndView mav = new ModelAndView();
    	mav.addObject("errName", e.getClass().getSimpleName());
		mav.addObject("errMessage", e.getMessage());
    	//mav.setViewName("omas/errors/exception.jsp");
    	//e.printStackTrace();
    	return mav;
    }
    
    
}
