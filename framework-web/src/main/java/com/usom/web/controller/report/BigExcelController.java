package com.usom.web.controller.report;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.usom.model.config.Configuration;
import com.usom.model.service.common.ExcelCommonService;

import net.sf.jxls.transformer.XLSTransformer;

@Controller
@RequestMapping("/bigexcel")
public class BigExcelController {

	@Autowired
    private Environment environment;

    @Autowired
    private ExcelCommonService excelCommonService;
    
    final int MAX_SIZE = 200;
    final int FIRST_SIZE = 0;
    
    @RequestMapping("/downloadBigExcel")//http://localhost:8080/excel/downloadExcel.do
    public void downloadBigExcel(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException, Exception {
//        HashMap<String, Object> paramMap = new HashMap<String, Object>();
       
    	
    	HashMap<String, Object> map = new HashMap<String, Object>();
        HashMap<String, Object> param = new HashMap<String, Object>();
		param = getParameterMap(req);
		
        String excelDir = Configuration.REPORT_FILE_TEMPLATE_PATH + "\\new\\";
        
        InputStream is = null;
        XSSFWorkbook workbook = null;
        FileOutputStream fos = null;
        FileInputStream fileInputStream = null;
        SXSSFWorkbook sxssfWorkbook = null;
        System.out.println("ssss여기");

        try {
            String templateFileName = (String)param.get("EXCEL_ID");
            String templateFileOrgName = "excel";
            
            SimpleDateFormat time = new SimpleDateFormat("yyyyMMddhhmmss");
            Date today = new Date();
            String date = time.format(today);
            
            is = new BufferedInputStream(new FileInputStream(excelDir + File.separator + templateFileName + ".xlsx"));
    		XLSTransformer xls = new XLSTransformer();
    		ArrayList<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
    		String[] Keys = null;
		    System.out.println(new Date());
            switch(templateFileName) {
    	        case "010303_big": { //잔존수명
    	        	 String[] cols = {
    	        			 "","RNUM","ASSET_CD","ASSET_NM"
    	        			 ,"CLASS1_CD","CLASS2_CD" ,"CLASS3_CD","CLASS4_CD"
    	        			 ,"CLASS5_CD","CLASS6_CD","CLASS7_CD","CLASS8_CD"
    	        			 ,"YEAR_OF_INSTALLATION", "LIFE", "YEARS_OF_USE", "LIFE_NUM"
    	        			 ,"RESTRICT_LIFE_YN", "RESTRICT_LIFE_NUM" 
    	        			 ,"VOLUME_LIFE_YN","VOLUME_LIFE_NUM"
    	        			 ,"SERVICE_LIFE_YN","SERVICE_LIFE_NUM"
    	        			 ,"ECONOMY_LIFE_YN","ECONOMY_LIFE_NUM" 
    	        			 ,"PHYSICAL_LIFE_METHOD_NM","PHYSICAL_LIFE_RESULT","PHYSICAL_LIFE_NUM"
    				 };
    	        	 Keys = cols;
    	        	 
    	        	 HashMap<String, Object> info = excelCommonService.getBig_Info_010303(param);
    	        	 map.put("info", info);
    	        	 
    	      		 data = excelCommonService.getBig_010303(param);
    	      		 
    	      		 templateFileOrgName = "잔존수명";
    	      	 } break;
    	        case "010307_big": { //재평가
   	        	 String[] cols = {
   	        			 "", "ROWNUM", "ASSET_CD","ASSET_NM"
   	        			 , "CLASS1_CD", "CLASS2_CD", "CLASS3_CD", "CLASS4_CD"
   	        			 , "CLASS5_CD", "CLASS6_CD", "CLASS7_CD", "CLASS8_CD", "CLASS9_CD"
   	        			 , "RE_EST_AMT", "REPURCHASE_AMT"
   	        			 , "LIFE_NUM", "USEFUL_LIFE"
   	        			 , "PRICE_INDEX", "WEIGHT_NUM" 
   				 };
   	        	 Keys = cols;
   	        	 
   	        	 HashMap<String, Object> info = excelCommonService.getBig_Info_010307(param);
   	        	 map.put("info", info);
   	        	 
   	      		 data = excelCommonService.getBig_010307(param);
   	      		 
   	      		 templateFileOrgName = "재평가";
   	      	 }break;
	         case "010306_big": { //생애주기 비용 분석
  	        	 String[] cols = {
  	        			 "", "ASSET_NM", "INIT_COST", "OP_COST", "MT_COST"
  	        			 , "RE_COST", "IMP_COST", "DI_COST", "TOTAL_COST_AMT"
  				 };
  	        	 Keys = cols;
  	        	 
  	        	HashMap<String, Object> asset = excelCommonService.getList1_010306_2(param);
 	    	   
  	        	map.put("flist", asset.get("flist"));
  	        	map.put("ftotal", asset.get("ftotal"));
 	    	   
  	        	map.put("nlist", asset.get("nlist"));
  	        	map.put("ntotal", asset.get("ntotal"));
 	    	   
  	        	map.put("ttotal", asset.get("ttotal"));
  	        	 
  	        	data = excelCommonService.getBig_010306(param);
  	      		 
  	        	templateFileOrgName = "생애주기_비용_분석";
  	      	}break;
	         case "010401_big": { //최적투자
  	        	 String[] cols = {
  	        			"", "PRIORITY", "ASSET_CD", "ASSET_NM"
  	        			, "LEVEL_1", "LEVEL_2", "LEVEL_3", "LEVEL_4", "LEVEL_5" 
  	        			, "LEVEL_6", "LEVEL_7", "LEVEL_8", "LEVEL_9"
  	        			, "RPN_SCORE", "PRIORITY", "MIN_NM", "MIN_FACILITY_AMT"
  				 };
  	        	 Keys = cols;
  	        	 
  	        	 HashMap<String, Object> info = excelCommonService.getBig_Info_010401(param);
  	        	 map.put("info", info);
  	        	 
  	      		 data = excelCommonService.getBig_010401(param);
  	      		 
  	        	 templateFileOrgName = "최적_투자_계획_보고서";
  	      	}break;
            }//스위치 종료
            
            //양식 변경
            workbook = (XSSFWorkbook) xls.transformXLS(is, map);
            setCellMarge(templateFileName, workbook, map);//셀 병합
            
            //워크북 복사
            Sheet originSheet = workbook.getSheetAt(FIRST_SIZE);
            int rowNo = originSheet.getLastRowNum();
            System.out.println(rowNo);
            originSheet.removeRow(originSheet.getRow(rowNo));
            
        	sxssfWorkbook = new SXSSFWorkbook(workbook, MAX_SIZE);
        	sxssfWorkbook.setCompressTempFiles(true);
        	SXSSFSheet  sheet = sxssfWorkbook.getSheetAt(FIRST_SIZE);
        	
        	//스타일
        	final XSSFCellStyle cellStyle = workbook.createCellStyle();
    		cellStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 252, 252)));
    		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    		cellStyle.setBorderLeft(BorderStyle.THIN);
    		cellStyle.setBorderTop(BorderStyle.THIN);
    		cellStyle.setBorderRight(BorderStyle.THIN);
    		cellStyle.setBorderBottom(BorderStyle.THIN);
    		
            //데이터 입력
     		int index = 0;
     		Object value = null;
     		for(HashMap<String, Object> item : data) {
     			SXSSFRow  row = sheet.createRow(rowNo);
                for(index = 1; index < Keys.length; index++) {
	               	value = item.get(Keys[index]);
	               	
	               	row.createCell(index).setCellValue(value != null ? value.toString() : "");
	               	sheet.getRow(rowNo).getCell(index).setCellStyle(cellStyle);
                }
               
                if(rowNo % MAX_SIZE == 0) sheet.flushRows(MAX_SIZE);
                
                rowNo++;
            }
     		 
            String excelFileName = templateFileOrgName + "-" + date + ".xlsx";
            res.setHeader("Content-Disposition", getDisposition(excelFileName, getBrowser(req)));
            res.setHeader("Content-Transfer-Encoding", "binary");
            res.setContentType("application/x-msexcel");
            res.setContentType("application/octet-stream; charset=utf-8");
           
            sxssfWorkbook.write(res.getOutputStream());
            System.out.println(new Date());
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) is.close();
            if (workbook != null) workbook.close();
            if (fos != null) fos.close();
            if (fileInputStream != null) fileInputStream.close();
            if (sxssfWorkbook != null) sxssfWorkbook.dispose();
        }
    }
    
    private void setCellMarge(String templateFileName, Workbook workbook, HashMap<String, Object> map) throws Exception {
    	String imageDir = Configuration.WEB_ROOT_PATH;
    	InputStream ips = null; 
    	try {
    		 switch (templateFileName) {
    		 case "010306_big": {
    			 ArrayList<HashMap<String, Object>> array = (ArrayList<HashMap<String, Object>>)map.get("nlist");
    	         if(array.size() == 0) return;
    			 XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(2);
    	         final int START_ROW = 3;
    	         //찾는 id
    	         String strTemp = "";
    	         String strPrve = array.get(0).get("BL_NAME").toString();
    	         int tempIndex = 0;
    	         for(int row = 0; row < array.size(); row++) {
    	        	 //id가 같은지 찾는다
    	        	 strTemp = array.get(row).get("BL_NAME") == null ? "":array.get(row).get("BL_NAME").toString();
    	        	 if(!strPrve.equals(strTemp)) {
    	        		 int start = START_ROW + tempIndex;
    	        		 int end = START_ROW + (row - 1);
    	        		 //중복없이 혼자인 경우는 병합을 못함
    	        		 if(start != end) {
    	        			 // 셀 병합(시작열, 종료열, 시작행, 종료행)
    	        			 sheet.addMergedRegion(new CellRangeAddress(start, end, 1, 1));
    	        		 }
    	        		 //찾는 id를 변경 
    	        		 strPrve = array.get(row).get("BL_NAME") == null ? "":array.get(row).get("BL_NAME").toString();
    	        		 tempIndex = row ;
    	        	 }
    	         }
		 	}break;
			}
    		 
    	} catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        if (ips != null) ips.close();
	    }
    }
    
    
    private String getDisposition(String filename, String browser) throws UnsupportedEncodingException {
        String dispositionPrefix = "attachment;filename=";
        String encodedFilename = null;
        if (browser.equals("MSIE")) {
            encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
        } else if (browser.equals("Firefox")) {
            encodedFilename = "\"" + new String(filename.getBytes(StandardCharsets.UTF_8), "8859_1") + "\"";
        } else if (browser.equals("Opera")) {
            encodedFilename = "\"" + new String(filename.getBytes(StandardCharsets.UTF_8), "8859_1") + "\"";
        } else if (browser.equals("Chrome")) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < filename.length(); i++) {
                char c = filename.charAt(i);
                if (c > '~') {
                    sb.append(URLEncoder.encode("" + c, "UTF-8"));
                } else {
                    sb.append(c);
                }
            }
            encodedFilename = sb.toString();
        }
        
        String rtn = "";
    	if (browser.equals("Chrome")) {
    		rtn = "attachment;filename=\"" + encodedFilename+ "\"";
    	}else {
    		rtn = dispositionPrefix + encodedFilename;
    	}
        
        return rtn;
    }    
    
    private String getBrowser(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        if (header.indexOf("MSIE") > -1 || header.indexOf("Trident") > -1)
            return "MSIE";
        else if (header.indexOf("Chrome") > -1)
            return "Chrome";
        else if (header.indexOf("Opera") > -1)
            return "Opera";
        return "Firefox";
    }    

    public XSSFCellStyle createHeaderCellStyle(Workbook workbook) {
        XSSFCellStyle xssfCellStyle = (XSSFCellStyle) workbook.createCellStyle();
        XSSFColor xssfColor = new XSSFColor(new Color(217,217,217));
        Font font = workbook.createFont();
        font.setBold(true);

        xssfCellStyle.setBorderTop(BorderStyle.MEDIUM);
        xssfCellStyle.setBorderBottom(BorderStyle.THIN);
        xssfCellStyle.setBorderLeft(BorderStyle.THIN);
        xssfCellStyle.setBorderRight(BorderStyle.THIN);

        xssfCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        xssfCellStyle.setAlignment(HorizontalAlignment.CENTER);
        xssfCellStyle.setFillForegroundColor(xssfColor);
        xssfCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        xssfCellStyle.setFont(font);

        return xssfCellStyle;
    }

    public XSSFCellStyle createDefaultCellStyle(Workbook workbook) {
        XSSFCellStyle xssfCellStyle = (XSSFCellStyle) workbook.createCellStyle();

        xssfCellStyle.setBorderTop(BorderStyle.THIN);
        xssfCellStyle.setBorderBottom(BorderStyle.THIN);
        xssfCellStyle.setBorderLeft(BorderStyle.THIN);
        xssfCellStyle.setBorderRight(BorderStyle.THIN);

        xssfCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        xssfCellStyle.setAlignment(HorizontalAlignment.CENTER);

        return xssfCellStyle;
    }
    
    @SuppressWarnings("static-access")
 	public static HashMap<String, Object> getParameterMap(HttpServletRequest request) throws Exception {

    	HashMap<String, Object> paramMap = new HashMap<String, Object>();

         Enumeration<String> e = request.getParameterNames();
         while (e.hasMoreElements()) {
             String key = e.nextElement();
             String[] values = request.getParameterValues(key);
             if (values.length == 1) {
                 paramMap.put(key, values[0].trim());
             } else {
                 paramMap.put(key, values);
             }
         }
         
         /** Push Map to Request Attribute **/
		Enumeration<String> attributeNames = (Enumeration<String>)request.getAttributeNames();
		while (attributeNames.hasMoreElements()) {
			String key = attributeNames.nextElement();
			Object obj = request.getAttribute(key);
			if (obj instanceof java.lang.String) {
				paramMap.put(key, obj);
			}
		}
 		
//    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//    	try {
//    		User user = ((UserDetailsServiceVO) auth.getPrincipal()).getUser();
//    		paramMap.put("login_user_id", user.getE_ID());	
//    	} catch (Exception ex) {
//    		paramMap.put("login_user_id", "NOTLOGIN");
//    		ex.printStackTrace();
//    	}

		System.out.println("paramMap===>"+paramMap.toString()); 		

		return paramMap;
     } 
}
