package com.usom.web.controller.user;

import java.net.URLEncoder;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.usom.model.service.common.CommonService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.web.WebAttributes;

import com.usom.model.vo.user.UserDetailsServiceVO;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.usom.model.service.common.CommonService;
import com.usom.model.service.user.UserDetailsServiceImpl;
import javax.annotation.Resource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping
public class LoginController {
	
    @Resource(name = "commonService")
    private CommonService commonService;
	
	@RequestMapping(value = "/login")
	public ModelAndView login(@RequestParam(required = false) String message) throws Exception {
		ModelAndView mav = new ModelAndView();
		mav.addObject("message", message);
		mav.setViewName("login/login");
		return mav;
	}
	
	//접근권한 제한시
	@RequestMapping(value = "/denied")
	public String denied() {
		//return "omas/login/denied.jsp";
		return "redirect:/";
	}
	
	@RequestMapping(value = "/login/success")
	public String loginSuccess() throws Exception {
		//String message = "아이디 또는 비밀번호가 맞지않습니다.";
		return "redirect:/";
	}
	
	@RequestMapping(value = "/login/failure")
	public String loginFailure(HttpServletRequest request) throws Exception {
		
		String errorMessage = "";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AuthenticationException ex = (AuthenticationException) request.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
			if (ex != null) {
			  errorMessage = ex.getMessage();
			}
		return "redirect:/login?message=" + URLEncoder.encode(errorMessage, "UTF-8");
	}
	//	@RequestMapping(value = "/login/failure")
	//	public void loginFailure(HttpServletRequest request, HttpServletResponse response) throws Exception {
	//		response.setContentType("text/html; charset=UTF-8");
	//		PrintWriter out = response.getWriter();
	//		out.println("<script>alert('아이디 또는 비밀번호가 맞지않습니다.'); history.go(-1);</script>");
	//		out.flush();
	//	}
	
	@RequestMapping(value = "/login/duplicate")
	public String loginDuplicate() throws Exception {
		String message = "같은아이디로 중복로그인 되었습니다.";
		return "redirect:/login?message=" + URLEncoder.encode(message, "UTF-8");
	}
	//	@RequestMapping(value = "/login/duplicate")
	//	public void loginDuplicate(HttpServletRequest request, HttpServletResponse response) throws Exception {
	//		response.setContentType("text/html; charset=UTF-8");
	//		PrintWriter out = response.getWriter();
	//		out.println("<script>alert('아이디 또는 비밀번호가 맞지않습니다.'); history.go(-1);</script>");
	//		out.flush();
	//	}
	
	@RequestMapping(value = "/logout/success")
	public String logoutSuccess(HttpServletRequest request) {
/*		HashMap<String, Object> param2 = new HashMap<String, Object>();
		try {
			String user_id = request.getSession().getAttribute("logout_user_id").toString();
			param2.put("lg_opt", "o");//1:로그인, 2:출력, 3:에러 , 4: 자산추가 , 5: 자산수정 , 6: 자산삭제 , 7: 자산폐기
			param2.put("lg_desc", user_id + "로그아웃");
			param2.put("login_user_id", user_id);
			 
			commonService.insertSystemLog(param2);
			
		}catch(Exception e) {
			//System.out.print(" db 에러 param ==>" + param.toString());
		}
	*/	
		return "redirect:/login";
	}
	
	@Resource(name="userDetailsService")
	protected UserDetailsService userDetailsService;

	@RequestMapping(value="/ssoLogin", method=RequestMethod.GET)
	public String ssologin(HttpServletRequest request) throws Exception{
		
		String user_id = (String) request.getSession().getAttribute("user_id").toString();
		
	    UserDetails ckUserDetails = userDetailsService.loadUserByUsername(user_id);
	    Authentication authentication = new UsernamePasswordAuthenticationToken(ckUserDetails, "USER_PASSWORD", ckUserDetails.getAuthorities());

	    SecurityContext securityContext = SecurityContextHolder.getContext();
	    securityContext.setAuthentication(authentication);
	    HttpSession session = request.getSession(true);
	    session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
		
	    return "redirect:/login/success";
	    
	}
}
