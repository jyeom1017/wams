angular.module('app').provider('runtimeStates', runtimeStates);

function runtimeStates($stateProvider, $urlRouterProvider) {
    //console.log('router-provider == : 1.5 ');
    this.$get = function($q, $timeout, $state) {

        //console.log('router-provider == : 2 ');
        var state = {
            removeState: function() {
                //$state
            },
            // 사용자 기본 화면
            userDefaultState: function() {

                $urlRouterProvider.otherwise('/');

                /*
                 $urlRouterProvider.otherwise(function($injector, $location) {
                		var $state = $injector.get('$state');
                });
                */

                //console.log('router-provider == : 4 ');

                // 메뉴 화면
                $stateProvider
                    .state('otherwise', {
                        url: '/',
//                        controller: 'nullController as main',
                        templateUrl: 'view/errors/404.html',
                        requireLogin: true
                    })
                    // sample start
                    /* .state(
                        'main', {
                        	url: '/',
                            controller: 'nullController as main',
                            templateUrl: 'view/error/404.html',
                            requireLogin: true
                        })  */                  
                    .state(
                        'login', {
                            url: '/loginSuccess',
                            controller: 'LoginController as main',
                            templateUrl: 'view/login/login.html',
                            requireLogin: false
                        })
                    .state(
                        'userManager', {
                            url: '/userManager',
                            controller: 'adminController as main',
                            templateUrl: 'view/admin/userManager.html',
                            requireLogin: false
                        })
                    .state(
                        'fileManager', {
                            url: '/fileManager',
                            controller: 'adminController as main',
                            templateUrl: 'view/admin/fileManager.html',
                            requireLogin: false
                        })
                    .state(
                        'menuManager', {
                            url: '/menuManager',
                            controller: 'adminController as main',
                            templateUrl: 'view/admin/menuManager.html',
                            requireLogin: true
                        })
                    .state(
                        'codeManager', {
                            url: '/codeManager',
                            controller: 'adminController as main',
                            templateUrl: 'view/admin/codeManager.html',
                            requireLogin: true
                        })
                    .state(
                        'grantManager', {
                            url: '/grantManager',
                            controller: 'adminController as main',
                            templateUrl: 'view/admin/grantManager.html',
                            requireLogin: true
                        })

                    .state(
                        '010110', {//로그관리
                            url: '/010110',
                            controller: 'logController as main',
                            templateUrl: 'view/admin/log.html',
                            requireLogin: true
                        })                        

                    //관리자 - 연계관리
                    .state(
                        'menu', {
                            url: '/menu',
                            //controller: 'sysManController as main',
                            controller: 'adminController as main',
                            templateUrl: 'view/admin/menu.html',
                            requireLogin: false
                        })
                    //관리자 - 연계관리
                    .state(
                        'relay', {
                            url: '/relay',
                            controller: 'relayController as main',
                            templateUrl: 'view/system/relay.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                    //관리자 - 사용자관리
                    .state(
                        '010106', {
                            url: '/010106',
                            controller: 'sysManController as main',
                            templateUrl: 'view/system/sysManage.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })                         
                    // 레벨 코드 관리
                    .state(
                        '010003', 
                        {
                            url: '/010003',
                            controller: 'assetController as main',
                            templateUrl: 'view/asset/levelCode.html',
                        })
                     // 인벤토리 경로 관리
                    .state(
                        '010004',
                        {
                            url: '/010004',
                            controller: 'itemController as main',
                            templateUrl: 'view/asset/item.html',
                        })
                     // 자산 기준정보 관리
                     .state(
                        '010005',
                        {
                        	url: '/010005',
                        	controller: 'assetStandardController as main',
                        	templateUrl: 'view/asset/assetStandard.html',
                        })
                	//정수/관망 시설 관리
                    .state(
                        '010102', 
                        {
                            url: '/010102',
                            controller: 'itemListController as main',
                            templateUrl: 'view/asset/itemlist.html',
                        })                        
                    //운영관리 - 자산비용관리
                    .state(
                        '011507', {
                            url: '/011507',
                            controller: 'costManageController as main',
                            templateUrl: 'view/operation/costManagement.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })                    
                    //운영관리 - 운영관리
                    .state(
                        '010201', {
                            url: '/010201',
                            controller: 'operationController as main',
                            templateUrl: 'view/operation/operation.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                    //개보수 사업관리
                     .state(
                        '010202', {
                            url: '/010202',
                            controller: 'renovationController as main',
                            templateUrl: 'view/operation/renovation.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //조사및탐사관리
                	.state(
                        '010203', {
                            url: '/010203',
                            controller: 'investigationController as main',
                            templateUrl: 'view/operation/investigation.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //nfc
                	.state(
                        '010204', {
                            url: '/010204',
                            controller: 'nfcController as main',
                            templateUrl: 'view/operation/nfc.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //민원
                	.state(
                        '010205', {
                            url: '/010205',
                            controller: 'minwonController as main',
                            templateUrl: 'view/operation/minwon.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //평가분석 - 기술진단자료
                	.state(
                        '010301', {
                            url: '/010301',
                            controller: 'technicalDiagController as main',
                            templateUrl: 'view/new_diagnosis/technicalDiag.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //평가분석 - 간접상태평가
                	.state(
                        '010302', {
                            url: '/010302',
                            controller: 'stateDiagController as main',
                            templateUrl: 'view/new_diagnosis/stateDiag.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //평가분석 - 직접상태평가
                	.state(
                        '011506', {
                            url: '/011506',
                            controller: 'state2DiagController as main',
                            templateUrl: 'view/new_diagnosis/stateDiag2.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })                        
                     //평가분석 - 잔존수명
                	.state(
                        '010303', {
                            url: '/010303',
                            controller: 'remainController as main',
                            templateUrl: 'view/new_diagnosis/remain.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //평가분석 - 위험도
                	.state(
                        '010304', {
                            url: '/010304',
                            controller: 'riskController as main',
                            templateUrl: 'view/new_diagnosis/risk.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //평가분석 - 서비스수준
                	.state(
                        '010305', {
                            url: '/010305',
                            controller: 'losController as main',
                            templateUrl: 'view/new_diagnosis/los.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //평가분석 - 생애주기비용
                	.state(
                        '010306', {
                            url: '/010306',
                            controller: 'lccController as main',
                            templateUrl: 'view/new_diagnosis/lcc.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //평가분석 - 자산가치 재평가
                	.state(
                        '010307', {
                            url: '/010307',
                            controller: 'reevalController as main',
                            templateUrl: 'view/new_diagnosis/reevaluation.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //재무분석 - 최적투자 계획 
                	.state(
                        '010401', {
                            url: '/010401',
                            controller: 'oipController as main',
                            templateUrl: 'view/finance/oip.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //재무분석 - 재정 계획
                	.state(
                        '010402', {
                            url: '/010402',
                            controller: 'financeController as main',
                            templateUrl: 'view/finance/financialplan.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })                    
                     //재무분석 - 관리 계획
                	.state(
                        '010403', {
                            url: '/010403',
                            controller: 'assetManageController as main',
                            templateUrl: 'view/finance/assetmanage.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                     //gis - 시설물관리
                	.state(
                        '010601', {
                            url: '/010601',
                            controller: 'GisController as main',
                            templateUrl: 'view/gis/gis.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                        
                	.state(
                        'gisdeveloper', {
                            url: '/gisdeveloper',
                            controller: 'gisExample1Controller as main',
                            templateUrl: 'view/gis/gis1.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                	.state(
                        'gisEdit', {
                            url: '/gisEdit',
                            controller: 'gisExample2Controller as main',
                            templateUrl: 'view/gis/gisEdit.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })                        
                     //gis - 단차분석
                	.state(
                        '010604', {
                            url: '/010604',
                            controller: 'stateDiagController as main',
                            templateUrl: 'view/gis/pipeLevelAnalysis.html',
                            params : {tabid : {}},
                            requireLogin: true
                        })
                	.state(
                            'gis',
                            {
                                url: '/gis',
                                controller: 'GisIframeController as main',
                                templateUrl: 'view/gis/gis2.html',
                            })
                	.state(
                            'gis1',
                            {
                                url: '/gis1',
                                controller: 'GisIframeController as main',
                                templateUrl: 'view/gis/gis1.html',
                            })
                    .state(
                            'gis2',
                            {
                                url: '/gis2',
                                controller: 'GisIframeController as main',
                                templateUrl: 'view/gis/gis2.html',
                            })
                            /*
                	.state(
                            'gis3',
                            {
                                url: '/gis3',
                                controller: 'GisIframeController as main',
                                templateUrl: 'view/gis/gis3.html',
                            })
                	.state(
                            'gis4',
                            {
                                url: '/gis4',
                                controller: 'gisExample3Controller as main',
                                templateUrl: 'view/gis/gis4.html',
                            })
                	.state(
                            'gis5',
                            {
                                url: '/gis5',
                                controller: 'gisNController as main',
                                templateUrl: 'view/gis/gis5.html',
                            })
                	.state(
                            'gis6',
                            {
                                url: '/gis6',
                                controller: 'gisNController as main',
                                templateUrl: 'view/gis/gis6.html',
                            })                            
                	.state(
                            'gis7',
                            {
                                url: '/gis7',
                                controller: 'gisNController as main',
                                templateUrl: 'view/gis/gis7.html',
                            })
                	.state(
                            'gis8',
                            {
                                url: '/gis8',
                                controller: 'gisNController as main',
                                templateUrl: 'view/gis/gis8.html',
                            })
                	.state(
                            'gis9',
                            {
                                url: '/gis9',
                                controller: 'gisNController as main',
                                templateUrl: 'view/gis/gis9.html',
                            })
                	.state(
                            'gis10',
                            {
                                url: '/gis10',
                                controller: 'gisNController as main',
                                templateUrl: 'view/gis/gis10.html',
                            })
                	.state(
                            'gis11',
                            {
                                url: '/gis11',
                                controller: 'gisNController as main',
                                templateUrl: 'view/gis/gis11.html',
                            })                            
                	.state(
                            'gis12',
                            {
                                url: '/gis12',
                                controller: 'gisNController as main',
                                templateUrl: 'view/gis/gis12.html',
                            })*/
                            //통계분석
                	.state(
                        '010801', {
                            url: '/010801?tabId',
                            controller: 'dataAnalysisController as main',
                            templateUrl: 'view/statistics/index.html',
                            params : {'tabId' : {value:'1', squash:false}}
                        })
                    //게시판
                    .state(
                            '011501',
                            {
                                url: '/011501',
                                controller: 'bbsController as main',
                                templateUrl: 'view/bbs/notice.html',
                            })
                    .state(
                            '011502',
                            {
                                url: '/011502',
                                controller: 'bbsController as main',
                                templateUrl: 'view/bbs/qna.html',
                            })
                    .state(
                            '011503',
                            {
                                url: '/011503',
                                controller: 'bbsController as main',
                                templateUrl: 'view/bbs/pbb.html'
                            })
            		.state(
                            'main',
                            {
                                url: '/main',
                                controller: 'mainPageController as main',
                                templateUrl: 'view/main/main.html'
                            })
            		.state(
                            'help',
                            {
                                url: '/help',
                                templateUrl: 'view/book/help.html'
                            })
                            ;
            } //end defaultState
        } //state
        return state;
    }
}
