
var createNewRanges = function createNewRanges(timeRanges, playbackRate) {
  var newRanges = [];
	//console.log('start:'+timeRanges.start(0));
	//console.log('end:'+timeRanges.start(0));
  for (var i = 0; i < timeRanges.length; i++) {
    newRanges.push([timeRanges.start(i) , timeRanges.end(i) ]);
  }

  return videojs.createTimeRange(newRanges);
};

var playbackrateAdjuster = function playbackrateAdjuster(player) {
  var tech = void 0;
  
  player.on('ratechange', function () {
    tech.trigger('durationchange');
    tech.trigger('timeupdate');
  });

  return {
    setSource: function setSource(srcObj, next) {
      next(null, srcObj);
    },
    setTech: function setTech(newTech) {
      tech = newTech;
    },
    duration: function duration(dur) {
	if(typeof player.durationEmpty!='undefined'){
		return dur + player.durationEmpty;	
	}else{
		return dur;
	}
	
    },
    currentTime: function currentTime(ct) {
		var timeinfo = player.timeinfo;
		var timeinfo2=player.timeinfo2;
		var start_time = player.start_time;
		var emptyPlayTime = 0.0;
		if(typeof player.timeinfo!='undefined'){
		for(var i=0;i<timeinfo2.length;i++){
			if(i<timeinfo2.length-1){
				if(timeinfo[i].durationSum > ct ){
				emptyPlayTime=timeinfo2[i].durationSum;
				if((player.sectionID!=i ) && player.syncMode==true){
					player.pause();
					player.hide();
					player.sectionEmpty = true; 
				}
				player.sectionID=i;
				break;
				}
			}else{
				emptyPlayTime=timeinfo2[i].durationSum;
			}
		}
		}
		//console.log(timeinfo);
		//console.log(timeinfo2);
		//console.log('ct:'+ct);
		//console.log('empty:'+emptyPlayTime);
      return ct +emptyPlayTime;
    },
    setCurrentTime: function setCurrentTime(ct) {
		var timeinfo = player.timeinfo;
		var timeinfo2=player.timeinfo2;
		//console.log(timeinfo2);
		var start_time = player.start_time;
		var emptyPlayTime = 0.0;
		if(typeof player.timeinfo!='undefined'){
		for(var i=0;i<timeinfo2.length;i++){
			if(i<timeinfo.length){
				
				if(timeinfo[i].start_time <= (ct*1000+player.start_time) && (ct*1000+player.start_time) <= timeinfo[i].end_time){
					console.log(i);
					emptyPlayTime = timeinfo2[i].durationSum;
					//console.log('1');
					player.show();
					player.sectionEmpty = false; 
					break;
				}

				if(timeinfo2[i].start_time <= (ct*1000+player.start_time) && (ct*1000+player.start_time) <= timeinfo2[i].end_time){
					emptyPlayTime = ct - (timeinfo[i].durationSum - timeinfo[i].duration);
					//console.log('2');
					player.pause();
					player.hide();
					player.sectionEmpty = true; 
					break;
				}
			}
			else if(timeinfo2[i].start_time <= (ct*1000+player.start_time) && (ct*1000+player.start_time) <= timeinfo2[i].end_time){
				emptyPlayTime = ct - timeinfo[i-1].durationSum;
				//console.log('3');
					player.pause();
					player.hide();
					player.sectionEmpty = true; 
				break;
			}
				/*
			if(timeinfo2[i].durationSum > ct){
				emptyPlayTime=timeinfo2[i].durationSum;
				break;
			}else if(timeinfo[i].durationSum+timeinfo2[i].durationSum > ct ){
				emptyPlayTime=timeinfo2[i].durationSum;
				break;
			}*/
		}
		}
		//console.log('ct:'+ct);
		//console.log('empty:'+emptyPlayTime);
	
      return (ct -emptyPlayTime<0)?0:(ct -emptyPlayTime);
    },
    buffered: function buffered(bf) {
      return createNewRanges(bf, player.playbackRate());
    },
    seekable: function seekable(_seekable) {
      return createNewRanges(_seekable, player.playbackRate());
    },
    played: function played(_played) {
      return createNewRanges(_played, player.playbackRate());
    }
  };
};

// Register the plugin with video.js.
videojs.use('*', playbackrateAdjuster);

// Include the version number.
playbackrateAdjuster.VERSION = '1.0.0';

//module.exports = playbackrateAdjuster;

