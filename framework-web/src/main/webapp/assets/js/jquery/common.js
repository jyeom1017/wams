//left 3depth menu
$(function() {
	return;
	var lnbUI = {
		click : function(target, speed) {
			var _self = this, $target = $(target);
			_self.speed = speed || 300;
			
			$target.each(function() {
				if (findChildren($(this))) {
					return;
				}
				$(this).addClass('noDepth');
			});
			
			function findChildren(obj) {
				return obj.find('> ul').length > 0;
			}
			
			$target.on('click', 'button', function(e) {
				e.stopPropagation();
				var $this = $(this), $depthTarget = $this.next(), $siblings = $this.parent().siblings();
				
				$this.parent('li').find('ul li').removeClass('on');
				$siblings.removeClass('on');
				$siblings.find('ul').slideUp(250);
				if (!$this.parent().hasClass('on')) {
					_self.activeOn($this);
					$depthTarget.slideDown(_self.speed);
				} else {
					$depthTarget.slideUp(_self.speed);
					_self.activeOff($this);
				}
			})
		},
		activeOff : function($target) {
			$target.parent().removeClass('on');
		},
		activeOn : function($target) {
			$target.parent().addClass('on');
		}
	}; // Call lnbUI
	$(function() {
		lnbUI.click('#lnb li', 300);
	});
	$(window).resize(function() {
		gridResize();
	});
	
	/*
	// content tab click event
	$(".tabs li button").click(function() {
		
		$(".tabs li button").removeClass("active"); // 기존선택된 active 클래스 삭제
		$(this).addClass("active"); // 새로 선택된 selected 클래스 생성
		
		$(".conts_box").hide(); // 기존의 보여진 내용 숨기기
		$('#' + $(this).data("tab")).show();// 새로 선택된 내용 href 연결된 내용 보여주기
		
		gridResize();
	});
	*/
	
	$(".conts_box:not(#" + $(".tabs li button.active").data("tab") + ")").hide();
	$(document).ready(function() {
		setTimePicker();
		setDatePicker();
		setDateTimePicker();
		setDatePickerToday();
		setDatePickerPop();
	});
});

// function gridResize(){
// $('.grid table').each(function(){
// $(this).setGridWidth($(this).parent().width()).trigger('reloadGrid');
// });
// }

function gridResize() {
	// alert('gridresize');
	$(".grid table").each(function() {
		$(this).setGridWidth($(this).parent().width());
		
	});
}
function setDatePickerPop() {
	
	var agent = navigator.userAgent.toLowerCase();
	
	$('.datePickerPop').each(function() {
		var _this = $(this);
		_this.datepicker({
			"dateFormat" : 'yy-mm-dd',
			
			fixFocusIE : false,
			
			/* blur needed to correctly handle placeholder text */
			onSelect : function(dateText, inst) {
				this.fixFocusIE = true;
				$(this).blur().change().focus();
			},
			onClose : function(dateText, inst) {
				this.fixFocusIE = true;
				$('.focusfix').focus();
			},
			beforeShow : function(input, inst) {
				var result = ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1)) ? !this.fixFocusIE : true;
				this.fixFocusIE = false;
				return result;
			}
		});
	});
	
}
function setDatePicker(options) {
	$('.datePicker').each(function() {
		// var id = $(this).attr('id');
		var _this = $(this);
		var _options = $.extend({
			"dateFormat" : 'yy-mm-dd'
		},{},options);
			//"minDate" : new Date('1910-01-01')
			//"minDate" : "-50year",
			//"maxDate" : "30year"
		_this.datepicker(_options);
	});
}
function setDatePickerToday() {
	$('.datePickerToday').each(function() {
		// var id = $(this).attr('id');
		var _this = $(this);
		_this.datepicker({
			"dateFormat" : 'yy-mm-dd'
		});
		_this.datetimepicker('setDate', new Date());
	});
}
function setTimePicker() {
	$('.timePicker').each(function() {
		// var id = $(this).attr('id');
		var _this = $(this);
		_this.timepicker({
			"timeFormat" : "H:mm"
		});
	});
}
function setDateTimePicker() {
	$('.dateTimePicker').each(function() {
		// var id = $(this).attr('id');
		var _this = $(this);
		_this.datetimepicker({
			"dateFormat" : 'yy-mm-dd',
			"timeFormat" : "H:mm"
		});
		_this.datetimepicker('setDate', 'today');
		_this.datetimepicker('setTime', new Date());
	});
}
function checkDate(startDate, endDate, limitDay, msg) {
	var startDate = startDate.split('-');
	var endDate = endDate.split('-');
	var flag = true;
	startDate = new Date(startDate[0], startDate[1] - 1, startDate[2]);
	endDate = new Date(endDate[0], endDate[1] - 1, endDate[2]);
	var result = (endDate.getTime() - startDate.getTime()) / (1000 * 3600 * 24);
	
	if (startDate.getTime() > endDate.getTime()) {
		if(typeof msg != 'undefined')
			alertify.alert("오류",msg);
		else 
			alertify.alert("Info","기간범위를 확인하세요.시작날짜가 종료날짜 보다 클수 없습니다.");
		return;
	}

	if ((limitDay != '') && (limitDay < result)) {
		alertify.alert("Info","조회기간을 확인해주십시오.[최대" + limitDay + "일까지]");
		flag = false;
	}
	return flag;
}

/* checkbox all check */
$(document).ready(function() {
	$('.check_all').click(function() {
		if ($('.check_all').prop('checked')) {
			$('.chk').prop('checked', true);
			alert(1);
		} else {
			$('.chk').prop('checked', false);
		}
	});
});

function f_checkbox(checked,id){
	//console.log(id);
	document.getElementById(id).checked = !checked;
}

