$(document).ready(function(){
	// checkbox
	$(".check_style").on("click", function  () {
		if ($(this).hasClass("check")) {
			$(this).removeClass("check").children("input").prop("checked", false);
		}else{
			$(this).addClass("check").children("input").prop("checked", true);
		}
	});

	// radio
	$(".radio_style").on("click", function  () {
		$(this).parents(".radio_wrap").find(".radio_style").removeClass("check");
		$(this).addClass("check").children("input").prop("checked", true);
		
	});

	// sortable
	$(".set_list").sortable({
		handle: ".list_header"
	});
	$(".set_list").disableSelection();

	$(".panel_sort").sortable({
		handle: ".video_list_header",
		connectWith: ".video_list",
	});
	$(".panel_sort").disableSelection();

	// video 아이콘 hover
	$(".video_list_cont").hover(
		function  () {
			$(this).children(".video_ico_list").stop().animate({"bottom":"0px"}, 300);
		},
		function  () {
			$(this).children(".video_ico_list").stop().animate({"bottom":"-28px"}, 300);
		}
	)

	$(".panel_cont9 .video_list_cont").hover(
		function  () {
			$(this).children(".video_ico_list").stop().animate({"bottom":"0px"}, 300);
		},
		function  () {
			$(this).children(".video_ico_list").stop().animate({"bottom":"-20px"}, 300);
		}
	)

	$('html').on("click", function  (e) {
		if (!$(e.target).hasClass("panel_change")) {
			$(".panel_change").hide();
		}
	});

	$(".header_over").hover(
		function  () {
			$(".header_btn.close").show().stop().animate({"top":"10px"}, 300);
		},function  () {
			$(".header_btn.close").stop().animate({"top":"-21px"}, 300);
		}
	)

	$(".wide_over_wrap").hover(
		function  () {
			$(".all_view_close").stop().animate({"top":"-1px"}, 300);
		},function  () {
			$(".all_view_close").stop().animate({"top":"-21px"}, 300);
		}
		
	)

	// datepicker

	/*$(".term_calendar input").datepicker({
		language: "kr",
		format: "yyyy-mm-dd",
		orientation: "bottom left"
	}).datepicker("setDate", "0");    

	$(".term_calendar2 input").datepicker({
		language: "kr",
		format: "yyyy-mm-dd",
		orientation: "top left"
	}).datepicker("setDate", "0");  */ 
	
	
	$(".term_area1 .input-daterange").datepicker({
		autoclose: true,
		language: "kr",
		orientation: "auto",
		format: 'yyyy/mm/dd'
	})
	$("#calendar3").datepicker({
		autoclose: true,
		language: "kr",
		orientation: "bottom auto",
		format: 'yyyy/mm/dd'
	}).datepicker("setDate", "0");

	$(".term_area2 .input-daterange").datepicker({
		autoclose: true,
		language: "kr",
		orientation: "bottom auto",
		format: 'yyyy/mm/dd',
		startView : 0
	})
	
	
	

	// bxslider
	$(".route_area ul").bxSlider({
		width:600,
		height:360
	});

	// 오디오정보
	$(".set04 .tree01 .audio_list button").on("click", function  () {
		$(this).parents(".audio_list").stop().slideUp(300)
	});
	
	var ticker = function()
	{
		
		timer = setTimeout(function(){
				$('#ticker li:first').animate( {marginTop: '-27px'}, 400, function()
				{
					$(this).detach().appendTo('ul#ticker').css("margin-top","0");
				});
				clearTimeout(timer);
				ticker();
			
		}, 60000);
	};
	//ticker();

	var ticker2 = function  () {
		timer2 = setTimeout(function(){
			$('#ticker2 li:first').animate( {marginTop: '-27px'}, 400, function()
			{
				$(this).detach().appendTo('ul#ticker2').css("margin-top","0");
			});
			clearTimeout(timer2);
			ticker2();
		}, 60000);
	}
	//ticker2();

	var ticker3 = function  () {
		timer3 = setTimeout(function(){
			$('#ticker3 li:first').animate( {marginTop: '-27px'}, 400, function()
			{
				$(this).detach().appendTo('ul#ticker3').css("margin-top","0");
			});
			clearTimeout(timer3);
			ticker3();
		}, 60000);
	}
	//ticker3();
    
    $(".search_result_list .vod_result_area>ul>li").on("click", function(){
		if($(this).hasClass("on")){
			$(this).removeClass("on");
		}else{
			$(this).addClass("on");
		}
	});
    
   // $( "#sortable" ).sortable();
    //$( "#sortable" ).disableSelection();   
	
});

// third
function thirdOpen(_this){
	var _this = $(_this);
	if($(".third_list").css("display") == "block"){
		$(".third_list").hide();
		_this.text("더보기");
		if($(".btn_set_color").hasClass("on")){
			$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","144px");
		}else{
			$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","184px");
		}	
	}else{
		$(".third_list").show();
		_this.text("닫기");
		if($(".btn_set_color").hasClass("on")){
			$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","33px");
		}else{
			$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","72px");
		}	
	}
	
}

function thirdSelect(_this){
	var _this = $(_this);
	if(_this.hasClass("on")){
		_this.removeClass("on");
	}else{
		_this.addClass("on");
	}
}

function wideStats(_this){
	var _this = $(_this);
	if(_this.hasClass("right")){
		$(".ui_stats_wrap").css("width","940px");
		$(".ui_resource_wrap").hide();
		_this.removeClass("right").addClass("left");
	}else{
		$(".ui_stats_wrap").css("width","466px");
		$(".ui_resource_wrap").show();
		_this.removeClass("left").addClass("right");
	}
}

function pagingBtn(_this){
	var _this = $(_this);
	_this.parent(".ui_paging").find(".on").removeClass("on");
	_this.addClass("on");
}

function mapBtn(_this){
	var _this = $(_this);
	_this.parent(".ui_btn_list").find(".on").removeClass("on");
	_this.addClass("on");
}

function deviceScale(_this){
	var _this = $(_this);
	if(!_this.hasClass("scale_ani")){
		_this.parents(".drawing_area").find(".scale_ani").removeClass("scale_ani");
		_this.addClass("scale_ani");
	}else if(_this.hasClass("scale_ani")){
		_this.parents(".drawing_area").find(".scale_ani").removeClass("scale_ani");
	}
}

function deviceScale2(_this){
	var _this = $(_this);
	if(!_this.hasClass("scale_ani")){
		_this.parents(".route_area_img").find(".scale_ani").removeClass("scale_ani");
		_this.addClass("scale_ani");
	}else if(_this.hasClass("scale_ani")){
		_this.parents(".route_area_img").find(".scale_ani").removeClass("scale_ani");
	}
}

/*function reportTr(_this){
	var _this = $(_this);
	_this.parents("table").find(".on").removeClass("on");
	_this.addClass("on");
}*/


function onCamera(_this){
	return;
	var _this = $(_this);
	//_this.toggleClass("on2");
	//_this.next(".tree02").find(".tree_list_btn").removeClass("on").addClass("on");
	if(!_this.hasClass("on2")){
		_this.addClass("on2");
		//alert("on없음")
		_this.next(".tree02").find(".tree_list_btn").removeClass("on").addClass("on");
	}else{
		_this.removeClass("on2");
		//alert("on있음");
		_this.next(".tree02").find(".tree_list_btn").removeClass("on");
	}
}

function deviceName(_this){
	var _this = $(_this);
	_this.parents(".drawing_camera").find("strong").css("z-index", "1");
	_this.css("z-index", "2");
}

//카메라 on표시
function videoOn (_this) {
	var _this = $(_this);
	if (_this.hasClass("on")) {
		_this.removeClass("on");
		_this.parents(".tree02").prev(".tree_list_btn").removeClass("on");
	}else {
		_this.addClass("on");
		var liLeng = _this.parents(".tree02").children("li").length;
		var onLeng = _this.parents(".tree02").find(".on").length
		if(liLeng == onLeng){
			_this.parents(".tree02").prev(".tree_list_btn").addClass("on");
		}
	}
}

function panelRadio (_this) {
	var _this = $(_this);
	var _thisRadio = _this.children("input");
	_this.parents(".detail_step1").find("input").prop("checked",false);
	_this.parents(".detail_step1").find(".on").removeClass("on");
	_this.addClass("on").children("input").prop("checked", true);
}

function step2Func1 (_this) {
	var _this = $(_this);
	var _thisChk = _this.children("input");
	var _thisIndx = _this.parent().parent().index();
	if (_thisChk.prop("checked") == true) {
		_thisChk.prop("checked", false);
		_this.removeClass("on");
		_this.parent("p").next(".step2_camera_list").find("input").prop("checked", false);
		_this.parent("p").next(".step2_camera_list").find("label").removeClass("on");
		
		_this.parent("p").next(".step2_camera_list").find("input").next("span").each(function  (index) {
			var _spanTxt = $(this).text();
			$(".step3_scroll ul").find(".chk"+_thisIndx+"_"+index).remove();
		});

	}else if (_thisChk.prop("checked") == false) {
		_thisChk.prop("checked", true);
		_this.addClass("on");
		_this.parent("p").next(".step2_camera_list").find("input").prop("checked", true);
		_this.parent("p").next(".step2_camera_list").find("label").addClass("on");

		_this.parent("p").next(".step2_camera_list").find("input").next("span").each(function  (index) {
			var _spanTxt = $(this).text();
			$(".step3_scroll ul").append("<li class='chk"+_thisIndx+"_"+index+"'>"+_spanTxt+" <button type='button' onclick='step3Del(this);'></button></li>");
		});
	}
}

function step2Func2 (_this) {
	var _this = $(_this);
	var _thisChk = _this.children("input");
	var _thisClass = _this.parent("li").attr("class");
	var _thisTxt = _this.children("input").next("span").text();
	var _chkLength = _this.parents(".step2_camera_list").find("input:checked").length+1;
	var _allLength = _this.parents(".step2_camera_list").children("li").length;
	
	if (_thisChk.prop("checked") == true) {
		_thisChk.prop("checked", false);
		_this.removeClass("on");
/* 		_this.parents(".step2_camera_list").prev("p").find("label").removeClass("on").children("input").prop("checked", false);
		$(".step3_scroll ul").find("."+_thisClass).remove();
		*/

	}else if (_thisChk.prop("checked") == false) {
		_thisChk.prop("checked", true);
		_this.addClass("on");
/*
		$(".step3_scroll ul").append("<li class='"+_thisClass+"'>"+_thisTxt+" <button type='button' onclick='step3Del(this);'></button></li>");
		if (_chkLength == _allLength) {
			_this.parents(".step2_camera_list").prev("p").find("label").addClass("on").children("input").prop("checked", true);
		}*/
	}
}

function step3Del (_this) {
	var _this = $(_this);
	var _thisClass = _this.parent("li").attr("class");
	$(".step2_scroll").find('.'+_thisClass).parent(".step2_camera_list").prev("p").children("label").removeClass("on").children("input").prop("checked", false);
	$(".step2_scroll").find('.'+_thisClass).removeClass().children("label").removeClass("on").children("input").prop("checked", false);
	_this.parent("li").remove();
}

function panelRegi () {
	
	$(".ui_panel_list").hide();
	$(".ui_panel_detail").show();
	$(".step2_scroll>ul>li").each(function  (index) {
		$(this).children("ul").find("li").each(function  (index2) {
			$(this).addClass("chk"+index+"_"+index2);
		});
	});
	
}

function regiComplete () {
	/*
	var regiPanel;
	if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel1") {
		regiPanel = 22;
	}else if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel2") {
		regiPanel = 33;
	}else if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel3") {
		regiPanel = 44;
	}else if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel4") {
		regiPanel = 15;
	}

	var tabLeng = $(".monitoring .tab_header>ul li").length;

	$(".detail_step2 p input:checked").next("span").each(function  (index) {
	
		$(".monitoring .tab_header>ul").append("<li><button type='button' class='ui_tab"+(tabLeng+index)+"' onclick='tabFunc(this);'>"+$(this).text()+"</button></li>");
		
		if (regiPanel == 22) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + "'style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list22' style='display:block;'><li></li><li></li><li></li><li></li></ul></div>");

		}else if (regiPanel == 33) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + "'style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list33' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");
		
		}else if (regiPanel == 44) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + "'style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list44' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");

		}else if (regiPanel == 15) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + " ' style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list15' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");
		}
	});
	*/
	/*
	var regiPanel;
	if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel1") {
		regiPanel = 22;
	}else if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel2") {
		regiPanel = 33;
	}else if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel3") {
		regiPanel = 44;
	}else if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel4") {
		regiPanel = 15;
	}else if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel5") {
		regiPanel = 66;
	}else if ($(".detail_step1 .on").parent("li").attr("class") == "step1_panel6") {
		regiPanel = 88;
	}

	var tabLeng = $(".monitoring .tab_header>ul li").length;

	$(".detail_step2 p input:checked").next("span").each(function  (index) {
	
		$(".monitoring .tab_header>ul").append("<li><button type='button' class='ui_tab"+(tabLeng+index)+"' onclick='tabFunc(this);'>"+$(this).text()+"</button></li>");
		
		if (regiPanel == 22) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + "'style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list22' style='display:block;'><li></li><li></li><li></li><li></li></ul></div>");

		}else if (regiPanel == 33) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + "'style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list33' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");
		
		}else if (regiPanel == 44) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + "'style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list44' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");

		}else if (regiPanel == 15) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + " ' style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list15' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");
		}else if (regiPanel == 66) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + " ' style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list66' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");
		}else if (regiPanel == 88) {
			$(".ui_tab").append("<div class='tab_cont ui_tab" +(tabLeng+index) + " ' style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list88' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");
		}
	});*/
	$(".ui_panel_list").show();
	$(".ui_panel_detail").hide();
	$(".detail_step2").find(".on").removeClass("on");
	$(".detail_step2").find("input").prop("checked", false);
	//$(".detail_step3 li").remove();
	//$(".transcode input").prop("checked", false);
}

function specialFunc (_this, _thisTxt) {
	var _thisTxt = _thisTxt;
	var _this = $(_this);
	if (_this.hasClass("on")) {
		_this.removeClass("on");
	}else {
		_this.addClass("on");
	}
/*
	if (_thisTxt == "face") {
		if ($(".monitoring .tab_header>ul li button").hasClass("ui_face")) {
			//$(".monitoring .tab_header>ul .ui_face").remove();
			
		}else {
			//$(".monitoring .tab_header>ul").append("<li><button type='button' class='ui_face' onclick='tabFunc(this);'>얼굴인식</button></li>");
			//document.getElementById("car").pause();
			document.getElementById("faceVideo").play();
			setTimeout(face());
		}

	}else if (_thisTxt == "car") {
		if ($(".monitoring .tab_header>ul li button").hasClass("ui_car")) {
			//$(".monitoring .tab_header>ul .ui_car").remove();

		}else {
			//$(".monitoring .tab_header>ul").append("<li><button type='button' class='ui_car' onclick='tabFunc(this);'>차량번호</button></li>");
			document.getElementById("car").play();
			//document.getElementById("faceVideo").pause();
			setTimeout(car());
		}
		
	}else if (_thisTxt == 'bi') {
		if ($(".monitoring .tab_header>ul li button").hasClass("ui_camera")) {
			//$(".monitoring .tab_header>ul .ui_camera").remove();
		}else {
			//$(".monitoring .tab_header>ul").append("<li><button type='button' class='ui_camera' onclick='tabFunc(this);'>피플카운팅&amp;히트맵</button></li>");
		}
	}*/
}

// 복합일때 slideDown
function detailShow(_this, event){
	var _this = $(_this);
	 event.stopPropagation();
	if(_this.parent("div").next("ul").css("display") == "block"){
		_this.parent("div").next("ul").stop().slideUp(300);
		_this.text("▼");
		//_this.children("button").css("border-color","#102229");
	}else{
		_this.parent("div").next("ul").stop().slideDown(300);
		_this.text("▲");
		//_this.children("button").css("border-color","#c4c4c4");
	}
}

function complexOn(_this){
	var _this = $(_this);
	if(_this.hasClass("on")){
		_this.removeClass("on");
	}else{
		_this.addClass("on");
	}
}

function transcode () {
	if ($(".transcode").children("input").prop("checked") == true) {
		$(".detail_step1 .step1_panel4 .hide_cover").show();
	}else {
		$(".detail_step1 .step1_panel4 .hide_cover").hide();
	}
}

function uiSetMenu (_this) {
	var _this = $(_this);
	$(".ui_set_left").find(".on").removeClass("on");
	_this.parent("li").addClass("on");
	if (_this.parent("li").index() == 0) {
		$(".ui_panel_list").show();
		$(".ui_panel_special").hide();
		$(".ui_panel_detail").hide();

	}else if (_this.parent("li").index() == 1) {
		$(".ui_panel_list").hide();
		$(".ui_panel_detail").hide();
		$(".ui_panel_special").show();
	}
}

function panelOn (_this) {
	var _this = $(_this);
	$(".ui_set_panel_btm").find(".on").removeClass("on");
	_this.parent("li").addClass("on");
}

function setClose(){
	$(".ui_set_left ul li.on").removeClass("on");
	$(".ui_set_left ul li:first-child").addClass("on");
	//$(".set_panel_list").hide();
	//$(".ui_set_panel_btm").find(".on").removeClass("on");
	//$(".ui_set_panel").show();
	//$(".ui_set_camera").hide();
	//$(".detail_step1").find(".on").removeClass("on");
	//$(".detail_step1").find("input").prop("checked", false);
	//$(".detail_step1 .step1_panel1").children("label").addClass("on").children("input").prop("checked", true);
	
	$(".detail_step2").find(".on").removeClass("on");
	$(".detail_step2").find("input").prop("checked", false);
	$(".detail_step3 li").remove();
	$(".transcode input").prop("checked", false);
	$(".ui_panel_list").show();
	$(".ui_panel_special").hide();
	$(".ui_panel_detail").hide();
	//alert('close');
}

function panelSelect () {
	var _thisTxt = $(".ui_set_panel_btm .on button").text();
	$(".ui_set_left").find(".on").removeClass("on");
	$(".ui_set_left li:last-child").addClass("on");
	$(".set_panel_list ").hide();
	$(".ui_set_panel").hide();
	$(".ui_set_camera").show();

	if (_thisTxt == "4x4") {
		$(".ui_set_panel44").show();
		$(".monitoring .tab_header ul").find(".on").removeClass("on");
		//$(".monitoring .tab_header ul:first-child").append("<li class='on'><button type='button' onclick='tabFunc(this);' class='ui_tab44'>카메라그룹1</button></li>");

	}else if (_thisTxt == "3x3") {
		$(".ui_set_panel33").show();
		$(".monitoring .tab_header ul").find(".on").removeClass("on");
		//$(".monitoring .tab_header ul:first-child").append("<li class='on'><button type='button' onclick='tabFunc(this);'  class='ui_tab33'>카메라그룹2</button></li>");

	}else if (_thisTxt == "2x2") {
		$(".ui_set_panel22").show();
		$(".monitoring .tab_header ul").find(".on").removeClass("on");
		//$(".monitoring .tab_header ul:first-child").append("<li class='on'><button type='button' onclick='tabFunc(this);'  class='ui_tab22'>카메라그룹3</button></li>");

	}else if (_thisTxt == "1x5") {
		$(".ui_set_panel15").show();
		$(".monitoring .tab_header ul").find(".on").removeClass("on");
		//$(".monitoring .tab_header ul:first-child").append("<li class='on'><button type='button' onclick='tabFunc(this);'  class='ui_tab15'>카메라그룹4</button></li>");

	}else if (_thisTxt == "얼굴인식" ){
		$(".monitoring .tab_cont ").hide();
		$(".ui_face").show();
		$("#uiSetPop").hide();
		$(".ui_set_panel").show();
		$(".ui_set_camera").hide();
		$(".ui_set_panel_btm").find(".on").removeClass("on");
		document.getElementById("car").pause();
		document.getElementById("faceVideo").play();
		setTimeout(face());

	}else if (_thisTxt == "차량번호" ) {
		$(".monitoring .tab_cont ").hide();
		$(".ui_car").show();
		$("#uiSetPop").hide();
		$(".ui_set_panel").show();
		$(".ui_set_camera").hide();
		$(".ui_set_panel_btm").find(".on").removeClass("on");
		document.getElementById("car").play();
		document.getElementById("faceVideo").pause();
		setTimeout(car());

	}else if (_thisTxt == "피플카운팅&히트맵") {
		$(".monitoring .tab_cont ").hide();
		$(".ui_tab00").show();
		$("#uiSetPop").hide();
		$(".ui_set_left").find(".on").removeClass("on");
		$(".ui_set_left ul li:first-child").addClass("on");
		$(".ui_set_panel").show();
		$(".ui_set_camera").hide();
		$(".ui_set_panel_btm").find(".on").removeClass("on");
	}
	
	// <li><button type="button">카메라그룹1</button></li>
}

function panelComplete () {
	var _thisTxt = $(".ui_set_panel_btm .on button").text();
	$(".monitoring .tab_cont ").hide();
	var _menuLeng = $(".monitoring .tab_header li").length + 1;
	if (_thisTxt == "4x4") {
		//$(".ui_tab01").show();
		//$(".monitoring_list ul").hide();
		//$(".monitoring_list44").show();
		$(".monitoring .tab_cont").hide();
		$(".ui_tab").append("<div class='tab_cont ui_tab" +_menuLeng + "'style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list44' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");
		$(".monitoring .tab_header ul:first-child").append("<li class='on'><button type='button' onclick='tabFunc(this);' class='ui_tab" + _menuLeng+ " '>카메라그룹1</button></li>");
		
	}else if (_thisTxt == "3x3") {
		$(".monitoring .tab_cont").hide();
		//$(".ui_tab01").show();
		//$(".monitoring_list ul").hide();
		//$(".monitoring_list33").show();
		$(".ui_tab").append("<div class='tab_cont ui_tab" +_menuLeng + "'style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list33' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");
		$(".monitoring .tab_header ul:first-child").append("<li class='on'><button type='button' onclick='tabFunc(this);' class='ui_tab" + _menuLeng+ " '>카메라그룹2</button></li>");
		
	}else if (_thisTxt == "2x2") {
		$(".monitoring .tab_cont").hide();
		$(".ui_tab01").show();
		$(".monitoring_list ul").hide();
		$(".monitoring_list22").show();
		$(".monitoring .tab_header ul:first-child").append("<li class='on'><button type='button' onclick='tabFunc(this);' class='ui_tab22>카메라그룹3</button></li>");
		
	}else if (_thisTxt == "1x5") {
		$(".monitoring .tab_cont").hide();
		//$(".ui_tab01").show();
		//$(".monitoring_list ul").hide();
		//$(".monitoring_list15").show();
		$(".ui_tab").append("<div class='tab_cont ui_tab" +_menuLeng + " ' style='display:block'><div class='monitoring_list'><button type='button' class='btn_monitoring_left'></button><button type='button' class='btn_monitoring_right'></button><ul class='monitoring_list15' style='display:block;'><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");
		$(".monitoring .tab_header ul:first-child").append("<li class='on'><button type='button' onclick='tabFunc(this);' class='ui_tab" + _menuLeng+ " '>카메라그룹4</button></li>");
	}
	
	$(".set_panel_list").hide();
	$(".ui_set_panel").show();
	$(".ui_set_camera").hide();
	$(".ui_set_left ul li").removeClass("on");
	$(".ui_set_left ul li:first-child").addClass("on");
	$(".ui_set_panel_btm").find(".on").removeClass("on");
	$("#uiSetPop").hide();
}

function faceTab (_this) {
		return;
	var _this = $(_this);
	var _thisClass = _this.attr("class");
	_this.parents(".face_tab").find(".on").removeClass("on");
	_this.parent("li").addClass("on");
	if (_thisClass == "face1") {
		_this.parents(".face_area").find(".face_left>div").removeClass().addClass("face_panel1");
		
	}else if (_thisClass == "face2") {
		_this.parents(".face_area").find(".face_left>div").removeClass().addClass("face_panel2");
		
	}else if (_thisClass == "face3") {
		_this.parents(".face_area").find(".face_left>div").removeClass().addClass("face_panel3");
		
	}else if (_thisClass == "face4") {
		_this.parents(".face_area").find(".face_left>div").removeClass().addClass("face_panel4");
	}
}

function faceOn (_this) {
	var _this = $(_this);
	_this.parent("ul").find(".on").removeClass("on");
	_this.addClass("on");
	$("#carPop").show();
}

function trOn (_this) {
	var _this = $(_this);
	_this.parent("tbody").find(".on").removeClass("on");
	_this.addClass("on");
	_this.next("tr").addClass("on");
}
function trOn2 (_this) {
	var _this = $(_this);
	_this.parent("tbody").find(".on").removeClass("on");
	_this.addClass("on");
}

function sPause(_this){
	var _this = $(_this);
	if(_this.children().prop("checked")){
		_this.addClass("play");
	}else{
		_this.removeClass("play");
	}
}

function summaryAdd () {
	$(".summary_video").show();
	$(".summary_video .video_list>li").show();
	$(".panel_timeline").hide();
}

function summaryFunc (_this) {
	var _this = $(_this);
	_this.next(".summary_detail").slideToggle(300);
}


function smartChk () {
	$(".smart_popup").hide();
	if ($("button.smart01").parent("li").hasClass("on")) {
	
		$(".search02 .histroty_list .vod_result>li").hide();
		$(".search02 .histroty_list .smart01").show();
		$(".search02 .video_list li").hide();

	}else if ($("button.smart04").parent("li").hasClass("on")) {
		
		$(".search02 .histroty_list .vod_result>li").hide();
		$(".search02 .video_list li").hide();
		$("#smartPop04").show();

	}else if ($("button.smart03").parent("li").hasClass("on")) {
	
		$(".search02 .histroty_list .vod_result>li").hide();
		$(".search02 .histroty_list .smart03").show();
		$(".search02 .video_list li").hide();

	}else if($("button.smart02").parent("li").hasClass("on")){
		
		$(".search02 .histroty_list .vod_result>li").show();
		$(".search02 .histroty_list .smart01").hide();
		$(".search02 .histroty_list .smart03").hide();
		$(".search02 .video_list li").hide();
		
	}
}

function smartOpt (_this) {
	var _this = $(_this);
	if (_this.hasClass("check")) {
		_this.removeClass("check");
		$(".smart_option").hide();
	}else {
		_this.addClass("check");
		$(".smart_option").show();
	}
}

function popClose2 (_this) {
	var _this = $(_this);
	_this.hide();
}

function setFunc (_this) {
	var _this = $(_this);
	var _id = _this.attr("id");
	var _thisClass = _this.children("span").attr("class");

	if (_this.hasClass("on")) {
		_this.removeClass("on");

		if (_thisClass == "event_txt") {
			_this.parents(".opt_detail_set").find(".set_event").hide();

		}else if (_thisClass == "color_txt") {
			_this.parents(".opt_detail_set").find(".set_color").hide();
		}
		
		// 일반, 스마트 분류- 이벤트, 컬러 선택 유무에 따른 결과 height 조절
		if (_this.parents(".tab_cont").hasClass("search01")) {
			
			if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="none" && _thisClass == 'event_txt') {
				$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","292px");
				

			}else if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="block" && _thisClass == 'event_txt') {
				$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","245px");

			}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="none" && _thisClass == 'color_txt') {
				$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","292px");

			}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="block" && _thisClass == 'color_txt') {
				if($(".third_list").css("display") == "block"){
					$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","72px");
				}else{
					$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","217px");
				}
			}

		}else if (_this.parents(".tab_cont").hasClass("search02")) {

			if ($(".smart01").parent("li").hasClass("on") || $(".smart04").parent("li").hasClass("on")) {

				if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="none" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","205px");

				}else if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="block" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","155px");
					
				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="none" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","205px");

				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="block" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","80px");
				}

			}else if ($(".smart02").parent("li").hasClass("on")) {

				if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="none" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","96px");

				}else if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="block" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","83px");
					
				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="none" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","96px");

				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="block" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","8px");
				}

			}else if ($(".smart03").parent("li").hasClass("on")) {

				if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="none" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","163px");

				}else if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="block" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","113px");
					
				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="none" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","163px");

				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="block" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","38px");
				}
			}
		}

	}else {

		if (_thisClass == "event_txt") {
			_this.parents(".opt_detail_set").find(".set_event").show();
		}else if (_thisClass == "color_txt") {
			_this.parents(".opt_detail_set").find(".set_color").show();
		}
		_this.addClass("on");

		if (_this.parents(".tab_cont").hasClass("search01")) {
			
			if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="none" && _thisClass == 'event_txt') {
				if ($(".third_list").css("display") == "block"){
					$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","72px");
				}else{
					$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","184px");
				}

			}else if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="block" && _thisClass == 'event_txt') {
				if ($(".third_list").css("display") == "block"){
					$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","33px");
				}else{
					$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","144px");
				}

			}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="none" && _thisClass == 'color_txt') {
				$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","245px");

			}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="block" && _thisClass == 'color_txt') {
				if($(".third_list").css("display") == "block"){
					$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","33px");
				}else {
					$(".left_menu_vod .search01 .search_result_list .histroty_list").css("height","144px");
				}
			}

		}else if (_this.parents(".tab_cont").hasClass("search02")) {

			if ($(".smart01").parent("li").hasClass("on") || $(".smart04").parent("li").hasClass("on")) {

				if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="none" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","81px");

				}else if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="block" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","42px");
					
				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="none" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","155px");

				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="block" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","42px");
				}

			}else if ($(".smart02").parent("li").hasClass("on")) {
			
				if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="none" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","8px");

				}else if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="block" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","0px");
					
				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="none" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","83px");

				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="block" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","0px");
				}

			}else if ($(".smart03").parent("li").hasClass("on")) {
				if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="none" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","38px");

				}else if (_this.parents(".opt_detail_set").find(".set_color").css("display") =="block" && _thisClass == 'event_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","0px");
					
				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="none" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","113px");

				}else if (_this.parents(".opt_detail_set").find(".set_event").css("display") =="block" && _thisClass == 'color_txt') {
					$(".left_menu_vod .search02 .search_result_list .histroty_list").css("height","0px");
				}
			}
		}
	}

	
}



function onFunc (_this) {
	var _this = $(_this);
	if (_this.hasClass("on")) {
		_this.removeClass("on");
	}else {
		_this.addClass("on");
	}
}

function onFunc2 (_this) {
	var _this = $(_this);
	_this.parents(".on_area").find(".on").removeClass("on");
	_this.addClass("on");
}

function resultFunc (_this) {
	$(".left_menu_alarm .set01 .list_cont").slideUp(300);
	$(".left_menu_alarm .set01 .list_tit_btn").addClass("close");
	$(".left_menu_alarm .set03 .summary01 .histroty_list").css("height","875px");
	$(".summary_result").show();
}

function resultUp(_this){
	var _this = $(_this);
	_this.next(".vod_result_area").stop().slideToggle(300);
}

function titleOn(_this){
	var _this = $(_this);
	if(_this.hasClass("on")){
		//_this.parent(".video_title").find(".on").removeClass("on");
	}else{
		_this.parent(".video_title").find(".on").removeClass("on");
		_this.addClass("on");
	}
	
	if (_this.hasClass("all_title")) {
		_this.parents(".video_title").siblings(".panel_cont").find(".video_list_header, .video_ico_list_div ").removeClass("view").addClass("view");
	}else if (_this.hasClass("hide_title")) {
		_this.parents(".video_title").siblings(".panel_cont").find(".view").removeClass("view");
	}
	
}

function viewTit (_this) {
	var _this = $(_this);
	if (_this.parents("li").find(".video_list_header, .video_ico_list_div").css("display") == "block") {

		_this.parents("li").find(".video_list_header, .video_ico_list_div").hide();
	}
	else {
		_this.parents("li").find(".video_list_header, .video_ico_list_div").show();
	}
}
function showTit(_this){
	var _this = $(_this);
	_this.parents("li").find(".video_list_header, .video_ico_list_div").show();
}
function hideTit(_this){
	var _this = $(_this);
	_this.parents("li").find(".video_list_header, .video_ico_list_div").hide();
}
function vaBtnFunc (_this) {
	var _this = $(_this);
	if (_this.hasClass("on")) {
		_this.text("OFF").removeClass().addClass("off");
		$(".service_people div").hide();
		$(".service_people_off").show();
	}else if (_this.hasClass("off")) {
		_this.text("ON").removeClass().addClass("on");
		$(".service_people div").hide();
		$(".service_people_on").show();
	}
}

function vaBtnFunc2 (_this) {
	var _this = $(_this);
	if (_this.hasClass("on")) {
		_this.text("OFF").removeClass().addClass("off");
		$(".service_people div").hide();
		$(".service_heatmap_off").show();
	}else if (_this.hasClass("off")) {
		_this.text("ON").removeClass().addClass("on");
		$(".service_people div").hide();
		$(".service_heatmap_on").show();
	}
}

function thumList (_this) {
	var _this = $(_this);
	var _thisSrc = _this.children("img").clone();
	$(".snapshot_video img").remove();
	$(".snapshot_video").append(_thisSrc);
}

function listSelect () {
	$(".hide_tr").show();
	$(".search_pop").hide();
}

//vod thum_list
function thumFunc (_this){
	var _this = $(_this);
	_this.parents(".thum_list").find("button").css("z-index","30");
	_this.css("z-index","60");
}

function eventUp (_this) {
	var _this = $(_this);
	_this.next().stop().slideToggle(300);
}

function cameraSelect () {
	$(".camera_select_list").stop().slideToggle(300);
}

function cameraSelectOn (_this) {
	var _this = $(_this);
	$(".camera_select_list").find(".on").removeClass("on");
	_this.parent().addClass("on");
	$(".camera_select_list").slideUp(300);
}

function btnView (_this) {
	var _this = $(_this);
	if (_this.hasClass("surround")) {
		_this.removeClass("surround").addClass("view");
		_this.text("주변영상보기");
		$("#surroundingsPop .popup").css("height","448px");
		$(".surroundings_top .img_view").show();
		$(".surroundings_top .img_surround").hide();
		$(".surroundings_list").hide();
	}else if(_this.hasClass("view")){
		_this.removeClass("view").addClass("surround");
		_this.text("도면보기");
		$("#surroundingsPop .popup").css("height","890px");
		$(".surroundings_top .img_view").hide();
		$(".surroundings_top .img_surround").show();
		$(".surroundings_list").show();
	}
}

function eventOn (_this) {
	var _this = $(_this);
	if (_this.hasClass("on")) {
		_this.parents(".event_history").find(".on").removeClass("on");
	}else {
		_this.parents(".event_history").find(".on").removeClass("on");
		_this.addClass("on");
	}
}

function searchTerm (_this) {
	var _this = $(_this);
	if (_this.hasClass("on")) {
		_this.parents(".history_btn_list").find(".on").removeClass("on");
	}else {
		_this.parents(".history_btn_list").find(".on").removeClass("on");
		_this.addClass("on");
	}
}

//ui
function timelineFunc (_this) {
	var _this = $(_this);
	var _thisTime = $(_this).children(".timeline_time").text();
	//$(".work_txt2").text(_thisTime);
	if (_this.hasClass("on")) {
		_this.parents(".list_wrap").find(".on").removeClass("on");
		$(".ui_view").hide();
		$(".view_area_detail").hide();
		$(".ui_view ").removeClass("marker_on");
		$(".view_area").find(".on").removeClass("on");
		$(".ui_event_list .ui_tit").show();
		$(".ui_event_list .tbl_wrap").show();

		//$(".ui_workflow .work_txt2").hide();
		$(".ui_event_list .ui_workflow span").show();
		//$(".ui_workflow .on").removeClass("on");
		$(".monitoring_trans .ico_trans ").show();
	}else {
		_this.parents(".list_wrap").find(".on").removeClass("on");
		_this.addClass("on");
		$(".ui_view").show();
		$(".ui_event_list .ui_tit").hide();
		$(".ui_event_list .tbl_wrap").hide();

		//$(".ui_workflow .work_txt2").show();
		//$(".ui_event_list .ui_workflow span").hide();
	//	$(".ui_workflow .flow1").addClass("on");
		$(".monitoring_trans .ico_trans ").hide();
	}
}

function marker (_this,mode) {
	var _this = $(_this);
	if(typeof mode=='undefined'){
		if (_this.hasClass("on")) {
			_this.parents(".view_area").find(".on").removeClass("on");
			$(".ui_view").removeClass("marker_on");
			$(".view_area_detail").hide();
		}else {
			_this.parents(".view_area").find(".on").removeClass("on");
			_this.addClass("on");
			$(".ui_view").addClass("marker_on");
			$(".view_area_detail").show();
		}
	}else{
		if (_this.hasClass("on")) {
			_this.parents(".view_area").find(".on").removeClass("on");
			$(".ui_view").removeClass("marker_on");
			$(".view_area_detail").hide();
		}else {
			_this.parents(".view_area").find(".on").removeClass("on");
			_this.addClass("on");
			$(".ui_view").removeClass("marker_on");
			$(".view_area_detail").hide();
		}		
	}
}


//eventrule
function ruleDetail (_this) {
	var _this = $(_this);
	if (_this.parents(".rule_list_top").next(".rule_list_btm").css("display") == "block") {
		_this.parents(".rule_list_cont").find("li").removeClass("on");
		_this.parents(".rule_list_cont").find(".rule_list_btm").slideUp();
	}else {
		_this.parents(".rule_list_cont").find("li").removeClass("on");
		_this.parents("li").addClass("on");
		_this.parents(".rule_list_cont").find(".rule_list_btm").slideUp();
		_this.parents(".rule_list_top").next(".rule_list_btm").slideDown();
	}
}

function wide2Close(_this){
	var _this = $(_this);
	_this.parents(".wide_video2").hide();
	$(".wide_video2").css("top","0px");
}

function videoBtnFunc(_this){
	var _this = $(_this);
	if(_this.hasClass("video_btn_start")){
		_this.removeClass("video_btn_start").addClass("video_btn_pause");
	}else if(_this.hasClass("video_btn_pause")){
		_this.removeClass("video_btn_pause").addClass("video_btn_start");
	}
}

var forceRedraw = function(element){
return;
    if (!element) { return; }

    var n = document.createTextNode(' ');
    var disp = element.style.display;  // don't worry about previous display style

    element.appendChild(n);
    element.style.display = 'none';

    setTimeout(function(){
        element.style.display = disp;
        n.parentNode.removeChild(n);
    },20); // you can play with this timeout to make it as short as possible
}

function videoBorder(_this){
	var _this = $(_this);
	if(_this.parents("li").hasClass("li_border")){
		_this.parents(".video_list").find("li").removeClass("li_border");
		_this.parents("li").removeClass("li_border");
		//_this.parents(".video_list").find("li").redraw();
		//forceRedraw(_this.parents(".video_list").find("li"));
		//var part = 
		//alert(_this);
	}else{
		_this.parents(".video_list").find("li").removeClass("li_border");
		_this.parents("li").addClass("li_border");
		//forceRedraw(_this.parents(".video_list").find("li"));
		
	}
}
function hideVideoBorder(){
	$(".video_list").find("li").removeClass("li_border");
}
function forceRefresh(_this){
	var _this = $(_this);
	
	forceRedraw($(".video_list").find("li"));
}


// alarm slideup
function slideUp2(_this){
	var _this = $(_this);
	$(".left_menu_alarm .set02 .list_cont .histroty_list li").show();
	_this.parents(".set02").siblings(".set01").find(".list_header .list_tit_btn").addClass("close");
	_this.parents(".set02").siblings(".set01").find(".list_cont").slideUp(300);
	if (_this.parents(".set02").siblings(".set01").find(".histroty_list").height() <= 294) {
		// 히스토리가 작을때
		_this.parents(".list_cont").css("height","100%");
		_this.parents(".list_cont").find(".histroty_list").stop().animate({"height": "443px"}, 300);
		
	}else if(_this.parents(".set02").siblings(".set01").find(".histroty_list").height() > 294){
		// 히스토리가 클때
		var thisH = _this.parents(".list_cont").height()- 330;
		_this.parents(".list_cont").find(".histroty_list").stop().animate({"height": thisH}, 300);
		_this.parents(".list_cont").css("height","100%");
		
	}
}

// vod slideup
function slideUp(_this){
	var _this = $(_this);
	_this.parents(".list_cont").stop().slideUp(300);
	_this.parents(".list_cont").prev(".list_header").children(".list_tit_btn").addClass("close");
	_this.parents(".set01").siblings("li").find(".search01 .search_result_list .histroty_list").stop().animate({"height":"862px"}, 300);
	$(".summary_video").hide();

}

function slideUp3 (_this) {
	var _this = $(_this);
	_this.parents(".list_cont").prev(".list_header").children(".list_tit_btn").addClass("close");
	_this.parents(".list_cont").stop().slideUp(500);
	$(".search03 .summary_list ul li").show();
	$(".search03 .summary_list").stop().animate({"height":"857px"}, 500);
}

function slideUp4 (_this) {
	var _this = $(_this);
	_this.parents(".list_cont").prev(".list_header").children(".list_tit_btn").addClass("close");
	_this.parents(".list_cont").stop().slideUp(500);
	$(".search04 .summary_list ul li").show();
	$(".search04 .summary_list").stop().animate({"height":"857px"}, 500);
	//$(".search04 .tree_area").find(".on").removeClass("on");
}



// 이벤트종류
function typeFunc(_this){
	var _this = $(_this);
	if(_this.hasClass("on")){
		_this.removeClass("on");
	}else{
		_this.addClass("on");
	}
}

function typeFunc2(_this){
	var _this = $(_this);
	if(_this.hasClass("on")){
		_this.parents(".left_menu_alarm").find(".histroty_list .on").removeClass("on");
	}else{
		_this.parents(".left_menu_alarm").find(".histroty_list .on").removeClass("on");
		_this.addClass("on");
	}
}

function typeFunc3(_this){
	var _this = $(_this);
	if(_this.parent().hasClass("on")){
		_this.parent().removeClass("on");
	}else{
		_this.parent().addClass("on");
	}
}

function typeFunc4(_this){
	var _this = $(_this);
	if(_this.hasClass("on")){
		_this.parents(".set02").find(".on").removeClass("on");
		_this.addClass("on");
	}else{
		_this.parents(".set02").find(".on").removeClass("on");
	}
	
}


// 검색 팝업
function searchPop(_this){
	var _this = $(_this);
	_this.show();
}

// 영상클릭
function videoClick (_this){
	var _this = $(_this);
	if(_this.parents("li").find(".video_green").css("display") == "none"){
		_this.prev(".video_ico_list").stop().animate({"bottom":"0px"});
		_this.parents("li").find(".video_green").show();
	}
}

function videoClick2 (_this){
	var _this = $(_this);
	_this.hide();
	_this.parents("li").find(".video_ico_list").stop().animate({"bottom":"-28px"});
}

// left menu
function leftFunc(_this){
	var _this = $(_this);
	if (_this.hasClass("close")) {
		_this.parent(".list_header").next(".list_cont").stop().slideDown(300);
		_this.removeClass("close");
		
		if( _this.parents("li").hasClass("set01")){

			if(_this.parents(".left_menu").hasClass("left_menu_alarm")){
				_this.parents(".set01").siblings(".set02").find(".histroty_list").stop().animate({"height":"132px"}, 300);
				
				if (_this.parents("li").hasClass("set02")) {
					_this.parents(".set02").siblings(".set03").find(".histroty_list").stop().animate({"height":"42px"}, 300);
				
				}else if (_this.parents("li").hasClass("set01") && _this.parents("li").siblings(".set02").find(".list_tit_btn").hasClass("close")) {
					_this.parents("li").siblings(".set03").find(".histroty_list").stop().animate({"height":"509px"}, 300);
				}
				
				if(_this.parents("li").hasClass("set01")){
					_this.parents("li").siblings(".set03").find(".histroty_list").stop().animate({"height":"100px"}, 300);
					_this.parents("li").siblings(".set03").find(".summary02 .summary_result").hide();
				}
				
			}
			if(_this.parents(".left_menu").hasClass("left_menu_vod")){
				
				if ($("button.search01").parent("li").hasClass("on")) {
					// 일반검색일때
	
					if($(".search01 .set_event").css("display") == "none" && $(".search01 .set_color").css("display") == "none"){
						$(".left_menu_vod .search01 .search_result_list .histroty_list").stop().animate({"height":"292px"}, 300);
	
					}else if ($(".search01 .set_event").css("display") == "block" && $(".search01 .set_color").css("display") == "none") {
						$(".left_menu_vod .search01 .search_result_list .histroty_list").stop().animate({"height":"184px"}, 300);
					
					}else if ($(".search01 .set_event").css("display") == "none" && $(".search01 .set_color").css("display") == "block") {
						$(".left_menu_vod .search01 .search_result_list .histroty_list").stop().animate({"height":"245px"}, 300);
	
					}else if ($(".search01 .set_event").css("display") == "block" && $(".search01 .set_color").css("display") == "block") {
						$(".left_menu_vod .search01 .search_result_list .histroty_list").stop().animate({"height":"144px"}, 300);
					}
				}else if ($("button.search02").parent("li").hasClass("on")) {
	
					if($(".smart01").parent("li").hasClass("on")){
	
						if($(".smart01 .set_event").css("display") == "none" && $(".smart01 .set_color").css("display") == "none"){
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"205px"}, 300);
	
						}else if ($(".smart01 .set_event").css("display") == "block" && $(".smart01 .set_color").css("display") == "none") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"184px"}, 300);
						
						}else if ($(".smart01 .set_event").css("display") == "none" && $(".smart01 .set_color").css("display") == "block") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"245px"}, 300);
	
						}else if ($(".smart01 .set_event").css("display") == "block" && $(".smart01 .set_color").css("display") == "block") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"144px"}, 300);
						}
	
					}else if ($(".smart02").parent("li").hasClass("on")) {
	
						if($(".smart02 .set_event").css("display") == "none" && $(".smart02 .set_color").css("display") == "none"){
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"96px"}, 300);
	
						}else if ($(".smart02 .set_event").css("display") == "block" && $(".smart02 .set_color").css("display") == "none") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"0px"}, 300);
						
						}else if ($(".smart02 .set_event").css("display") == "none" && $(".smart02 .set_color").css("display") == "block") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"83px"}, 300);
	
						}else if ($(".smart02 .set_event").css("display") == "block" && $(".smart02 .set_color").css("display") == "block") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"0px"}, 300);
						}
	
					}else if ($(".smart03").parent("li").hasClass("on")) {
	
						if($(".smart03 .set_event").css("display") == "none" && $(".smart03 .set_color").css("display") == "none"){
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"163px"}, 300);
	
						}else if ($(".smart03 .set_event").css("display") == "block" && $(".smart03 .set_color").css("display") == "none") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"38px"}, 300);
						
						}else if ($(".smart03 .set_event").css("display") == "none" && $(".smart03 .set_color").css("display") == "block") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"113px"}, 300);
	
						}else if ($(".smart03 .set_event").css("display") == "block" && $(".smart03 .set_color").css("display") == "block") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"0px"}, 300);
						}
	
					}else if ($(".smart04").parent("li").hasClass("on")) {
						
						if($(".smart04 .set_event").css("display") == "none" && $(".smart04 .set_color").css("display") == "none"){
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"205px"}, 300);
	
						}else if ($(".smart04 .set_event").css("display") == "block" && $(".smart04 .set_color").css("display") == "none") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"81px"}, 300);
						
						}else if ($(".smart04 .set_event").css("display") == "none" && $(".smart04 .set_color").css("display") == "block") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"155px"}, 300);
	
						}else if ($(".smart04 .set_event").css("display") == "block" && $(".smart04 .set_color").css("display") == "block") {
							$(".left_menu_vod .search02 .search_result_list .histroty_list").stop().animate({"height":"42px"}, 300);
						}
					}
				}else if ($("button.search03").parent("li").hasClass("on")) {
					$(".search03 .summary_list").stop().animate({"height":"691px"}, 300);
				}else if ($("button.search04").parent("li").hasClass("on")) {
					$(".search04 .summary_list").stop().animate({"height":"365px"}, 300);
				}
				
				
				//_this.parents(".set01").siblings("li").find(".search01 .histroty_list").stop().animate({"height":"114px"}, 300);
				//_this.parents(".set01").siblings("li").find(".search02 .histroty_list").stop().animate({"height":"195px"}, 300);
			}
		}
	}else {
		// close
		_this.parent(".list_header").next(".list_cont").stop().slideUp(300);
		_this.addClass("close");
		
		if(_this.parents("li").hasClass("set01")){
			if(_this.parents(".left_menu").hasClass("left_menu_alarm")){
				_this.parents(".set01").siblings(".set02").find(".histroty_list").stop().animate({"height":"443px"}, 300);
				
				if (_this.parents("li").hasClass("set02") && !(_this.parents("li").siblings(".set01").find(".list_tit_btn").hasClass("close"))) {
					_this.parents(".set02").siblings(".set03").find(".histroty_list").stop().animate({"height":"509px"}, 300);
	
				}else if (_this.parents("li").hasClass("set02") && _this.parents("li").siblings(".set01").find(".list_tit_btn").hasClass("close")) {
					_this.parents("li").siblings(".set03").find(".histroty_list").stop().animate({"height":"823px"}, 300);
	
				}else if (_this.parents("li").hasClass("set01") && _this.parents("li").siblings(".set02").find(".list_tit_btn").hasClass("close")) {
					_this.parents("li").siblings(".set03").find(".histroty_list").stop().animate({"height":"823px"}, 300);
	
				}
				
				if(_this.parents("li").hasClass("set01")){
					_this.parents("li").siblings(".set03").find(".histroty_list").stop().animate({"height":"875px"}, 300);
					_this.parents("li").siblings(".set03").find(".summary02 .summary_result").show();
					_this.parents("li").siblings(".set03").find(".summary02 .histroty_list").stop().animate({"height":"760px"}, 300);
				}
			}
			if(_this.parents(".left_menu").hasClass("left_menu_vod")){
				if (_this.parents(".set01").find(".search01").parent("li").hasClass("on") && _this.parents("li").hasClass("set01")){
					$(".search01").find(".histroty_list").stop().animate({"height":"862px"}, 300);
					
				}else if(_this.parents(".set01").find(".search02").parent("li").hasClass("on") && _this.parents("li").hasClass("set01")){
					$(".search02").find(".histroty_list").stop().animate({"height":"862px"}, 300);
					
				}else{
					$(".summary_list").stop().animate({"height":"862px"}, 300);
				}	
				
			}
		}
	}
}

// search02 slideup
function slideUp1(_this){
	var _this = $(_this);
	$(".left_menu_vod .search02 .histroty_list li").show();
	$(".left_menu_vod .set01 .list_cont").stop().slideUp(300);
	$(".left_menu_vod .set01 .list_header .list_tit_btn").addClass("close");
	//$(".left_menu_vod .search02 .set02 .search_result_list .histroty_list").stop().animate({"height":"862px"}, 300);
	_this.parents(".set01").siblings("li").find(".search02 .search_result_list .histroty_list").stop().animate({"height":"862px"}, 300);
	$(".summary_video").hide();
}

// select
function selectFunc (_this) {
	var _this = $(_this);
	_this.next("ul").stop().slideToggle(300);
}

function selectClick (_this) {
	var _this = $(_this);
	var _parent = _this.parent().parent("ul");
	_parent.prev().text(_this.text());
	_parent.slideUp(300);
	
	var today = new Date();
	var sdate = new Date();
	//var edate = null;
	
		switch(_this.context.innerText){
			
			case '1일':
				sdate.setDate(today.getDate()-1);				
				$("#calendar3").val(""+sdate.getFullYear()+'/'+ ("0"+(sdate.getMonth()+1)).substr(-2)+'/'+ ("0"+sdate.getDate()).substr(-2) + ' ~ ' +
						today.getFullYear()+'/'+ ("0"+(today.getMonth()+1)).substr(-2)+'/'+ ("0"+today.getDate()).substr(-2))
				break;
			case '2일':
				sdate.setDate(today.getDate()-2);				
				$("#calendar3").val(""+sdate.getFullYear()+'/'+ ("0"+(sdate.getMonth()+1)).substr(-2)+'/'+ ("0"+sdate.getDate()).substr(-2) + ' ~ ' +
						today.getFullYear()+'/'+ ("0"+(today.getMonth()+1)).substr(-2)+'/'+ ("0"+today.getDate()).substr(-2))
				break;
			case '3일':
				sdate.setDate(today.getDate()-3);				
				$("#calendar3").val(""+sdate.getFullYear()+'/'+ ("0"+(sdate.getMonth()+1)).substr(-2)+'/'+ ("0"+sdate.getDate()).substr(-2) + ' ~ ' +
						today.getFullYear()+'/'+ ("0"+(today.getMonth()+1)).substr(-2)+'/'+ ("0"+today.getDate()).substr(-2))
				break;
			case '1주':
				sdate.setDate(today.getDate()-7);				
				$("#calendar3").val(""+sdate.getFullYear()+'/'+ ("0"+(sdate.getMonth()+1)).substr(-2)+'/'+ ("0"+sdate.getDate()).substr(-2) + ' ~ ' +
						today.getFullYear()+'/'+ ("0"+(today.getMonth()+1)).substr(-2)+'/'+ ("0"+today.getDate()).substr(-2))
				break;
			case '3주':
				sdate.setDate(today.getDate()-21);				
				$("#calendar3").val(""+sdate.getFullYear()+'/'+ ("0"+(sdate.getMonth()+1)).substr(-2)+'/'+ ("0"+sdate.getDate()).substr(-2) + ' ~ ' +
						today.getFullYear()+'/'+ ("0"+(today.getMonth()+1)).substr(-2)+'/'+ ("0"+today.getDate()).substr(-2))
				break;
			case '1개월':
				sdate.setDate(today.getDate()-30);				
				$("#calendar3").val(""+sdate.getFullYear()+'/'+ ("0"+(sdate.getMonth()+1)).substr(-2)+'/'+ ("0"+sdate.getDate()).substr(-2) + ' ~ ' +
						today.getFullYear()+'/'+ ("0"+(today.getMonth()+1)).substr(-2)+'/'+ ("0"+today.getDate()).substr(-2))
				break;
			case '3개월':
				sdate.setDate(today.getDate()-90);				
				$("#calendar3").val(""+sdate.getFullYear()+'/'+ ("0"+(sdate.getMonth()+1)).substr(-2)+'/'+ ("0"+sdate.getDate()).substr(-2) + ' ~ ' +
						today.getFullYear()+'/'+ ("0"+(today.getMonth()+1)).substr(-2)+'/'+ ("0"+today.getDate()).substr(-2))
				break;
			case '1년':
				sdate.setDate(today.getDate()-365);				
				$("#calendar3").val(""+sdate.getFullYear()+'/'+ ("0"+(sdate.getMonth()+1)).substr(-2)+'/'+ ("0"+sdate.getDate()).substr(-2) + ' ~ ' +
						today.getFullYear()+'/'+ ("0"+(today.getMonth()+1)).substr(-2)+'/'+ ("0"+today.getDate()).substr(-2))
				break;
			case '모두':
				sdate = new Date(0,0,0); 
				$("#calendar3").val("");
				break;
		}
		$("#sdate1").val(""+sdate.getFullYear()+'-'+ ("0"+(sdate.getMonth()+1)).substr(-2)+'-'+ ("0"+sdate.getDate()).substr(-2));
}

// 헤더 상하 슬라이드
function wideHeadFunc (_this) {
	var _this = $(_this);

	if (_this.parent().hasClass("open")) {
		$(".right_top").fadeOut(300);
		$(".header").stop().animate({"margin-top":"-80px"}, 300);
		$(".container").stop().animate({"height":"1080px"}, 300);
		$(".header_over").stop().animate({"top":"80px"}, 300);
		$(".video_tab").fadeOut(300);
		$(".heder_event ul").stop().animate({"top":"47px"}, 300);
	}else {
		$(".right_top").fadeIn(300);
		$(".header").stop().animate({"margin-top":"0px"}, 300);
		$(".container").stop().animate({"height":"1000px"}, 300);
		$(".header_over").stop().animate({"top":"-50px"}, 300);
		$(".video_tab").fadeIn(300);
		$(".heder_event ul").stop().animate({"top":"36px"}, 300);
	}
}

// 좌측 메뉴 좌우 슬라이드
function wideFunc (_this) {
	var _this = $(_this);
	if (_this.hasClass("left_open")) {
		$(".left_menu").stop().animate({"left":"-306px"}, 300);
		$(".right_content").stop().animate({"left":"15px"} , 300);
		_this.addClass("left_close").removeClass("left_open");
		$(".smart_popup").stop().animate({"left":"8px"}, 300);
		$(".smart_popup .smart_popup_img").stop().animate({"width":"1905px"}, 300);
		$(".smart_popup .snapshot_video").stop().animate({"width":"1905px"}, 300);
		$(".smart_popup .snapshot_top ul").stop().animate({"width":"100%"}, 300);
	}else if (_this.hasClass("left_close")){
		$(".left_menu").stop().animate({"left":"0px"}, 300);
		$(".right_content").stop().animate({"left":"314px"}, 300);
		_this.addClass("left_open").removeClass("left_close");
		$(".smart_popup").stop().animate({"left":"314px"}, 300);
		$(".smart_popup .smart_popup_img").stop().animate({"width":"1606px"}, 300);
		$(".smart_popup .snapshot_video").stop().animate({"width":"1606px"}, 300);
		$(".smart_popup .snapshot_top ul").stop().animate({"width":"1541px"}, 300);
	}
	setTimeLineFlag(1);
}

function treeBtnFunc (_this) {
	var _this = $(_this);
	var _parent = _this.parent();

	if (_this.hasClass("select_folder")) {
		_this.removeClass('select_folder');
	}else{
		$(".set01 .select_folder").removeClass("select_folder");
		_this.addClass("select_folder");
	}

	// panel 클릭
	if (_this.parent().parent("ul").hasClass("tree02")) {
		var panelNum = _this.parent("li").attr("class").substr(9,15);
		//$(".panel_cont>ul").hide();
		//$(".panel_cont").find(".panel_cont"+panelNum).show();
	}else if(_this.parent().parent("ul").hasClass("tree01")){
		
	}
	
	setTimeout(function(){
		var tabLength = $(".video_tab .crsl-wrap .crsl-item").length;
		if (tabLength == 1) {
			$(".video_tab .crsl-wrap").css("text-align","center");
		}else if (tabLength == 2) {
			$(".video_tab .crsl-wrap").css("text-align","center");
		}else if (tabLength >= 3) {
			$(".video_tab .crsl-wrap").css("text-align","left");
		}
		
	}, 100)
	
}

//tab slide 
function nextFunc () {
	var wrapMarL = parseInt($(".crsl-wrap").css("margin-left")) 
	var rightNum = wrapMarL-163 
	var tabLength = $(".video_tab .crsl-wrap .crsl-item").length;
	var quotient = Math.floor(tabLength / 3)   
	if (( tabLength % 3 ) > 0) {
		quotient = quotient+1;
	}
	if ( (( rightNum/163 )*-1) == quotient  ) {
		
	}else {
		$(".crsl-wrap").css("margin-left", rightNum);
	}
}

function prevFunc () {
	var wrapMarL = parseInt($(".crsl-wrap").css("margin-left")) 
	var rightNum = wrapMarL+163 
	var tabLength = $(".video_tab .crsl-wrap .crsl-item").length; 
	var quotient = Math.floor(tabLength / 3)   
	if (wrapMarL == 0) {
		
	}else {
		$(".crsl-wrap").css("margin-left", rightNum);
	}
}

// 오디오정보 리스트
function audioList (_this) {
	var _this = $(_this);
	_this.next(".audio_list").slideDown(300);
}

function audioListClose (_this) {
	var _this = $(_this);
	_this.parent(".audio_list").slideUp(300);
}

// 이벤트 리스트
function eventList (_this) {
	var _this = $(_this);
	if (_this.hasClass("on")) {
		_this.removeClass("on");
		$(".heder_event ul").stop().slideUp(300);
		$("#alarm_list_bg").hide();
	}else {
		_this.addClass("on");
		$(".heder_event ul").stop().slideDown(300);
		$("#alarm_list_bg").show();
	}
	
}
function eventListAuto () {
	$(".heder_event ul").stop().slideDown(300);
	$(".heder_event p button").addClass("on");
}

// 전체화면
function allView () {
	$(".right_top").fadeOut(300);
	$(".header").stop().animate({"margin-top" : "-70px"}, 300);
	$(".container").stop().animate({"height" : "1070px"}, 300);
	$(".left_menu").stop().animate({"left" : "-306px"}, 300);
	$(".right_content").stop().animate({"left" : "15px"}, 300);
	$(".right_content.content_event").stop().animate({"left" : "0px"}, 300);
	$(".left_open").addClass("left_close").removeClass("left_open");
	// $(".heder_event").html($(".header_time_event .heder_event").html());
	$(".video_tab").fadeOut(300);
	$(".wide_over_wrap").css({"display" : "block","top" : "80px"});
	$(".wide_over_wrap .all_view_close").stop().animate({"top" : "-25px"}, 300)
	// $(".heder_event ul").stop().animate({"top":"47px"}, 300);
	setTimeout(function() {
		$(".all_view_close").fadeIn(100);
	}, 600);

	$(".right_content_alarm .video_list ").stop().animate({"height" : "88.9%"}, 300);
	$(".heder_event").addClass("wide_event2");
	// 전체뷰에서는 숨기기
	$(".heder_event").hide();
	$(".smart_popup").stop().animate({"left":"8px"}, 300);
	$(".smart_popup .smart_popup_img").stop().animate({"width":"1905px", "height":"884px"}, 300);
	$(".smart_popup .snapshot_video").stop().animate({"width":"1905px", "height":"773px"}, 300);
	$(".smart_popup .snapshot_top ul").stop().animate({"width":"100%"}, 300);
	
	$("#wide_video2_panel_cont").css("height","950px");
	$("#wide_video2_panel_cont2").css("height","977px");
	setTimeLineFlag(1);
}

// 전체화면 해제
function allViewClose () {
	$(".right_top").fadeIn(300);
	$(".header").stop().animate({"margin-top" : "0px"}, 300);
	$(".container").stop().animate({"margin-top" : "0px","height" : "1000px"}, 300);
	$(".left_menu").stop().animate({"left" : "0px"}, 300);
	$(".right_content").stop().animate({"left" : "314px"}, 300);
	$(".right_content.content_event").stop().animate({"left" : "0px"}, 300);
	$(".left_close").addClass("left_open").removeClass("left_close");
	$(".video_tab").fadeIn(300);
	$(".wide_over_wrap").css("top", "-130px");
	// $(".heder_event ul").stop().animate({"top":"36px"}, 300);
	$(".all_view_close").hide();
	$(".right_content_alarm .video_list ").stop().animate({"height" : "88.1%"}, 300);
	$(".heder_event").removeClass("wide_event2");
	$(".heder_event").removeClass("wide_event");

	// 전체뷰에서 보이기
	$(".heder_event").show();
	$(".smart_popup").stop().animate({"left":"314px"}, 300);
	$(".smart_popup .smart_popup_img").stop().animate({"width":"1606px", "height":"817px"}, 300);
	$(".smart_popup .snapshot_video").stop().animate({"width":"1606px", "height":"705px"}, 300);
	$(".smart_popup .snapshot_top ul").stop().animate({"width":"1541px"}, 300);
	
	$("#wide_video2_panel_cont").css("height","890px");
	$("#wide_video2_panel_cont2").css("height","902px");
	setTimeLineFlag(1);
}


// 트리 화살표(슬라이드효과)
function treeSlide (_this) {
	var _this = $(_this);
	if ( _this.siblings("ul").children("li").length >= 1) {
		// 하위 패널이 있을때만 작동
		if (_this.hasClass("ico_arrow_open")) {
			_this.addClass("ico_arrow_close").removeClass("ico_arrow_open");
			_this.next().children(".ico").addClass("ico_folder_close").removeClass("ico_folder_open");
		}else if(_this.hasClass("ico_arrow_close")){
			_this.addClass("ico_arrow_open").removeClass("ico_arrow_close");
			_this.next().children(".ico").addClass("ico_folder_open").removeClass("ico_folder_close");
		}
		_this.siblings("ul").slideToggle(300);
	}
}

// 오른쪽 마우스
function mouseRight (_this) {
	var _this = $(_this);
	var $x=event.clientX;
    var $y=event.clientY;
	var panelNum;
	event.preventDefault();

	if (_this.hasClass("select_folder")) {
		$(".panel_change").show()
		if ($(".select_folder").parent().attr("class") == "panel_btn1") {
			panelNum == '1x1';
		}else if ($(".select_folder").parent().attr("class") == "panel_btn2") {
			panelNum == '1+3';
		}else if ($(".select_folder").parent().attr("class") == "panel_btn3") {
			panelNum == '3+1';
		}else if ($(".select_folder").parent().attr("class") == "panel_btn4") {
			panelNum == '2x2';
		}else if ($(".select_folder").parent().attr("class") == "panel_btn5") {
			panelNum == '1+5';
		}else if ($(".select_folder").parent().attr("class") == "panel_btn6") {
			panelNum == '2+8';
		}else if ($(".select_folder").parent().attr("class") == "panel_btn7") {
			panelNum == '4x4';
		}else if ($(".select_folder").parent().attr("class") == "panel_btn8") {
			panelNum == '8x8';
		}
		$(".panel_change>li>button").text(panelNum);
		$(".panel_change").css({"top":$y-119, "left":$x-40});
	}
}


// 폴더 추가
function folderAdd () {
	var btnEle = "";

	btnEle += "<li>";
	btnEle += "<span class='ico_arrow ico_arrow_close'></span> ";
	btnEle += "<span class='ico ico_folder_close'></span> ";
	btnEle += "<input type='text' class='tree_input' /> ";
	btnEle += "<button type='button' class='tree_input_btn' onclick='folderAdd2(this);'><span>생성</span></button></li>";

	$(".set01 .tree01").append(btnEle);
	
}

// 폴더 추가 -> 생성
function folderAdd2 (_this) {
	var _this = $(_this);
	var thisValue = _this.parent().find(".tree_input").val();
	var btnEle = "";

	btnEle += "<li>";
	btnEle += "<span class='ico_arrow ico_arrow_close'></span> ";
	btnEle += "<button type='button' class='tree_list_btn' onclick='treeBtnFunc(this);'>"
	btnEle += "<span class='ico ico_folder_close'></span> ";
	btnEle += "<span class='tree_list_text'>" + thisValue + "</span>"
	btnEle += "</button><ul class='tree02'></ul></li>"
	
	$(".set01 .tree01").append(btnEle);
	_this.parent().remove();
}


// 폴더 이름 수정
function folderName () {
	var btnEle = "";
	
	if ($(".select_folder").hasClass("tree_local")) {
		// 로컬은 이름 수정 불가능
	}else {
		if ($(".select_folder").siblings().hasClass("ico_arrow_open")) {
		// 열린 모양 폴더 이름 수정시
		btnEle += " <span class='ico_arrow ico_arrow_open' onclick='treeSlide(this);'></span> ";
		btnEle += "<span class='select_folder'>";
		btnEle += "<span class='ico ico_folder_open'></span> ";
		btnEle += "<input type='text' class='tree_input' /> ";
		btnEle += "<button type='button' class='tree_input_btn' onclick='folderNameComplete(this);' ><span>수정</span></button> ";
		btnEle += "<button type='button' class='tree_input_btn' onclick='folderNameCancel(this);'><span>취소</span></button></span>";

	}else if($(".select_folder").siblings().hasClass("ico_arrow_close")){
		// 닫힌 모양 폴더 이름 수정시
		btnEle += "<span class='select_folder'>";
		btnEle += "<span class='ico_arrow ico_arrow_close' onclick='treeSlide(this);'></span> ";
		btnEle += "<span class='ico ico_folder_close'></span> ";
		btnEle += "<input type='text' class='tree_input' /> ";
		btnEle += "<button type='button' class='tree_input_btn' onclick='folderNameComplete(this);' ><span>수정</span></button> ";
		btnEle += "<button type='button' class='tree_input_btn' onclick='folderNameCancel(this);'><span>취소</span></button></span>";

	}else if ($(".select_folder").children().hasClass("ico_panel")) {
		// 패널 이름 수정시
		btnEle += "<span class='select_folder'>";
		btnEle += "<span class='ico ico_panel'></span> ";
		btnEle += "<input type='text' class='tree_input' /> ";
		btnEle += "<button type='button' class='tree_input_btn' onclick='folderNameComplete(this);' ><span>수정</span></button> ";
		btnEle += "<button type='button' class='tree_input_btn' onclick='folderNameCancel(this);'><span>취소</span></button></span>";
		}
		
		$(".set01 .tree01").find(".select_folder").parent().prepend(btnEle);
		$(".set01 .tree01").find("button.select_folder").hide();
		$(".set01 .tree01").find(".select_folder").next(".ico_arrow").remove();
		//$(".select_folder").css("background-color","#00090d");
	}
	
}

// 이름 수정 완료
function folderNameComplete (_this) {
	var _this = $(_this);
	var thisValue = _this.parent().find(".tree_input").val();
	var btnEle = "";
	
	if ($(".select_folder").siblings().hasClass("ico_arrow_open")) {
		// 열린 모양 폴더 이름 수정 완료
		btnEle += "<span class='ico_arrow ico_arrow_open' onclick='treeSlide(this);'></span> ";
		btnEle += "<button type='button' class='tree_list_btn' onclick='treeBtnFunc(this);'>"
		btnEle += "<span class='ico ico_folder_open'></span> ";
		btnEle += "<span class='tree_list_text'>" + thisValue + "</span>"
		btnEle += "</button>"

	}else if($(".select_folder").siblings().hasClass("ico_arrow_close")){
		// 닫힌 모양 폴더 이름 수정 완료
		btnEle += "<span class='ico_arrow ico_arrow_close' onclick='treeSlide(this);'></span> ";
		btnEle += "<button type='button' class='tree_list_btn' onclick='treeBtnFunc(this);'>"
		btnEle += "<span class='ico ico_folder_close'></span> ";
		btnEle += "<span class='tree_list_text'>" + thisValue + "</span>"
		btnEle += "</button>"

	}else if ($(".select_folder").children().hasClass("ico_panel")) {
		// 패널 이름 수정 완료
		btnEle += "<button type='button' class='tree_list_btn tree_panel' onclick='treeBtnFunc(this);'>"
		btnEle += "<span class='ico ico_panel'></span> ";
		btnEle += "<span class='tree_list_text'>" + thisValue + "</span>"
		btnEle += "</button>"
	}
	$("span.select_folder").prev(".ico_arrow").remove();
	$(".set01 .tree01").find(".select_folder").parent().prepend(btnEle);
	$(".set01 .tree01").find("button.select_folder").remove();
	_this.parent().remove();
	$(".select_folder").css("background-color","666");
}

// 이름 수정 취소
function folderNameCancel (_this) {
	var _this = $(_this);
	_this.parent().remove();
	$(".set01 .tree01").find("button.select_folder").show();
	$(".select_folder").css("background-color","666");
}

// 폴더 삭제
function folderDel () {
	if ($(".select_folder").hasClass("tree_local")) {
	}else {
		if ($(".select_folder").children().hasClass("ico_panel")) {
		
			// 패널 삭제시 우측 영상 패널도 같이 사라지게
			var selectPanel = $(".select_folder").parent("li").attr("class");
			var panelIdx = selectPanel.substr(9,15);
			$(".panel_cont" + panelIdx).hide();

			// 패널 삭제했을때 더이상 그룹안에 패널이 없을때 상위 폴더 close 모양으로 변경
			if ($(".select_folder").parents(".tree02").children("li").length == 1) {
				$(".select_folder").parents(".tree02").prev(".tree_list_btn").siblings(".ico_arrow_open").addClass("ico_arrow_close").removeClass("ico_arrow_open");
				$(".select_folder").parents(".tree02").prev(".tree_list_btn").children(".ico_folder_open").addClass("ico_folder_close").removeClass("ico_folder_open");
				$(".select_folder").parent("li").remove();
			}else {
				$(".select_folder").parent("li").remove();
			}
		}else {
			$(".select_folder").parent("li").remove();
		}
	}
}

// 패널 리스트 띄우기
function panelList () {
	if ($(".select_folder").hasClass("tree_panel ")) {
		// 패널을 선택한 상태일때는 패널 추가 리스트 띄우지않음
	}else {
		if ($(".panel_list").css("display") == "none") {
			$(".panel_list").show();
		}else {
			$(".panel_list").hide();
		}
	}
}

// 패널 추가
function panelAdd (_this) {
	var _this = $(_this);
	var thisValue = _this.children(".panel_list_name").text();
	var classNum = _this.children(".panel_ico").parents("li").index() + 1 ;
	var btnEle = "";

	btnEle += "<li class='panel_btn" + classNum+ "'>";
	btnEle += "<button type='button' class='tree_list_btn tree_panel' onclick='treeBtnFunc(this);' oncontextmenu='mouseRight(this);'>";
	btnEle += "<span class='ico ico_panel'></span> ";
	btnEle += "<span class='tree_list_text'>패널 (" + thisValue + ")</span>";
	btnEle += "</button>";

	if ($(".select_folder").children().hasClass("ico_panel")) {
		// panel을 선택했을시에는 하위에 추가X
	}else {
		if ($(".select_folder").next(".tree02").children("li").length >= 1) {
			$(".select_folder").next(".tree02").append(btnEle);
		}else if($(".select_folder").next(".tree02").children("li").length == 0) {
			$(".select_folder").next(".tree02").append(btnEle);
			$(".select_folder").siblings(".ico_arrow_close").addClass("ico_arrow_open").removeClass("ico_arrow_close");
			$(".select_folder").children(".ico_folder_close").addClass("ico_folder_open").removeClass("ico_folder_close");
		}
	}

	$(".panel_list").hide();
}

// 패널 스타일 변경
function listOpen (_this) {
	var _this = $(_this);
	if (_this.next(".panel_change_list").css("display") == "block") {
		_this.next(".panel_change_list").hide();
	}else {
		_this.next(".panel_change_list").show();
	}
	setTimeout(function  () {
		_this.parents(".panel_change").show();
	}, 50)
	
}

// 패널 변경(우측 마우스 눌러서 나오는 메뉴에서 작동)
function panelChange (_this) {
	var _this = $(_this);
	var thisTxt = _this.children(".panel_list_name").text();
	var thisIdx = _this.parent().index() + 1;
	$(".panel_change_list").prev("button").text(thisTxt);
	$(".select_folder").parent("li").removeClass().addClass("panel_btn"+thisIdx);
	$(".panel_change").hide();
}

// 패널/폴더 복사
function copyFunc (_this) {
	var selectEle ="";
	var _this = $(_this);
	$(".folder_copy").remove();
	var selectEleClass = $(".select_folder").parent().attr("class");
	selectEle = $(".select_folder").parent().html();
	if ($(".select_folder").hasClass("tree_local")) {

	}else {
		_this.parents(".list_cont").find(".tree_area").append("<div class='folder_copy' style='display:none'><li class='"+selectEleClass+"'>"+selectEle +"</div>")
	}
	
	
}

// 패널/폴더 붙여넣기
function pasteFunc (_this) {
	var pasteEle ="";
	var _this = $(_this);
	pasteEle = _this.parents(".list_cont").find(".tree_area").find(".folder_copy").html();
	if ($(".select_folder").hasClass("tree_local")) {
	}else {
		$(".select_folder").parent().after(pasteEle);
		$(".select_folder").eq(1).removeClass("select_folder");
		$(".folder_copy").remove();
	}
	
}


// 알람 슬라이드
function alarmOpen () {
	$(".alarm_slide").show().stop().animate({"opacity":"1", "right":"0px"},300);
}

function alarmClose () {
	$(".alarm_slide").stop().animate({"opacity":"0", "right":"-255px"}, 300);
}

// 영상 확대보기
function wideViewOpen (_this) {
	var _this = $(_this);
	var thisHtml ="";
	
	$(".heder_event ul").slideUp(300);
	$(".heder_event button").removeClass("on");
	$(".alarm_slide").stop().animate({"opacity":"0", "right":"-255px"}, 300);
	if (_this.hasClass("wide")) {
		$(".right_top").fadeIn(300);
		$(".header").stop().animate({"margin-top":"0px"}, 300);
		$(".container").stop().animate({"margin-top":"0px", "height":"1000px"}, 300);
		$(".panel_cont").show();
		$(".wide_video").hide();
		$(".video_list .now_wide").append($(".wide_video .video_list>li").children());
		$(".now_wide").removeClass();
		_this.removeClass("wide");
		$(".header_over").css("top","-50px");
		$(".heder_event ul").stop().animate({"top":"36px"}, 300);
	}else {
		$(".right_top").fadeOut(300);
		$(".header").stop().animate({"margin-top":"-80px"}, 300);
		$(".container").stop().animate({"margin-top":"0px", "height":"1050px"}, 300);
		$(".panel_cont").hide();
		_this.parents(".video_list>li").addClass("now_wide");
		$(".heder_event ul").stop().animate({"top":"47px"}, 300);
		
		thisHtml = _this.parents(".video_list>li").children();

		$(".wide_video").show();
		$(".wide_video ul li").append(thisHtml);
		_this.addClass("wide");
	}
	
}

// 팝업
function popOpen (popup) {
	var popup = $(popup);
	popup.show();
	$(".panel_change").hide();
}

function popClose (_this) {
	var _this = $(_this);
	_this.parents(".popup_wrap").hide();
}

// 탭
function tabFunc_z (_this) {
	var _this = $(_this);
	var _thisClass = _this.attr("class");
	_thisClass = _thisClass.split(" ")[0];
	_this.parents(".tab_header").find(".on").removeClass("on");
	_this.parent("li").addClass("on");
	
	_this.parents(".tab_style").find(".tab_cont").hide();
	_this.parents(".tab_style").find('.'+_thisClass).show();

}

function tabFunc (_this) {
	var _this = $(_this);
	var _thisClass = _this.attr("class");
	_this.parents(".tab_header").find(".on").removeClass("on");
	_this.parent("li").addClass("on");
	_this.parents(".tab_style").find(".tab_cont").hide();
	_this.parents(".tab_style").find('.'+_thisClass).show();
	/*
	if (_thisClass =="search01" || _thisClass =="search02" || _thisClass == "search03" || _thisClass == "search04") {
		$(".panel_cont .tab_cont").hide();
		$('.'+_thisClass).show();
	}*/
	
	if(_this.hasClass("search02")){
			$(".btn_route").show();
			$('#slider456').removeClass("timeline2");
			$('#slider456').addClass("timeline1");
	}else if(_this.hasClass("search01")){
		$(".btn_route").hide();
			$('#slider456').removeClass("timeline2");
			$('#slider456').addClass("timeline1");
	}else if(_this.hasClass("search03")){
			$('#slider456').removeClass("timeline1");
			$('#slider456').addClass("timeline2");		
	}else if(_this.hasClass("search04")){
			$('#slider456').removeClass("timeline1");
			$('#slider456').addClass("timeline2");	
	}
	if(_thisClass!="state01" && _thisClass!="state02"){
		$(".ui_view").hide();
		$(".ui_workflow").hide();
		$(".view_area_detail").hide();
		$(".ui_event_list .tbl_wrap").show();
		$(".ui_event_list .ui_tit").show();
		$(".timeline_list").find(".on").removeClass("on");		
	}

}

function tabFunc2 (_this) {
	var _this = $(_this);
	var _thisClass = _this.attr("class");
	_this.parents(".tab_header").find(".on").removeClass("on");
	_this.parent("li").addClass("on");
	_this.parents(".tab_style2").find(".tab_cont2").hide();
	_this.parents(".tab_style2").find('.'+_thisClass).show();

	if (_this.hasClass("smart02")) { // 이미지업로드
		$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","96px");

		if ($(".smart02 .set_event").css("display") == "block" ) {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","0px");

		}else if ($(".smart02 .set_event").css("display") == "block" && $(".smart02 .set_color").css("display") == "block") {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","0px");

		}else if ($(".smart02 .set_color").css("display") == "block") {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","83px");
		}

	}else if (_this.hasClass("smart03")) { // 차량
		$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","163px");

		if ($(".smart03 .set_event").css("display") == "block" ) {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","38px");

		}else if ($(".smart03 .set_color").css("display") == "block" ) {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","113px");

		}else if ($(".smart03 .set_event").css("display") == "block"  && $(".smart03 .set_color").css("display") == "block" ) {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","0px");
		}

	}else if(_this.hasClass("smart01")){ // 영역
		$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","205px");

		if ($(".smart01 .set_event").css("display") == "block") {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","81px");

		}else if ($(".smart01 .set_color").css("display") == "block") {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","155px");

		}else if ($(".smart01 .set_event").css("display") == "block" && $(".smart01 .set_color").css("display") == "block") {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","42px");
		}

	}else if (_this.hasClass("smart04")) { // 스냅샷
		$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","205px");

		if ($(".smart04 .set_event").css("display") == "block") {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","81px");

		}else if ($(".smart04 .set_color").css("display") == "block") {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","155px");

		}else if ($(".smart04 .set_event").css("display") == "block" && $(".smart01 .set_color").css("display") == "block") {
			$(".left_menu_vod .tab_cont.search02 .histroty_list").css("height","42px");
		}
	}
}

// 목록지우기
function listDel (_this) {
	var _this = $(_this);
	_this.parents(".tab_cont").find(".histroty_list ul li").remove();
	_this.parents(".tab_cont").find(".search_result_list .check").removeClass("check").children("input").prop("checked", false);
	if (_this.parents().hasClass("search01")) {
		$(".tab_cont.search01 .video_list li").remove();
		$(".tab_cont.search01 .panel_timeline").remove();
	}else if(_this.parents().hasClass("search02")) {
		$(".tab_cont.search02 .video_list li").remove();
		$(".tab_cont.search02 .panel_timeline").remove();
	}else if (_this.parents().hasClass("search03")) {
		$(".tab_cont.search03 .summary_list ul li").hide();
	}else if (_this.parents().hasClass("search04")) {
		$(".tab_cont.search04 .summary_list ul li").hide();
	}
}

//모아보기 개별보기
function alarmTab (_this) {
	var _this = $(_this);
	if(alarmViewOn==0) return;
		_this.parents(".alarm_view").find(".on").removeClass("on");
		_this.addClass("on");

		if (_this.hasClass("each")) {
			$(".right_content_alarm .panel_cont .video_list").hide();
			$(".alarm_each").show();
		}else if (_this.hasClass("all")) {
			$(".right_content_alarm .panel_cont .video_list").hide();
			$(".alarm_all").show();
		}

}

// VA 보기/끄기
/*function vaView (_this) {
	var _this = $(_this);
	if (_this.parents(".video_list_header").next(".video_list_cont").find(".video_va_area").css("display") == "block") {
		_this.parents(".video_list_header").next(".video_list_cont").find(".video_va_area").hide();
	}else {
		_this.parents(".video_list_header").next(".video_list_cont").find(".video_va_area").show();
	}
}*/



// on표시 함수
function thisOn (_this) {
	var _this = $(_this);
	if (_this.hasClass("on")) {
		_this.removeClass("on");
	}else {
		_this.parent().find(".on").removeClass("on");
		_this.addClass("on");
	}
}

function searchResult () {
	$(".video_list").hide();
	$(".video_list.smart_result").show();
	$(".panel_timeline").hide();
	$(".panel_timeline.smart_result").show();
	
}


function ConvertTimestamp(unix_timestamp){
	
//console.log(unix_timestamp);

var date = new Date(parseInt(unix_timestamp)-9*3600*1000);
 var year = date.getFullYear();
  var month = "0" +(date.getMonth()+1);
  var today = "0" +date.getDate();
// Hours part from the timestamp
var hours = "0" +date.getHours();
// Minutes part from the timestamp
var minutes = "0" + date.getMinutes();
// Seconds part from the timestamp
var seconds = "0" + date.getSeconds();

// Will display time in 10:30:23 format
var formattedTime = year + month.substr(-2) + today.substr(-2) + hours.substr(-2)  + minutes.substr(-2)  + seconds.substr(-2);

return formattedTime;
}

function ConvertTimeString(timestr){
	
 var year = timestr.substr(0,4);
  var month = timestr.substr(4,2);
  var today = timestr.substr(6,2);
// Hours part from the timestamp
var hours = timestr.substr(8,2);
// Minutes part from the timestamp
var minutes = timestr.substr(10,2);
// Seconds part from the timestamp
var seconds = timestr.substr(12,2);

// Will display time in 10:30:23 format
var formattedTime = year +'-'+ month.substr(-2) +'-'+ today.substr(-2) +' '+ hours.substr(-2)  +':'+ minutes.substr(-2)  +':'+ seconds.substr(-2);

return formattedTime;
}

function ConvertTimestamp2(timestr){
var year = timestr.substr(0,4);
  var month = timestr.substr(4,2);
  var today = timestr.substr(6,2);
// Hours part from the timestamp
var hours = timestr.substr(8,2);
// Minutes part from the timestamp
var minutes = timestr.substr(10,2);
// Seconds part from the timestamp
var seconds = timestr.substr(12,2);
var milisec = 0;
if(parseInt(timestr.substr(15,3))>0) milisec = (parseInt(timestr.substr(15,3)));

	return (new Date(year,parseInt(month,10)-1,today,hours,minutes,seconds)).getTime() + ((new Date()).getTimezoneOffset() * 60 * 1000 * -1) + milisec;
}

function ConvertURL(url){
	return url;
	//console.log(url);
	//url=url.trim();
	if(url.substr(-4)=='m3u8')
		return 'http://localhost:8030/test/10110000001091001.m3u8';
		//return 'http://211.54.3.129:8888/hlstest3/16fps.m3u8';
	else if(url.substr(-6)=='stream')
		//return url;
		return 'rtsp://192.168.0.100:1935/gigaeyeslive/test-1002.stream';
	return url;
	
var list = [
	//{src:'rtsp:\/\/192.168.0.100:1935\/',tgt:'rtsp:\/\/211.54.3.138:1935\/'}
	//{src:'192.168.0.100',tgt:'211.54.3.138'}
	//{src:'211.54.3.138',tgt:'192.168.0.100'}
	{src:'rtsp://210.101.86.241:1935/gigaeyesmonitor/cam0000000274.stream',tgt:'file:///C:/Users/signalpark/Videos/vlc-record-2016-12-29-17h22m52s-rtsp___118.129.90.193_554_Master-0-.mp4'},
	{src:'rtsp://210.101.86.241:1935/gigaeyesmonitor/cam0000000234.stream',tgt:'file:///C:/Users/signalpark/Videos/vlc-record-2016-12-29-17h22m52s-rtsp___118.129.90.193_554_Master-0-.mp4'},
	{src:'rtsp://210.101.86.241:1935/gigaeyesmonitor/cam0000000189.stream',tgt:'file:///C:/Users/signalpark/Videos/vlc-record-2016-12-29-17h22m52s-rtsp___118.129.90.193_554_Master-0-.mp4'},
	{src:'rtsp://210.101.86.241:1935/gigaeyesmonitor/cam0000000250.stream',tgt:'file:///C:/Users/signalpark/Videos/vlc-record-2016-12-29-17h22m52s-rtsp___118.129.90.193_554_Master-0-.mp4'},
	{src:'rtsp://210.101.86.241:1935/gigaeyesmonitor/cam0000000236.stream',tgt:'file:///C:/Users/signalpark/Videos/vlc-record-2016-12-29-17h22m52s-rtsp___118.129.90.193_554_Master-0-.mp4'}
	
	];
	
	for(var i=0;i < list.length;i++){
		var src = list[i].src;
		var tgt = list[i].tgt;
		//if(url.substr(0,src.length) == src)
		url = url.replace(src,tgt);
	//break;
	}
	//console.log(url);
	return url;
	
}
function ConvertTagToStyleName(code){
	console.log('event_detail_tag_cd : '+code);
	switch(code){
		case '11': 
			return 'event_red';
		case '12':
			return 'event_red';
		case '13':
			return 'event_red';
		case '14':
			return 'event_red';
		case '15':
			return 'event_red';
		case '16':
			return 'event_red';
		case '21':
			return 'event_red';
		case '22':  //침범
			return 'event_orange';
		case '23':  // 이탈 
			return 'event_purple';
		case '24':  // 침범 이탈
			return 'event_orange';
		case '25':  // 배회
			return 'event_yellow';
		case '26':  // 제외
			return 'event_gray';
		case '10001':  // 도난 센서
			return 'event_green';		
		case '10002':  // 문열림 센서
			return 'event_green';			
		case '10003':  // 소리 ? 움직임 
			return 'event_green';						
		default : 
			return 'event_red';
	}
}
function ConvertColorCdToStyleName(code){
	switch(code){
		case '1': //검(Black) #000000
			return 'color07';
		case '2': //흰(White) #FFFFFF
			return 'color06';
		case '3': //빨(Red) #FF0000
			return 'color01';
		case '4': //노(Yellow) #FFFF00
			return 'color02';
		case '5': //초(Green) #00FF00 
			return 'color03';
		case '6': //(Cyan) #00FFFF
			return 'color08';
		case '7': //파(Blue) #0000FF
			return 'color04';
		case '8': //보(Magenta) #FF00FF
			return 'color05';
	}			
}
function ConvertAspectRatio(strRatio){
	console.log(strRatio);
	var tmp = strRatio.split(":");
	var ratio = parseFloat(tmp[0])/parseFloat(tmp[1]);
	return ratio;
}

function setCursor(){
	$( "#my_cursor" ).html("<img src=\"./assets/cursor.png\" width=\"7px\" height=\"7px\" style=\"position:absolute;left:0px;top:0px;z-index:10;cursor:hand;\" ><div style=\"position:absolute;left:2px;top:-4px;z-index:1;\"><iframe name=\"iframe1\" src=\"\" border=\"0\" width=\"7px\" height=\"7px\" frameborder=\"0\"></iframe></div>");

}
function showImage(event,url){
	$( "#my_cursor" ).show();
	var left = (event.clientX<1500)?0:-340;
	$( "#my_cursor" ).html("<img src='"+url+"' onclick='hideImage()' style='position:relative;z-index:100;left:"+left+"px;top:-220px;bottom:0px;width:340px;height:220px;' />");
	  $( "#my_cursor" ).position({
		my: "left-5 top-5",
		of: event,
		collision: "fit"
	  });	
}
function showCurrentTime(event,str){
	
	var align = (event.clientX<1700)?"left":"right";
	var left = (event.clientX<1700)?"0":"-200";
	$( "#my_cursor" ).html("<div style='position:relative;z-index:100;left:"+left+"px;text-align:"+align+";top:10px;bottom:40px;width:200px;height:30px;'>"+str+"</div>");
	  $( "#my_cursor" ).position({
		my: "left+10 top+10",
		of: event,
		collision: "fit"
	  });
	$( "#my_cursor" ).show();	  
}
function hideImage(){
	$( "#my_cursor" ).hide();
}
function showThumb(_this,mode){
	var _this = $(_this);
	var dp_mode = (typeof mode=='undefined')?$("#display_mode").val():mode;
	var height="135px";
	switch(dp_mode)
	{
		case '0': height = "135px";break;
		case '1': height = "90px";break;
		case '2': height = "63px";break;
		case '3': height = "45";break;
		
	}
	_this.find("img").css("height",height);	
}
function hideThumb(_this){
	var _this = $(_this);
	_this.find("img").css("height","0px");	
}
function showThumbImage (_this) {
	var _this = $(_this);
	_this.parent("div").find(".thum_list").show();
}
function hideThumbImage (_this) {
	var _this = $(_this);
	//_this.parent("div").find(".thum_list").hide();
	_this.parents(".thum_list").hide();
}
function setAlarmListFlag(flag){
	$("#alarmCheckFlag").val(flag);
}
function setGroupReload(flag){
	$("#groupReload").val(flag);
}
function setTimeLineFlag(flag){
	$("#timelineFlag").val(flag);
}
function pushListWSdata(ws_string){
	if(master_ws_list.length>1000){
	master_ws_list=[];
	}
	master_ws_list.push(ws_string);
	
}
function getAlarmListFlag(){
	return $("#alarmCheckFlag").val();
}
function getGroupReload(){
	return $("#groupReload").val();
}
function getTimeLineFlag(){
	return $("#timelineFlag").val();
}
function  urlBase64Decode(str,str1) {
	//if(str1.substr(-6) != '==.txt' && str1.substr(-5) != '=.txt' && str1.substr(-4) != '.txt') return str1;
	try{
      var output = str.replace('-', '+').replace('_', '/');
      switch (output.length % 4) {
        case 0: { break; }
        case 2: { output += '=='; break; }
        case 3: { output += '='; break; }
        default: {
			
          //throw 'Illegal base64url string!';
		 
        }
      }
      // return window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
      return decodeURIComponent(escape(window.atob(output))); //polifyll https://github.com/davidchambers/Base64.js
	}catch(err){
		console.log(err);
		return(str1);
	}

}
function allowAttachFileExtension(){
var extension_list = ['docx', 'doc', 'hwp', 'xls', 'xlsx', 'ppt', 'pptx', 'zip', 'pdf', 'txt','jpg','gif','png','jpeg' ];
	return extension_list;
}
function checkAttachFileExtension(filename)
{
var extension_list = ['docx', 'doc', 'hwp', 'xls', 'xlsx', 'ppt', 'pptx', 'zip', 'pdf', 'txt','jpg','gif','png','jpeg' ];
var extensions = filename.split('.');
var extension = extensions[extensions.length -1];
	console.log(extension);
	if($.inArray(extension,extension_list)>=0){
		return true;
	}
	return false;
	
}