var g_imgDomHtml = "";

g_imgDomHtml += "<div class='thum thum0<<thum_num>>'>";
g_imgDomHtml += "<div class='thum_conts'>";
g_imgDomHtml += "<div class='thum_img'>";
g_imgDomHtml += "<img src='<<img_src>>' alt='<<img_alt>>' width='363' height='239' ng-click=\"viewImg('<<img_path>>');\">";
g_imgDomHtml += "<button class='btn_x' id='delete' ng-click=\"deleteImg('<<f_num>>');\">X</button>";
g_imgDomHtml += "</div>";
g_imgDomHtml += "<div class='thum_txt'>";
g_imgDomHtml += "<p><<img_desc>></p>";
g_imgDomHtml += "</div>";
g_imgDomHtml += "</div>";
g_imgDomHtml += "</div>";

var g_imgEmptyDomHtml = "";
g_imgEmptyDomHtml += "<div class='thum thum0<<thum_num>>'>";
g_imgEmptyDomHtml += "<div class='thum_conts'>";
g_imgEmptyDomHtml += "<div class='thum_img'  ng-click=\"fileNew('<<img_id>>');\">";
g_imgEmptyDomHtml += "<div class='uptxt'>";
g_imgEmptyDomHtml += "<p>사진업로드</p>";
g_imgEmptyDomHtml += "</div>";
g_imgEmptyDomHtml += "<i class='fas fa-cloud-upload-alt'></i>";
g_imgEmptyDomHtml += "</div>";
g_imgEmptyDomHtml += "<div class='thum_txt'>";
g_imgEmptyDomHtml += "<p>&nbsp;</p>";
g_imgEmptyDomHtml += "</div>";
g_imgEmptyDomHtml += "</div>";
g_imgEmptyDomHtml += "</div>";

var commonUtil = {
	imageFileDrag:{
		el : {
			popupId : "#dialog_commonImageFileDrag",
		},
		openPopup : function(elId, inputName, folderName, returnFuc) {
//			$(commonUtil.imageFileDrag.el.popupId).find("input").val("");//공백 처리(기존 입력 삭제)
			$(commonUtil.imageFileDrag.el.popupId).find("input[name='folderName']").val(folderName);//저장폴더 세팅 (불러온 js에 정의 되어있음 )
			$(commonUtil.imageFileDrag.el.popupId).dialog({
			    open: function(event, ui) {
		            		$(".ui-dialog-titlebar-close", $(this).parent()).hide();
			    	  }, //상단 닫기 버튼 안보임
				title: "이미지 업로드 ",
				modal : true,
				draggable: true,
			    resizable: false,
				height: 700,
				width: 	580,
				draggable: true,
				closeOnEscape: false,
				buttons : {
					"확인" : function() {
						alert($(this).dialog( "option", "draggable" ));



						commonUtil.imageFileDrag.upload(elId, inputName, folderName, returnFuc);
					},
					"취소" : function() {
						var existFile=$(commonUtil.imageFileDrag.el.popupId).find("input[name='orgFileName']").val();
						console.log("existFile",existFile);
						if(existFile==""){
							$(this).dialog("close");
						}else{
							alert("이미지를 삭제해주세요");
						}
					},
				},
				close: function( event, ui ) {
					var instance = $(this).dialog("instance");
					if(typeof instance != 'undefined') {
						$(this).dialog("destroy");
					}
				}
			});

		},
		upload: function(elId, inputName, folderName, returnFuc) {

			var fileData = $(commonUtil.imageFileDrag.el.popupId + " input[name='fileData']").val();
			console.log('fileData :',fileData);
			if(commonUtil.isEmpty(fileData)|| fileData=="") {
				alert("[필수] 이미지를 선택하여 주세요");
				return;
			}

			var fileDesc = $(commonUtil.imageFileDrag.el.popupId + " input[name='fileDesc']").val();
			console.log('fileDesc :',fileDesc);
			if(commonUtil.isEmpty(fileDesc)|| fileDesc=="") {
				alert("[필수] 이미지설명을 넣어주세요");
				return;
			}
			var orgFileName = $(commonUtil.imageFileDrag.el.popupId + " input[name='orgFileName']").val();


			var condition = {};
			condition.folderName = folderName;
			condition.imageName = folderName;
			condition.fileDesc = fileDesc;
			condition.orgFileName=orgFileName;
			$.ajax({
				type: "POST",
				url:  '/common/dragFileUpload.json',
				async: false,
				data: condition,
				dataType: "json",
				success: function(data) {
					if(data.errName) {
						alert(data.errName + ":" + data.errMessage);
					}else {
						if(! commonUtil.isEmpty(data)) {
							console.log("data 입니다.: ",data);
//							commonUtil.imageFileDrag.setFileElement(elId, inputName, data);
		        			$(commonUtil.imageFileDrag.el.popupId).dialog("close");

//		        			$(".uploadedList").children("div").remove();
		        			console.log("returnFuc = ", returnFuc);
		        			window[returnFuc](data);
		        		}else {
		        			alert("파일업로드에 문제가 발생하였습니다.");
		        		}
					}
				},
				error: function(XMLHttpRequest,textStatus,errorThrown) {
					alert("[Error] 이미지 저장 실패: "+textStatus);
				}
			});
		},
//		setFileElement: function(elId, inputName, fileInfo) {
////			$("#"+elId).find(".none").hide();
////			$("#"+elId).find(".visible").show();
//			$("#"+elId).find(".visible img").attr("src",  fileInfo.F_PATH);
//			$("#"+elId).find("input[name='imageDesc']").val(fileInfo.F_TITLE);
//			$("#"+elId).find("input[name='"+inputName+"']").val(fileInfo.F_NUM);//[중요] 파일번호
//		},
		//이미지 객체를 찾아서 초기화 시켜준다.
		initFiles: function(elId) {

			$("#dialog_commonImageFileDrag").find(".uploadedList").empty();
			$("#dialog_commonImageFileDrag").find("#fileData").val("");
			$("#dialog_commonImageFileDrag").find("#fileDesc").val("");
			$("#dialog_commonImageFileDrag").find("#folderName").val("");
			$("#dialog_commonImageFileDrag").find("#orgFileName").val("");
			$("#dialog_commonImageFileDrag").find("#fileDataName").val("");

		},
		//파일정보를 읽어와 해당영역에 세팅한다.
//		drawFileInfo: function(elId, inputName, filenumber) {
//			if(! commonUtil.isEmpty(filenumber)) {
//				var condition = {};
//				condition.filenumber = filenumber;
//				$.post( '/common/getFileInfo.json', condition, function(data) {
//					console.log(data);
//					if(data!=null){
//					commonUtil.imageFileDrag.setFileElement(elId, inputName, data);
//					}
//				}, "json");
//			}
//		},
		//팝업으로 이미지 다운로드 지원
		//exportImage : function(elId) {
		exportImage : function(imgPath) {
			//var dataUrl = $('#'+elId + ' .visible img').attr('src');
			var dataUrl = imgPath;
			var img = new Image();
			img.style.border = 'none';
			img.style.outline = 'none';
			img.style.position = 'fixed';
			img.style.left = '0';
			img.style.top = '0';
			img.title = '마우스 우클릭후 다른이름으로 저장 하세요!';
			img.onload = function() {
				var newWindow = window.open("", "popup_exportImage","scrollbars=0, toolbar=0, width="+img.width+", height="+img.height);
                newWindow.document.write("<a href='javascript: window.close();'>" + img.outerHTML + "</a>");
			};
			img.src = dataUrl;
		},
//		remove: function(elId, inputName, returnFuc) {
//			var filenumber = $("#"+elId).find("input[name='"+inputName+"']").val();
//			if(commonUtil.isEmpty(filenumber)) {
//				alert("삭제할 파일이 없습니다.");
//				return;
//			}
//			jConfirm("파일을 삭제하시겠습니까?").then(function(answer) {
//				if(answer == "true") {
//					var condition = {};
//					condition.filenumber = filenumber;
//					$.ajax({
//						type: "POST",
//						url:   '/common/dragFileRemove.json',
//						data: condition,
//						dataType: "json",
//						async: false,
//						success: function(data) {
//							if(data != null && data.errName) {
//								alert(data.errName + ":" + data.errMessage);
//								return;
//							}else {//Success
//								$("#"+elId).find("input[name='"+inputName+"']").val("");
//								$("#"+elId).find(".none").show();
//				    			$("#"+elId).find(".visible").hide();
//				    			$("#"+elId).find(".visible img").attr("src", "");
//				    			$("#"+elId).find("input[name='imageDesc']").val("");
//				    			$("#"+elId).find("input[name='"+inputName+"']").val("");
//
////				    			alert(returnFuc);
//								//실테이블 파일필드 상태 업데이트
//								window[returnFuc]();  //호출한 함수로 돌아가서 호출한다. 이것을 안하면 삭제가 덜되서 오류가 난다.
//							}
//						},
//						error: function(XMLHttpRequest,textStatus,errorThrown) {
//							alert("[Error] 삭제 실패: "+textStatus);
//						}
//					});
//				}
//			});
//
//		},
	},
//	imageFileUploder : {
//		el : {
//			popupId : "#dialog_commonImageFileUploader",
//		},
//		openPopup : function(elId, inputName, folderName, returnFuc) {
//			$(commonUtil.imageFileUploder.el.popupId).find("input").val("");
//			$(commonUtil.imageFileUploder.el.popupId).find("input[name='folderName']").val(folderName);//저장폴더 세팅
//			$(commonUtil.imageFileUploder.el.popupId).dialog({
//			    open: function(event, ui) {
//            		$(".ui-dialog-titlebar-close", $(this).parent()).hide();
//	    	  }, //상단 닫기 버튼 안보임
//				title: "이미지업로드",
//				modal : true,
//				buttons : {
//					"확인" : function() {
//						commonUtil.imageFileUploder.upload(elId, inputName, folderName, returnFuc);
//					},
//					"취소" : function() {
//						$(this).dialog("close");
//					},
//				},
//				close: function( event, ui ) {
//					var instance = $(this).dialog("instance");
//					if(typeof instance != 'undefined') {
//						$(this).dialog("destroy");
//					}
//				}
//			});
//		},
//		upload: function(elId, inputName, folderName, returnFuc) {
//			var fileData = $(commonUtil.imageFileUploder.el.popupId + " input[name='fileData']");
//			if(commonUtil.isEmpty($(fileData).val())) {
//				alert("[필수] 이미지를 선택하여 주세요");
//				return;
//			}
//			/* 이미지 파일인지도 체크  */
//			if( !(/image/i).test(fileData[0].files[0].type ) ) {
//				alert("올바른 이미지 파일을 선택해 주세요!");
//				return false;
//			}
//
//			var fileDesc = $(commonUtil.imageFileUploder.el.popupId + " input[name='fileDesc']").val();
//			if(commonUtil.isEmpty(fileDesc)) {
//				alert("[필수] 이미지설명을 넣어주세요");
//				return;
//			}
//
//            var max_width = 800;
//            var max_height = 600;
//
//			var reader = new FileReader();
//			reader.onload = function (e) {
//
//                var tmpImg = new Image();
//                tmpImg.src = reader.result;
//
//                tmpImg.onload = function() {
//                    var tmpW = tmpImg.width;
//                    var tmpH = tmpImg.height;
//
//                    if (tmpW > tmpH) {
//                        if (tmpW > max_width) {
//                           tmpH *= max_width / tmpW;
//                           tmpW = max_width;
//                        }
//                    } else {
//                        if (tmpH > max_height) {
//                           tmpW *= max_height / tmpH;
//                           tmpH = max_height;
//                        }
//                    }
//
//                    var canvas = document.createElement('canvas');
//                    canvas.width = tmpW;
//                    canvas.height = tmpH;
//                    var ctx = canvas.getContext('2d');
//                    ctx.drawImage(this, 0, 0, tmpW, tmpH);
//                    sURL = canvas.toDataURL("image/png");
//
//    				var imageObject = sURL;
//    				var condition = {};
//    				condition.folderName = folderName;
//    				condition.imageName = folderName;
//    				condition.imageObject = JSON.stringify(imageObject);
//    				condition.fileDesc = fileDesc;
//    				console.log("sURL");
//    				console.log(sURL);
//    				$("#cmFile").find("img").attr("src", sURL);
//    				alert("끝");
//    				return;
//    				$.ajax({
//    					type: "POST",
//    					url:  '/common/imageFileUpload.json',
//    					async: false,
//    					data: condition,
//    					dataType: "json",
//    					success: function(data) {
//    						if(data.errName) {
//    							alert(data.errName + ":" + data.errMessage);
//    						}else {
//    							if(! commonUtil.isEmpty(data)) {
//    								console.log("data 입니다.: ",data);
//    								commonUtil.imageFileUploder.setFileElement(elId, inputName, data);
//    			        			$(commonUtil.imageFileUploder.el.popupId).dialog("close");
//    			        			console.log("returnFuc = ", returnFuc);
//    			        			window[returnFuc]();
//    			        		}else {
//    			        			alert("파일업로드에 문제가 발생하였습니다.");
//    			        		}
//    						}
//    					},
//    					error: function(XMLHttpRequest,textStatus,errorThrown) {
//    						alert("[Error] 이미지 저장 실패: "+textStatus);
//    					}
//    				});
//
//                };
//
////				$('#'+prevId + ' img').attr('src', e.target.result);
//			};
//			/* input file에 있는 파일 하나를 읽어온다. */
//			reader.readAsDataURL(fileData[0].files[0]);
//
//		},
//		//파일정보를 읽어와 해당영역에 세팅한다.
//		drawFileInfo: function(elId, inputName, filenumber) {
//			if(! commonUtil.isEmpty(filenumber)) {
//				var condition = {};
//				condition.filenumber = filenumber;
//				$.post( '/common/getFileInfo.json', condition, function(data) {
//				    if(data!=null){
//					commonUtil.imageFileUploder.setFileElement(elId, inputName, data);
//				    }
//				}, "json");
//			}
//		},
//		//해당역역에 파일링크를 세팅한다.
//		setFileElement: function(elId, inputName, fileInfo) {
//			$("#"+elId).find(".none").hide();
//			$("#"+elId).find(".visible").show();
//			$("#"+elId).find(".visible img").attr("src",  fileInfo.F_PATH);
//			$("#"+elId).find("input[name='imageDesc']").val(fileInfo.F_TITLE);
//			$("#"+elId).find("input[name='"+inputName+"']").val(fileInfo.F_NUM);//[중요] 파일번호
//		},
//		//이미지 객체를 찾아서 초기화 시켜준다.
//		initFiles: function(elId, inputName) {
//			var fileUploader = $("#"+elId + " .imageFileUpload");
//			for(var i=0; i<fileUploader.length; i++) {
//				var id = $(fileUploader[i]).attr("id");
//				$("#"+id).find(".none").show();
//    			$("#"+id).find(".visible").hide();
//    			$("#"+id).find(".visible img").attr("src", "");
//    			$("#"+id).find("input[name='imageDesc']").val("");
//    			$("#"+id).find("input[name='"+inputName+"']").val("");
//			}
//		},
//		//팝업으로 이미지 다운로드 지원
//		exportImage : function(elId) {
//			var dataUrl = $('#'+elId + ' .visible img').attr('src');
//			var img = new Image();
//			img.style.border = 'none';
//			img.style.outline = 'none';
//			img.style.position = 'fixed';
//			img.style.left = '0';
//			img.style.top = '0';
//			img.title = '마우스 우클릭후 다른이름으로 저장 하세요!';
//			img.onload = function() {
//				var newWindow = window.open("", "popup_exportImage","scrollbars=0, toolbar=0, width="+img.width+", height="+img.height);
//                newWindow.document.write("<a href='javascript: window.close();'>" + img.outerHTML + "</a>");
//			};
//			img.src = dataUrl;
//		},
//		remove: function(elId, inputName, returnFuc) {
//			var filenumber = $("#"+elId).find("input[name='"+inputName+"']").val();
//			if(commonUtil.isEmpty(filenumber)) {
//				alert("삭제할 파일이 없습니다.");
//				return;
//			}
//			jConfirm("파일을 삭제하시겠습니까?").then(function(answer) {
//				if(answer == "true") {
//					var condition = {};
//					condition.filenumber = filenumber;
//					$.ajax({
//						type: "POST",
//						url:   '/common/deleteFileInfo.json',
//						data: condition,
//						dataType: "json",
//						async: false,
//						success: function(data) {
//							if(data != null && data.errName) {
//								alert(data.errName + ":" + data.errMessage);
//								return;
//							}else {//Success
//								$("#"+elId).find("input[name='"+inputName+"']").val("");
//								$("#"+elId).find(".none").show();
//				    			$("#"+elId).find(".visible").hide();
//				    			$("#"+elId).find(".visible img").attr("src", "");
//				    			$("#"+elId).find("input[name='imageDesc']").val("");
//				    			$("#"+elId).find("input[name='"+inputName+"']").val("");
//
//								//실테이블 파일필드 상태 업데이트
//								window[returnFuc]();
//							}
//						},
//						error: function(XMLHttpRequest,textStatus,errorThrown) {
//							alert("[Error] 삭제 실패: "+textStatus);
//						}
//					});
//				}
//			});
//			/*
//
//			*/
//		},
//	},
	isEmpty: function(value) {
		return value === undefined || value === null || value === '';
	},
	isNotEmpty: function(value) {
		return !this.isEmpty(value);
	},
	isObject: function(value) {
		var type = typeof value;
		return value !== null && (type === 'object' || type === 'function');
	},
	isNotObject: function () {
		return !this.isObject;
	},
  isEmptyObject: function(obj) {
    try {
			if (this.isObject(obj)) {
				return Object.values(obj).every(function(value) {
					return this.commonUtil.isEmpty(value);
				});
			}
      console.error('is not object');
    } catch (e) {
      console.error('is not object');
    }
  },
  isNotEmptyObject: function(obj) {
    return !this.isEmptyObject(obj);
  },
	// 속성으로 객체 분류하기
	groupBy: function groupBy(objectArray, property) {
		return objectArray.reduce(function(acc, obj) {
			var key = obj[property];
			if (!acc[key]) {
				acc[key] = [];
			}
			acc[key].push(obj);
			return acc;
		}, {});
	}
};
