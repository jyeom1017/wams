
angular.module('app.login' ).controller('LoginController', LoginController);

function LoginController($scope, $state, $stateParams, $rootScope, ConfigService,mainDataService,$timeout) {
	console.log('=====login Controller=====2');	
	var main = this;
	$scope.logintimer = 0;
	  // $rootScope.$broadcast('loginSuccess', { 'res': false, 'loginNm': '' });
		//alert('test');
	  $scope.clickLogin = function () {
		  return;
	    
/*	    var sendParam = {};
	    sendParam.id = $scope.inputId;
	    sendParam.pw = $scope.inputPw;*/
		  
	    var sendParam = $('#loginForm').serialize();
	    
	    mainDataService.login(sendParam).success(function (obj) {
	    
	    	  if(obj.response.res_code == 200){
	    		  console.log('login success');
	    		  sessionStorage.clear();
		    	  sessionStorage.setItem('loginId', obj.response.data.user_id);
		          sessionStorage.setItem('loginNm', obj.response.data.user_name);
		          sessionStorage.setItem('groupNm', obj.response.data.group_name);
		          sessionStorage.setItem('groupCd', obj.response.data.group_code);
		          sessionStorage.setItem('lastMenu', obj.response.data.last_menu);
		          
		          $rootScope.$broadcast('loginSuccess', { 'res': true, 'loginNm': obj.response.data.user_name, 'loginId': obj.response.data.user_id });
		          $scope.inputId = '';
		          $scope.inputPw = '';

		          $state.go('010601');	    		  
	    	  }else{
	    		  alertify.alert('로그인이 실패하였습니다.\n아이디 또는 비밀번호를 확인하여 주세요.');
	    		  console.log('login failure');
	    	  }

	      });
	      
	  }

	  
	  $timeout(function(){
	  mainDataService.getUserInfo({}).success(function(obj){
		  //alert(obj.loginId);
		  if(typeof obj.loginId == 'undefined' ){
			  return;
		  }
          if($scope.logintimer>0)
              clearTimeout($scope.logintimer);
          sessionStorage.clear();          
		  sessionStorage.setItem('loginId', obj.loginId);
          sessionStorage.setItem('loginNm', obj.loginNm);
          sessionStorage.setItem('groupNm', obj.groupNm);
          sessionStorage.setItem('groupCd', obj.groupCd);
          sessionStorage.setItem('lastMenu', obj.lastMenu);
          // $rootScope.$broadcast('loginSuccess', { 'res': true, 'loginNm': obj.loginNm });
          $rootScope.$broadcast('loginSuccess', { 'res': true, 'loginNm': obj.loginNm, 'loginId': obj.loginId }); // 0818 수정
          $scope.inputId = '';
          $scope.inputPw = '';

	  });
	  },100);
	  
	  if(sessionStorage.getItem('loginId')==null){
		  $scope.logintimer = setTimeout(function(){
			  top.location.href="./login";
		  },3000);		  
	  }	  
	  
}

