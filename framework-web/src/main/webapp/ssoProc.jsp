<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>로그인</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="../assets/js/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/assets/css/fontawesome_all.css">
	<link rel="stylesheet" href="/assets/XEIcon/xeicon.min.css">
	<link rel="stylesheet" href="/assets/css/common.css">
	<link rel="stylesheet" href="/assets/css/main.css">
	<script src="/assets/js/jquery/jquery-1.12.4.min.js"></script>
	<script src="/assets/js/jquery/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
</head>
<% 
	String user_id = (String)session.getAttribute("user_id");
	//String user_id = "signal2";
	//session.setAttribute("user_id",user_id);
	if(user_id == null || user_id.equals("") ){
%>
	<p>아이디가 없습니다.</p>
<%		
	}else{
%>
 <BODY>
 loading....
 <div style="display:none;">
  <form id="loginForm" method='GET' action="/ssoLogin"></form>
 </div>
<script>
// ajax 통신
$.ajax({
    type : "POST",           
    url : "/basic/employee/CreateUser",     
    data : {},           
    success : function(res){ 
        console.log(res);
    	document.getElementById('loginForm').submit();
    },
    error : function(XMLHttpRequest, textStatus, errorThrown){ // 비동기 통신이 실패할경우 error 콜백으로 들어옵니다.
        //alert("통신 실패.")
    }
});
	
</script>  
 </BODY>
 <% } %>
</HTML>
