﻿angular.module('app').service('mainDataService', mainDataService);

function mainDataService($http, $upload, Version, $rootScope) {
	// console.log(Version);
	var service = {
		
		setLogout : setLogout,
		/*초기위치정보 설정 */
		setPostgisGeoInfo : setPostgisGeoInfo,
		
		/*단차분석 데이터*/
		getGoogleGeoElevationApi : getGoogleGeoElevationApi,
		getElevationDifferenceAnalysisInfo : getElevationDifferenceAnalysisInfo,
		saveTempPipeList : saveTempPipeList,
		getTempPipeList : getTempPipeList,
		
		/* 사용자조회 로그 기록*/
		updateLogUser : updateLogUser,
			
		/*oneData : oneDataFunc,*/	

		/*데시보드차트 */
		getChartData : getChartData,
		
		/*연동정보*/
		getRelayInfo : getRelayInfo,
		getRelayInfoListExcel : getRelayInfoListExcel,
		
		initLoginFailCnt :initLoginFailCnt, 
		/*비용관리*/
		saveCostList : saveCostList,
		updateCostList : updateCostList ,
		deleteCost : deleteCost ,
		updateCost : updateCost ,
		getCostListExcel : getCostListExcel,
		
		//시스템로그 다운로드
		getLogExcel : getLogExcel,
		//재평가 자산그릅 
		getReEvalAssetGroup : getReEvalAssetGroup,

		//직접,간잡 평가 자산그룹
		getEstAssetGroup : getEstAssetGroup, 
		/*
		 * 간접 상태평가
		*/
		getIndirEstList : getIndirEstList,
		getIndirEstHistoryList : getIndirEstHistoryList,
		insertIndirEst : insertIndirEst,
		updateIndirEst : updateIndirEst,
		deleteIndirEst : deleteIndirEst,
		getIndirEstAsset : getIndirEstAsset,
		getIndirEstAssetResult : getIndirEstAssetResult,
		saveIndirEstAsset : saveIndirEstAsset,
		insertEstAsset : insertEstAsset,
		deleteEstAsset : deleteEstAsset,
		saveIndirEstAssetResult : saveIndirEstAssetResult,
		getGISInformationAndConditionAssessmentResults: getGISInformationAndConditionAssessmentResults,
		getAssetNonePage: getAssetNonePage,
		/*간접평가항목관리*/
		getIndirEstFactorList : getIndirEstFactorList,
		getIndirEstFactorDetail : getIndirEstFactorDetail,
		insertIndirEstFactorList : insertIndirEstFactorList,
		updateIndirEstFactorList : updateIndirEstFactorList,
		/*개량방안관리 등급*/
		getIndirEstGradeList : getIndirEstGradeList,
		getIndirEstGradeDetail : getIndirEstGradeDetail,
		insertIndirEstGrade : insertIndirEstGrade,
		updateIndirEstGrade : updateIndirEstGrade,
		/*
		 * 직접 상태평가
		*/
		getDirEstList : getDirEstList,
		getDirEstHistoryList : getDirEstHistoryList,
		insertDirEst : insertDirEst,
		updateDirEst : updateDirEst,
		deleteDirEst : deleteDirEst,
		getDirEstAssetResult : getDirEstAssetResult,
		getDirEstGradeList : getDirEstGradeList,
		
		getDirEstFactorList : getDirEstFactorList,
		getDirEstFactorDetail : getDirEstFactorDetail,
		insertDirEstFactorList : insertDirEstFactorList,
		updateDirEstFactorList : updateDirEstFactorList,
		
		/*개량방안 관리등급*/
		insertDirEstGrade : insertDirEstGrade,	
		updateDirEstGrade : updateDirEstGrade,
		
		saveDirEstAsset : saveDirEstAsset,
		saveDirEstAssetResult : saveDirEstAssetResult,
		saveDirEstAssetResult2 : saveDirEstAssetResult2,
		
/*		getDirEstAsset : getDirEstAsset,
		saveDirEstAsset : saveDirEstAsset,
		insertDirEstAsset : insertDirEstAsset,
		deleteDirEstAsset : deleteDirEstAsset,
		saveDirEstAssetResult : saveDirEstAssetResult,
		간접평가항목관리
		getDirEstFactorList : getDirEstFactorList,
		getDirEstFactorDetail : getDirEstFactorDetail,
		insertDirEstFactorList : insertDirEstFactorList,
		updateDirEstFactorList : updateDirEstFactorList,
		개량방안관리 등급
		getDirEstGradeDetail : getDirEstGradeDetail,	
		/*
		* 위험도 평가
		*/
		updateRiskAsset : updateRiskAsset,
		insertRisk : insertRisk,
		updateRisk : updateRisk,
		deleteRisk : deleteRisk,
		getFactorRiskList : getFactorRiskList,
		insertRiskFactorList : insertRiskFactorList,
		updateRiskFactorList : updateRiskFactorList,
		getFactorRiskDetail : getFactorRiskDetail,
		
		getGradeRiskList : getGradeRiskList,
		insertRiskGradeList : insertRiskGradeList,
		updateRiskGradeList : updateRiskGradeList,		
		getRiskGradeDetail : getRiskGradeDetail,
		
		getRiskAssetGroup : getRiskAssetGroup, 
		saveRiskAsset : saveRiskAsset,
		saveRiskAssetResult : saveRiskAssetResult,
		getRiskResult : getRiskResult,
		deleteRiskAsset : deleteRiskAsset,
		
		/*
		 * ASSET GIS INFO
		 */
		getAssetGisInfo : getAssetGisInfo,
		getAssetFromGisInfo : getAssetFromGisInfo,
		insertAssetGis : insertAssetGis, 
		/*
		 * ASSET LEVEL PATH  
		 */
		getAssetLevelList : getAssetLevelList,
		getAssetPathInfo : getAssetPathInfo,
		getAssetLevelPath : getAssetLevelPath,
		getAssetItemInfo : getAssetItemInfo,
		getFindAssetList : getFindAssetList, 
		getAssetPic		: getAssetPic,
		updateAssetPic  : updateAssetPic ,
		deleteAssetPic  : deleteAssetPic ,
		getAssetHistory : getAssetHistory,
		insertAssetItemInfo : insertAssetItemInfo,
		updateAssetItemInfo : updateAssetItemInfo,
		deleteAssetItemInfo2 : deleteAssetItemInfo2,
		deleteAssetItemInfo : deleteAssetItemInfo,
		deleteAsset : deleteAsset,
		// 게시판 게시물삭제
		deleteBbsItem : deleteBbsItem,
		// 게시판 임시게시물삭제
		deleteBbsTempItem : deleteBbsTempItem,
		// 게시판 임시게시물에 업로드 된 파일 삭제 (공지사항, 자료실)
		deleteBbsFile : deleteBbsFile,
		// 게시판 임시게시물 등록 (공지사항, 자료실)
		insertBbsTempItem : insertBbsTempItem,
		// 게시판 게시물 등록, 수정
		insertBbsItem : insertBbsItem,
		// 게시판 게시물 수정
		// updateBbsItem : updateBbsItem,
		// 게시판 파일 등록 (공지사항, 자료실)
		saveBbsFile : saveBbsFile,
		// 게시판 파일 목록 불러오기 (공지사항, 자료실)
		getBbsFileList : getBbsFileList,
		// 게시판 파일 개별 삭제 (공지사항, 자료실)
		deleteBbsFileOne : deleteBbsFileOne,
		// 게시판 댓글 개수 (질의/응답)
		replyCount : replyCount,
		// 게시판 댓글 목록 불러오기 (질의/응답)
		getReplyList : getReplyList,
		// 게시판 조회수 증가
		updateReadCount: updateReadCount,
		// 로그인된 사용자정보
		getUserInfo : getUserInfo,
		//패스워드 채크
		checkPassword : checkPassword,
		//패스워드 저장 -- 기존패스워드가 맞아야 함,관리자는 기존 패스워드 입력 없이 저장 가능  
		saveNewPassword : saveNewPassword, 
		// 로그인처리
		login : login,
		// 메뉴리스트
		getMenuList : getMenuList,
		// 사용자 목록
		getEmployeeList : getEmployeeList,
		// 시설물정보 목록
		getFacilityList : getFacilityList,
		// 시설물정보 불러오기
		getFacilityInfo : getFacilityInfo,
		// 시설물정보저장
		saveFacilityInfo : saveFacilityInfo,
		// 행정동코드
		getHJDCode : getHJDCode,
		// 시설물코드(표준코드)
		getFacCodeList : getFacCodeList,
		// 시설물 사진대지 목록
		getPictureList : getPictureList,
		// 시설물 사진대지 등록
		saveFacPicture : saveFacPicture,
		// 시설물 사진대지 삭제
		deleteFacPicture : deleteFacPicture,
		// 시설물 이력 사진
		getHisPictureList : getHisPictureList,
		// 시설물 이력 사진 등록
		saveFacHisPicture : saveFacHisPicture,
		// 시설물 이력 사진 삭제
		deleteFacHisPicture : deleteFacHisPicture,
		// 시설물 펌프목록
		getFacPumpList : getFacPumpList,
		// 시설물 파일목록
		getFacFileList : getFacFileList,
		// 시설물 펌프목록 저장
		saveFacPumpList : saveFacPumpList,
		// 시설물 파일 저장
		saveFacFile : saveFacFile,
		// 시설물 파일삭제
		deleteFacFile : deleteFacFile,
		// 시설물 이력 목록
		getFacHistoryList : getFacHistoryList,
		// 시설물 이력 추가
		saveFacHistory : saveFacHistory,
		// 시설물 이력 삭제
		deleteFacHistory : deleteFacHistory,
		// 파일 목록
		getFileList : getFileList,
		// 대용량파일업로드
		largeFileUpload : largeFileUpload,
		// 파일업로드
		fileUpload : fileUpload,
		// cctv파일업로드
		cctvfileUpload : cctvfileUpload,
		// 파일삭제
		fileDelete : fileDelete,
		// 공통코드
		getCommonCodeInfo : getCommonCodeInfo,
		// 공통그룹코드 
		getCommonCodeGroup : getCommonCodeGroup,		
		// 공통코드
		getCommonCodeList : getCommonCodeList,
		// 공통코드 추가
		insertCommonCodeList : insertCommonCodeList,
		// 공통코드 수정
		updateCommonCodeList : updateCommonCodeList,
		// 공통코드 삭제
		deleteCommonCodeList : deleteCommonCodeList,
		// 사용자 추가
		addEmployee : addEmployee,
		// 사용자 수정
		updateEmployee : updateEmployee,
		// 사용자 마지막 메뉴 수정
		updateEmpLastMenu : updateEmpLastMenu,
		// 사용자 삭제
		deleteEmployee : deleteEmployee,
		deleteEmployeePhoto: deleteEmployeePhoto,
		// 아이디 중복채크
		checkUserId : checkUserId,
		// 메뉴목록-메뉴권한관리
		getCommonMenuList : getCommonMenuList,
		// 메뉴추가
		insertMenu : insertMenu,
		// 메뉴수정
		updateMenu : updateMenu,
		// 메뉴삭제
		deleteMenu : deleteMenu,
		// 메뉴 - 메뉴정렬 최댓값
		getMaxSort : getMaxSort,
		// 메뉴권한 저장
		saveAuthMenuList : saveAuthMenuList,
		// 메뉴권한 조회
		getAuthMenuList : getAuthMenuList,
		// 메뉴 권한그룹 목록
		getAuthGroupList : getAuthGroupList,
		// 메뉴 권한그룹 목록
		getNextGroupCode : getNextGroupCode,		
		// 메뉴 권한그룹 추가
		insertAuthGroup : insertAuthGroup,
		// 메뉴 권한그룹 수정
		updateAuthGroup : updateAuthGroup,
		// 메뉴 권한그룹 삭제
		deleteAuthGroup : deleteAuthGroup,
		// 공통CRUD
		cmCRUD : cmCRUD,
		// 연간 계획서 목록
		getYearPlanList : getYearPlanList,
		// 연간 계획서 등록
		saveYearPlan : saveYearPlan,
		// 연간 계획서 삭제
		deleteYearPlan : deleteYearPlan,
		// 시설물 점검 결과 목록
		getFcResultList : getFcResultList,
		/*
		 * //시설물 점검 결과2 목록 getFcResult2List:getFcResult2List,
		 */
		// 시설물 점검 결과 장비목록
		getFcResultEquipmentList : getFcResultEquipmentList,
		// 시설물 점검 결과 시설목록
		getFcResultFacilityList : getFcResultFacilityList,
		// 시설물 점검 결과 파일 목록
		getFcResultFileList : getFcResultFileList,
		// 시설물 점검 결과 사진목록
		getFcResultPicList : getFcResultPicList,
		// 시설물 점검 결과 정보 삭제
		deleteFcResultMaster : deleteFcResultMaster,
		// 시설물 점검 결과 정보 저장
		saveFcResultMaster : saveFcResultMaster,
		// 시설물 점검 결과 파일 추가
		saveFcResultFile : saveFcResultFile,
		// 시설물 점검 결과 파일 삭제
		deleteFcResultFile : deleteFcResultFile,
		// 시설물장비 선택 팝업
		getEquipMasterPopList : getEquipMasterPopList,
		// 시설물장비 목록 저장
		saveFcResultEquipList : saveFcResultEquipList,
		// 시설물장비 목록 삭제
		deleteFcResultEquip : deleteFcResultEquip,
		// 시설물장비 목록 모두 삭제
		deleteFcResultEquipAll : deleteFcResultEquipAll,
		// 대상 시설물 삭제
		deleteFcResultFac : deleteFcResultFac,
		// 시설물점검 사진대지삭제
		deleteFcResultPic : deleteFcResultPic,
		// 시설물점검 사진대장 사잔저장
		saveFcResultPic : saveFcResultPic,
		// 시설물점검 조사결과 상세팝업
		getResultDetailInfo : getResultDetailInfo,
		// 조사결과 등록
		saveResultDetailInfo : saveResultDetailInfo,
		// 시설물점검 조사결과 시설물 추가
		saveFcResultFac : saveFcResultFac,
		// 시설물점검 조사결과 시설물 삭제
		deleteFcResultDetailInfo : deleteFcResultDetailInfo,
		// 엑셀 양식 다운로드
		getExcelDownload : getExcelDownload,
		// 사용자메뉴추가삭제
		setUserMenu : setUserMenu,
		// GIS정보 불러오기
		getFcResultEyeGisInfo : getFcResultEyeGisInfo,
		// 시설물점검 비디오 목록
		getFcResultVideoList : getFcResultVideoList,
		// 시설물점검 비디오 등록
		insertFcResultVideo : insertFcResultVideo,
		// 시설물점검 비디오 삭제
		deleteFcResultVideo : deleteFcResultVideo,
		// 민원 및 사고 등록 master insertFcResult2Master.json
		insertFcResult2Master : insertFcResult2Master,
		// 민원 및 사고 조치결과 등록 sub updateFcResult2Sub.json
		updateFcResult2Sub : updateFcResult2Sub,
		// 민원 및 사고 상세조회
		getFcResult2Info : getFcResult2Info,
		// 관로진단 상세조회
		getFcResultPipeInfo : getFcResultPipeInfo,
		// 관로진단 대상시설물 삭제
		deleteCmFac : deleteCmFac,
		// 교육현황 관리 상세조회
		getFcResultEduInfo : getFcResultEduInfo,
		// 교육 참석자 등록
		insertFcResultEduEmp : insertFcResultEduEmp,
		// 교육 참석자 조회
		getFcResultEduEmpList : getFcResultEduEmpList,
		// 교육 참석자 삭제
		deleteFcResultEduEmp : deleteFcResultEduEmp,
		// 도서 및 성과품 관리 상세조회
		getFcResultBookInfo : getFcResultBookInfo,
		// 로그 리스트 가져오기
		getSystemLogList : getSystemLogList,
		// 하수처리시설 가져오기
		getsewageList : getsewageList,
		// 세션만료여부 확인
		getHasSession : getHasSession,
		// 자산 전체에 필요한 데이터(시설/관망의 각 최대 레벨(미완성)과 유무 기준)
		getAssetCriteria : getAssetCriteria,
		// 레벨 코드 리스트 가져오기
		getLevelCodeList : getLevelCodeList,
		// 레벨 코드 삭제
		deleteLevelCode : deleteLevelCode,
		// 레벨 코드 수정
		updateLevelCode : updateLevelCode,
		// 신규 레벨 코드 추가
		insertLevelCode : insertLevelCode,
		// 자산 정보 경로 리스트 가져오기
		getItemListExcel : getItemListExcel,		
		// 자산 정보 경로 리스트 가져오기
		getItemList : getItemList,
		// 자산 정보 레벨 리스트 가져오기
		getLevelNumList : getLevelNumList,
		// 자산 정보 경로 삭제(사용여부 N으로 변경)
		deleteAssetPath : deleteAssetPath,
		// 자산 정보 표준 항목 리스트 가져오기
		getInfoItemList : getInfoItemList,
		// 전체 자산 레벨 리스트 가져오기
		getAllAssetLevelList : getAllAssetLevelList,
		// 전체 표준/상태 항목 가져오기
		getAllInfoItemList : getAllInfoItemList,
		// 자산 최대 레벨 리스트 가져오기
		getAssetMaxLevelList : getAssetMaxLevelList,
		// 자산 최대 레벨 수정
		updateMaxLevelList : updateMaxLevelList,
		// 자산 최대 레벨 삭제
		deleteMaxLevel : deleteMaxLevel,
		// 자산 경로 신규 생성
		saveAssets : saveAssets,
		// 자산 경로 업데이트
		updateAssets : updateAssets,
		// 자산 사용자 정의 항목 가져오기
		getCustomItemList : getCustomItemList,
		// 레벨 코드 중복 여부 확인
		isAssetClassCdExists : isAssetClassCdExists,
		// 자산 경로 중복 여부 확인
		isAssetPathExists : isAssetPathExists,
		// 자산 경로 전체 개수 가져오기
		getLevelPathTotalCount : getLevelPathTotalCount,
		// 레벨 코드 전체 개수 가져오기
		getLevelCodeTotalCount : getLevelCodeTotalCount,
		// 레벨 코드 삭제 전 사용 중인 자산 코드인지 체크
		isLevelCodeUsed : isLevelCodeUsed,
		// 자산 경로 삭제 전 사용 중인 자산 경로인지 체크
		isAssetPathUsed : isAssetPathUsed,
		// 자산 기준 정보
		updateAssetBase : updateAssetBase,
		getAssetBaseInfo :getAssetBaseInfo, 
		
		//공통 
		CommunicationRelay : CommunicationRelay,
		
		//상수운영관리 블록코드 목록
		getBlockCodeList : getBlockCodeList,

		// 상수운영관리 요금정보 - 블록그룹별
		getConsumerGroup : getConsumerGroup,
		// 상수운영관리 요금정보 - 목록
		getConsumerList : getConsumerList,		
		// 상수운영관리 블록 목록 -계측기
		getBlockList : getBlockList,
		// 상수운영관리 유량 및 수압 데이터 (월 단위)
		getData_flux_wp_month : getData_flux_wp_month,
		// 상수운영관리 유량 및 수압 데이터 (일 단위)
		getData_flux_wp_month : getData_flux_wp_month,
		// 상수운영관리 유량 및 수압 데이터 (시간 단위)
		getData_flux_wp_hour : getData_flux_wp_hour,
		// 상수운영관리 유량 및 수압 데이터 (분 단위)
		getData_flux_wp_min : getData_flux_wp_min,
		// 상수운영관리 수질 데이터
		getData_waterQuality : getData_waterQuality,
		// 상수운영관리 수질 데이터 (시간 단위)
		getData_waterQuality_hour : getData_waterQuality_hour,
		// 상수운영관리 수질 데이터 (분 단위)
		getData_waterQuality_min : getData_waterQuality_min,
		// 민원 임시글 등록
		insertMinwonTemp : insertMinwonTemp,
		// 민원 임시글 삭제
		deleteMinwonTemp : deleteMinwonTemp,
		// 민원 저장 (등록, 수정)
		saveMinwon : saveMinwon,
		// 민원 삭제
		deleteMinwon : deleteMinwon,
		// 민원 파일 업로드
		insertMinwonFile : insertMinwonFile,
		// 민원 파일 불러오기
		getMinwonFileList : getMinwonFileList,
		// 민원 파일 삭제
		deleteMinwonFileOne : deleteMinwonFileOne,
		// 민원 자산 등록
		insertMinwonAsset : insertMinwonAsset,
		// 민원 자산 사진 불러오기
		getMinwonAssetPic : getMinwonAssetPic,
		
		//통계 
		getStatAsset : getStatAsset,
		getStatMinwon : getStatMinwon,
		getStatRenovation : getStatRenovation,
		getStatInspect : getStatInspect,
		
		getStatIndirEst : getStatIndirEst,
		getStatRemainLife : getStatRemainLife,
		getStatRisk : getStatRisk,
		getStatLoS : getStatLoS,
		getStatLcc : getStatLcc,
		getStatOIP : getStatOIP,
		getStatFinance : getStatFinance,

		// 차입/상환/이자 정보 
		getLoanInfo : getLoanInfo, 

		// 재정계획 수립 > 물가상승률
		getFinacePriceRaiseRatio : getFinacePriceRaiseRatio,
		// 재정계획 수립 > 재정계획 목록
		getFinancialPlanList: getFinancialPlanList,
		// 재정계획 수립 > 재정수지분석 관리
		getFinancialPlanDetailList: getFinancialPlanDetailList,
		// 재정계획 수립 > 재무재표(BS) 관리
		getFinancialStatementsList: getFinancialStatementsList,
		setFinancialStatement: setFinancialStatement,
		// 재정계획 수립 > 손익계산서(PL) 관리
		getIncomeStatement: getIncomeStatement,
		setIncomeStatement: setIncomeStatement,
		// 재정계획 수립 > 현금흐름표(CF) 관리
		getCashFlowStatement: getCashFlowStatement,
		setCashFlowStatement: setCashFlowStatement,
		// 재정계획 수립 > 기타 비용 관리
		getOtherCostManagement: getOtherCostManagement,
		setOtherCostManagement: setOtherCostManagement,
		// 재정계획 수립 > 재정계획 삭제
		deleteFinancialPlan: deleteFinancialPlan,
		// 재정계획 수립 > 재정수지 분석 (팝업, TAB-재정수지총괄)
		insertTempFinancialPlan: insertTempFinancialPlan,
		// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 재정수지 분석 (팝업, TAB-운영관리비)
		getOperationAndManagementExpensesPopupList: getOperationAndManagementExpensesPopupList,
		// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 재정수지 분석 (팝업, TAB-감가상각비)
		getDepreciationPopupList: getDepreciationPopupList,
		setFinancialPlanDetail: setFinancialPlanDetail,
		
		setFinancialBS: setFinancialBS,
		setFinancialPL: setFinancialPL,
		setFinancialCF: setFinancialCF,
		setFinancialETC: setFinancialETC,
		
		getFinancialData : getFinancialData,
		
		updateFinancialPlan: updateFinancialPlan,
		// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가 설정 (팝업)
		getUnitPriceSettingInfo: getUnitPriceSettingInfo,
		// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가/생산량/구입량 설정 (팝업)
		getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo: getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo,
		// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 전기 말 이자율 설정 (팝업)
		getInterestRateEndOfLastYearInfo: getInterestRateEndOfLastYearInfo,
		// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가, 요금단가/생산량/구입량 설정 저장
		setFinancialAnalysisChargeUnitPrice: setFinancialAnalysisChargeUnitPrice,
		// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 전기 말 이자율 설정 (팝업) 저장
		updateInterestRateEndOfLastYearInfo: updateInterestRateEndOfLastYearInfo,
		// 자산관리 계획수립 목록
		getAssetManagementPlanList: getAssetManagementPlanList,
		// 자산관리 계획 수립
		getAssetManagementPlanDetailList: getAssetManagementPlanDetailList,
		// 
		updateAssetManagementPlanDetail: updateAssetManagementPlanDetail,
		// 자산관리 계획 삭제
		deleteAssetManagementPlan: deleteAssetManagementPlan,
		// 자산관리 계획 신규
		insertAssetManagementPlan: insertAssetManagementPlan,
		// 자산관리 계획 저장
		updateAssetManagementPlan: updateAssetManagementPlan,

		getAssetInfoWithGisInfo : getAssetInfoWithGisInfo,

		updateFeatureCodeWithLastData: updateFeatureCodeWithLastData,

		getAssetInfoWithLayer: getAssetInfoWithLayer,

		addAssetGis: addAssetGis,

		getAssetInfoWithGisList : getAssetInfoWithGisList,

		getGISInfoByAssetPath: getGISInfoByAssetPath,

		// 진단 구역 관리 (팝업, TAB-진단 구역) 진단 구역 삭제
		deleteDiagnosticArea: deleteDiagnosticArea,
		getDiagnosticAreaDetailEditList: getDiagnosticAreaDetailEditList,
		// 진단 구역 등록/수정 (팝업) 저장
		saveDiagnosticAreaDetail: saveDiagnosticAreaDetail,
		// 진단 구역 등록/수정 (팝업) 진단 구역 자산 삭제
		deleteDiagnosticAreaAsset: deleteDiagnosticAreaAsset,
		// 진단 구역 관리 (팝업, TAB-상태진단 그룹) 상태진단 그룹 삭제
		deleteStateDiagnosisGroup: deleteStateDiagnosisGroup,
		// 상태진단 그룹 저장
		saveStateDiagnosisGroup: saveStateDiagnosisGroup,
		
		addComCodeListYr2 : addComCodeListYr2,
		
		addLoanItemYear : addLoanItemYear,

		//재정수주분석 신규 초기화 - 항목별
		initFinancialPlanDetailList : initFinancialPlanDetailList,

		getStateDiagnosticGroupDetailPopupInfo: getStateDiagnosticGroupDetailPopupInfo,

		getAssetsByAssetPath: getAssetsByAssetPath,

		getAssetsByGISProperty: getAssetsByGISProperty,

		getStateDiagnosisGroupDetailEditList: getStateDiagnosisGroupDetailEditList,

		getAssetByAssetCode: getAssetByAssetCode,
		
		getREPAIR_ASSET  : getREPAIR_ASSET , 
		getINSPECT_ASSET : getINSPECT_ASSET,
		
		deleteTemporaryAsset: deleteTemporaryAsset,
	};

	return service;

	function getRelayInfo (parameter){
		return $http.post('/operation/getRelayInfo.json', parameter,{
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}			
		}).success(function(response) {
			return response;
		}).error(function(error) {
			
		});
		
	}
	
	function getRelayInfoListExcel (parameter){
		return $http.post('/operation/getRelayInfoListExcel.json', parameter,{
		}).success(function(response) {
			return response;
		}).error(function(error) {
			
		});
		
	}
	
	function initLoginFailCnt(parameter){
		var param = "E_ID="+parameter.E_ID;
		return $http.post('/common/initLoginFailCnt.json', param,{
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
				}				
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});				
	}
	
	function getCostListExcel(parameter){
		return $http.post('/operation/getCostListExcel.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function getLogExcel(parameter){
		return $http.post('/api/getSystemLogExcel.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	
	function saveCostList(parameter){
		return $http.post('/operation/saveCostList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateCostList(parameter){
		return $http.post('/operation/updateCostList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function deleteCost(parameter){
		return $http.post('/operation/deleteCost.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateCost(parameter){
		return $http.post('/operation/setCostItem.json', parameter,{
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}}).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	function getDirEstList(parameter){
		return $http.post('/est/getList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getDirEstHistoryList(parameter){
		return $http.post('/est/historyList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function insertDirEst(parameter){
		return $http.post('/est/insert.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateDirEst(parameter){
		return $http.post('/est/update.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function deleteDirEst(parameter){
		return $http.post('/est/delete.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function saveDirEstAsset(parameter){
		return $http.post('/est/saveDirEstAsset.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	

	function insertEstAsset(parameter){
		return $http.post('/est/insertAsset.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function deleteEstAsset(parameter){
		return $http.post('/est/deleteAsset.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function getDirEstAssetResult(parameter){
		return $http.get('/est/assetResult.json?EST_SID='+ parameter.EST_SID+'&ASSET_SID='+parameter.ASSET_SID+'&category='+parameter.category).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function saveDirEstAssetResult(parameter){
		return $http.post('/est/saveDirEstAssetResult.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function saveDirEstAssetResult2(parameter){
		return $http.post('/est/saveDirEstAssetResult2.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	function getDirEstGradeList(parameter){
		return $http.post('/est/getGradeList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function insertDirEstGrade(parameter){
		return $http.post('/est/insertGrade.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateDirEstGrade(parameter){
		return $http.post('/est/updateGrade.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	/*---------------------------------*/
	function getReEvalAssetGroup(parameter){
		return $http.post('/reeval/getListAssetGroup.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function getEstAssetGroup(parameter){
		return $http.post('/est/assetGroup.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	/*---------------------------------*/
	function getIndirEstList(parameter){
		return $http.post('/est/getList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getIndirEstHistoryList(parameter){
		return $http.post('/est/historyList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function insertIndirEst(parameter){
		return $http.post('/est/insert.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateIndirEst(parameter){
		return $http.post('/est/update.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function deleteIndirEst(parameter){
		return $http.post('/est/delete.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function getIndirEstAsset(parameter){
		return $http.post('/est/asset.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function getIndirEstAssetResult(parameter){
		return $http.get('/est/assetResult.json?EST_SID='+ parameter.EST_SID+'&ASSET_SID='+parameter.ASSET_SID+'&category=1').success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function saveIndirEstAsset(parameter){
		return $http.post('/est/saveIndirEstAsset.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}

	function saveIndirEstAssetResult(parameter){
		return $http.post('/est/saveIndirEstAssetResult.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}

	/**
	 * @param {Object}parameter
	 * @param{String} parameter.category
	 * @param{String | number} parameter.est_sid
	 * @returns {*}
	 */
	function getGISInformationAndConditionAssessmentResults(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/est/getGISInformationAndConditionAssessmentResults.json?' + param).success(response => response);
	}

	/**
	 * @param {Object}parameter
	 * @param{String} parameter.category
	 * @param{String | number} parameter.est_sid
	 * @returns {*}
	 */
	function getAssetNonePage(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/est/assetNonePage.json?' + param).success(response => response);
	}
	//----------------------------------------------------------------------------------------------
	function getDirEstFactorList(parameter){
		return $http.post('/est/getFactorList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getDirEstFactorDetail(parameter){
		return $http.post('/est/getFactorInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function insertDirEstFactorList(parameter){
		return $http.post('/est/insertFactor.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateDirEstFactorList(parameter){
		return $http.post('/est/updateFactor.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	//-----------------------------------------------------------------------------------------------------------
	function getIndirEstFactorList(parameter){
		return $http.post('/est/getFactorList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getIndirEstFactorDetail(parameter){
		return $http.post('/est/getFactorInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function insertIndirEstFactorList(parameter){
		return $http.post('/est/insertFactor.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateIndirEstFactorList(parameter){
		return $http.post('/est/updateFactor.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	//-----------------------------------------------------------------------------------------------------------
	function getIndirEstGradeList(parameter){
		return $http.post('/est/getGradeList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getIndirEstGradeDetail(parameter){
		return $http.post('/est/getGradeInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getDirEstGradeDetail(parameter){
		return $http.post('/est/getGradeInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	function insertIndirEstGrade(parameter){
		return $http.post('/est/insertGrade.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateIndirEstGrade(parameter){
		return $http.post('/est/updateGrade.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	//-----------------------------------------------------------------------------------------------------------
	
	function insertRisk(parameter){
		return $http.post('/risk/insert.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateRisk(parameter){
		return $http.post('/risk/update.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	function deleteRisk(parameter){
		return $http.post('/risk/delete.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getFactorRiskList(parameter){
		return $http.post('/risk/getFactorList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function insertRiskFactorList(parameter){
		return $http.post('/risk/insertFactor.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateRiskFactorList(parameter){
		return $http.post('/risk/updateFactor.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getFactorRiskDetail(parameter){
		return $http.post('/risk/getFactorInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function getGradeRiskList(parameter){
		return $http.post('/risk/getGradeList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function insertRiskGradeList(parameter){
		return $http.post('/risk/insertGrade.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateRiskGradeList(parameter){
		return $http.post('/risk/updateGrade.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getRiskGradeDetail(parameter){
		return $http.post('/risk/getGradeInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function getRiskAssetGroup(parameter){
		return $http.post('/risk/assetGroup.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function saveRiskAsset(parameter){
		return $http.post('/risk/saveAsset.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function saveRiskAssetResult(parameter){
		return $http.post('/risk/saveAssetResult.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getRiskResult(parameter){
		return $http.post('/risk/getRiskResult.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function deleteRiskAsset(parameter){
		return $http.post('/risk/deleteAsset.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateRiskAsset(parameter){
		return $http.post('/risk/updateAsset.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	//-------------------------------------------------------------------------------------------------------------
	function getAssetItemInfo(parameter){
		return $http.post('/asset/getSelectAssetItemInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function getFindAssetList(parameter){
		return $http.post('/asset/getFindAssetList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	 
	function getAssetPic(parameter){
		return $http.get('/asset/getSelectAssetPic.json?AssetSid='+parameter.ASSET_SID).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function updateAssetPic(parameter){
		return $http.post('/asset/updateAssetPic.json',parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function deleteAssetPic(parameter){
		return $http.post('/asset/deleteAssetPic.json',parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	function getAssetHistory(parameter){
		return $http.get('/asset/getSelectAssetHistory.json?AssetSid='+parameter.ASSET_SID).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	
	function insertAssetItemInfo(parameter){
		return $http.post('/asset/insertAssetItemInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}

	function updateAssetItemInfo(parameter){
		return $http.post('/asset/AssetItemInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	

	//자산 불용
	function deleteAssetItemInfo2(parameter){
		return $http.post('/asset/deleteAssetItemInfo2.json?ASSET_SID='+parameter.ASSET_SID, parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	//자산 폐기
	function deleteAssetItemInfo(parameter){
		return $http.post('/asset/deleteAssetItemInfo.json?ASSET_SID='+parameter.ASSET_SID, parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}

	//자산 삭제 - 완전 삭제 
	function deleteAsset(parameter){
		return $http.post('/asset/deleteAssetItem.json?ASSET_SID='+parameter.ASSET_SID, parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getAssetPathInfo(parameter){
		return $http.get('/asset/getAssetPathInfo.json?ASSET_PATH_SID='+parameter.ASSET_PATH_SID).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	function getAssetLevelPath(parameter){
		return $http.get('/asset/getSelectAssetLevelPathList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	
	function getAssetLevelList(parameter){
		return $http.get('/asset/getAssetLevelList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	
	function updateAssetBase(parameter){
		return $http.post('/asset/updateAssetBaseInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	
	function getAssetBaseInfo(parameter){
		return $http.post('/asset/getAssetBaseInfoList.json', parameter,{
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}			
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	function getAssetGisInfo(parameter){
		var PARAMS = '?ASSET_SID=' + (parameter.ASSET_SID | 0);
		if(parameter.ASSET_PATH_SID > 0){
			PARAMS += '&ASSET_PATH_SID=' + parameter.ASSET_PATH_SID;	
		}
		if(parameter.FTR_IDN>0){
		PARAMS += '&LAYER_CD=' + parameter.LAYER_CD;
		PARAMS += '&FTR_IDN=' + parameter.FTR_IDN;
		}
		return $http.get('/asset/getAssetGisInfo.json'+ PARAMS).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getAssetFromGisInfo(parameter){
		return $http.post('/asset/getAssetFromGisInfo.json',parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}	
	
	function insertAssetGis(parameter){
		console.log(parameter);
		return $http.post('/asset/insertAssetGis.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	/*
	게시판 (bbs) 관련
 	*/
	function deleteBbsItem(parameter){
		return $http.post('/bbs/deleteBbsItem', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function deleteBbsTempItem(parameter){
		return $http.post('/bbs/deleteBbsTempItem', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function deleteBbsFile(parameter){
		return $http.post('/bbs/deleteBbsFile', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function insertBbsTempItem(parameter){
		return $http.post('/bbs/insertBbsTempItem', parameter).success(function(response) {
			return response; //글 insert 후 bbs_sid 값 리턴
		}).error(function(error) {
		});
	}
	function insertBbsItem(parameter){
		return $http.post('/bbs/insertBbsItem', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getBbsFileList(parameter){
		console.log(Version);
		return $http.post('/bbs/getBbsFileList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function saveBbsFile(parameter) {
		return $http.post('/bbs/saveBbsFile', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function deleteBbsFileOne(parameter){
		return $http.post('/bbs/deleteBbsFileOne.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function replyCount(parameter){
		return $http.post('/bbs/replyCount.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getReplyList(parameter){
		return $http.post('/bbs/getReplyList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function updateReadCount(parameter) {
		return $http.post('/bbs/updateReadCount.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}




	///////////////////////////////////////////////

	function getUserInfo(parameter) {
		return $http.post(Version.V1 + '/getUserInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	
	function checkPassword(parameter) {
		var param = 'E_ID=' + (parameter.E_ID || '') + '&chkPassword=' + (parameter.chkPassword || '') ;
		return $http.post('/basic/employee/checkEqualUserPassword.json', param,	{
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
			
		});
	}
	
	function saveNewPassword(parameter) {
		var param = 'E_ID=' + (parameter.E_ID || '') + '&chkPassword=' + (parameter.chkPassword || '') + '&E_PW_01=' + (parameter.E_PW_01 || '') ;
		return $http.post('/basic/employee/updateUserPassword.json', param,{
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}			
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	
	function login(parameter) {
		return $http.post('/j_spring_security_check', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	/***************************************************************************
	 * function getSmartCarList(parameter){ var param = { cid :
	 * 'get_smart_car_list', itemID : parameter.itemID || '' }; return
	 * $http.post(Version.V1 + '', param) .success(function(response){return
	 * response;}) .error(function(error){}); }
	 **************************************************************************/

	// 자산 전체 로직
	function getAssetCriteria(parameter) {
		return $http.get('/asset/getAssetCriteria.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getLevelNumList() {
		return $http.get('/asset/getLevelNumList.json').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	// 자산 - 레벨 코드
	function getLevelCodeList(parameter) {
		return $http.post('/asset/getLevelCodeList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function updateLevelCode(parameter) {
		return $http.post('/asset/updateLevelCode.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function insertLevelCode(parameter) {
		return $http.post('/asset/insertLevelCode.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function deleteLevelCode(parameter) {
		return $http.post('/asset/deleteLevelCode.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function isAssetClassCdExists(parameter) {
		return $http.post('/asset/isAssetClassCdExists.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	// 자산 - 전체 목록
	function getItemListExcel(parameter) {
		/*
		var url_param = "";
		if(parameter.ASSET_NM!='' && parameter.ASSET_NM!=null) url_param = url_param + "&ASSET_NM=" + (parameter.ASSET_NM);
		if(parameter.USE_YN!='' && parameter.USE_YN!=null) url_param = url_param + "&USE_YN=" + (parameter.USE_YN);
		if(parameter.ASSET_ADDR!='' && parameter.ASSET_ADDR!=null) url_param = url_param + "&ASSET_ADDR=" + (parameter.ASSET_ADDR);
		if(parameter.ASSET_CD!='' && parameter.ASSET_CD!=null) url_param = url_param + "&ASSET_CD=" + (parameter.ASSET_CD);
		if(parameter.ASSET_PATH_SID!='' && parameter.ASSET_PATH_SID!=null) url_param = url_param + "&ASSET_PATH_SID=" + parameter.ASSET_PATH_SID;
		if(parameter.ASSET_PATH_CD!='' && parameter.ASSET_PATH_CD!=null) url_param = url_param + "&ASSET_PATH_CD=" + parameter.ASSET_PATH_CD;
		
		return $http.get(encodeURI('/asset/getSelectAssetItemList.json?PAGE_NO='+ parameter.PAGE_NO +'&ROW_CNT='+parameter.ROW_CNT + url_param)).success(function(response) {
			return response;
		}).error(function(error) {
		});*/
		return $http.post('/asset/getSelectAssetItemListExcel.json',parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}	
	// 자산 - 경로
	function getItemList(parameter) {
		return $http.post('/asset/getItemList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getInfoItemList(parameter) {
		return $http.post('/asset/getInfoItemList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getCustomItemList(parameter) {
		return $http.post('/asset/getCustomItemList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getAllAssetLevelList(parameter) {
		return $http.post('/asset/getAllAssetLevelList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	// getAllInfoItemList
	function getAllInfoItemList() {
		return $http.get('/asset/getAllInfoItemList.json').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function deleteAssetPath(parameter) {
		return $http.post('/asset/deleteAssetPath.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getAssetMaxLevelList() {
		return $http.get('/asset/getAssetMaxLevelList.json').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function deleteMaxLevel(parameter) {
		return $http.post('/asset/deleteMaxLevel.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function updateMaxLevelList(parameter) {
		return $http.post('/asset/updateMaxLevelList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function saveAssets(parameter) {
		return $http.post('/asset/saveAssets.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function updateAssets(parameter) {
		return $http.post('/asset/updateAssets.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function isAssetPathExists(parameter) {
		return $http.post('/asset/isAssetPathExists.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getLevelPathTotalCount(){
		return $http.get('/asset/getLevelPathTotalCount.json').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getLevelCodeTotalCount(){
		return $http.get('/asset/getLevelCodeTotalCount.json').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function isLevelCodeUsed(parameter) {
		return $http.post('/asset/isLevelCodeUsed.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function isAssetPathUsed(parameter) {
		return $http.post('/asset/isAssetPathUsed.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	// 메뉴
	function getMenuList() {
		return $http.get(Version.V1 + '/getMenuList.json').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getEmployeeList(parameter) {
		/*
		 * var param = { page : parameter.page || '1', rows : parameter.rows || '10' };
		 */
		return $http.post('/basic/employee/getEmployeeList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getFacilityList(parameter) {
		var param = {
			page : parameter.page || '1',
			rows : parameter.rows || '10'
		};
		return $http.post('/basic/facility/getFacilityList.json', param).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getFacilityInfo(parameter) {
		var param = 'category=' + (parameter.category || '') + '&FTR_CDE=' + (parameter.FTR_CDE || '') + '&FTR_IDN=' + (parameter.FTR_IDN || '');

		return $http.post('/basic/facility/getFacilityInfo.json', param, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getHJDCode(parameter) {
		return $http.get(Version.V1 + '/getHJDCode.json').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getFacCodeList(parameter) {
		console.log('getFacCodeList');
		console.log(parameter);
		return $http.post(Version.V1 + '/getFacCodeList.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getPictureList(parameter) {
		return $http.post(Version.V1 + '/getPictureList.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function saveFacPicture(parameter) {
		return $http.post(Version.V1 + '/saveFacPicture', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function deleteFacPicture(parameter) {
		return $http.post(Version.V1 + '/deleteFacPicture', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getHisPictureList(parameter) {

		return $http.post(Version.V1 + '/getHisPictureList.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function saveFacHisPicture(parameter) {
		return $http.post(Version.V1 + '/saveFacHisPicture', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function deleteFacHisPicture(parameter) {
		return $http.post(Version.V1 + '/deleteFacHisPicture', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getFacFileList(parameter) {

		return $http.post(Version.V1 + '/getFacFileList.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getFacPumpList(parameter) {

		return $http.post(Version.V1 + '/getFacPumpList.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getFacHistoryList(parameter) {

		return $http.post(Version.V1 + '/getFacHistoryList.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function saveFacHistory(parameter) {

		return $http.post(Version.V1 + '/saveFacHistory', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function deleteFacHistory(parameter) {

		return $http.post(Version.V1 + '/deleteFacHistory', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function saveFacPumpList(parameter) {

		return $http.post(Version.V1 + '/saveFacPumpList', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function saveFacFile(parameter) {

		return $http.post(Version.V1 + '/saveFacFile', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function deleteFacFile(parameter) {

		return $http.post(Version.V1 + '/deleteFacFile', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function saveFacilityInfo(parameter) {
		return $http.post('/basic/facility/saveFacilityInfo.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getFileList(parameter) {
		return $http.get(Version.V1 + '/getFileList.json').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function largeFileUpload(fileDesc, folderName, files) {
		return $upload.upload({
			url : Version.V1 + '/largeFileUpload',
			method : 'POST',
			file : files,
			fields : {
				file : files
			},
			// data 속성으로 별도의 데이터를 보냄.
			data : {
				folderName : folderName,
				fileDesc : fileDesc
			},
			fileFormDataName : 'fileData',
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function fileUpload(fileDesc, folderName, files, dirType, bookOpt) {
		bookOpt = bookOpt || '99';
		return $upload.upload({
			url : Version.V1 + '/fileUpload',
			method : 'POST',
			file : files,
			fields : {
				file : files
			},
			// data 속성으로 별도의 데이터를 보냄.
			data : {
				folderName : folderName,
				fileDesc : fileDesc,
				dirType : dirType,
				bookOpt : bookOpt
			},
			fileFormDataName : 'fileData',
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function cctvfileUpload(fileDesc, files) {
		return $upload.upload({
			url : Version.V1 + '/cctvFileUpload',
			method : 'POST',
			file : files,
			fields : {
				file : files
			},
			// data 속성으로 별도의 데이터를 보냄.
			data : {
				fileDesc : fileDesc,
				bookOpt : '04'
			},
			fileFormDataName : 'fileData',
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function fileDelete(f_num) {
		return $http.post(Version.V1 + '/deleteFileInfo', 'f_num=' + f_num, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getCommonCodeGroup(parameter) {
		return $http.get(Version.V1 + '/getCommonCodeGroup.json' ).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

  /**
   * @param {Object} parameter
   * @param {string} parameter.gcode
   * @param {string} [parameter.name]
   * @returns {*}
   */
  function getCommonCodeInfo(parameter) {
/*		var param = new URLSearchParams(parameter).toString();

    return $http.get(Version.V1 + '/getCommonCodeInfo.json?' + param).success(function(response) {
      return response;
    });*/
	  
	  return $http.post('/api/getCommonCodeInfo.json',parameter,{
				/*headers : {
					'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
				}*/
  			}).success(function(response) {
	      return response;
	    });	  
  }
	function getCommonCodeList(parameter) {
		// console.log(parameter);

		parameter.gcode = parameter.gcode || '';
		parameter.name = parameter.name || '';

		var param = '?gcode=' + parameter.gcode + '&name=' + parameter.name;

		if (typeof parameter.usegbn != 'undefined' && parameter.usegbn != '')
			param += '&usegbn=' + parameter.usegbn;
		return $http.get(Version.V1 + '/getCommonCodeList.json' + param).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function updateCommonCodeList(parameter) {

		if (parameter.C_GCODE == null || parameter.C_GCODE == '') {
			alertify.alert('Error','C_GCODE is null',function(){});
			return;
		}
		if (parameter.C_SCODE == null || parameter.C_SCODE == '') {
			alertify.alert('Error','C_SCODE is null',function(){});
			return;
		}
		return $http.post('/api/updateCommonCodeList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function deleteCommonCodeList(parameter) {

		if (parameter.C_GCODE == null || parameter.C_GCODE == '') {
			alertify.alert('Error','C_SCODE is null',function(){});
			return;
		}
		if (parameter.C_SCODE == null || parameter.C_SCODE == '') {
			alertify.alert('Error','C_SCODE is null',function(){});
			return;
		}
		return $http.post('/api/deleteCommonCodeList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function insertCommonCodeList(parameter) {

		if (parameter.C_GCODE == null || parameter.C_GCODE == '') {
			alertify.alert('Error','C_SCODE is null',function(){});
			return;
		}
		if (parameter.C_SCODE == null || parameter.C_SCODE == '') {
			alertify.alert('Error','C_SCODE is null',function(){});
			return;
		}

		return $http.post('/api/insertCommonCodeList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function addEmployee(parameter) {

		if (parameter.E_ID == null || parameter.E_ID == '') {
			alertify.alert('Error','e_id is null',function(){});
			return;
		}

		return $http.post('/basic/employee/insertEmployee.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function updateEmployee(parameter) {

		if (parameter.E_ID == null || parameter.E_ID == '') {
			alertify.alert('Error','e_id is null',function(){});
			return;
		}

		return $http.post('/basic/employee/updateEmployeeBasicInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function updateEmpLastMenu(parameter) {

		if (parameter.E_ID == null || parameter.E_ID == '') {
			alertify.alert('Error','e_id is null',function(){});
			return;
		}

		return $http.post('/basic/employee/updateEmpLastMenu.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function deleteEmployee(parameter) {

		if (parameter.E_ID == null || parameter.E_ID == '') {
			alertify.alert('Error','e_id is null',function(){});
			return;
		}

		return $http.post('/basic/employee/deleteEmployee.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	
	function deleteEmployeePhoto(parameter) {
		/*if (parameter.e_id == null || parameter.e_id == '') {
			alert('e_id is null');
			return;
		}*/

		return $http.post('/basic/employee/deleteEmployeePhoto.json?e_id=' + parameter.E_ID, parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getCommonMenuList(parameter) {
		return $http.get(Version.V1 + '/getCommonMenuList.json').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function insertMenu(parameter) {
		return $http.post(Version.V1 + '/insertMenu.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function updateMenu(parameter) {
		return $http.post(Version.V1 + '/updateMenu.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function deleteMenu(parameter) {
		return $http.post(Version.V1 + '/deleteMenu', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getMaxSort(parameter) {
		return $http.post(Version.V1 + '/getMaxSort', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function saveAuthMenuList(parameter) {
		return $http.post(Version.V1 + '/saveAuthMenuList', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getAuthMenuList(parameter) {
		return $http.post(Version.V1 + '/getAuthMenuList', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getAuthGroupList(parameter) {
		return $http.get(Version.V1 + '/getAuthGroupList').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	
	function getNextGroupCode() {
		return $http.get(Version.V1 + '/getNextGroupCode').success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	
	function insertAuthGroup(parameter) {
		return $http.post(Version.V1 + '/insertAuthGroup', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function updateAuthGroup(parameter) {
		return $http.post(Version.V1 + '/updateAuthGroup', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function deleteAuthGroup(parameter) {
		return $http.post(Version.V1 + '/deleteAuthGroup', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function checkUserId(parameter) {
		return $http.post(Version.V1 + '/checkUserId', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function cmCRUD(parameter) {
		return $http.post(parameter.crudURL, parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function getYearPlanList(parameter) {
		var param = '';
		// param +='page='+ (parameter.page || '1');
		param += 's_py_year=' + (parameter.year || '2019');
		param += '&s_py_jopt=' + (parameter.jopt || '01');
		// param +='&rows=' + (parameter.rows || '6');
		// param +='&sidx=' + (parameter.sidx || '');
		// alert(param);
		return $http.post('/fc/getFcList.json', param, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function saveYearPlan(parameter) {
		var url = '';
		if (parameter.py_num > 0) {
			url = '/fc/updateFc.json';
		} else {
			url = '/fc/insertFc.json';
		}
		return $http.post(url, parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function deleteYearPlan(parameter) {
		return $http.post('/fc/deleteFc.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}

	function getFcResultList(parameter) {
		return $http.post('/fc/getFcResultList.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	/*
	 * function getFcResult2List(parameter){ return
	 * $http.post('/fc/getFcResult2List.json', parameter,{ headers : {
	 * 'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8' } })
	 * .success(function(response){return response;})
	 * .error(function(error){alert('error');}); }
	 */
	function getFcResultEquipmentList(parameter) {
		return $http.post('/fc/getFcResultEquipList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function getFcResultFacilityList(parameter) {
		return $http.post('/fc/getFcResultFacList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function getFcResultFileList(parameter) {
		return $http.post('/fc/getFcResultFileList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function getFcResultPicList(parameter) {
		return $http.post('/fc/getFcResultPicList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function deleteFcResultMaster(parameter) {
		return $http.post('/fc/deleteFcResultMaster.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function saveFcResultMaster(parameter) {
		var url = '';
		if (parameter.jnum > 0) {
			url = '/fc/updateFcResultMaster.json';
		} else {
			url = '/fc/insertFcResultMaster.json';
		}
		return $http.post(url, parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function saveFcResultFile(parameter) {
		return $http.post('/fc/insertFcResultFile.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function deleteFcResultFile(parameter) {
		return $http.post('/fc/deleteFcResultFile.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function getEquipMasterPopList(parameter) {
		return $http.post('/sys/getEquipMasterPopList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function saveFcResultEquipList(parameter) {
		return $http.post('/fc/insertFcResultEquip.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function deleteFcResultEquip(parameter) {
		return $http.post('/fc/deleteFcResultEquip.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function deleteFcResultEquipAll(parameter) {
		return $http.post('/fc/deleteFcResultEquipAll.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function deleteFcResultFac(parameter) {
		console.log('deleteFcResultFac');
		console.log(parameter);
		return $http.post('/fc/deleteFcResultFac.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function deleteFcResultPic(parameter) {
		return $http.post('/fc/deleteFcResultPic.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function saveFcResultPic(parameter) {
		return $http.post('/fc/insertFcResultPic.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function getResultDetailInfo(parameter) {
		console.log('parameter');
		console.log(parameter);
		var url = '';
		switch (parameter.code) {
			case '1':
				url = '/fc/getFcResultFlowInfo.json';
				break;
			case '2':
				url = '/fc/getFcResultWqInfo.json';
				break;
			case '3':
				url = '/fc/getFcResultEyeInfo.json';
				break;
			case '4':
				url = '/fc/getFcResultEyeInfo.json';
				break;
			case '5':
				url = '/fc/getFcResultSmokeInfo.json';
				break;
			case '6':
				url = '/fc/getFcResultSmokeInfo.json';
				break;
			case '11':
				url = '/fc/getFcResultCleanInfo.json';
				break;
			case '21':
				url = '/fc/getFcResultRemodelInfo.json';
				break;
			default:
				alertify.error('getResultDetailInfo error');
				return;
				break;
		}
		return $http.post(url, parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function saveResultDetailInfo(parameter) {
		var url = '';
		switch (parameter.code) {
			case '1':
				url = '/fc/insertFcResultFlowInfo.json';
				break;
			case '2':
				url = '/fc/insertFcResultWqInfo.json';
				break;
			case '3':
				url = '/fc/insertFcResultEyeInfo.json';
				break;
			case '5':
				url = '/fc/insertFcResultSmokeInfo.json';
				break;
			case '11':
				url = '/fc/insertFcResultCleanInfo.json';
				break;
			case '21':
				url = '/fc/insertFcResultRemodelInfo.json';
				break;
			default:
				alertify.error('saveResultDetailInfo error');
				return;
				break;
		}
		return $http.post(url, parameter.param).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function saveFcResultFac(parameter) {
		url = '/fc/insertFcResultFac.json';
		return $http.post(url, parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function deleteFcResultDetailInfo(parameter) {
		var url = '';
		switch (parameter.jopt) {
			case '01':
				url = '/fc/deleteFcResultFlowInfo.json';
				break;
			case '02':
				url = '/fc/deleteFcResultWqInfo.json';
				break;
			case '03':
				url = '/fc/deleteFcResultEyeInfo.json';
				break;
			case '04':
				url = '/fc/deleteFcResultEyeInfo.json';
				break;
			case '05':
				url = '/fc/deleteFcResultSmokeInfo.json';
				break;
			case '11':
				url = '/fc/deleteFcResultCleanInfo.json';
				break;
			case '21':
				url = '/fc/deleteFcResultRemodelInfo.json';
				break;
			default:
				alertify.error('deleteFcResultDetailInfo error');
				return;
				break;
		}
		return $http.post(url, parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function getExcelDownload(parameter) {
		return $http.post('/report/getExcelDownload.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function setUserMenu(parameter) {
		return $http.post('/api/setUserMenu', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	function getFcResultEyeGisInfo(parameter) {
		return $http.post('/fc/getFcResultEyeGisInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	// 시설물점검 비디오 목록
	function getFcResultVideoList(parameter) {
		return $http.post('/fc/getFcResultVideoList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	// 시설물점검 비디오 등록
	function insertFcResultVideo(parameter) {
		return $http.post('/fc/insertFcResultVideo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	// 시설물점검 비디오 삭제
	function deleteFcResultVideo(parameter) {
		return $http.post('/fc/deleteFcResultVideo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	// 민원 및 사고 등록 master insertFcResult2Master.json
	function insertFcResult2Master(parameter) {
		return $http.post('/fc/insertFcResult2Master.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	// 민원 및 사고 조치결과 등록 sub updateFcResult2Sub.json
	function updateFcResult2Sub(parameter) {
		return $http.post('/fc/updateFcResult2Sub.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	// 민원 및 사고 상세조회
	function getFcResult2Info(parameter) {
		var url = '';
		switch (parameter.jm_jopt) {
			// 민원
			case '31':
				url = '/fc/getFcResultComplaintInfo.json';
				break;
			// 사고
			case '32':
				url = '/fc/getFcResultAccidentInfo.json';
				break;
			default:
				break;
		}
		if (url == '') {
			alert('error');
			return;
		}

		return $http.post(url, parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	// 관로진단 상세조회
	function getFcResultPipeInfo(parameter) {
		return $http.post('/fc/getFcResultPipeInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alertify.error('getFcResultPipeInfo error');
		});
	}
	// 관로진단 대상시설물 삭제
	function deleteCmFac(parameter) {
		console.log('deleteCmFac');
		console.log(parameter);
		return $http.post('/fc/deleteCmFac.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alert('error');
		});
	}
	// 교육현황 관리 상세조회
	function getFcResultEduInfo(parameter) {
		return $http.post('/fc/getFcResultEduInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alertify.error('getFcResultEduInfo error');
		});
	}
	// 교육 참석자 등록
	function insertFcResultEduEmp(parameter) {
		return $http.post('/fc/insertFcResultEduEmp.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alertify.error('insertFcResultEduEmp error');
		});
	}
	// 교육 참석자 조회
	function getFcResultEduEmpList(parameter) {
		return $http.post('/fc/getFcResultEduEmpList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alertify.error('getFcResultEduEmpList error');
		});
	}
	// 교육 참석자 등록
	function deleteFcResultEduEmp(parameter) {
		return $http.post('/fc/deleteFcResultEduEmp.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alertify.error('deleteFcResultEduEmp error');
		});
	}
	// 교육현황 관리 상세조회
	function getFcResultBookInfo(parameter) {
		return $http.post('/fc/getFcResultBookInfo.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
			alertify.error('getFcResultBookInfo error');
		});
	}
	// 로그 조회
	function getSystemLogList(parameter) {

		parameter.lg_opt = parameter.lg_opt || '';

		var param = '?lg_opt=' + parameter.lg_opt;

		return $http.get(Version.V1 + '/getSystemLog.json' + param).success(function(response) {
			return response;
		}).error(function(error) {
			alertify.error('getSystemLogList error');
		});
	}
	// 하수처리시설 리스트
	function getsewageList() {
		return $http.get('/basic/facility/getsewageList.json?ftr_cde=SB200').success(function(response) {
			return response;
		}).error(function(error) {
			alertify.error('getsewageList error');
		});
	}

	//세션
	function getHasSession() {
		return $http.get('/common/getLogin.json').success(function(response) {
			return response;
		}).error(function(error) {
			alertify.error(error);
		});
	}

	/*
	상수운영관리
 	*/
	function getBlockCodeList(parameter) {
		return $http.get('/api/getBlockCodeList.json').success(function (response) {
			return response;
		}).error(function(error) {
			alertify.error('get blockCodeList error');
		});
	}
	
	// 상수운영관리 요금정보 - 블록그룹별
	function getConsumerGroup(parameter) {
		return $http.post('/operation/getConsumerGroup.json', parameter).success(function (response) {
			return response;
		}).error(function(error) {
			//alertify.error('get blockList error');
		});
	}
	// 상수운영관리 요금정보 - 목록
	function getConsumerList(parameter) {
		return $http.post('/operation/getConsumerList.json', parameter).success(function (response) {
			return response;
		}).error(function(error) {
			//alertify.error('get blockList error');
		});
	}
	
	function getBlockList(parameter) {
		return $http.post('/operation/getBlockList.json', parameter).success(function (response) {
			return response;
		}).error(function(error) {
			alertify.error('get blockList error');
		});
	}

	function getData_flux_wp_month(parameter) {
		return $http.post('/operation/getData_flux_wp_month.json', parameter).success(function (response) {
			console.log(parameter);
			return response;
		}).error(function(error) {
			alertify.error('get flux and water pressure data (month) error');
		});
	}

	function getData_flux_wp_day(parameter) {
		return $http.post('/operation/getData_flux_wp_day.json', parameter).success(function (response) {
			console.log(parameter);
			return response;
		}).error(function(error) {
			alertify.error('get flux and water pressure data (day) error');
		});
	}


	
	function CommunicationRelay(URL, parameter = {}){
		return $http.post(URL, parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	
	function getData_flux_wp_hour(parameter) {
		return $http.post('/operation/getData_flux_wp_hour.json', parameter).success(function (response) {
			console.log(parameter);
			return response;
		}).error(function(error) {
			alertify.error('get flux and water pressure data (hour) error');
		});
	}
	function getData_flux_wp_min(parameter) {
		return $http.post('/operation/getData_flux_wp_min.json', parameter).success(function (response) {
			console.log(parameter);
			return response;
		}).error(function(error) {
			alertify.error('get flux and water pressure data (hour) error');
		});
	}

	// 삭제 예정. 수질 데이터를 grid 2개로 쪼개기 전
	function getData_waterQuality(parameter) {
		return $http.post('/operation/getData_waterQuality.json', parameter).success(function (response) {
			console.log(parameter);
			return response;
		}).error(function(error) {
			alertify.error('get water quality data error');
		});
	}

	function getData_waterQuality_hour(parameter) {
		return $http.post('/operation/getData_waterQuality_hour.json', parameter).success(function (response) {
			return response;
		}).error(function(error) {
			alertify.error('get water quality data (hour) error');
		});
	}

	function getData_waterQuality_min(parameter) {
		return $http.post('/operation/getData_waterQuality_min.json', parameter).success(function (response) {
			console.log(parameter);
			return response;
		}).error(function(error) {
			alertify.error('get water quality data (min) error');
		});
	}

	function insertMinwonTemp(parameter){
		return $http.post('/operation/insertMinwonTemp.json', parameter).success(function(response) {
			return response; //글 insert 후 값 리턴
		}).error(function(error) {
		});
	}

	function deleteMinwonTemp(parameter){
		return $http.post('/operation/deleteMinwonTemp.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function saveMinwon(parameter){
		return $http.post('/operation/saveMinwon.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function deleteMinwon(parameter){
		return $http.post('/operation/deleteMinwon.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function insertMinwonFile(parameter) {
		return $http.post('/operation/insertMinwonFile.json', parameter, {
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getMinwonFileList(parameter){
		console.log(Version);
		return $http.post('/operation/getMinwonFileList.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function deleteMinwonFileOne(parameter){
		return $http.post('/operation/deleteMinwonFileOne.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function insertMinwonAsset(parameter){
		return $http.post('/operation/insertMinwonAsset.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}

	function getMinwonAssetPic(parameter){
		return $http.get('/operation/getSelectMinwonAssetPic.json?complain_sid='+parameter.COMPLAIN_SID+'&AssetSid='+parameter.ASSET_SID+'&photo_yn='+parameter.PHOTO_YN)
			.success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	
	//통계 자산
	function getStatAsset(parameter){
		return $http.post('/asset/getStatAsset.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	//통계 개보수
	function getStatRenovation(parameter){
		return $http.post('/operation/getStatRenovation.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	//통계 민원
	function getStatMinwon(parameter){
		return $http.post('/operation/getStatMinwon.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}

	//통계 조사및탐사
	function getStatInspect(parameter){
		return $http.post('/operation/getStatInspect.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	//통계 상태평가
	function getStatIndirEst(parameter){
		return $http.post('/est/getStatIndirEst.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	//통계 잔존수명
	function getStatRemainLife(parameter){
		return $http.post('/est/getStatRemainLife.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	//통계 위험도평가
	function getStatRisk(parameter){
		return $http.post('/est/getStatRisk.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	//통계 서비스수준
	function getStatLoS(parameter){
		return $http.post('/est/getStatLoS.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}	
	
	//통계 생애주기
	function getStatLcc(parameter){
		return $http.post('/est/getStatLcc.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	//통계 최적투자
	function getStatOIP(parameter){
		return $http.post('/est/getStatOIP.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	//통계 재정수지
	function getStatFinance(parameter){
		return $http.post('/est/getStatFinance.json',parameter)
		.success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}
	
	
	// 재정계획 수립 > 재정계획 목록
	function getFinancialPlanList(parameter) {
		return $http.post('/finance/getFinancialPlanList.json', parameter).success(response => response);
	}

	// 재정계획 수립 > 재정수지분석 관리
	function getFinancialPlanDetailList(parameter) {
		return $http.post('/finance/getFinancialPlanDetailList.json', parameter).success(response => response);
	}

	// 재정계획 수립 > 재무재표(BS) 관리
	function getFinancialStatementsList(parameter) {
		return $http.post('/finance/getFinancialStatementsList.json', parameter).success(response => response);
	}
	
	function setFinancialStatement(parameter) {
		return $http.post('/finance/setFinancialStatement.json', parameter).success(response => response);
	}

	// 재정계획 수립 > 손익계산서(PL) 관리
	function getIncomeStatement(parameter) {
		return $http.post('/finance/getIncomeStatement.json', parameter).success(response => response);
	}

	function setIncomeStatement(parameter) {
		return $http.post('/finance/setIncomeStatement.json', parameter).success(response => response);
	}

	// 재정계획 수립 > 현금흐름표(CF) 관리
	function getCashFlowStatement(parameter) {
		return $http.post('/finance/getCashFlowStatement.json', parameter).success(response => response);
	}

	function setCashFlowStatement(parameter) {
		return $http.post('/finance/setCashFlowStatement.json', parameter).success(response => response);
	}

	// 재정계획 수립 > 기타 비용 관리
	function getOtherCostManagement(parameter) {
		return $http.post('/finance/getOtherCostManagement.json', parameter).success(response => response);
	}

	/**
	 * @param parameter
	 * @param {Array} parameter.item_list
	 * @returns {*}
	 */
	// 재무 분석 > 재정계획 수립 > 기타비용관리 (팝업) 저장
	function setOtherCostManagement(parameter) {
		return $http.post('/finance/setOtherCostManagement.json', parameter).success(response => response);
	}

	// 재정계획 수립 > 재정계획 삭제
	function deleteFinancialPlan(parameter) {
		return $http.post('/finance/deleteFinancialPlan.json', parameter).success(response => response);
	}

	function insertTempFinancialPlan() {
		return $http.post('/finance/insertTempFinancialPlan.json').success(response => response);
	}

	/**
	 * @param {Object} parameter
	 * @param {Number} parameter.fin_anls_sid
	 * @param {String} parameter.fin_anls_type
	 * @param {String} parameter.fin_anls_ymd
	 * @returns {*}
	 */
	function getOperationAndManagementExpensesPopupList(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/finance/getOperationAndManagementExpensesPopupList.json?' + param).success(response => response);
	}

	/**
	 * @param {Object} parameter
	 * @param {Number} parameter.fin_anls_sid
	 * @param {String} parameter.fin_anls_type
	 * @param {String} parameter.fin_anls_ymd
	 * @returns {*}
	 */
	function getDepreciationPopupList(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/finance/getDepreciationPopupList.json?' + param).success(response => response);
	}

	/**
  * @param {Object} parameter
  * @param {Array} parameter.item_list
  * @param {string} parameter.temp_yn
  * @param {string} [parameter.fin_anls_nm]
  * @param {string} [parameter.fin_anls_ymd]
  * @returns {*}
  */
	// 재정계획 수립 > 재정수지 분석 (팝업, TAB-재정수지총괄)
	function setFinancialPlanDetail(parameter) {
		return $http.post('/finance/setFinancialPlanDetail.json', parameter).success(response => response);
	}

	/**
	  * @param {Object} parameter
	  * @param {Array} parameter.item_list
	  * @returns {*}
	  */
		// 재정계획 수립 > 재정수지 분석 (팝업, TAB-BS수정)
		function setFinancialBS(parameter) {
			return $http.post('/finance/setFinancialStatement.json', parameter).success(response => response);
		}
		
		// 재정계획 수립 > 재정수지 분석 (팝업, TAB-PL수정)
		function setFinancialPL(parameter) {
			return $http.post('/finance/setIncomeStatement.json', parameter).success(response => response);
		}

		// 재정계획 수립 > 재정수지 분석 (팝업, TAB-CF수정)
		function setFinancialCF(parameter) {
			return $http.post('/finance/setCashFlowStatement.json', parameter).success(response => response);
		}
		
		// 재정계획 수립 > 재정수지 분석 (팝업, TAB-ETC수정)
		function setFinancialETC(parameter) {
			return $http.post('/finance/setOtherCostManagement.json', parameter).success(response => response);
		}		
			
	/**
  * @param {Object} parameter
  * @param {string | number} parameter.fin_anls_sid
  * @param {string} [parameter.fin_anls_nm]
  * @param {string} [parameter.fin_anls_ymd]
  * @param {string} [parameter.temp_yn]
  * @param {string | number} [parameter.price_raise_ratio]
  * @returns {*}
  */
	function updateFinancialPlan(parameter) {
		return $http.post('/finance/updateFinancialPlan.json', parameter).success(response => response);
	}

	/**
	 *
	 * @param {Object} parameter
	 * @param {string} parameter.fin_anls_sid
	 * @returns {*}
	 */
	// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가 설정 (팝업)
	function getUnitPriceSettingInfo(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/finance/getUnitPriceSettingInfo.json?' + param).success(response => response);
	}

	/**
	 *
	 * @param {Object} parameter
	 * @param {string} parameter.fin_anls_sid
	 * @returns {*}
	 */
	// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가 설정 (팝업)
	function getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/finance/getUnitPriceAndProductionVolumeAndPurchaseVolumeInfo.json?' + param).success(response => response);
	}

	// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 전기 말 이자율 설정 (팝업)
	function getInterestRateEndOfLastYearInfo() {
		return $http.get('/finance/getInterestRateEndOfLastYearInfo.json').success(response => response);
	}

	/**
	 * @param {Object} parameter
	 * @param {number} parameter.fin_anls_sid
	 * @param {Object} parameter.item
	 * @returns {*}
	 */
	// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 요금단가, 요금단가/생산량/구입량 설정 저장
	function setFinancialAnalysisChargeUnitPrice(parameter) {
		return $http.post('/finance/setFinancialAnalysisChargeUnitPrice.json', parameter).success(response => response);
	}

	/**
	 * @param {Object} parameter
	 * @param {Object} parameter.item
	 * @returns {*}
	 */
	// 재무 분석 > 재정계획 수립 > 재정수지 분석 > 전기 말 이자율 설정 (팝업) 저장
	function updateInterestRateEndOfLastYearInfo(parameter) {
		return $http.post('/finance/updateInterestRateEndOfLastYearInfo.json', parameter).success(response => response);
	}

	// 자산관리 계획수립 목록
	function getAssetManagementPlanList(parameter) {
		return $http.post('/finance/getAssetManagementPlanList.json', parameter).success(response => response);
	}

	// 자산관리 계획 수립
	function getAssetManagementPlanDetailList(parameter) {
		return $http.post('/finance/getAssetManagementPlanDetailList.json', parameter).success(response => response);
	}

	// 자산관리 계획 삭제
	function deleteAssetManagementPlan(parameter) {
		return $http.post('/finance/deleteAssetManagementPlan.json', parameter).success(response => response);
	}
	
	// 자산관리 계획 신규
	function insertAssetManagementPlan(parameter){
		return $http.post('/finance/insertAssetManagementPlan.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});		
	}

	// 자산관리 계획 저장
	function updateAssetManagementPlan(parameter){
		return $http.post('/finance/updateAssetManagementPlan.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	
	// 
	function updateAssetManagementPlanDetail(parameter){
		return $http.post('/finance/updateAssetManagementPlanDetail.json', parameter).success(function(response) {
			return response;
		}).error(function(error) {
		});
	}
	function getFinacePriceRaiseRatio(parameter){
		return $http.post('/finance/getFinacePriceRaiseRatio.json', parameter).success(response => response);
	}
	function getFinancialData(parameter) {
		return $http.post('/finance/getFinancialData.json', parameter).success(response => response);
	}
	
	/**
	 * @param {Object} parameter
	 * @param {*} parameter.ftr_idn
	 * @param {String} parameter.ftr_cde
	 * @param {String} parameter.layer_cd
	 */
	function getAssetInfoWithGisInfo(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/gis/getAssetInfoWithGisInfo.json?' + param).success(response => response);
	}

	function updateFeatureCodeWithLastData(parameter) {
		return $http.post('/gis/updateFeatureCodeWithLastData.json', parameter).success(function(response) {
			return response;
		});
	}

	function getAssetInfoWithLayer(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/gis/getAssetInfoWithLayer.json?' + param).success(response => response);
	}


	function addAssetGis(parameter) {
		return $http.post('/gis/insertAssetGis.json', parameter).success(response => response);
	}

	function getAssetInfoWithGisList(parameter) {
		return $http.post('/gis/getAssetInfoWithGisList.json', parameter).success(response => response);
	}

	function getGISInfoByAssetPath(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/gis/getGISInfoByAssetPath.json?' + param).success(response => response);
	}

	// 진단 구역 관리 (팝업, TAB-진단 구역) 진단 구역 삭제
	function deleteDiagnosticArea(parameter) {
		return $http.post('/gis/deleteDiagnosticArea.json', parameter).success(response => response);
	}

	function getDiagnosticAreaDetailEditList(parameter) {
		return $http.post('/gis/getDiagnosticAreaDetailEditList.json', parameter).success(function(response) {
			return response;
		});
	}

	// 진단 구역 등록/수정 (팝업) 저장
	function saveDiagnosticAreaDetail(parameter) {
		return $http.post('/gis/saveDiagnosticAreaDetail.json', parameter, {
			'headers': { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
		}).success(response => response);
	}

	// 진단 구역 등록/수정 (팝업) 진단 구역 자산 삭제
	function deleteDiagnosticAreaAsset(parameter) {
		return $http.post('/gis/deleteDiagnosticAreaAsset.json', parameter).success(response => response);
	}

	// 진단 구역 관리 (팝업, TAB-상태진단 그룹) 상태진단 그룹 삭제
	function deleteStateDiagnosisGroup(parameter) {
		return $http.post('/gis/deleteStateDiagnosisGroup.json', parameter).success(response => response);
	}

	// 상태진단 그룹 저장
	function saveStateDiagnosisGroup(parameter) {
		return $http.post('/gis/saveStateDiagnosisGroup.json', parameter, {
		        // method: "POST",
		        headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'},
		        // transformRequest: transformUrlEncoded
		    }).success(response => response);
	}
	
	function getLoanInfo(parameter) {
		return $http.post('/finance/getLoanInfo.json', parameter).success(response => response);
	}

	function addComCodeListYr2(parameter) {
		return $http.post('/finance/addComCodeListYr2.json', parameter).success(response => response);
	} 
	
	function addLoanItemYear(parameter) {
		return $http.post('/finance/addLoanItemYear.json', parameter).success(response => response);
	}
	
	function initFinancialPlanDetailList(parameter) {
		return $http.post('/finance/initFinancialPlanDetailList.json', parameter).success(response => response);
	}

	function getStateDiagnosticGroupDetailPopupInfo(parameter) {
		const stateDiagGroupSid = parameter.state_diag_group_sid || '';
		const param = '?state_diag_group_sid=' + stateDiagGroupSid;

		return $http.get('/gis/getStateDiagnosticGroupDetailPopupInfo.json' + param).success(response => response);
	}

	function getAssetsByAssetPath(parameter) {
		return $http.post('/gis/getAssetsByAssetPath.json', parameter).success(response => response);
	}

	function getAssetsByGISProperty(parameter) {
		return $http.post('/gis/getAssetsByGISProperty.json', parameter).success(response => response);
	}

	function getChartData(parameter){
		return $http.post(parameter.url, parameter).success(response => response);
	}


	function getStateDiagnosisGroupDetailEditList(parameter) {
		return $http.post('/gis/getStateDiagnosisGroupDetailEditList.json', parameter, {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
		}).success(response => response);
	}

	
	/**
	 * @param {Object} parameter
	 * @param {String} parameter.asset_cd
	 * @param {String} parameter.memo
	 * @param {string} [parameter.cvalue1]
	 * @param {string} parameter.cvalue2
	 * @returns {*}
	 */
	function getAssetByAssetCode(parameter) {
		var param = new URLSearchParams(parameter).toString();

		return $http.get('/gis/getAssetByAssetCode.json?' + param).success(response => response);
	}
	
	function getINSPECT_ASSET(parameter){
		console.log(parameter)
		return $http.post('/operation/getINSPECT_ASSET.json',parameter,{
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response; 
		}).error(function(error) {
		});
	}
	
	function getREPAIR_ASSET(parameter){
		console.log(parameter)
		return $http.post('/operation/getREPAIR_ASSET.json',parameter,{
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).success(function(response) {
			return response; 
		}).error(function(error) {
		});
	}
	
	function updateLogUser(parameter){
		return $http.post('/basic/employee/updateLogUser.json', parameter).success(response => response);
	}
	
	function saveTempPipeList(parameter){
		return $http.post('/gis/saveTempPipeList.json', parameter).success(response => response);
	} 
	function getTempPipeList(parameter){
		return $http.post('/gis/getTempPipeList.json', parameter).success(response => response);
	}
	function getElevationDifferenceAnalysisInfo(parameter){
		return $http.post('/gis/getElevationDifferenceAnalysisInfo.json', parameter).success(response => response);
		//return $http.get('/gis/getElevationDifferenceAnalysisInfo.json', parameter).success(response => response);
	}
	function getGoogleGeoElevationApi(parameter){
		//"POINT(126.50981927110001 35.6380688110438)"
		//parameter = '37.55157798469981,126.98770523071289'
		var param = parameter.replace('POINT(','').replace(')','').split(' ');
		var param1 = param[1].substring(0,17)+','+param[0].substring(0,17);
		return $http.get('gis/getGoogleMapApi?locations='+param1).success(response=>response);
		//return $http.get('https://maps.googleapis.com/maps/api/elevation/json?locations='+param1+'&sensor=false&key=AIzaSyBnV9yDpkCXPci_6qrxYZVwn46N4UVjxBc').success(response=>response);
	}
	function getVWorldAddress(parameter){
		var param = new URLSearchParams(parameter).toString();
		//alert(param);
		return $http.get('https://api.vworld.kr/req/address?' + param).success(response => response);		
	}
	
	function setLogout (parameter){
		return $http.post('/common/setLogout.json', parameter).success(response => response);
	}

	function deleteTemporaryAsset(parameter) {
		return $http.post('/gis/deleteTemporaryAsset.json', parameter).success(response => response);
	}
	
	function setPostgisGeoInfo(parameter){
		return $http.post('/gis/setPostgisGeoInfo.json', parameter).success(response => response);
	}
}