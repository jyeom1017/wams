angular.module('app.book').controller('bookController', bookController);

function bookController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig) {

	var jopt = '91';

	switch ($state.current.name) {
		case '011001':
			$scope.MCODE = '1001';
			break;
		case '011003':
			$scope.MCODE = '1003';
			break;
		default:
			break;
	}

	$scope.ResultInfo = {};
	$scope.storeFileList = [];
	$scope.IsNewRecord = true;

	$scope.rows = 24; // 책 개수
	$scope.page = 1;

	// $scope.storeYear = getYears(YearSet.baseYear,
	// YearSet.plus).sort(function(a, b) {
	// return b - a;
	// });
	$scope.storeYear = getYears(YearSet.baseYear, YearSet.plus);
	var today = new Date();
	var year = today.getFullYear();
	var yyyymmdd = year + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

	$scope.$on('$viewContentLoaded', function() {
		init1();
		gridResize();
		$scope.ResultInfo.JM_SDATE = yyyymmdd;
		// setDatePicker();
		setDatePickerToday();
	});
	$timeout(function() {
		$scope.selectedYear = '' + (new Date()).getFullYear();
	});

	$scope.loadAllFileList = function() {
		mainDataService.getFileList({}).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.error('errMessage : ' + obj.errMessage);
			} else {
				// console.log(obj);
				$scope.storeFileList = obj;
			}
		});
	};
	$scope.cmCodeAction = function() {
		$scope.cmCodeList = [];
		mainDataService.getCommonCodeList({
			gcode : '801'
		}).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.error('errMessage : ' + obj.errMessage);
			} else {
				$scope.bookCodeList = obj;
			}
		});
	};
	$scope.searchfBookOpt = '';
	$scope.loadAllFileCountList = function(callback) {

		var param = {};
		param.crudURL = '/fc/getFcResultBookAllList.json';
		param.page = $scope.page;
		param.rows = $scope.rows;
		param.srh_txt = $scope.searchTitle || '';
		param.srh_book_opt = $scope.searchfBookOpt || '';

		mainDataService.cmCRUD(param).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.error('errMessage : ' + obj.errMessage);
			} else {
				// console.log(obj);
				if (obj.total >= obj.page) {
					$scope.storeFileList = obj.rows;
					$scope.page = obj.page;
					$scope.records = obj.records;
					$scope.pagination = {
						current : obj.page,
						last : obj.total
					};

				} // else if (obj.total < obj.page) {
				// alertify.error('유효하지 않은 페이지 입니다.');
				// return;
				// }
				callback();
			}
		});
	};
	$scope.boundaryLinks = true;
	$scope.directionLinks = true;
	// $scope.pages = [];
	$scope.pagination = {
		current : 1,
		last : 100
	};
	$scope.setCurrent = function(page) {
		if (page < 1)
			return;
		// if (page > $scope.pagination.last)
		// return;

		$scope.pagination.current = page;

		if ($scope.pagination.last < $scope.pagination.current) {
			$scope.pagination.current = $scope.pagination.last;
			$scope.page = $scope.pagination.current;
		} else {
			$scope.page = page;
		}

		$scope.loadAllFileCountList(function() {

			drawPage();

		});
		$scope.cmCodeAction();
	};

	function drawPage() {
		$scope.totalpages = [];
		for (var i = 1; i <= $scope.pagination.last; i++)
			$scope.totalpages.push(i);
		// console.log($scope.totalpages);

		$scope.pages = [];
		var k = 0;
		var startPage = 1;

		// 현재 페이지가 10의 배수일 때
		if (parseInt($scope.pagination.current % 10) == 0) {
			startPage = parseInt($scope.pagination.current - 10) + 1;
		} else {
			startPage = parseInt($scope.pagination.current / 10) * 10 + 1;
		}
		$.each($scope.totalpages, function(idx, Item) {
			if (startPage <= Item) {
				$scope.pages.push(Item);
				k++;
				if ($scope.pagination.last <= Item) {
					return false;
				}
			}

			if (10 <= k) {
				return false;
			}
		});

	}

	function init1() {

		pagerJsonGrid({
			grid_id : 'workList',
			pager_id : 'workPager',
			url : '/fc/getFcResultList.json',
			condition : {
				page : 1,
				rows : GridConfig.sizeXL,
				jm_jopt : jopt,
				s_jm_sdate : $scope.selectedYear || ''
			},
			rowNum : GridConfig.sizeXL,
			colNames : [/* 'JOPT', */'순번', '관리번호', '도서 및 성과품', '작성자', '등록일자', '담당자', '' ],
			colModel : [ /*
							 * { name : 'JM_JOPT', width : 0, index : 'JM_JOPT',
							 * hidden : true },
							 */{
				name : 'RNUM',
				width : 50,
				index : 'RNUM',
				sorttype : 'int'
			}, {
				name : 'JNUM',
				width : 120,
				index : 'JNUM',
				sorttype : 'int'
			}, {
				name : 'JM_NAME',
				width : 160,
				index : 'JM_NAME'
			}, {
				name : 'JM_JUNAME',
				width : 80,
				index : 'JM_JUNAME'
			}, {
				name : 'JM_SDATE',
				width : 120,
				index : 'JM_SDATE'
			}, {
				name : 'JM_MEID_TEXT',
				width : 80,
				index : 'JM_MEID_TEXT'
			}, {
				name : 'JM_MEID',
				index : 'JM_MEID',
				hidden : true
			} ],
			onSelectRow : function(rowid, status, e) {
				var param = $(this).jqGrid('getRowData', rowid);
				// console.log(parseInt(param.JNUM));
				// param.jm_jopt = jopt;
				if (parseInt(param.JNUM) > 0) {
					// $scope.ResultInfo(param);
					setResultInfo(param);
					// console.log(param);
					loadFileList(param.JNUM);
					$scope.file = null;
					// $scope.fileSelected = false;
					$scope.IsNewRecord = false;
				} else {
					$scope.newFcResultInfo();
				}
				if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

				} else {
					$scope.$apply();
				}

			}
		});

		return [ {
			'name' : 'JNUM',
			'title' : '민원번호',
			'name2' : 'jnum',
			'maxlength' : 38
		}, {
			'name' : 'JM_JOPT',
			'title' : '작업구분',
			'name2' : 'jm_jopt',
			'maxlength' : 2
		}, {
			'name' : 'JM_NAME',
			'title' : '도서 및 성과품',
			'name2' : 'jm_name',
			'required' : true,
			'maxlength' : 100
		}, {
			'name' : 'JM_JUNAME',
			'title' : '작성자',
			'name2' : 'jm_juname',
			'required' : true,
			'maxlength' : 100
		}, {
			'name' : 'JM_SDATE',
			'title' : '등록일자',
			'name2' : 'jm_sdate',
			'maxlength' : 10
		}, {
			'name' : 'JM_MEID',
			'title' : '접수자 ID',
			'name2' : 'jm_meid',
		}, {
			'name' : 'JM_MEID_TEXT',
			'title' : '담당자',
			'name2' : 'jm_meid_text',
			'required' : true
		} ];
	}

	function init2() {
		// $scope.loadAllFileList();
		$scope.setCurrent(1);
		// $scope.loadAllFileCountList();
		// $scope.cmAction();
		return [];
	}
	var data;
	switch ($state.current.name) {
		case '011001':
			$scope.MCODE = '1001';
			jopt = '91';
			type = '011001';
			data = init1();
			break;
		case '011003':
			$scope.MCODE = '1003';
			jopt = '';
			type = '011003';
			data = init2();
			break;

		default:
			break;
	}

	$scope.newFcResultInfo = function() {
		$scope.ResultInfo = {};
		$scope.ResultInfo.JM_SDATE = yyyymmdd;
		$scope.IsNewRecord = true;
	};

	// 목록 검색
	$scope.searchFile = function() {

		$scope.page = 1;

		$scope.loadAllFileCountList(function() {

			drawPage();
		});
	};

	// 파일 업로드 validation check
	$scope.onFileSelect = function($files, cmd) {
		// console.log($files);
		if ($files.length === 0) {
			return;
		}
		var str = '\.(' + cmd + ')$';
		var FileFilter = new RegExp(str);
		var ext = cmd.split('|');
		if ($files[0].name.toLowerCase().match(FileFilter)) {
			$scope.file = $files;
			// $scope.fileSelected = true;
		} else {
			$files = null;
			$scope.file = $files;
			// $scope.fileSelected = false;
			alertify.alert('Info', '확장자가 ' + ext + ' 인 파일만 선택가능합니다.');
		}
		if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

		} else {
			$scope.$apply();
		}
	};

	// 파일 업로드
	$scope.fileUpload = function(mode) {
		var jnum = $scope.ResultInfo.JNUM;
		if (!(jnum > 0)) {
			alertify.alert('작업을 선택해주세요');
			return;
		}
		if ($scope.file == null || $scope.file.length == 0) {
			alertify.alert('파일을 선택하세요');
			return;
		}
		var fileDesc = $scope.file[0].name;
		if ($scope.file != null)
			mainDataService.fileUpload(fileDesc, 'book', $scope.file, '', jopt).success(function(obj) {
				if (!common.isEmpty(obj.errMessage)) {
					alertify.error('errMessage : ' + obj.errMessage);
				} else {
					// console.log(obj);
					if (parseInt(obj.filenumber) > 0) {
						$scope.file = null;
						// $scope.fileSelected = false;
						// console.log('$scope.file');
						// console.log($scope.file);
						mainDataService.saveFcResultFile({
							jnum : jnum,
							f_num : obj.filenumber
						}).success(function(data) {
							if (!common.isEmpty(data.errMessage)) {
								alertify.error('errMessage : ' + data.errMessage);
							} else {
								// console.log(data);
								$scope.loadFileList(jnum);
								alertify.success('파일업로드성공');
							}
						});
					} else {
						alertify.alert('파일업로드 실패');
					}
				}
			});
	};

	// 파일삭제
	$scope.deleteFile = function(file) {

		if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
			alertify.alert('권한이 없습니다.');
			return;
		}

		var jnum = $scope.ResultInfo.JNUM;
		if (!(jnum > 0)) {
			alertify.alert('작업을 선택해주세요');
			return;
		}

		alertify.confirm('정말 삭제하시겠습니까?', function(e) {
			if (e) {
				mainDataService.deleteFcResultFile({
					jnum : jnum,
					f_num : file.JF_FNUM
				}).success(function(obj) {
					if (!common.isEmpty(obj.errMessage)) {
						alertify.error('errMessage : ' + obj.errMessage);
					} else {
						if (obj == '1') {
							alertify.success('삭제되었습니다.');
							loadFileList(jnum);
						} else {
							alertify.alert('error' + obj);
						}
					}
				});
			} else {
				return;
			}
		});
	};

	// 파일리스트 가져오기
	function loadFileList(jnum) {
		mainDataService.getFcResultFileList({
			jnum : jnum
		}).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.error('errMessage : ' + obj.errMessage);
			} else {
				$scope.storeFileList = obj;
			}
		});
	}
	$scope.loadFileList = loadFileList;

	$scope.preView = function(file) {
		// console.log(file);
		if (file.F_PATH != null && file.F_PATH != '' && typeof file.F_PATH != 'undefined') {
			window.open(file.F_PATH, '_blank');
		}
	};

	// $scope.deleteFile = function(file) {
	// var jnum = $scope.ResultInfo.JNUM;
	// if (!(jnum > 0)) {
	// alertify.alert('작업을 선택해주세요');
	// return;
	// }
	//		
	// alertify.confirm('정말 삭제하시겠습니까?', function(e) {
	// if (e) {
	// mainDataService.deleteFcResultFile({
	// jnum : jnum,
	// f_num : file.JF_FNUM
	// }).success(function(obj) {
	// if (obj == '1') {
	// alertify.success('삭제되었습니다.');
	// loadFileList(jnum);
	// } else {
	// alertify.alert('error' + obj);
	// }
	// });
	// } else {
	// return;
	// }
	// });
	// }

	$scope.selectManager = function(callbackId) {
		var param = {};
		// param._search = true;
		// param.rules = " AND A.E_GCODE1 NOT IN ('G001')";
		param.crudURL = '/basic/employee/getEmployeeList1.json';
		mainDataService.cmCRUD(param).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.error('errMessage : ' + obj.errMessage);
			} else {
				obj.callbackId = callbackId; // 'selectUserID_1';
				$rootScope.$broadcast('popupEmployeeList', obj);
			}
		});
	};

	$rootScope.$on('selectUserID', function(event, data) {
		$scope.ResultInfo.JM_MEID_TEXT = data.E_NAME;
		$scope.ResultInfo.JM_MEID = data.E_ID;
	});

	$scope.newFcResultInfo();
	gridResize();

	// grid 조회
	$scope.searchFcResultList = function(callback) {
		// console.log($('#searchText').val().trim() == '');
		// console.log($('#searchText').val());
		$('#workList').jqGrid('setGridParam', {
			search : true,
			postData : {
				page : 1,
				rows : 10,
				jm_jopt : jopt,
				s_jm_sdate : $scope.selectedYear || '',
				rules : " AND JM_NAME LIKE '%" + $('#searchText').val().trim() + "%'"
			}
		}).trigger('reloadGrid', {
			current : true
		});
		if (typeof callback == 'function')
			callback();
	};

	// Master 저장
	$scope.saveResult = function() {
		if (!ItemValidation($scope.ResultInfo, data))
			return;
		var param = ItemFilter($scope.ResultInfo, data, 1);
		param.jm_jopt = jopt;
		// console.log('param');
		// console.log(param);
		mainDataService.insertFcResult2Master(param).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.error('errMessage : ' + obj.errMessage);
			} else {
				// console.log(obj);
				if (parseInt(obj) > 0) {
					// alertify.success('저장되었습니다.');
					var jnum = obj;
					$scope.selectedYear = (param.jm_sdate).substring(0, 4);
					// $scope.searchFcResultList();

					$scope.searchFcResultList(function() {
						mainDataService.getFcResultBookInfo({
							jnum : jnum,
							jm_jopt : jopt
						}).success(function(data) {
							if (!common.isEmpty(data.errMessage)) {
								alertify.error('errMessage : ' + data.errMessage);
							} else {
								setResultInfo(data);
								alertify.success('저장되었습니다.');
							}
						});
					});
				} else {
					alertify.error('error saveFcResultMaster');
				}
			}
		});
	};

	// Master 삭제
	$scope.deleteResult = function() {
		if (parseInt($scope.ResultInfo.JNUM) > 0) {
			alertify.confirm('정말 삭제하시겠습니까?', function(e) {
				if (e) {
					mainDataService.deleteFcResultMaster({
						jnum : $scope.ResultInfo.JNUM,
						jm_jopt : jopt
					}).success(function(obj) {
						if (!common.isEmpty(obj.errMessage)) {
							alertify.error('errMessage : ' + obj.errMessage);
						} else {
							$scope.searchFcResultList();
							$scope.newFcResultInfo();
							$scope.IsNewRecord = true;
						}
					});
					alertify.success('결과정보가 삭제되었습니다.');
				} else {
					return;
				}
			});
		}
	};

	// 상세보기
	function setResultInfo(param) {
		var jnum = param.JNUM;

		$scope.ResultInfo = param;
		if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

		} else {
			$scope.$apply();
		}
		$scope.IsNewRecord = false;

		// console.log('setResultInfo');
		// console.log($scope.ResultInfo);

		loadFileList(jnum);
	}

	// 사이즈가 조절 될 떄 마다 책 개수
	if ($state.current.name == '011003') {
		$(window).resize(function() {
			if ($state.current.name == '011003') {
				var widthBooks = $('#books').width();
				// console.log(widthBooks);
				// 책장 resize 4개이하 180 5개 이상 230
				// var width_list = [ 0, 400, 775, 1050, 1300, 1900, 2200, 2600,
				// 3000, 3600 ];
				var countBook = 24;
				// $.each(width_list, function(idx, Item) {
				// if (widthBooks < Item) {
				// countBook = idx * 3;
				// return false;
				// }
				// });
				countBook = parseInt(widthBooks / 165) * 3;
				// console.log(countBook);

				$scope.rows = countBook;
				$scope.pagination.last = parseInt(($scope.records - 1) / $scope.rows) + 1;

				// drawPage();
				$scope.setCurrent($scope.pagination.current);

				if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

				} else {
					$scope.$apply();
				}
				// gridResize();
			}
		});
	}

	$scope.isPdf = function(fileName, event) {
		if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
			alertify.alert('권한이 없습니다.');
			return;
		}
		var fileLength = fileName.length;
		var lastDot = fileName.lastIndexOf('.');
		var fileExtension = fileName.substring(lastDot + 1, fileLength);
		var result = fileExtension == 'pdf' ? true : false;
		if (!result) {
			alertify.alert('pdf파일만 미리보기 가능합니다.');
			event.preventDefault();
		}
	};

}
