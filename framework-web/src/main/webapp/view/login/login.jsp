<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>로그인</title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="../assets/js/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="../assets/css/fontawesome_all.css">
	<link rel="stylesheet" href="../assets/XEIcon/xeicon.min.css">
	<link rel="stylesheet" href="../assets/css/common.css">
	<link rel="stylesheet" href="../assets/css/main.css">
	<script src="../assets/js/jquery/jquery-1.12.4.min.js"></script>
	<script src="../assets/js/jquery/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
</head>
	<body>
		<section class="login">
			<div class="loginWrap">
				<div class="logoImg">
					<img src="../assets/images/common/logo_login.png" alt="로고">
				</div>
				<div class="loginText">
					<h1><span>지리정보시스템을 이용한 상하수도 자산관리시스템</h1>
				</div>
				<div class="login">
					<p>LOGIN</p>
					<div class="loginInfo">
						<div class="loginLeft">
							<form id="loginForm" method='POST' action="<c:url value="j_spring_security_check" />">
								<input type="text" placeholder="아이디 입력" class="form-control" name="user_id" value="" maxlengt="30">
								<input type="password" placeholder="비밀번호 입력" class="form-control" name="user_pw" value="" maxlengt="30">
								<button class="btn_login">로그인</button>
							</form>
						</div>
						<!-- <div class="loginRight">
							<div class="electronicLogin">
								<a href="">
									<i class="xi-lock-o xi-3x"></i>
									<span>인증서<br>LOGIN</span>
								</a>
							</div>
						</div> -->
					</div>
				</div>
				<div class="loginTextBtm">
					<div>
						<address class="address">(14059)경기도 안양시 동안구 흥안대로 415, 평촌동 두산벤처다임 1126호 Tel. 031-478-5647, Fax. 031-478-5656</address>
					</div>
					<p>
						Copyrightⓒ 2022, EPS-EnE, Inc. All right reserved.
					</p>
				</div>
			</div>
		</section>
	</body>
</html>