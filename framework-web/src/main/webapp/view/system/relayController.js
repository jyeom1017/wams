angular.module('app.system').controller('relayController', relayController);

function relayController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, GridConfig) {
	$scope.$on('$viewContentLoaded', function () {
        //
        gridResize();
        setDatePicker();
        $scope.setWeek();
        setGrid();
    });
		
	 var rows = [];

	 function loadNext(param){
	 var colIndex = [{name:'순번',id:'RANKNUM'},{name:'날짜',id:'LG_YMD'},{name:'대상시스템',id:'RELAY_NM'},{name:'연계정보구분',id:'RELAY_GBN'},{name:'연계항목',id:'RELAY_ITEM_CD'}
		 	,{name:'연계주기',id:'RELAY_CYCLE_CD'}	,{name:'송신구분',id:'TR_GBN'},{name:'전체데이터건수',id:'TOTAL_CNT'},{name:'활용데이터건수',id:'VALID_CNT'},{name:'불일치건수',id:'INVALID_CNT'}	];
	 var RELAY_GBN = {'0':'활용','1':'비활용'};
	 var RELAY_ITEM_CD = {'1':'유량','2':'수압','3':'수질','4':'요금'};
	 var RELAY_CYCLE_CD = {'1':'매일','2':'매월'};
	 var TR_GBN = {'0':'수신','1':'송신'};
	 	mainDataService.getRelayInfoListExcel(param)
	 	.success(function(Obj){
	 		
	 		if(Obj.length>0 ){
	     		$.each(Obj,function(idx,Item){
	     			var collumns = [];
	     			if(idx==0 && param.PAGE_NO== 0 ){
	     				var headers = [];				
	     				$.each(colIndex,function(idx1,item){
	     						headers.push(item.name);
	     				});
	     				rows.push(headers);
	     			}
	     			$.each(colIndex,function(idx1,item){
	     				switch(item.id){
	     				case 'RELAY_GBN' : collumns.push(RELAY_GBN[Item[item.id]]);break;
	     				case 'RELAY_ITEM_CD' : collumns.push(RELAY_ITEM_CD[Item[item.id]]);break;
	     				case 'RELAY_CYCLE_CD' : collumns.push(RELAY_CYCLE_CD[Item[item.id]]);break;
	     				case 'TR_GBN' : collumns.push(TR_GBN[Item[item.id]]);break;
	     				default : 
	     					collumns.push(Item[item.id]);
	     					break;
	     				}
	     				
	     			});
	     			rows.push(collumns);
	     		});    		
				param.PAGE_NO = param.PAGE_NO +1;
				$( "#progressbar" ).progressbar({value: param.PAGE_NO /( $scope.totalCount / 100.0) * 100 });
				
				if((param.PAGE_NO)* param.ROW_CNT <= $scope.totalCount){ 
					$timeout(function(){loadNext(param)},500);
					return;
				}
	 		}
	 		
	 		if((param.PAGE_NO+1)* param.ROW_CNT > $scope.totalCount){
	     		const workSheetData = rows;
	     		const workSheet = XLSX.utils.aoa_to_sheet(workSheetData);
	     		//workSheet['!autofilter'] = {ref : "A1:R11"};
	     		const workBook = XLSX.utils.book_new();
	     		XLSX.utils.book_append_sheet(workBook, workSheet, '연계정보목록');
	     		XLSX.writeFile(workBook, "연계정보_"+ $scope.getToday()+".xlsx");  
	     		$("#dialog-progressBar").hide();
	 		}
	 	});    	
	 }
 
	$scope.DownLoadExcel = function(){
	 	rows=[];
	 	var param={
	 				PAGE_NO:0,ROW_CNT:100,
	 		        type : $scope.searchDateType || '',
	 		        RELAY_ITEM_CD : $scope.SelectRelayItem || '',	 				
	 		        s_date : $scope.searchDate.S_DATE.replace(/-/gi,'') || '',
	 		        e_date : $scope.searchDate.E_DATE.replace(/-/gi,'') || '' 
	 			};
	 	loadNext(param);

	 	
	 	$rootScope.$broadcast('ShowCommonProgressBar',{callback_fnc:function(){
	 		//alert('test');
	 		$( "#progressbar" ).progressbar({value: 0});
	 	},alertMessage : '다운로드중입니다.'
	 	});
	 	//$scope.progress(0);		
	}
	
	$scope.changeRadio = function (type) {
        $scope.searchDateType = (type === 1) ? 1 : 2;
    };
    $scope.searchDateType = 1; // 1:최근1주일 , 2:사용자 선택 
    $scope.searchDate = {}; // 기간선택
    $scope.SelectRelayItem = '';
    
   $scope.setWeek= function setLastWeek() {
        var sday = new Date(); // 일주일전
        sday.setDate(sday.getDate() - 7); // 일주일전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.searchDate.S_DATE = s_yyyymmdd;
        $('#searchDate_S_DATE').val(s_yyyymmdd);
        $scope.searchDate.E_DATE = e_yyyymmdd;
        $('#searchDate_E_DATE').val(e_yyyymmdd);
    }
	
	 function setGrid() {
	        return pagerJsonGrid({
	            grid_id : 'list',
	            pager_id : 'listPager',
	            url : '/operation/getRelayInfo.json',
	            condition : {
	                page : 1,
	                rows : GridConfig.sizeL,
	 		        type : $scope.searchDateType || '',
	 		        RELAY_ITEM_CD : $scope.SelectRelayItem || '',	 				
	 		        s_date : $scope.searchDate.S_DATE.replace(/-/gi,'') || '',
	 		        e_date : $scope.searchDate.E_DATE.replace(/-/gi,'') || '' 	                
	            },
	            rowNum : 20,
	            colNames : [ 
	            	'날짜',
	            	'대상 시스템',
	            	'연계정보 구분', 
	            	'연계 항목',
	            	'연계 주기', 
	            	'송신 구분',
	            	'데이터 1',
	            	'데이터 2',
	            	'상태확인건수'
	            	],
	            colModel : [
	                { name : 'LG_YMD', width : 50 ,index : 'LG_DT', resizable: false},
	                { name : 'RELAY_NM', width : 100, resizable: false },
	                { name : 'RELAY_GBN', width : 30, edittype : 'select', formatter : 'select', editoptions : {value :'0:활용;1:비활용;'}, resizable: false },
	                { name : 'RELAY_ITEM_CD', width : 30, edittype : 'select', formatter : 'select', editoptions : {value :'1:유량;2:수압;3:수질;4:요금'} , resizable: false},
	                { name : 'RELAY_CYCLE_CD', width : 30, edittype : 'select', formatter : 'select', editoptions : {value :'1:매일;2:매월'} , resizable: false},
	                { name : 'TR_GBN', width : 30, edittype : 'select', formatter : 'select', editoptions : {value :'0:수신;1:송신'}, resizable: false },
	                { name : 'TOTAL_CNT', width : 100, resizable: false, formatter : function(cellValue, options, rowObject) {
		            	  if (rowObject.RELAY_ITEM_CD === '4' ) {
		            		  return '전월건수 '+'('+rowObject.TOTAL_CNT+')';
		            	  }else if(rowObject.RELAY_ITEM_CD != '4'){
		            		  if(rowObject.RELAY_ITEM_CD == null){
		            			  return '';
		            		  }else{
			            		  return '예측건수 '+'('+rowObject.TOTAL_CNT+')';
		            		  }
		            	  }
		              } 
	                },
	                { name : 'VALID_CNT', width : 100, resizable: false, formatter : function(cellValue, options, rowObject) {
		            	  if (rowObject.RELAY_ITEM_CD === '4' ) {
		            		  return '당월건수 '+'('+rowObject.VALID_CNT+')';
		            	  }else if(rowObject.RELAY_ITEM_CD != '4'){
		            		  if(rowObject.RELAY_ITEM_CD == null){
		            			  return '';
		            		  }else{
			            		  return '실측건수 '+'('+rowObject.VALID_CNT+')';
		            		  }
		            	  }
		              }  
	                },
	                { name : 'INVALID_CNT', width : 50, resizable: false ,cellattr: function(rowId, e, rowObject, d, rdata) {
	                    // rowObject 변수로 그리드 데이터에 접근
	                    // INVALID_CNT값이 0보다 크거나 작으면 글자 컬러 red 설정
	                    if (rowObject.INVALID_CNT == 0 ) { 
	                    	return 'style="color:blue;"' 
	                    }else{
	                    	return 'style="color:red;"' 
	                    }
	              },formatter : function(cellValue, options, rowObject) {
	            	  if (rowObject.INVALID_CNT > 0 || rowObject.INVALID_CNT < 0 ) {
	            		  return '불일치'+'('+rowObject.INVALID_CNT+')';
	            	  }else if (rowObject.INVALID_CNT == 0){
	            		  return '정상';
	            	  }else if(rowObject.INVALID_CNT == null){
	            		  return '';
	            	  }
	              }}
	            ],
	            onSelectRow : function(rowid, status, e) {
	            	$scope.SelectedAssetBaseInfo = $('#list').jqGrid('getRowData', rowid);
	            	$scope.$apply();
	            },
	            gridComplete : function() {
					var ids = $(this).jqGrid('getDataIDs');
					$(this).setSelection(ids[0]);
					$scope.currentPageNo=$('#list').getGridParam('page');
					$scope.count = $('#list').getGridParam('records');
					$scope.totalCount = $scope.count; 
					$scope.$apply();
				}	
	        });
	    }
	 
	 $scope.loadList = function(){
			$("#list").setGridParam({
				datatype : 'json',
				page : $scope.currentPageNo,
				postData : {
				
				}
			}).trigger('reloadGrid', {
				current : true
			});		
		}
	 
	 $scope.search = function(){
		 var data = {};
	        data.page = 1;
	        data.rows = GridConfig.sizeL;
	        data.type = $scope.searchDateType || '';
	        data.RELAY_ITEM_CD = $scope.SelectRelayItem || '';

	        // 최근 일주일 검색
	        if ($scope.searchDateType === 1) {
	        	$scope.setWeek();
	        // 기간선택 검색
	        } else {
	            data.s_date = $scope.searchDate.S_DATE.replace(/-/gi,'') || '';
	            data.e_date = $scope.searchDate.E_DATE.replace(/-/gi,'') || '';

	            // 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
	            if (!checkDate($scope.searchDate.S_DATE.replace(/-/gi,''), $scope.searchDate.E_DATE.replace(/-/gi,''), '')) {
	                return;
	            }
	        }
	        
	        //구분 검색
	        if ($scope.SelectRelayItem !== null && $scope.SelectRelayItem !== '') {
	        	data.RELAY_ITEM_CD = $scope.SelectRelayItem;
	        }
	        
	        console.log($scope.SelectRelayItem);

	        console.log(data);
	        
	        $('#list').jqGrid('setGridParam', {
	        	page : 1,
	            postData: data,
	        }).trigger('reloadGrid', {
	            current: true,
	        }); 
	 };
}