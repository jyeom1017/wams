angular.module('app.system').controller('sysManController', sysManController);


function sysManController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, GridConfig) {
	$('#modalGate').on('click',function(){$('#PicturePopup3').show()})
	let as = null;
	let files = null;
	$scope.isNew = null;
	$scope.MCODE = '0106';
	/*	if ($scope.tabid == undefined)
		$scope.tabid = 1;
	switch ($state.current.name) {
		case '010105':
			$scope.MCODE = '0105';
			$('#tab1').css('display', '');
			break;
		case '010106':
			$scope.MCODE = '0106';
			$scope.tabid = 2;
			$('#tab2').css('display', '');
			break;
		default:
			break;
	}

	// 하수관로 시설 현황
	(function() {
		$scope.sewerageList = [];
		var textList = [ '오수관로(m)', '우수관로(m) 합류관 포함', '차집관로(m)', '배수설비(개소)', '우수토실(개소)', '토구(개소)' ];
		var valueList = [ 'C_2', 'C_3', 'C_4', 'C_5', 'C_6', 'C_7' ];
		var param = {};
		param.crudURL = '/sys/getSewerageStatus.json';
		mainDataService.cmCRUD(param).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.error('errMessage : ' + obj.errMessage);
			} else {
				var temp = obj;
				for (var i = 0; i < temp.length; i++) {
					for (var j = 0; j < 6; j++) {
						$scope.sewerageList.push({
							area : temp[i].C_1,
							text : textList[j],
							value : temp[i][valueList[j]]
						});
					}
				}
			}
		});
	})();
*/
	$scope.file = null;
	$scope.picLoaded = false;
	$scope.readURL = readURL;
	$scope.previewImg0 = '';
	$scope.initMap = null;
	
	$scope.storeUsrGroup = [];

	function readURL(file) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$scope.previewImg0 = e.target.result;
			$scope.picLoaded = true;
			if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

			} else {
				$scope.$apply();
			}
		};
		reader.readAsDataURL(file);
	}
	
	function onFileSelect($files, cmd){
		 //console.log($files);
		files =$files;
		if ($files.length === 0) {
			return;
		}
		
		var str = '\.(' + cmd + ')$';
		var FileFilter = new RegExp(str);
		var ext = cmd.split('|');
		console.log('1'+$files);
		if ($files[0].name.toLowerCase().match(FileFilter)) {
			$scope.pictureValue= $files[0].name
			console.log($scope.pictureValue);
			} else {
			alertify.alert('확장자가 ' + ext + ' 인 파일만 선택가능합니다.');
			$Scope.file = null;
		}
	}
	
	$scope.onFileSelect = onFileSelect;
	
	var userItem = [ {
		'name' : 'E_ID',
		'title' : '아이디',
		'required' : true,
		'maxlength' : 10,
		'minlength' : 4,
		'type':'user_id'
	}, {
		'name' : 'E_ID_DUPL_CHK',
		'title' : '아이디 중복확인',
		// 'type' : 'checkbox',
		'required' : true
	},  {
		'name' : 'E_ENUM',
		'title' : '사번',
		'required' : false,
		'maxlength' : 10
	}, {
		'name' : 'E_NAME',
		'title' : '성명',
		'required' : true,
		'maxlength' : 30,
		'type':'user_name'
	}, {
		'name' : 'E_AFF',
		'title' : '소속',
		'required' : true,
		'maxlength' : 30
	}, {
		'name' : 'E_POS',
		'title' : '직급',
		'required' : true,
		'maxlength' : 2
	}, {
		'name' : 'E_DBIRTH',
		'title' : '생년월일',
		'maxlength' : 10,
		'type' : 'date'
	}, {
		'name' : 'E_ADDR',
		'title' : '주소',
		'maxlength' : 200
	}, {
		'name' : 'E_ZIP',
		'title' : '우편번호',
		'maxlength' : 10
	}, {
		'name' : 'E_JOB',
		'title' : '업무그룹',
		'required' : true,
		'maxlength' : 100
	}, {
		'name' : 'E_PHONE',
		'maxlength' : 20,
		'type' : 'phone'
	}, {
		'name' : 'E_MPHONE',
		'title' : '연락처',
		'maxlength' : 20,
		'type' : 'phone'
	}, {
		'name' : 'E_FAX',
		'maxlength' : 20
	}, {
		'name' : 'E_EMAIL',
		'title' : '이메일',
		'maxlength' : 30,
		'required' : true,
		'type' : 'email'
	}, {
		'name' : 'E_GCODE1',
		'title' : '그룹권한',
		'required' : true,
		'maxlength' : 4
	}, {
		'name' : 'E_INOFF',
		'title' : '상태정보',
		'required' : true,
		'maxlength' : 1
	}, {
		'name' : 'E_POTO',
		'maxlength' : 38
	}, {
		'name' : 'G_UDATE'
	}, {
		'name' : 'G_UUSER'
	}, {
		'name' : 'E_TGRADE',
		'maxlength' : 2
	}, {
		'name' : 'E_SIGN',
		'maxlength' : 38
	}, {
		'name' : 'E_MEMO',
		'maxlength' : 1000
	}, {
		'name' : 'FINAL_CONFIRM_CHK',
		'title' : '변경할 내용 확인',
		// 'type' : 'checkbox',
		'required' : false
	} ];

	//$scope.MCODE = '0105';

	$scope.IsNewRecord = true;

	$scope.UserSearch = function() {
		$('#list').jqGrid('setGridParam', {
			postData : {
				page : 1,
				rows : GridConfig.sizeXL,
				useraff : $scope.SearchInfo.E_AFF || '',
				userjob : $scope.SearchInfo.E_JOB || '',
				username : $scope.SearchInfo.username || '',
				userpos : $scope.SearchInfo.userpos || '',
				authgroup : $scope.SearchInfo.authgroup || ''
			}
		}).trigger('reloadGrid');
	};

	$scope.SearchInfo = {};
	$scope.UserInfo = {
		'FINAL_CONFIRM_CHK' : true
	};

	$scope.delet =function(){
		alertify("a")
	}
	let creUser = function() {
		// alert('신규계정');
		$scope.IsNewRecord = true;
		$scope.UserInfo = {
			'FINAL_CONFIRM_CHK' : true
		};
		$scope.file = null;
		$scope.picLoaded = false;
		$('#E_ID').focus();
	};
	
	$scope.CreateUser = creUser()
	
	function FileUpload(callback) {

		if ($scope.file != null) {
			// if (confirm('사진 파일을 업로드 하시겠습니까?')) {

			// 기존 사진 삭제
			if ($scope.UserInfo.E_POTO != '' && $scope.UserInfo.E_POTO != null) {
				mainDataService.fileDelete(parseInt($scope.UserInfo.E_POTO)).success(function(obj) {
				});
			}

			mainDataService.fileUpload(($scope.file[0].name), 'userPics', $scope.file).success(function(obj) {
				if (!common.isEmpty(obj.errMessage)) {
					alertify.error('errMessage : ' + obj.errMessage);
				} else {
					// console.log(obj);
					if (parseInt(obj.filenumber) > 0) {
						// alert('파일업로드성공');
						$scope.file = null;

						$scope.UserInfo.E_POTO = parseInt(obj.filenumber);

						callback();

					} else {
						alertify.error('사진파일업로드 실패');
						$scope.file = null;
					}
				}
			});

			// } else {
			// $scope.file = null;
			// $scope.picLoaded = false;
			// alertify.error('사진을 삭제합니다.');
			// return;
			// }
		} else {

			callback();
		}
	}

	$scope.UpdateUserInfo = function() {
		var item = $scope.UserInfo2;
		// validation check
		if (!ItemValidation(item, userItem)) {
			return false;
		}

		modalFileUpload(function(e) {
			console.log(item)
			item.E_POTO = e;
			mainDataService.updateEmployee(item).success(function(obj) {
				if (!common.isEmpty(obj.errMessage)) {
					alertify.error('errMessage : ' + obj.errMessage);
				} else {
					// console.log(obj);
					if (obj == '1') {
						alertify.success('수정되었습니다.');
						$scope.UserSearch();
					} else {
						// console.log(obj);
						alertify.error('오류');
					}
				}
			});
		});
		return true;
	};

	$scope.SaveUserInfo = function() {
		var item = $scope.UserInfo;
		console.log(item)
		FileUpload(function() {
			// validation check
			if (ItemValidation(item, userItem))
				mainDataService.addEmployee(item).success(function(obj) {
					if (!common.isEmpty(obj.errMessage)) {
						alertify.error('errMessage : ' + obj.errMessage);
					} else {
						// console.log(obj);
						if (obj == '1') {
							alertify.success('저장되었습니다.');
							$scope.UserSearch();
							$scope.IsNewRecord = false;
							$scope.UserInfo.FINAL_CONFIRM_CHK = false;
						} else {
							// console.log(obj);
							alertify.error('오류');
						}
						// $scope.getUserList();
					}
				});
		});
	};

	$scope.chkIdDupl = function() {
		var userId = $scope.UserInfo.E_ID;
		if ($('#E_ID').val() == '') {
			alertify.alert('','아이디를 입력하세요.');
			$('#E_ID').focus();
			return;
		}
		mainDataService.checkUserId('userId=' + userId).success(function(obj) {
			// console.log(obj);
			if (!obj) {
				alertify.alert('','사용 가능한 아이디입니다.');
				$scope.UserInfo.E_ID_DUPL_CHK = true;
			} else {
				alertify.alert('','이미 사용 중인 아이디입니다.');
				$scope.UserInfo.E_ID_DUPL_CHK = null;
			}
		});
	};

	$timeout(function() {
/*
		// content tab click event
		$('.tabs li button').click(function() {

			$('.tabs li button').removeClass('active'); // 기존선택된 active 클래스 삭제
			$(this).addClass('active'); // 새로 선택된 selected 클래스 생성

			$('.conts_box').hide(); // 기존의 보여진 내용 숨기기
			$('#' + $(this).data('tab')).show();// 새로 선택된 내용 href 연결된 내용 보여주기

			gridResize();
			initPage('List', 'paginate', true, 'TOTP');

			return false; // a기능 차단
		});

		$('.conts_box:not(#' + $('.tabs li button.active').data('tab') + ')').hide();
*/		
		mainDataService.CommunicationRelay("/basic/employee/getEmployeeClass.json").success(function(array) {$scope.names = array;});
		setGrid();
		$scope.UserSearch();
		setDatePicker({yearRange: 'c-100:c+10'});
		gridResize();
	}, 500);

	function UserINOFF(val) {
		if (typeof val == 'undefined' || val == null)
			val = '';
		var ret = val;
		$.each($scope.storeUsrINOFF, function(i, item) {
			if (item.C_SCODE == val) {
				ret = item.C_NAME;
				return false;
			}
		});
		return ret;
	}

	function UserAuthGROUP(val) {
		if (typeof val == 'undefined' || val == null)
			val = '';
		var ret = val;
		$.each($scope.storeUsrAuthGroup, function(i, item) {
			if (item.G_GCODE == val) {
				ret = item.G_GNAME;
				return false;
			}
		});
		return ret;
	}

	function UserPOS(val) {
		if (typeof val == 'undefined' || val == null)
			val = '';
		var ret = val;
		$.each($scope.storeUsrPOS, function(i, item) {
			if (item.C_SCODE == val) {
				ret = item.C_NAME;
				return false;
			}
		});
		return ret;
	}
	function UserGROUP(val) {
		if (typeof val == 'undefined' || val == null)
			val = '';
		var ret = val;
		$.each($scope.storeUsrGroup, function(i, item) {
			if (item.C_SCODE == val) {
				ret = item.C_NAME;
				return false;
			}
		});
		return ret;
	}
	function LoadUserPicure(url, f_num) {
		if (url != '' && f_num > 0) {
			console.log(url)
			$scope.previewImg0 = url;
			// console.log('$scope.previewImg0');
			// console.log($scope.previewImg0);
			$scope.picLoaded = true;
		}
	}

	function setGrid() {
		return pagerJsonGrid({
			grid_id : 'list',
			pager_id : 'listPager',
			url : '/basic/employee/getEmployeeList.json',
			//url : '/asset/getAssetBaseInfoList.json',
			condition : {
				page : 1,
				rows : GridConfig.sizeM,
				/*
				useraff : $scope.SearchInfo.E_AFF || '',
				userjob : $scope.SearchInfo.E_JOB || '',				
				username : $scope.SearchInfo.username || '',
				userpos : $scope.SearchInfo.userpos || '',
				authgroup : $scope.SearchInfo.authgroup || ''
				*/
			},
			rowNum : GridConfig.sizeM,
			colNames : [ '순번', 'ID', '소속','업무그룹', '직급', '성명', '사용자권한', '상태', 'E_ID',  'E_DBIRTH', 'E_ADDR', 'E_ZIP', 'E_EMAIL', 'E_JOB', 'E_PHONE', 'E_MPHONE', 'E_FAX', 'E_EMAIL', 'E_POTO', 'E_GCODE1', 'E_POS', 'E_INOFF', 'E_PIC_URL', 'E_MEMO','LOGIN_STAT' ],
			colModel : [ {
				name : 'RNUM',
				width : 53,
				index : 'Num',
				sorttype : 'int',
				sortable:false,
                resizable: false
			}, {
				name : 'E_ID',
				width : 131,
				index : 'E_ID',
				sorttype : 'date',
				sortable:false,
                resizable: false
			}, {
				name : 'E_AFF',
				width : 143,
				index : 'E_AFF',
				sortable:false,
                resizable: false
			}, {
				name : 'E_JOB',
				width : 143,
				index : 'E_JOB',
				sortable:false,
                resizable: false,
				formatter : function(cval){
					if(cval==null ) return '';
					else return $scope.usrGroupMap[cval];
				}
			}, {				
				name : 'E_POS',
				width : 119,
				index : 'RANK',
				formatter : UserPOS,
				sortable:false,
                resizable: false
			}, {
				name : 'E_NAME',
				width : 113,
				index : 'E_NAME',
				sortable:false,
                resizable: false
			}, {
				name : 'E_GCODE1',
				width : 113,
				index : 'AUTHGROUP',
				formatter : UserAuthGROUP,
				sortable:false,
                resizable: false
			}, {
				name : 'E_INOFF',
				width : 107,
				index : 'STATUS',
				sortable : false,
				formatter : UserINOFF,
                resizable: false
			}, {
				name : 'E_ENUM',
				width : 0,
				index : 'E_ENUM',
				hidden : true
			}, {
				name : 'E_DBIRTH',
				width : 0,
				index : 'E_DBIRTH',
				hidden : true
			}, {
				name : 'E_ADDR',
				width : 0,
				index : 'E_ADDR',
				hidden : true
			}, {
				name : 'E_ZIP',
				width : 0,
				index : 'E_ZIP',
				hidden : true
			}, {
				name : 'E_JOB',
				width : 0,
				index : 'E_JOB',
				hidden : true
			}, {
				name : 'E_EMAIL',
				width : 0,
				index : 'E_EMAIL',
				hidden : true
			}, {
				name : 'E_PHONE',
				width : 0,
				index : 'E_PHONE',
				hidden : true
			}, {
				name : 'E_MPHONE',
				width : 0,
				index : 'E_MPHONE',
				hidden : true
			}, {
				name : 'E_FAX',
				width : 0,
				index : 'E_FAX',
				hidden : true
			}, {
				name : 'E_EMAIL',
				width : 0,
				index : 'E_EMAIL',
				hidden : true
			}, {
				name : 'E_POTO',
				width : 0,
				index : 'E_POTO',
				hidden : true
			}, {
				name : 'E_GCODE1',
				width : 0,
				index : 'E_GCODE1',
				hidden : true
			}, {
				name : 'E_POS',
				width : 0,
				index : 'E_POS',
				hidden : true
			}, {
				name : 'E_INOFF',
				width : 0,
				index : 'E_INOFF',
				hidden : true
			}, {
				name : 'E_PIC_URL',
				width : 0,
				index : 'E_PIC_URL',
				hidden : true
			}, {
				name : 'E_MEMO',
				width : 0,
				index : 'E_MEMO',
				hidden : true
			}, {
				name : 'LOGIN_STAT',
				hidden : true
			}],
			// loadonce:true,
			onSelectRow : function(id) {
				var ret = $('#list').jqGrid('getRowData', id);
				// console.log('name=' + ret.E_NAME + ' id=' + ret.E_ID +
				// '...');
				ret.E_DBIRTH = $scope.displayDateFormat(ret.E_DBIRTH); 
				$scope.UserInfo = ret;
				/*$scope.IsNewRecord = false;*/
				$scope.UserInfo.FINAL_CONFIRM_CHK = false;
				$scope.UserInfo.E_ID_DUPL_CHK = true;
				$scope.file = null;
				$scope.picLoaded = false;
				LoadUserPicure(ret.E_PIC_URL, parseInt(ret.E_POTO));
				$scope.$apply('UserInfo');
				var param = {E_ID:$scope.UserInfo.E_ID};
				mainDataService.updateLogUser(param)
				.success(function(data){
					
				});
				// console.log(typeof parseInt($scope.UserInfo.E_POTO));
				// console.log(parseInt($scope.UserInfo.E_POTO));
			}
			,
			gridComplete : function() {
				var ids = $(this).jqGrid('getDataIDs');
				$(this).setSelection(ids[0]);
				$scope.currentPageNo=$('#list').getGridParam('page');
				$scope.totalCount = $('#list').getGridParam('records');
				if($('#list').jqGrid('getRowData')[0].RNUM=="") $scope.totalCount = 0;
				$scope.$apply();
			}		
		});
	}

	$scope.getCodeList = function() {
		//직급
		mainDataService.getCommonCodeList({
			gcode : '501',
			name : ''
		}).success(function(obj) {
			// console.log(obj);
			$scope.storeUsrPOS = obj;
		});
		//재직상태
		mainDataService.getCommonCodeList({
			gcode : '502',
			name : ''
		}).success(function(obj) {
			// console.log(obj);
			$scope.storeUsrINOFF = obj;
			// $scope.UserInfo.E_INOFF = $scope.storeUsrINOFF[0].C_SCODE;
		});
		// 업무그룹
		mainDataService.getCommonCodeList({
			gcode : '507',
			name : ''
		}).success(function(obj) {
			// console.log(obj);
			$scope.storeUsrGroup = obj;
			$scope.usrGroupMap = {};
			$.each($scope.storeUsrGroup,function(idx,Item){
				$scope.usrGroupMap[Item.C_SCODE] = Item.C_NAME; 	
			});
			// $scope.UserInfo.E_INOFF = $scope.storeUsrINOFF[0].C_SCODE;
		});		
		 mainDataService.getAuthGroupList({}).success(function(obj) {
		 	$scope.storeUsrAuthGroup = obj;
		 });
	};
	
	function pictureRead(file) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$scope.pictureImg0 = e.target.result;
			$scope.pictureLoaded = true;
			if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

			} else {
				$scope.$apply();
			}
		};
		reader.readAsDataURL(file);
	}
	
	$scope.deletePicture = function(){
		$scope.pictureLoaded = false;
		$scope.previewImg0 = '';
		if($scope.isNew == true){
			$scope.UserInfo.E_ID = '';
		}
		console.log($scope.UserInfo.E_ID);
		$scope.changePhoto = true;
		
	}
	
	var chkkin4 = function (str, max){
        if(!max) max = 3; // 글자수를 지정하지 않으면 4로 지정
        var i, j, k, x, y;
        var buff = ["0123456789", "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
        var src, src2, ptn="";

        for(i=0; i<buff.length; i++){
            src = buff[i]; // 0123456789
            src2 = buff[i] + buff[i]; // 01234567890123456789
            for(j=0; j<src.length; j++){
                x = src.substr(j, 1); // 0
                y = src2.substr(j, max); // 0123
                ptn += "["+x+"]{"+max+",}|"; // [0]{4,}|0123|[1]{4,}|1234|...
                ptn += y+"|";
            }
        }
        ptn = new RegExp(ptn.replace(/.$/, "")); // 맨마지막의 글자를 하나 없애고 정규식으로 만든다.

        if(ptn.test(str)) return true;
        return false;
    }
    
	function passValidation(){
		
		if($scope.UserInfo2.E_PASS_NEW==''){
			alertify.alert('Info','신규 비밀번호를 입력하세요');
			return false;
		}
		
		if($scope.UserInfo2.E_PASS_CONFIRM==''){
			alertify.alert('Info','신규 비밀번호를 입력하세요');
			return false;
		}
		
		if($scope.isNew ==false && $scope.UserInfo2.E_PW == $scope.UserInfo2.E_PASS_NEW){
			alertify.alert('Info','신규 비밀번호를 입력하세요');
			return false;
			}
		
		if($scope.UserInfo2.E_PASS_NEW != $scope.UserInfo2.E_PASS_CONFIRM ){
			alertify.alert('Info','비밀번호가 일치하지 않습니다');
			return false;
			}

		if($scope.UserInfo2.E_PASS_NEW == $scope.UserInfo2.E_ID ){
			alertify.alert('Info','아이디와 동일한 비밀번호를 사용할 수 없습니다.');
			return false;
			}		

            var requestInfo = {};
            requestInfo.password = $scope.UserInfo2.E_PASS_NEW ;
            if(requestInfo.password.length == 0) {
                //$('.div_pwd_msg').show();
                return('비밀번호를 입력해주세요.');
                $('[name=loginPw]').focus();
                return false;
            }
            var num = requestInfo.password.search(/[0-9]/g);
            var eng = requestInfo.password.search(/[a-z]/ig);
            var spe = requestInfo.password.search(/[`~!@@#$%^&*|\\\'\";:\/?,.]/gi);
            var speWord = ['password', 'iloveyou', 'letmein', 'qwerty', 'trustno1', 'qazwsx'];
			var reg1 = /[A]{3}|[B]{3}|[C]{3}|[D]{3}|[E]{3}|[F]{3}|[G]{3}|[H]{3}|[I]{3}|[J]{3}|[K]{3}|[L]{3}|[M]{3}|[N]{3}|[O]{3}|[P]{3}|[Q]{3}|[R]{3}|[S]{3}|[T]{3}|[U]{3}|[V]{3}|[W]{3}|[X]{3}|[Y]{3}|[Z]{3}/
			var reg2 = /[a]{3}|[b]{3}|[c]{3}|[d]{3}|[e]{3}|[f]{3}|[g]{3}|[h]{3}|[i]{3}|[j]{3}|[k]{3}|[l]{3}|[m]{3}|[n]{3}|[o]{3}|[p]{3}|[q]{3}|[r]{3}|[s]{3}|[t]{3}|[u]{3}|[v]{3}|[w]{3}|[x]{3}|[y{3}|[z]{3}/
			var reg3 = /[1]{3}|[2]{3}|[3]{3}|[3]{3}|[5]{3}|[6]{3}|[7]{3}|[8]{3}|[9]{3}|[0]{3}/
			var reg4 = /[!]{4}|[@]{3}|[#]{3}|[\$]{3}|[%]{3}|[\^]{3}|[&]{3}|[*]{3}|[(]{3}|[)]{3}|[-]{3}|[+]{3}|[=]{3}|[_]{3}|[|]{3}|[<]{3|[>]{3}}|[\/]{3}|[\"]{4}|[\']{4}|[:]{4}|[;]{4}|[~]{4}|[`]{4}/

            var regexpN = /^[0-9]*$/
            var spcArr = ['<','>','(',')','#','\'','"','/','|','_','-','=','+','\\',';', ' ', '[', ']','{','}'];

            if(chkkin4(requestInfo.password)){
                //$('.div_pwd_msg').show();
                var pwdMessage = '동일한 문자 또는 숫자를 연속해서 3개 이상 사용할 수 없습니다. ';
                return(pwdMessage);
                $('[name=loginPw]').focus();
                return false;
            }
            if(reg1.test(requestInfo.password) || reg2.test(requestInfo.password) || reg3.test(requestInfo.password) || reg4.test(requestInfo.password)){
                //$('.div_pwd_msg').show();
                var pwdMessage = '동일한 문자 또는 숫자를 연속해서 3개 이상 사용할 수 없습니다. ';
                return(pwdMessage);
                $('[name=loginPw]').focus();
                return false;
            }


            for(var i=0;i<spcArr.length;i++){
               var spc = spcArr[i];
               if(requestInfo.password.indexOf(spc) >= 0){
                  //$('#div_pwd_msg').addClass('warning');
                  var pwdMessage = '비밀번호에 < > ( ) # \' " / | - _ = + \\ [ ] { }; 공백은 사용할 수 없습니다.';
                  return(pwdMessage);
                  $('[name=loginPw]').focus();
                  return false;
               }
            }
            if(requestInfo.password.length >= 6 && requestInfo.password.length <= 15){
            }else{
                var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                return(pwdMessage);
                $('[name=loginPw]').focus();
                return false;
            }

            /*console.log("eng",eng);
            console.log("num",num);
            console.log("spe",spe);*/
            if(eng < 0 && num < 0 && spe < 0){
                var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                return(pwdMessage);
                $('[name=loginPw]').focus();
                return false;
            }
            if(eng >= 0){
                if( (eng >= 0) &&  (num >= 0 || spe >= 0) && requestInfo.password.length >= 6 && requestInfo.password.length <= 15){
                    // 정상 , 영어-숫자-특수문자 셋중에 2개는 필수 / 6자이상 15자 이하
                } else {
                    //$('#div_pwd_msg').addClass('warning');
                    var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                    return(pwdMessage);
                    $('[name=loginPw]').focus();
                    //$('#userPwd').focus();
                    return false;
                }
            }else if(num >= 0){
                if( (num >= 0) &&  (eng >= 0 || spe >= 0) && requestInfo.password.length >= 6 && requestInfo.password.length <= 15){
                    // 정상 , 영어-숫자-특수문자 셋중에 2개는 필수 / 6자이상 15자 이하
                } else {
                    //$('#div_pwd_msg').addClass('warning');
                    var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                    return(pwdMessage);
                    $('[name=loginPw]').focus();
                    //$('#userPwd').focus();
                    return false;
                }
            }else if(spe >= 0){
                if( (spe >= 0) &&  (eng >= 0 || num >= 0) && requestInfo.password.length >= 6 && requestInfo.password.length <= 15){
                    // 정상 , 영어-숫자-특수문자 셋중에 2개는 필수 / 6자이상 15자 이하
                } else {
                    //$('#div_pwd_msg').addClass('warning');
                    var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                        return(pwdMessage);
                        $('[name=loginPw]').focus();
                    //$('#userPwd').focus();
                    return false;
                }
            }

        
		//$scope.E_PW = $scope.UserInfo2.E_PASS_NEW
		//$scope.UserInfo2.E_PW = $scope.UserInfo2.E_PASS_NEW
		return true;
	}
	$scope.initLoginFailCnt = function(){
		if($scope.UserInfo2.LOGIN_STAT == "가능") return;
		var param = {E_ID:$scope.UserInfo2.E_ID};
		mainDataService.initLoginFailCnt(param)
		.success(function(data){
			console.log(data);
			$scope.UserInfo2.LOGIN_STAT = "가능";
			$scope.UserSearch();
			alertify.notify('잠금해제', 'success', 5, function(){  
				console.log('dismissed');
			});
			
		});
	}
	
	function modalFileUpload(callback) {
		console.log(files)
		if (files != null) {
			mainDataService.fileUpload((files[0].name), 'userPics', files).success(function(obj) {
				if (!common.isEmpty(obj.errMessage)) {
					alertify.error('errMessage : ' + obj.errMessage);
				} else {
					// console.log(obj);
					if (parseInt(obj.filenumber) > 0) {
						// alert('파일업로드성공');
						files = null;
						
						$scope.UserInfo2.E_POTO = parseInt(obj.filenumber);
						callback($scope.UserInfo2.E_POTO);

					} else {
						alertify.alert('','사진파일 업로드에 실패하였습니다.');
						files = null;
					}
				}
			});
		} else {

			callback();
		}
	}
	//초기화
	function ReSet(){
		$scope.pictureValue = '사진을 등록하십시오.';
		$scope.UserInfo2 = {'FINAL_CONFIRM_CHK' : true};
		files= null;
		isNew = null;
		$scope.pictureSelet=false;
		//$scope.userModalSelet =false;
		$("#userModalSelet").hide();
		$scope.UserInfo2.E_PW='';
		$scope.UserInfo2.E_PASS_NEW='';
		$scope.UserInfo2.E_PASS_CONFIRM='';
		$scope.UserInfo2.E_POTO = '';
		$scope.pictureLoaded = false;
		$scope.changePhoto = false;
	}
	
	$scope.modalSaveUserInfo = function() {
		var item = $scope.UserInfo2;
		modalFileUpload(function(e) {
			// validation check
			//console.log(item);
			//return;
			if (ItemValidation(item, userItem)){
				item.E_POTO=e;
				console.log(e);
				if(item.E_PASS_NEW ==''){
					alertify.alert('Info','비밀번호를 설정하세요',function(){
						
					});
					return;		
				}				
				mainDataService.addEmployee(item).success(function(obj) {
					if (!common.isEmpty(obj.errMessage)) {
						alertify.error('errMessage : ' + obj.errMessage);
					} else {
						// console.log(obj);
						if (obj == '1') {
							alertify.success('저장되었습니다.');
							$scope.UserSearch();
							$scope.IsNewRecord = false;
							ReSet();
							$scope.UserInfo2.FINAL_CONFIRM_CHK = false;
						} else {
							// console.log(obj);
							alertify.error('','오류');
						}
						// $scope.getUserList();
					}
				});
			}
		});
	};
	
	ReSet();
	
	$scope.getCodeList();
	$scope.UserSearch();
	
    $scope.onKeyPress = function(event){
        if (event.key === 'Enter') {
        	$scope.UserSearch();
        }
    }
    
	//신규 메세지
	$scope.NewUserShow= function(){
		//$scope.userModalSelet =true;
		$("#userModalSelet").show();
		$scope.isNew = true;
		$scope.saveMode = 'new';
	}
	//수정 메세지
	$scope.SetUserShow = function(){
		 if( $scope.UserInfo.E_ID =='' || $scope.UserInfo.E_ID == undefined){
			 alertify.alert('','조회를 먼저 수행하십시오.');
			 return;
		 }
		$scope.saveMode = '';
		$scope.isNew = false;
		//$scope.userModalSelet =true;
		$("#userModalSelet").show();
		$scope.UserInfo2 = angular.copy($scope.UserInfo);
		$scope.pictureImg0 = $scope.previewImg0;
		$scope.pictureLoaded=$scope.picLoaded;
		$scope.E_PW = $scope.UserInfo.E_PW;
	}
	$scope.ModalShow = function(e){$(e).show();}
	
	//사진등록
	$scope.pctureOpen=function(){ $scope.pictureSelet=true; }
	$scope.pctureCancelMsg= function(){ $scope.pictureSelet=false; }
	$scope.pctureSelectMsg=function(){ pictureRead(files[0]); $scope.pictureSelet=false; }
	//비밀번호
	$scope.passCancel= function(e){$(e).hide();}
	$scope.passCheck= function(e){
		var validResult = passValidation();
		if(validResult==true){
			if($scope.isNew){
				//신규인경우 
				$scope.UserInfo2.E_PW = $scope.UserInfo2.E_PASS_NEW;
				$(e).hide();
			}else{
				//수정인경우
			var param = {E_ID : $scope.UserInfo2.E_ID , chkPassword:$scope.E_PW };
			mainDataService.checkPassword(param)
			.success(function(data){
				if(data==true|| $scope.isAdmin){
					var param2 = {E_ID:$scope.UserInfo2.E_ID, chkPassword:$scope.E_PW ,E_PW_01 : $scope.UserInfo2.E_PASS_NEW };
					mainDataService.saveNewPassword(param2)
					.success(function(data){
						$scope.UserInfo2.E_PASS_NEW='';
						$scope.UserInfo2.E_PASS_CONFIRM = '';
						alertify.alert('','정상적으로 처리되었습니다.');
						$(e).hide();						
					});					
				}else{
					alertify.alert('','기존 패스워드가 맞지않습니다.');
				}
			});
			}
		}else{
			if(validResult!=false)
				alertify.alert(validResult);
		}
	}
	$scope.resetChkIdUser = function(){
		$scope.UserInfo2.E_ID_DUPL_CHK = null;
	}
	//아이디
	$scope.chkIdUser = function() {
		console.log( $scope.UserInfo2.E_ID)
		var userId = $scope.UserInfo2.E_ID;
		if (userId == undefined || userId == '') {
			alertify.alert('오류','아이디를 입력하세요.',function(){
				$timeout(function(){$('#E_ID').focus()},500);	
			});
			return;
		}
		mainDataService.checkUserId('userId=' + userId).success(function(obj) {
			// console.log(obj);
			if (!obj) {
				alertify.alert('info','사용 가능한 아이디입니다.');
				$scope.UserInfo2.E_ID_DUPL_CHK = true;
			} else {
				alertify.alert('info','이미 사용 중인 아이디입니다.');
				$scope.UserInfo2.E_ID_DUPL_CHK = null;
			}
		});
	};
	//저장
	$scope.alertifySave = function(e) {
		console.log($scope.UserInfo2)
		$scope.UserInfo2.E_NAME = xssFilter($scope.UserInfo2.E_NAME);
		$scope.UserInfo2.E_ID = xssFilter($scope.UserInfo2.E_ID);
		$scope.UserInfo2.E_ADDR = xssFilter($scope.UserInfo2.E_ADDR);
		$scope.UserInfo2.E_EMAIL = xssFilter($scope.UserInfo2.E_EMAIL);
		$scope.UserInfo2.E_DBIRTH = xssFilter($scope.UserInfo2.E_DBIRTH);
		$scope.UserInfo2.E_MPHONE = xssFilter($scope.UserInfo2.E_MPHONE);
		
		//alertify.confirm('저장 확인','작성하신 자료를 저장 하시겠습니까?', function() {
			if(!$scope.isNew){
				//수정				
				$scope.UserInfo.FINAL_CONFIRM_CHK=true;
				if ($scope.changePhoto === true) {
					mainDataService.deleteEmployeePhoto({
						e_id: $scope.UserInfo.E_ID
					})
					.success(function(data){
						
					});	
				}
				if ($scope.UserInfo2.E_EMAIL == '' || $scope.UserInfo2.E_EMAIL == null) {
					alertify.alert("","이메일은 필수항목 입니다.");
					return false;
				}
				if($scope.UpdateUserInfo()==true){
					//$scope.userModalSelet =false;
					$("#userModalSelet").hide();
					ReSet();					
				}
				return;
			}
			else {
				//신규				
				$scope.modalSaveUserInfo()
			}
//			},function(){});
	}
	//취소
	$scope.cancelMsg= function(){
		alertify.confirm('Info','작성 중인 내용이 저장되지 않았습니다. 정말 취소하시겠습니까?'
		,function(){
			//$scope.userModalSelet =false;
			$("#userModalSelet").hide();
			ReSet();
			alertify.alert('Info','취소되었습니다.',function(){
				return;
			});
			return;
		}
		,function(){
			return;
		});
	}
	//삭제
	$scope.DeleteUser = function() {
		if(sessionStorage.getItem('loginId')==$scope.UserInfo.E_ID){
			alertify.alert('Info','본인의 아이디는 삭제할수 없습니다.',function(){
				
			});
			return;
		}
		alertify.confirm('삭제','삭제하시겠습니까?', function(e) {
			if (e) {
				var item = $scope.UserInfo;
				mainDataService.deleteEmployee(item).success(function(obj) {
					if (!common.isEmpty(obj.errMessage)) {
						alertify.error('errMessage : ' + obj.errMessage);
					} else {
						alertify.success('삭제되었습니다.');
						$scope.IsNewRecord = true;
						$scope.UserInfo = {
							'FINAL_CONFIRM_CHK' : true
						};
						$scope.UserSearch();
					}
				});
			} else {
				return;
			}
			creUser()
		},function(){});
	};
	
    // 엑셀 파일 다운로드
    $scope.getExcelDownload = function() {
        const jobType = $state.current.name;
        const levelNm = $scope.levelNm;
        console.log($scope.SearchInfo)
        console.log($state.current.name)
         console.log($scope.levelNm)
        $('#exportForm').find('[name=jobType]').val(jobType);
        $('#exportForm').find('input[name=username]').val($scope.SearchInfo.username);
        $('#exportForm').find('input[name=userjob]').val($scope.SearchInfo.E_JOB);
        $('#exportForm').find('input[name=useraff]').val($scope.SearchInfo.E_AFF);
        $('#exportForm').submit();
    };
}
