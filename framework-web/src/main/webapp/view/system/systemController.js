angular.module('app.system').controller('sysInfoController', sysInfoController);

function sysInfoController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, GridConfig) {
	$scope.tabid = $stateParams.tabid;
	if ($.isEmptyObject($scope.tabid)) {
		$scope.tabid = 1;
	}
	switch ($state.current.name) {
		case '010102':
			$scope.MCODE = '0102';
			break;
		case '010103':
			$scope.MCODE = '0103';
			break;
		default:
			break;
	}

	$scope.btnIsUpdate = function(tab) {
		return (tab.Grid1_Item.EM_NUM != '' && tab.Grid1_Item.EM_NUM != null);
	};

	$scope.isTab = function(tab) {
		return (tab.typeId == '1');
	};

	$scope.specShow = function(tab) {
		return (tab.Grid1_Item.EM_NUM != '' && tab.Grid1_Item.EM_NUM != null);
	};

	$scope.btnIsUpdate2 = function(tab) {
		return (tab.Grid2_Item.ES_SEQ != '' && tab.Grid2_Item.ES_SEQ != null);
	};

	$scope.btnIsUpdate3 = function(tab) {
		return (tab.Grid3_Item.EH_SEQ != '' && tab.Grid3_Item.EH_SEQ != null);
	};

	$scope.TabList = [ {
		title : '하드웨어사양',
		title1 : '하드웨어',
		typeId : 1,
		gridId1 : 'list1',
		gridId2 : 'list2_1',
		gridId3 : 'list3_1',
		pagerId1 : 'pager1',
		pagerId2 : 'pager2_1',
		pagerId3 : 'pager3_1',
		Grid1_Item : {},
		Grid2_Item : {},
		Grid3_Item : {}
	}, {
		title : '소프트웨어사양',
		title1 : '소프트웨어',
		typeId : 2,
		gridId1 : 'list2',
		gridId2 : 'list2_2',
		gridId3 : 'list3_2',
		pagerId1 : 'pager2',
		pagerId2 : 'pager2_2',
		pagerId3 : 'pager3_2',
		Grid1_Item : {},
		Grid2_Item : {},
		Grid3_Item : {}
	}, {
		title : '네트워크사양',
		title1 : '네트워크',
		typeId : 3,
		gridId1 : 'list3',
		gridId2 : 'list2_3',
		gridId3 : 'list3_3',
		pagerId1 : 'pager3',
		pagerId2 : 'pager2_3',
		pagerId3 : 'pager3_3',
		Grid1_Item : {},
		Grid2_Item : {},
		Grid3_Item : {}
	}, {
		title : '부대장비',
		title1 : '부대장비',
		typeId : 4,
		gridId1 : 'list4',
		gridId2 : 'list2_4',
		gridId3 : 'list3_4',
		pagerId1 : 'pager4',
		pagerId2 : 'pager2_4',
		pagerId3 : 'pager3_4',
		Grid1_Item : {},
		Grid2_Item : {},
		Grid3_Item : {}
	} ];

	var g_em_type = '1';
	// $scope.Grid1_Item = {};
	// var List1 = null;

	// init
	$timeout(function() {
		tabInit();
		$.each($scope.TabList, function(index, item) {
			list1Set($scope.TabList[index], item.gridId1, index, item.typeId, item.pagerId1);
		});
		$.each($scope.TabList, function(index, item) {
			list2Set($scope.TabList[index], item.gridId2, index, item.typeId, item.pagerId2);
		});
		$.each($scope.TabList, function(index, item) {
			list3Set($scope.TabList[index], item.gridId3, index, item.typeId, item.pagerId3);
		});
		// gridResize();
		setDatePicker();
		$rootScope.setBtnAuth($scope.MCODE);
	}, 100);

	function tabInit() {
		// $scope.list1_clearItem();
		$('.tabs li button').click(function() {

			$('.tabs li button').removeClass('active'); // 기존선택된 active 클래스 삭제
			$(this).addClass('active'); // 새로 선택된 selected 클래스 생성

			$('.conts_box').hide(); // 기존의 보여진 내용 숨기기
			$('#' + $(this).data('tab')).show();// 새로 선택된 내용 href 연결된 내용 보여주기
			if ($(this).data('tab') == 'tab1') {

			} else {
				var grid_id1 = $scope.TabList[$(this).data('idx')].gridId1;
				var page_id1 = $scope.TabList[$(this).data('idx')].pagerId1;
				initPage(grid_id1, grid_id1 + '_page', true, 'TOTP');

				var grid_id3 = $scope.TabList[$(this).data('idx')].gridId3;
				var page_id3 = $scope.TabList[$(this).data('idx')].pagerId3;
				initPage(grid_id3, grid_id3 + '_page', true, 'TOTP');
				gridResize();
			}

			return false; // a기능 차단
		});

		$('.conts_box:not(#' + $('.tabs li button.active').data('tab') + ')').hide();

	}
	// function gridDelete (sEs_seq) {
	$scope.grid2Delete = function(tabObj, sEs_seq) {

		if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
			alertify.alert('','권한이 없습니다.');
			return;
		}

		// var tabObj = $scope.TabList[tabId-1];
		var msg = typeToMsg('delete');
		var grid1_item = tabObj.Grid1_Item;
		var item = {};
		item.crudURL = '/sys/deleteEquipSpec.json';
		item.es_num = grid1_item.EM_NUM;
		item.es_seq = sEs_seq;

		if (confirm(msg + '하시겠습니까?')) {
			mainDataService.cmCRUD(item).success(function(obj) {
				if (!common.isEmpty(obj.errMessage)) {
					alertify.alert('errMessage : ' + obj.errMessage);
				} else {
					alertify.alert('Data가 ' + msg + '되었습니다.');
					$scope.list2_search(tabObj, grid1_item.EM_NUM);
					// gridResize();
				}
			});
		}
	};
	$scope.grid3Delete = function(tabObj, sEh_seq) {
		console.log('grid3Delete');
		var msg = typeToMsg('delete');
		var grid1_item = tabObj.Grid1_Item;
		var item = {};
		item.crudURL = '/sys/deleteEquipHist.json';
		item.eh_num = grid1_item.EM_NUM;
		item.eh_seq = sEh_seq;

		if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
			alertify.alert('','권한이 없습니다.');
			return;
		}

		if (confirm(msg + '하시겠습니까?')) {
			mainDataService.cmCRUD(item).success(function(obj) {
				if (!common.isEmpty(obj.errMessage)) {
					alertify.alert('errMessage : ' + obj.errMessage);
				} else {
					alertify.alert('Data가 ' + msg + '되었습니다.');
					$scope.list3_search(tabObj, grid1_item.EM_NUM);
					// gridResize();
				}
			});
		}
	};

	/* Grid Start */
	// function grid2DelButton(cellvalue, options, rowObject, tabObj){
	//		
	// var str = '<span class='left'>'+cellvalue+'</span><div
	// id='delBtn'+options.rowId+''></div>';
	// var str1 ='<button class='btn_x delete'
	// ng-click=\'grid2Delete(''+tabObj.typeId+'','+options.rowData.ES_SEQ+')\'
	// >X</button>';
	// $timeout(function(){
	// $('#delBtn'+options.rowId).html(($compile(str1))($scope));
	// },100);
	// return str;
	// };
	function cmDelButton(cellvalue, options, rowObject, tabObj) {
		// var str = '<button class='btn_x delete' id='delBtn'+options.rowId+''
		// >X</button>';
		// var str = '<button class='btn_x delete' id='delBtn' >X</button>';
		var str = '';
		if (!common.isEmpty(options.rowData.ES_SEQ) != '') {
			str = "<button class='btn_x delete btnEdit' id='delBtn'>X</button>";
		}
		return str;
	}

	function grid3DelButton(cellvalue, options, rowObject, tabObj) {
		var str = '';
		if (!common.isEmpty(options.rowData.EH_SEQ) != '') {
			str = "<button class='btn_x delete btnEdit' id='delBtn' >X</button>";
		}
		return str;
	}

	$scope.list1_search = function(tabObj) {
		// list1Set();
		// $('#list1').jqGrid('setGridParam', {
		// search:true,
		// page: 1,//1page로 이동
		// postData:{em_type : g_em_type, s_em_name : $('#s_em_name').val()}
		// });
		// $('#aGrid').jqGrid('clearGridData', true);
		// List1.clearGridData();
		$scope.list1_clearItem(tabObj);
		tabObj.List1.setGridParam({
			datatype : 'json',
			page : 1,
			postData : {
				em_type : tabObj.typeId,
				s_em_name : $('#s_em_name_' + tabObj.typeId).val()
			}
		}).trigger('reloadGrid');
	};

	$scope.list2_search = function(tabObj, sEm_num) {

		var param = tabObj.Grid1_Item;
		param.crudURL = '/sys/getEquipSpecList.json';
		param.es_num = param.EM_NUM;
		mainDataService.cmCRUD(param).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.alert('errMessage : ' + obj.errMessage);
			} else {
				$scope.list2_clearItem(tabObj);
				// tabObj.List2.GridUnload;
				tabObj.List2.clearGridData();
				tabObj.List2.setGridParam({
					datatype : 'local',
					data : obj,
					postData : {
						em_num : sEm_num
					}
				}).trigger('reloadGrid');
				console.log('리스트 2 서치 ');
				console.log(obj.length);
				addTempRowsGrid(tabObj.List2, '7', obj.length);

			}
		});
		// gridResize();
	};

	$scope.list3_search = function(tabObj, isInit) {

		var grid1_item = tabObj.Grid1_Item;
		var em_num = grid1_item.EM_NUM;
		if (isInit == 'init') {
			em_num = -1;
		}

		$scope.list3_clearItem(tabObj);
		tabObj.List3.setGridParam({
			datatype : 'json',
			page : 1,
			postData : {
				eh_num : em_num
			}
		}).trigger('reloadGrid');
	};

	$scope.list1_clearItem = function(tabObj) {
		tabObj.Grid1_Item = {};
		$scope.list2_clearItem(tabObj);
		$scope.list3_clearItem(tabObj);
		$scope.list3_search(tabObj, 'init');
	};
	$scope.list2_clearItem = function(tabObj) {
		tabObj.Grid2_Item = {};
		$('#es_tname' + tabObj.typeId).focus();
		// $scope.list3_clearItem(tabObj);
		// tabObj.List3.clearGridData();
	};
	$scope.list3_clearItem = function(tabObj) {
		tabObj.Grid3_Item = {};
		// $('#eh_hdate' + tabObj.typeId).focus();
	};

	$scope.list1_crud = function(tabObj, url) {
		var msg = typeToMsg(url);
		var item = tabObj.Grid1_Item;
		item.em_type = tabObj.typeId;
		item.em_pos = '1';// code 212
		item.crudURL = '/sys/' + url + 'EquipMaster.json';

		if (url == 'insert' || url == 'update') {

			if (!common.required('em_name' + tabObj.typeId, tabObj.Grid1_Item.EM_NAME, '제품명')) {
				return;
			}
			if (!common.required('em_model' + tabObj.typeId, tabObj.Grid1_Item.EM_MODEL, '모델명')) {
				return;
			}
			if (!common.required('em_maker' + tabObj.typeId, tabObj.Grid1_Item.EM_MAKER, '제조/공급사')) {
				return;
			}
			if (!common.maxlength('em_name' + tabObj.typeId, tabObj.Grid1_Item.EM_NAME, 30)) {
				return;
			}
			if (!common.maxlength('em_model' + tabObj.typeId, tabObj.Grid1_Item.EM_MODEL, 30)) {
				return;
			}
			if (!common.maxlength('em_license' + tabObj.typeId, tabObj.Grid1_Item.EM_LICENSE, 30)) {
				return;
			}
			if (!common.maxlength('em_maker' + tabObj.typeId, tabObj.Grid1_Item.EM_MAKER, 30)) {
				return;
			}
			if (!common.maxlength('em_phone' + tabObj.typeId, tabObj.Grid1_Item.EM_PHONE, 20)) {
				return;
			}
			if (!common.maxlength('em_cdate' + tabObj.typeId, tabObj.Grid1_Item.EM_CDATE.replace(/-/g, ''), 8)) {
				return;
			}

		}
		if (confirm(msg + '하시겠습니까?')) {
			mainDataService.cmCRUD(item).success(function(obj) {

				if (!common.isEmpty(obj.errMessage)) {
					alertify.alert('errMessage : ' + obj.errMessage);
				} else {
					if (url == 'delete') {
						$scope.list1_clearItem(tabObj);
					}
					alertify.alert('Data가 ' + msg + '되었습니다.');
					tabObj.List1.trigger('reloadGrid');
				}
			});
		}
	};
	$scope.list2_crud = function(tabObj, url) {
		var msg = typeToMsg(url);
		var item = tabObj.Grid2_Item;
		var grid1_item = tabObj.Grid1_Item;
		item.crudURL = '/sys/' + url + 'EquipSpec.json';
		item.es_num = grid1_item.EM_NUM;

		if (url == 'insert' || url == 'update') {

			if (!common.required('es_tname' + tabObj.typeId, tabObj.Grid2_Item.ES_TNAME, '항목')) {
				return;
			}
			if (!common.required('em_tnote' + tabObj.typeId, tabObj.Grid2_Item.ES_TNOTE, '용량(사양)')) {
				return;
			}
			if (!common.maxlength('em_cdate' + tabObj.typeId, tabObj.Grid2_Item.ES_TNAME, 20)) {
				return;
			}
			if (!common.maxlength('em_tnote' + tabObj.typeId, tabObj.Grid2_Item.ES_TNOTE, 100)) {
				return;
			}

		}
		if (confirm(msg + '하시겠습니까?')) {
			mainDataService.cmCRUD(item).success(function(obj) {
				console.log(obj);
				if (!common.isEmpty(obj.errMessage)) {
					alertify.alert('errMessage : ' + obj.errMessage);
				} else {
					alertify.alert('Data가 ' + msg + '되었습니다.');
					$scope.list2_search(tabObj, grid1_item.EM_NUM);
					// tabObj.List2.trigger('reloadGrid');
					// gridResize();
				}
			});
		}
	};
	$scope.list3_crud = function(tabObj, url) {
		var msg = typeToMsg(url);
		var item = tabObj.Grid3_Item;
		var grid1_item = tabObj.Grid1_Item;
		item.crudURL = '/sys/' + url + 'EquipHist.json';
		item.eh_num = grid1_item.EM_NUM;

		if (url == 'insert' || url == 'update') {

			if (!common.required('eh_hdate' + tabObj.typeId, tabObj.Grid3_Item.EH_HDATE, '보수일자')) {
				return;
			}
			if (!common.required('eh_desc' + tabObj.typeId, tabObj.Grid3_Item.EH_DESC, '보수내용')) {
				return;
			}
			if (!common.maxlength('eh_hdate' + tabObj.typeId, tabObj.Grid3_Item.EH_HDATE.replace(/-/g, ''), 8)) {
				return;
			}
			if (!common.maxlength('eh_desc' + tabObj.typeId, tabObj.Grid3_Item.EH_DESC, 100)) {
				return;
			}

		}
		if (confirm(msg + '하시겠습니까?')) {
			mainDataService.cmCRUD(item).success(function(obj) {
				if (!common.isEmpty(obj.errMessage)) {
					alertify.alert('errMessage : ' + obj.errMessage);
				} else {
					alertify.alert('Data가 ' + msg + '되었습니다.');
					$scope.list3_search(tabObj, grid1_item.EM_NUM);
					// gridResize();
				}
			});
		}
	};

	function list1Set(tabObj, id, index, typeId, pagerId) {
		// $scope.list1_clearItem();
		var gridInfo1 = {
			grid_id : id,
			pager_id : pagerId,
			rowNum : GridConfig.sizeM,
			url : '/sys/getEquipMasterPagerList.json',
			// condition : $('#frm').serialize(),
			condition : {
				em_type : typeId,
				s_em_name : $('#s_em_name_' + typeId).val()
			},
			rownumbers : false,
			autoWidth : true,
			shrinkToFit : true,
			colNames : [ '순번', '제품번호', '제품명', '설치일자', '모델명', '제조/공급사', 'AS연락처', '설치일자', '라이센스' ],
			colModel : [ {
				name : 'RNUM',
				index : 'RNUM',
				width : 80,
				resizable : false,
				sorttype : 'int'
			}, {
				name : 'EM_NUM',
				index : 'EM_NUM',
				width : 150,
				resizable : false,
				sorttype : 'int',
				key : true
			}, {
				name : 'EM_NAME',
				index : 'EM_NAME',
				resizable : false,
				width : 120,
				sorttype : 'string'
			}, {
				name : 'G_UDATE_TRAN',
				index : 'G_UDATE_TRAN',
				width : 150,
				resizable : false,
				sorttype : 'date',
				formatter : 'date',
				formatoptions : {
					srcformat : 'Y-m-d',
					newformat : 'Y-m-d'
				}
			}, {
				name : 'EM_MODEL',
				index : 'EM_MODEL',
				hidden : true
			}, {
				name : 'EM_MAKER',
				index : 'EM_MAKER',
				hidden : true
			}, {
				name : 'EM_PHONE',
				index : 'EM_PHONE',
				hidden : true
			}, {
				name : 'EM_CDATE',
				index : 'EM_CDATE',
				hidden : true
			}, {
				name : 'EM_LICENSE',
				index : 'EM_LICENSE',
				hidden : true
			} ],
			onSelectRow : function(rowid, status, e) {
				$scope.TabList[index].Grid1_Item = $(this).jqGrid('getRowData', rowid);
				if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

				} else {
					$scope.$apply();
				}

				var selRowData = $(this).jqGrid('getRowData', rowid);

				$scope.list2_search(tabObj, selRowData.EM_NUM);
				$scope.list3_search(tabObj, selRowData.EM_NUM);

			},
			gridComplete : function(rowid, status, e) {
				// To-Do
			}

		};
		$scope.TabList[index].List1 = pagerJsonGrid(gridInfo1);
		// gridResize();

	}

	function list2Set(tabObj, id, index, typeId, pagerId) {

		var gridInfo2 = {
			grid_id : id,
			pager_id : pagerId,
			// url : '/sys/getEquipSpecList.json',
			condition : {
				em_type : typeId,
				em_num : typeId
			},
			rownumbers : false,
			autoWidth : true,
			shrinkToFit : true,
			// width:'600',
			colNames : [ '관리<br>번호', '항목', '용량(사양)', '삭제' ],
			colModel : [ {
				name : 'ES_SEQ',
				resizable : false,
				width : 50,
				index : 'ES_SEQ',
				sortable : false
			}, {
				name : 'ES_TNAME',
				resizable : false,
				index : 'ES_TNAME',
				sortable : false
			}, {
				name : 'ES_TNOTE',
				resizable : false,
				index : 'ES_TNOTE_TEMP',
				sortable : false
			}, {
				name : 'BTN_DEL',
				resizable : false,
				width : 50,
				index : 'BTN_DEL',
				formatter : function(cellvalue, options, rowObject) {
					return cmDelButton(cellvalue, options, rowObject, tabObj);
				},
				sortable : false
			} ],
			tempData : [ {
				ES_SEQ : '',
				ES_TNAME : '',
				ES_TNOTE : '',
				BTN_DEL : ''
			} ],
			isTempData : true,
			tempRowNum : 7,
			onSelectRow : function(rowid, status, e) {

				$scope.TabList[index].Grid2_Item = $(this).jqGrid('getRowData', rowid);

				if (!common.isEmpty($scope.TabList[index].Grid2_Item.ES_SEQ)) {
					if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

					} else {
						$scope.$apply();
					}
				}

			},
			onCellSelect : function(rowid, findex, contents, event) {

				// var cellAttr = $(this).jqGrid('getGridParam', 'colModel');
				// if(cellAttr[findex].name == 'BTN_DEL'){
				// var es_seq = $(this).jqGrid('getCell', rowid,'ES_SEQ');
				// $scope.grid2Delete(tabObj,es_seq);
				// }
			},
			gridComplete : function(rowid, status, e) {
				// To-Do
			},
			beforeSelectRow : function(rowid, ev) {

				if (ev.target.id == 'delBtn') {
					var es_seq = $(this).jqGrid('getCell', rowid, 'ES_SEQ');
					$scope.grid2Delete(tabObj, es_seq);
					return false;
				} else {
					return true;
				}
			}

		};
		// console.log(gridInfo2)
		$scope.TabList[index].List2 = nonPagerGrid(gridInfo2);
		// gridResize();

	}

	function list3Set(tabObj, id, index, typeId, pagerId) {

		var gridInfo3 = {
			grid_id : id,
			pager_id : pagerId,
			url : '/sys/getEquipHistList.json',
			condition : {
				page : 1,
				eh_num : -1
			},
			rowNum : GridConfig.sizeXS,
			autoWidth : true,
			shrinkToFit : true,
			colNames : [ '순번', '이력번호', '보수일자', '보수내용', '삭제' ],
			colModel : [ {
				name : 'RNUM',
				index : 'RNUM',
				resizable : false,
				width : 30,
				sorttype : 'int'
			}, {
				name : 'EH_SEQ',
				index : 'EH_SEQ',
				resizable : false,
				width : 50,
				sorttype : 'int',
				key : true
			}, {
				name : 'EH_HDATE',
				index : 'EH_HDATE',
				resizable : false,
				width : 100,
				sorttype : 'date',
				formatter : 'date',
				formatoptions : {
					srcformat : 'Y-m-d',
					newformat : 'Y-m-d'
				}
			}, {
				name : 'EH_DESC',
				index : 'EH_DESC',
				resizable : false,
				width : 100,
				sorttype : 'string'
			}, {
				name : 'BTN_DEL',
				resizable : false,
				width : 30,
				index : 'BTN_DEL',
				formatter : function(cellvalue, options, rowObject) {
					return grid3DelButton(cellvalue, options, rowObject, tabObj);
				}
			} ],
			onSelectRow : function(rowid, status, e) {
				$scope.TabList[index].Grid3_Item = $(this).jqGrid('getRowData', rowid);
				if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

				} else {
					$scope.$apply();
				}
			},
			onCellSelect : function(rowid, findex, contents, event) {
				// To-Do
			},
			gridComplete : function(rowid, status, e) {
				// To-Do
			},
			beforeSelectRow : function(rowid, ev) {
				if (ev.target.id == 'delBtn') {
					var eh_seq = $(this).jqGrid('getCell', rowid, 'EH_SEQ');
					$scope.grid3Delete(tabObj, eh_seq);
					return false;
				} else {
					return true;
				}
			}

		};
		// $scope.TabList[index].List3 = pagerGrid(gridInfo3);
		$scope.TabList[index].List3 = pagerJsonGrid(gridInfo3);
		// gridResize();
	}

	// //하드웨어 사양 - 보수 이력
	// var asList1 = $('#asList1').jqGrid({
	// datatype: 'local',//로컬그리드이용
	// height: 'auto',
	// // width: '1000',
	// colNames:['순번','이력번호', '보수일자', '보수내용'],
	// colModel:[
	// {name:'Num',width:80,index:'Num', sorttype:'int'},
	// {name:'Product',width:80,index:'Product', sorttype:'date'},
	// {name:'asDay',width:120,index:'asDay'},
	// {name:'asHistory',width:220,index:'asHistory',formatter:delButton}
	// ],
	// rowNum: 13,
	// pager: '#asPager1' // pager 태그 지정
	// }).navGrid('#asPager1',{edit:false,add:false,del:false}); //.navGrid를
	// 설정해야 찾기,새로고침 버튼이 나옴
	// // delete 버튼 생성
	// var asList1_data =
	// [{Num:'1',Product:'001',asDay:'2018-08-16',asHistory:'팬 교체 '},
	// {Num:'2',Product:'002',asDay:'2018-08-16',asHistory:'내장디스크 교체'},
	// {Num:'3',Product:'003',asDay:'2018-08-16',asHistory:'메모리 교체'},
	// {Num:'4',Product:'004',asDay:'2018-08-16',asHistory:'팬 교체'},
	// {Num:'5',Product:'005',asDay:'2018-08-16',asHistory:'내장디스크 교체'},
	// {Num:'6',Product:'006',asDay:'2018-08-16',asHistory:'메모리 교체'},
	// {Num:'7',Product:'007',asDay:'2018-08-16',asHistory:'팬 교체'},
	// {Num:'8',Product:'008',asDay:'2018-08-16',asHistory:'내장디스크 교체'},
	// {Num:'9',Product:'009',asDay:'2018-08-16',asHistory:'메모리 교체'},
	// {Num:'10',Product:'010',asDay:'2018-08-16',asHistory:'팬 교체'},
	// {Num:'11',Product:'011',asDay:'2018-08-16',asHistory:'메모리 교체'},
	// {Num:'12',Product:'012',asDay:'2018-08-16',asHistory:'팬교체'},
	// {Num:'13',Product:'013',asDay:'2018-08-16',asHistory:'메모리 교체'}
	// ];
	//
	// // 스크립트 변수에 담겨있는 json데이터의 길이만큼
	// for(var i=0;i<=asList1_data.length;i++){
	// //jqgrid의 addRowData를 이용하여 각각의 row에 gridData변수의 데이터를 add한다
	// asList1.jqGrid('addRowData',i+1,asList1_data[i]); // <-- 실제로 데이터를 하나씩
	// 넣어주는 부분
	// }

	// var hwVolume1 = $('#hwVolume1').jqGrid({
	// datatype: 'local',//로컬그리드이용
	// height: 'auto',
	// height:'auto',
	// colNames:['항목','용량(사양)'],
	// colModel:[
	// {name:'product',width:80,index:'Num', sorttype:'date'},
	// {name:'volume',width:150,index:'Product', sorttype:'date', formatter :
	// delButton }
	// ],
	// autowidth : 'true'//그리드타이틀
	// //pager: '#hwVPager1' // pager 태그 지정
	// }).navGrid('#hwVPager1',{edit:false,add:false,del:false}); //.navGrid를
	// 설정해야 찾기,새로고침 버튼이 나옴
	//
	// var hwVolume1_data = [{product:'DB 서버',volume:'windows10'},
	// {product:'WEB 서버',volume:'windows10'},
	// {product:'WEB1 서버',volume:'windows10'},
	// {product:'WEB2 서버',volume:'windows10'},
	// {product:'WEB3 서버',volume:'windows10'},
	// {product:'WEB4 서버',volume:'windows10'},
	// {product:'WEB5 서버',volume:'windows10'},
	// {product:'WEB6 서버',volume:'windows10'},
	// {product:'WEB7 서버',volume:'windows10'},
	// {product:'WEB8 서버',volume:'windows10'},
	// {product:'WEB9 서버',volume:'windows10'},
	// ];
	// // 스크립트 변수에 담겨있는 json데이터의 길이만큼
	// for(var i=0;i<=hwVolume1_data.length;i++){
	// //jqgrid의 addRowData를 이용하여 각각의 row에 gridData변수의 데이터를 add한다
	// hwVolume1.jqGrid('addRowData',i+1,hwVolume1_data[i]);
	// }

}
