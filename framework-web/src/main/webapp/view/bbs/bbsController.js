angular.module('app.bbs').controller('bbsPopupController', bbsPopupController);
angular.module('app.bbs').controller('bbsController', bbsController);

function bbsPopupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, $sce, MaxUploadSize) {
    // $scope.grid_item = {}; // 신규, 수정 등 팝업창
    $scope.answerPlaceholder = ""; // 응답 게시글 제목 (질의/응답)
    $scope.popupStoreFileList = []; // 파일 리스트 (신규, 수정 팝업창)
    $scope.grid_item = {};
    $scope.loadFileList_popup = loadFileList_popup;
    // $scope.file2  = [];
    $scope.uploadfilename = "";
    $scope.fileListLimit = '4'; // 파일 최대 업로드 개수
    $scope.MaxUploadSize = MaxUploadSize.size;
    setDatePicker();

    $scope.BBSPopupInfo = [];
    
    // console.log(sessionStorage);
    $scope.$on('$viewContentLoaded', function () {
    	setDatePicker();
    });

    //validation 체크에 사용
    var BbsItem = [{
        'name'     : 'SUBJECT',
        'title'    : '제목',
        'required' : true,
        'maxlength': 100
    }, {
        'name'     : 'CONTENT',
        'title'    : '내용',
        'required' : true,
        'maxlength': 64000
    }];

    //글자수 바이트에 맞게 자르기
    function cutByLen(str, maxByte) { //문자열, 최대바이트
        for (b = i = 0; c = str.charCodeAt(i);) {
            b += c >> 7 ? 2 : 1;
            if (b > maxByte)
                break;
            i++;
        }
        return str.substring(0, i);
    }

    $scope.newBbsResultInfo = function () {
        $scope.ResultInfo = {};
        // $scope.IsNewRecord = true;
        $scope.trustedHtml = "";
    };

    $rootScope.isEditorInit = false;

    function initEditor(){
        console.log('init editor ', $rootScope.isEditorInit);
        //if ($rootScope.isEditorInit ) return;
        // 팝업 영역 보여준 후 에디터 생성
        //$timeout(function(){
        //$(".conts_box .main_conts").show();
        $("#test_id_visible").css("display","block"); // 이 영역이 display="none" 되어서 에디터가 적용 안 되는 문제 있음.
        $rootScope.oEditors = [];
        console.log('에디터 생성');
        nhn.husky.EZCreator.createInIFrame({
            oAppRef      : $rootScope.oEditors,
            elPlaceHolder: "CONTENT_EDITOR",
            sSkinURI     : "./assets/js/smarteditor2/SmartEditor2Skin.html",
            fCreator     : "createSEditor2",
            htParams : {fOnBeforeUnload : function(){}}
            // fOnAppLoad: function () {
                // $rootScope.oEditors.getById["CONTENT_EDITOR"].exec("SET_IR", [""]); // 초기화
            // }
        });
        $rootScope.isEditorInit = true;
        setDatePicker();
        //},100);
    }

    $scope.initEditor = function(){
    	$scope.uploadfilename = "";
    	$scope.file2 = null;
    	$timeout(function(){
        	initEditor();
        	$timeout(function(){
            $rootScope.oEditors.getById["CONTENT_EDITOR"].exec("SET_IR", [""]); // 초기화
            if ($scope.bbsType === 'update') {

                // 댓글 수정일 때
                if ($scope.reply != undefined) {
                    console.log('댓글 수정');
                    console.log($scope.reply.CONTENT);
                    $rootScope.oEditors.getById["CONTENT_EDITOR"].exec("PASTE_HTML", [$scope.reply.CONTENT.toString()]);

                } else {
                    console.log($scope.ResultInfo.CONTENT);
                    $rootScope.oEditors.getById["CONTENT_EDITOR"].exec("PASTE_HTML", [$scope.ResultInfo.CONTENT]);
                }
            }
        	},500);
        },500);    	
    }
    
    $scope.$on('bbsPopupOpen', function (event, data) {
        // if($scope.page_id == 'parentWindow') return;
    	//alert('bbsPopupOpen');
        // console.log($rootScope);
        // console.log(data);
        // console.log($scope.page_id);
        $scope.bbsType = data.bbsType;
        $scope.bbs_group_cd = data.bbs_group_cd;
        $scope.bbs_current_sid = data.bbs_current_sid;
        $scope.parent_bbs_sid = data.parent_bbs_sid;
        $scope.grid_item = data.grid_item;
        $scope.popupStoreFileList = data.storeFileList;
        $scope.ResultInfo = data.ResultInfo;
        $scope.answerPlaceholder = data.answerPlaceholder;
        $scope.MCODE = data.mcode;
        $scope.ResultInfo = data.ResultInfo;
        $scope.reply = data.reply;

        $scope.BBSPopupInfo.push({html:"./view/bbs/bbs_popup.html"});
        
        $('#dialog-bbsPopup').show(); // 팝업 보이게 하기
        //initEditor(); // 에디터 생성
        
        /*
        $timeout(function(){
            $rootScope.oEditors.getById["CONTENT_EDITOR"].exec("SET_IR", [""]); // 초기화
            if ($scope.bbsType === 'update') {

                // 댓글 수정일 때
                if ($scope.reply != undefined) {
                    console.log('댓글 수정');
                    console.log($scope.reply.CONTENT);
                    $rootScope.oEditors.getById["CONTENT_EDITOR"].exec("PASTE_HTML", [$scope.reply.CONTENT.toString()]);

                } else {
                    console.log($scope.ResultInfo.CONTENT);
                    $rootScope.oEditors.getById["CONTENT_EDITOR"].exec("PASTE_HTML", [$scope.ResultInfo.CONTENT]);
                }
            }
        },500);
        */
    });

    // 신규 또는 수정 모달창에서 취소 버튼 클릭 시
    $scope.cancelPop = function () {
        var subject = $scope.grid_item.SUBJECT;
        var contentEditor = $rootScope.oEditors.getById["CONTENT_EDITOR"].getIR(); // 에디터의 내용 얻기
        var isNotEmpty = false; // 제목 또는 내용에 값이 있으면 true, 없으면 false. 값이 있으면 confirm을 띄워야 한다.

        // 질의/응답 게시판의 '응답'이 아닌 모든 경우 (공지사항, 자료실 게시판이거나 질의/응답의 '질의'일때)
        if ($scope.answerPlaceholder === '') {
            //제목 값이 있으면 isNotEmpty = true
            if (subject != '' && subject != undefined) {
                isNotEmpty = true;
            }
        }

        // 내용 값이 있으면 isNotEmpty = true
        // 질의/응답 게시판의 '응답'인 경우는 위의 제목 체크를 건너뛰고 내용만 체크한다. answerPlaceholder = 're: 부모글제목'이 있기 때문
        if (contentEditor != '' && contentEditor != '<p><br></p>' && contentEditor != '<p></p>' && contentEditor != '<br>') {
            isNotEmpty = true;
        }

        var msg = ($scope.bbsType === 'new') ? '작성' : '수정';

        // 제목 또는 내용에 값이 있을 때는 confirm 창을 띄운다.
        alertify.confirm('취소 확인', msg + ' 중인 내용이 저장되지 않았습니다. 정말 취소하시겠습니까?',
            function () {
                $('#dialog-bbsPopup').hide();
                $scope.BBSPopupInfo = [];
                if ($scope.bbsType === 'new') {
                    deleteTemp(); // 작성중인 글일 경우 임시글 삭제
                }
                $scope.grid_item = {}; // 모달창 내용 삭제
                alertify.alert('Info','취소되었습니다.',function(){
                    return;
                });
            },
            function () {
                return;
            }).set('basic', false);
        if ($scope.bbsType === 'new') {
            deleteTemp(); // 작성중인 글일 경우 임시글 삭제
        }
    }

    // 임시글 삭제
    // 신규 모달창이 저장되지 않고 종료(취소 또는 창 닫기)되었을 때 실행
    // M2_BBS에 저장된 임시글 삭제하고, 파일이 있는 경우엔 파일도 삭제함
    function deleteTemp() {
        mainDataService.deleteBbsTempItem({
            bbs_group_cd: $scope.bbs_group_cd,
            bbs_sid     : $scope.bbs_current_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                if (obj > 0) {
                    if ($scope.bbs_group_cd === 'notice' || $scope.bbs_group_cd === 'pbb') {
                        $scope.deleteAllFiles($scope.popupStoreFileList, $scope.bbs_current_sid);
                    }
                    //firstPage("workList"); //목록 새로고침
                    $rootScope.$broadcast('refresh_bbs_list',{});
                }
                $scope.grid_item = {};
                $scope.parent_bbs_sid = "";
                $scope.bbs_current_sid = "";
            }
        });
    }

    // 1. M2_BBS_FILE에서 삭제
    // 2. M2_FILE에서 삭제, 실제 경로에서 삭제
    $scope.deleteAllFiles = function (files, bbs_sid) {
        var files = files; // storeFileList 또는 popupStoreFileList
        console.log(files);
        console.log(bbs_sid);

        if (files != undefined && files.length != 0) {
            mainDataService.deleteBbsFile({ // 1. M2_BBS_FILE에서 삭제
                bbs_group_cd: $scope.bbs_group_cd,
                bbs_sid     : bbs_sid
            }).success(function (data) {
                if (!common.isEmpty(data.errMessage)) {
                    alertify.error('errMessage : ' + data.errMessage);
                } else {
                    // 반복문 돌면서 파일 하나씩 삭제
                    for (var i = 0; i < files.length; i++) {
                        console.log(i);
                        // 2. M2_FILE에서 삭제, 실제 경로에서 삭제
                        mainDataService.fileDelete(files[i].F_NUM).success(function (obj) {
                            if (!common.isEmpty(obj.errMessage)) {
                                alertify.error('errMessage : ' + obj.errMessage);
                            } else {
                            }
                        });
                    }
                }
            });
        }
    };

    // 파일삭제 (X 버튼 눌렀을 때 하나씩 삭제)
    $scope.deleteFile = function (file) {
        // console.log(sessionStorage);
        // console.log($scope.MCODE);

        if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
            alertify.alert('Info','권한이 없습니다.');
            return;
        }

        console.log("file=== ", file);
        console.log($scope.grid_item);

        var bbs_sid = '';

        if ($scope.bbsType === 'new') {
            bbs_sid = $scope.bbs_current_sid;
        } else if ($scope.bbsType === 'update') {
            bbs_sid = $scope.grid_item.BBS_SID;
        }

        console.log("bbs_sid", bbs_sid);
        if (!(bbs_sid > 0)) {
            alertify.alert('','게시물을 선택해주세요');
            return;
        }

        alertify.confirm('삭제 확인', '정말 삭제하시겠습니까?',
            function () {
                //1. 경로에서 파일 삭제, 2. M2_FILE 삭제
                mainDataService.fileDelete(file.F_NUM).success(function (obj) {
                    if (!common.isEmpty(obj.errMessage)) {
                        alertify.error('errMessage : ' + obj.errMessage);
                    } else {
                        console.log(obj);
                        if (parseInt(obj) > 0) {
                            mainDataService.deleteBbsFileOne({ //M2_BBS_FILE 삭제
                                bbs_sid     : bbs_sid,
                                f_num       : file.F_NUM,
                                bbs_group_cd: $scope.bbs_group_cd
                            }).success(function (obj) {
                                if (!common.isEmpty(obj.errMessage)) {
                                    alertify.error('errMessage : ' + obj.errMessage);
                                } else {
                                    if (obj == '1') {
                                        alertify.success('삭제되었습니다.');
                                        loadFileList_popup(bbs_sid);
                                    } else {
                                        alertify.alert('error' + obj);
                                    }
                                }
                            });
                        }
                    }
                });
            }, function () {}).set('basic', false);
    };

    // 파일리스트 가져오기 (팝업)
    function loadFileList_popup(bbs_sid) {
        mainDataService.getBbsFileList({
            bbs_group_cd : $scope.bbs_group_cd,
            bbs_sid: bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log("popupStoreFileList=== ", obj);
                $scope.popupStoreFileList = obj;
            }
        });
    }

    // 팝업에서 저장 or 수정 클릭했을 때
    $scope.saveItem = function () {
        // console.log($scope.popupStoreFileList);
        var msg1 = ($scope.bbsType === 'new')? '저장' : '수정';
        var msg2 = ($scope.bbsType === 'new')? '작성' : '수정';

        var contentEditor = $rootScope.oEditors.getById["CONTENT_EDITOR"].getIR(); // 에디터의 내용 얻기
        // console.log(contentEditor);
        contentEditor = contentEditor.replace(/<(\/p|p)([^>]*)>/gi,""); // 내용의 태그 제거
        console.log(contentEditor);
        $scope.grid_item.CONTENT = contentEditor;
        console.log($scope.grid_item);
        console.log($scope.ResultInfo);

        // content에서 html 태그를 제거한 값
        var removeHtmlTags = $scope.grid_item.CONTENT.replace(/(<([^>]+)>)/ig, "");
        console.log(removeHtmlTags);

        // 유효성 검사
        if (!ItemValidation($scope.grid_item, BbsItem)) {
            return;
        }

        // 자료실 -> 첨부파일 없을 시 리턴
        if ($scope.bbs_group_cd === 'pbb') {
            if ($scope.popupStoreFileList === undefined || $scope.popupStoreFileList.length == 0) {
                alertify.alert('','저장된 첨부파일이 없습니다.');
                return;
            }
        }
        
        //공지사항 - 관리자 필수항목 체크
        if ($scope.bbs_group_cd === 'notice') {
        	if($scope.grid_item.GUBUN == null || $scope.grid_item.GUBUN == ''){ //게시여부를 선택
        		alertify.alert('Info','게시여부는 필수항목입니다.');
                return;
        	}
        	if($scope.grid_item.GUBUN == "공지"){ //게시할경우 게시순서, 게시기간 필수항목
        		if($scope.grid_item.E_VALUE3 == '' || $scope.grid_item.E_VALUE3 == null){
            		alertify.alert('Info','게시순서는 필수항목입니다.');
                    return;
        		}
        		if($scope.grid_item.E_VALUE1 == '' || $scope.grid_item.E_VALUE1 == null){
        			alertify.alert('Info','게시기간은 필수항목입니다.');
                    return;
        		}
        		if($scope.grid_item.E_VALUE2 == '' || $scope.grid_item.E_VALUE2 == null){
        			alertify.alert('Info','게시기간은 필수항목입니다.');
        			return;
        		}

            	// 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
                // if (!checkDate($scope.grid_item.E_VALUE1 , $scope.grid_item.E_VALUE2, '')) {
                //     return;
                // }
        	}
            if(($scope.grid_item.E_VALUE1||'')!='' && ($scope.grid_item.E_VALUE2||'')!=''){
                if(!checkDate($scope.grid_item.E_VALUE1, $scope.grid_item.E_VALUE2, '')){
                    alertify.alert('Info','게시기간 입력값을 확인하세요 . 종료날짜가 시작날짜보다 나중일수 없습니다.');
                    return;
                }
            }
        }

        alertify.confirm(msg1 + ' 확인', msg2 + '하신 자료를 저장 하시겠습니까?',
            // ok
            function () {
                $scope.grid_item.BBS_GROUP_CD = $scope.bbs_group_cd;
                $scope.grid_item.TYPE = 'update'; // type은 기본으로 update를 설정함

                // 신규 글 작성
                if ($scope.bbsType === 'new') {
                    $scope.grid_item.BBS_SID = $scope.bbs_current_sid; //등록한 임시글의 글 번호
                    $scope.grid_item.TEMP_YN = 'N'; //임시글 해제
                    $scope.grid_item.TYPE = 'new';
                    $scope.grid_item.PARENT_BBS_SID = $scope.parent_bbs_sid; // 질의/응답의 '응답'의 경우에는 부모글 번호가 들어감
                    if($scope.bbs_group_cd != 'notice') $scope.grid_item.GUBUN = '';

                    // 질의응답 게시판
                    if ($scope.bbs_group_cd === 'qna') {

                        // 댓글(응답)일 경우
                        if ($scope.grid_item.PARENT_BBS_SID != null && $scope.grid_item.PARENT_BBS_SID != ""
                            && $scope.grid_item.PARENT_BBS_SID != $scope.grid_item.BBS_SID) {
                            $scope.grid_item.GUBUN = '응답';

                            // var subject_content = $scope.grid_item.SUBJECT + " - " + removeHtmlTags;
                            var subject_content = $scope.grid_item.SUBJECT + " - " + removeHtmlTags;
                            subject_content = cutByLen(subject_content, 100);

                            // console.log($scope.ResultInfo.SUBJECT);


                            console.log(subject_content);

                            $scope.grid_item.subject_content = subject_content;
                        } else {
                            $scope.grid_item.GUBUN = '질의';
                        }
                    }
                    // 게시글 수정
                } else if ($scope.bbsType === 'update') {

                    if ($scope.bbs_group_cd === 'qna') {
                        if ($scope.grid_item.GUBUN === '응답') {
                            // var subject_content = $scope.answerPlaceholder.substr(4) + " - " + removeHtmlTags;
                            var subject_content = $scope.ResultInfo.SUBJECT + " - " + removeHtmlTags;
                            subject_content = cutByLen(subject_content, 100);
                            console.log(subject_content);

                            $scope.grid_item.subject_content = subject_content;

                        } else {
                            $scope.grid_item.GUBUN = '질의';
                        }
                    }
                }

                var item = angular.copy($scope.grid_item);
                console.log(item);
                console.log($scope.parent_bbs_sid);
                $rootScope.oEditors.getById["CONTENT_EDITOR"].exec("UPDATE_CONTENTS_FIELD", []);
                item.SUBJECT = xssFilter(item.SUBJECT);
                if ($scope.bbs_group_cd === 'notice') {
                item.evalue1 = item.E_VALUE1;
                item.evalue2 = item.E_VALUE2;
                item.evalue3 = item.E_VALUE3;
                }
                mainDataService.insertBbsItem(item).success(function (obj) { // 신규, 또는 수정
                    if (!common.isEmpty(obj.errMessage)) {
                        alertify.error('errMessage : ' + obj.errMessage);
                    } else {
                        console.log(item);
                        alertify.success('게시글이 ' + msg2 + ' 되었습니다.');
                        if ($scope.bbsType === 'update') {
                            if (item.GUBUN != '응답') {
                                $rootScope.$broadcast('save_edit_bbs',
                                    {
                                        'subject' : xssFilter(item.SUBJECT),
                                        'content' : item.CONTENT,
                                        'popupStoreFileList' : $scope.popupStoreFileList
                                    });

                            } else if (item.GUBUN === '응답') {
                                $rootScope.$broadcast('save_edit_reply',
                                    {
                                        'parent_bbs_sid' : item.PARENT_BBS_SID
                                    });
                            }
                        }
                        //firstPage("workList"); //목록 새로고침
                        $rootScope.$broadcast('refresh_bbs_list',{});
                        $scope.grid_item = {}; //팝업창 리셋
                        $scope.parent_bbs_sid = '';
                        $scope.bbs_sid = '';
                        $('#dialog-bbsPopup').hide();
                        $scope.BBSPopupInfo = [];
                    }
                });
            },
            function () {
            }).set('basic', false);
    };

    // 질의/응답 게시판의 댓글 불러오기
    function getReplyList(parent_bbs_sid) {
        mainDataService.getReplyList({
            bbs_group_cd  : $scope.bbs_group_cd,
            parent_bbs_sid: parent_bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errorMessage)) {
                alertify.error('errMessage : ' + obj.errorMessage);
            } else {
                $scope.ResultInfo = obj[0]; // obj의 첫번째 요소를 ResultInfo에 넣음 (부모글)
                $scope.trustedHtml = $sce.trustAsHtml($scope.ResultInfo.CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
                obj.shift(); // obj에서 첫번째 항목 삭제함 (부모글 삭제하고 obj에는 댓글만 남는다)

                $scope.replyList = obj; // 부모글 제거 후 나머지를 댓글에 넣음

                // console.log($scope.replyList);

                // 댓글 개수만큼 반복문 돌면서 html 태그 적용 가능하게 만든다
                for (i = 0; i < $scope.replyList.length; i++) {
                    $scope.replyList[i].CONTENT = $sce.trustAsHtml($scope.replyList[i].CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
                }
            }
        });
    }

    $scope.fileUpload = function (mode) {
        console.log($scope.fileListLimit);
        if ($scope.popupStoreFileList != undefined && $scope.popupStoreFileList.length >= $scope.fileListLimit) {
            alertify.alert('','첨부파일은 ' + $scope.fileListLimit + '개까지 업로드 가능합니다.');
            return;
        }

        var bbs_sid = $scope.grid_item.BBS_SID;

        // var bbs_current_sid = $scope.bbs_current_sid; //임시글 번호
        if ($scope.grid_item.BBS_SID == null || $scope.grid_item.BBS_SID == '') {
            bbs_sid = $scope.bbs_current_sid; //임시글 번호
        }

        $scope.uploadfilename = ""; // 업로드 하기 전에 빈 값으로 바꾼다.

        if ($scope.file2 == null || $scope.file2.length == 0) {
            alertify.alert('Info','파일을 선택하세요');
            return;
        }

        if ($scope.file2[0].size === 0) {
            alertify.alert('Info','사이즈가 0인 파일은 업로드 할 수 없습니다.');
            return;
        }

        if ($scope.file2[0].size > MaxUploadSize.size) {
            alertify.alert('Info','파일 최대 업로드 용량을 초과하였습니다.');
            return;
        }

        var fileDesc = $scope.file2[0].name;

        if ($scope.file2 != null) {
            // 로딩바
            $('#isLoading').css('display', '');
            var target = document.getElementById('spinnerContainer');
            var spinner = new Spinner().spin(target);

            mainDataService.fileUpload(fileDesc, 'bbs', $scope.file2, '', '').success(function (obj) {
                if (!common.isEmpty(obj.errMessage)) {
                    alertify.error('errMessage : ' + obj.errMessage);
                } else {
                    console.log("m2_file에 insert====", obj);
                    if (parseInt(obj.f_num) > 0) {
                        if (!common.isEmpty(obj.errMessage)) {
                            alertify.alert(obj.errMessage).set('basic', true);
                        } else { 
                            mainDataService.saveBbsFile({ // M2_BBS_FILE에 넣는다
                                bbs_group_cd: $scope.bbs_group_cd,
                                f_num       : obj.f_num,
                                bbs_sid     : bbs_sid
                            }).success(function (data) {
                                $('#isLoading').css('display', 'none');
                                spinner.stop();

                                if (!common.isEmpty(data.errMessage)) {
                                    alertify.error('errMessage : ' + data.errMessage);
                                } else {
                                    alertify.success('파일업로드성공');
                                    $scope.loadFileList_popup(bbs_sid); //현재 글 번호 넘겨서 파일 리스트 불러오기
                                    $scope.file2[0].name = '파일을 입력해주세요.';
                                    console.log($scope.file2);
                                }
                                $files = null;
                                $scope.file2 = null;
                                $scope.fileSelected = false;                                
                            });
                        }
                    } else {
                        alertify.alert('파일업로드 실패').set('basic', true);
                        $('#isLoading').css('display', 'none');
                        spinner.stop();
                    }
                }
            });
        }
    };

    // 파일 업로드 validation check
    // 파일 선택했을 때. 업로드 하기 전
    $scope.onFileSelect = function ($files, cmd) {
        if ($files.length === 0) {
            return;
        }
        var str = '\.(' + cmd + ')$';
        var FileFilter = new RegExp(str);
        var ext = cmd.split('|');
        if ($files[0].name.toLowerCase().match(FileFilter)) {
            $scope.file2 = $files;
            $scope.uploadfilename = $files[0].name; // 파일의 이름을 넣고 화면에서 불러오기 위함.
            // $scope.fileSelected = true;
        } else {
            $files = null;
            $scope.file2 = null;
            $scope.fileSelected = false;
            alertify.alert('Info', '확장자가 ' + ext + ' 인 파일만 선택가능합니다.');
        }
        if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

        } else {
            $scope.$apply();
        }
    };
}

/****** bbs controller ***********/
function bbsController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, $sce) {

    var Category = '';
    var Category_kr = '';

    switch ($state.current.name) {
        case '011501': //공지사항
            $scope.MCODE = '1501';
            //Category = 'notice';
            Category_kr = '공지사항';
            $scope.bbs_group_cd = "notice";
            break;
        case '011502': //질의/응답
            $scope.MCODE = '1502';
            //Category = 'qna';
            Category_kr = '질의/응답';
            $scope.bbs_group_cd = "qna";
            break;
        case '011503': //자료실
            $scope.MCODE = '1503';
            //Category = 'pbb';
            Category_kr = '자료실';
            $scope.bbs_group_cd = "pbb";
            break;
        default:
            break;
    }

    var jopt = '00'; //임의로 설정

    $scope.bbs_sid = '';
    $scope.gubun = '';
    $scope.bbs_current_sid = ''; // 현재 글 번호 (임시글)
    $scope.parent_bbs_sid = ""; // 부모게시글 글 번호 (질의/응답)
    $scope.answerPlaceholder = ""; // 응답 게시글 제목 (질의/응답)
    $scope.ResultInfo = {}; // 화면 우측 게시물 상세
    $scope.grid_item = {}; // 신규, 수정 등 팝업창
    $scope.storeFileList = []; // 파일 리스트
    // $scope.popupStoreFileList = []; // 파일 리스트 (신규, 수정 팝업창)
    $scope.replyList = []; // 화면 우측 게시물 아래 댓글창
    $scope.bbsType = ""; //new(신규) 또는 update(수정)
    $scope.loginId = sessionStorage.getItem('loginId'); // 현재 로그인한 사용자의 아이디


    $scope.$on('$viewContentLoaded', function () {
        console.log($scope.MCODE);
        setGrid();
        gridResize();
        $rootScope.setBtnAuth($scope.MCODE); // 버튼 권한

    });

    $scope.newBbsResultInfo = function () {
        $scope.ResultInfo = {};
        $scope.trustedHtml = "";
    };
    $scope.onKeyPress = function(event){
        if (event.key === 'Enter') {
            $scope.search();
        }
    }
    $scope.$on('refresh_bbs_list',function(event,data){
    	$scope.search(data.opt);
    });
    $scope.search = function (opt) {
        $('#workList').jqGrid('setGridParam', {
            page    : (opt==1)?1:$scope.currentPageNo,
            rows    : GridConfig.sizeM,
        	postData: {
                category: Category,
                subject : $scope.searchSubject || ''
            }
        }).trigger('reloadGrid', {
            current: true
        });
    };

    function setResultInfo(param) {
        // $scope.ResultInfo = param;

        // 질의/응답 게시판
        if ($scope.bbs_group_cd === 'qna') {

            //해당 글번호의 질의, 응답글을 모두 가져와서 질의는 ResultInfo에 넣고 나머지는 replyList에 넣는다
            getReplyList(param.PARENT_BBS_SID);

            // 공지사항, 자료실 게시판
        } else if ($scope.bbs_group_cd === 'notice' || $scope.bbs_group_cd === 'pbb') {
            $scope.ResultInfo = param; // 우측 화면 ResultInfo에 param 넣음
            $scope.trustedHtml = $sce.trustAsHtml($scope.ResultInfo.CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;")); // html 태그 적용하기 위해 ng-bing-html 사용
            loadFileList(param.BBS_SID); // 파일 리스트 불러오기
        }

        if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

        } else {
            $scope.$apply();
        }
    }

    function setGrid() {
        var colNames = [];
        var colModel = [];

        // colNames.push(
        //     '순번', '제목'
        // );
        if($scope.bbs_group_cd === 'notice'){
	        colNames.push(
	            '순번', '글번호', '제목','공지기간','공지기간시작','공지기간끝','공지','순위'
	        );
    	}else{
	        colNames.push(
		            '순번', '글번호', '제목'
	        );        	
        }
        
        if($scope.bbs_group_cd === 'notice'){
        colModel.push(
            {
                name : 'RNUM',
                // width: 53,
                // width: 26,
                width: "40px",
                hidden : true,
                sortable: false ,
                index: 'RNU',
                resizable: false
            }, {
                name : 'BBS_SID',
                // width : 26,
                width : 20,
                index : 'BBS_SID',
                resizable: false
            }, {
                name : 'SUBJECT',
                // width: 247,
                width: 100,
                // autowidth: true,
                align    : 'left',
                index    : 'SUBJECT',
                resizable: false,
                formatter: function (cellValue, options, rowObject) {
                    if (rowObject.BBS_SID != null && rowObject.BBS_SID != "") {
                        if (rowObject.GUBUN == '응답') {
                            return "   re: " + cellValue; // 질의/응답의 '응답' 게시물은 제목 앞에 're: ' 붙여서 출력
                        } else {
                            return cellValue;
                        }
                    } else {
                        return "";
                    }
                }
            },{
            	name : 'NOTICE_DT',
            	width: 40,
            	sortable: false ,
            	resizable: false,
            	formatter:function(cellValue, options, rowObject){
            		if(rowObject.E_VALUE1 != null && rowObject.E_VALUE1 != ''){
            			if(rowObject.E_VALUE2 != null && rowObject.E_VALUE2 != ''){
                    		return rowObject.E_VALUE1 + '~' + rowObject.E_VALUE2;
            			}else {
            				return rowObject.E_VALUE1;
            			}
            		}else{
            			return "";
            		}
            	}
            },{
            	name : 'E_VALUE1',
            	hidden : true
            },{
            	name : 'E_VALUE2',
            	hidden: true
            },{
            	name : 'GUBUN',
            	width: 30,
            	resizable: false,
            	hidden : (!($scope.isAdmin))?true:false
            },{
            	name : 'E_VALUE3',
            	width: 30,
            	resizable: false,
            	hidden : (!($scope.isAdmin))?true:false
            }
        );
        }else{
        	colModel.push(
                    {
                        name : 'RNUM',
                        // width: 53,
                        // width: 26,
                        width: 34,
                        hidden : true,
                        sortable: false ,
                        index: 'RNU',
                    }, {
                        name : 'BBS_SID',
                        // width : 26,
                        width : 34,
                        index : 'BBS_SID',
                        resizable: false
                    }, {
                        name : 'SUBJECT',
                        // width: 247,
                        width: 270,
                        resizable: false,
                        // autowidth: true,
                        align    : 'left',
                        index    : 'SUBJECT',
                        formatter: function (cellValue, options, rowObject) {
                            if (rowObject.BBS_SID != null && rowObject.BBS_SID != "") {
                                if (rowObject.GUBUN == '응답') {
                                    return "   re: " + cellValue; // 질의/응답의 '응답' 게시물은 제목 앞에 're: ' 붙여서 출력
                                } else {
                                    return cellValue;
                                }
                            } else {
                                return "";
                            }
                        }
                    }
                );        	
        }

        // 질의/응답 게시판에는 '구분' 컬럼을 추가함.
        // '제목'의 가로 길이를 줄이고 줄어든 만큼 '구분' 추가
        if ($scope.bbs_group_cd === 'qna') {
            colModel.push(
                {
                    name : 'GUBUN',
                    // width: 53,
                    width: 40,
                    // autowidth: true,
                    index: 'GUBUN',
                    resizable: false
                }, {
                    name  : 'REPLY_COUNT',
                    hidden: true
                }
            );
            colNames.push('구분','댓글갯수');

            //SUBJECT의 길이 수정
            // colModel[1].width = 194; // index=1인 컬럼(제목)
            // colModel[2].width = 195;
            colModel[2].width = 259; // index=2인 컬럼(제목)
        }

        colModel.push(
            {
                name : 'CREATEUSER',
                // width: 70,
                width: 50,
                index: 'CREATEUSER',
                resizable: false,
                hidden : ($scope.bbs_group_cd == 'notice' && sessionStorage.getItem("groupCd") != 'G001')?true:false 
            }, {
                name  : 'CONTENT',
                width : 0,
                index : 'CONTENT',
                hidden: true,
            }, {
                name : 'CREATEDT',
                // width: 100,
                width: 50,  // 132
                index: 'CREATEDT',
                resizable: false,
                hidden : ($scope.bbs_group_cd == 'notice' && sessionStorage.getItem("groupCd") != 'G001')?true:false
            }, {
                name : 'UPDATEDT',
                width: 50,
                index: 'UPDATEDT',
                resizable: false,
                hidden : ($scope.bbs_group_cd == 'notice' && sessionStorage.getItem("groupCd") != 'G001')?true:false
            // }, {
            //     name  : 'BBS_SID', // '글번호'로 화면에 보이게 처리함
            //     width : 0,
            //     index : 'BBS_SID',
            //     hidden: true
            }, {
                name  : 'PARENT_BBS_SID',
                width : 0,
                index : 'PARENT_BBS_SID',
                hidden: true
            }, {
                name  : 'E_ID',
                width : 0,
                index : 'E_ID',
                hidden: true
            }
        );

        colNames.push('작성자', '내용', '작성일', '수정일',
            // '글번호',
            '부모글번호', '작성자아이디');

        return pagerJsonGrid({
            grid_id    : 'workList',
            pager_id   : 'workPager',
            url        : '/bbs/getBBSList.json',
            page        : 1,
            rows        : GridConfig.sizeM,
            condition  : {
                bbs_group_cd: $scope.bbs_group_cd,
                temp_yn     : 'N',
                delete_yn   : 'N'
            },
            rowNum     : GridConfig.sizeM,
            colNames   : colNames,
            colModel   : colModel,
            onSelectRow: function (rowid, status, e) {
                var param = $(this).jqGrid('getRowData', rowid);
                console.log("selectParam === ", param);
                $scope.replyList = {};
                if (parseInt(param.BBS_SID) > 0) {
                    // $scope.bbs_current_sid = param.BBS_SID; //현재 글 번호
                    $scope.bbs_sid = param.BBS_SID;
                    // 부모 글 번호. 공지사항, 자료실 게시판에서는 빈 값이 들어간다.
                    // $scope.bbs_current_sid = param.PARENT_BBS_SID;
                    $scope.gubun = param.GUBUN;

                    // if ($scope.sessionStorage.getItem($scope.MCODE + 'V') == 'true') {
                        updateReadCount(param.BBS_SID); // 조회수 증가
                        setResultInfo(param); // 오른쪽에 글 세팅
                    // }

                } else {
                    $scope.newBbsResultInfo(); // 오른쪽 영역 비어있게 함.
                    $scope.storeFileList = []; //첨부파일 초기화
                }
                if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

                } else {
                    $scope.$apply();
                }
            },
            gridComplete : function() {
            	if($scope.bbs_sid==0 || typeof  $scope.bbs_sid =='undefined'){
    				var ids = $(this).jqGrid('getDataIDs');
   					$(this).setSelection(ids[0]);
            	}
				//alert($('#list').getGridParam('page'));
				$scope.currentPageNo = $('#workList').getGridParam('page');
				$scope.totalCount = $('#workList').getGridParam('records');
				if($('#workList').jqGrid('getRowData')[0].RNUM=="") $scope.totalCount = 0;
				$scope.$apply();

			}
        });

    }

    // 파일리스트 가져오기
    function loadFileList(bbs_sid) {
        mainDataService.getBbsFileList({
            bbs_group_cd: $scope.bbs_group_cd,
            bbs_sid: bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log("storeFileList=== ", obj);
                $scope.storeFileList = obj;
            }
        });
    }

    // 질의/응답 게시판의 댓글 불러오기
    function getReplyList(parent_bbs_sid) {
        mainDataService.getReplyList({
            bbs_group_cd  : $scope.bbs_group_cd,
            parent_bbs_sid: parent_bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errorMessage)) {
                alertify.error('errMessage : ' + obj.errorMessage);
            } else {
                $scope.ResultInfo = obj[0]; // obj의 첫번째 요소를 ResultInfo에 넣음 (부모글)
                $scope.trustedHtml = $sce.trustAsHtml($scope.ResultInfo.CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
                obj.shift(); // obj에서 첫번째 항목 삭제함 (부모글 삭제하고 obj에는 댓글만 남는다)

                $scope.replyList = obj; // 부모글 제거 후 나머지를 댓글에 넣음

                // console.log($scope.replyList);

                // 댓글 개수만큼 반복문 돌면서 html 태그 적용 가능하게 만든다
                for (i = 0; i < $scope.replyList.length; i++) {
                    $scope.replyList[i].CONTENT = $sce.trustAsHtml($scope.replyList[i].CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
                }
            }
        })
    }

    $scope.loadFileList = loadFileList;
    // $scope.loadFileList_popup = loadFileList_popup;

    $scope.newTempItem = function () {
        $scope.newBbsResultInfo(); //item 초기화
        $scope.storeFileList = []; //첨부파일 초기화
        $scope.popupStoreFileList = []; // 팝업 첨부파일 초기화
        $scope.grid_item = {}; //팝업창 초기화
        $scope.replyList = {}; //댓글창 초기화
        $scope.answerPlaceholder = ""; //댓글창의 제목 초기화
        $scope.bbsType = "new";
        $scope.parent_bbs_sid = 0;
        //신규 버튼 클릭 시
        //DB에 임시값을 저장
        mainDataService.insertBbsTempItem({
            BBS_GROUP_CD  : $scope.bbs_group_cd,
            parent_bbs_sid: $scope.parent_bbs_sid // 매퍼 로직이 복잡해서 수정함. 빈 값으로 넘긴다.
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                $scope.newBbsResultInfo(); // resultInfo 초기화
                $scope.bbs_current_sid = obj; // obj : insert한 임시글 번호

                console.log("현재글번호?", $scope.bbs_current_sid);

                //DB 임시값 저장 성공 했을 경우 신규 팝업 띄우기
                $rootScope.$broadcast('bbsPopupOpen',
                    {
                        'bbsType': $scope.bbsType,
                        'bbs_current_sid': $scope.bbs_current_sid,
                        'bbs_group_cd': $scope.bbs_group_cd,
                        'parent_bbs_sid': 0,
                        'grid_item': $scope.grid_item,
                        'mcode': $scope.MCODE
                    });
            }
        });
    };

    $scope.deleteItem = function (reply) {
    	if ($scope.bbs_group_cd === 'notice') {
        	if($scope.ResultInfo.GUBUN == "공지"){ //게시할경우 게시순서, 게시기간 필수항목
        		alertify.alert('Info','공지된 게시글은 삭제 할 수 없습니다.');
                return;
        	}
        }
    	console.log($scope.ResultInfo.REPLY_COUNT);
        // 삭제 권한 체크
        if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
            alertify.alert('Info','권한이 없습니다.');
            return;
        }
        
        if($scope.ResultInfo.GUBUN=='공지'){
        	alertify.alert("Info","공지게시된 게시물은  수정/삭제 할 수 없습니다.<br> 재공지,수정공지 사항을 등록해주세요.");
        	return;
        }        
        
        var writerId = "";
        var gubun = '';

        // 댓글 삭제일때
        if (reply != undefined) {
            $scope.bbs_sid = reply.BBS_SID;
            gubun = reply.GUBUN;
            writerId = reply.E_ID;
            // 원글 삭제일때 (공지사항, 자료실 게시판의 글 삭제 또는 질의/응답 게시판의 부모글 삭제)
        }else {

            if($scope.ResultInfo.REPLY_COUNT > 0){
            	alertify.alert("Info","답변이 있는경우 삭제 할 수 없습니다.");
            	return;
            }
            
        	$scope.bbs_sid = $scope.ResultInfo.BBS_SID;
            writerId = $scope.ResultInfo.E_ID;
            gubun = $scope.ResultInfo.GUBUN;
            // parent_bbs_sid = $scope.ResultInfo.PARENT_BBS_SID;
        }

        if (!($scope.bbs_sid > 0)) {
            console.log($scope.bbs_sid);
            alertify.alert('Info','게시물을 선택해주세요');
            return;
        }

        if (sessionStorage.getItem($scope.MCODE + 'E') === 'true' && $scope.loginId != writerId) {
            alertify.alert('Info','권한이 없습니다.'); // 문구 수정?
            return;
        }

        alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?',
            // ok
            function () {
                // 1. 공지사항, 자료실 => 첨부파일 O
                if ($scope.bbs_group_cd === 'notice' || $scope.bbs_group_cd === 'pbb') {
                    // M2_BBS 삭제
                    mainDataService.deleteBbsItem({
                        bbs_group_cd: $scope.bbs_group_cd,
                        // bbs_sid: $scope.ResultInfo.BBS_SID,
                        bbs_sid       : $scope.bbs_sid,
                        parent_bbs_sid: $scope.ResultInfo.PARENT_BBS_SID,
                        // parent_bbs_sid: parent_bbs_sid,
                        delete_yn: 'Y',
                        // gubun    : $scope.gubun
                        gubun: gubun
                    }).success(function (obj) {
                        if (!common.isEmpty(obj.errMessage)) {
                            alertify.error('errMessage : ' + obj.errMessage);
                        } else {
                            //firstPage("workList"); //목록 새로고침
                        	$rootScope.$broadcast('refresh_bbs_list',{opt:1});
                            alertify.success('삭제되었습니다.');
                            console.log('삭제');
                            $scope.deleteAllFiles($scope.storeFileList, $scope.ResultInfo.BBS_SID); // 파일 삭제 (M2_BBS_FILE, M2_FILE, 실제 경로에서 삭제)
                            $scope.newBbsResultInfo(); // ResultInfo 초기화
                            $scope.storeFileList = {};
                        }
                    });

                    // 2. 질의/응답 => 첨부파일 X
                } else if ($scope.bbs_group_cd === 'qna') {
                    mainDataService.deleteBbsItem({
                        bbs_group_cd: $scope.bbs_group_cd,
                        // bbs_sid: $scope.ResultInfo.BBS_SID,
                        bbs_sid       : $scope.bbs_sid,
                        parent_bbs_sid: $scope.ResultInfo.PARENT_BBS_SID,
                        // parent_bbs_sid: parent_bbs_sid,
                        delete_yn: 'Y',
                        // gubun    : $scope.gubun
                        gubun: gubun
                    }).success(function (obj) {
                        if (!common.isEmpty(obj.errMessage)) {
                            alertify.error('errMessage : ' + obj.errMessage);
                        } else {
                            //firstPage("workList"); //목록 새로고침
                        	$rootScope.$broadcast('refresh_bbs_list',{});
                            alertify.success('삭제되었습니다.');

                            // 부모글 삭제의 경우 우측 ResultInfo, 댓글창 초기화
                            if (reply == undefined) {
                                $scope.newBbsResultInfo();
                                $scope.replyList = {};

                                // 댓글 삭제의 경우 댓글창 새로고침
                            } else {
                                getReplyList($scope.ResultInfo.PARENT_BBS_SID);
                            }
                        }
                    });
                }
            },

            // cancel
            function () {
            }
        ).set('basic', false);
    };

    $scope.$on("save_edit_bbs", function(event, data){
    	console.log(data)
        console.log(data.subject);
        // alert('반영');

        $scope.ResultInfo.SUBJECT = data.subject;
        $scope.trustedHtml = $sce.trustAsHtml(data.content.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
        $scope.storeFileList = data.popupStoreFileList;
    });

    $scope.$on("save_edit_reply", function (event, data) {
        console.log(data);
        getReplyList(data.parent_bbs_sid);
    });

    //수정 클릭 시 게시물 수정 팝업 띄우기
    $scope.updateItemPopup = function (reply) {
    	if ($scope.bbs_group_cd === 'notice') {
        	if($scope.ResultInfo.GUBUN == "공지"){ //게시할경우 게시순서, 게시기간 필수항목
        		//alertify.alert('','공지된 게시글은 수정이 불가합니다.');
                //return;
        	}
        }
    	
        // $scope.ResultInfo = angular.copy();
        var param = angular.copy($scope.ResultInfo);
        $scope.popupStoreFileList = $scope.storeFileList; // 수정 팝업창의 첨부파일에 메인 화면의 첨부파일 넣기
        // $scope.storeFileList = {}; // 첨부파일 초기화
        // $scope.replyList = {}; //댓글창 초기화
        $scope.bbsType = "update";

        var loginId = sessionStorage.getItem('loginId'); // 현재 로그인한 사용자의 아이디

        console.log(reply);
        if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
            alertify.alert('Info','권한이 없습니다.');
            return;
        }

        if (!($scope.isAdmin || loginId === $scope.ResultInfo.E_ID)) {
            alert("222222");
            alertify.alert('Info','권한이 없습니다.'); // 문구 수정?
            return;
        }

        var bbs_sid = "";
        // 댓글일 때
        if (reply != undefined) {
            bbs_sid = reply.BBS_SID;
            if (loginId != reply.E_ID) {
                alertify.alert('Info','권한이 없습니다.');
                return;
            }

            // 댓글 아닐때
        } else {
            if ($scope.ResultInfo.REPLY_COUNT > 0){
            	alertify.alert('Info','답변이 존재하여 질문을 수정 할 수 없습니다.');
                return;
            }
            // bbs_sid = $scope.ResultInfo.BBS_SID;
            bbs_sid = param.BBS_SID;
            $scope.answerPlaceholder = "";
        }

        if (!(bbs_sid > 0)) {
            alertify.alert('Info','게시물을 선택해주세요');
            return;
        }

        if (reply != undefined) {
            $scope.grid_item = reply;
            // $scope.answerPlaceholder = 're: ' + $scope.ResultInfo.SUBJECT;
            $scope.answerPlaceholder = 're: ' + reply.SUBJECT; // 2021-10-22 수정. 댓글 수정 팝업에서 제목 수정

            console.log($scope.answerPlaceholder);
        } else {
            $scope.grid_item = param; //수정 팝업창에 선택된 글의 제목, 내용 넣는다.
            console.log($scope.grid_item);
        }

        $rootScope.$broadcast('bbsPopupOpen', /* bbsUpdatePopupOpen */
            {
                'bbsType': $scope.bbsType,
                'bbs_current_sid': $scope.bbs_current_sid,
                'bbs_group_cd': $scope.bbs_group_cd,
                'parent_bbs_sid': $scope.parent_bbs_sid,
                'grid_item': $scope.grid_item,
                'storeFileList': $scope.storeFileList,
                'answerPlaceholder': $scope.answerPlaceholder,
                'mcode': $scope.MCODE,
                'ResultInfo': angular.copy($scope.ResultInfo),
                'reply': reply
            });
    }

    $scope.insertReply = function (rowid) {
        $scope.parent_bbs_sid = $scope.ResultInfo.BBS_SID;
        $scope.bbsType = "new";

        console.log("부모글번호", $scope.parent_bbs_sid);

        if ($scope.parent_bbs_sid == null || $scope.parent_bbs_sid == "") {
            alertify.alert('Info','게시물을 선택해주세요');
            return;
        }

        if ($scope.ResultInfo.GUBUN != '질의') {
            alertify.alert('Info','댓글 작성은 질의 게시물에만 가능합니다.'); //문구 수정
            return;
        }

        //신규 버튼 클릭 시
        //DB에 임시값을 저장
        mainDataService.insertBbsTempItem({
            BBS_GROUP_CD  : $scope.bbs_group_cd,
            PARENT_BBS_SID: $scope.parent_bbs_sid, //부모글 번호
            // QNA_TYPE      : 'answer' //응답
            gubun: '응답'
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                $scope.bbs_current_sid = obj; // obj : insert한 임시글 번호
                $scope.answerPlaceholder = 're: ' + $scope.ResultInfo.SUBJECT;
                $scope.replyList = {}; //댓글창 초기화

                //DB 임시값 저장 성공 했을 경우 신규 팝업 띄우기
                $scope.grid_item.SUBJECT = $scope.ResultInfo.SUBJECT; //제목은 고정값으로
                $scope.newBbsResultInfo(); //resultInfo 초기화
                $scope.grid_item.CONTENT = '';

                $rootScope.$broadcast('bbsPopupOpen',
                    {
	                    'bbsType': $scope.bbsType,
	                    'bbs_current_sid': $scope.bbs_current_sid,
	                    'bbs_group_cd': $scope.bbs_group_cd,
	                    'parent_bbs_sid': $scope.parent_bbs_sid,
	                    'grid_item': $scope.grid_item,
	                    'answerPlaceholder': $scope.answerPlaceholder,
	                    'mcode': $scope.MCODE,
	                    'ResultInfo': angular.copy($scope.ResultInfo)
                		/*
                        'bbsType': $scope.bbsType,
                        'bbs_current_sid': $scope.bbs_current_sid,
                        'bbs_group_cd': $scope.bbs_group_cd,
                        'parent_bbs_sid': $scope.parent_bbs_sid,
                        'grid_item': $scope.grid_item,
                        'ResultInfo': $scope.ResultInfo,
                        'answerPlaceholder': $scope.answerPlaceholder,
                        'mcode': $scope.MCODE*/
                    });
            }
        });
    }

    function updateReadCount(bbs_sid) {
        mainDataService.updateReadCount({
            bbs_group_cd: $scope.bbs_group_cd,
            bbs_sid     : bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errorMessage)) {
                alertify.error('errMessage : ' + obj.errorMessage);
            } else {
            }
        });
    };

    // 1. M2_BBS_FILE에서 삭제
    // 2. M2_FILE에서 삭제, 실제 경로에서 삭제
    $scope.deleteAllFiles = function (files, bbs_sid) {
        var files = files; // storeFileList 또는 popupStoreFileList
        console.log(files);
        console.log(bbs_sid);

        if (files != undefined && files.length != 0) {
            mainDataService.deleteBbsFile({ // 1. M2_BBS_FILE에서 삭제
                bbs_group_cd: $scope.bbs_group_cd,
                bbs_sid     : bbs_sid
            }).success(function (data) {
                if (!common.isEmpty(data.errMessage)) {
                    alertify.error('errMessage : ' + data.errMessage);
                } else {
                    // 반복문 돌면서 파일 하나씩 삭제
                    for (var i = 0; i < files.length; i++) {
                        console.log(i);
                        // 2. M2_FILE에서 삭제, 실제 경로에서 삭제
                        mainDataService.fileDelete(files[i].F_NUM).success(function (obj) {
                            if (!common.isEmpty(obj.errMessage)) {
                                alertify.error('errMessage : ' + obj.errMessage);
                            } else {
                            }
                        });
                    }
                }
            });
        }
    };

    $scope.ExcelDownload = function(){
    	//alert('출력');
    	console.log($state.current.name);
    	var excel_id = $state.current.name;
   		var group_cd = $scope.bbs_group_cd;
    	//param.put("BBS_GROUP_CD", "notice");param.put("BBS_SID", "64");	param.put("EXCEL_ID", "011501");//공지사항
    	window.open("/excel/downloadExcel.do?EXCEL_ID="+excel_id+"&BBS_GROUP_CD="+group_cd+"&BBS_SID="+ $scope.bbs_sid,"_blank");
    }

}