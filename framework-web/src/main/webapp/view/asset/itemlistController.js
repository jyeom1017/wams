angular.module('app.asset').controller('itemListController', itemListController);

function itemListController($scope, $state, $stateParams, mainDataService, $rootScope, $compile,$filter, $timeout, $interval, ConfigService, YearSet, GridConfig) {
	$scope.MCODE = '0111';
	$scope.currentPageNo = 1;
	$scope.SearchYear  = ''+(new Date()).getFullYear();
	$scope.pic_list = [];
	if($scope.pic_list.length==0){
		$scope.pic_list.push({title:'pic'});
		$scope.pic_list.push({title:'pic'});
		$scope.pic_list.push({title:'pic'});
		$scope.pic_list.push({title:'pic'});
		$scope.pic_list.push({title:'pic'});
	}
	
	$scope.level_path_nm = "";	
	$scope.AssetItemInfo = {};
	$scope.SearchAssetName = "";
	$scope.$on('$viewContentLoaded', function() {
		setGrid();
		setGrid2();
        gridResize();
        setDatePicker();
        $scope.initSearchOptions();
        $rootScope.setBtnAuth($scope.MCODE);
        /*
        mainDataService.getAssetItemInfo({ASSET_PATH_SID:10015})
    	.success(function(data){
    		console.log(data);
    		$scope.ItemList = data; 
    		$timeout(function() {
    			setDatePicker();
    		}, 1000);    		
    	}); 
    	*/       
    });

	function getLevelName(pathCd,level){
		//console.log($rootScope.LevelName);
		//console.log(pathCd,level);
		var LEVEL_CD = pathCd.split('_')[level-1];
		if(LEVEL_CD!=''){
			var LevelName = $rootScope.LevelName[''+(level -1) +'_'+ LEVEL_CD];
			return LevelName;
		}else{
			return "";
		}
	}
	/*
	$scope.loadItemInfo = function(param){
        mainDataService.getAssetItemInfo(param)
        .success(function(data){
        	var list = new Array();
        	console.log(data);
        	$.each(data,function(idx,Item){
        		Item.REQUIRED = (Item.REQUIRED=='Y')?true:false;
        		var dtype = Item.DATA_TYPE.split(':');
            		if(dtype.length>=2){ 
            			Item.maxLen = parseInt(dtype[1]);
                		if(dtype[1].indexOf(".") >= 0 ){
                			var floatLen = dtype[1].split(".");
                			Item.maxLen += parseInt(floatLen[1]) + 1;
                			Item.floatLen = parseInt(floatLen[1]); 
                		}
            		}
        		
        			switch(dtype[0]){
        			case 'number':
        			case 'Num' :
        				if(Item.ITEM_VAL == null) Item.ITEM_VAL="";
        				if(dtype[1].indexOf(".") >= 0 ){
        					Item.ITEM_VAL = (new Intl.NumberFormat('ko-KR', { minimumFractionDigits: Item.floatLen }).format(parseFloat(Item.ITEM_VAL.replace(/,/gi,''))));
        				}else{
        					Item.ITEM_VAL = parseInt(Item.ITEM_VAL.replace(/,/gi,''));	
        				}
        				break;
        			case 'select':
        				Item.REF_CODE_DATA = Item.REF_CODE_DATA.split('/'); 
        				break;
        			case 'select2':
        				console.log(Item.REF_CODE_DATA); 
        				console.log($scope.ComCodeList);
        				if(Item.REF_CODE_DATA.indexOf("::")>0){
        					var REF_CD = Item.REF_CODE_DATA.split("::")[0];
        					var filterObj = {C_MEMO : Item.REF_CODE_DATA.split("::")[1] };
        					Item.REF_CODE_DATA = $filter('filter')($scope.ComCodeList[REF_CD],filterObj);
        				}else{
        					Item.REF_CODE_DATA = $scope.ComCodeList[Item.REF_CODE_DATA];	
        				} 
        				break;        				
        			case 'check':
        				var checkedVal = (Item.ITEM_VAL==''||Item.ITEM_VAL==null)?[]:Item.ITEM_VAL.split(',');
        				Item.CheckedItem = new Array();
        				Item.REF_CODE_DATA = Item.REF_CODE_DATA.split('/');
        				$.each(Item.REF_CODE_DATA,function(idx2,checked){
        					Item.CheckedItem.push(($.inArray(checked,checkedVal)<0)?false:true);
        				});
        				break;
        			case 'check2':
        				Item.CheckedItem = [false];
        				if(Item.ITEM_VAL==''||Item.ITEM_VAL==null)
        					Item.CheckedItem[0]= false;
        				else
        					Item.CheckedItem[0]= true;
        				break;        				
        			case 'string' :
        				if(Item.ITEM_VAL == null) Item.ITEM_VAL="";
        				break;
        			case 'date' :
        				if(Item.ITEM_VAL == null || Item.ITEM_VAL=='0000-00-00') Item.ITEM_VAL="";
        				break;
        			default : 
        				break;
        			}
        		console.log(Item);
        		list.push(Item);
        	});
        	$scope.ItemList = list;
        });
	}
	*/
	
    function setGrid() {
        return pagerJsonGrid({
            grid_id : 'list',
            pager_id : 'listPager',
            url : '/asset/getSelectAssetItemList.json',
            condition : {
                page : 1,
                rows : GridConfig.sizeS
            },
            width : 1400,
            rowNum : 15,
            //colNames : [ '순번','path_sid','sid','asset_cd','asset_nm','경로코드','레벨1','레벨2','레벨3','레벨4','레벨5','레벨6','레벨7','레벨8','레벨9','레벨10','레벨11', '생성일', '작성자'],
            colNames : [ '순번','path_sid','path_cd','sid','자산명','구분','사업장/계통','공정/관로부속시설','공종/대그룹','대그룹/소그룹','소그룹','주소(장소)','설치연도','내용연수','잔존수명','지자체','관리부서','자산코드','생성일','작성자'],
            colModel : [
            {
                name : 'RNUM',
                width : "40px",
                sortable:false,
                resizable: false
            }, {
                name : 'PATH_CD',
                width : 0,
                hidden :true
            }, {
                name : 'ASSET_PATH_SID',
                width : 0,
                hidden :true
            }, {
                name : 'ASSET_SID',
                width : 0,
                hidden :true
            }, {
                name : 'ASSET_NM',
                width : 0,
                sortable:false,
                resizable: false
            },{
                name : 'L3', //구분
                width : 50,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,3);},
                sortable:false,
                resizable: false
            }, {
                name : 'L4', //사업장계통
                width : 100,
                sortable:false,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,4);},
                resizable: false
            }, {
                name : 'L5', //공정
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,5);},
                sortable:false,
                resizable: false
            }, {
                name : 'L6', //공종 , 대그룹
                width : 100,
                sortable:false,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,6);},
                resizable: false
            }, {
            	name : 'L7', //대그룹 , 소그룹
                width : 100,
                sortable:false,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,7);},
                resizable: false
            }, {
                name : 'L8', //소그룹
                width : 100,
                sortable:false,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,8);},
                resizable: false
            }, {
                name : 'PLACE', //설치장소
                width : 100,
                sortable:false,
                resizable: false
            }, {
                name : 'INSTALL_DT', //설치연도
                width : 80,
                sortable:false,
                resizable: false
            }, {
                name : 'LIFE', //내용연수
                width : 60,
                sortable:false,
                resizable: false,
                hidden: true
            }, {
                name : 'REMAIN', //잔존수명
                width : 60,
                sortable:false,
                resizable: false,
                hidden: true
            }, {
            	name : 'L1',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,1);},
                sortable:false,
                resizable: false
            }, {
                name : 'L2',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,2);},
                sortable:false,
                resizable: false
            }, {
                name : 'ASSET_CD',
                width : "250px",
                sortable:false,
                resizable: false
            }, {            	
                name : 'INSERT_DT',
                hidden:true,
                width : 143
            }, {
                name : 'INSERT_ID',
                hidden:true,
                width : 143
            }],

            onSelectRow : function(id) {
            	$scope.AssetItemInfo = $('#list').jqGrid('getRowData', id);
            	$scope.AssetSid = $scope.AssetItemInfo.ASSET_SID;
            	//alert($scope.AssetItemInfo.PATH_CD);
            	var levelPath = $scope.AssetItemInfo.PATH_CD.split('_');
            	//console.log(levelPath);
            	$scope.levelInfoList = [];
            	$.each(levelPath,function(idx,Item){
            		$scope.levelInfoList.push({idx: idx,cd:Item,val:$rootScope.LevelName[''+(idx) +'_'+ Item]}); 
            	});
            	//console.log($scope.levelInfoList);
                //console.log($scope.AssetItemInfo);
                $scope.search_history();
                $scope.search_picture();
                $scope.search_baseinfo();
                $scope.pic_list = []; // 사진대지 초기화
                //alert(id);
                $scope.search_picture();
        		$scope.fn_name = $scope.levelInfoList[2].val;
                var param = {ASSET_PATH_SID:$scope.AssetItemInfo.ASSET_PATH_SID,ASSET_SID: $scope.AssetItemInfo.ASSET_SID};
                $scope.loadItemInfo(param,$scope);
                
        		var param = { ASSET_SID: $scope.AssetItemInfo.ASSET_SID };
        		mainDataService.getAssetGisInfo(param).success(function(data) {
        			//console.log(data);
        			$("#dispLocationInfo2").val(data.GEOM_TEXT);
        			$scope.dispLocationInfo2 = data.GEOM_TEXT || '';
        			if($("#dispLocationInfo2").val!='')
        			$("#dispLocationInfo2Span").show();
        		});
            }
            ,
            gridComplete : function() {
				var ids = $(this).jqGrid('getDataIDs');
				//alert(ids);
				if ($.isEmptyObject($scope.AssetItemInfo)) {
					$(this).setSelection(ids[0]);
				}
				//alert($('#list').getGridParam('page'));
				$scope.currentPageNo=$('#list').getGridParam('page');
				$scope.totalCount = $('#list').getGridParam("records");
				if($('#list').jqGrid('getRowData', ids[0]).RNUM=="") $scope.totalCount = 0;
				$scope.$apply();
				// if ($.isEmptyObject($stateParams.info)) {
				// $('#facility').setSelection(ids[0]);
				// }
			}            
        });
    }
    //기준정보 선택
    $scope.showPopupBaseInfo = function (opt)
    {
    	$scope.level_path_nm = "";
       	$scope.asset_path_sid = 0;
    	$scope.level_path_cd = "";
    	$scope.level_path_list = [];    	
   		$rootScope.$broadcast('loadAssetBaseInfoList',{callbackId:'itemlist_selectAssetBaseInfo',namefilter:$scope.level_path_nm,returnType:'multi'});
    	    	
    }
    
    $scope.$on('itemlist_selectAssetBaseInfo',function(event,data){
    	//console.log(data);
    	if(data.returnType=='multi'){
    		//alert('t1')
    		console.log(data);
    		$scope.base_info_list = angular.copy(data.list);
    	}else{
    		//alert('t2')
    	}
    });
    
    //자산경로선택
    $scope.showPopupLevelPath = function (opt)
    {
    	//alert('showpopup')
    	/*
    	$scope.level_path_nm = "";
    	$scope.asset_path_sid = 0;
    	$scope.level_path_cd = "";
    	$scope.level_path_list = [];
    	*/	
    	$("#dialog-assetlevelpath").show();
    	if(typeof opt=='undefined'){
    		//$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'path'});
    		$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:"",returnType:'path'});
    	}else{
    		$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'multi'});
        	//$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'multi'});
    	}

    	
    }
    $scope.$on('itemlist_selectAssetLevelPath',function(event,data){
    	console.log(data);
    	if(data.returnType=='path'){
    	$scope.asset_path_sid = data.ASSET_PATH_SID;
    	$scope.level_path_cd = data.LEVEL_PATH_CD;
    	$scope.level_path_nm = data.LEVEL_PATH_NM;
    	$scope.searchAssetItem();
    	} else if(data.returnType=='multi'){
    		console.log(data);
    		$scope.level_path_list = angular.copy(data.list);
    	}
    	else{
    		alertify.alert('오류','설정되지 않음');
    	}
    });
    $scope.initSearchOptions=function(){
    	$scope.base_info_list = [];
    	$scope.level_path_list = [];
    	$scope.searchInstallDt = {from:"",to:""};
    	$scope.searchDisposeDt = {from:"",to:""};
    	$scope.searchInitAmt = {from:"",to:""};
    	$scope.SearchAssetState = "";
    	$scope.SearchStateEvalMethod="";
    	$scope.SearchExchangeRequired = "";
    	/*
    	$scope.SearchRemainlife1 = true;
    	$scope.SearchRemainlife2 = false;
    	$scope.SearchRemainlife3 = false;
    	$scope.SearchRemainlife4 = false;
    	*/
    	$("#remain_check01_1").prop("checked",true);
    	$("#remain_check01_2").prop("checked",false);
    	$("#remain_check01_3").prop("checked",false);
    	$("#remain_check01_4").prop("checked",false);
    }
    //$scope.initSearchOptions();
    //신규
    $scope.newItem = function(){
    	//var scope = angular.element($('#dialog-registAssetItemPopup')).scope();
    	//scope.saveMode = 'new';
    	$scope.AssetItemInfo.ASSET_SID =  0;
    	$rootScope.$broadcast('RegistAssetItemPopup',{asset_info:$scope.AssetItemInfo,ItemList:angular.copy($scope.ItemList),callback_Id:'ItemList_Add',callback_Fn : function(){
    		$("#list").setGridParam({
    			datatype : 'json',
    			page : $scope.currentPageNo,
    			rows : GridConfig.sizeS,
    			postData : {

    			}
    		}).trigger('reloadGrid', {
    			current : true
    		});
    	}});    	
    }
    
    //수정
    $scope.editItem = function(){
		//angular.element($('#dialog-registAssetItemPopup')).scope().$parent.saveMode = '';
    	$scope.AssetItemInfo.ASSET_SID =  $scope.AssetSid;
    	$rootScope.$broadcast('RegistAssetItemPopup',{asset_info:$scope.AssetItemInfo,ItemList:angular.copy($scope.ItemList),callback_Id:'ItemList_Update',callback_Fn : function(){
    		$("#list").setGridParam({
    			datatype : 'json',
    			page : $scope.currentPageNo,
    			rows : GridConfig.sizeS,
    			postData : {

    			}
    		}).trigger('reloadGrid', {
    			current : true
    		});
            var param = {ASSET_PATH_SID:$scope.AssetItemInfo.ASSET_PATH_SID,ASSET_SID: $scope.AssetItemInfo.ASSET_SID};
            $scope.loadItemInfo(param,$scope);
    	}});    	
    }
    
    //삭제
    $scope.deleteItem = function(){
    	
    			alertify.confirm('삭제확인','선택한 자산을 삭제하시겠습니까?'
				,function(bt){
    				
    	        	var param = {ASSET_SID:$scope.AssetItemInfo.ASSET_SID};
    	        	mainDataService.deleteAsset(param)
    	        	.success(function(data){
    	        		console.log(data);
    	        		alertify.success('삭제되었습니다.');
    	        		$scope.AssetItemInfo = null;
    	        		$("#list").setGridParam({
    	        			datatype : 'json',
    	        			page : $scope.currentPageNo,
    	        			rows : GridConfig.sizeS
    	        		}).trigger('reloadGrid', {
    	        			current : true
    	        		});
    	        	});    				
				}
    			,function(){
    				alertify.error('삭제취소');
    			});
    	    	
    }
    
    var oldTabId="tab1";
    $scope.showTab = function(tabId,event){
    	if(oldTabId==tabId) return;
    	$("#" + oldTabId).hide();	
    	$("#" + tabId).show();
    	oldTabId = tabId;
    	$(event.currentTarget).parent().parent().find('button').removeClass('active');
    	$(event.currentTarget).addClass('active');
    	//setGrid2();
    	setDatePicker();
    	//alert(tabId);
    	//if(tabId=='tab3')
    	//$scope.search_history();
    	
    	//if(tabId=='tab2')
    	//$scope.search_picture();
    }
    
    $scope.onKeyPress = function(event){
        if (event.key === 'Enter') {
            $scope.searchAssetItem();
        }
    }
    //조회
    $scope.searchAssetItem = function(){
    	
    	var path_list = "";
    	var list = [];
    	var cnt = 0 ;
    	$.each($scope.base_info_list,function(idx,item){
    		if(item.IsChecked){
    			if(cnt>0) path_list += ",";
    			path_list += item.LEVEL_PATH_CD;
    			list.push(item.LEVEL_PATH_CD);
    			cnt ++;
    		}
    	});
    	var remain_life = 0;
    	if($("#remain_check01_1").is(":checked")) remain_life =0;
    	else if($("#remain_check01_2").is(":checked")) remain_life =1;
    	else if($("#remain_check01_3").is(":checked")) remain_life =2;
    	else if($("#remain_check01_4").is(":checked")) remain_life =3;
    	
    	if($scope.searchInstallDt.to !='' && $scope.searchInstallDt.from != ''){
            // 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
            if (!checkDate($scope.searchInstallDt.from, $scope.searchInstallDt.to, '','설치년도 최소값 ~ 최대값을  확인해주세요.')) {
                return;
            }    		
    	}
    	if($scope.searchInitAmt.to !='' && $scope.searchInitAmt.from != ''){
            // 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
            if ($scope.searchInitAmt.from > $scope.searchInitAmt.to) {
            	alertify.alert("오류",'초기투자비 최소값 ~ 최대값을  확인해주세요.',function(){
            		
            	});
                return;
            }    		
    	}
    	if($scope.searchDisposeDt.to !='' && $scope.searchDisposeDt.from != ''){
            // 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
            if (!checkDate($scope.searchDisposeDt.from, $scope.searchDisposeDt.to, '','패기년도 최소값 ~ 최대값을  확인해주세요.')) {
                return;
            }    		
    	}
    	$("#list").setGridParam({
			datatype : 'json',
			page : 1,
			rows : GridConfig.sizeS,
			postData : {
				ASSET_NM : $scope.SearchAssetName,
				ASSET_ADDR : $scope.SearchAssetAddr,
				ASSET_CD : $scope.SearchAssetCd,
				USE_YN : ($scope.AssetUseYn)?'ALL':'',
				ASSET_PATH_SID : $scope.asset_path_sid,
				ASSET_PATH_CD : $scope.level_path_cd,
				BASE_INFO_LIST : path_list,
				//list : list,
				INSTALL_DT_MIN : $scope.searchInstallDt.from,
				INSTALL_DT_MAX : $scope.searchInstallDt.to,
				DISPOSE_DT_MIN : $scope.searchDisposeDt.from,
				DISPOSE_DT_MAX : $scope.searchDisposeDt.to,
				INIT_AMT_MIN : $scope.searchInitAmt.from,
				INIT_AMT_MAX : $scope.searchInitAmt.to,
				ASSET_STATE : $scope.SearchAssetState,
				STATE_EVAL_METHOD :   $scope.SearchStateEvalMethod,
				EXCHANGE_REQUIRED :   $scope.SearchExchangeRequired,
				REMAIN_LIFE_CASE :   remain_life
			}
		}).trigger('reloadGrid', {
			current : true
		});    	
    }
    $scope.IsShowSearchOptions = false;
    $scope.toggleSearchOptions = function(){
    	$scope.IsShowSearchOptions = !($scope.IsShowSearchOptions);
    	if($scope.IsShowSearchOptions == true) {
    		$(".detailBtn").children("i").removeClass("xi-caret-down");
    		$(".detailBtn").children("i").addClass("xi-caret-up");
    	}else{
    		$(".detailBtn").children("i").removeClass("xi-caret-up");
    		$(".detailBtn").children("i").addClass("xi-caret-down");
    	}
    }
    
    $scope.clearAssetPathFilter=function(){
    	$scope.asset_path_sid=0;
    	$scope.level_path_nm='';
    	$scope.level_path_cd='';
    }
    $scope.AssetPathCdUpLevel = function(){
    	$scope.asset_path_sid=0;
    	var lcd = $scope.level_path_cd.split('_');
    	var lnm = $scope.level_path_nm.split('>');
    	if(lnm.length==0){
    		$scope.level_path_cd = "";
    		$scope.level_path_nm = "";
    		return; 
    	}
    	var cnt = lnm.length;
    	var path_cd = '';
    	var path_nm = '';
    	cnt --;
    	for(levelCd in lcd){
    		if(levelCd>=(cnt-1)) break; 
    		if(levelCd>0){
    			path_cd += '_' ;
    		}
    		path_cd += lcd[levelCd];
    		path_nm += '>' + lnm[''+(parseInt(levelCd)+1)];
    	}
    	$scope.level_path_nm = path_nm;
    	$scope.level_path_cd = path_cd;
    }
    $scope.$on('$viewContentLoaded', function () {
    	/*
    	$("#tab1_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab2_btn, #tab3_btn").removeClass("active");
    		$("#tab1").show();
    		$("#tab2, #tab3").hide();
    	});
    	$("#tab2_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab1_btn, #tab3_btn").removeClass("active");
    		$("#tab2").show();
    		$("#tab1, #tab3").hide();
    	});
    	$("#tab3_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab2_btn, #tab1_btn").removeClass("active");
    		$("#tab3").show();
    		$("#tab1, #tab2").hide();
    	});
    	*/
    });

    function setGrid2() {
        return pagerGrid({
            grid_id : 'list_history',
            pager_id : 'list_historyPager',
            url : '/asset/getSelectAssetHistory.json?AssetSid='+($scope.AssetItemInfo.ASSET_SID|0),
            condition : {
                page : 1,
                rows : GridConfig.sizeS
            },
            rowNum : GridConfig.sizeS,
            colNames : [ '순번','작업목록','기간','내용','작성자','작성일'],
            colModel : [
            {
                name : 'RNUM',
                width : "40px",
                sortable:false,
                resizable: false
            }, {
                name : 'NAME',
                width : 100,
                hidden :false,
                sortable:false,
                resizable: false
            }, {
                name : 'TERMS',
                width : 0,
                hidden :false,
                resizable: false,
                sortable:false,
                formatter:function(cellvalue,options,rowdata,action){
                	if(rowdata.RNUM==null) return '';
                	else return (rowdata.START_DT + '~' + rowdata.END_DT);
                	}
            }, {
                name : 'C_MEMO',
                width : 0,
                hidden :false,
                sortable:false,
                resizable: false
            }, {
                name : 'INSERT_ID',
                width : 0,
                hidden :false,
                sortable:false,
                resizable: false
            }, {
                name : 'INSERT_DT',
                width : 0,
                hidden :false,
                sortable:false,
                resizable: false
            }],

            onSelectRow : function(id) {
                console.log($('#list_history').jqGrid('getRowData', id));
                
            }
        });
    }
    $scope.getLevelName1 = function(level_cd,step){
    	if(typeof $scope.SelectedAssetBaseInfo=='undefined') return '';
    	//console.log($scope.SelectedAssetBaseInfo);
    	if($scope.SelectedAssetBaseInfo.FN_GBN=='N' && step >=5 ) step=step-1;
    	return $rootScope.LevelName[''+(step -1) +'_'+ level_cd];
    };
    
    $scope.search_option = 1;
    $scope.search_history_option = function(option){
    	$scope.search_option = option;
    }
    
    $scope.search_baseinfo = function(){
    	
    	var param ={};
    	if($scope.levelInfoList[2].cd=='N'  ){
    		param=	{	
        			FN_GBN: $scope.levelInfoList[2].cd, 
        			WORK_GBN: $scope.levelInfoList[4].cd,
        			ASSET_GBN1: $scope.levelInfoList[5].cd,
       				ASSET_GBN2: $scope.levelInfoList[6].cd
        		};    		
    	}else{
    		param=	{	
        			FN_GBN: $scope.levelInfoList[2].cd, 
        			WORK_GBN: $scope.levelInfoList[5].cd,
        			ASSET_GBN1: $scope.levelInfoList[6].cd,
       				ASSET_GBN2: $scope.levelInfoList[7].cd
        		};    		
    	}
    	
    	var strParam = "";
    	var cnt = 0;
    	$.each(param,function(idx,Item){
    		if(cnt >0 ) strParam  += '&'; 
    		strParam  += idx +'=' + Item;
    		cnt ++;
    	});
    	
    	mainDataService.getAssetBaseInfo(strParam)
    	.success(function(data){
    		console.log(data);
    		$scope.SelectedAssetBaseInfo = data.rows[0];
    	});    	
    }
    
    $scope.search_picture = function(){
    	mainDataService.getAssetPic({ASSET_SID:$scope.AssetItemInfo.ASSET_SID|0})
    	.success(function(data){
    		console.log(data);
    		$scope.pic_list = data;
    	});
    }
    
    $scope.search_history = function(){
    	if($scope.search_option==1){
    		if($scope.SearchYear==""){
    			//alertify.alert('오류','기간을 입력하 세요');
    			//$("#selectYear").focus();
    			//return;
    			$scope.StartDt="";
    			$scope.EndDt="";
    		}else{
            	$scope.StartDt = $scope.SearchYear + "0101"; 
               	$scope.EndDt =  $scope.SearchYear + "1231";
    		}           	
    	}else{
    		if($("#start_dt").val()==""){
    			alertify.alert('오류','기간을 입력하 세요');
    			$("#start_dt").focus();
    			return;
    		}
    		if($("#end_dt").val()==""){
    			alertify.alert('오류','기간을 입력하 세요');
    			$("#end_dt").focus();
    			return;
    		}
        	$scope.StartDt = $("#start_dt").val().replace(/-/gi,''); 
           	$scope.EndDt =  $("#end_dt").val().replace(/-/gi,'');    		
    	}
 
    	$("#list_history").setGridParam({
    		url : '/asset/getSelectAssetHistory.json?AssetSid='+$scope.AssetItemInfo.ASSET_SID,
			datatype : 'json',
			page : 1,
			postData : {
				ASSET_SID : $scope.AssetItemInfo.ASSET_SID,
				START_DT : $scope.StartDt,
				END_DT : $scope.EndDt
			}				
		}).trigger('reloadGrid', {
			current : true
		});    	
    	
    }
    /*
    $scope.uploadPic = function (picInfo){
    	$rootScope.callback_savePicture = function(obj){
    		console.log(picInfo);
    		console.log(obj);
    		var param = {AssetSid: picInfo.ASSET_SID,F_NUM:obj.f_num,PHOTO_NUM : picInfo.SEQ, DESCRIPTION : obj.F_TITLE }; 
    		mainDataService.updateAssetPic(param)
    		.success(function(data){
    			console.log(data);
    			$scope.search_picture();
    		});
    	}
    	$('#dialog_commonImageFileDrag2').dialog();
    }
    
    $scope.showPic = function(pic){
    	$rootScope.$broadcast("popupShowPicture",{URL:pic.F_PATH,DESC:pic.DESCRIPTION,pic,callbackDeletePic:function(data){
    		var picInfo = data.pic;
    		var param = {AssetSid: picInfo.ASSET_SID,F_NUM:0,PHOTO_NUM : picInfo.SEQ, DESCRIPTION : '' }; 
    		mainDataService.deleteAssetPic(param)
    		.success(function(data){
    			console.log(data);
    			$scope.search_picture();
    		});    		
    	},
    	updateDlgId : '#dialog_commonImageFileDrag2',
    	callbackUpdatePic : function(data){
    		var picInfo = data;
				console.log(picInfo);
				console.log(data);
				var param = { AssetSid: picInfo.ASSET_SID, F_NUM: data.f_num, PHOTO_NUM: picInfo.SEQ, DESCRIPTION: data.F_TITLE };
				mainDataService.updateAssetPic(param).success(function(data) {
					console.log(data);
				});
			}
			});
		}
	*/
    $scope.uploadPic = function (picInfo){
        console.log(picInfo);
        $rootScope.$broadcast("showImageUploadPopup", {
            'picInfo' : picInfo //ASSET_SID, SEQ
            }
        );

        $rootScope.callback_savePicture = function(obj){
            console.log(picInfo);
            console.log(obj);
            
    		var param = {AssetSid: picInfo.ASSET_SID,F_NUM:obj.f_num,PHOTO_NUM : picInfo.SEQ, DESCRIPTION : obj.F_DESC }; 
    		mainDataService.updateAssetPic(param)
    		.success(function(data){
    			console.log(data);
    			$scope.search_picture();
    		});            
        }
        $scope.image_popupInfo = [];
        $('#dialog-imageUpload-popup2').show();
    }
    
    $scope.showPic = function(pic){
    	$rootScope.$broadcast("popupShowPicture",{URL:pic.F_PATH,DESC:pic.F_DESC,pic
    		,callbackDeletePic:function(data){
    			//debugger;
        		var picInfo = data.pic;
        		var param = {AssetSid: picInfo.ASSET_SID,F_NUM:0,PHOTO_NUM : picInfo.SEQ, DESCRIPTION : '' }; 
        		mainDataService.deleteAssetPic(param)
        		.success(function(data){
        			console.log(data);
        			alertify.success('삭제되었습니다.');	
        			$scope.search_picture();
        		});    		
        	},
        	updateDlgId : '#dialog-imageUpload-popup2',
        callbackUpdatePic:function(data){
        	//debugger;
        	var picInfo = data;
            if(picInfo.f_num>0 && typeof picInfo.pic !='undefined' ){
            	picInfo.ASSET_SID = picInfo.pic.ASSET_SID;
            	picInfo.PHOTO_NUM = picInfo.pic.PHOTO_NUM;
            }        	
    		var param = {AssetSid: picInfo.ASSET_SID,F_NUM:picInfo.f_num,PHOTO_NUM : picInfo.PHOTO_NUM, DESCRIPTION : picInfo.F_DESC };
    		
        		mainDataService.updateAssetPic(param)
        		.success(function(data){
        			console.log(data);
        			$scope.search_picture();
        		});
            $('#dialog-imageUpload-popup2').hide();
        }
      });
    }
    
    
	$scope.ShowAssetItemPopup = function() {
		//010102
		//console.log()
		window.open('/excel/downloadExcel.do?EXCEL_ID=' + $state.current.name + '&ASSET_SID=' + $scope.AssetSid, '_blank');
		//alert('자산대장');
		/*$scope.AssetItemInfo.ASSET_SID = $scope.AssetSid;
    console.log($scope.AssetItemInfo);
    var param = {ASSET_PATH_SID:$scope.AssetItemInfo.ASSET_PATH_SID,ASSET_SID: $scope.AssetItemInfo.ASSET_SID,AssetItemInfo: $scope.AssetItemInfo,ItemList:angular.copy($scope.ItemList)};
  $rootScope.$broadcast('ShowAssetItemPopup',param);*/
	};

	$scope.ShowAssetGisEditPopup = function() {
		var param = { ASSET_SID: $scope.AssetItemInfo.ASSET_SID };
		mainDataService.getAssetGisInfo(param).success(function(data) {
			console.log(data);
			$("#dispLocationInfo2").val(data.GEOM_TEXT);
			$scope.dispLocationInfo2 = data.GEOM_TEXT || '';
		$rootScope.$broadcast('showGisPopup', {
			ASSET_INFO : data,
			gisPopupMode : 'gisEdit',
			init: function() {
				var layer_name = data.LAYER;
				var ftr_idn = '\'' + data.FTR_IDN + '\'';
				
				switch(data.LAYER_CD){
				case "FA" : 
					$("#editTarget").val("ams:wtl_fire_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("소방시설");
					break;
				case "F6":
					$("#editTarget").val("ams:wtl_flow_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("유량계");
					break;
				case "F8":
					$("#editTarget").val("ams:wtl_manh_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("상수맨홀");
					break;
				case "FB":
					$("#editTarget").val("ams:wtl_meta_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("수도미터");
					break;
				case "P4":
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA004':LineString");
					$("#dispEditTargetNm").text("배수관로");
					break;
				case "P3":
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA003':LineString");
					$("#dispEditTargetNm").text("송수관로");
                	break;
				case "P2":
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA002':LineString");
					$("#dispEditTargetNm").text("도수관로");
            		break;
				case "P1":
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA001':LineString");
					$("#dispEditTargetNm").text("취수관로");
            	break;
				case "P5":                
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA005':LineString");
					$("#dispEditTargetNm").text("급수관로");
            	break;
				case "F4":                
					$("#editTarget").val("ams:wtl_pres_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("가압장");
            	break;
				case "F5":                
					$("#editTarget").val("ams:wtl_rsrv_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("저수조");
            	break;
				case "FC":                
					$("#editTarget").val("ams:wtl_stpi_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("스탠드파이프");
            	break;
				case "F9":                
					$("#editTarget").val("ams:wtl_valv_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("변류시설");
            	break;
				                
				}
                
				if($("#dispLocationInfo2").val()!=''){
					$("#start_edit_mode").show();
					$("#gotoLocationBtn").hide();
					showAsset(layer_name, ftr_idn);	
				}else{
					$("#start_edit_mode").hide();
					$("#gotoLocationBtn").show();					
				}

				
			},
			callbackId: 'RegistAssetItem_ItemCoordinate'
		});
		});
	}
	
	$scope.ShowAssetGisInfoPopup = function() {
		var param = { ASSET_SID: $scope.AssetItemInfo.ASSET_SID };
		mainDataService.getAssetGisInfo(param).success(function(data) {
			console.log(data);
			if(data.GEOM_TEXT=="" || data.GEOM_TEXT == undefined) {
				alertify.alert("Info","표시할 위치정보가 없습니다.",function(){});
				return;
			}
			$rootScope.$broadcast('showGisPopup', {
				ASSET_INFO : $scope.AssetItemInfo,
				gisPopupMode : 'gisView',
				init: function() {
					var layer_name = data.LAYER;
					var ftr_idn = '\'' + data.FTR_IDN + '\'';

					showAsset(layer_name, ftr_idn);
				}, callbackId: 'RegistAssetItem_selectGisInfo'
			});
		});
	};
	//var colIndex = ['ASSET_SID','ASSET_PATH_SID','ASSET_CD','ASSET_NM','CLASS1_NM','CLASS2_NM','CLASS3_NM','CLASS4_NM','CLASS5_NM','CLASS6_NM','CLASS7_NM','CLASS8_NM','CLASS9_NM','CLASS10_NM','CLASS11_NM','CLASS12_NM','CLASS13_NM','CLASS14_NM','CLASS15_NM','INSERT_ID','INSERT_DT'];
	var colIndex = [
		{ name: '순번', id: 'NUM' }
		, { name: '자산명', id: 'ASSET_NM' }
		, { name: '구분', id: 'CLASS3_NM' }
		, { name: '사업장/계통', id: 'CLASS4_NM' }
		, { name: '공정/관로부속시설', id: 'CLASS5_NM' }
		, { name: '공종/대그룹', id: 'CLASS6_NM' }
		, { name: '대그룹/소그룹', id: 'CLASS7_NM' }
		, { name: '소그룹', id: 'CLASS8_NM' }
		, { name: '주소(장소)', id: 'I10005' }
		, { name: '설치연도', id: 'I10004' }
		//, { name: '내용연수', id: 'I10601' }
		//, { name: '잔존수명', id: 'I10602' }
		, { name: '지자체', id: 'CLASS1_NM' }
		, { name: '관리부서', id: 'CLASS2_NM' }
		, { name: '자산코드', id: 'ASSET_CD' }
		, { name: '초기투자비(원)', id: 'I10006' }
		, { name: '폐기일자', id: 'I10008' }
		, { name: '지형지물 관리번호', id: 'I10009' }
		,{name:"지형지물 부호코드",id:"I10010"}
		,{name:"수질부식성(L.I. : Langelier Index)",id:"I10011"}
		,{name:"토양부식성(Ω-㎝)",id:"I10012"}
		,{name:"매설깊이(m)",id:"I10013"}
		,{name:"도로형태",id:"I10014"}
		,{name:"최대수압(kgf/㎠)",id:"I10015"}
		,{name:"대블록",id:"I10016"}
		,{name:"중블록",id:"I10017"}
		,{name:"소블록",id:"I10018"}
		,{name:"관리블록",id:"I10019"}
		,{name:"급수용도",id:"I10020"}
		,{name:"관로계통",id:"I10021"}
		,{name:"관종",id:"I10022"}
		,{name:"관내부 도장재",id:"I10023"}
		,{name:"관외부 도장재",id:"I10024"}
		,{name:"관경(mm)",id:"I10025"}
		,{name:"길이(m)",id:"I10026"}
		,{name:"관접합 종류",id:"I10030"}
		,{name:"밸브실 구분",id:"I10036"}
		,{name:"밸브 종류",id:"I10037"}
		,{name:"밸브 형식",id:"I10038"}
		,{name:"밸브구동 형식",id:"I10039"}
		,{name:"밸브구경(mm)",id:"I10043"}
		,{name:"밸브재질",id:"I10044"}
		,{name:"폭(m)",id:"I10057"}
		,{name:"가로(m)",id:"I10059"}
		,{name:"세로(m)",id:"I10060"}
		,{name:"높이(m)",id:"I10061"}
		,{name:"직경(mm)",id:"I10062"}
		,{name:"(펌프)토출구경(mm)",id:"I10083"}
		,{name:"모델명",id:"I10086"}
		,{name:"제작/제조회사명",id:"I10087"}
		,{name:"형식/종류",id:"I10089"}
		,{name:"규격",id:"I10090"}
		,{name:"펌프동력값(KW)",id:"I10092"}
		,{name:"펌프정격유량/유량(l/min)",id:"I10093"}
		,{name:"회전수(rpm)",id:"I10095"}
		,{name:"전동기동력(kW)",id:"I10099"}
		,{name:"튜브직경(mm)",id:"I10102"}
		,{name:"모터용량(kW)",id:"I10120"}
		,{name:"정격전압(V)",id:"I10122"}
		,{name:"송풍량범위(㎥/min)",id:"I10124"}
		,{name:"최대허용하중(톤)",id:"I10150"}
		,{name:"감가상각비율(%)",id:"I10293"}
	];    
    var rows = [];
    function loadNext(param){
    	mainDataService.getItemListExcel(param)
    	.success(function(Obj){
    		if(Obj.length>0){
        		$.each(Obj,function(idx,Item){
        			var collumns = [];
        			if(idx==0 && param.PAGE_NO== 0 ){
        				var headers = [];				
        				$.each(colIndex,function(idx1,item){
        						headers.push(item.name);
        				});
        				rows.push(headers);
        			}
        			var strArray = null;
        			$.each(colIndex,function(idx1,item){
        				
        				switch(item.id){
        				case 'I10014': //도로형태
        					strArray = $filter('filter')($scope.ComCodeList['RDF'],{C_SCODE : Item[item.id]})[0] || {C_NAME:''};
        					collumns.push(strArray.C_NAME);         					
        					break;
        				case 'I10016':
        				case 'I10017':
        				case 'I10018':
        				//case 'I10019':
        					strArray = $filter('filter')($scope.ComCodeList['BLOC'],{C_SCODE : Item[item.id]})[0] || {C_NAME:''};
        					collumns.push(strArray.C_NAME);
        					break;        					
        				case 'I10021': //관로계통
        					strArray = $filter('filter')($scope.ComCodeList['SAA'],{C_SCODE : Item[item.id]})[0] || {C_NAME:''};
        					collumns.push(strArray.C_NAME);        					
        					break;        					
        				case 'I10022': //관종
        					strArray = $filter('filter')($scope.ComCodeList['MOP'],{C_SCODE : Item[item.id]})[0] || {C_NAME:''};
        					collumns.push(strArray.C_NAME);        					
        					break;        					
        				case 'I10025': //관경
        					strArray = $filter('filter')($scope.ComCodeList['CN21'],{C_SCODE : Item[item.id]})[0] || {C_NAME:''};
        					collumns.push(strArray.C_NAME);        					        					
        					break;
        				default :
        					collumns.push(Item[item.id]);
        				}
        				
        			});
        			rows.push(collumns);
        		});    		
   			param.PAGE_NO = param.PAGE_NO +1;
   			$( "#progressbar" ).progressbar({value: param.PAGE_NO /( $scope.totalCount / 500) * 100 });
    		loadNext(param);
    		}else{
        		const workSheetData = rows;
        		const workSheet = XLSX.utils.aoa_to_sheet(workSheetData);
        		//workSheet['!autofilter'] = {ref : "A1:R11"};
        		const workBook = XLSX.utils.book_new();
        		XLSX.utils.book_append_sheet(workBook, workSheet, '자산목록');
        		XLSX.writeFile(workBook, "자산목록_"+ $scope.getToday()+".xlsx");  
        		$("#dialog-progressBar").hide();
    		}
    	});    	
    }
    $scope.downLoadExcel_new = function(){
    	var path_list = "";
    	var list = [];
    	var cnt = 0 ;
    	$.each($scope.base_info_list,function(idx,item){
    		if(item.IsChecked){
    			if(cnt>0) path_list += ",";
    			path_list += item.LEVEL_PATH_CD;
    			list.push(item.LEVEL_PATH_CD);
    			cnt ++;
    		}
    	});
    	var remain_life = 0;
    	if($("#remain_check01_1").is(":checked")) remain_life =0;
    	else if($("#remain_check01_2").is(":checked")) remain_life =1;
    	else if($("#remain_check01_3").is(":checked")) remain_life =2;
    	else if($("#remain_check01_4").is(":checked")) remain_life =3;    	
  	  var searchOptions = {
				ASSET_NM : $scope.SearchAssetName,
				ASSET_ADDR : $scope.SearchAssetAddr,
				ASSET_CD : $scope.SearchAssetCd,
				USE_YN : ($scope.AssetUseYn)?'ALL':'',
				ASSET_PATH_SID : $scope.asset_path_sid,
				ASSET_PATH_CD : $scope.level_path_cd,
				BASE_INFO_LIST : path_list,
				//list : list,
				INSTALL_DT_MIN : $scope.searchInstallDt.from,
				INSTALL_DT_MAX : $scope.searchInstallDt.to,
				DISPOSE_DT_MIN : $scope.searchDisposeDt.from,
				DISPOSE_DT_MAX : $scope.searchDisposeDt.to,
				INIT_AMT_MIN : $scope.searchInitAmt.from,
				INIT_AMT_MAX : $scope.searchInitAmt.to,
				ASSET_STATE : $scope.SearchAssetState,
				STATE_EVAL_METHOD :   $scope.SearchStateEvalMethod,
				EXCHANGE_REQUIRED :   $scope.SearchExchangeRequired,
				REMAIN_LIFE_CASE :   remain_life  			
			};
  	  var columnIdList = [];
  	  var columnNameList = [];
  	  	$.each(colIndex,function(idx,Item){
  	  		columnIdList.push(Item.id);
  	  		columnNameList.push(Item.name);
  	  	});
  	  	
	    $('#excelDown').find('[name=selectName]').val("selectAssetItemListExcel");//CommonDao.
	    $('#excelDown').find('[name=searchOptions]').val(JSON.stringify(searchOptions));
	    $('#excelDown').find('[name=outputFileName]').val(encodeURI("자산목록_"+ $scope.getToday()+".xlsx"));
	    $('#excelDown').find('[name=templateFileName]').val("");
	    $('#excelDown').find('[name=startRowIdx]').val('1');
	    $('#excelDown').find('[name=startColIdx]').val('0');
	    $('#excelDown').find('[name=columnIdList]').val(columnIdList.toString());
	    $('#excelDown').find('[name=columnNameList]').val(columnNameList.toString());
	    $('#excelDown').find('[name=columnWidthList]').val("3000");

	     $('#excelDown').submit();
    }

    //_PROGRESSBAR
    $scope.downLoadExcel = function(){
    	//alert('test');
    	/*const jobType = $state.current.name;
        //const levelNm = $scope.levelNm;

        $('#exportForm').find('[name=jobType]').val(jobType);
        $('#exportForm').submit();
        */
    	var path_list = "";
    	var list = [];
    	var cnt = 0 ;
    	$.each($scope.base_info_list,function(idx,item){
    		if(item.IsChecked){
    			if(cnt>0) path_list += ",";
    			path_list += item.LEVEL_PATH_CD;
    			list.push(item.LEVEL_PATH_CD);
    			cnt ++;
    		}
    	});
    	var remain_life = 0;
    	if($("#remain_check01_1").is(":checked")) remain_life =0;
    	else if($("#remain_check01_2").is(":checked")) remain_life =1;
    	else if($("#remain_check01_3").is(":checked")) remain_life =2;
    	else if($("#remain_check01_4").is(":checked")) remain_life =3;
    	
    	rows=[];
    	var param={PAGE_NO:0,ROW_CNT:500,
    			//ASSET_NM: $scope.SearchAssetName,
				//ASSET_PATH_SID : $scope.asset_path_sid,
				//ASSET_PATH_CD : $scope.level_path_cd,
				//BASE_INFO_LIST :
				ASSET_NM : $scope.SearchAssetName,
				ASSET_ADDR : $scope.SearchAssetAddr,
				ASSET_CD : $scope.SearchAssetCd,
				USE_YN : ($scope.AssetUseYn)?'ALL':'',
				ASSET_PATH_SID : $scope.asset_path_sid,
				ASSET_PATH_CD : $scope.level_path_cd,
				BASE_INFO_LIST : path_list,
				//list : list,
				INSTALL_DT_MIN : $scope.searchInstallDt.from,
				INSTALL_DT_MAX : $scope.searchInstallDt.to,
				DISPOSE_DT_MIN : $scope.searchDisposeDt.from,
				DISPOSE_DT_MAX : $scope.searchDisposeDt.to,
				INIT_AMT_MIN : $scope.searchInitAmt.from,
				INIT_AMT_MAX : $scope.searchInitAmt.to,
				ASSET_STATE : $scope.SearchAssetState,
				STATE_EVAL_METHOD :   $scope.SearchStateEvalMethod,
				EXCHANGE_REQUIRED :   $scope.SearchExchangeRequired,
				REMAIN_LIFE_CASE :   remain_life					
    			};
    	loadNext(param);
    	$rootScope.$broadcast('ShowCommonProgressBar',{callback_fnc:function(){
    		//alert('test');
    		$( "#progressbar" ).progressbar({value: 0});
    	},alertMessage : '다운로드중입니다.'
    	});
    	//$scope.progress(0);

    }
    /*
    $scope.progress = function(tick){
    	$timeout(function(){
    		if(tick<100){
        		$( "#progressbar" ).progressbar({value: tick});
        		$scope.progress(tick+1);    			
    		}
		
    	},100);
    }*/
    /*
    var closable = alertify.alert().setting('closable');
  //grab the dialog instance using its parameter-less constructor then set multiple settings at once.
  alertify.alert()
    .setting({
      'label':'Agree',
      'message': 'This dialog is : ' + (closable ? ' ' : ' not ') + 'closable.' ,
      'onok': function(){ 
    	  //alertify.success('Great');
    	  }
    }).show();
    */
}