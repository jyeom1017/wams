angular.module('app.item').controller('itemController', itemController);
angular.module('app.item').controller('itemInitController', itemInitController);

function itemInitController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, $q, ConfigService, YearSet, GridConfig, FixedAssetItem) {
	const USER_ID = sessionStorage.getItem('loginId'); // 현재 로그인된 아이디
    let lastSel; // 자산 최대 레벨 설정에서 onCellSelect 메서드에 필요한 변수
    const numberOnly = /^[0-9]+$/; // 숫자 체크 validation

    // broadcast 두 컨트롤러 연결
    $scope.$on('openMaxLevelModal', function(event, data) {
        $('#dialog_item_max_level').show();
        $scope.getAssetMaxLevelList();
    });
    
    $scope.$on('$viewContentLoaded', function () {
        $rootScope.setBtnAuth($scope.MCODE);
    });

    // 자산(시설/관망) 최대 레벨 리스트 가져오기
    $scope.getAssetMaxLevelList = function() {
        mainDataService.getAssetMaxLevelList().success(function(obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.alert('', '자산 최대 레벨 리스트를 가져오는 중 에러 발생 : ' + obj.errMessage);
            } else {
                let obj_F = [];
                let obj_N = [];

                // 자산 최대 레벨 리스트를 시설과 관망으로 분리해 보관
                obj.forEach(element => {
                    if (element.FN_CD === 'F') { obj_F.push(element) }
                    else { obj_N.push(element) }
                })

                set_F_Grid();
                set_N_Grid();

                $scope.list_F.setGridParam({
                    datatype: 'local',
                    data: obj_F,
                }).trigger('reloadGrid');

                $scope.list_N.setGridParam({
                    datatype: 'local',
                    data: obj_N,
                }).trigger('reloadGrid');
            }
        });
    };

    // 시설 자산 최대 레벨 설정 그리드
    function set_F_Grid() {
        let gridInfo = {
            grid_id : 'list_F',
            cellEdit : true,
            colModel : [
                {
                    name : 'FN_CD',
                    hidden : true
                }, {
                    name : 'LEVEL_STEP',
                    width : 60,
                    label : '레벨 단계',
                    sortable : false,
                    editable : false,
                }, {
                    name : 'CD_LEN',
                    width : 60,
                    label : '코드 자리수',
                    editable : true,
                    sortable : false,

                }, {
                    name : 'USER_ID',
                    hidden : true
                }],

            // onSelectRow는 cellEdit:true를 사용하면 사용 불가능. 이땐 onCellSelect 사용
            // edit 상태에서 getRowData()로 그리드 정보를 받으면 값이 아닌 cell html 정보가 넘어간다. 그래서 받아오기 전에 해당 row를 저장해 edit이 끝나도록 설정
            onCellSelect : function(id) {
                if (id && id !== lastSel) {
                    $('#list_F').restoreRow(lastSel, true);
                    lastSel = id;
                    $('#list_F').jqGrid('saveRow', id, true, 'clientArray');
                }

                $('#list_F').editRow(id, true);
                $('#'+id+'_CD_LEN').blur(function() {
                    $('#list_F').jqGrid('setCell', id, 'USER_ID', ''); // edit한 row의 유저 아이디를 현재 로그인 중인 유저 아이디로 설정
                    $('#list_F').jqGrid('saveRow', id, true, 'clientArray'); // 선택 중인 row 저장 및 edit 종료
                })
            }
        }
        $scope.list_F = nonPagerGrid(gridInfo);
    }

    // 관망 자산 최대 레벨 설정 그리드
    function set_N_Grid() {
        let gridInfo = {
            grid_id : 'list_N',
            cellEdit : true,
            colModel : [
                {
                    name : 'FN_CD',
                    hidden : true
                }, {
                    name : 'LEVEL_STEP',
                    width : 60,
                    label : '레벨 단계',
                    sortable : false
                }, {
                    name : 'CD_LEN',
                    width : 60,
                    label : '코드 자리수',
                    editable : true,
                    sortable : false,
                }, {
                    name : 'USER_ID',
                    hidden : true
                }],

            onCellSelect : function(id) {
                if(id && id !== lastSel) {
                    $('#list_N').restoreRow(lastSel, true);
                    lastSel = id;
                    $('#list_N').jqGrid('saveRow', id, true, 'clientArray');
                }

                $('#list_N').editRow(id, true);
                $('#'+id+'_CD_LEN').blur(function() {
                    $('#list_N').jqGrid('setCell', id, 'USER_ID', '');
                    $('#list_N').jqGrid('saveRow', id, true, 'clientArray');
                })
            }
        }
        $scope.list_N = nonPagerGrid(gridInfo);
    }

    // 자산(시설/관망) 최대 레벨 추가
    $scope.addMaxLevel = function(FN) {
        let rowId = $('#list_'+FN+' tbody:first tr:last-child td:nth-child(2)').attr('title');
        rowId = Number(rowId) + 1;

        if (rowId > 15) {
            alertify.alert('', '최대 레벨은 15까지만 가능합니다.');
        } else {
            let data = { FN_CD: FN, LEVEL_STEP: rowId, CD_LEN: "", USER_ID: '' };
            $('#list_'+FN).jqGrid('addRowData', rowId, data);
        }
    }
    $scope.deleteTaskReserveList = [];
    // 자산(시설/관망) 최대 레벨 삭제
    $scope.deleteMaxLevel = function(FN) {
        alertify.confirm('', '레벨을 삭제하시겠습니까?', function(e) {
            if (e) {
                const rowId = $('#list_'+FN+' tbody:first tr:last-child').attr('id');
                const rowData = $('#list_'+FN).getRowData(rowId);

                if (rowData.LEVEL_STEP < 2) {
                    alertify.alert('', '레벨 단계에서 최소 레벨 1은 있어야 합니다.');
                    return false;
                }
                $scope.deleteTaskReserveList.push({rowData});
                $('#list_'+FN).jqGrid('delRowData', rowId);
            	/*
                mainDataService.deleteMaxLevel(rowData).success(function() {
                    $('#list_'+FN).jqGrid('delRowData', rowId);
                    alertify.success('최대 레벨을 삭제했습니다.');
                    $timeout(function(){
                    	alertify.alert("최대 레벨을 변경 사항을 적용하기 위해 화면 새로고침을  합니다.",function(){location.reload();});
                    },8000);
                });
                */
            }
        }, function(e) {}); // alertify.confirm에서 실패 시 동작하는 함수를 선언해줘야 정상 작동함
    }

    // 자산(시설/관망) 최대 레벨 일괄 저장
    $scope.saveLevelFN = function() {
        let rowDataF = $('#list_F').getRowData();
        let rowDataN = $('#list_N').getRowData();
        let param = rowDataF.concat(rowDataN);

        for (let i = 0; i < param.length; i++) {
            if (!param[i].CD_LEN.match(numberOnly)) {
                alertify.alert('', '빈 칸 혹은 숫자 외 문자는 기입이 불가능합니다.');
                return false;
            }
        }
        $scope.updateMaxLevelList(param);
    }

    // 자산(시설/관망) 최대 레벨 수정
    $scope.updateMaxLevelList = function(param) {
        mainDataService.updateMaxLevelList(param).success(function(obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('최대 레벨 갱신 중 에러 발생 : ' + obj.errMessage);
            } else {
                alertify.success('변경된 최대 레벨을 저장했습니다.');
                $timeout(function(){
                	alertify.alert("최대 레벨을 변경 사항을 적용하기 위해 화면 새로고침을  합니다.",function(){location.reload();});
                },8000);                
            }
            $scope.closeMaxLevelModal();
        });
    }

    // 자산(시설/관망) 최대 레벨 모달창 닫기
    $scope.closeMaxLevelModal = function() {
        $('#dialog_item_max_level').hide();
        $('#list_F').jqGrid('clearGridData');
        $('#list_N').jqGrid('clearGridData');
    }
}

function itemController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, $q, ConfigService, YearSet, GridConfig, FixedAssetItem) {
    const USER_ID = sessionStorage.getItem('loginId'); // 현재 로그인된 아이디
    $scope.levelNm = ''; // 레벨명 조회
    $scope.PathInfo = {}; // 자산 레벨 경로 정보
    $scope.StandardList = []; // 표준 항목 (화면)
    $scope.StatusList = []; // 상태 항목 (화면)
    $scope.CustomList = []; // 사용자 정의 항목 (화면)
    $scope.StandardModalList = []; // 표준 항목 (팝업창)
    $scope.StatusModalList = []; // 상태 항목 (팝업창)
    $scope.CustomModalList = []; // 사용자 정의 항목 (팝업창)
    $scope.AssetLevelStorage = {}; // 전체 시설/관망 자산 레벨 데이터
    $scope.AssetLevel = {}; // 자산 레벨 (팝업창)
    $scope.MaxLevel = 0; // 최대 레벨 (화면)
    $scope.MaxLevelModal = 0; // 최대 레벨 (팝업창)
    $scope.FN_val = ''; // 시설(F) 또는 관망(N) 분류
    $scope.isNewAsset = true; // 자산 신규 생성/수정 식별
    $scope.showItemAddButton = false; // 사용자 정의 항목 추가 버튼
    $scope.LevelF = []; // 시설의 각 레벨 정보
    $scope.LevelN = []; // 관망의 각 레벨 정보
    $scope.MCODE = '0004';
    
    $scope.$on('$viewContentLoaded', function() {
        $rootScope.setBtnAuth($scope.MCODE);
    });

    // 항목 설정의 탭 기능
    var oldTabId = "tab1";
    $scope.showTab = function(tabId, event) {
    	if (oldTabId === tabId) return;
    	$("#" + tabId).show();
    	$("#" + oldTabId).hide();
    	oldTabId = tabId;
    	$(event.currentTarget).parent().parent().find('button').removeClass('active');
    	$(event.currentTarget).addClass('active');
    }

    var oldTabId2 = "tab_1";
    $scope.showTab2 = function(tabId, event) {
        if (oldTabId2 === tabId) return;
        $("#" + tabId).show();
        $("#" + oldTabId2).hide();
        oldTabId2 = tabId;
        $(event.currentTarget).parent().parent().find('button').removeClass('active');
        $(event.currentTarget).addClass('active');

        if (tabId === 'tab_3') { $scope.showItemAddButton = true; }
        else { $scope.showItemAddButton = false; }
    }

    // 자산 최대 레벨 설정 모달창 열기
    $scope.openMaxLevelModal = function() {
        $rootScope.$broadcast('openMaxLevelModal',{});
    };

    // 그리드 레벨명 검색 또는 그리드 초기화로 사용
    $scope.searchItem = function() {
        $('#list').jqGrid('setGridParam', {
            postData : {
                rows : GridConfig.sizeL,
                levelNm : $scope.levelNm || '',
            }
        }).trigger('reloadGrid', [{ page : 1 }]);
        /*
        mainDataService.getLevelPathTotalCount().success(function(obj) {
            $scope.count = obj;
        });
        */
    };

    // 자산의 레벨 길이 가져오기(부안군 프로젝트는 레벨11로 고정)
    $scope.getAssetCriteria = function() {
        mainDataService.getAssetCriteria().success(function(obj) {
            $scope.assetCriteria = obj;
            let len = obj.MAX_LEVEL; // len = 15
            $scope.MaxLevel = len - 7;
            setGrid();
            gridResize();
        });
    }

    // 레벨 인벤토리 경로 설정에 필요한 레벨 단계
    $scope.getLevelNumList = function() {
        mainDataService.getLevelNumList().success(function(obj) {
            for (let i = 0; i < obj.length; i++) {
                let num = obj[i].LEVEL_NUM.substr(5).split('_')[0];
                obj[i].LEVEL_NUM = '레벨' + num;
                obj[i].NUM = num;
            }
            $scope.storeLevelNum = obj;
        });
    };

    // Enter 키 이벤트 발생 시 레벨명으로 조회
    $scope.onKeyPress = function(event) {
        if (event.key === 'Enter') {
            $scope.searchItem();
        }
    }

    // LEVEL_CD에 매칭되는 레벨명으로 가져오기
    function getLevelName(pathCd, level) {
        let LEVEL_CD = pathCd.split('_')[level-1];
        if (LEVEL_CD !== '') {
            return $rootScope.LevelName['' + (level - 1) + '_' + LEVEL_CD];
        }
        return LEVEL_CD;
    }

    // 자산 경로 그리드 생성
    function setGrid() {
        let colModel = [];

        colModel.push(
            { name : 'RNUM', width : 80, label : '순번' },
            {
                name : 'PATH_CD',
                width : 410,
                label : '경로 코드',
                formatter : function(cellvalue, options, rowdata, action) {
                    return (rowdata.PATH_CD === null) ? "" : cellvalue.replace(/_/gi, "");
                }
            },
            { name : 'ASSET_PATH_SID', label : '자산코드', hidden : true }
        );

        // 최대 레벨에 따라 컬럼 개수가 동적으로 생성되도록 설정. 현재 레벨11을 최대 레벨로 고정
        for (let i = 1; i <= $scope.MaxLevel; i++) {
            colModel.push(
                {
                    name : 'CLASS'+i+'_NM',
                    index : 'CLASS'+i+'_CD',
                    width : 250,
                    label : '레벨'+i,
                    formatter : function(cellvalue, options, rowdata, action) {
                        return (rowdata.PATH_CD === null) ? '' : getLevelName(rowdata.PATH_CD, i);
                    }
                },
                {
                    name : 'CLASS'+i+'_CD',
                    hidden : true
                }
            );
        }

        colModel.push(
            { name : 'TEMPLATE_SID', hidden : true },
            { name : 'LAYER_NM', hidden : true },
            { name : 'LAYER_CD', hidden : true },
            { name : 'FTR_CDE', hidden : true },
            { name : 'INSERT_DT', width : 200, label : '생성일' },
            { name : 'INSERT_ID', width : 131, label : '작성자' }
        );

        return pagerJsonGrid({
            grid_id : 'list',
            pager_id : 'listPager',
            url : '/asset/getItemList.json',
            condition : {
                page : 1,
                rows : GridConfig.sizeL,
            },
            rowNum : 20,
            colModel : colModel,

            onSelectRow : function (id) {
                $scope.PathInfo = $('#list').jqGrid('getRowData', id);
                $scope.$apply('PathInfo');
                $scope.getTemplateInfoItems();
            }
            ,
            gridComplete: function() {
                var ids = $(this).jqGrid('getDataIDs');
                console.log($scope.PathInfo);
                if ($.isEmptyObject($scope.PathInfo)) {
                    $(this).setSelection(ids[0]);
                } else {
                    var rowData = $(this).jqGrid('getRowData');
                    for (var i = 0; i < rowData.length; i++) {
                        if (rowData[i].ASSET_PATH_SID === $scope.PathInfo.ASSET_PATH_SID) {
                            $(this).setSelection(ids[i]);
                            break;
                        }
                    }
                }
                //alert($('#list').getGridParam('page'));
                $scope.currentPageNo = $('#list').getGridParam('page');
                $scope.count = $('#list').getGridParam('records');
                if ($('#list').jqGrid('getRowData', ids[0]).RNUM == '') {
                    $scope.count = 0;
                }
                $scope.$apply();

            }
        });
    }

    // 엑셀 파일 다운로드(추후 수정 필요)
    $scope.getExcelDownload = function() {
    	/*
        const jobType = $state.current.name;
        const levelNm = $scope.levelNm;

        $('#exportForm').find('[name=jobType]').val(jobType);
        $('#exportForm').find('#levelNm').val(levelNm);
        $('#exportForm').submit();
        */
      	var searchOptions = {
      			levelNm : $scope.levelNm || ''
    		};
        $('#excelDown').find('[name=selectName]').val("getLevelNumListExcel");//CommonDao.
        $('#excelDown').find('[name=searchOptions]').val(JSON.stringify(searchOptions));
        $('#excelDown').find('[name=outputFileName]').val(encodeURI("자산인벤토리경로목록_"+ $scope.getToday()+".xlsx"));
        $('#excelDown').find('[name=templateFileName]').val("");
        $('#excelDown').find('[name=startRowIdx]').val('1');
        $('#excelDown').find('[name=startColIdx]').val('0');
        $('#excelDown').find('[name=columnIdList]').val("RNUM,PATH_CD,CLASS1_NM,CLASS2_NM,CLASS3_NM,CLASS4_NM,CLASS5_NM,CLASS6_NM,CLASS7_NM,CLASS8_NM,INSERT_DT,INSERT_ID");
        $('#excelDown').find('[name=columnNameList]').val("순번,경로코드,레벨1,레벨2,레벨3,레벨4,레벨5,레벨6,레벨7,레벨8,생성일,작성자");
        $('#excelDown').find('[name=columnWidthList]').val("3000");

         $('#excelDown').submit();
    };

    // 자산 경로 삭제
    $scope.deleteAssetPath = function() {
    	
        // 선택한 자산 경로가 없으면 막기
        if (angular.equals({}, $scope.PathInfo)) {
            alertify.alert('', '삭제할 자산 경로를 선택해주세요.');
            return false;
        }

        // 자산 경로 내 레벨 중 undefined 값이 있으면 막기
        for (let i = 1; i <= 15; i++) {
            if ($scope.PathInfo['CLASS'+i+'_NM'] === 'undefined') {
                alertify.alert('', 'undefined가 포함된 자산 경로는 삭제할 수 없습니다.');
                return false;
            }
        }

        let value = { ASSET_PATH_SID : $scope.PathInfo.ASSET_PATH_SID };

        // 삭제하려는 자산 경로가 자산에서 쓰이고 있는지 체크
        mainDataService.isAssetPathUsed(value).success(function(obj) {
            if (obj > 0) {
                alertify.alert('', '사용 중인 자산 경로는 삭제할 수 없습니다.');
                return false;
            } else {
                alertify.confirm('', '해당 자산 경로를 삭제하시겠습니까?', function(e) {
                    if (e) {
                        const param = { ASSET_PATH_SID : $scope.PathInfo.ASSET_PATH_SID
                        		,FN_GBN: $scope.PathInfo.CLASS3_CD
                       			,WORK_GBN: ($scope.PathInfo.CLASS3_CD=='F')?$scope.PathInfo.CLASS6_CD:$scope.PathInfo.CLASS5_CD
                       			,ASSET_GBN1: ($scope.PathInfo.CLASS3_CD=='F')?$scope.PathInfo.CLASS7_CD:$scope.PathInfo.CLASS6_CD
                   				,ASSET_GBN2: ($scope.PathInfo.CLASS3_CD=='F')?$scope.PathInfo.CLASS8_CD:$scope.PathInfo.CLASS7_CD                        		
                        		};
                        mainDataService.deleteAssetPath(param).success(function() {
                            alertify.success('자산 레벨을 삭제했습니다.');
                            setGrid();
                            $scope.searchItem();
                            $scope.PathInfo = {};
                            $scope.StandardList = $scope.StatusList = $scope.CustomList = {};
                        });
                    }
                }, function(e) {});
            }
        })
    }

    // 자산 경로 선택 시 해당 경로의 표준 항목 리스트 불러오기
    $scope.getTemplateInfoItems = function() {
        $scope.StandardList = [];
        $scope.StatusList = [];

        const TEMPLATE_SID = { TEMPLATE_SID : $scope.PathInfo.TEMPLATE_SID };
        const ASSET_PATH_SID = { ASSET_PATH_SID : $scope.PathInfo.ASSET_PATH_SID };

        mainDataService.getInfoItemList(TEMPLATE_SID).success(function(obj) {
            for (let i = 0; i < obj.length; i++) {
                if (obj[i].ITEM_GB === '1') {
                    $scope.StandardList.push(obj[i]);
                } else {
                    $scope.StatusList.push(obj[i]);
                }
            }
        });

        // 해당 경로의 사용자 정의 항목 가져오기
        mainDataService.getCustomItemList(ASSET_PATH_SID).success(function(obj) {
            $scope.CustomList = obj;
        });
    }

    $scope.getAssetMaxLevelList = function() {
        mainDataService.getAssetMaxLevelList().success(function(obj) {
            for (let i = 0; i < obj.length; i++) {
                if (obj[i].FN_CD === 'F') { $scope.LevelF.push(obj[i]); }
                else { $scope.LevelN.push(obj[i]); }
            }
        })
    }

    // 시설/자산 신규 생성 모달창
    $scope.openFNModal = function(FN) {
        $('#popup_modal2').show();
        $scope.isNewAsset = true;
        $scope.saveMode = 'new';
        if (FN === 'F') {
            $scope.FN_val = 'F';
            $scope.MaxLevelModal = $scope.LevelF.length;
        } else {
            $scope.FN_val = 'N';
            $scope.MaxLevelModal = $scope.LevelN.length;
        }

        $scope.getAllAssetLevelList(); // 시설/관망의 자산 경로 가져오기
        $scope.getAllInfoItemList(); // 자산 항목 가져오기

        //$scope.PathInfo = {};
        //$scope.StandardList = $scope.StatusList = $scope.CustomList = [];
    };

    $scope.getAllAssetLevelList = function() {
        const param = { FN_CD: $scope.FN_val };
        mainDataService.getAllAssetLevelList(param).success(function(obj) {
            $scope.AssetLevelStorage = obj;
            $scope.sortByLevel();
            $scope.sortByLevel(1);
        });
    }

    // chosen.js를 이용한 검색 가능한 select 생성
    $scope.sortByLevel = function(Idx) {
        $('.asset_level').chosen(
            {
                no_results_text : '일치하는 단어가 없습니다.', // 검색 단어 일치 확인
                disable_search_threshold : 5 // 일정 개수 이하면 검색 기능 끄기
            }
        );
        //debugger;
        console.log(Idx);
        // half_formB class의 css 설정 override
        $('.chosen-search-input').attr('style', function(i,s) { return (s || '') + 'width: 100% !important;' });
        $('.chosen-search-input').css('float', 'none');
        // select options 추가
        for(let i = 1; i <= 15; i++) {
        	if(typeof Idx!='undefined' && Idx >= 0 && i-1 >= Idx ){ 
        		$('#asset_level_'+i).html("<option value=''>자산을 선택하세요.</option>");
        		$scope.AssetLevel['CLASS'+(i)+'_CD'] = "";
        	}
        	if(typeof Idx!='undefined' && Idx >= 0 && Idx != i-1 ) continue;
            for (let j = 0; j < $scope.AssetLevelStorage.length; j++) {
                if ($scope.AssetLevelStorage[j].LEVEL_STEP === i) {
                	
                	var P_CLASS_CD = $scope.AssetLevel['CLASS'+($scope.AssetLevelStorage[j].P_LEVEL_STEP)+'_CD'];
                	if($scope.AssetLevelStorage[j].P_LEVEL_STEP > 0 &&  ($scope.AssetLevelStorage[j].P_CLASS_CD != '' || $scope.AssetLevelStorage[j].P_CLASS_CD !=null) ){
                        let name = $scope.AssetLevelStorage[j].LEVEL_NM;
                        let code = $scope.AssetLevelStorage[j].CLASS_CD;
                        //debugger;
                		if($scope.AssetLevelStorage[j].P_CLASS_CD == P_CLASS_CD)
                			$('#asset_level_'+i).append("<option  value='"+code+"' class='asset-level-options'>"+name+"</option>");                		
                	}else{
                        let name = $scope.AssetLevelStorage[j].LEVEL_NM;
                        let code = $scope.AssetLevelStorage[j].CLASS_CD;
                        $('#asset_level_'+i).append("<option  value='"+code+"' class='asset-level-options'>"+name+"</option>");
                	}
                }
            }
            // 각 select 레벨 끝에 빈 option 태그를 추가하지 않으면 마지막 아이템이 선택 안 되는 에러가 있음.
            //$('#asset_level_'+i).append("<option disabled></option>");
            if(typeof Idx=='undefined'){
            	$('.asset_level').trigger("chosen:updated");
            	return;
            }
        }

        // chosen 업데이트 반영
        $('.asset_level').trigger("chosen:updated");
    }

    // select option 선택 변경마다 적용되도록 업데이트
    $scope.optionChanged = function(Idx) {
    	$scope.sortByLevel(Idx);
        //$('.asset_level').trigger("chosen:updated");
    }

    // 자산 항목 불러오기
    $scope.getAllInfoItemList = function(callback) {
        mainDataService.getAllInfoItemList().success(function(obj) {
            obj.forEach(function(element) {
                if (element.ITEM_GB === '1') {
                    let result = $scope.isRequired(element);
                    $scope.StandardModalList.push(result);
                }
                else {
                    element.isChecked = false;
                    $scope.StatusModalList.push(element);
                }
                if (typeof callback == 'function') {
                    callback();
                }
            })
        });
    }

    // 자산의 표준 항목 중 필수 항목 체크
    $scope.isRequired = function(element){
        $.each(FixedAssetItem.list, function(idx, item){
            if (item === element.ITEM_CD) {
                element.isChecked = true;
                return false;
            }
            if (idx > 10) element.isChecked = false;
        });
        return element;
    }

    // 사용자 항목 추가 버튼
    $scope.addCustomItem = function() {
        const rowCount = $('.custom-item-tr').length + 1;
        $('#custom-items-table').append(
        		$compile(
        				"<tr id='custom-item-tr-"+rowCount+"' class='custom-item-tr'><td class='c'>"+rowCount+"" +
        						"</td><td class='ct'>" +
        						"<input kr-input type='text' class='custom-item-input' style='width: 100%'></td>" +
        						"<td class='ct'>" +
        						"<button data-id='"+rowCount+"' class='custom-item-delete-button btn btn-danger' ng-click='removeCustomItem($event)'>삭제</button>" +
        								"</td></tr>")($scope));
    }

    // 사용자 항목 삭제 버튼
    $scope.removeCustomItem = function(e) {
        const id = $(e.target).data('id');
        $('#custom-item-tr-'+id).remove();
    }

    // 사용자 항목 전체 초기화
    $scope.removeCustomItems = function() {
        $('.custom-item-tr').remove();
    }

    // 선택된 자산 경로 레벨, 표준/상태/사용자정의 항목 일괄 저장
    $scope.saveAsset = function() {
        let checkedAssets = []

        $scope.StandardModalList.forEach(element => {
            if (element.isChecked) {
                element.USER_ID = USER_ID;
                checkedAssets.push(element);
            }
        })

        $scope.StatusModalList.forEach(element => {
            if (element.isChecked) {
                element.USER_ID = USER_ID;
                checkedAssets.push(element);
            }
        })

        let param = {}
        param.infoItems = checkedAssets;

        let customItems = document.getElementsByClassName("custom-item-input");
        $scope.CustomModalList = [];

        if ($scope.isNewAsset) {
            $scope.saveNewAsset(param, customItems); // 신규 저장
        }
        else {
            $scope.saveExistedAsset(param, customItems); // 수정
        }
    }

    $scope.saveNewAsset = function(param, customItems) {
        for (let i = 1; i <= $scope.MaxLevelModal; i++) {
            if ($scope.AssetLevel['CLASS'+i+'_CD'] === undefined || $scope.AssetLevel['CLASS'+i+'_CD'] ==="") {
                alertify.alert('', '모든 레벨을 입력해주세요.');
                return false;
            }
        }

        let pathCd = '';

        for (let i = 1; i <= 15; i++) {
            let classCd = $scope.AssetLevel['CLASS'+i+'_CD'];
            pathCd = classCd !== undefined ? pathCd.concat(classCd+'_') : pathCd.concat('_');
        }

        const value = { PATH_CD : pathCd };

        mainDataService.isAssetPathExists(value).success(function(obj) {
            if (obj === 0) {
                $scope.AssetLevel.USER_ID = USER_ID;
                param.assetPath = $scope.AssetLevel;

                for (let i = 0; i < customItems.length; i++) {
                    if (customItems[i].value !== '') {
                        $scope.CustomModalList.push({
                            ITEM_NM : customItems[i].value,
                            MANUAL_ITEM_SID : i+1,
                            USER_ID : USER_ID
                        });
                    }
                }
                param.customItems = $scope.CustomModalList;

                mainDataService.saveAssets(param).success(function(obj) {
                    if (obj >= 0) {
                        alertify.success('자산 레벨을 저장했습니다.');
                        $scope.closeItemModal();
                        $scope.searchItem();
                        $scope.removeCustomItems();
                        setGrid();
                    } else {
                        alertify.alert('', '자산 경로 저장 중 에러가 발생했습니다.');
                        return false;
                    }
                });
            } else {
                alertify.alert('', '이미 존재하는 자산 경로입니다. 다른 값으로 시도해주세요.');
                return false;
            }
        })
    }

    $scope.saveExistedAsset = function(param, customItems) {
        $scope.PathInfo.USER_ID = USER_ID;
        $scope.PathInfo['LAYER_NM'] = $scope.AssetLevel.LAYER_NM;
        $scope.PathInfo['LAYER_CD'] = $scope.AssetLevel.LAYER_CD;
        $scope.PathInfo['FTR_CDE'] = $scope.AssetLevel.FTR_CDE;
        param.assetPath = $scope.PathInfo;

        for (let i = 0; i < customItems.length; i++) {
            $scope.CustomModalList.push({
                ASSET_PATH_SID : $scope.PathInfo.ASSET_PATH_SID,
                ITEM_NM : customItems[i].value,
                MANUAL_ITEM_SID : i+1,
                USER_ID : USER_ID
            });
        }
        param.customItems = $scope.CustomModalList;

        mainDataService.updateAssets(param).success(function(obj) {
            if (obj >= 0) {
                alertify.success('자산 레벨을 수정했습니다.');
                $scope.closeItemModal();
                $scope.searchItem();
                $scope.StandardList = $scope.StatusList = $scope.CustomList = {};
                $scope.removeCustomItems();
                setGrid();
            } else {
                alertify.alert('', '자산 경로 수정 중 에러가 발생했습니다.');
                return false;
            }
        });
    }

    // 자산 경로 수정 모달창 열기
    $scope.openFNEditModal = function() {
        if (angular.equals({}, $scope.PathInfo)) {
            alertify.alert('', '수정할 자산 경로를 선택해주세요.');
            return false;
        }
        $scope.saveMode = '';
        $scope.MaxLevelModal =15;
        if ('F' == $scope.PathInfo.CLASS3_CD) {
            $scope.FN_val = 'F';
            $scope.MaxLevelModal = $scope.LevelF.length;
        } else {
            $scope.FN_val = 'N';
            $scope.MaxLevelModal = $scope.LevelN.length;
        }
        $scope.AssetLevel.LAYER_NM = $scope.PathInfo.LAYER_NM;
        $scope.AssetLevel.LAYER_CD = $scope.PathInfo.LAYER_CD;
        $scope.AssetLevel.FTR_CDE = $scope.PathInfo.FTR_CDE;
        /*
        $.each($scope.PathInfo,function(idx,val){
        	if(val=='')
       		{
        		console.log(idx);
        		$scope.MaxLevelModal = parseInt(idx.replace('CLASS','').replace('NM','')) -1;
        		return false;
       		}
        });
        */

        $scope.getAllInfoItemList(function() {
            $('#popup_modal2').show();
            $scope.isNewAsset = false;
            $scope.saveMode = '';
            for(let i = 0; i < $scope.StandardModalList.length; i++) {
                for (let j=0; j < $scope.StandardList.length; j++) {
                    if ($scope.StandardModalList[i].ITEM_CD === $scope.StandardList[j].ITEM_CD) {
                        $scope.StandardModalList[i].isChecked = true;
                    }
                }
            }

            for(let i = 0; i < $scope.StatusModalList.length; i++) {
                for (let j=0; j < $scope.StatusList.length; j++) {
                    if ($scope.StatusModalList[i].ITEM_CD === $scope.StatusList[j].ITEM_CD) {
                        $scope.StatusModalList[i].isChecked = true;
                    }
                }
            }
            $scope.CustomModalList = $scope.CustomList;
        });
    }

    // 자산 경로 신규/수정 모달창 닫기
    $scope.closeItemModal = function() {
        $('#popup_modal2').hide();
        $('.asset-level-options').remove();
        $('.asset_level').trigger('chosen:updated');

        $scope.StandardModalList = [];
        $scope.StatusModalList = [];
        $scope.CustomModalList = [];
        $scope.AssetLevel = {};
        $scope.removeCustomItems();
    }

    $scope.f_checkbox = function(ItemCd){
    	document.getElementById('item_'+ItemCd).checked = true;
    }
    
    $scope.IsFixed = function(ItemCd){
    	var check = false;
    	$.each(FixedAssetItem.list, function(idx, item){
    		if (item === ItemCd) {
    			check = true;
    			return false;
    		}
    		if (idx > 10) return false;
    	});
    	if(check) $scope.f_checkbox(ItemCd);
    }
        
    // 처음 화면을 불러올 때 실행
    $scope.getAssetCriteria();
    $scope.getLevelNumList();
    $scope.searchItem();
    $scope.getAssetMaxLevelList();
}