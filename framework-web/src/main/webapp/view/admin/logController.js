angular.module('app.admin').controller('logController', logController);

function logController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService,GridConfig) {
	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 1주일
        sday.setDate(sday.getDate() - 7); // 1주일전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.startDate = s_yyyymmdd;
        $('#start_dt').val(s_yyyymmdd);
        $scope.endDate = e_yyyymmdd;
        $('#end_dt').val(e_yyyymmdd);
    }
	
	
	$scope.logListRows = 18;
	
/*    $scope.loadLog= function(){
		$scope.logList = [];
		mainDataService.getSystemLogList({lg_opt:$scope.LG_OPT})
		.success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.error('errMessage : ' + obj.errMessage);
			} else {
				$scope.logList=obj;
			}
		});
	}
    
    $scope.loadLog();*/
	$scope.$on('$viewContentLoaded', function() {
		$scope.setWeek();
		setGrid();
		setDatePicker();
        gridResize();
         
    });
	
	var colIndex = [{name:'순번',id:'NUM'},{name:'로그일',id:'LG_DT'},{name:'로그구분',id:'LG_OPT_TEXT'},{name:'로그내용',id:'LG_DESC'}
	,{name:'접속IP',id:'IP_ADDR'},{name:'사용자',id:'G_UUSER_TEXT'},{name:'사용자ID',id:'G_UUSER'}];
	
	var rows = [];
	function loadNext(param){
		mainDataService.getLogExcel(param)
		.success(function(Obj){
			
			if(Obj.length>0 ){
	    		$.each(Obj,function(idx,Item){
	    			var collumns = [];
	    			if(idx==0 && param.PAGE_NO== 0 ){
	    				var headers = [];				
	    				$.each(colIndex,function(idx1,item){
	    						headers.push(item.name);
	    				});
	    				rows.push(headers);
	    			}
	    			$.each(colIndex,function(idx1,item){
	    				collumns.push(Item[item.id]);
	    			});
	    			rows.push(collumns);
	    		});    		
				param.PAGE_NO = param.PAGE_NO +1;
				$( "#progressbar" ).progressbar({value: param.PAGE_NO /( $scope.totalCount / 100) * 100 });
			loadNext(param);
			}else{
	    		const workSheetData = rows;
	    		const workSheet = XLSX.utils.aoa_to_sheet(workSheetData);
	    		//workSheet['!autofilter'] = {ref : "A1:R11"};
	    		const workBook = XLSX.utils.book_new();
	    		XLSX.utils.book_append_sheet(workBook, workSheet, '로그');
	    		XLSX.writeFile(workBook, "시스템로그_"+ $scope.getToday()+".xlsx");  
	    		$("#dialog-progressBar").hide();
			}
		});    	
	}

	$scope.ExcelReport = function(){
		//alertify.alert('알림','기간을 설정하세요',function(){
			rows=[];
	    	var param={PAGE_NO:0,ROW_CNT:100,
	                startDt : $scope.startDate,
	                endDt : $scope.endDate,
	                searchText : $scope.searchText,
	                lg_opt:$scope.LG_OPT
	    			};
	    	loadNext(param);

	    	
	    	$rootScope.$broadcast('ShowCommonProgressBar',{callback_fnc:function(){
	    		//alert('test');
	    		$( "#progressbar" ).progressbar({value: 0});
	    	},alertMessage : '다운로드중입니다.'
	    	});
		//});
	}
	
	$scope.loadLog = function(){
        $('#list').jqGrid('setGridParam', {
            page : 1,
            rows : GridConfig.sizeXL,
        	postData: {
                startDt : $scope.startDate,
                endDt : $scope.endDate,
                searchText : $scope.searchText,
                lg_opt:$scope.LG_OPT
            }
        }).trigger('reloadGrid', {
            current: true
        });
	}
	
	function setGrid(){
	pagerJsonGrid({
        grid_id    : 'list',
        pager_id   : 'listPager',
        url        : '/api/getSystemLog.json',
        condition  : {
            page : 1,
            rows : GridConfig.sizeM,
            startDt : $scope.startDate,
            endDt : $scope.endDate,            
            lg_opt:$scope.LG_OPT
        },
        rowNum     : GridConfig.sizeM,
        colNames   : ['순번', '로그일', '로그구분', '로그내용','접속IP','사용자', '사용자ID'],
        colModel   : [
            {
                name : 'RNUM', // 순번
                width: 22,
                index: 'RNUM',
                sortable:false,
                resizable: false
            }, {
                name : 'LG_DT', // 로그일
                width: 30,
                index: 'LG_DT',
                resizable: false
            }, {
                name : 'LG_OPT_TEXT', // 로그구분
                width: 22,
                index: 'LG_OPT_TEXT',
                resizable: false
            }, {
                name : 'LG_DESC', // 로그상세
                width: 53,
                index: 'LG_DESC',
                resizable: false
            }, {
            	name : 'IP_ADDR', // 접속아이피
                width: 53,
                index: 'IP_ADDR',
                resizable: false
            }, {            	
                name : 'G_UUSER_TEXT', // 사용자
                width: 53,
                index: 'G_UUSER_TEXT',
                resizable: false
            }, {            	
                name : 'G_UUSER', // 사용자ID
                width: 53,
                index: 'G_UUSER',
                resizable: false
            }
        ],
        gridComplete : function() {
        	var ids = $(this).jqGrid('getDataIDs');
			$scope.currentPageNo=$('#list').getGridParam('page');
			$scope.totalCount = $('#list').getGridParam("records");
			if($('#list').jqGrid('getRowData', ids[0]).RNUM=="") $scope.totalCount = 0;
			$scope.$apply();
		}   
    });
	}
	
	$scope.onKeyPress = function(event){
        if (event.key === 'Enter') {
        	$scope.loadLog();
        }
    }
}
