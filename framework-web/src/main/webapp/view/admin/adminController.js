angular.module('app.admin').controller('adminController', adminController);

function adminController($scope, $state, $stateParams, mainDataService, $rootScope, $compile,$filter, $timeout, $interval, ConfigService, GridConfig) {
	//$scope.MCODE = '1510';
    /**
     * 메뉴 관리
     */
    // 우측 메뉴 정보 > 고급 클릭 시 메뉴코드, 메뉴정렬 보이게
    $scope.advanced = function () {
        $("#clear_wrap").show();
    };

    // 추가/수정 팝업에서 고급 클릭 시 메뉴코드, 메뉴정렬 보이게
    $scope.advancedPopup = function () {
        // alert('ad');
        $("#clear_wrap_popup").show();
    };

    // 좌측 메뉴 관리 > collapse 실행 시 (show/hide) 아이콘 plus, minus 변경하기
    $(document).on('hide.bs.collapse', '.collapse', function (e) {
        if ($(this).is(e.target)) {
            $(this).prev().find('.fas').removeClass("fa-minus").addClass("fa-plus");
        }
    }).on('show.bs.collapse', '.collapse', function (e) {
        if ($(this).is(e.target)) {
            $(this).prev().find('.fas').removeClass("fa-plus").addClass("fa-minus");
        }
    });

    $scope.crudType = '';
    $scope.alertMessage = '';
    $scope.codeList = [];
    $scope.codeItem = [{
        // 'name'     : '그룹코드',
        // 'name2'    : 'C_GCODE',
        // 아래 항목 전부 수정함
        'name' : 'C_GCODE',
        'name2' : 'C_GCODE',
        'title'    : '그룹코드',
        'required' : true,
        'maxlength': 4
    }, {
        'name' : 'C_SCODE',
        'name2': 'C_SCODE',
        'title': '코드',
        'required' : true,
        'maxlength': 10
    }, {
        'name' : 'C_NAME',
        'name2': 'C_NAME',
        'title': '코드명',
        'maxlength': 50
    }, {
        'name' : 'C_MEMO',
        'name2': 'C_MEMO',
        'title': '코드설명',
        'maxlength': 1000
    }, {
        'name' : 'C_VALUE1',
        'name2': 'C_VALUE1',
        'title': '데이터1',
        'maxlength': 38,
        'type': 'number'
    }, {
        'name' : 'C_VALUE2',
        'name2': 'C_VALUE2',
        'title': '데이터2',
        'maxlength': 38,
        'type': 'number'
    }, {
        'name' : 'C_VALUE3',
        'name2': 'C_VALUE3',
        'title': '데이터3',
        'maxlength': 38,
        'type': 'number'
    }, {
        'name' : 'C_VALUE4',
        'name2': 'C_VALUE4',
        'title': '데이터4',
        'maxlength': 38,
        'type': 'number'
    }, {
        'name' : '문자데이터1',
        'name2': 'C_CVALUE1',
        'title': '문자데이터1',
        'maxlength': 200
    }, {
        'name' : 'C_CVALUE2',
        'name2': 'C_CVALUE2',
        'title': '문자데이터2',
        'maxlength': 200
    }, {
        'name' : 'C_CVALUE3',
        'name2': 'C_CVALUE3',
        'title': '문자데이터2',
        'maxlength': 200
    }, {
        'name' : 'C_CVALUE4',
        'name2': 'C_CVALUE4',
        'title': '문자데이터4',
        'maxlength': 200
    }, {
        'name' : 'C_DATE1',
        'name2': 'C_DATE1',
        'title': '날짜데이터1'
    }, {
        'name' : 'C_DATE2',
        'name2': 'C_DATE2',
        'title': '날짜데이터2'
    }, {
        'name' : 'C_USEGBN',
        'name2': 'C_USEGBN',
        'title': '사용여부'
    }];

    $scope.GCODE = '';
    $scope.codeSearchText = '';
    mainDataService.getCommonCodeGroup({})
    .success(function(data){
    var gcode_list = [];
    	$.each(data,function(idx,Item){
    		gcode_list.push({code:Item.C_GCODE,name:Item.C_GNAME +'('+Item.C_GCODE+')'});	
    	});
    	$scope.GCODE_LIST = gcode_list;
    	console.log($scope.GCODE_LIST);
    	console.log($scope.Code);
    	
    	setGrid();
        gridResize();
    	
    });
    $scope.Code = {}; // $scope.codeList[0];
    $scope.codeListRows = 18;
    $scope.fileListRows = 18;
    // $scope.CodeList = angular.copy($scope.codeList);
    // $scope.CodeInfo = {};
    $scope.isReadonly = true;
    $scope.isDisabled = false;
    $scope.saveType = ""; // 등록(add) 혹은 수정(edit)

    $scope.$on('$viewContentLoaded', function () {
        /*setGrid();
        gridResize();*/
        setDatePickerToday();
       // $rootScope.setBtnAuth($scope.MCODE);
    });

    $scope.showCodeInfoPopup = function(){
    	$("#codeInfoPopup").show();
    }
    
    $scope.hideCodeInfoPopup = function(){
    	alertify.confirm('Info','작성 중인 내용이 저장되지 않았습니다. 정말 취소하시겠습니까?'
			,function(){
			$("#codeInfoPopup").hide();
				alertify.alert('Info','취소되었습니다.',function(){
					return;
				});
				return;
			}
			,function(){
				return;
			});
    }
    $scope.saveCodeInfo = function(){

        if ($scope.Code2.C_GCODE == null || $scope.Code2.C_GCODE == ''){
        	alertify.alert("Info","그룹코드는 필수항목입니다.");
        	return;
        }
        if ($scope.Code2.C_SCODE == null || $scope.Code2.C_SCODE == ''){
        	alertify.alert("Info","코드는 필수항목입니다.");
        	return;
        }
        if ($scope.Code2.C_NAME == null || $scope.Code2.C_NAME == ''){
        	alertify.alert("Info","코드명은 필수항목입니다.");
        	return;
        }
        
        $scope.checkDuplicatedCode($scope.Code2,function(data){
            var flag = false;
        	if($scope.isNew){
        		if(data==[]){
        			flag = $scope.addCode();	
        		}
        		else{
        			alertify.alert("Error","코드값이 존재합니다.",function(){
        				return;
        			});
        			flag=false;
        		}
        	}else{
        		flag = $scope.editCode();
        	}
        	if(flag)
        	$("#codeInfoPopup").hide();
        	
    	});

    }
    
    $scope.newItem = function(){
    	$scope.isNew = true;
    	$scope.saveMode = 'new';
    	//$scope.Code = {};
    	$scope.Code2 = {};
    	$scope.showCodeInfoPopup();
    }
    
    $scope.editItem = function(){
    	$scope.isNew = false;
    	$scope.saveMode = '';
    	$scope.Code2 = angular.copy($scope.Code);
    	$scope.showCodeInfoPopup();
    }
    
    function setGrid() {
        return pagerJsonGrid({
            grid_id    : 'list',
            pager_id   : 'listPager',
            url        : '/common/getCommonCodeList.json',
            condition  : {
                page : 1,
                rows : GridConfig.sizeM,
                gcode: $scope.GCODE,
                name : $scope.codeSearchText,
            },
            rowNum     : GridConfig.sizeM,
            colNames   : ['순번', '그룹코드','그룹코드', '코드', '코드명', '코드설명', '등록/수정일', '등록자', '사용여부',
                '데이터1', '데이터2', '데이터3', '데이터4', '문자데이터1', '문자데이터2', '문자데이터3', '문자데이터4',
                '날짜데이터1', '날짜데이터2','정렬순서', '등록일'],
            colModel   : [
                {
                    name : 'RNUM', // 순번
                    width: 22,
                    index: 'RNUM',
	                resizable: false
                }, {
                    name : 'C_GCODE', // 그룹코드
                    width: 0,
                    index: 'C_GCODE',
                    hidden : true,
	                resizable: false	                
                }, {
                    name : 'GCODE_NM', // 그룹코드
                    width: 80,
	                resizable: false,
	                formatter:function(cellvalue,options,rowdata,action){
	                	if(rowdata.RNUM==null) return '';
	                	else{
	                		try{
	                		var GCODE_NM = $filter('filter')($scope.GCODE_LIST,{code:rowdata.C_GCODE})[0].name;
	                		return GCODE_NM;
	                		}catch(e){
	                			console.log(e);
	                			return rowdata.C_GCODE;
	                		}
	                		//return (GCODE_NM + '(' +  rowdata.C_GCODE +')');
	                	}
	                	
	                	}
                }, {
                    name : 'C_SCODE', // 코드
                    width: 22,
                    index: 'C_SCODE',
	                resizable: false
                }, {
                    name : 'C_NAME', // 코드명
                    width: 53,
                    index: 'C_NAME',
	                resizable: false
                }, {
                    name : 'C_MEMO', // 코드설명
                    width: 53,
                    index: 'C_MEMO',
	                resizable: false
                }, {
                    name : 'C_DT', // 등록/수정일
                    width: 40,
                    index: 'C_DT',
	                resizable: false
                }, {
                    name : 'INSERT_NAME', // 등록자
                    width: 40,
                    index: 'INSERT_NAME',
	                resizable: false
                }, {
                    name : 'C_USEGBN', // 사용여부
                    width: 30,
                    index: 'C_USEGBN',
	                resizable: false
                }, {
                    name  : 'C_VALUE1', // 데이터1
                    width : 0,
                    index : 'C_VALUE1',
                    hidden: true
                }, {
                    name  : 'C_VALUE2', // 데이터2
                    width : 0,
                    index : 'C_VALUE2',
                    hidden: true
                }, {
                    name  : 'C_VALUE3', // 데이터3
                    width : 0,
                    index : 'C_VALUE3',
                    hidden: true
                }, {
                    name  : 'C_VALUE4', // 데이터4
                    width : 0,
                    index : 'C_VALUE4',
                    hidden: true
                }, {
                    name  : 'C_CVALUE1', // 문자데이터1
                    width : 0,
                    index : 'C_CVALUE1',
                    hidden: true
                }, {
                    name  : 'C_CVALUE2', // 문자데이터2
                    width : 0,
                    index : 'C_CVALUE2',
                    hidden: true
                }, {
                    name  : 'C_CVALUE3', // 문자데이터3
                    width : 0,
                    index : 'C_CVALUE3',
                    hidden: true
                }, {
                    name  : 'C_CVALUE4', // 문자데이터4
                    width : 0,
                    index : 'C_CVALUE4',
                    hidden: true
                }, {
                    name  : 'C_DATE1', // 날짜데이터1
                    width : 0,
                    index : 'C_DATE1',
                    hidden: true
                }, {
                    name  : 'C_DATE2', // 날짜데이터2
                    width : 0,
                    index : 'C_DATE2',
                    hidden: true
                }, {
                    name  : 'C_SORT', // 정렬순서
                    hidden: true
                }, {
                    name  : 'INSERT_DT', // 등록일
                    width : 0,
                    index : 'INSERT_DT',
                    hidden: true
                }

            ],
            onSelectRow: function (rowid, status, e) {
                var param = $(this).jqGrid('getRowData', rowid);
                console.log("selectParam === ", param);
                $scope.isReadonly = true;
                $scope.isDisabled = false;
                $scope.Code = param;
                $scope.saveType = '';
                console.log(param.C_GCODE);
                console.log($scope.Code.C_GCODE);
                console.log($scope.Code);
                $scope.$apply('Code');
                
                $timeout(function () {
                	$scope.$apply('Code');
                }, 1000);

                // console.log($scope.isReadonly);
                // console.log($scope.CodeInfo);
                // $('#C_DATE1').datepicker('readonly');
            },
            gridComplete : function() {
				var ids = $(this).jqGrid('getDataIDs');
				$(this).setSelection(ids[0]);
				$scope.currentPageNo=$('#list').getGridParam('page');
				$scope.totalCount = $('#list').getGridParam('records');
				if($('#list').jqGrid('getRowData')[0].RNUM=="") $scope.totalCount = 0;
				$scope.$apply();
			}
        });
    }

    $scope.addCode = function () {
        var item = angular.copy($scope.Code2);
        item.C_GCODE = xssFilter(item.C_GCODE);
        item.C_SCODE = xssFilter(item.C_SCODE);
        item.C_NAME = xssFilter(item.C_NAME);
        item.C_MEMO = xssFilter(item.C_MEMO);
        item.C_CVALUE1 = xssFilter(item.C_CVALUE1);
        item.C_CVALUE2 = xssFilter(item.C_CVALUE2);
        item.C_CVALUE3 = xssFilter(item.C_CVALUE3);
        item.C_CVALUE4 = xssFilter(item.C_CVALUE4);
        // validation check
        if (!ItemValidation(item, $scope.codeItem)) {
            return false;
        }

       /* if ($scope.Code2.C_GCODE == null || $scope.Code2.C_GCODE == ''){
        	alertify.alert("","그룹코드는 필수항목입니다.");
        	return;
        }
        if ($scope.Code2.C_SCODE == null || $scope.Code2.C_SCODE == ''){
        	alertify.alert("","코드는 필수항목입니다.");
        	return;
        }
        if ($scope.Code2.C_NAME == null || $scope.Code2.C_NAME == ''){
        	alertify.alert("","코드명은 필수항목입니다.");
        	return;
        }*/

        $.each($scope.codeItem, function (idx, Item) {
            // item[Item.name2] = item[Item.name2] || ''; // 기존 코드
            // item[Item.name2] = item[Item.name2] || ''; // 기존 코드
            item[Item.name] = item[Item.name] || ''; // name으로 변경해봄
        });

        mainDataService.insertCommonCodeList(item).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                $scope.reloadCode($scope.currentPageNo);
                $scope.Code = {};
                alertify.success('코드가 저장 되었습니다.');
            }
        });
        return true;
    }

    $scope.deleteCode = function () {
        var item = angular.copy($scope.Code);
        console.log(item.C_GCODE);
        /*
         * var list = angular.copy($scope.codeList); var list1 = new Array(); $.each(list,function(index,Item){ if(item.C_GCODE==Item.C_GCODE && item.C_SCODE==Item.C_SCODE ){ //---선택된 항목은 새 List에 추가하지 않음 (삭제코드) }else{ list1.push(Item); } }); $scope.codeList = list1;
         */

        if (item.C_GCODE == '' || item.C_GCODE == undefined || item.C_SCODE == '' || item.C_SCODE == undefined) {
            alertify.alert('','삭제할 코드가 없습니다.');
            return;
        }
        // alertify.confirm('정말 코드를 삭제하시겠습니까? 사용안함은 C_USEGBN값을 N으로 수정하시면됩니다.', function (confirmDelete) {
        alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?', function () {
            // if (confirmDelete) {
                mainDataService.deleteCommonCodeList(item).success(function (obj) {
                    if (!common.isEmpty(obj.errMessage)) {
                        alertify.error('errMessage : ' + obj.errMessage);
                    } else {
                        console.log(obj);
                        $scope.reloadCode($scope.currentPageNo);
                        $scope.Code = {};
                        $('#C_GCODE').attr('disabled', false); // 그룹코드 활성화
                        $('#C_SCODE').attr('disabled', false); // 코드 활성화
                        $scope.saveType = '';

                        alertify.success('삭제되었습니다.');
                    }
                });
            // } else {

            // }
        }, function () {}).set('basic', false);
    }
    
    $scope.checkDuplicatedCode = function(info,callback){
    	console.log(info);
    	//var param = 'C_GCODE='+info.C_GCODE+'&C_SCODE=' + info.C_SCODE;
    	var param = {gcode:info.C_GCODE,scode:info.C_SCODE};
    	mainDataService.getCommonCodeInfo(param)
    	.success(function(data){
    		console.log(data);
    		if(typeof callback=='function')
    			callback(data);
    	});
    }
    
    $scope.editCode = function () {
        var item = $scope.Code2;
        //item.C_GCODE = xssFilter(item.C_GCODE);
        item.C_GCODE = $scope.Code.C_GCODE;
        item.C_SCODE = xssFilter(item.C_SCODE);
        item.C_NAME = xssFilter(item.C_NAME);
        item.C_MEMO = xssFilter(item.C_MEMO);
        item.C_CVALUE1 = xssFilter(item.C_CVALUE1);
        item.C_CVALUE2 = xssFilter(item.C_CVALUE2);
        item.C_CVALUE3 = xssFilter(item.C_CVALUE3);
        item.C_CVALUE4 = xssFilter(item.C_CVALUE4);

        // validation check
        if (!ItemValidation(item, $scope.codeItem)) {
            return false;
        }
        
       /* if ($scope.Code2.C_GCODE == null || $scope.Code2.C_GCODE == ''){
        	alertify.alert("","그룹코드는 필수항목입니다.");
        	return;
        }
        if ($scope.Code2.C_SCODE == null || $scope.Code2.C_SCODE == ''){
        	alertify.alert("","코드는 필수항목입니다.");
        	return;
        }
        if ($scope.Code2.C_NAME == null || $scope.Code2.C_NAME == ''){
        	alertify.alert("","코드명은 필수항목입니다.");
        	return;
        }*/

        $.each($scope.codeItem, function (idx, Item) {
            // item[Item.name2] = item[Item.name2] || ''; // 기존 코드
            item[Item.name] = item[Item.name] || ''; // name으로 수정
        });

        /*
         * $.each($scope.codeList,function(index,Item){ if(item.C_GCODE==Item.C_GCODE && item.C_SCODE==Item.C_SCODE ){ $scope.codeList[index] = angular.copy(item); } });
         */

        mainDataService.updateCommonCodeList(item).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                $scope.reloadCode($scope.currentPageNo);
                $scope.Code = {};
                $scope.isDisabled = false;
                $('#C_GCODE').attr('disabled', false); // 공통코드 활성화
                $('#C_SCODE').attr('disabled', false); // 코드 활성화
                alertify.success('코드가 수정 되었습니다.');
            }
        });
        return true;
    }

    $scope.onKeyPress = function(event){
        if (event.key === 'Enter') {
        	$scope.reloadCode(1);
        }
    }
    
    $scope.reloadCode = function (pageNo) {
        // $scope.codeList = [];// angular.copy($scope.MenuList);
        // mainDataService.getCommonCodeList({
        // 	gcode : $scope.GCODE,
        // 	name : $scope.codeSearchText,
        // 	usegbn : 1
        // }).success(function(obj) {
        // 	if (!common.isEmpty(obj.errMessage)) {
        // 		alertify.error('errMessage : ' + obj.errMessage);
        // 	} else {
        // 		console.log(obj);
        // 		$scope.codeList = obj;
        // 	}
        // });

        $('#list').jqGrid('setGridParam', {
        	page : (typeof pageNo=='undefined')?1:pageNo,
            rows : GridConfig.sizeXL,        	
            postData: {
                gcode: $scope.GCODE || '',
                name : $scope.codeSearchText || ''
            }
        }).trigger('reloadGrid', {
            current: true
        });
    }

    $scope.selectCode = function (item) {

        if (item.C_GCODE == '' || item.C_GCODE == undefined) {
            $scope.MenuEditMode = 1;
        } else {
            $scope.MenuEditMode = 2;
        }

        $scope.Code = angular.copy(item);
        $('#admin_code').dialog({
            title    : '코드 추가',
            autoOpen : true,
            modal    : true,
            height   : $('#dialog-form').height(),
            width    : 750,
            resizable: false
        });
    }

    // '등록' 클릭 시 실행
    $scope.clearCode = function () {
        $scope.Code = {}; // 우측 화면 input 빈 값으로 설정
        $scope.Code.INSERT_NAME = $scope.loginNm;

        // 2000-01-01 타입의 date format
        var today = new Date();
        var year = today.getFullYear();
        var yyyymmdd = year + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.Code.INSERT_DT = yyyymmdd;
        $scope.isReadonly = false; // 우측 화면 입력할 수 있게 함
        $scope.isDisabled = true; // 등록자, 등록일 비활성화
        $('#C_GCODE').attr('disabled', false); // 그룹코드 활성화
        $('#C_SCODE').attr('disabled', false); // 코드 활성화
        $scope.saveType = "add";
        // $('#editMode').hide();
    }

    // '수정모드' 클릭 시 실행
    $scope.editMode = function () {
        var item = $scope.Code;
        console.log(item.C_GCODE);

        if (item.C_GCODE == '' || item.C_GCODE == undefined || item.C_SCODE == '' || item.C_SCODE == undefined) {
            alertify.alert('','수정할 코드가 없습니다.');
            return;
        }

        $scope.isReadonly = false; // 우측 화면 입력할 수 있게 함
        $scope.isDisabled = true; // 등록자, 등록일 비활성화
        $('#C_GCODE').attr('disabled', true); // 공통코드 비활성화
        $('#C_SCODE').attr('disabled', true); // 코드 비활성화
        $scope.saveType = "edit";
        // $('#editMode').hide();
    };

    $scope.save = function () {
        console.log($scope.saveType);
        if ($scope.saveType === 'add') {
        	$scope.checkDuplicatedCode('',function(data){
        		if(data==[])
        			$scope.addCode();
        		else
        			return; 
        	});

        } else if ($scope.saveType === 'edit') {
            $scope.editCode();

        } else {
            alertify.alert('','저장할 내용이 없습니다.');
            return;
        }

    };

    $scope.reloadCode();

    // -------------- menu Manager ----------

    $scope.Menu = {};
    $scope.MenuItem = {};
    $scope.MenuEditMode = 0;
    $scope.popupType = '';
    $scope.maxMcode = 0;
    $scope.maxTsort = 0;
    $scope.SampleMenuList = []; // angular.copy($scope.MenuList);
    // var Scope = angular.element(document.getElementById('dialog-menuPopup')).scope();
    var Scope = null;


    $scope.addMenu = function () {
        var item = angular.copy($scope.MenuItem);
        console.log(item);

        // validation check
        if (!ItemValidation(item, editMenuItem)) {
            return;
        }

        item.M_PART = '01';
        
        item.M_NAME = xssFilter(item.M_NAME);
        item.M_PG_FILE_NAME = xssFilter(item.M_PG_FILE_NAME);
        item.M_NAME = xssFilter(item.M_NAME);
        //item.M_MCODE = xssFilter(item.M_MCODE);
        //item.M_SORT = xssFilter(item.M_SORT);
        
        item.M_NAME2 = item.M_NAME;

        console.log(item);
        // return;

        mainDataService.insertMenu(item).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                // $('#admin_menu').dialog('close');
                // $scope.menuPopupShow = false;
                alertify.success('저장되었습니다');
                $('#dialog-menuPopup').hide();
                $scope.clearMenu();
                $scope.reloadMenu();
            }
        });
    }

    // 엑셀 파일 다운로드
    $scope.getExcelDownload = function() {
    	/*
        const jobType = $state.current.name;
        const levelNm = $scope.levelNm;
        console.log($scope.SearchInfo)
        console.log($state.current.name)
         console.log($scope.levelNm)
        $('#exportForm').find('[name=jobType]').val(jobType);
       // $('#exportForm').find('input[name=username]').val($scope.SearchInfo.username);
       // $('#exportForm').find('input[name=userjob]').val($scope.SearchInfo.E_JOB);
       // $('#exportForm').find('input[name=useraff]').val($scope.SearchInfo.E_AFF);
        $('#exportForm').submit();
        */
    	/*
	<input type="hidden" name="selectName"  />
    <input type="hidden" name="searchOptions"  />
	<input type="hidden" name="startRowIdx" />
	<input type="hidden" name="startColIdx" />
	<input type="hidden" name="columnIdList" />
	<input type="hidden" name="columnNameList" />
	<input type="hidden" name="columnWidthList" />
    	 */
    	var searchOptions = {
                gcode: $scope.GCODE || '',
                name : $scope.codeSearchText || ''    			
    		};
        $('#excelDown').find('[name=selectName]').val("getCommonCodeList");//CommonDao.
        $('#excelDown').find('[name=searchOptions]').val(JSON.stringify(searchOptions));
        $('#excelDown').find('[name=outputFileName]').val(encodeURI("공통코드_"+ $scope.getToday()+".xlsx"));
        $('#excelDown').find('[name=templateFileName]').val("");
        $('#excelDown').find('[name=startRowIdx]').val('1');
        $('#excelDown').find('[name=startColIdx]').val('0');
        $('#excelDown').find('[name=columnIdList]').val("RNUM,C_GCODE2,C_SCODE,C_NAME,C_MEMO,C_DT,INSERT_NAME,C_USEGBN");
        $('#excelDown').find('[name=columnNameList]').val("순번,그룹코드,코드,코드명,코드설명,등록/수정일,등록자,사용여부");
        $('#excelDown').find('[name=columnWidthList]').val("3000");

         $('#excelDown').submit();    	
    };
    
    $scope.addPop = function () {

        var item = angular.copy($scope.Menu); // 선택된 메뉴. 즉, 입력할 서브메뉴의 부모메뉴.
        var addItem = {}; // 저장할 메뉴

        // $scope.menuPopupShow = true;
        $scope.popupType = '추가';
        $scope.Menu = {};
        // $('#dialog-addMenu').show();

        console.log("선택된 메뉴 >>> ", item);
        /*
        '추가'를 눌렀을때
        아무것도 선택 안된 상태에서 누르면 대메뉴 추가됨
        선택된 상태에서 누르면 중메뉴 추가됨
         */

        // $scope.MenuItem.M_MCODE = parseInt($scope.maxMcode) + 1; // M2_MENU의 M_MCODE 중에서 최댓값 + 1
        addItem.M_MCODE = parseInt($scope.maxMcode) + 1; // M2_MENU의 M_MCODE 중에서 최댓값 + 1

        // 아무것도 선택 안 된 상태 -> 대메뉴 추가
        if (item.M_MCODE === undefined) {
            console.log('대메뉴 추가');
            // $scope.MenuItem.M_MCODE = $scope.maxMcode + 1; // M2_MENU의 M_MCODE 중에서 최댓값 + 1

            addItem.M_TSORT = $scope.maxTsort + 1;
            addItem.M_SORT = 1;
            addItem.M_LEVEL = 0;

        // 메뉴가 선택된 상태 -> 중메뉴 또는 소메뉴 추가
       } else {
            // depth 3 이상은 불가능
            if (parseInt(item.M_LEVEL) === 2) {
                alertify.alert('','추가할 수 없습니다.'); // 추후 수정
                return;
            }

            mainDataService.getMaxSort(item).success(function (obj) {
                if (!common.isEmpty(obj.errMessage)) {
                    alertify.error('errMessage : ' + obj.errMessage);
                } else {
                    console.log(obj);
                    // $scope.MenuItem.M_SORT = parseInt(obj) + 1;
                    addItem.M_SORT = parseInt(obj) + 1;
                }
            });

            addItem.M_MCODE = parseInt($scope.maxMcode) + 1; // M2_MENU의 M_MCODE 중에서 최댓값 + 1
            addItem.M_LEVEL = parseInt(item.M_LEVEL) + 1;
            addItem.M_PARENT_MCODE = item.M_MCODE;
            addItem.M_TSORT = item.M_TSORT;

            // console.log($scope.MenuItem);
            // $('#dialog-addMenu').show();
        }

        $scope.MenuItem = addItem;
        // console.log($scope.MenuItem);

        // $('#dialog-addMenu').show();

        // console.log($scope.MenuItem);

        $rootScope.$broadcast('addMenuItem',
            {
                'MenuItem': $scope.MenuItem,
                'popupType': $scope.popupType,
                'reloadMenu': $scope.reloadMenu
            });
        //var Scope = angular.element(document.getElementById('dialog-menuPopup')).scope();
        //Scope.MenuItem = $scope.MenuItem;
        //Scope.popupType = $scope.popupType;
        //$('#dialog-menuPopup').show();
    };

    $scope.$on('addMenuItem', function (event, data) {
        console.log(data);
        $scope.MenuItem = data.MenuItem;
        $scope.popupType = data.popupType;
        $scope.reloadMenu = data.reloadMenu;


        $('#dialog-menuPopup').show();
    });

    $scope.editPop = function () {
        // $scope.crudType = '수정';
        // $scope.alertMessage = '수정하신 자료를 저장 하시겠습니까?'

        var item = angular.copy($scope.Menu);
        if (item.M_MCODE != undefined) {
            $scope.Menu = {};
            // $scope.menuPopupShow = true;
            $scope.popupType = '수정';
            $scope.MenuItem = item;
            $('#dialog-menuPopup').show();

            $rootScope.$broadcast('editMenuItem', {'MenuItem': $scope.MenuItem, 'popupType': $scope.popupType});

        } else {
            alertify.alert('','수정할 내용이 없습니다.'); // 추후 수정
            return;
        }
    };

    $scope.$on('editMenuItem', function (event, data) {
        // console.log(data);
        $scope.MenuItem = data.MenuItem;
        $scope.popupType = data.popupType;
        $('#dialog-menuPopup').show();
    });

    $scope.cancelPop = function () {
        // $scope.popupSectionShow = false;
        // $scope.menuPopupShow = false;
        // $scope.deletePopupShow = false;
        $('#dialog-menuPopup').hide();
        $scope.MenuItem = {};
        $("#clear_wrap_popup").hide();
    }

    // $scope.cancelConfirmPop = function () {
    //     $scope.confirmPopupShow = false;
    // };

    $scope.deleteMenu = function () {
        var item = angular.copy($scope.Menu);
        console.log(item);

        if (item.M_MCODE === undefined) {
            alertify.alert('','삭제할 내용이 없습니다.'); // 추후 수정
            return;
        }

        alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?', function () {
                mainDataService.deleteMenu('M_PART=' + item.M_PART + '&M_MCODE=' + item.M_MCODE).success(function (obj) {
                    if (!common.isEmpty(obj.errMessage)) {
                        alertify.error('errMessage : ' + obj.errMessage);
                    } else {
                        console.log(obj);
                        if (parseInt(obj) > 0) {
                            alertify.success('메뉴가 삭제되었습니다.');
                            // $('#admin_menu').dialog('close');
                            // $scope.confirmPopupShow = false;
                            $scope.reloadMenu();
                            $scope.Menu = {};
                            // $scope.getMenuList();
                        }
                    }
                });
            },
        function () {
        }).set('basic', false);
    }

    var editMenuItem = [{
        'name'     : 'M_PART',
        'title'    : '메뉴구분',
        'maxlength': 2,
    }, {
        'name'     : 'M_MCODE',
        'title'    : '메뉴코드',
        'maxlength': 4,
        'required' : true
    }, {
        'name'     : 'M_LEVEL',
        'title'    : '메뉴레벨',
        'maxlength': 2
    }, {
        'name'     : 'M_TSORT',
        'title'    : '메뉴TOP정렬',
        'maxlength': 38,
        'type'     : 'number'

    }, {
        'name'     : 'M_SORT',
        'title'    : '메뉴정렬',
        'maxlength': 38,
        'type'     : 'number'
    }, {
        'name'     : 'M_NAME',
        'title'    : '메뉴명',
        'maxlength': 30
    }, {
        'name'     : 'M_NAME2',
        'title'    : '메뉴 약식명',
        'maxlength': 30
    }, {
        'name'     : 'M_PG_FILE_NAME',
        'title'    : 'PG파일',
        'maxlength': 200
    }, {
        'name'     : 'M_PARENT_MCODE',
        'title'    : '상위메뉴코드',
        'maxlength': 4
    }, {
        'name'     : 'M_HELP_PAGE',
        'title'    : '도움말 페이지 번호',
        'maxlength': 3,
        'type'     : 'number'
    }]

    $scope.editMenu = function () {
        // var item = '';
        var item = angular.copy($scope.MenuItem);

        /*
         * $.each($scope.SampleMenuList,function(index,menuItem){ if(item.a_MCODE==menuItem.a_MCODE){ $scope.SampleMenuList[index] = angular.copy(item); } });
         */
        // console.log($scope.MenuItem);
        // item = ItemFilter($scope.MenuItem, editMenuItem, false);
        item.M_NAME = xssFilter(item.M_NAME);
        item.M_PG_FILE_NAME = xssFilter(item.M_PG_FILE_NAME);
        item.M_NAME = xssFilter(item.M_NAME);
        item.M_MCODE = xssFilter(item.M_MCODE);
        item.M_SORT = item.M_SORT;
        console.log(item);

        // validation check
        if (!ItemValidation(item, editMenuItem)) {
            return;
        }

        mainDataService.updateMenu(item).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                $scope.MenuItem = {};
                $scope.Menu = {};
                // $scope.menuPopupShow = false;
                $('#dialog-menuPopup').hide();
                alertify.alert('','메뉴가 수정 되었습니다.');
                // $('#admin_menu').dialog('close');
                $scope.reloadMenu();
            }
        });
    }

    $scope.saveMenu = function () {
        if ($scope.popupType === '추가') {
            $scope.addMenu();
        } else if ($scope.popupType === '수정') {
            $scope.editMenu();
        }
    };

    $scope.reloadMenu = function () {
        console.log('reload menu');
        mainDataService.getCommonMenuList({}).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                    $scope.SampleMenuList = [];
                    var mCodeList = [];
                    var tSortList = [];

                    for (var i = 0; i < obj.length; i++) {
                        obj[i].MenuList = [];
                        mCodeList.push(obj[i].M_MCODE); // M_MCODE의 최댓값을 구하기 위해 배열에 넣는다.
                        tSortList.push(obj[i].M_TSORT);

                        for (var j = 0; j < obj.length; j++) {
                            if (obj[i].M_MCODE === obj[j].M_PARENT_MCODE)
                                obj[i].MenuList.push(obj[j]);
                        }
                    }
                }
                var Scope = angular.element(document.getElementById('menu_admin')).scope();
                if(Scope){
                Scope.maxMcode = Math.max.apply(null, mCodeList); // M_MCODE의 최댓값을 스코프 변수에 넣는다.
                Scope.maxTsort = Math.max.apply(null, tSortList);
                Scope.SampleMenuList = obj;
                }
        });
    }


    $scope.selectMenu = function (item) {
        $scope.Menu = {};
        $scope.Menu = item;
    };

    $scope.clearMenu = function () {
        $scope.MenuItem = {};
    }

    /*
     * //관리자메뉴- 메뉴관리 팝업 $('#adMenu').on('click', function() { $('#admin_menu').dialog({ title : '속성 및 이력 정보', autoOpen : true, height : $('#dialog-form').height() + 65, width : 750, modal : true }); });
     */

    $scope.reloadMenu();

    // ---------------------------------------- 권한관리

    $scope.authGroup = [];
    /*
     * {G_GCODE:'G001','G_GNAME':'관리자(모든권한)'}, {G_GCODE:'G002','G_GNAME':'일반사용자 (일반메뉴 등록+조회)'}, {G_GCODE:'G003','G_GNAME':'조회자(일반메뉴조회)'}, {G_GCODE:'G004','G_GNAME':'GIS편집자(일반사용자+GIS편집가능)'}, {G_GCODE:'G011','G_GNAME':'모니터링 전용일반(모니터링메뉴 등록+조회)'}, {G_GCODE:'G012','G_GNAME':'모니터링 전용조회(모니터링메뉴 조회)'}
     */

    $scope.AuthMenuList = [];

    // -----------------------------------------------사용자관리
    $scope.page = 1;
    $scope.rows = 10;
    $scope.User = {};
    $scope.userList = [];
    $scope.userItem = [{
        'name': 'E_ID'
    }, {
        'name': 'E_PW'
    }, {
        'name': 'E_ENUM'
    }, {
        'name': 'E_NAME'
    }, {
        'name': 'E_AFF'
    }, {
        'name': 'E_POS'
    }, {
        'name': 'E_DBIRTH'
    }, {
        'name': 'E_ADDR'
    }, {
        'name': 'E_ZIP'
    }, {
        'name': 'E_JOB'
    }, {
        'name': 'E_PHONE'
    }, {
        'name': 'E_MPHONE'
    }, {
        'name': 'E_FAX'
    }, {
        'name': 'E_EMAIL'
    }, {
        'name': 'E_GCODE1'
    }, {
        'name': 'E_POTO'
    }, {
        'name': 'E_INOFF'
    }, {
        'name': 'E_UDATE'
    }, {
        'name': 'E_UUSER'
    }, {
        'name': 'E_TGRADE'
    }, {
        'name': 'E_SIGN'
    }, {
        'name': 'E_MEMO'
    }];

    function getNextGCode(callback) {
    	mainDataService.getNextGroupCode()
    	.success(function(data){
    		//console.log(data);
			if(typeof callback=='function'){
				//alert(data);
				callback(data.GCODE);
			}
    	});
    }

    $scope.createGroup_Pop = function (callback) {
        var groupName = prompt('그룹이름을 입력해주세요.');
        //debugger;
        if (!common.maxlength('', groupName, 30)) {
            return;
        }
        if (groupName == null) {
            return;
        } else if (!groupName) {
            alertify.alert('','그룹이름을 확인해주세요.');
            return;
        } else {
            getNextGCode(function(nextAuthGroupCd){
            	var g_code = nextAuthGroupCd;
                $scope.authGroup.push({
                    G_GCODE: g_code,
                    G_GNAME: groupName
                });
                alertify.alert('','권한그룹을 추가했습니다.');
            });
        }
    }
    $scope.SelectedGroup = function (item) {
        console.log(item);
        // alert('그릅을 선택함');
        $timeout(function () {
            $scope.reloadAuthMenuList();
        }, 100);
        return true;
    }
    $scope.toggle_Auth_View = function (item) {
        if (item.AuthView == 'Y') {
            item.AuthView = 'N';
        } else {
            item.AuthView = 'Y';
        }
    }
    $scope.toggle_Auth_Modify = function (item) {
        if (item.AuthModify == 'Y') {
            item.AuthModify = 'N';
        } else {
            item.AuthModify = 'Y';
        }
    }
    $scope.toggle_Auth_Stat = function (item) {
        if (item.AuthStat) {
            item.AuthStat = false;
        } else {
            item.AuthStat = true;
        }
    }
    $scope.SaveAuthGroup = function () {
    	console.log($scope.AuthGroup);
    	if($scope.AuthGroup=={} || $scope.AuthGroup == undefined ){
    		alertify.alert('Info','권한그룹을 선택하세요.',function(){
    			
    		});
    		return;
    	}
        var MenuList = [];
        $.each($scope.AuthMenuList, function (idx) {
            var item = $scope.AuthMenuList[idx];
            if (item.AuthStat)
                MenuList.push({
                    'M_MCODE': item['M_MCODE'],
                    'A_VIEW' : item['AuthView'],
                    'A_MANG' : item['AuthModify']
                });
        });

        mainDataService.saveAuthMenuList({
            'G_GCODE'    : $scope.AuthGroup.G_GCODE,
            'G_GNAME'    : $scope.AuthGroup.G_GNAME,
            'M_PART'     : '01',
            'G_MENU_LIST': MenuList
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                alertify.alert('저장되었습니다.');
            }
        });
    }

    $scope.getUserList = function () {
        mainDataService.getEmployeeList({
            page: $scope.page,
            rows: $scope.rows
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                $scope.page = obj.page;
                $scope.total = obj.total;
                $scope.records = obj.records;
                $scope.userList = obj.rows;
            }
        });
    }

    $scope.getUserList();

    $scope.addUser = function () {
        var item = angular.copy($scope.User);
        // $scope.userList.push(item)

        $.each($scope.userItem, function (idx, Item) {
            item[Item.name] = item[Item.name] || '';
        });

        mainDataService.addEmployee(item).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                $scope.getUserList();
            }
        });
    }

    $scope.deleteUser = function () {
        var item = angular.copy($scope.User);
        /*
         * var list = angular.copy($scope.userList); var list1 = new Array(); $.each(list,function(index,Item){ if(Item.E_ID==item.E_ID){ //---선택된 항목은 새 List에 추가하지 않음 (삭제코드) }else{ list1.push(Item); } }); $scope.userList = list1;
         */
        mainDataService.deleteEmployee(item).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                $scope.getUserList();
                $scope.clearUser();
            }
        });

    }

    $scope.editUser = function () {
        var item = $scope.User;

        /*
         * $.each($scope.userList,function(index,Item){ if(item.E_ID==Item.E_ID){ $scope.userList[index] = angular.copy(item); } });
         */

        $.each($scope.userItem, function (idx, Item) {
            item[Item.name] = item[Item.name] || '';
        });
        mainDataService.updateEmployee(item).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                $scope.getUserList();
            }
        });
    }

    $scope.realoadUser = function () {
        // $scope.userList = angular.copy($scope.UserList);
    }

    $scope.selectUser = function (item) {
        $scope.User = angular.copy(item);
    }

    $scope.clearUser = function () {

        $scope.User = {};
    }
    $scope.reloadAuthMenuList = function () {
        // if($scope.loadingEnded)
        var gCode = 'G000';
        if ($scope.AuthGroup != null) {
            gCode = $scope.AuthGroup['G_GCODE'];
        }

        mainDataService.getAuthMenuList('G_GCODE=' + gCode + '&M_PART=01').success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                // console.log(obj);
                $scope.AuthMenuList = [];
                $.each(obj, function (index, item) {
                    item.AuthView = (item.A_VIEW == 'Y') ? 'Y' : 'N';
                    item.AuthModify = (item.A_MANG == 'Y') ? 'Y' : 'N';
                    item.AuthStat = (item.A_MCODE) ? true : false;
                    $scope.AuthMenuList.push(item);
                    // $scope.loadingEnded = true;
                });
            }
        });
    }

    mainDataService.getAuthGroupList({}).success(function (obj) {
        if (!common.isEmpty(obj.errMessage)) {
            alertify.error('errMessage : ' + obj.errMessage);
        } else {
            $scope.authGroup = obj;
        }
    });

    $scope.reloadAuthMenuList();
    // ---------------------------------------------------파일관리자
    $scope.File = {};
    $scope.fileList = [];
    $scope.files;

    $scope.fileItem = [{
        'name' : 'F_NUM',
        'width': '50'
    }, {
        'name' : 'F_TITLE',
        'width': '50'
    }, {
        'name' : 'F_NAME',
        'width': '150'
    }, {
        'name' : 'F_PATH',
        'width': '350'
    }, {
        'name' : 'G_UDATE',
        'width': '100'
    }, {
        'name' : 'G_UUSER',
        'width': '100'
    }];

    $scope.FolderList = [{
        code: '01',
        name: '유량조사'
    }, {
        code: '02',
        name: '수질조사'
    }, {
        code: '03',
        name: '육안조사'
    }, {
        code: '04',
        name: 'CCTV조사'
    }, {
        code: '05',
        name: '송연조사'
    }, {
        code: '06',
        name: '염료조사'
    }, {
        code: '11',
        name: '청소및준설'
    }, {
        code: '21',
        name: '개보수'
    }, {
        code: '31',
        name: '민원'
    }, {
        code: '32',
        name: '사고'
    }, {
        code: '41',
        name: '관로진단'
    }, {
        code: '51',
        name: '교육'
    }, {
        code: '91',
        name: '도서및성과폼'
    }];

    $scope.addFile = function () {
        var item = angular.copy($scope.File);
        // $scope.fileList.push(item)
        // var sendParam = $('#uploadForm').serialize();
        console.log($scope.files);
        mainDataService.fileUpload(($scope.files[0].name), '' + $scope.SelectFolder.code, $scope.files).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                if (parseInt(obj.filenumber) > 0) {
                    alertify.success('파일업로드성공');
                    $scope.realoadFile();
                }
            }
        });
    }

    $scope.deleteFile = function () {
        var item = angular.copy($scope.File);
        /*
         * var list = angular.copy($scope.fileList); var list1 = new Array(); $.each(list,function(index,Item){ if(Item.F_NUM==item.F_NUM){ //---선택된 항목은 새 List에 추가하지 않음 (삭제코드) }else{ list1.push(Item); } }); $scope.fileList = list1;
         */
        mainDataService.fileDelete(item.F_NUM).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                if (parseInt(obj) > 0) {
                    alertify.alert('','파일삭제');
                    $scope.realoadFile();
                }
            }
        });
    }

    $scope.editFile = function () {
        var item = $scope.File;
        /*
         * $.each($scope.userList,function(index,Item){ if(item.F_NUM==Item.F_NUM){ $scope.fileList[index] = angular.copy(item); } });
         */

    }

    $scope.realoadFile = function () {
        // $scope.userList = angular.copy($scope.UserList);
        mainDataService.getFileList({}).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                $scope.fileList = obj;
            }
        });
    }

    $scope.selectFile = function (item) {
        $scope.File = angular.copy(item);
    }

    $scope.clearFile = function () {

        $scope.File = {};
    }

    function readURL(file) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

            } else {
                $scope.$apply();
            }
        };
        reader.readAsDataURL(file);
    }

    $scope.onFileSelect = function ($files, cmd) {

        console.log($files);

        if ($files.length === 0) {
            return;
        }
        if (cmd != 'COM')
            if ($files[0].name.substr(-3) != cmd) {
                alertify.alert('Info', '확장자가 ' + cmd + '인 파일만 선택가능합니다.')
                return;
            }
        // readURL($files[0]);
        // $scope.files = $files[0];
    }

    $scope.Download = function () {
        var url = $scope.File.F_PATH;
        window.open(url, '_filedownload', '');
    }

    $scope.Upload = function () {
        var item = angular.copy($scope.File);
        // $scope.fileList.push(item)
        // var sendParam = $('#uploadForm').serialize();
        console.log($scope.files);
        mainDataService.largeFileUpload(($scope.files[0].name), '' + $scope.SelectFolder.code, $scope.files).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log(obj);
                if (parseInt(obj.filenumber) > 0) {
                    alertify.success('파일업로드성공');
                    $scope.realoadFile();
                }
            }
        });
    }
    // $scope.realoadFile();
    $scope.$on('$destroy', function () {
        // alert('Destroy!!');
        $('#admin_menu').remove();
        $('#admin_code').remove();

    });

}
