angular.module('app.asset').controller('assetInfoController',assetInfoController);
function assetInfoController($scope, $state, $stateParams, mainDataService, $rootScope, $compile,$filter, $timeout, $interval, ConfigService, YearSet, GridConfig) {
	
	$scope.levelInfoList = [];
	$scope.AssetItemInfo = {ASSET_SID:0};
	var assetPopupCnt = 0;

	$scope.$on('ShowAssetItemPopup', function(event, data) {
		assetPopupCnt++;
		// console.log(assetPopupCnt);
		// console.log(data);
		$("#p1_list_history").jqGrid('clearGridData');		
		if (typeof data.AssetItemInfo === 'undefined' || commonUtil.isEmpty(data.AssetItemInfo)) {
			//alertify.alert('오류', '등록된 자산이 없습니다.');
			return;
		}
		var param = {};
		$('#p1_tab1').show();
		$('#p2_tab3').hide();
    	$('#p1_tab1_btn').removeClass('active');
    	$('#p1_tab3_btn').removeClass('active');
    	$('#p1_tab1_btn').addClass('active');
    	oldTabId='p1_tab1';
    	
		$('#dialog-AssetItemPopup').show();
		// console.log(data);
		$scope.AssetItemInfo =	data.AssetItemInfo;
		$scope.AssetSid =  data.AssetItemInfo.ASSET_SID;
		var level_path_nm = "";
		for(var i=1;i<=15;i++){
			if(data.AssetItemInfo['L'+i]!=null){
				level_path_nm += data.AssetItemInfo['L'+i];
				level_path_nm += '>';
			}
		}
		$scope.level_path_nm = level_path_nm;
		
		$scope.fn_name = '';
		try{
		var arr = $scope.level_path_nm.split('>');
			$scope.fn_name = arr[2];
			//alert($scope.fn_name);
		}catch(e){
			
		}
		setGrid2(0);
		param=data; 
		$scope.loadItemInfo(param,$scope);
	});
	
	$scope.popupClose= function(){
		$('#dialog-AssetItemPopup').hide();
		//clearSelectedInfoList();
		//clearSelectedInfoListBroadcast($scope);
		//$rootScope.$broadcast('ClearSelectedInfoList', {});
	}
    var oldTabId="p1_tab1";
    $scope.showTab = function(tabId,event){
    	if(oldTabId==tabId) return;
    	$("#" + oldTabId).hide();	
    	$("#" + tabId).show();
    	oldTabId = tabId;
    	$(event.currentTarget).parent().parent().find('button').removeClass('active');
    	$(event.currentTarget).addClass('active');
    	//setGrid2();
    	setDatePicker();
    	//alert(tabId);
    	if(tabId=='p1_tab3')
    	$scope.search_history();
    	
    	//if(tabId=='tab2')
    	//$scope.search_picture();
    }
    //신규
    $scope.newItem = function(){
    	$scope.saveMode = 'new';
    	$scope.AssetItemInfo.ASSET_SID =  0;
    	$rootScope.$broadcast('RegistAssetItemPopup',{asset_info:$scope.AssetItemInfo,ItemList:angular.copy($scope.ItemList),callback_Id:'ItemList_Add',callback_Fn : function(){
    		$("#list").setGridParam({
    			datatype : 'json',
    			page : $scope.currentPageNo,
    			rows : GridConfig.sizeS,
    			postData : {

    			}
    		}).trigger('reloadGrid', {
    			current : true
    		});
    	}});
    	$scope.popupClose();
    }
    
    //수정
    $scope.editItem = function(){
			angular.element($('#dialog-registAssetItemPopup')).scope().$parent.saveMode = '';
    	$scope.saveMode = '';
    	$scope.AssetItemInfo.ASSET_SID =  $scope.AssetSid;
    	$rootScope.$broadcast('RegistAssetItemPopup',{asset_info:$scope.AssetItemInfo,ItemList:angular.copy($scope.ItemList),callback_Id:'ItemList_Update',callback_Fn : function(){
    		$("#list").setGridParam({
    			datatype : 'json',
    			page : $scope.currentPageNo,
    			rows : GridConfig.sizeS,
    			postData : {

    			}
    		}).trigger('reloadGrid', {
    			current : true
    		});
            var param = {ASSET_PATH_SID:$scope.AssetItemInfo.ASSET_PATH_SID,ASSET_SID: $scope.AssetItemInfo.ASSET_SID};
            $scope.loadItemInfo(param,$scope);
    	}});
    	$scope.popupClose();
    }
        
/*	$scope.loadItemInfo = function(param){
        mainDataService.getAssetItemInfo(param)
        .success(function(data){
        	var list = new Array();
        	console.log(data);
        	$.each(data,function(idx,Item){
        		Item.REQUIRED = (Item.REQUIRED=='Y')?true:false;
        		var dtype = Item.DATA_TYPE.split(':');
            		if(dtype.length>=2){ 
            			Item.maxLen = parseInt(dtype[1]);
                		if(dtype[1].indexOf(".") >= 0 ){
                			var floatLen = dtype[1].split(".");
                			Item.maxLen += parseInt(floatLen[1]) + 1;
                			Item.floatLen = parseInt(floatLen[1]); 
                		}
            		}
        		
        			switch(dtype[0]){
        			case 'number':
        			case 'Num' :
        				if(Item.ITEM_VAL == null) Item.ITEM_VAL="";
        				if(dtype[1].indexOf(".") >= 0 ){
        					Item.ITEM_VAL = (new Intl.NumberFormat('ko-KR', { minimumFractionDigits: Item.floatLen }).format(parseFloat(Item.ITEM_VAL.replace(/,/gi,''))));
        				}else{
        					Item.ITEM_VAL = parseInt(Item.ITEM_VAL.replace(/,/gi,''));	
        				}
        				break;
        			case 'select':
        				Item.REF_CODE_DATA = Item.REF_CODE_DATA.split('/'); 
        				break;
        			case 'select2':
        				console.log(Item.REF_CODE_DATA); 
        				console.log($scope.ComCodeList);
        				if(Item.REF_CODE_DATA.indexOf("::")>0){
        					var REF_CD = Item.REF_CODE_DATA.split("::")[0];
        					var filterObj = {C_MEMO : Item.REF_CODE_DATA.split("::")[1] };
        					Item.REF_CODE_DATA = $filter('filter')($scope.ComCodeList[REF_CD],filterObj);
        				}else{
        					Item.REF_CODE_DATA = $scope.ComCodeList[Item.REF_CODE_DATA];	
        				}
        				break;        				
        			case 'check':
        				var checkedVal = (Item.ITEM_VAL==''||Item.ITEM_VAL==null)?[]:Item.ITEM_VAL.split(',');
        				Item.CheckedItem = new Array();
        				Item.REF_CODE_DATA = Item.REF_CODE_DATA.split('/');
        				$.each(Item.REF_CODE_DATA,function(idx2,checked){
        					Item.CheckedItem.push(($.inArray(checked,checkedVal)<0)?false:true);
        				});
        				break;
        			case 'check2':
        				Item.CheckedItem = [false];
        				if(Item.ITEM_VAL==''||Item.ITEM_VAL==null)
        					Item.CheckedItem[0] = false;
        				else
        					Item.CheckedItem[0] = true;
        				break;        				
        			case 'string' :
        				if(Item.ITEM_VAL == null) Item.ITEM_VAL="";
        				break;
        			case 'date' :
        				if(Item.ITEM_VAL == null || Item.ITEM_VAL=='0000-00-00') Item.ITEM_VAL="";
        				break;
        			default : 
        				break;
        			}
        		console.log(Item);
        		list.push(Item);
        	});
        	$scope.ItemList = list;
        });
		
	}*/
	
	function setGrid2(asset_sid) {
		
        return pagerGrid({
            grid_id : 'p1_list_history',
            pager_id : 'p1_list_historyPager',
            url : '/asset/getSelectAssetHistory.json?AssetSid='+(asset_sid|0),
            condition : {
                page : 1,
                rows : GridConfig.sizeS
            },
            rowNum : GridConfig.sizeS,
            colNames : [ '순번','작업목록','기간','작성자','작성일'],
            colModel : [
            {
                name : 'RNUM',
                width : "40px",
                sortable:false,
                resizable: false
            }, {
                name : 'NAME',
                width : 100,
                hidden :false,
                sortable : false,
                resizable: false
            }, {
                name : 'TERMS',
                width : 0,
                hidden :false,
                sortable:false,
                resizable: false,
                formatter:function(cellvalue,options,rowdata,action){
                	if(rowdata.RNUM==null) return '';
                	else
                	return (rowdata.START_DT + '~' + rowdata.END_DT);
                	}
            }, {
                name : 'INSERT_ID',
                width : 0,
                hidden :false,
                sortable:false,
                resizable: false
            }, {
                name : 'INSERT_DT',
                width : 0,
                hidden :false,
                sortable:false,
                resizable: false
            }],

            onSelectRow : function(id) {
                console.log($('#p1_list_history').jqGrid('getRowData', id));
                
            }
        });
    }
	
    $scope.search_option = 1;
    $scope.search_history_option = function(option){
    	$scope.search_option = option;
    }
    
    $scope.search_picture = function(){
    	mainDataService.getAssetPic({ASSET_SID:$scope.AssetItemInfo.ASSET_SID|0})
    	.success(function(data){
    		console.log(data);
    		$scope.pic_list = data;
    	});
    }
    //$scope.search_option="1";
    $scope.p1SearchYear = ''+(new Date()).getFullYear(); 
    $scope.setChangep1SearchYear = function(val){
    	$scope.p1SearchYear = val;
    }
    $scope.search_history = function(){
    	if($scope.search_option==1){
    		if($scope.p1SearchYear==""){
    			//alert('기간을 입력하 세요');
    			//$("#p1_selectYear").focus();
    			//return;
    			$scope.StartDt="";
    			$scope.EndDt="";
    		}else{
            	$scope.StartDt = $scope.p1SearchYear + "0101"; 
               	$scope.EndDt =  $scope.p1SearchYear + "1231";
    		}
    	}else{
    		if($("#p1_start_dt").val()==""){
    			alertify.alert('오류','기간을 입력하 세요');
    			$("#p1_start_dt").focus();
    			return;
    		}
    		if($("#p1_end_dt").val()==""){
    			alertify.alert('오류','기간을 입력하 세요');
    			$("#p1_end_dt").focus();
    			return;
    		}
        	$scope.StartDt = $("#p1_start_dt").val().replace(/-/gi,''); 
           	$scope.EndDt =  $("#p1_end_dt").val().replace(/-/gi,'');    		
    	}
 
    	$("#p1_list_history").setGridParam({
    		url : '/asset/getSelectAssetHistory.json?AssetSid='+$scope.AssetItemInfo.ASSET_SID,
			datatype : 'json',
			page : 1,
			rows : GridConfig.sizeS,
			postData : {
				ASSET_SID : $scope.AssetItemInfo.ASSET_SID,
				START_DT : $scope.StartDt,
				END_DT : $scope.EndDt
			}				
		}).trigger('reloadGrid', {
			current : true
		});    	
    	
    }
}
