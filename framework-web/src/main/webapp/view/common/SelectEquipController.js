angular.module('app.common').controller('selectEquipController', selectEquipController);

function selectEquipController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService) {
	
	$scope.storePopupEquipmentList = [];
	$scope.storePopupSelectedEquipmentList = [];
	var Scope = null;
	$rootScope.$on('popupEquipList', function(event, data) {
		
		$('#dialog-equip').dialog({
			title : '장비 선택',
			autoOpen : true,
			modal : true,
			height : $('#dialog-equip').height(),
			width : 1000,
			resizable : false
		});
		Scope = angular.element(document.getElementById('dialog-equip')).scope();
		Scope.searchEquipName = '';
		Scope.storePopupEquipmentList = [];
		Scope.storePopupSelectedEquipmentList = [];
		$.each(data.data, function(idx, Item) {
			if (parseInt(Item.EM_NUM) > 0)
				Scope.storePopupSelectedEquipmentList.push(Item)
		});
		
		Scope.JNUM = data.JNUM;
		Scope.callback = data.callback;
		var checkList = [];
		$.each(Scope.storePopupSelectedEquipmentList, function(idx, Item) {
			checkList.push(Item.EM_NUM);
		});
		mainDataService.getEquipMasterPopList({
			em_type : '4',
			em_name : ''
		}).success(function(data) {
			if (!common.isEmpty(data.errMessage)) {
				alertify.error('errMessage : ' + data.errMessage);
			} else {
				console.log(data);
				$.each(data, function(idx, Item) {
					if ($.inArray(Item.EM_NUM, checkList) >= 0) {
						
					} else {
						Item.EM_check = false;
						Scope.storePopupEquipmentList.push(Item);
					}
					
				});
			}
		});
		
	});
	
	$scope.closeEquipPopup = function() {
		$('#dialog-equip').dialog('close');
	}

	$scope.addSelectedEquipList = function() {
		var list = [];
		$.each(Scope.storePopupEquipmentList, function(idx, Item) {
			console.log(Item);
			if (Item.EM_check) {
				var item = angular.copy(Item)
				Scope.storePopupSelectedEquipmentList.push(item);
			} else {
				list.push(Item);
			}
		});
		Scope.storePopupEquipmentList = list;
	}

	$scope.removeSelectedEquipList = function() {
		list = [];
		$.each(Scope.storePopupSelectedEquipmentList, function(idx, Item) {
			if (Item.EM_check) {
				var item = angular.copy(Item)
				Scope.storePopupEquipmentList.push(item);
			} else {
				list.push(Item);
			}
		});
		Scope.storePopupSelectedEquipmentList = list;
	}

	$scope.saveEquipList = function() {
		var jnum = Scope.JNUM;
		var je_enum;
		var list = Scope.storePopupSelectedEquipmentList;
		console.log(list);
		if (list.length == 0) {
			alertify.alert('장비를 선택해주세요');
			return;
		} else {
			// DB에 있는 장비를 모두 삭제
			mainDataService.deleteFcResultEquipAll({
				jnum : jnum
			}).success(function(obj) {
				var list = Scope.storePopupSelectedEquipmentList;
				$.each(list, function(idx, Item) {
					je_enum = Item.EM_NUM;
					mainDataService.saveFcResultEquipList({
						jnum : jnum,
						je_enum : je_enum
					});
					
				});
				$('#EquipmentList').removeClass('noData');
			});
			Scope.callback(angular.copy(Scope.storePopupSelectedEquipmentList));
		}
		
		/*
		 * $scope.storeEquipmentList = angular.copy($scope.storePopupSelectedEquipmentList); for(var i=0; i < list.length ; i++){ $scope.storeEquipmentList[i].RNUM = i+1; }
		 */

		$('#dialog-equip').dialog('close');
	}

	$scope.check_all = function(id) {
		var boolVal = $('#' + id).is(':checked');
		for (var i = 0; i < Scope.storePopupEquipmentList.length; i++)
			Scope.storePopupEquipmentList[i].EM_check = boolVal;
	}
	$scope.check_all2 = function(id) {
		var boolVal = $('#' + id).is(':checked');
		for (var i = 0; i < Scope.storePopupSelectedEquipmentList.length; i++)
			Scope.storePopupSelectedEquipmentList[i].EM_check = boolVal;
	}
}