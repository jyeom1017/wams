angular.module('app.common').controller('selectUserController', selectUserController);

function selectUserController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService) {
	$scope.searchUserName = '';
	var Scope = null;
	$rootScope.$on('popupEmployeeList', function(event, data) {
		$('#dialog_commonUserList').dialog({
			title : '담당자 선택',
			autoOpen : true,
			modal : true,
			height : 450,
			width : 500,
			resizable : false
		});
		Scope = angular.element(document.getElementById('dialog_commonUserList')).scope();
		// console.log(event);
		Scope.storeEmployeeList = data.rows;
		Scope.callbackId = data.callbackId;// 'selectUserID_1'
		Scope.searchUserName = '';
		// $('#searchUserName').val('');
		
	});
	
	$scope.selectUser = function() {
		var ID = $("input[name='selectUserID']:checked").val();
		var name = '';
		
		if (!ID) {
			alertify.alert('담당자를 선택하십시오');
			return;
		}
		$.each(Scope.storeEmployeeList, function(idx, Item) {
			if (Item['E_ID'] == ID) {
				name = Item['E_NAME'];
				return false;
			}
		});
		
		$('#dialog_commonUserList').dialog('close');
		
		$rootScope.$broadcast(Scope.callbackId, {
			E_ID : ID,
			E_NAME : name
		});
	}

	// 직급코드 로딩
	var posCodeList = [];
	mainDataService.getCommonCodeList({
		gcode : '501',
		gname : ''
	}).success(function(data) {
		if (!common.isEmpty(data.errMessage)) {
			alertify.error('errMessage : ' + data.errMessage);
		} else {
			posCodeList = data;
		}
	});
	
	$scope.UserPOS = function(val) {
		var ret = val;
		$.each(posCodeList, function(i, item) {
			if (item.C_SCODE == val) {
				ret = item.C_NAME;
				return false;
			}
		});
		return ret;
	}

	$scope.selectUser1 = function(index) {
		$('#radio' + index).prop('checked', true);
	}

	$scope.canclePop = function() {
		$('#dialog_commonUserList').dialog('close');
	}
}