angular.module('app.common').controller('gisPopupController', gisPopupController);


function gisPopupController($scope, $state, $stateParams, mainDataService,
  $rootScope, $compile, $timeout, $interval, ConfigService) {

  $scope.MapItem = [];
  $scope.SelectMapItem = [];
  var JNUM = null;
  var JOPT = null;
  var callback_id = null;
	// $rootScope.$on('gisPopup', function(event, data) {
	// $('#dialog-gis').dialog({
	// title: '시설물 선택(GIS)',
	// autoOpen: true,
	// modal: true,
	// height: 600,
	// width: 800,
	// resizable : false
	// });
	// JNUM = data.JNUM;
	// JOPT = data.JOPT;
	// callback_id = data.callback_id;
	// $scope.SelectMapItem = [];
	//  });
  
  /*
  SB300	처리구역
  SB103	우수토실
  SB003	하수연결관
  SB200	하수처리장
  SB101	하수맨홀
  SB001	하수암거
  SB002	하수개거
  SB210	하수펌프장
  SB104	토구
  SB102	물받이
  SA117	유량계
   */

  $scope.MapItem.push({
    name: '맨홀1',
    x: 100,
    y: 300,
    color: '#ff00ff',
    width: 20,
    height: 10,
    FTR_CDE: 'SB101',
    FTR_IDN: '1'
  });
  $scope.MapItem.push({
    name: '맨홀2',
    x: 300,
    y: 400,
    color: '#00ff00',
    width: 30,
    height: 30,
    FTR_CDE: 'SB101',
    FTR_IDN: '2'
  });
  $scope.MapItem.push({
    name: '관로3',
    x: 400,
    y: 500,
    color: '#00ffff',
    width: 20,
    height: 20,
    FTR_CDE: 'SB001',
    FTR_IDN: '3'
  });
  $scope.MapItem.push({
    name: '관로4',
    x: 500,
    y: 300,
    color: 'red',
    width: 30,
    height: 30,
    FTR_CDE: 'SB001',
    FTR_IDN: '4'
  });
  $scope.MapItem.push({
    name: '맨홀5',
    x: 600,
    y: 50,
    color: '#000000',
    width: 30,
    height: 30,
    FTR_CDE: 'SB101',
    FTR_IDN: '5'
  });
  $scope.MapItem.push({
    name: '맨홀6',
    x: 150,
    y: 50,
    color: '#000000',
    width: 30,
    height: 30,
    FTR_CDE: 'SB101',
    FTR_IDN: '6'
  });

  $scope.mapLocation = function(item) {
    return {
      position: 'absolute',
      'left': item.x + 'px',
      'top': item.y + 'px',
      'background-color': item.color
    };
  }

  $scope.toggleSelect = function(item) {

    var checkList = [];
    $.each($scope.SelectMapItem, function(idx, Item) {
      checkList.push(Item.FTR_IDN);
    });

    if ($.inArray(item.FTR_IDN, checkList) >= 0) {
      var list = [];
      $.each($scope.SelectMapItem, function(idx, Item) {
        if (Item.FTR_IDN == item.FTR_IDN) {

        } else {
          list.push(Item);
        }
        $scope.SelectMapItem = list;
      });

    } else {
      $scope.SelectMapItem.push(item);
    }

  }
  
  	$scope.saveSelectMapItem2 = function(JOPT,JNUM,data){ 
  		$scope.SelectMapItem = JSON.parse(data);
		
    $.each($scope.SelectMapItem, function(idx, Item) {
        var jnum = JNUM;
        var jopt = JOPT;
        var ftr_cde = Item.ftrcde;
        var ftr_idn = '' + Item.ftridn;
        var jg_gnum = $('#selectedGroup').val();

        mainDataService.saveFcResultFac({
            jnum: jnum,
            ftr_cde: ftr_cde,
            ftr_idn: ftr_idn,
            jm_jopt: jopt,
            jg_gnum: jg_gnum
          })
          .success(function(data) {
            console.log('saveFcResultFac');
            console.log(data);
          });
      });
    
   	$timeout(function() {
        //loadFacList($scope.ResultInfo.JNUM);
        $rootScope.$broadcast(callback_id, {
          JNUM: JNUM
        });
      }, 1000);    
	
	}

  $scope.saveSelectMapItem = function() {
    //alert(JSON.stringify($scope.SelectMapItem));
    $.each($scope.SelectMapItem, function(idx, Item) {
      var jnum = JNUM;
      var jopt = JOPT;
      var ftr_cde = Item.FTR_CDE;
      var ftr_idn = Item.FTR_IDN;
      var jg_gnum = $('#selectedGroup').val();

      mainDataService.saveFcResultFac({
          jnum: jnum,
          ftr_cde: ftr_cde,
          ftr_idn: ftr_idn,
          jm_jopt: jopt,
          jg_gnum: jg_gnum
        })
        .success(function(data) {
          console.log('saveFcResultFac');
          console.log(data);
        });
    });

    $timeout(function() {
      //loadFacList($scope.ResultInfo.JNUM);
      $rootScope.$broadcast(callback_id, {
        JNUM: JNUM
      });
    }, 1000);
    $('#dialog-gis').dialog('close');
  }
  /*
  $scope.getGIS = function(){
  	console.log('$('#selectedGroup').val()');
  		var selectedGroup = parseInt($('#selectedGroup').val());
  		console.log(selectedGroup);
  		if(!(selectedGroup >0)){
  			alertify.alert('Group을 선택해주세요');
  			return;
  		}
  	    $( '#dialog-gis' ).dialog({title: '시설물 선택(GIS)',autoOpen: true,height:600,width: 800,modal: true});
  		$scope.SelectMapItem = [];
  }

  $scope.showGIS = function(){
  	    $( '#dialog-gisView' ).dialog({title: 'GIS 위치보기',autoOpen: true,height:600,width: 800,modal: true});
  	    var list = [];
  	    $.each($scope.storeFacilityList, function(idx, item){
  	    	if(item.checked){
  	    		list.push({FTR_IDN:item.FTR_IDN,FTR_CDE:item.FTR_CDE});
  	    	}
  	    });
  	    //console.log(list);
  	    alertify.alert(JSON.stringify(list));
  }
  */

	  $interval(function() {
		// console.log('completeFlag:' + $('#completeflag').val());
		if ($('#completeflag').val() == '1') {
			$('#completeflag').val('');
			callback_id = gisParam.callbackId.value;
			var jopt = gisParam.jopt.value;
			var JNUM = gisParam.JNUM.value;
			var data = gisParam.data.value;
			var imptType = gisParam.imptType.value;
			console.log(jopt);
			console.log(JNUM);
			console.log(data);

			if (imptType == 'importantFac') {

				$scope.importantFacTemp = JSON.parse(data);
				$('#TEMP_FTR_CDE').val($scope.importantFacTemp[0].ftrcde);
				$('#TEMP_FTR_IDN').val($scope.importantFacTemp[0].ftridn);

				$rootScope.$broadcast('tempFtrIdn', $scope.importantFacTemp[0].ftridn);

				$('input[name=imptType]').val('');

			} else {
				$scope.saveSelectMapItem2(jopt, JNUM, data);
			}
			// console.log('saveSelectMapItem2');
		}

	}, 1000);
}
