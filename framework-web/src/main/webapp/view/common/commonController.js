angular.module('app.common').controller('CommonMessagePopupController', CommonMessagePopupController);
angular.module('app.common').controller('SelectAssetLevelPathController', SelectAssetLevelPathController);
angular.module('app.common').controller('SelectAssetBaseInfoController', SelectAssetBaseInfoController);
angular.module('app.common').controller('SelectAssetListController', SelectAssetListController);

function SelectAssetListController($scope, $state, $stateParams, mainDataService,
		$rootScope, $compile, $filter,$timeout, $interval, ConfigService) {
	$scope.Init = false;
	$scope.param = {};
	$scope.totalCountAssetListPopup =0;
	$scope.currentPageNoAssetListPopup = 1; 
	$scope.$on('SelectAssetListPopup', function(event, data) {
		$scope.param = data;	
		$("#dialog-AssetListPopup").show();
		console.log(event);
		console.log(data);
		//$scope.param = data;
		/*
		mainDataService.getFindAssetList(param)
		.success(function(data){
			$scope.AssetList = data;
		});
		$timeout(function() {
			setDatePicker();
		}, 1000);
		*/
		if($scope.Init){
			setGrid2(data);
			/*
			$("#list_popupAssetList").setGridParam({
	    		datatype : 'json',
				page : $scope.currentPageNoAssetListPopup,
				rows : 15,
				multiselect : (data.opt==3)?false:true,
				postData : data,
				/ *
	            gridComplete : function() {
					$scope.currentPageNoAssetListPopup=$('#list_popupAssetList').getGridParam('page');
					$scope.totalCountAssetListPopup = $('#list_popupAssetList').getGridParam("records")
					$scope.$apply();
				}* /				
	        }).trigger('reloadGrid');
			*/
		}else{
			setGrid2(data);
			$scope.Init = true;
		}
			
		
	});
	function getLevelName(pathCd,level){
		//console.log($rootScope.LevelName);
		//console.log(pathCd,level);
		var LEVEL_CD = pathCd.split('_')[level-1];
		if(LEVEL_CD!=''){
			var LevelName = $rootScope.LevelName[''+(level -1) +'_'+ LEVEL_CD];
			return LevelName;
		}else{
			return "";
		}
	}
	//list_popupAssetList
	function setGrid2(data) {
		$("#list_popupAssetList").jqGrid("GridUnload");
		$timeout(function(){
			$("#cb_list_popupAssetList").css("position","relative").css("left","20px");
		},100);
        return pagerJsonGrid({
            grid_id : 'list_popupAssetList',
            pager_id : 'list_popupAssetListPager',
            url : '/asset/getSelectAssetItemList.json',
            multiselect : (data.opt==3)?false:true,
            condition : $scope.param,
            rowNum : 100,
            rows : 100,
            colNames : [ '순번','asset_path_sid','asset_sid','자산명','자산코드','pathcd','레벨1','레벨2','레벨3','레벨4','레벨5','레벨6','레벨7','레벨8','시설/관망','준공년도','위치/장소','내용연수','초기비용','감가상각비율(%)','대블록','중블록','소블록'],
            colModel : [
            {
                name : 'RNUM',
                width : 20
            }, {
                name : 'ASSET_PATH_SID',
                width : 50,
                hidden : true
            }, {
                name : 'ASSET_SID',
                width : 20,
                hidden : true
            }, {
                name : 'ASSET_NM',
                width : 80,
            }, {
                name : 'ASSET_CD',
                width : "100px",
            },{
                name : 'PATH_CD',
                width : 0,
                hidden: true
            }, {
                name : 'L1',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,1);}
            }, {
                name : 'L2',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,2);}
            }, {
                name : 'L3',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,3);}
            }, {
                name : 'L4',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,4);}
            }, {
                name : 'L5',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,5);}
            }, {
                name : 'L6',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,6);}
            }, {
                name : 'L7',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,7);}
            }, {
                name : 'L8',
                width : 100,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,8);}
            }, {
                name : 'FN_CD',
                width : 100,
                hidden: true,
                formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,3);}
            }, {
                name : 'INSTALL_DT',
                width : 70,
                hidden: true,
            }, {
                name : 'PLACE',
                width : 70,
                hidden: true,
            }, {
                name : 'LIFE',
                width : 70,
                hidden: true,
            }, {
                name : 'INIT_AMT',
                width : 70,
                hidden: true,
            }, {
                name : 'A',
                width : 70,
                hidden: true,
            }, {
                name : 'DAE',
                width : 70,
                resizable:false,
                formatter:function(cellvalue, options, rowdata, action){
                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
            		try{
            			return $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:cellvalue})[0].C_NAME;
            		}catch(ex){
            			return "";
            		}
                }                
            }, {
                name : 'JUNG',
                width : 70,
                resizable:false,
                formatter:function(cellvalue, options, rowdata, action){
                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
            		try{
            			return $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:cellvalue})[0].C_NAME;
            		}catch(ex){
            			return "";
            		}
                }                
            }, {
                name : 'SO',
                width : 50,
                resizable:false,
                formatter:function(cellvalue, options, rowdata, action){
                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
                	try{
                		return $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:cellvalue})[0].C_NAME;
            		}catch(ex){
            			return "";
            		}	                		
                }	
            }
            
            ],
            mtype: "POST",       //요청방식 GET/POST
            onSelectRow : function(id) {
            	
                $scope.AssetItemInfo = $('#list_popupAssetList').jqGrid('getRowData', id);
                console.log($scope.AssetItemInfo);
            },
            gridComplete : function() {
            	$scope.currentPageNoAssetListPopup=$('#list_popupAssetList').getGridParam('page');
				$scope.totalCountAssetListPopup = $('#list_popupAssetList').getGridParam("records")
				$scope.$apply();
			}             
        });
    }	
	
	
	
	$scope.popupClose = function(){
		$("#dialog-AssetListPopup").hide();
	}
	
	$scope.applySelected = function(){
		//$("#dialog-AssetListPopup").hide();
		var list = [];
		/*
		$.each($scope.AssetList,function(idx,Item){
			if(Item.IsSelected)
				list.push({SID:Item.ASSET_SID,ASSET_NM:Item.ASSET_NM,ASSET_CD:Item.ASSET_CD});
		});
		*/
		if($scope.param.opt==3){
			list.push({SID : $scope.AssetItemInfo.ASSET_SID,ASSET_NM :$scope.AssetItemInfo.ASSET_NM,ASSET_CD:$scope.AssetItemInfo.ASSET_CD}); 
			$scope.param.list = list;
		}else{
		var selectedList = $('#list_popupAssetList').jqGrid('getGridParam','selarrrow'); 
		$.each(selectedList,function(idx,item){
			var RowData = $('#list_popupAssetList').jqGrid('getRowData', item);
			list.push({SID:RowData.ASSET_SID,ASSET_NM:RowData.ASSET_NM,ASSET_CD:RowData.ASSET_CD});
		});
		$scope.param.list = list;
		}
		$rootScope.$broadcast($scope.param.callbackId,angular.copy($scope.param));
		$scope.param.idx = $scope.param.idx + list.length; 
	}
	
	
}

angular.module('app.common').controller('RegistAssetItemController', RegistAssetItemController);

function RegistAssetItemController($scope, $state, $stateParams, mainDataService,
		$rootScope, $compile, $filter, $timeout, $interval, ConfigService) {
	$scope.filtername = "";
	//$scope.testlist = new Array();
	//for(var i=0;i<30;i++)
	//$scope.testlist.push({test:i});
	
	$scope.$on('RegistAssetItemPopup', function(event, data) {
		$("#dialog-registAssetItemPopup").show();
		//console.log(event);
		console.log(data);
		//자산수정 인경우 
		if(data.asset_info.ASSET_SID > 0){
			//$("#gisLocationInfo").show();
			$scope.saveMode='';
		}else{ //자산 신규인경우
			$scope.saveMode='new';
			$("#gisLocationInfo").hide();
		}
		
		$("#dispLocationInfo").text("");
		$("#gisPopup").hide();
		
		$scope.level_path_nm = '';
		var levels = ['L1','L2','L3','L4','L5','L6','L7','L8','L9','L10','L11','L12','L13','L14','L15'];
		for (var i in levels) {
			if(typeof data.asset_info[levels[i]]!='undefined' && data.asset_info[levels[i]]!='' && data.asset_info[levels[i]]!=null )
				$scope.level_path_nm += '>' + data.asset_info[levels[i]];
		}
		// 첫번째 기호 제거
		$scope.level_path_nm = $scope.level_path_nm.replace('>', '');
		//$scope.alertMessage= data.alertMessage;
		//$scope.crudType = data.crudType;
		//alert('showCommonMessagePopoup');
		//$scope.$apply();
		
		$scope.fn_name = '';
		try{
		var arr = $scope.level_path_nm.split('>');
			$scope.fn_name = arr[2];
			//alert($scope.fn_name);
		}catch(e){
			
		}
		
		$scope.callback_Fn = (typeof data.callback_Fn=='function')?data.callback_Fn:function(){};
		if(typeof data.asset_info=='undefined'){
			$scope.asset_info = {ASSET_SID:0};
			$scope.asset_path_sid = data.ItemList[0].ASSET_PATH_SID;
		}else{
			$scope.asset_info = data.asset_info;
			$scope.asset_path_sid = data.asset_info.ASSET_PATH_SID;
			$("#dispLocationInfo").text("");
			$("#dispLocationGid").val("");			
			var param = {ASSET_SID:$scope.asset_info.ASSET_SID,ASSET_PATH_SID:$scope.asset_path_sid};
			console.log(param);
			mainDataService.getAssetGisInfo(param).success(function(data) {
				console.log("getAssetGisInfo");
				console.log(data);
				//layer_cd 편집기사용 기본값 설정용
				$scope.asset_info.LAYER_CD = data.LAYER_CD;
				
				$scope.asset_info.LAYER = data.LAYER;
				$scope.asset_info.FTR_IDN = data.FTR_IDN;
				$("#dispLocationInfo").text(data.GEOM_TEXT);
				$("#dispLocationGid").val(data.GID);
			});
		}
		//alert(data.callback_Id);
		if (data.callback_Id != 'ItemList_Update') {
			data.ItemList = $scope.clearData(data.ItemList);
		}
		//alert($scope.asset_info.ASSET_SID);
		//alert($scope.asset_path_sid);
		$scope.loadItemInfo({ ASSET_PATH_SID: $scope.asset_path_sid, ASSET_SID: $scope.asset_info.ASSET_SID}, $scope,function($scope) {
			$scope.updateAssetCd($scope.asset_path_sid,$scope.asset_info.ASSET_SID,function(data){
				//alert(data[0].LEVEL_PATH_CD);
				if($scope.ItemList[1].ITEM_VAL=='')
				$scope.ItemList[1].ITEM_VAL = data[0].LEVEL_PATH_CD.replace(/_/gi,'') + ('000000' +  $scope.asset_info.ASSET_SID ).substr(-6);
			});

			$timeout(function(){
				setDatePicker({yearRange: 'c-100:c+10'});	
				//ITEM_10009 FTR_IDN 속성값을 readonly로 변경
				$("#Item_10009").attr('readonly',true);
				$("#Item_10010").attr('readonly',true);
				$("#Item_10009").attr('disabled','disabled');
				$("#Item_10010").attr('disabled','disabled');
				$(".boxs.box-scroll.p-2")[0].scroll(0,0);
				$(".conts_box.box-scroll.p-2.mt-1")[0].scroll(0,0);			
			},500);
		}); // $scope.ItemList = data.ItemList;
		$scope.callback_Id = data.callback_Id;
		/*
		$timeout(function() {
			setDatePicker();
			
			$scope.updateAssetCd($scope.asset_path_sid,$scope.asset_info.ASSET_SID,function(data){
				//alert(data[0].LEVEL_PATH_CD);
				if($scope.ItemList[1].ITEM_VAL=='')
				$scope.ItemList[1].ITEM_VAL = data[0].LEVEL_PATH_CD.replace(/_/gi,'') + ('000000' +  $scope.asset_info.ASSET_SID ).substr(-6);
			});
			
			//ITEM_10009 FTR_IDN 속성값을 readonly로 변경
			$("#Item_10009").attr('readonly',true);
			$("#Item_10010").attr('readonly',true);
			$("#Item_10009").attr('disabled','disabled');
			$("#Item_10010").attr('disabled','disabled');
			
			$(".boxs.box-scroll.p-2")[0].scroll(0,0);
			$(".conts_box.box-scroll.p-2.mt-1")[0].scroll(0,0);
		}, 100);
		*/
	});
	$scope.showGisPopup = function() {
		/*
		$rootScope.$broadcast('showGisPopup', {
			init: function() {
				var layer_name = $scope.asset_info.LAYER;
				var ftr_idn = '\'' + $scope.asset_info.FTR_IDN + '\'';

				showAsset(layer_name, ftr_idn);
			},
			callbackId: 'RegistAssetItem_selectGisInfo'
		});
		*/
		//debugger;
		$rootScope.$broadcast('showGisPopup', {
			ASSET_INFO : $scope.asset_info,
			gisPopupMode : 'gisEdit',
			init: function() {
				var layer_name = $scope.asset_info.LAYER;
				var ftr_idn = '\'' + $scope.asset_info.FTR_IDN + '\'';
				
				switch($scope.asset_info.LAYER_CD){
				case "FA" : 
					$("#editTarget").val("ams:wtl_fire_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("소방시설");
					break;
				case "F6":
					$("#editTarget").val("ams:wtl_flow_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("유량계");
					break;
				case "F8":
					$("#editTarget").val("ams:wtl_manh_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("상수맨홀");
					break;
				case "FB":
					$("#editTarget").val("ams:wtl_meta_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("수도미터");
					break;
				case "P4":
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA004':LineString");
					$("#dispEditTargetNm").text("배수관로");
					break;
				case "P3":
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA003':LineString");
					$("#dispEditTargetNm").text("송수관로");
                	break;
				case "P2":
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA002':LineString");
					$("#dispEditTargetNm").text("도수관로");
            		break;
				case "P1":
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA001':LineString");
					$("#dispEditTargetNm").text("취수관로");
            	break;
				case "P5":                
					$("#editTarget").val("ams:wtl_pipe_lm:use_yn=true AND saa_cde='SAA005':LineString");
					$("#dispEditTargetNm").text("급수관로");
            	break;
				case "F4":                
					$("#editTarget").val("ams:wtl_pres_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("가압장");
            	break;
				case "F5":                
					$("#editTarget").val("ams:wtl_rsrv_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("저수조");
            	break;
				case "FC":                
					$("#editTarget").val("ams:wtl_stpi_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("스탠드파이프");
            	break;
				case "F9":                
					$("#editTarget").val("ams:wtl_valv_ps:use_yn=true:Point");
					$("#dispEditTargetNm").text("변류시설");
            	break;
				                
				}
                
				if($("#dispLocationInfo").text()!=''){
					$("#start_edit_mode").show();
					$("#gotoLocationBtn").hide();
					showAsset(layer_name, ftr_idn);	
				}else{
					$("#start_edit_mode").hide();
					$("#gotoLocationBtn").show();					
				}

				
			},
			callbackId: 'RegistAssetItem_ItemCoordinate'
		});		
	};
	$scope.$on('RegistAssetItem_ItemCoordinate',function(event,data){
		console.log('RegistAssetItem_ItemCoordinate');
    	console.log(data);
    	var coord_type='POINT';
    	var coord_string = '';
    	if($scope.asset_info.LAYER.toUpperCase() == 'WTL_PIPE_LM' ){
    		coord_type='MULTILINESTRING';
    	}
    	$.each(data.data.coordinate,function(idx,value){
    		if(idx>0 && idx%2==0) coord_string +=",";
    		coord_string += "" + value + " ";
    	});
    	if(coord_type == 'POINT' ){
    		var list = coord_string.split(",");
    		coord_string = coord_type + "(" + list[list.length-1] + ")";
    	}else{
    		coord_string = coord_type + "((" + coord_string + "))";	
    	}
    	$("#dispLocationInfo").text(coord_string);
    });

	$scope.popupClose = function() {
		console.log($scope);
		if($scope.saveMode=='newGisAssetItem'){
			alertify.confirm('삭제확인','등록한자산을 삭제할까요?',function(){
				$rootScope.$broadcast('removeGisAsset',{FLAG : true,ASSET_SID: $scope.asset_info.ASSET_SID,GID:$("#dispLocationGid").val(),LAYER:$scope.asset_info.LAYER,FTR_IDN:$scope.asset_info.FTR_IDN});	
			},function(){
				$rootScope.$broadcast('removeGisAsset',{FLAG : false,ASSET_SID: $scope.asset_info.ASSET_SID,GID:$("#dispLocationGid").val(),LAYER:$scope.asset_info.LAYER,FTR_IDN:$scope.asset_info.FTR_IDN});
				alertify.success('지우지 않음');
			});
		}
		alertify.confirm('Info','작성 중인 내용이 저장되지 않았습니다. 정말 취소하시겠습니까?'
			,function(){
				//$scope.userModalSelet =false;
				$('#dialog-registAssetItemPopup').hide();
				alertify.alert('Info','취소되었습니다.',function(){
					return;
				});
				return;
			}
			,function(){
				return;
			});
	};

	$scope.clearData = function(dataList) {

		var list = [];
		try {
			$.each(dataList, function(idx, Item) {
				Item.ITEM_VAL = '';
				var dtype = Item.DATA_TYPE.split(':');
				switch (dtype[0]) {
					case 'number':
					case 'Num' :
						if (dtype[1].indexOf('.') >= 0) {
							if (Item.ITEM_VAL == '') {
								Item.ITEM_VAL = 0.0;
							}
							Item.ITEM_VAL = (new Intl.NumberFormat('ko-KR', { minimumFractionDigits: Item.floatLen }).format(Item.ITEM_VAL));
						} else {
							if (Item.ITEM_VAL == '') {
								Item.ITEM_VAL = 0;
							}
							Item.ITEM_VAL = parseInt(new Intl.NumberFormat('ko-KR', { minimumIntegerDigits: 1 }).format(Item.ITEM_VAL));
				}
				break;
			
			case 'check' :
				$.each(Item.CheckedItem,function(idx){
					Item.CheckedItem[idx] = false;
				});
				break;
			case 'check2' :
					Item.CheckedItem[0] = false;
				break;
			case 'readonly' :
				Item.ITEM_VAL = ""; 
				break;
			default : 
				break;
			}
			list.push(Item);
		});
		}catch(e){
			console.log(e);
		}
		dataList = list;
		return list;
	}
	$scope.InputValidate = function(dataList,callback){
		var msg = "";
		var error = false;
		//new Intl.NumberFormat('ko-KR', { style: 'currency', currency: 'KRW' }).format(number)
		//new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(number)
		// https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat
		// new Intl.NumberFormat('en-IN', { minimumFractionDigits: 2 }).format(number)
		
		//debugger;
		$.each(dataList,function(idx,Item){
			if(Item.ITEM_CD=='10004' ){
				var flag = false;
				if(Item.ITEM_VAL=='시설'){
					//alert($scope.level_path_cd);
					/*
				$.each(dataList,function(idx2,Item2){
					if(Item2.ITEM_CD=='10001' && Item2.ITEM_VAL.substr){
						flag=true; 
						error = true;
						msg = '';
					}
				});
				if(flag) return false;
				*/
				}else{
					//alert($scope.level_path_cd);
					/*
					$.each(dataList,function(idx2,Item2){
						if(Item2.ITEM_CD=='10008' && Item2.ITEM_VAL != '0000-00-00'){
							flag=true; 
							error = true;
							msg = '폐기 일자를 삭제하세요';
						}
					});
					*/					
				}
			}
			if(error) return false;
			if(Item.ITEM_CD=='10350' ){
				var flag = false;
				if(Item.ITEM_VAL=='폐기'){
				$.each(dataList,function(idx2,Item2){
					if(Item2.ITEM_CD=='10008' && Item2.ITEM_VAL == '0000-00-00'){
						error = true;
						msg = '폐기 일자를 입력하세요';
						return false;
					}
				});
					if(!checkDate($('#Item_10004').val(), $('#Item_10008').val(), '')){
						error = true;
						msg = '설치 일자와 폐기 일자를 확인해주세요';
						return false;
					}

				// if(flag) return false;

				}else{
					$.each(dataList,function(idx2,Item2){
						if(Item2.ITEM_CD=='10008' && Item2.ITEM_VAL != '0000-00-00'){
							flag=true; 
							error = true;
							msg = '폐기 일자를 삭제하세요';
						}
					});					
				}
			}
			if(error) return false;
			var dtype=Item.DATA_TYPE.split(':');
			console.log(Item);
			if(Item.REQUIRED){
				console.log(Item);
				if((''+Item.ITEM_VAL).trim()=='null' || (''+Item.ITEM_VAL).trim()=='' || (''+Item.ITEM_VAL).trim()=='NaN' || ((dtype[0].indexOf('number')>=0 || dtype[0].indexOf('Num')>=0)  && (typeof Item.ITEM_VAL== 'undefined') )){
					msg = Item.ITEM_NM +'은 필수항목 입니다';
					//msg = '필수입력항목을 입력하십시오';
					error = true;
					return false;
				}
			}			
			
			switch(dtype[0]){
				case 'number' :
				case 'Num' :
				if(Item.ITEM_VAL == null) Item.ITEM_VAL="";
				Item.ITEM_VAL = (''+Item.ITEM_VAL).trim(); 
				if(Item.ITEM_VAL=="") break;
				
				if(dtype[1].indexOf(".") >= 0 ){
					if(Item.ITEM_VAL=='') Item.ITEM_VAL=0.0;
					Item.ITEM_VAL = (new Intl.NumberFormat('ko-KR', { minimumFractionDigits: Item.floatLen }).format(parseFloat((''+Item.ITEM_VAL).replace(/,/gi,''))));
				}else{
					if(Item.ITEM_VAL=='') Item.ITEM_VAL=0;
					Item.ITEM_VAL = (new Intl.NumberFormat('ko-KR', { minimumIntegerDigits: 1 }).format(parseInt((''+Item.ITEM_VAL).replace(/,/gi,''))));
				}
				var chktest = /^[+-]?\d*(\.?\d*)$/;
				if(chktest.test(Item.ITEM_VAL.replace(/,/gi,''))!=true){
					flag=true; 
					error = true;
					msg = '잘못된 입력 값이 있습니다.숫자로 입력하세요';
					return false;
				}				
				break;
			case 'select':
			case 'select2':
				break;
			case 'check':
				Item.ITEM_VAL = "";
				var cnt = 0;
				$.each(Item.REF_CODE_DATA,function(idx2,checked){
					if(Item.CheckedItem[idx2]){
					if(cnt>0) Item.ITEM_VAL +=','; 
					Item.ITEM_VAL += checked;
					cnt = cnt+1;
					}
				});
				break;
			case 'check2':
				if(Item.CheckedItem[0])
					Item.ITEM_VAL = Item.ITEM_NM;
				else
					Item.ITEM_VAL = '';	
				break;
			case 'string' :
				break;
			case 'date':
				var flag = true;
				if(Item.ITEM_VAL != '' && Item.ITEM_VAL != null)
				checkDateFormat(Item.ITEM_VAL, function(callback) {
					flag = callback;
				});
				if(!flag){
					error = true;
					msg = Item.ITEM_NM +'은(는) 올바른 날짜형식이 아닙니다.';
					return false;
				}
				if(Item.ITEM_VAL=='') Item.ITEM_VAL='0000-00-00';
			default : 
				break;
			}			
			
			
		});
		if(error){
			callback(msg);
			return true;
		}
		return false;
	}

	//asset_cd 값을 검증 빈값이면 asset_path_sid 기준으로 생성
	$scope.updateAssetCd = function(asset_path_sid,asset_sid,callback){
		var param = {ASSET_PATH_SID:asset_path_sid};
		mainDataService.getAssetPathInfo(param)
		.success(function(data){
			console.log(data);
			if(typeof callback=='function')
				callback(data,asset_sid);	
		});
	}
	
	$scope.saveConfirm = function(){
		var ASSET_INFO = angular.copy($scope.ItemList);
		if($scope.InputValidate(ASSET_INFO,function(msg){
			alertify.alert('Info',msg);
		})){
			return; 
		}
		
		//$("#dialog-registAssetItemPopup").hide();
		$scope.asset_path_sid = $scope.ItemList[0].ASSET_PATH_SID;
		var param = {ASSET_PATH_SID:$scope.asset_path_sid, ASSET_SID : $scope.asset_info.ASSET_SID,ASSET_INFO:ASSET_INFO,ASSET_CD:'',ASSET_NM:xssFilter(ASSET_INFO[0].ITEM_VAL)
				//,GEOM_TEXT : $("#dispLocationInfo").text() 
				};
		
		param.ASSET_CD_CHANGE = ASSET_INFO[1].ITEM_VAL;
		
		if($scope.asset_info.ASSET_SID==0){
			//신규등록
			mainDataService.insertAssetItemInfo(param)
			.success(function(data){
				alertify.alert("저장","신규 자산을 등록했습니다.",function(){});
				$("#dialog-registAssetItemPopup").hide();
				$rootScope.$broadcast($scope.callback_Id,{RESULT:'ID_OK'});
				$scope.callback_Fn();				
			});			
		}else{
			//정보수정
			$("#dialog-registAssetItemPopup").hide();
			mainDataService.updateAssetItemInfo(param)
			.success(function(data){
				$rootScope.$broadcast($scope.callback_Id,{RESULT:'ID_OK'});
				$scope.callback_Fn();
				alertify.success("변경된 정보가 저장 되었습니다.");
			});			
		}

	}
	
	//자산위치등록
	$scope.showSelectGisPopup = function(){
		var param = {ASSET_SID:$scope.asset_info.ASSET_SID};
		console.log(param);
		mainDataService.getAssetGisInfo(param)
		.success(function(data){
			console.log(data);
		});
    	$rootScope.$broadcast('showGisPopup',{callbackId:'RegistAssetItem_selectGisInfo'});
    	$("#dialog-registAssetItemPopup").hide();
	}
    $scope.$on('RegistAssetItem_selectGisInfo',function(event,data){
    	console.log(data);
    	//$scope.asset_path_sid = data.ASSET_PATH_SID;
    	//$scope.level_path_cd = data.LEVEL_PATH_CD;
    	//$scope.level_path_nm = data.LEVEL_PATH_NM;
    	alert('test');
    	
    	/*
    	var obj = angular.copy(getSelectGisObj());
    	console.log(obj);
    	var param = {ASSET_SID:$scope.asset_info.ASSET_SID, FTR_IDN:obj[0].ftr_idn,LAYER_CD : obj[0].fid}; 
    	mainDataService.insertAssetGis(param)
    	.success(function(data){
    		console.log(data);
    	});*/
    	$("#dialog-registAssetItemPopup").show();
    });	
	
    //자산경로선택
    $scope.showPopupLevelPath = function ()
    {
    	//alert('showpopup')
    	$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'RegistAssetItem_selectAssetLevelPath',namefilter:$scope.filtername,returnType:'single'});
    }
    
    $scope.$on('RegistAssetItem_selectAssetLevelPath',function(event,data){
    	console.log(data);
    	$scope.asset_path_sid = data.ASSET_PATH_SID;
    	$scope.level_path_cd = data.LEVEL_PATH_CD;
    	$scope.level_path_nm = data.LEVEL_PATH_NM.replace('>','');
    	//mainDataService.getInfoItemList({TEMPLATE_SID:2})
		var param = {ASSET_SID:$scope.asset_info.ASSET_SID,ASSET_PATH_SID:$scope.asset_path_sid};
		console.log(param);
		$("#dispLocationInfo").text("");
		$("#dispLocationGid").val("");		
		mainDataService.getAssetGisInfo(param).success(function(data) {
			console.log("getAssetGisInfo");
			console.log(data);
			//layer_cd 편집기사용 기본값 설정용
			$scope.asset_info.LAYER_CD = data.LAYER_CD;
			$scope.asset_info.LAYER = data.LAYER;
			$scope.asset_info.FTR_IDN = data.FTR_IDN;
			$("#dispLocationInfo").text(data.GEOM_TEXT);
			$("#dispLocationGid").val(data.GID);
		});
    	mainDataService.getAssetItemInfo({ASSET_PATH_SID:$scope.asset_path_sid})
    	.success(function(data){
    		var list = new Array();
        	console.log(data);
        	$.each(data,function(idx,Item){
        		Item.REQUIRED = (Item.REQUIRED=='Y')?true:false;
        		var dtype = Item.DATA_TYPE.split(':');
        		if(dtype.length>=2){ 
        			Item.maxLen = parseInt(dtype[1]);
            		if(dtype[1].indexOf(".") >= 0 ){
            			var floatLen = dtype[1].split(".");
            			Item.maxLen += parseInt(floatLen[1]) + 1;
            			Item.floatLen = parseInt(floatLen[1]);
            		}
        		}
        		
        			switch(dtype[0]){
        			case 'number':
        			case 'Num' :
        				if(Item.ITEM_VAL == null) Item.ITEM_VAL="";
        				if(dtype[1].indexOf(".") >= 0 ){
        					Item.ITEM_VAL = (new Intl.NumberFormat('ko-KR', { minimumFractionDigits: Item.floatLen }).format(Item.ITEM_VAL.replace(/,/gi,'')));
        				}else{
        					Item.ITEM_VAL = parseInt(Item.ITEM_VAL.replace(/,/gi,''));	
        				}
        				break;
        			case 'select':
        				Item.REF_CODE_DATA = Item.REF_CODE_DATA.split('/'); 
        				break;
        			case 'select2':
        				console.log(Item.REF_CODE_DATA); 
        				console.log($scope.ComCodeList);
        				if(Item.REF_CODE_DATA.indexOf("::")>0){
        					var REF_CD = Item.REF_CODE_DATA.split("::")[0];
        					var filterObj = {C_MEMO : Item.REF_CODE_DATA.split("::")[1] };
        					Item.REF_CODE_DATA = $filter('filter')($scope.ComCodeList[REF_CD],filterObj);
        				}else{
        					Item.REF_CODE_DATA = $scope.ComCodeList[Item.REF_CODE_DATA];	
        				}  
        				break;        				
        			case 'check':
        				var checkedVal = (Item.ITEM_VAL==''||Item.ITEM_VAL==null)?[]:Item.ITEM_VAL.split(',');
        				Item.CheckedItem = new Array();
        				Item.REF_CODE_DATA = Item.REF_CODE_DATA.split('/');
        				$.each(Item.REF_CODE_DATA,function(idx2,checked){
        					Item.CheckedItem.push(($.inArray(checked,checkedVal)<0)?false:true);
        				});
        				break;
        			case 'check2':
        				Item.CheckedItem = [false];
        				if(Item.ITEM_VAL==''||Item.ITEM_VAL==null)
        					Item.CheckedItem[0]=false;
        				else
        					Item.CheckedItem[0]=true;
        				break;
        			case 'readonly':
        				if(Item.ITEM_CD=='10003'){
        				$scope.fn_name = '';
        				try{
        				var arr = $scope.level_path_nm.split('>');
        					$scope.fn_name = arr[2];
        				}catch(e){
        				}
        				if(Item.ITEM_VAL == null) Item.ITEM_VAL = $scope.fn_name;
        				}
        				break;
        			case 'string' :
        				if(Item.ITEM_VAL == null) Item.ITEM_VAL="";
        				break;        				
        			case 'date' : 
        				if(Item.ITEM_VAL == null || Item.ITEM_VAL=='0000-00-00') Item.ITEM_VAL="";
        			default : 
        				break;
        			}
    				if(Item.ITEM_CD=='10001'){
    					Item.ITEM_VAL = ($scope.level_path_cd + ('000000'+$scope.asset_info.ASSET_SID).substr(-6)).replace(/_/gi,'');
    				}
        		console.log(Item);
        		list.push(Item);
        	});
        	$.each(list,function(idx,item){
        		$.each($scope.ItemList,function(idx2,Item){
        			if(item.ITEM_CD == Item.ITEM_CD && Item.ITEM_CD != '10001' && Item.ITEM_CD != '10003'){
        				item.ITEM_VAL = Item.ITEM_VAL;
        				return false;
        			}	
        		});
        	});
        	$scope.ItemList = list;
        	$scope.$apply();
    		$timeout(function() {
    			setDatePicker();
				//ITEM_10009 FTR_IDN 속성값을 readonly로 변경
				$("#Item_10009").attr('readonly',true);
				$("#Item_10010").attr('readonly',true);
				$("#Item_10009").attr('disabled','disabled');
				$("#Item_10010").attr('disabled','disabled');
				$(".boxs.box-scroll.p-2")[0].scroll(0,0);
				$(".conts_box.box-scroll.p-2.mt-1")[0].scroll(0,0);    			
    		}, 500);    		
    	});
    });
	
}

function CommonMessagePopupController($scope, $state, $stateParams, mainDataService,
	$rootScope, $compile, $filter, $timeout, $interval, ConfigService) {

	$scope.$on('ShowCommonMessagePopup', function(event, data) {
		$("#dialog-commonMessagePopup").show();
		console.log(event);
		console.log(data);
		$scope.callback_Id = data.callback_Id;
		$scope.alertMessage= data.alertMessage;
		$scope.crudType = data.crudType;
		//alert('showCommonMessagePopoup');
	});

	$scope.popupClose = function(){
		("#dialog-commonMessagePopup").hide();
	}

	$scope.selectConfirm = function(){
		$("#dialog-commonMessagePopup").hide();
		$rootScope.$broadcast($scope.callback_Id,{RESULT:'ID_OK'});
	}
	
	$scope.$on('ShowCommonProgressBar', function(event, data) {
		$("#dialog-progressBar").show();
		//$scope.callback_Id = data.callback_Id;
		//$scope.callback_fnc = data.callback_fnc;
		$scope.alertMessage= data.alertMessage;
		$scope.crudType = data.crudType;		
		$scope.setProgressBar(0);	  
	});

	$scope.setProgressBar = function(val){
		$( "#progressbar" ).progressbar({value: val});		
	}

}





function SelectAssetLevelPathController($scope, $state, $stateParams, mainDataService,
  $rootScope, $compile, $filter, $timeout, $interval, ConfigService) {
	
	$scope.template_html = "./view/common/SelectAssetLevelPath.html";
	var lastTreeNodeId = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	var treeNodeId = 0;
	
		$scope.param = {};
		$rootScope.LevelName = {};
		$scope.ap_sid = 0;
		$scope.namefilter = "";
		$scope.treeNode = new Array();
		$scope.AssetLevelPathList = {};
		$scope.AseetLevelInfo = {};
		$scope.lpath = new Array();
	
	$scope.lpath_init=function(){
	treeNodeId = 0;		
	lastTreeNodeId = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	var i =0;
	for(i=0;i<15;i++)
	$scope.lpath[i]= new Array();
	$scope.treeNode = new Array();
	
	}
	
	$scope.popupClose = function(){
		//$scope.template_html = "";
		$("#dialog-assetlevelpath").hide();
		$scope.ap_sid = 0;
		var last_list = $(".last_list");
		last_list.find(".blockName").removeClass("active_color");		
	}
	$scope.selectConfirm = function(){
		if($scope.returnType=='single' && ($scope.ap_sid==0 || $scope.ap_sid==null)){
			alertify.alert('Error','자산 경로를 선택해주세요.',function(){});
			return;
		}
		var name_path = "";
		var cd_path = "";
		//console.log($scope.lpath);
		//console.log($scope.treeNode);
		console.log($scope.lastNid);
		var param = $scope.treeNode[$scope.lastNid-1];
		console.log(findPath(param.lv,param.nId,''));
		console.log(findPath(param.lv,param.nId,'cd'));
		//$scope.returnType // single(pathSid,pathCd),multi(pathCd)
		
		if($scope.returnType=='path'){
			cd_path = findPath(param.lv,param.nId,'cd');
			name_path = findPath(param.lv,param.nId,'');
			$scope.ap_sid = 0;
		}
			
		if($scope.returnType=='single')
		$.each($scope.AssetLevelPathList,function(idx,Item){
			if(Item.ASSET_PATH_SID==$scope.ap_sid){
				var path = Item.LEVEL_PATH_CD.split("_"); 
				for(var level=0;level<15;level++){
					if(path[level]=='') break;
					name_path +=">"+$rootScope.LevelName[level+"_"+path[level]];	
				}
				cd_path = Item.LEVEL_PATH_CD;
				return false;
			}
		});
		console.log($scope.callbackId);
		console.log(name_path);
		console.log(cd_path);
		if($scope.returnType=='multi'){
		var list = new Array();
		$.each($scope.treeNode,function(idx,Item){
			if(Item.IsChecked){
				cd_path = findPath(Item.lv,Item.nId,'cd');
				name_path = findPath(Item.lv,Item.nId,''); 
				list.push({	ASSET_PATH_SID : 0,
					LEVEL_PATH_CD : cd_path,
					LEVEL_PATH_NM : name_path
					});
			}
		});

			$scope.param.list = list;
			$rootScope.$broadcast($scope.callbackId, angular.copy($scope.param));			
		}else{
			$scope.param.ASSET_PATH_SID = $scope.ap_sid;
			$scope.param.LEVEL_PATH_CD = cd_path;
			$scope.param.LEVEL_PATH_NM = name_path;
			
			$rootScope.$broadcast($scope.callbackId, angular.copy($scope.param));				
		}
		//$scope.template_html = "";
		$("#dialog-assetlevelpath").hide();
	}
	
	
	mainDataService.getAssetLevelList({})
	.success(function(data){
		// console.log(data);
		$scope.AseetLevelInfo = data;
		$.each($scope.AseetLevelInfo,function(idx,Item){
			$rootScope.LevelName[''+(Item.LEVEL_STEP -1) +'_'+ Item.LEVEL_CD] = Item.LEVEL_NM;
		});
		
	});

	
	$scope.getAssetLevelPath = function(callback){
		var param={namefilter : $scope.namefilter };
		mainDataService.getAssetLevelPath(param)
		.success(function(data){
			//console.log(data);
			$scope.AssetLevelPathList = data;
			if(typeof callback == 'function') callback();
		});
	}
	/*
	$scope.getAssetLevelList = function(LevelStep){
		var list = [];
		$.each($scope.AssetLevelPathList,function(idx,Item){
			if(LevelStep==Item.LEVEL_STEP)
			list.push();
		});
		return list;
	}
	*/
	$scope.lastNid = 0;
	$scope.toggleTreeNode = function(level,nId,ap_sid){
		
		var last_list = $(".last_list");
		last_list.find(".blockName").removeClass("active_color");
		$("#path_li_"+nId).find(".blockName").addClass("active_color");
		$scope.ap_sid = ap_sid;
		
		$scope.lastNid = nId;
		console.log(ap_sid);
		if(typeof ap_sid=='undefined'){
			if($("#path"+nId).find('li').length==0) 
				$("#path"+nId).append(MakeTreeHTML(level,nId));
			
			$("#path"+nId).collapse("toggle");
			if($("#path_li_"+nId).find('a').attr('aria-expanded')==='false'){
				$("#path_li_"+nId).find('a').attr('aria-expanded','true');
			}else{
				$("#path_li_"+nId).find('a').attr('aria-expanded','false');	
			}
		}else{
			var last_list = $(".last_list");
			last_list.find(".blockName").removeClass("active_color");
			$("#path_li_"+nId).find(".blockName").addClass("active_color");
			$scope.ap_sid = ap_sid;
		}
	}
	
	function MakeTreeHTML(LevelStep,nId){
		
		var sHTML = "";
		//if(LevelStep > 11)  return "";
		$.each($scope.treeNode,function(idx,Item){
				if(Item.pId == nId){
					var isLeaf = !(typeof Item['ap_sid'] == 'undefined');
					sHTML += "<li id='path_li_"+ Item.nId + "'>";
					sHTML += "<a data-toggle='collapse' class='"+((isLeaf)?"last_list":"last_list")+"'  " +((isLeaf)?"":"aria-expanded='false'") + " ng-click='toggleTreeNode("+(LevelStep+1)+","+Item.nId+ ((isLeaf)?(","+Item.ap_sid):"")+")' >";
					sHTML += "<span class='blockName'> &nbsp;&nbsp;&nbsp;"; 
					sHTML += $rootScope.LevelName[LevelStep+'_'+Item.lcd]+"</span>";
					sHTML += "</a> ";
					if($scope.returnType == 'multi'){
						if(isLeaf)
							sHTML += "<input type=checkbox class='checkTreeNode' ng-model=\"treeNode[" + (Item.nId-1) + "].IsChecked\" />";
						else
							sHTML += "<input type=checkbox class='checkTreeNode1' ng-model=\"treeNode[" + (Item.nId-1) + "].IsChecked\" />";
					}
					sHTML += "</li>";
					sHTML += "<ul id='path"+Item.nId+"' class='list-group panel-collapse collapse' style='padding-left:20px !important;'>";			
					sHTML += "</ul>";					
					/*
					if(LevelStep<6){
					sHTML += "<ul id='node"+Item.nId+"' class='list-group panel-collapse collapse' style='padding-left:20px !important;'>";			
					sHTML += MakeTreeHTML(LevelStep+1,Item.nId);
					sHTML += "</ul>";
					}
					*/
				}

		});
		var template = angular.element(sHTML);
		var linkFunction = $compile(template);
		linkFunction($scope);
		return template;
	}
	
	function findPath(LevelStep,nId,type){
	if (LevelStep< 0 ) return "";
	var isFind = false;
	var pathcd = "";
	var pathname = "";
	var pId = "";
		$.each($scope.treeNode,function(idx,Item){
			if(Item.nId == nId){
				isFind = true;
				pId = Item.pId;
				pathname = $rootScope.LevelName[LevelStep+'_'+Item.lcd];
				pathcd = Item.lcd;
				return false;
			}
		});
		if(isFind){
			if(type=='cd'){
				if(LevelStep> 0)
					return findPath(LevelStep-1,pId,'cd') +'_' + pathcd;
				else
					return pathcd;
			}
			else{
				if(LevelStep> 0)
					return findPath(LevelStep-1,pId,'') +'>'+ pathname;
				else
					return pathname;
			}
		}	
		 

	}
	
	function addPath(lpath,asset_path_sid){
		var i=0;
		var Path = lpath.split('_');
		var Path1 = "";
		
		if($scope.namefilter!=""){
			var name_path = "";
			for(var level=0;level<15;level++){
				if(Path[level]=='') break;
				name_path +=">"+$rootScope.LevelName[level+"_"+Path[level]];	
			}			
			if(name_path.indexOf($scope.namefilter) < 0) return;
		}
		
		for(level=0;level<15;level++){
			if(Path[level]=='') break;
			Path1 += Path[level]+"_";
			if($.inArray(Path1,$scope.lpath[level])<0){
				var pid = (level>0)?lastTreeNodeId[level-1]:0; 
				treeNodeId++;
				$scope.lpath[level].push(Path1);
				if(Path[level+1]==''){
					$scope.treeNode.push({pId:pid,nId:treeNodeId,lv:level,lcd:Path[level],ap_sid:asset_path_sid});
				}else{
					$scope.treeNode.push({pId:pid,nId:treeNodeId,lv:level,lcd:Path[level]});	
				}
				lastTreeNodeId[level]=treeNodeId;
			}
		}
			
	}
	
	$scope.initAssetPathControl = function(){

		if(typeof $scope.returnType=='undefined') $scope.returnType='single';
		$scope.getAssetLevelPath(function(){
			
			if($("#path0").find('li').length==0){
			$scope.lpath_init();
			//var i=0;
			$.each($scope.AssetLevelPathList,function(idx,Item){
				//console.log(Item.LEVEL_PATH_CD);
				addPath(Item.LEVEL_PATH_CD,Item.ASSET_PATH_SID);
			});
			}
			//console.log($scope.treeNode);
			if($("#path0").find('li').length==0){
				var sHTML = ""
				sHTML +="<div id='path_li_0' class='blockPanel-heading'> ";
				sHTML +="<a data-toggle='collapse' aria-expanded='true' onclick='toggleTreeNode(0,0)'  class='collapsed'>";
				sHTML +="<span class='blockName'>&nbsp;&nbsp;&nbsp;자산인벤토리경로</span>";
				sHTML +="</a>";
				sHTML +="</div>";
				sHTML +="<ul id='path0' class='list-group panel-collapse collapse show'>";
				sHTML +="</ul>";
				
				$("#Id_assetLevelPath").html(sHTML);
				$("#path0").append(MakeTreeHTML(0,0));
				$(".side_nav").show();
			}
		});			
	}
	$scope.$on('loadAssetLevelPathList', function(event, data) {
		//$scope.template_html = "./view/common/SelectAssetLevelPath.html";
		console.log(event);
		console.log(data);
		$scope.callbackId = data.callbackId;
		$scope.param = data;
		
		//if($scope.namefilter!=data.namefilter){
			$("#path0").remove();						
		//}
			/*
		if($scope.returnType = data.returnType){
			$("#path0").remove();
		}*/
			
		$scope.namefilter = data.namefilter;
		$scope.returnType = data.returnType; // single(pathSid,pathCd),multi(pathCd)		
		$timeout(function(){
			$scope.initAssetPathControl();	
			$("#dialog-assetlevelpath").show();
		},500);
		
	});

}

function SelectAssetBaseInfoController($scope, $state, $stateParams, mainDataService,
		  $rootScope, $compile, $filter, $timeout, $interval, ConfigService) {
			
			$scope.template_html = "./view/common/SelectAssetBaseInfo.html";
			var lastTreeNodeId = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
			var treeNodeId = 0;
			
				$scope.param = {};
				$scope.BaseInfoName = {};
				$scope.ap_sid = 0;
				$scope.namefilter = "";
				$scope.treeNode = new Array();
				$scope.AssetLevelPathList = {};
				$scope.AseetLevelInfo = {};
				$scope.lpath = new Array();
			
			$scope.lpath_init=function(){
			treeNodeId = 0;		
			lastTreeNodeId = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
			var i =0;
			for(i=0;i<15;i++)
			$scope.lpath[i]= new Array();
			$scope.treeNode = new Array();
			
			}
			
			$scope.popupClose = function(){
				//$scope.template_html = "";
				$("#dialog-assetbaseinfo").hide();
				$scope.ap_sid = 0;
				var last_list = $(".last_list");
				last_list.find(".blockName").removeClass("active_color");		
			}
			$scope.selectConfirm = function(){
				if($scope.returnType=='single' && ($scope.ap_sid==0 || $scope.ap_sid==null)){
					alertify.alert('Error','자산 기준정보를 선택해주세요.',function(){});
					return;
				}
				var name_path = "";
				var cd_path = "";
				//console.log($scope.lpath);
				//console.log($scope.treeNode);
				console.log($scope.lastNid);
				var param = $scope.treeNode[$scope.lastNid];
				console.log(findPath(param.lv,param.nId,''));
				console.log(findPath(param.lv,param.nId,'cd'));
				//$scope.returnType // single(pathSid,pathCd),multi(pathCd)
				
				if($scope.returnType=='path'){
					cd_path = findPath(param.lv,param.nId,'cd');
					name_path = findPath(param.lv,param.nId,'');
					$scope.ap_sid = 0;
				}
					
				if($scope.returnType=='single')
				$.each($scope.AssetBaseInfoList,function(idx,Item){
					if(Item.BASE_SID==$scope.ap_sid){
						var path = Item.ASSET_CD.split("_"); 
						for(var level=0;level<15;level++){
							if(path[level]=='') break;
							name_path +=">"+$scope.BaseInfoName[level+"_"+path[level]];	
						}
						cd_path = Item.ASSET_CD;
						return false;
					}
				});
				console.log($scope.callbackId);
				console.log(name_path);
				console.log(cd_path);
				if($scope.returnType=='multi'){
				var list = new Array();
				$.each($scope.treeNode,function(idx,Item){
					if(Item.IsChecked){
						cd_path = findPath(Item.lv,Item.nId,'cd');
						name_path = findPath(Item.lv,Item.nId,''); 
						list.push({	ASSET_PATH_SID : 0,
							LEVEL_PATH_CD : cd_path,
							LEVEL_PATH_NM : name_path
							});
					}
				});

					$scope.param.list = list;
					$rootScope.$broadcast($scope.callbackId, angular.copy($scope.param));			
				}else{
					$scope.param.ASSET_PATH_SID = $scope.ap_sid;
					$scope.param.LEVEL_PATH_CD = cd_path;
					$scope.param.LEVEL_PATH_NM = name_path;
					
					$rootScope.$broadcast($scope.callbackId, angular.copy($scope.param));				
				}
				//$scope.template_html = "";
				$("#dialog-assetbaseinfo").hide();
			}
			
			
			mainDataService.getAssetBaseInfo('sidx=ASSET_CD&sord=ASC')
			.success(function(data){
				// console.log(data);
				//debugger;
				$scope.AssetBaseInfo = data.rows;
				$.each($scope.AssetBaseInfo,function(idx,Item){
					$scope.BaseInfoName[''+(0) +'_'+ Item.FN_GBN] = Item.FN_GBN_NM;
					$scope.BaseInfoName[''+(1) +'_'+ Item.WORK_GBN] = Item.WORK_GBN_NM;
					$scope.BaseInfoName[''+(2) +'_'+ Item.ASSET_GBN1] = Item.ASSET_GBN1_NM;
					$scope.BaseInfoName[''+(3) +'_'+ Item.ASSET_GBN2] = Item.ASSET_GBN2_NM;
				});
				
			});

			
			$scope.getAssetBaseInfo = function(callback){
				var param='sidx=ASSET_CD&sord=ASC';
				mainDataService.getAssetBaseInfo(param)
				.success(function(data){
					//debugger;
					console.log(data);
					$scope.AssetBaseInfoList = data.rows;
					if(typeof callback == 'function') callback();
				});
			}
			/*
			$scope.getAssetLevelList = function(LevelStep){
				var list = [];
				$.each($scope.AssetBaseInfoList,function(idx,Item){
					if(LevelStep==Item.LEVEL_STEP)
					list.push();
				});
				return list;
			}*/
			$scope.lastNid = 0;
			$scope.toggleTreeNode = function(level,nId,ap_sid){
				
				var last_list = $(".last_list");
				last_list.find(".blockName").removeClass("active_color");
				$("#base_li_"+nId).find(".blockName").addClass("active_color");
				$scope.ap_sid = ap_sid;
				
				$scope.lastNid = nId;
				console.log(ap_sid);
				if(typeof ap_sid=='undefined'){
					if($("#base"+nId).find('li').length==0) 
						$("#base"+nId).append(MakeTreeHTML(level,nId));
					
					$("#base"+nId).collapse("toggle");
					if($("#path_li_"+nId).find('a').attr('aria-expanded')==='false'){
						$("#base_li_"+nId).find('a').attr('aria-expanded','true');
					}else{
						$("#base_li_"+nId).find('a').attr('aria-expanded','false');	
					}
				}else{
					var last_list = $(".last_list");
					last_list.find(".blockName").removeClass("active_color");
					$("#base_li_"+nId).find(".blockName").addClass("active_color");
					$scope.ap_sid = ap_sid;
				}
			}
			function MakeTreeHTML(LevelStep,nId){
				
				var sHTML = "";
				//if(LevelStep > 11)  return "";
				$.each($scope.treeNode,function(idx,Item){
						if(Item.pId == nId){
							var isLeaf = !(typeof Item['ap_sid'] == 'undefined');
							sHTML += "<li id='base_li_"+ Item.nId + "'>";
							sHTML += "<a data-toggle='collapse' class='"+((isLeaf)?"last_list":"last_list")+"'  " +((isLeaf)?"":"aria-expanded='false'") + " ng-click='toggleTreeNode("+(LevelStep+1)+","+Item.nId+ ((isLeaf)?(","+Item.ap_sid):"")+")' >";
							sHTML += "<span class='blockName'> &nbsp;&nbsp;&nbsp;"; 
							sHTML += $scope.BaseInfoName[LevelStep+'_'+Item.lcd]+"</span>";
							sHTML += "</a> ";
							if($scope.returnType == 'multi'){
								if(isLeaf)
									sHTML += "<input type=checkbox class='checkTreeNode' ng-model=\"treeNode[" + (Item.nId-1) + "].IsChecked\" />";
								else
									sHTML += "<input type=checkbox class='checkTreeNode1' ng-model=\"treeNode[" + (Item.nId-1) + "].IsChecked\" />";
							}
							sHTML += "</li>";
							sHTML += "<ul id='base"+Item.nId+"' class='list-group panel-collapse collapse' style='padding-left:20px !important;'>";			
							sHTML += "</ul>";					
							/*
							if(LevelStep<6){
							sHTML += "<ul id='node"+Item.nId+"' class='list-group panel-collapse collapse' style='padding-left:20px !important;'>";			
							sHTML += MakeTreeHTML(LevelStep+1,Item.nId);
							sHTML += "</ul>";
							}
							*/
						}

				});
				var template = angular.element(sHTML);
				var linkFunction = $compile(template);
				linkFunction($scope);
				return template;
			}
			
			function findPath(LevelStep,nId,type){
			if (LevelStep< 0 ) return "";
			var isFind = false;
			var pathcd = "";
			var pathname = "";
			var pId = "";
				$.each($scope.treeNode,function(idx,Item){
					if(Item.nId == nId){
						isFind = true;
						pId = Item.pId;
						pathname = $scope.BaseInfoName[LevelStep+'_'+Item.lcd];
						pathcd = Item.lcd;
						return false;
					}
				});
				if(isFind){
					if(type=='cd'){
						if(LevelStep> 0)
							return findPath(LevelStep-1,pId,'cd') +'_' + pathcd;
						else
							return pathcd;
					}
					else{
						if(LevelStep> 0)
							return findPath(LevelStep-1,pId,'') +'>'+ pathname;
						else
							return pathname;
					}
				}	
				 

			}
			
			function addPath(lpath,base_sid){
				var i=0;
				var Path = lpath.split('_');
				var Path1 = "";
				
				if($scope.namefilter!=""){
					var name_path = "";
					for(var level=0;level<4;level++){
						if(Path[level]=='') break;
						name_path +=">"+$scope.BaseInfoName[level+"_"+Path[level]];	
					}			
					if(name_path.indexOf($scope.namefilter) < 0) return;
				}
				
				for(level=0;level<4;level++){
					if(Path[level]=='') break;
					Path1 += Path[level]+"_";
					if($.inArray(Path1,$scope.lpath[level])<0){
						var pid = (level>0)?lastTreeNodeId[level-1]:0; 
						treeNodeId++;
						$scope.lpath[level].push(Path1);
						if(Path[level+1]==''){
							$scope.treeNode.push({pId:pid,nId:treeNodeId,lv:level,lcd:Path[level],ap_sid:base_sid});
						}else{
							$scope.treeNode.push({pId:pid,nId:treeNodeId,lv:level,lcd:Path[level]});	
						}
						lastTreeNodeId[level]=treeNodeId;
					}
				}
					
			}
			
			$scope.initAssetBaseInfoControl = function(){

				if(typeof $scope.returnType=='undefined') $scope.returnType='single';
				$scope.getAssetBaseInfo(function(){
					
					if($("#base0").find('li').length==0){
					$scope.lpath_init();
					//var i=0;
					$.each($scope.AssetBaseInfoList,function(idx,Item){
						//console.log(Item.LEVEL_PATH_CD);
						addPath(Item.ASSET_CD + "_",Item.BASE_SID);
					});
					}
					//console.log($scope.treeNode);
					if($("#base0").find('li').length==0){
						var sHTML = ""
						sHTML +="<div id='base_li_0' class='blockPanel-heading'> ";
						sHTML +="<a data-toggle='collapse' aria-expanded='true' onclick='toggleTreeNode(0,0)'  class='collapsed'>";
						sHTML +="<span class='blockName'>&nbsp;&nbsp;&nbsp;자산인벤토리경로</span>";
						sHTML +="</a>";
						sHTML +="</div>";
						sHTML +="<ul id='base0' class='list-group panel-collapse collapse show'>";
						sHTML +="</ul>";
						
						$("#Id_assetBaseInfo").html(sHTML);
						$("#base0").append(MakeTreeHTML(0,0));
						$(".side_nav").show();
					}
				});			
			}
			$scope.$on('loadAssetBaseInfoList', function(event, data) {
				//$scope.template_html = "./view/common/SelectAssetLevelPath.html";
				console.log(event);
				console.log(data);
				$scope.callbackId = data.callbackId;
				$scope.param = data;
				
				//if($scope.namefilter!=data.namefilter){
					$("#base0").remove();						
				//}
					/*
				if($scope.returnType = data.returnType){
					$("#path0").remove();
				}*/
					
				$scope.namefilter = data.namefilter;
				$scope.returnType = data.returnType; // single(pathSid,pathCd),multi(pathCd)		
				$timeout(function(){
					$scope.initAssetBaseInfoControl();	
					$("#dialog-assetbaseinfo").show();
				},500);
				
			});

		}

