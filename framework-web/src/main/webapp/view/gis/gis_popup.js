let WFS_URL;
let WMS_URL;
let isEditMode = false;
let wtl_pipe_lm_Style = false;

(() => {
  const hostname = location.hostname;
  //debugger;
  if (hostname === 'localhost' || hostname === '127.0.0.1') {  // 개발 PC
    WFS_URL = 'http://192.168.1.123:9080/geoserver/ams/ows';
    WMS_URL = 'http://192.168.1.123:9080/geoserver/ams/wms';
  } else if (hostname === '14.35.198.58') { // 평촌 개발 서버 (외부)
    WFS_URL = 'http://14.35.198.58:9080/geoserver/ams/ows';
    WMS_URL = 'http://14.35.198.58:9080/geoserver/ams/wms';
  } else if (hostname === '192.168.1.123') {  // 평촌 개발 PC (내부)
    WFS_URL = 'http://192.168.1.123:9080/geoserver/ams/ows';
    WMS_URL = 'http://192.168.1.123:9080/geoserver/ams/wms';
  }
})();
const DEFAULT_CENTER_POINT = [14088979.82, 4257570.74];
const DEFAULT_ZOOM = 12;
const MIN_ZOOM = 10;
const MAX_ZOOM = 22;

let map;
let view;
let editVectorLayer; // 편집용
let snapVectorLayer;
let tileLayer;       // 선택용
let searchLayer;     // 검색용

function initMap() {
  // Define VectorLayer Style
  const defaultStyle = new ol.style.Style({
    image: new ol.style.Circle({
      radius: 2,
      fill: new ol.style.Fill({ color: '#000000' }),
      stroke: new ol.style.Stroke({
        color: [255, 0, 0],
        width: 1
      })
    }),
    stroke: new ol.style.Stroke({
      color: '#eb4034',
      width: 3
    })
  });

  const defaultStyle1 = new ol.style.Style({
	    image: new ol.style.Circle({
	      radius: 2,
	      fill: new ol.style.Fill({ color: '#000000' }),
	      stroke: new ol.style.Stroke({
	        color: [255, 0, 0],
	        width: 1
	      })
	    }),
	    stroke: new ol.style.Stroke({
	      color: '#ff0000',
	      width: 3
	    })
	  });  
  

  // MAP Settings
  map = new ol.Map({
    target: 'gis_map'
  });

  // VIEW Settings
  view = new ol.View({
    projection: new ol.proj.Projection({
      code: 'EPSG:3857',
      units: 'm',
      axisOrientation: 'enu'
    }),
    center: DEFAULT_CENTER_POINT,
    minZoom: MIN_ZOOM,
    maxZoom: MAX_ZOOM,
    zoom: DEFAULT_ZOOM
  });
  map.setView(view);

  // Default Background Layer
  let baseLayer = new ol.layer.Tile({
    source: defaultBackgroundSource
  });
  map.addLayer(baseLayer);

  // 편집용 백터 레이어 정의
  editVectorLayer = new ol.layer.Vector({
    style: defaultStyle1
  });

  map.addLayer(editVectorLayer);

  snapVectorLayer = new ol.layer.Vector({
    style: defaultStyle
  });

  map.addLayer(snapVectorLayer);

  // 선택용 타일 레이어 정의
  tileLayer = new ol.layer.Tile({
    style: defaultStyle
  });

  map.addLayer(tileLayer);

  // 검색용 타일 레이어 정의
  searchLayer = new ol.layer.Tile({
    style: new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: 'rgba(255,0,0,1.0)',
        width: 3
      }),
      image: new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: 'rgba(255,0,0,1.0)'
        }),
        stroke: new ol.style.Stroke({
          color: 'rgba(255,255,255,1.0)',
          width: 1.5
        })
      })
    })
  });

  map.addLayer(searchLayer);

  const defaultBackgroundButton = document.getElementById('defaultBackgroundButton');
  defaultBackgroundButton.addEventListener('click', (e) => {
    baseLayer.setSource(defaultBackgroundSource);
  });

  const satelliteBackgroundButton = document.getElementById('satelliteBackgroundButton');
  satelliteBackgroundButton.addEventListener('click', (e) => {
    baseLayer.setSource(satelliteBackgroundSource);
  });

  const hybridBackgroundButton = document.getElementById('hybridBackgroundButton');
  hybridBackgroundButton.addEventListener('click', (e) => {
    baseLayer.setSource(hybridBackgroundSource);
  });

  const grayBackgroundButton = document.getElementById('grayBackgroundButton');
  grayBackgroundButton.addEventListener('click', (e) => {
    baseLayer.setSource(grayBackgroundSource);
  });

  const goToCurrentLocation = document.getElementById('goToCurrentLocation');
  goToCurrentLocation.addEventListener('click', (e) => {
    map.getView().setCenter(DEFAULT_CENTER_POINT);
    map.getView().setZoom(DEFAULT_ZOOM);
  });

  let fullMode = '';
  const requestFullscreenButton = document.getElementById('requestFullscreenButton');
  requestFullscreenButton.addEventListener('click', (e) => {
    if (fullMode === 'full') {
      fullMode = '';
      requestFullscreenButton.innerHTML = '전체';

      ol.control.FullScreen.exitFullScreen();
    } else {
      fullMode = 'full';
      requestFullscreenButton.innerHTML = '기본';

      try {
        ol.control.FullScreen.requestFullScreen(document.getElementById('gis'));
      } catch (e) {

      }

      try {
        ol.control.FullScreen.requestFullScreen(document.getElementById('gisPopup'));
      } catch (e) {

      }
    }
  });

  const zoomInButton = document.getElementById('zoomInButton');
  zoomInButton.addEventListener('click', (e) => {
    const currentZoomLevel = map.getView().getZoom();
    if (currentZoomLevel !== MAX_ZOOM) {
      map.getView().setZoom(currentZoomLevel + 1);
    }
  });

  const zoomOutButton = document.getElementById('zoomOutButton');
  zoomOutButton.addEventListener('click', (e) => {
    const currentZoomLevel = map.getView().getZoom();
    if (currentZoomLevel !== MIN_ZOOM) {
      map.getView().setZoom(currentZoomLevel - 1);
    }
  });

  const saveMap = document.getElementById('saveMap');
  saveMap.addEventListener('click', (e) => {
    console.log('save map');

    map.once('postcompose', function(event) {
      var canvas = event.context.canvas;
      if (navigator.msSaveBlob) {
        navigator.msSaveBlob(canvas.msToBlob(), 'map.png');
      } else {
        canvas.toBlob(function(blob) {
          saveAs(blob, 'map.png');
        });
      }
    });
    map.renderSync();
  });

  map.on('singleclick', function(event) {
    NonAssetLayerInfo.setPixel(event.pixel);
    NonAssetLayerInfo.getBlockBoundaryInformation(event);
    NonAssetLayerInfo.getOtherInformation(event);
  	
    var l_coordinate = ol.proj.transform(event.coordinate, 'EPSG:3857', 'EPSG:4326');
  	console.log(event.coordinate);
  	console.log(l_coordinate);
  	
  	$("#searchLongitude").val(l_coordinate[0]);
    $("#searchLatitude").val(l_coordinate[1]);
  });
}

// 선택용 타일 레이어 소스 가져오기
function getTileLayer(tileLayerName) {
  return new ol.source.TileWMS({
    url: WMS_URL,
    params: {
      'FORMAT': 'image/png',
      'VERSION': '1.1.1',
      'TILED': true,
      'LAYERS': tileLayerName
    },
    crossOrigin: 'Anonymous'
  });
}

// 검색용 타일 레이어 소스 가져오기
function getTileLayerCQL(tileLayerName, CQL) {
  return new ol.source.TileWMS({
    url: WMS_URL,
    crossOrigin: 'Anonymous',
    params: {
      'FORMAT': 'image/png',
      'VERSION': '1.1.1',
      'TILED': true,
      'CQL_FILTER': CQL,
      'LAYERS': tileLayerName,
      'STYLES' : (tileLayerName.indexOf('wtl_pipe_lm')>0 && wtl_pipe_lm_Style)?"red_line":"",
      'current_time' : (new Date()).getTime()
    },
    serverType: ol.source.WMSServerType.GEOSERVER
  });
}

// 편집용 백터 레이어 소스 가져오기
function getVectorLayer(vectorLayerName) {
  const formatWFS = new ol.format.WFS();

  let tempSource = new ol.source.Vector({
    loader: function(extent) {
      $.ajax(WFS_URL, {
        type: 'GET',
        data: {
          service: 'WFS',
          version: '1.1.0',
          request: 'GetFeature',
          typeName: vectorLayerName,
          srsName: 'EPSG:3857',
          bbox: extent.join(',') + ',EPSG:3857'
        }
      }).done(function(response) {
        tempSource.addFeatures(formatWFS.readFeatures(response));
      });
    },
    strategy: ol.loadingstrategy.bbox,
    projection: 'EPSG:3857',
    crossOrigin: 'Anonymous'
  });

  return tempSource;
}

function getImageLayer(tileLayerName) {
  return new ol.source.ImageWMS({
    url: WMS_URL,
    params: {
      'FORMAT': 'image/png',
      'VERSION': '1.1.1',
      'LAYERS': tileLayerName
    },
    serverType: ol.source.WMSServerType.GEOSERVER
  });
}
