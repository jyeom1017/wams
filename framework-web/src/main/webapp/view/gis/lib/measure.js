function initMeasure() {
  const MeasureClass = {
    el: {
      source: null,
      draw: null,
      mapPointermoveEvent: null
    },
    init: function() {
      this.el.source = new ol.source.Vector();
      const vector = new ol.layer.Vector({
    	name : 'Measure',
        source: this.el.source,
        style: new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
          }),
          stroke: new ol.style.Stroke({
            color: '#ffcc33',
            width: 2
          }),
          image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
              color: '#ffcc33'
            })
          })
        })
      });
      map.addLayer(vector);
    },
    addInteraction: function(paramType) {
      const continuePolygonMsg = '계속해서 다각형을 그리려면 클릭하세요';
      const continueLineMsg = '계속해서 선을 그리려면 클릭하세요';
      const continueCircleMsg = '계속해서 원을 그리려면 클릭하세요';
      let sketch;
      let helpTooltipElement;
      let helpTooltip;
      let measureTooltipElement;
      let measureTooltip;

      if (MeasureClass.el.draw != null) {
        map.removeInteraction(MeasureClass.el.draw);
        MeasureClass.el.draw = null;
      }

      if (MeasureClass.el.mapPointermoveEvent != null) {
        MeasureClass.el.mapPointermoveEvent = null;
      }

      this.removeHelpTooltip();

      const pointerMoveHandler = function(evt) {
        if (evt.dragging) {
          return;
        }

        if (MeasureClass.el.draw == null) {
          return;
        }

        let helpMsg = '클릭하여 그리기 시작';

        if (sketch) {
          const geom = (sketch.getGeometry());
          if (geom instanceof ol.geom.Polygon) {
            helpMsg = continuePolygonMsg;
          } else if (geom instanceof ol.geom.LineString) {
            helpMsg = continueLineMsg;
          } else if (geom instanceof ol.geom.Circle) {
            helpMsg = continueCircleMsg;
          }
        }

        helpTooltipElement.innerHTML = helpMsg;
        helpTooltip.setPosition(evt.coordinate);

        helpTooltipElement.classList.remove('hidden');
      };

      MeasureClass.el.mapPointermoveEvent = map.on('pointermove', pointerMoveHandler);
      map.getViewport().addEventListener('mouseout', function() {
        helpTooltipElement.classList.add('hidden');
      });

      // var typeSelect = paramType;
      // var draw; // global so we can remove it later

      const type = ((paramType === 'area') ? 'Polygon' :( (paramType === 'length') ? 'LineString' : ((paramType === 'circle') ? 'Circle' : 'Point')));
      this.el.draw = new ol.interaction.Draw({
        source: this.el.source,
        type: type,
        style: new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
          }),
          stroke: new ol.style.Stroke({
            color: 'rgba(0, 0, 0, 0.5)',
            lineDash: [10, 10],
            width: 2
          }),
          image: new ol.style.Circle({
            radius: 5,
            stroke: new ol.style.Stroke({
              color: 'rgba(0, 0, 0, 0.7)'
            }),
            fill: new ol.style.Fill({
              color: 'rgba(255, 255, 255, 0.2)'
            })
          })
        })
      });
      map.addInteraction(this.el.draw);

      createMeasureTooltip();
      createHelpTooltip();

      let listener;
      this.el.draw.on('drawstart', function(evt) {
        sketch = evt.feature;
        //debugger;
        //const feature = map.getFeaturesAtPixel(evt.target.downPx_)[0];
	  	//const coordinate = feature.getGeometry().getCoordinates();
        //debugger;
        //const feature = map.getFeaturesAtPixel(evt.target.downPx_)[0];
	  	const coordinate = sketch.getGeometry().getCoordinates();
	  	console.log(coordinate);
	  	//console.log(ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326'));
	  	if(type=='Point'){
		    map.getLayers().getArray()
		    .filter(layer => layer.get('name') === 'Marker')
		    .forEach(layer => map.removeLayer(layer));
		    var l_coordinate = ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326');
		    $("#searchLongitude").val(l_coordinate[0]);
		    $("#searchLatitude").val(l_coordinate[1]);
		    
		    $("#getLocationConfirm").show();
		    $("#display_LocationCoord").text(''+l_coordinate.toString());
		    getScope().$apply();
	  	}
	  	
	  	
        //let tooltipCoord = evt.coordinate;
        //console.log(tooltipCoord);
        
        listener = sketch.getGeometry().on('change', function(evt) {
          var geom = evt.target;
          var output;
          if (geom instanceof ol.geom.Polygon) {
            output = formatArea(geom);
            tooltipCoord = geom.getInteriorPoint().getCoordinates();
          } else if (geom instanceof ol.geom.LineString) {
            output = formatLength(geom);
            tooltipCoord = geom.getLastCoordinate();
          } else if (geom instanceof ol.geom.Circle) {
            output = formatRadius(geom);
            tooltipCoord = geom.getLastCoordinate();
          } else{
        	  //removeDraw();
        	  //tooltipCoord = geom.getLastCoordinate();
        	  
        	  /*
        	  const feature = map.getFeaturesAtPixel(event.pixel)[0];
        	  if (!feature) {
        	    return;
        	  }
        	  const coordinate = feature.getGeometry().getCoordinates();
        	  popup.setPosition([
        	    coordinate[0] + Math.round(event.coordinate[0] / 360) * 360,
        	    coordinate[1],
        	  ]);
        	  */
          }
          measureTooltipElement.innerHTML = output;
          measureTooltip.setPosition(tooltipCoord);
        });
      }, this);

      this.el.draw.on('drawend', function() {
    	  console.log('test');
        measureTooltipElement.className = 'tooltip tooltip-static';
        measureTooltip.setOffset([0, -7]);
        // unset sketch
        sketch = null;
        // unset tooltip so that a new one can be created
        measureTooltipElement = null;
        createMeasureTooltip();
        ol.Observable.unByKey(listener);
        
        if(typeof type !='undefined' && type!=null && type!='')
        setTimeout(function(){
        	MeasureClass.removeInteraction();
        },100);
        
      }, this);

      var formatLength = function(line) {
        var length = ol.Sphere.getLength(line);
        var output;
        if (length > 100) {
          output = (Math.round(length / 1000 * 100) / 100) + ' ' + 'km';
        } else {
          output = (Math.round(length * 100) / 100) + ' ' + 'm';
        }
        return output;
      };

      var formatArea = function(polygon) {
        var area = ol.Sphere.getArea(polygon);
        var output;
        if (area > 10000) {
          output = (Math.round(area / 1000000 * 100) / 100) + ' ' + 'km<sup>2</sup>';
        } else {
          output = (Math.round(area * 100) / 100) + ' ' + 'm<sup>2</sup>';
        }
        return output;
      };

      var formatRadius = function(circle) {
    	  var a = circle.getRadiusSquared_ ();
    	  var radius =Math.sqrt(a) / 1.234234;
    	  
        //var radius = circle.getRadius();
        
        console.log(radius);
        
        var output;
        if (radius > 100) {
          output = (Math.round(radius / 1000 * 100) / 100) + ' ' + 'km';
        } else {
          output = (Math.round(radius * 100) / 100) + ' ' + 'm';
        }
        return output;
      };

      function createHelpTooltip() {
        if (helpTooltipElement) {
          helpTooltipElement.parentNode.removeChild(helpTooltipElement);
        }
        helpTooltipElement = document.createElement('div');
        helpTooltipElement.className = 'tooltip hidden';
        helpTooltipElement.id = 'helpTooltip';
        helpTooltip = new ol.Overlay({
          element: helpTooltipElement,
          offset: [15, 0],
          positioning: 'center-left'
        });
        map.addOverlay(helpTooltip);
      }

      function createMeasureTooltip() {
        if (measureTooltipElement) {
          measureTooltipElement.parentNode.removeChild(measureTooltipElement);
        }
        measureTooltipElement = document.createElement('div');
        measureTooltipElement.className = 'tooltip tooltip-measure';
        measureTooltipElement.id = 'measureTooltip';
        measureTooltip = new ol.Overlay({
          element: measureTooltipElement,
          offset: [0, -15],
          positioning: 'bottom-center'
        });
        map.addOverlay(measureTooltip);
      }
    },
    removeInteraction: function() {
      map.removeInteraction(this.el.draw);
      
      if (this.el.mapPointermoveEvent != null) {
          this.el.mapPointermoveEvent = null;
        }

        map.getOverlays().getArray().slice(0).forEach(function(overlay) {
          const id = $(overlay.getElement()).attr('id');
          if (id === 'helpTooltip') {
            map.removeOverlay(overlay);
          }
        });
        
    },
    removeDraw: function() {
      if (this.el.draw != null) {
        map.removeInteraction(this.el.draw);
        this.el.draw = null;
      }

      if (this.el.mapPointermoveEvent != null) {
        this.el.mapPointermoveEvent = null;
      }

      map.getOverlays().getArray().slice(0).forEach(function(overlay) {
        const id = $(overlay.getElement()).attr('id');
        if (id === 'measureTooltip' || id === 'helpTooltip') {
          map.removeOverlay(overlay);
        }
      });

      this.el.source.clear();
    },
    removeHelpTooltip: function() {
      map.getOverlays().getArray().slice(0).forEach(function(overlay) {
        const id = $(overlay.getElement()).attr('id');
        if (id === 'helpTooltip') {
          map.removeOverlay(overlay);
        }
      });
    }
  };

  MeasureClass.init();

  var distanceButton = document.getElementById('distanceButton');
  distanceButton.addEventListener('click', function() {
	  $("#drawSelector").val("None").prop("selected",true);
    var scope = getScope();
    //scope.$emit('disableAllGISMenuFunctions',{menuid:'measure'});
    document.getElementById('start_select_mode').click();
    MeasureClass.addInteraction('length');
  });

  var areaButton = document.getElementById('areaButton');
  areaButton.addEventListener('click', function() {
	  $("#drawSelector").val("None").prop("selected",true);
    var scope = getScope();
    //scope.$emit('disableAllGISMenuFunctions',{menuid:'measure'});
    document.getElementById('start_select_mode').click();
    MeasureClass.addInteraction('area');
  });

  var circleButton = document.getElementById('circleButton');
  circleButton.addEventListener('click', function() {
	  $("#drawSelector").val("None").prop("selected",true);
    var scope = getScope();
    //scope.$emit('disableAllGISMenuFunctions',{menuid:'measure'});
    document.getElementById('start_select_mode').click();
    MeasureClass.addInteraction('circle');
  });

  var pointButton = document.getElementById('gotoLocationBtn');
  if(pointButton !=null){
	  pointButton.addEventListener('click', function() {
		    var scope = getScope();
		    //scope.$emit('disableAllGISMenuFunctions',{menuid:'measure'});
		    MeasureClass.removeInteraction();
		    MeasureClass.removeDraw();
		    $("#getLocationConfirm").hide();
		    MeasureClass.addInteraction('point');
		  });	  
  }
  
  var removeMeasure = document.getElementById('removeMeasure');
  removeMeasure.addEventListener('click', function() {
	  $("#drawSelector").val("None").prop("selected",true);
    MeasureClass.removeInteraction();
    MeasureClass.removeDraw();
    $("#getLocationConfirm").hide();
  });
  
  var cancelMeasure = document.getElementById('cancelMeasure');
  cancelMeasure.addEventListener('click', function() {
	  $("#drawSelector").val("None").prop("selected",true);
	  //alert('cancelMeasure');
	    MeasureClass.removeInteraction();
  });  
}
