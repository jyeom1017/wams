var gen;
var gis_edit_selected_ftr_idn=''; 
function initEdit() {
  function transactWFS(mode, f) {
    const formatWFS = new ol.format.WFS();
    const xs = new XMLSerializer();

    console.log(`mode: ${mode}, f: ${f}`);

    const editVectorLayerNameSpace = getEditVectorLayerNameSpace();
    const editVectorLayerOnlyName = getEditVectorLayerOnlyName();

    const insertGML = new ol.format.GML({
      featureNS: editVectorLayerNameSpace, featureType: editVectorLayerOnlyName, srsName: 'EPSG:3857'
    });
    const updateDeleteGML = new ol.format.GML({
      featureNS: WFS_URL, featureType: editVectorLayerOnlyName, srsName: 'EPSG:3857'
    });

    console.log('insertGML');
    console.log(insertGML);

    let node;
    switch (mode) {
      case 'insert':
        node = formatWFS.writeTransaction([f], null, null, insertGML);
        break;
      case 'update':
        node = formatWFS.writeTransaction(null, [f], null, updateDeleteGML);
        break;
      case 'delete':
        node = formatWFS.writeTransaction(null, null, [f], updateDeleteGML);
        console.log('--asset 삭제--');
        console.log(f);
        var layer_nm = f.id_.split(".")[0];
        var ftr_idn = f.values_.ftr_idn;
        $.ajax("/gis/getAssetInfoWithGisInfo.json", {
            type: 'GET',
            data: {
            	layer_nm : layer_nm,
            	ftr_idn : ftr_idn
            }
          }).done(function(data) {
        	  $.ajax("/gis/deleteGisAsset.json", {
                  type: 'POST',
                  data: {
                	  ASSET_SID : data.ASSET_SID
                  }
                }).done(function(data) {
                  console.log('삭제완료');
                });            
          });        
        break;
    }

    console.log(node);
    let payload = xs.serializeToString(node);

    switch (mode) {
      case 'update':
        payload = payload.replace('feature:' + editVectorLayerOnlyName, editVectorLayerNameSpace + ':' + editVectorLayerOnlyName).
            replace('geometry', 'geom');
        break;
    }

    console.log('payload');
    console.log(payload);

    $.ajax(WFS_URL, {
      type: 'POST',
      dataType: 'text',
      processData: false,
      contentType: 'text/xml',
      data: payload
    }).done(function(data, textStatus, jqXHR) {
      console.log('=============== RESULT S ===============');
      console.log(data);
      console.log(textStatus);
      console.log(jqXHR);
      console.log('=============== RESULT E ===============');

      // Uncomment it to click once.
      /*
      DrawClass.setActive(false);
      ModifyClass.setActive(false);
      DeleteClass.setActive(false);
      UseLessClass.setActive(false);
      */

      if (target === 'editTarget') {
        switch (mode) {
          case 'insert':
            // gets the JSON string
            var jsonstr = xml2json.fromStr(data, 'string');
            var parse = JSON.parse(jsonstr);
            var rootScope = angular.element($('#MainController')).scope();
            var fid = parse['wfs:TransactionResponse']['wfs:InsertResults']['wfs:Feature']['ogc:FeatureId']['@attributes'].fid.split('.');
            rootScope.$broadcast('addGisAsset', fid);
            //rootScope.$broadcast('removeGisAsset', fid);
            // GIS 팝업보다 위에 나오기 위함
            document.getElementById('dialog-registAssetItemPopup').style.zIndex = '9991';
            document.getElementById('asset-inventory-path-selection-popup').style.zIndex = '9992';
            break;
        }
      } else if (target === 'targetBlock') {
        switch (mode) {
          case 'insert':
            // gets the JSON string
            var jsonstr = xml2json.fromStr(data, 'string');
            var parse = JSON.parse(jsonstr);
            var fid = parse['wfs:TransactionResponse']['wfs:InsertResults']['wfs:Feature']['ogc:FeatureId']['@attributes'].fid;

            $.ajax(WFS_URL, {
              type: 'GET',
              data: {
                'service': 'WFS',
                'version': '1.1.0',
                'request': 'GetFeature',
                'typeName': fid.toString().split('.')[0],
                'outputFormat': 'application/json',
                'srsName': 'EPSG:4326',
                'featureid': fid
              }
            }).done(function(data) {
              var extent = f.getProperties().geom.getExtent();
              var X = extent[0] + (extent[2] - extent[0]) / 2;
              var Y = extent[1] + (extent[3] - extent[1]) / 2;
              var pixel = map.getPixelFromCoordinate([X, Y]);
              setPixel(pixel);
              treatResponse(data);
            });
        }
      }
    }).always(function() {
      if (getEditVectorLayerCQL() === '') {
        editVectorLayer.setSource(getVectorLayer(getEditVectorLayerName()));
      } else {
        editVectorLayer.setSource(getVectorLayerCQL(getEditVectorLayerName(), getEditVectorLayerCQL()));
      }
    });
    console.groupEnd();
  }

  let target = ''; // required. [editTarget, targetBlock]

  /* UseLess */
  const UseLessClass = {
    init: function() {
      this.select = new ol.interaction.Select({ hitTolerance: 3 });
      map.addInteraction(this.select);

      this.setEvents();
      this.setActive(false);
    },
    setEvents: function() {
      const selectedFeatures = this.select.getFeatures();
      selectedFeatures.on('add', function(event) {
        alertify.confirm('불용처리', '선택한시설물을 불용 하시겠습니까?', function() {
          var selectItemForDelete = event.element;
          var fid = selectItemForDelete.getId().split('.');
          var ftrIdn = selectItemForDelete.get('ftr_idn');
          var param = { 'table_nm': fid[0], 'gid': fid[1], 'layer_cd': fid[0], 'ftr_idn': ftrIdn };
          
          $.ajax('/gis/under42.json', {
            method: 'POST',
            data: JSON.stringify(param),
            // dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
          }).done(function(data) {
            if (data > 0) {
              alertify.success('저장되었습니다.');
            } else {
              console.log(data);
              alertify.success('불용처리 할 자산정보가 완전하지 않습니다.');
            }
          });
          //---자산 불용처리----
          const Scope = angular.element(document.getElementById('MainController')).scope();
          Scope.$broadcast('SetDisabledAsset', param);
          
        }, function() {

        });

      });
    },
    setActive: function(active) {
      this.select.setActive(active);
    },
    removeInteraction: function() {
      map.removeInteraction(this.select);
    }
  };

  /* Delete */
  const DeleteClass = {
    init: function() {
      this.select = new ol.interaction.Select({ hitTolerance: 3 });
      map.addInteraction(this.select);

      this.setEvents();
      this.setActive(false);
    },
    setEvents: function() {
      const selectedFeatures = this.select.getFeatures();
      selectedFeatures.on('add', function(event) {
        var selectItemForDelete = event.element;
        alertify.confirm('삭제', '선택한 시설물을 삭제 하시겠습니까?', function() {
          DrawClass.dirty.ldelete.push(selectItemForDelete);
          DrawClass.dirty.history.push({ 'action': 'remove', 'target': selectItemForDelete });
          editVectorLayer.getSource().removeFeature(selectItemForDelete);
          DeleteClass.select.getFeatures().clear();
        }, function() {

        });
      });
    },
    setActive: function(active) {
      this.select.setActive(active);
    },
    removeInteraction: function() {
      map.removeInteraction(this.select);
    }
  };
  // DeleteClass.init();

  /* Modify */
  const ModifyClass = {
    init: function() {
      this.select = new ol.interaction.Select({ hitTolerance: 3 });
      map.addInteraction(this.select);
      
      this.modify = new ol.interaction.Modify({
        features: this.select.getFeatures()
      });
      map.addInteraction(this.modify);
      

      this.snap = new ol.interaction.Snap({
        source: editVectorLayer.getSource()
      });
      map.addInteraction(this.snap);

      this.setEvents();
      this.setActive(false);
    },
    setEvents: function() {
      const selectedFeatures = this.select.getFeatures();

      // XXX: select 동일 객체 여러번 클릭 안 됨
      this.select.on('select', function(event) {
        if (target === 'targetBlock') {
          var selected = event.selected;

          if (selected.length > 0) {
            var fid = selected[0].getId();

            $.ajax(WFS_URL, {
              type: 'GET',
              data: {
                'service': 'WFS',
                'version': '1.1.0',
                'request': 'GetFeature',
                'typeName': fid.toString().split('.')[0],
                'outputFormat': 'application/json',
                'srsName': 'EPSG:4326',
                'featureid': fid
              }
            }).done(function(data) {
              setPixel(event.mapBrowserEvent.pixel);
              treatResponse(data);
            });
          }
        }
      });

      this.select.on('change:active', function() {
        selectedFeatures.forEach(function(each) {
          selectedFeatures.remove(each);
        });
      });

      this.dirty = {};
      this.headquarters = {};

      // ⚠️ModifyClass DrawClass 혼동 주의
      this.select.getFeatures().on('add', function(e) {
        var feature = e.element;
        
        if (!ModifyClass.dirty.hasOwnProperty(feature.getId())) {
          var clone = new ol.Feature(feature.getProperties());
          clone.setId(feature.getId());
          clone.setGeometryName(feature.getGeometryName());
          var geometry = feature.getGeometry();
          if (geometry) {
            clone.setGeometry(geometry.clone());
          }
          ModifyClass.headquarters = clone;
        }

        feature.on('change', function(e) {
	        if(ModifyClass.dirty[e.target.getId()] != true){
	            const feature = e.target;
	            DrawClass.dirty.update.push(feature);
	            DrawClass.dirty.history.push({ 'action': 'update', 'target': ModifyClass.headquarters });
	        }
          ModifyClass.dirty[e.target.getId()] = true;
        });
      });

      this.select.getFeatures().on('remove', function(e) {
        const feature = e.element;
        if (ModifyClass.dirty[feature.getId()]) {
          delete ModifyClass.dirty[feature.getId()];
          DrawClass.dirty.update.push(feature);
          DrawClass.dirty.history.push({ 'action': 'update', 'target': ModifyClass.headquarters });
        }
      });
    },
    setActive: function(active) {
      this.select.setActive(active);
      this.modify.setActive(active);
      this.snap.setActive(active);
    },
    removeInteraction: function() {
   	  map.removeInteraction(this.select);
      map.removeInteraction(this.modify);
      map.removeInteraction(this.snap);
    }
  };
  // ModifyClass.init();

  /* Draw */
  const DrawClass = {
    dirty: { add: [], update: [], ldelete: [], history: [] },
    init: function() {
      this.setDrawType();
      map.addInteraction(this.Point);
      map.addInteraction(this.LineString);
      map.addInteraction(this.Polygon);
      map.addInteraction(this.Circle);
      map.addInteraction(this.Snap);

      this.Point.on('drawend', function(event) {
        var feature = event.feature;
        DrawClass.dirty.add.push(feature);
        DrawClass.dirty.history.push({ 'action': 'add', 'target': feature });
      });
      this.LineString.on('drawend', function(event) {
        var feature = event.feature;
        DrawClass.dirty.add.push(feature);
        DrawClass.dirty.history.push({ 'action': 'add', 'target': feature });
      });
      this.Polygon.on('drawend', function(event) {
        var feature = event.feature;
        feature.setGeometryName('geom');
        DrawClass.dirty.add.push(feature);
        DrawClass.dirty.history.push({ 'action': 'add', 'target': feature });
      });
      this.Circle.on('drawend', function(event) {
        var feature = event.feature;
        DrawClass.dirty.add.push(feature);
        DrawClass.dirty.history.push({ 'action': 'add', 'target': feature });
      });

      this.Point.setActive(false);
      this.LineString.setActive(false);
      this.Polygon.setActive(false);
      this.Circle.setActive(false);
    },
    setDrawType: function() {
      let source = editVectorLayer.getSource();

      this.Point = new ol.interaction.Draw({
        source: source, type: 'Point', geometryName: 'geom'
      });
      this.LineString = new ol.interaction.Draw({
        source: source, type: 'MultiLineString', geometryName: 'geom'
      });
      this.Polygon = new ol.interaction.Draw({
        source: source, type: 'MultiPolygon', geometryName: 'geom'
      });
      this.Circle = new ol.interaction.Draw({
        source: source, type: 'Circle', geometryName: 'geom'
      });
      this.Snap = new ol.interaction.Snap({
        source: source
      });
    },
    setSnapSource: function(param_source) {
      map.removeInteraction(this.Snap);
      this.Snap = new ol.interaction.Snap({
        source: param_source
      });
      map.addInteraction(this.Snap);
    },
    getActive: function() {
      return this.activeType ? this[this.activeType].getActive() : false;
    },
    setActive: function(active) {
      const type = getEditVectorLayerType();

      if (active) {
        this.activeType && this[this.activeType].setActive(false);
        this[type].setActive(true);
        this.activeType = type;
      } else {
        this.activeType && this[this.activeType].setActive(false);
        this.activeType = null;
      }
    },
    removeInteraction: function() {
      map.removeInteraction(this.Point);
      map.removeInteraction(this.LineString);
      map.removeInteraction(this.Polygon);
      map.removeInteraction(this.Circle);
      map.removeInteraction(this.Snap);
    }
  };

  document.getElementById('editTarget').addEventListener('change',(e)=>{
	  //var scope = angular.element(document.querySelector('#MainController')).scope();
	  //scope.$broadcast('disableAllGISMenuFunctions');
	  //alert('te');
	  console.log(e);
	  if($("#editTarget").val()==""){
		  tileLayer.setSource(null);
		  searchLayer.setSource(null);
		  return;
	  }
	  var selectedlayerInfo = $("#editTarget").val().split(":");
	  var tileLayerName = "ams:" + selectedlayerInfo[1];
	  //if(selectedlayerInfo[1]=='wtl_pipe_lm'){
		  var CQL = selectedlayerInfo[2];
		  searchLayer.setSource(null);
		  tileLayer.setSource(getTileLayerCQL(tileLayerName,CQL));		  
	  //}else{
		//  searchLayer.setSource(null);
		//  tileLayer.setSource(getTileLayer(tileLayerName));		  
	  //}
  });
  
  // 편집 시작 버튼
  document.getElementById('start_edit_mode').addEventListener('click', (e) => {
    var scope = angular.element(document.querySelector('#MainController')).scope();
    scope.$broadcast('disableAllGISMenuFunctions');
    isEditMode = true;
    target = 'editTarget';
    	$("#measureBntGroup").hide();
	  $('#drawEditButton').attr("disabled", false);
	  $('#modifyEditButton').attr("disabled", false);
	  $('#deleteEditButton').attr("disabled", false);
	  
	  //assetEditShow(true,'gisEdit');
	  

    var editVectorLayerName = getEditVectorLayerName();
    
	  if(gis_edit_selected_ftr_idn==''){
		  tileLayer.setSource(null);
		  searchLayer.setSource(null);		  
	  }else{
		  var CQL = getEditVectorLayerCQL();
		  tileLayer.setSource(getTileLayerCQL(editVectorLayerName,CQL + " and ftr_idn not in (" + gis_edit_selected_ftr_idn + ")"));
		  searchLayer.setSource(null);		  
	  }    

    if (editVectorLayerName === '' || editVectorLayerName === undefined || editVectorLayerName === ':undefined') {
      alertify.alert('정보', '대상을 먼저 선택해주세요.');
      editVectorLayer.setSource(null);
    } else {
    	//$("#evaluationAnalysis").hide();
        //$("#topToolbar").find(".btn_wrap").hide();
        //$(".input_info_1").hide();
        //$(".input_info_2").hide();   
        
      document.getElementById('start_edit_mode').style.display = 'none';
      document.getElementById('close_edit_mode').style.display = 'block';
      document.getElementById('edit_mode').style.display = 'block';

      document.getElementById('editTarget').disabled = true;

      console.log('editVectorLayerName: ' + editVectorLayerName);
      var editVectorLayerCQL = getEditVectorLayerCQL();
      if (editVectorLayerCQL === '') {
        editVectorLayer.setSource(getVectorLayer(editVectorLayerName));
      } else {
    	if(gis_edit_selected_ftr_idn!=''){
    		editVectorLayerCQL = editVectorLayerCQL + " and ftr_idn in (" + gis_edit_selected_ftr_idn + ")";
    	}
        editVectorLayer.setSource(getVectorLayerCQL(editVectorLayerName, editVectorLayerCQL ));
      }
    }
  });

  // 편집 종료 버튼
  document.getElementById('close_edit_mode').addEventListener('click', (e) => {
	// target = '';
  	if(location.href.indexOf("main")<0){
  		//$("#evaluationAnalysis").show();
	}
  	
  	$("#measureBntGroup").show();
  	//$("#topToolbar").find(".btn_wrap").show();
    //$(".input_info_1").show();
    //$(".input_info_2").show();
    
    document.getElementById('start_edit_mode').style.display = 'block';
    document.getElementById('close_edit_mode').style.display = 'none';
    
    //assetEditShow(true,'gisEdit');
    
    
    $('[name="btn_edit_mode"]').removeClass('btn-primary active');
    
    document.getElementById('editTarget').disabled = false;
    document.getElementById('snapTarget').disabled = true;
    document.getElementById('snapTargetChange').disabled = true;
    
    removeAllInteraction();
     DrawClass.dirty.add = [];
     editVectorLayer.setSource(null); // XXX:
     snapVectorLayer.setSource(null); // XXX:
     DrawClass.dirty.update = [];
     DrawClass.dirty.ldelete = [];
     
     var editVectorLayerName = getEditVectorLayerName();
     //alert(editVectorLayerName);
     //alert(document.getElementById('close_edit_mode').style.display);
     
     if(editVectorLayerName.indexOf('wtl_pipe_lm')>0 && isEditMode){
    	 //alert('layer sync');
    	 
    	 $.ajax('/gis/updatePipeLmLayers.json', {
             method: 'POST',
             data: {MSG:'1234'},
             dataType: 'json',
             contentType: 'application/json;',
             beforeSend: function() {
               // TODO: 로딩 표시
             }
           }).done(function(data) {
             /*if (data > 0) {
               alertify.success('저장되었습니다.');
             } else {
               console.log(data);
               alertify.error('오류');
             }*/
           }).always(function(data) {
             console.log(data);
             // TODO: 로딩 숨기기
           });
     }
     document.getElementById('edit_mode').style.display = 'none'; 
     
      var selectedlayerInfo = $("#editTarget").val().split(":");
	  var tileLayerName = "ams:" + selectedlayerInfo[1];
	  //if(selectedlayerInfo[1]=='wtl_pipe_lm'){
		  var CQL = selectedlayerInfo[2];
		  searchLayer.setSource(null);
		  tileLayer.setSource(getTileLayerCQL(tileLayerName,CQL));		  
	  //}else{
		//  searchLayer.setSource(null);
		  //tileLayer.setSource(getTileLayer(tileLayerName));		  
	  //}
	  isEditMode = false;
    
  });
  
  document.getElementById('finish_edit_mode').addEventListener('click', (e) => {
	  $('#drawEditButton').attr("disabled", false);
	  $('#modifyEditButton').attr("disabled", false);
	  $('#deleteEditButton').attr("disabled", false);
    // target = '';
	  /*
    $("#evaluationAnalysis").show();
    $(".btn_wrap").show();
    $(".input_info_1").show();
    $(".input_info_2").show();
    document.getElementById('start_edit_mode').style.display = 'block';
    document.getElementById('close_edit_mode').style.display = 'none';
    document.getElementById('edit_mode').style.display = 'none';

    $('[name="btn_edit_mode"]').removeClass('btn-primary active');

    document.getElementById('editTarget').disabled = false;
    document.getElementById('snapTarget').disabled = true;
    document.getElementById('snapTargetChange').disabled = true;
	*/
    removeAllInteraction();

      for (var i = 0; i < DrawClass.dirty.update.length; i++) {
        var feature = DrawClass.dirty.update[i];
        transactWFS('update', feature);
      }
      DrawClass.dirty.update = [];

      for (var i = 0; i < DrawClass.dirty.ldelete.length; i++) {
        var feature = DrawClass.dirty.ldelete[i];

        transactWFS('delete', feature);
        //editVectorLayer.getSource().removeFeature(feature);
      }
      DrawClass.dirty.ldelete = [];

    gen = (function* () {
      var index = 0;

      while (index < DrawClass.dirty.add.length) {
        var feature = DrawClass.dirty.add[index++];
        feature.values_.use_yn=true;
        console.log(feature);
        yield transactWFS('insert', feature);
      }

      DrawClass.dirty.add = [];
      DrawClass.dirty.history = [];
      setTimeout(function(){
    	editVectorLayer.setSource(null); // XXX:
    	snapVectorLayer.setSource(null); // XXX:    	  
      	document.getElementById('start_edit_mode').click();
      },500);      
    })();
    gen.next();

  });

  // 스냅 변경
  document.getElementById('snapTargetChange').addEventListener('click', (e) => {
    const snapVectorLayerName = getSnapVectorLayerName();

    if (snapVectorLayerName === '' || snapVectorLayerName === undefined || snapVectorLayerName === ':undefined') {
      alertify.alert('정보', '대상을 먼저 선택해주세요.');
      snapVectorLayer.getSource().clear();
      snapVectorLayer.setSource(null);
    } else {
      console.log(`snapVectorLayerName: ${snapVectorLayerName}`);
      if (getSnapVectorLayerCQL() === '') {
        snapVectorLayer.setSource(getVectorLayer(getSnapVectorLayerName()));
      } else {
        snapVectorLayer.setSource(getVectorLayerCQL(getSnapVectorLayerName(), getSnapVectorLayerCQL()));
      }
      DrawClass.setSnapSource(snapVectorLayer.getSource());
    }
  });

  document.getElementById('uselessEditButton').addEventListener('click', (e) => {
    const editVectorLayerName = getEditVectorLayerName();

    console.log('useless start');
    console.log('layer: ' + editVectorLayerName);

    if (editVectorLayerName === '' || editVectorLayerName === ':undefined') {
      alertify.alert('정보', '대상을 먼저 선택해주세요.');
    } else {
      removeAllInteraction();
      initAllClasses();

      DrawClass.setActive(false);
      ModifyClass.setActive(false);
      DeleteClass.setActive(false);
      UseLessClass.setActive(true);
    }
  });

  document.getElementById('deleteEditButton').addEventListener('click', (e) => {
    const editVectorLayerName = getEditVectorLayerName();
    
	  $('#drawEditButton').attr("disabled", true);
	  $('#modifyEditButton').attr("disabled", true);
	  $('#deleteEditButton').attr("disabled", true);    

    console.log('delete start');
    console.log('layer: ' + editVectorLayerName);

    if (editVectorLayerName === '' || editVectorLayerName === ':undefined') {
      alertify.alert('정보', '대상을 먼저 선택해주세요.');
    } else {
      removeAllInteraction();
      initAllClasses();

      DrawClass.setActive(false);
      ModifyClass.setActive(false);
      DeleteClass.setActive(true);
      UseLessClass.setActive(false);
    }
  });

  document.getElementById('modifyEditButton').addEventListener('click', (e) => {
    const editVectorLayerName = getEditVectorLayerName();
    
	  $('#drawEditButton').attr("disabled", true);
	  $('#modifyEditButton').attr("disabled", true);
	  $('#deleteEditButton').attr("disabled", true);    

    console.log('modify start');
    console.log('layer: ' + editVectorLayerName);

    if (editVectorLayerName === '' || editVectorLayerName === ':undefined') {
      alertify.alert('정보', '대상을 먼저 선택해주세요.');
    } else {
      removeAllInteraction();
      initAllClasses();

      DrawClass.setActive(false);
      ModifyClass.setActive(true);
      DeleteClass.setActive(false);
      UseLessClass.setActive(false);
    }
  });

  $('[name="btn_edit_mode"]').on('click', function() {
    var disabled = this !== document.getElementById('drawEditButton');

    $(this).addClass('btn-primary active').siblings().removeClass('btn-primary active');

    document.getElementById('snapTarget').disabled = disabled;
    document.getElementById('snapTargetChange').disabled = disabled;
  });

  document.getElementById('drawEditButton').addEventListener('click', (e) => {
    var editVectorLayerName = getEditVectorLayerName();
	  $('#drawEditButton').attr("disabled", true);
	  $('#modifyEditButton').attr("disabled", true);
	  $('#deleteEditButton').attr("disabled", true);
	  
    console.log('draw start');
    console.log('layer: ' + editVectorLayerName);

    if (editVectorLayerName === '' || editVectorLayerName === ':undefined') {
      alertify.alert('정보', '대상을 먼저 선택해주세요.');
    } else {
      removeAllInteraction();
      initAllClasses();

      DrawClass.setActive(true);
      ModifyClass.setActive(false);
      DeleteClass.setActive(false);
      UseLessClass.setActive(false);
    }
  });

  if (document.getElementById('blockDivision')) {
    document.getElementById('blockDivision').addEventListener('click', function() {
      var scope = angular.element(document.querySelector('#MainController')).scope();
      scope.$broadcast('disableAllGISMenuFunctions');

      target = 'targetBlock';
      document.getElementById('block_division_mode').style.display = 'block';
      
      $('#targetBlock').attr('disabled',false);
      $('#changeBlockButton').attr('disabled', false);

      $('[name="btn_block_division_mode"]').each(function() {
        $(this).attr('disabled', true);
      });
      
      $("#evaluationAnalysis").hide();
      $("#assetEdit").hide();
      $("#topToolbar").find(".btn_wrap").hide();
      $(".input_info_1").hide();
      $(".input_info_2").hide();
      
    });
  }

  if (document.getElementById('btnCloseBlockDivision')) {
    document.getElementById('btnCloseBlockDivision').addEventListener('click', function(event) {
    	console.log(event);
    	if(event !=null && event.clientX < 1000) return;
      document.getElementById('block_division_mode').style.display = 'none';
      console.log(location.href);
      if(location.href.indexOf("main")<0){
          $("#evaluationAnalysis").show();
          $("#assetEdit").show();    	  
      }
      $("#topToolbar").find(".btn_wrap").show();
      $(".input_info_1").show();
      $(".input_info_2").show();      
      target = '';
      removeAllInteraction();
      editVectorLayer.setSource(null);
      // defaultStyle of gispopup.js
      editVectorLayer.setStyle(new ol.style.Style({
        image: new ol.style.Circle({ radius: 2,
        			fill: new ol.style.Fill({ color: '#000000' }),
        			stroke: new ol.style.Stroke({color: [0, 0, 255], width: 1})
        			}),
        stroke: new ol.style.Stroke({
          color: '#ff0000',
          width: 3
        })
      }));
    });
  }

  if (document.getElementById('changeBlockButton')) {
    document.getElementById('changeBlockButton').addEventListener('click', function() {
      var editVectorLayerName = getEditVectorLayerName();

      removeAllInteraction();

      editVectorLayer.setSource(null);

      if (editVectorLayerName === '' || editVectorLayerName === undefined || editVectorLayerName === ':undefined') {
        alertify.alert('정보', '대상을 먼저 선택해주세요.');
        return;
      }

      var editVectorLayerCQL = getEditVectorLayerCQL();
      var vectorSource = editVectorLayerCQL === '' ? getVectorLayer(editVectorLayerName) : getVectorLayerCQL(editVectorLayerName, editVectorLayerCQL);
      editVectorLayer.setSource(vectorSource);
      editVectorLayer.setStyle(style);

      function style(feature) {
        return new ol.style.Style({
          image: new ol.style.Circle({
            radius: 2, fill: new ol.style.Fill({ color: '#000000' }), stroke: new ol.style.Stroke({
              color: [255, 0, 0], width: 1
            })
          }), stroke: new ol.style.Stroke({
            color: '#eb4034', width: 1
          }), text: new ol.style.Text({
            font: '13px 맑은고딕',
            text: feature.get('블록명'),
            stroke: new ol.style.Stroke({ color: '#000000', width: 0.5 }),
            backgroundFill: new ol.style.Fill({ color: '#fafafa' })
          })
        });
      }
      $('#targetBlock').attr('disabled',true);
      $('#changeBlockButton').attr('disabled', true);

      $('[name="btn_block_division_mode"]').each(function() {
        $(this).attr('disabled', false);
      });
    });
  }

  if (document.getElementById('deleteBlockButton')) {
    document.getElementById('deleteBlockButton').addEventListener('click', (e) => {
      const editVectorLayerName = getEditVectorLayerName();

      console.log('delete start');
      console.log('layer: ' + editVectorLayerName);

      if (editVectorLayerName === '' || editVectorLayerName === ':undefined') {
        alertify.alert('정보', '대상을 먼저 선택해주세요.');
      } else {
        removeAllInteraction();
        initAllClasses();

        DrawClass.setActive(false);
        ModifyClass.setActive(false);
        DeleteClass.setActive(true);
        UseLessClass.setActive(false);
      }
    });
  }

  if (document.getElementById('editBlockButton')) {
    document.getElementById('editBlockButton').addEventListener('click', (e) => {
      const editVectorLayerName = getEditVectorLayerName();

      console.log('modify start');
      console.log('layer: ' + editVectorLayerName);

      if (editVectorLayerName === '' || editVectorLayerName === ':undefined') {
        alertify.alert('정보', '대상을 먼저 선택해주세요.');
      } else {
        removeAllInteraction();
        initAllClasses();

        DrawClass.setActive(false);
        ModifyClass.setActive(true);
        DeleteClass.setActive(false);
        UseLessClass.setActive(false);
      }
    });
  }

  if (document.getElementById('addBlockButton')) {
    document.getElementById('addBlockButton').addEventListener('click', (e) => {
      var editVectorLayerName = getEditVectorLayerName();

      console.log('draw start');
      console.log('layer: ' + editVectorLayerName);

      if (editVectorLayerName === '' || editVectorLayerName === ':undefined') {
        alertify.alert('정보', '대상을 먼저 선택해주세요.');
      } else {
        removeAllInteraction();
        initAllClasses();

        DrawClass.setActive(true);
        ModifyClass.setActive(false);
        DeleteClass.setActive(false);
        UseLessClass.setActive(false);
      }
    });
  }

  if (document.getElementById('changeBlockButton2')) {

	  document.getElementById('changeBlockButton2').addEventListener('click', function() {
	  try{
	  if(DrawClass){
		  DrawClass.setActive(false);		  
	  }		  
	  if(ModifyClass){
		  ModifyClass.setActive(false);		  
	  }
	  if(DeleteClass){
		  DeleteClass.setActive(false);		  
	  }	  
	  }catch(ex){
		  console.log(ex.message);
	  }
	  
      for (var i = 0; i < DrawClass.dirty.add.length; i++) {
        var feature = DrawClass.dirty.add[i];

        transactWFS('insert', feature);
      }
      DrawClass.dirty.add = [];

      for (var i = 0; i < DrawClass.dirty.update.length; i++) {
        var feature = DrawClass.dirty.update[i];

        transactWFS('update', feature);
      }
      DrawClass.dirty.update = [];
      for (var i = 0; i < DrawClass.dirty.ldelete.length; i++) {
        var feature = DrawClass.dirty.ldelete[i];

        transactWFS('delete', feature);
        //editVectorLayer.getSource().removeFeature(feature);
      }
      DrawClass.dirty.ldelete = [];
      
      document.getElementById('changeBlockButton').click();
      $('#targetBlock').attr('disabled',false);
      $('#changeBlockButton').attr('disabled', false);      
      $('#changeBlockButton').attr('disabled', false);
      
      $('[name="btn_block_division_mode"]').each(function() {
          $(this).attr('disabled', true);
        });
      
    });
  }

  if (document.getElementById('confirmBlockButton')) {
    document.getElementById('confirmBlockButton').addEventListener('click', function() {
      alertify.confirm('컴펌메세지', '선택한 블록영역내 자산 블록정보가 변경됩니다.\n진행하시겠습니까?', function() {
        var editVectorLayerOnlyName = getEditVectorLayerOnlyName();

        $.ajax('/gis/updateAllAssetInformationInTheBlock.json', {
          method: 'POST',
          data: JSON.stringify({ 'blockNames': [editVectorLayerOnlyName]}),
          dataType: 'json',
          contentType: 'application/json;',
          beforeSend: function() {
            // TODO: 로딩 표시
          }
        }).done(function(data) {
          if (data > 0) {
            alertify.success('저장되었습니다.');
          } else {
            console.log(data);
            alertify.error('오류');
          }
        }).always(function(data) {
          console.log(data);
          // TODO: 로딩 숨기기
        });
      }, function() {

      });
    });
  }

  function getEditVectorLayerName() {
    const editVectorLayerNameTempSplit = getEditVectorLayerNameTempSplit();

    return editVectorLayerNameTempSplit[0] + ':' + editVectorLayerNameTempSplit[1];
  }

  function getEditVectorLayerNameSpace() {
    const editVectorLayerNameTempSplit = getEditVectorLayerNameTempSplit();

    return editVectorLayerNameTempSplit[0];
  }

  function getEditVectorLayerOnlyName() {
    const editVectorLayerNameTempSplit = getEditVectorLayerNameTempSplit();

    return editVectorLayerNameTempSplit[1];
  }

  function getEditVectorLayerCQL() {
    const editVectorLayerNameTempSplit = getEditVectorLayerNameTempSplit();

    return editVectorLayerNameTempSplit[2];
  }

  function getEditVectorLayerType() {
    const editVectorLayerNameTempSplit = getEditVectorLayerNameTempSplit();

    return editVectorLayerNameTempSplit[3];
  }

  function getEditVectorLayerNameTempSplit() {
    if (!target) {
      return false;
    }

    const element = document.getElementById(target);

    //return element.options[element.selectedIndex].value.split(':');
    return element.value.split(':');
  }

  function getSnapVectorLayerName() {
    const editTarget = document.getElementById('snapTarget');
    const editVectorLayerNameTemp = editTarget.options[editTarget.selectedIndex].value;
    const editVectorLayerNameTempSplit = editVectorLayerNameTemp.split(':');

    return editVectorLayerNameTempSplit[0] + ':' + editVectorLayerNameTempSplit[1];
  }

  function getSnapVectorLayerCQL() {
    const editTarget = document.getElementById('snapTarget');
    const editVectorLayerNameTemp = editTarget.options[editTarget.selectedIndex].value;
    const editVectorLayerNameTempSplit = editVectorLayerNameTemp.split(':');

    return editVectorLayerNameTempSplit[2];
  }

  // 편집용 백터 레이어 소스 가져오기 (검색용)
  function getVectorLayerCQL(vectorLayerName, cql) {
    const vectorSource = new ol.source.Vector({
          format: new ol.format.WFS(),
          loader: function(extent) {
            $.ajax(WFS_URL, {
              type: 'GET',
              data: {
                service: 'WFS',
                version: '1.1.0',
                request: 'GetFeature',
                typeName: vectorLayerName,
                srsname: 'EPSG:3857',
                CQL_FILTER: cql
              }
            }).done(function(response) {
              var features = vectorSource.getFormat().readFeatures(response);
              vectorSource.addFeatures(features);
            });
          },
          strategy: ol.loadingstrategy.bbox,
          // strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({ maxZoom: 22 })), // 중복해서 가져오는거 같음
          projection: 'EPSG:3857',
          crossOrigin: 'Anonymous'
        }
    );

    return vectorSource;
  }

  function removeAllInteraction() {
    DrawClass.removeInteraction();
    ModifyClass.removeInteraction();
    DeleteClass.removeInteraction();
    UseLessClass.removeInteraction();
  }

  function initAllClasses() {
    DrawClass.init();
    ModifyClass.init();
    DeleteClass.init();
    UseLessClass.init();
  }

  var pixel;
  var vworldInfoGroup = [];

  function setPixel(_pixel) {
    pixel = _pixel;
  }

  function treatResponse(response) {
    var features = response.features;

    if (response.totalFeatures < 1) {
      return;
    }

    features.sort(function(a, b) {
      var aval = a.id.split('.')[0];
      var bval = b.id.split('.')[0];
      return aval < bval ? -1 : aval == bval ? 0 : 1;
    });

    vworldInfoGroup = [];

    var initGroup = false;
    var curAlias = '';
    var curFeats = [];
    var curGroup = '';
    var tabCount = 0;
    for (var i = 0; i < features.length; i++) {
      initGroup = false;
      var tmpgroup = features[i].id.split('.')[0];
      if (curGroup == '' || curGroup != tmpgroup) {
        if (curGroup != '') {
          //피쳐그룹 추가
          vworldInfoGroup.push({ 'alias': curAlias, 'layer': curGroup, 'features': curFeats });
          tabCount++;
        }
        initGroup = true;
        curFeats = [];
        curAlias = '';
        curGroup = tmpgroup;
      }
      //모든 건수를 목록으로..
      if (initGroup) {
        //그룹이 초기화된경우 한건만 등록하도록..
        // curAlias = allLayerList.getLayerKoNameFromName(curGroup);
        if (curAlias == '') {
          curAlias = curGroup;
        }
      }
      curFeats.push(features[i]);
      if (i == features.length - 1) {
        //피쳐그룹 추가
        vworldInfoGroup.push({ 'alias': curAlias, 'layer': curGroup, 'features': curFeats });
        tabCount++;
      }
    }
    _information(0, 0, response);
    features = null;
  }

  function _information(groupid, idx, response) {
    if (groupid >= vworldInfoGroup.length || groupid == null) {
      groupid = 0;
    }
    if (idx >= vworldInfoGroup[groupid].length || idx == null) {
      idx = 0;
    }

    var feature = vworldInfoGroup[groupid].features[idx];
    var canShow = true;
    var layername = feature.id.split('.')[0];

    //popup
    //1. 레이어의 첫번째 객체정보 보여주기
    var content = '';
    var subcontent = '';
    var lcnt = 0;
    // var alias = allLayerList.getLayerKoNameFromName(layername);
    //console.log("alias :" , alias);
    if (alias === '' || alias === undefined || alias === 'undefined') {
      alias = layername;
    }
    var sizeW = 140;
    var sizeH = 0;
    if (canShow || vworldInfoGroup[groupid].features.length > 1 || vworldInfoGroup.length > 1) {
      content = '<div class="olInfolayer olInfoboard" style="border:0!important;width:100%">';
      content += '  <div class="title"><h2 style="color: #ffffff;font-size: 14px;font-weight: bold;margin: 0 0 0 7px;padding:2px 0 3px 0;"><span style="vertical-align:top">' + alias + '</span></h2>';
      // content += '    <a href="#" style="position: absolute;right: 45px;top: 0;margin-top: 9px;" onclick="_zoomToFeature(\'' + groupid + '\',\'' + idx + '\');"><img style="vertical-align:middle" src="/images/v2map/map/icon/icon_viewplus.gif"></a>';
      // content += '    <a href="#" style="position: absolute;right: 30px;top: 0;margin-top: 9px;" onclick="_prevMap();"><img style="vertical-align:middle" src="/images/v2map/map/icon/icon_viewback.gif"></a>';
      // content += '    <a href="#" class="layerX btnClose" onclick="_initInfos();parent.mapController.clearAll();"><img style="vertical-align:middle" src="/images/v2map/map/btn/btn-layer-x.png"></a>';
      content += '    <a href="#" class="layerX btnClose" onclick="NonAssetLayerInfo._initInfos();"><i class="xi-close-circle"></i></a>';
      content += '  </div>';
      content += '</div>';
      sizeH += 45;
      var tmpW = (alias.length * 24) + 45;
      if (sizeW < tmpW) {
        sizeW = tmpW;
      }
    }

    var height = 0;
    if (canShow) {
      subcontent += '<form id="blockSaveForm">';
      subcontent += '<table>';
      for (var k in feature.properties) {
        if ((k.toLowerCase() === 'gid') || (k.toLowerCase() === 'geom')) {
          continue;
        }
        var title = k;
        var value = feature.properties[k];
        if (value == null || value === 'undefined' || value === '0000') {
          value = '-';
        }

        var topBorder = '';
        var tmp1 = '' + k + '';
        var tmp2 = '' + value + '';
        var tmpW = 90 + (tmp2.length * 13) + 50;
        if (sizeW < tmpW) {
          sizeW = tmpW;
        }
        if (lcnt == 0) {
          topBorder = 'style="border-top: 1px #a8a8a8 solid;"';
        }
        subcontent += '<tr>';
        subcontent += '<td class="nam" ' + topBorder + '>' + title + '</td>';
        subcontent += '<td class="val" ' + topBorder + '><input type="text" class="w-100 border-0" name="' + title + '" value="' + value + '"></td>';
        subcontent += '<tr>';
        lcnt++;
      }
      subcontent += '</table>';
      subcontent += '</form>';
      height = lcnt * 26;
      sizeH += height + 7;
    }

    if (height > 300) {
      sizeH = 350; //팝업 높이
      height = 301; //속성목록 영역 높이
    }

    var paging = '';
    if (vworldInfoGroup[groupid].features.length > 1) {
      paging = '<div class=\'no_list\' style=\'height:5px;\'>&nbsp;</div>';
      paging += '<div class=\'navi\' style=\'padding-left:calc(50% - ' + (vworldInfoGroup[groupid].features.length * 10) + 'px); margin-top:30px;\'>';
      for (var ei = 0; ei < vworldInfoGroup[groupid].features.length; ei++) {
        var ci = ei + 1;
        var classname = '';
        if (ei == idx) {
          classname = 'navi_focus';
        } else {
          classname = 'navi';
        }
        paging += '<a class=\'' + classname + '\' href=\'javascript:NonAssetLayerInfo._information(' + groupid + ',' + ei + ');\'>' + ci + '</a>';
      }
      paging += '</div>';
      var tmpW = vworldInfoGroup[groupid].features.length * 21;
      if (sizeW < tmpW) {
        sizeW = tmpW;
      }
      sizeH += 37;//수정
    }

    //20181204 - 속성창 크기
    //if (height > 0) {content += "<div class='list' style=\'margin-top:42px;height:"+ height + "px;\'>" + subcontent + "</div>";}
    if (height > 0) {
      content += '<div class=\'list\' style=\'margin-top:10px;height:auto;overflow: hidden; overflow-y: auto;\'>' + subcontent + '</div>';
      //content += '<div class=\'list\' style=\'margin-top:10px;height:' + height + 'px;overflow: hidden; overflow-y: auto;\'>' + subcontent + '</div>';
    }

    if (content.indexOf('class=\'list\'') > -1) { //팝업창 view조정
      paging = paging.replace('margin-top:30px;', '');
    }
    //페이징 추가
    content += paging;

    //3. 다른 레이어 리스트 생성
    var nidx = idx + 1;
    if (nidx >= vworldInfoGroup.length) {
      nidx = 0;
    }

    // 선택된 레이어가 두개 이상일 경우,,,
    if (vworldInfoGroup.length > 1) {
      var str = '[지도 선택]';
      if (content.indexOf('class=\'list\'') > -1) { //팝업창 view 조정
        content += '<div class=\'etc\'><div class=\'name\'> ' + str + '</div>';
      } else {
        content += '<div class=\'etc\' style=\'margin-top:30px;\'><div class=\'name\'> ' + str + '</div>';
      }
      sizeH += 95;
    }

    var tmpW = 0;
    for (var ni = 0; ni < vworldInfoGroup.length; ni++) {
      if (ni != groupid) {
        var alias = vworldInfoGroup[ni].alias;
        content += '<a class=\'etc\' href=\'javascript:NonAssetLayerInfo._information(' + ni + ',0);\'>' + alias + '</a>';
        tmpW += alias.length * 12 + 10;
        if (tmpW > 250) {
          tmpW = 250;
          sizeH += 30;
        }
        if (sizeW < tmpW) {
          sizeW = tmpW;
        }
        if (tmpW >= 250) {
          tmpW = 0;
        }
      }
    }

    if (canShow && (vworldInfoGroup[groupid].features.length >= 1)) {

      //20181204 - 속성창크기
      //content += '    <a href="#" style="position: absolute;left: 15px;bottom: 0px;margin-bottom: 10px;" onclick="_getFeatureInfos(\'' + groupid+ '\');"><img style="vertical-align:middle" src="/images/v2map/map/btn/btn-excel.gif"></a>';
      if (vworldInfoGroup.length > 1) {// 선택된 레이어가 두개 이상일 경우... 엑셀다운로드 위치 변경
        content += '    <a href="#" style="position: absolute;top: 63px;left: 15px;bottom: 0px;margin-bottom: 10px;" onclick="_getFeatureInfos(\'' + groupid + '\');"><img style="vertical-align:middle" src="/images/v2map/map/btn/btn-excel.gif"></a>';
      } else {
        content += '<div class="text-center mb-2">';
        content += '  <button type="button" class="btn btn-success" onclick="saveBlockInformation(\'' + feature.id + '\')">저장</button>';
        content += '</div>';
        sizeH += 30; //지도선택이 없는경우,, 레이어가 하나인경우,,, 페이징 밑으로 버튼이 들어가기 때문에 높이 20정도가 더 필요하다..
      }
    }
    if (vworldInfoGroup.length > 1) {
      content += '</div>';
    }

    var maxw = document.getElementById('gis_map').offsetWidth;
    var maxh = document.getElementById('gis_map').offsetHeight;

    if (content != '') {
      //_zoomToFeature(groupid,idx);
      var width = sizeW * 1.3;
      var height = sizeH;

      //20181204 - 속성창크기
      if (width > 600) {
        width = 600;
      }

      var tmpw = pixel[0] + width;
      var tmph = pixel[1] + height;

      if (tmpw > maxw) {
        pixel[0] -= (tmpw - maxw);
      }
      if (tmph > maxh) {
        pixel[1] -= (tmph - maxh);
      }

      var left = pixel[0];
      var top = pixel[1];
      var divpop = document.getElementById('mappop_html');
      if (divpop == null) {
        divpop = document.createElement('div');
      }
      divpop.style.display = 'none';
      divpop.setAttribute('id', 'mappop_html');
      divpop.frameBorder = '0';
      divpop.scrolling = 'no';
      divpop.style.position = 'absolute';
      divpop.style.overflow = 'hidden';
      divpop.style.width = width + 'px';
      //divpop.style.height = height + 'px';
      divpop.style.height = 'auto';
      divpop.style.left = left + 'px';
      divpop.style.top = top + 'px';
      divpop.style.border = '2px solid #696969';	//add
      divpop.className = 'olFramedCloudPopupContent';
      divpop.style.zIndex = 10001;

      document.getElementById('gis_map').appendChild(divpop);
      divpop.innerHTML = content;

      divpop.style.display = 'inline-block';

      var dragFunc = function() {
        return {
          move: function(divid, xpos, ypos) {
            divid.style.cursor = 'move';
            divid.style.left = xpos + 'px';
            divid.style.top = ypos + 'px';
          },
          startMoving: function(divid, container, evt) {
            var posX = evt.clientX,
                posY = evt.clientY,
                divTop = divid.style.top,
                divLeft = divid.style.left,
                eWi = parseInt(divid.style.width),
                eHe = parseInt(divid.style.height),
                cWi = parseInt(document.getElementById(container).style.width),
                cHe = parseInt(document.getElementById(container).style.height);
            divTop = divTop.replace('px', '');
            divLeft = divLeft.replace('px', '');
            var diffX = posX - divLeft,
                diffY = posY - divTop;
            document.onmousemove = function(evt) {
              var posX = evt.clientX,
                  posY = evt.clientY,
                  aX = posX - diffX,
                  aY = posY - diffY;
              if (aX < 0) {
                aX = 0;
              }
              if (aY < 0) {
                aY = 0;
              }
              if (aX + eWi > cWi) {
                aX = cWi - eWi;
              }
              if (aY + eHe > cHe) {
                aY = cHe - eHe;
              }
              dragFunc.move(divid, aX, aY);
            };
          },
          stopMoving: function(container) {
            var a = document.createElement('script');
            document.getElementById(container).style.cursor = 'default';
            document.onmousemove = function() {
            };
          }
        };
      }();

      divpop.addEventListener('mousedown', function(evt) {
        dragFunc.startMoving(divpop, 'gis_map', evt);
      }, false);
      divpop.addEventListener('mouseup', function() {
        dragFunc.stopMoving('gis_map');
      }, false);
    } else {
      if (commonUtil.isNotEmpty(checkTimeout)) {
        clearTimeout(checkTimeout);
      }
      checkTimeout = setTimeout(function() {
        clearVectorLayer();
      }, 4000);
    }
  }

  return { DrawClass: DrawClass };
}

function saveBlockInformation(featureId) {
  var form = document.getElementById('blockSaveForm');
  var formData = new FormData(form);
  var url = '/gis/updateBlockInformation.json';
  var param = { 'feature_id': featureId };

  for (var pair of formData.entries()) {
    param[pair[0]] = pair[1];
  }

  $.ajax({
    method: 'POST',
    url: url,
    data: JSON.stringify(param),
    dataType: 'json',
    contentType: 'application/json;'
  }).done(function(data) {
    if (data > 0) {
      alertify.success('저장되었습니다.');
    } else {
      console.log(data);
      alertify.error('오류');
    }
  });
}