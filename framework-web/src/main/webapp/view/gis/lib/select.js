function showSelectedInfoList() {
  const Scope = angular.element(document.getElementById('MainController')).scope();
  Scope.$broadcast('ShowSelectedInfoList', {});
}

function clearSelectedInfoList() {
  const Scope = angular.element(document.getElementById('MainController')).scope();
  Scope.$broadcast('ClearSelectedInfoList', {});
}

function showAsset(layerName, ftrIdn) {
  var tileLayerName = 'ams' + ':' + layerName.toLowerCase();
  var CQL = 'ftr_idn in (' + ftrIdn + ')';

  tileLayer.setSource(null);
  searchLayer.setSource(getTileLayerCQL(tileLayerName, CQL));
  //alert(tileLayerName);
  $.ajax(WFS_URL, {
    data: {
      service: 'WFS',
      version: '1.1.0',
      request: 'GetFeature',
      typeName: tileLayerName,
      srsname: 'EPSG:3857',
      outputFormat: 'application/json',
      cql_filter: CQL
    }
  }).done(function(data) {
    var features = angular.copy(data.features);

    if (features.length > 0) {
      var features1 = new ol.format.GeoJSON().readFeatures(features[0]);
      if(features1[0].getGeometry()!=null){
          var extent = features1[0].getGeometry().getExtent();
          var l_coordinate = ol.proj.transform(extent, 'EPSG:3857', 'EPSG:4326');
          $("#searchLongitude").val(l_coordinate[0]);
          $("#searchLatitude").val(l_coordinate[1]);
          map.getView().fit(extent, { duration: 300, maxZoom: 18 });    	  
      }
    }
  });
}

function initSelect() {
  /**
   * 단일 선택툴
   * single click = 단일선택
   * shift + click = 복수개 선택 또는 객체 선택 해제
   */
	var selected_polygon_style = new ol.style.Style({
		image: new ol.style.Circle({
		    radius: 6,
		    fill: new ol.style.Fill({color: 'rgba(255,0,0,1.0)'}),
		    stroke: new ol.style.Stroke({color: 'rgba(255,255,255,1.0)',width: 1.5})
			}),            	  
		stroke: new ol.style.Stroke({color: '#ff0000',width: 3})
	  });
    
  const SingleSelector = {
    el: {
      select: null,
      pointer: null,
      source: null,
      layer: null
    },
    enable: function() {
      SingleSelector.el.source = new ol.source.Vector({
        format: new ol.format.GeoJSON()
      });
      SingleSelector.el.layer = new ol.layer.Vector({
        source: SingleSelector.el.source,
        style: new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'rgba(255,255,255,0.01)',
            width: 10
          }),
          image: new ol.style.Circle({
            radius: 10,
            fill: new ol.style.Fill({
              color: 'rgba(255,255,255,0.01)'
            })
          })
        })
      });
      map.addLayer(SingleSelector.el.layer);

      SingleSelector.el.select = new ol.interaction.Select();
      SingleSelector.el.pointer = new ol.interaction.Pointer({
        handleDownEvent: function(event) {
          const source = tileLayer.getSource();
          const url = source.getGetFeatureInfoUrl(event.coordinate, view.getResolution(), view.getProjection(), { 'INFO_FORMAT': 'application/json' });
          SingleSelector.el.source.clear();
          $.ajax(url, {
            dataType: 'json'
          }).done(function(data) {
            if (data.features === undefined || data.features.length === 0) {
              return;
            }
            for (var i = 0; i < data.features.length; i++) {
           	  var feature = new ol.format.GeoJSON().readFeature(data.features[i]);
              feature.setStyle(selected_polygon_style);
              var validFeature = SingleSelector.el.source.getFeatureById(feature.getId());
              // 새로운 feature만 생성하여 준다.
              if (validFeature == null) {
                SingleSelector.el.source.addFeature(feature);
              } else {
                validFeature.setGeometry(feature.getGeometry());
              }
            }
            SingleSelector.el.select.dispatchEvent('select');
          }).fail(function(jqXHR, textStatus, errorThrown) {
            alert('[Error]: ' + textStatus);
          });
        }
      });
      map.addInteraction(SingleSelector.el.pointer);

      if (SingleSelector.el.select != null) {
        map.addInteraction(SingleSelector.el.select);

        SingleSelector.el.select.on('select', function(e) {
          SingleSelector.result(e);
        });
      }
    },
    //선택된 feature들을 표시한다.
    result: function(evt) {
      // console.log('result :', evt);
      // $("#"+drawResultGridId).jqGrid('clearGridData');
      const localData = [];
      evt.target.getFeatures().forEach(function(feature) {
        feature.setProperties({ 'fid': feature.getId() });//[중요] feature ID를 PK로 잡는다.
        localData.push(feature.getProperties());
      });
      //[중요] geometry 필드 속성 제거
      for (let i = 0, len = localData.length; i < len; i++) {
        for (const key in localData[i]) {
          if (key === 'geometry') {
            localData[i][key] = '';
          }
        }
      }
      if(evt.selected !=undefined && evt.selected.length > 0){
    	  parent.setSelectGisObj(localData);  
      }else{
    	  if(localData.length>0)
    		  parent.setSelectGisObj(localData);
    	  else
    		  parent.setSelectGisObj([]);
      }
      showSelectedInfoList();
      // $('#'+drawResultGridId).jqGrid('setGridParam', {data: localData}).trigger('reloadGrid');
    },
    destroy: function() {
      map.removeInteraction(SingleSelector.el.select);
      map.removeInteraction(SingleSelector.el.pointer);
      SingleSelector.el.source.clear();
      map.removeLayer(SingleSelector.el.layer);
      SingleSelector.el.select = null;
      SingleSelector.el.pointer = null;
      SingleSelector.el.source = null;
      SingleSelector.el.layer = null;
    }
  };

  /**
   * 사각형 선택툴
   * shift + mouse drag
   */
  const BoxSelector = {
    el: {
      select: null,// ref to currently selected interaction
      pointer: null,
      selectedFeatures: null,
      source: null,
      dragBox: null

    },
    enable: function() {

      // a normal select interaction to handle click
      BoxSelector.el.select = new ol.interaction.Select();
      map.addInteraction(BoxSelector.el.select);

      BoxSelector.el.selectedFeatures = BoxSelector.el.select.getFeatures();
      BoxSelector.el.source = new ol.source.Vector({
        format: new ol.format.GeoJSON({
          projection: 'EPSG:3857'
        })
      });

      // a DragBox interaction used to select features by drawing boxes
      BoxSelector.el.dragBox = new ol.interaction.DragBox({
        //        condition: ol.events.condition.platformModifierKeyOnly
        condition: ol.events.condition.shiftKeyOnly             //shiftKey 누를경우 생성
      });

      map.addInteraction(BoxSelector.el.dragBox);

      //      BoxSelector.el.dragBox.on('boxstart', function() {
      //        console.log("boxstart");
      //      });

      //single click시에 모든 선택 feature들을 초기화 시켜준다.
      BoxSelector.el.pointer = new ol.interaction.Pointer({
        handleEvent: function(evt) {
          if (evt.type === 'singleclick') {
            BoxSelector.el.selectedFeatures.clear();
            BoxSelector.result();
            return false;
          }
          return true;
        }
      });
      map.addInteraction(BoxSelector.el.pointer);

      BoxSelector.el.dragBox.on('boxend', function() {
        var selectVectorLayerName = getSelectVectorLayerName();
        var selectVectorLayerCQL = getSelectVectorLayerCQL();
        var cql = selectVectorLayerCQL === '' ? '' : 'AND ' + selectVectorLayerCQL;
        var extent = BoxSelector.el.dragBox.getGeometry().getExtent();
        var paramsObj = {
          service: 'WFS',
          version: '1.1.0',
          request: 'GetFeature',
          typeName: selectVectorLayerName,
          outputFormat: 'application/json',
          cql_filter: 'BBOX(geom,' + extent.join(',') + ',\'EPSG:3857\')' + cql
        };
        var url = WFS_URL + '?' + new URLSearchParams(paramsObj).toString();

        $.ajax(url, {
          dataType: 'json'
        }).done(function(data) {
          BoxSelector.el.source.clear();
          for (var i = 0; i < data.features.length; i++) {
        	  				//new ol.format.GeoJSON().readFeature(data.features[i]);
        	  var feature = new ol.format.GeoJSON().readFeature(data.features[i], {
                  dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'
              });
        	  feature.setStyle(selected_polygon_style);
            BoxSelector.el.source.addFeature(feature);
          }

          BoxSelector.el.source.forEachFeature(function(feature) {
            var chk = true;
            BoxSelector.el.selectedFeatures.forEach(function(currFeature) {
              if (currFeature != null && feature != null) { // 오류방지.
                if (currFeature.getId() === feature.getId()) {
                  chk = false;
                  BoxSelector.el.selectedFeatures.remove(currFeature);
                }
              }
            });
            if (chk) {
              BoxSelector.el.selectedFeatures.push(feature);
            }
          });
          //최종 선택 features result
          BoxSelector.result();
        }).fail(function(jqXHR, textStatus) {
          alertify.alert('Error','[Error] 속성조회 실패: ' + textStatus,function(){});
        });
      });
    },
    //선택된 feature들을 표시한다.
    result: function() {
      // $("#"+drawResultGridId).jqGrid('clearGridData');
      const localData = [];
      BoxSelector.el.selectedFeatures.forEach(function(feature) {
        feature.setProperties({ 'fid': feature.getId() });//[중요] feature ID를 PK로 잡는다.
        localData.push(feature.getProperties());
      });
      //[중요] geometry 필드 속성 제거
      for (let i = 0, len = localData.length; i < len; i++) {
        for (const key in localData[i]) {
          if (key === 'geometry') {
            localData[i][key] = '';
          }
        }
      }
      // $('#'+drawResultGridId).jqGrid('setGridParam', {data: localData}).trigger('reloadGrid');
      parent.setSelectGisObj(localData);
      showSelectedInfoList();
    },
    destroy: function() {
      map.removeInteraction(BoxSelector.el.select);
      map.removeInteraction(BoxSelector.el.pointer);
      map.removeInteraction(BoxSelector.el.dragBox);
      BoxSelector.el.selectedFeatures.clear();
      BoxSelector.el.source.clear();
      BoxSelector.el.select = null;
      BoxSelector.el.pointer = null;
      BoxSelector.el.dragBox = null;
      BoxSelector.el.selectedFeatures = null;
      BoxSelector.el.source = null;
    }
  };

  /**
   * 다각형 선택툴
   *
   */
  const PolygonSelector = {
    el: {
      draw: null,
      source: null,
      layer: null
    },
    enable: function() {
      //    PolygonSelector.el.source = new ol.source.Vector({wrapX: false});
      PolygonSelector.el.source = new ol.source.Vector({
        format: new ol.format.GeoJSON({
          projection: 'EPSG:3857'
        })
      });
      PolygonSelector.el.layer = new ol.layer.Vector({
        source: PolygonSelector.el.source,
        style: new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'rgba(0,153,255,1.0)',
            width: 3
          }),
          image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
              color: 'rgba(255,0,0,1.0)'
            }),
            stroke: new ol.style.Stroke({
              color: 'rgba(255,255,255,1.0)',
              width: 1.5
            })
          })
        })
      });
      map.addLayer(PolygonSelector.el.layer);

      PolygonSelector.el.draw = new ol.interaction.Draw({
        source: PolygonSelector.el.source,
        type: 'Polygon'
      });
      map.addInteraction(PolygonSelector.el.draw);

      PolygonSelector.el.draw.on('drawstart', function(evt) {
        PolygonSelector.el.source.clear();
      });

      PolygonSelector.el.draw.on('drawend', function(event) {
        var geometry = event.feature.getGeometry();
        var filter = [
          ol.format.filter.intersects('geom', geometry),
          ol.format.filter.notEqualTo('ftr_cde', '0')
        ];

        try {
          var CQLParams = getSelectVectorLayerCQL().toString().replace(/\s/g, '').split('AND');
          for (var i = 0; i < CQLParams.length; i++) {
            var param = CQLParams[i].split('=');
            filter.push(ol.format.filter.equalTo(param[0], param[1].replace(/'/g, '')));
          }
        } catch (e) {
          console.error(e);
        }

        // generate a GetFeature request
        const formatWFS = new ol.format.WFS();
        const featureRequest = formatWFS.writeGetFeature({
          featureNS: getSelectVectorLayerNameSpace(),
          featurePrefix: getSelectVectorLayerNameSpace(),
          featureTypes: [getSelectVectorLayerOnlyName()],
          srsName: 'EPSG:3857',
          outputFormat: 'application/json',
          filter: ol.format.filter.and.apply(null, filter)
        });

        // then post the request and add the received features to a layer
        fetch(WFS_URL, {
          method: 'POST',
          body: new XMLSerializer().serializeToString(featureRequest)
        }).then(function(response) {
          return response.json();
        }).then(function(json) {
          PolygonSelector.el.source.clear();
          var features = new ol.format.GeoJSON().readFeatures(json);
          features.forEach(function(feature) {
        	  feature.setStyle(selected_polygon_style);
          });          
          PolygonSelector.el.source.addFeatures(features);
          //map.getView().fit(PolygonSelector.el.source.getExtent(), /** @type {ol.Size} */ (map.getSize()));
          PolygonSelector.result();
          // loadingDiv.off();
        });

      });
    },
    result: function() {
      //    $("#status").html("자유그리기 : ");
      //    PolygonSelector.el.source.forEachFeature(function(feature) {
      //      $("#status").append(feature.getId() + ", ");
      //    });

      // $("#"+drawResultGridId).jqGrid('clearGridData');
      const localData = [];
      PolygonSelector.el.source.forEachFeature(function(feature) {
        feature.setProperties({ 'fid': feature.getId() });//[중요] feature ID를 PK로 잡는다.
        localData.push(feature.getProperties());
      });
      //[중요] geometry 필드 속성 제거
      for (let i = 0, len = localData.length; i < len; i++) {
        for (const key in localData[i]) {
          if (key === 'geometry') {
            localData[i][key] = '';
          }
        }
      }
      // $('#'+drawResultGridId).jqGrid('setGridParam', {data: localData}).trigger('reloadGrid');
      parent.setSelectGisObj(localData);
      showSelectedInfoList();
    },
    destroy: function() {
      map.removeInteraction(PolygonSelector.el.draw);
      PolygonSelector.el.source.clear();
      map.removeLayer(PolygonSelector.el.layer);
      PolygonSelector.el.draw = null;
      PolygonSelector.el.source = null;
      PolygonSelector.el.layer = null;
    }
  };

  /**
   * 그리기 선택툴
   */
  const DrawSelector = {
    // 툴활성화
    enableSelector: function(type) {
      DrawSelector.destroy();

      if (type === 'None') {
        // DrawSelector.destroy();
      } else if (type === 'Polygon') {
        PolygonSelector.enable();
        DrawSelector.hide();
        // $("#polygonDiv").show();
      } else if (type === 'Circle') {
        // DrawSelector.addInteraction(type);
      } else if (type === 'Box') {
        BoxSelector.enable();
        DrawSelector.hide();
        // $("#boxDiv").show();
      } else if (type === 'Single') {
        SingleSelector.enable();
        DrawSelector.hide();
        // $("#singleDiv").show();
      } else {
        alert('선택된 툴이 없습니다.');
      }
    },
    destroy: function() {
      if (SingleSelector.el.select != null) {
        SingleSelector.destroy();
      }
      if (BoxSelector.el.select != null) {
        BoxSelector.destroy();
      }
      if (PolygonSelector.el.draw != null) {
        PolygonSelector.destroy();
      }
      // DrawSelector.hide();
    },
    unselect: function() {
      if (SingleSelector.el.select != null) {
        SingleSelector.el.select.getFeatures().clear();
      }
      if (BoxSelector.el.select != null) {
        BoxSelector.el.select.getFeatures().clear();
      }
      if (PolygonSelector.el.draw != null) {
        PolygonSelector.el.select.getFeatures().clear();
      }
    },
    hide: function() {
      $('#singleDiv').hide();
      $('#boxDiv').hide();
      $('#polygonDiv').hide();
    }
  };

  /**
   * 검색용 선택툴
   */
  var SearchSelector = {
    el: {
      select: null,
      source: null,
      layer: null
    },
    enable: function() {
      SearchSelector.el.source = new ol.source.Vector({
        format: new ol.format.GeoJSON()
      });
      SearchSelector.el.layer = new ol.layer.Vector({
        source: SearchSelector.el.source,
        style: new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'rgba(255,255,255,0.01)',
            width: 10
          }),
          image: new ol.style.Circle({
            radius: 10,
            fill: new ol.style.Fill({
              color: 'rgba(255,255,255,0.01)'
            })
          })
        })
      });
      map.addLayer(SearchSelector.el.layer);

      SearchSelector.el.select = new ol.interaction.Select();
      map.addInteraction(SearchSelector.el.select);

      SearchSelector.el.select.on('select', function(e) {
        SingleSelector.result(e); // 단일 선택툴의 결과 함수
      });
    },
    unselect: function() {
      SearchSelector.el.select.getFeatures().clear();
    },
    destroy: function() {
      SearchSelector.unselect();
      map.removeInteraction(SearchSelector.el.select);
      SearchSelector.el.source.clear();
      map.removeLayer(SearchSelector.el.layer);
      SearchSelector.el.select = null;
      SearchSelector.el.source = null;
      SearchSelector.el.layer = null;
    }
  };

  // 선택 모드 변경
  document.getElementById('drawSelector').addEventListener('change', (e) => {
    const tileLayerName = getSelectVectorLayerName();

    if (tileLayerName === '' || tileLayerName === undefined || tileLayerName === ':undefined' || tileLayerName == null) {
      alertify.alert('Info','대상자산을 먼저 선택해주세요.',function(){});
      document.getElementById('drawSelector').selectedIndex = '0';
    } else {
      const mode = e.target.value;
      DrawSelector.enableSelector(mode);
    }
    if (document.getElementById('inputFTRIDN')) {
        document.getElementById('inputFTRIDN').value = '';
      }
    //document.getElementById('inputFTRIDN').value = '';
  });

  // 자산선택 시작 버튼
  document.getElementById('start_select_mode').addEventListener('click', (e) => {
    var scope = angular.element(document.querySelector('#MainController')).scope();
    scope.$broadcast('disableAllGISMenuFunctions1');
    //document.querySelector('#select_mode p').innerText = '자산선택 모드';
    enableAssetSelection();
    $('#drawSelector').attr('disabled',false);
    DrawSelector.destroy();
    $('#drawSelector').val('None').prop("selected", true);
    
  });

  // 자산선택 종료 버튼
  document.getElementById('finish_select_mode').addEventListener('click', (e) => {
    // document.getElementById('selectTarget').getElementsByTagName('option')[0].selected = 'selected';
    // document.getElementById('drawSelector').getElementsByTagName('option')[0].selected = true;

    document.getElementById('start_select_mode').style.display = null;
    document.getElementById('finish_select_mode').style.display = 'none';

    //document.getElementById('select_mode').style.display = 'none';

    // document.getElementById('selectTarget').selectedIndex = 0;
    // document.getElementById('drawSelector').selectedIndex = 0;

    if (document.getElementById('inputFTRIDN')) {
      document.getElementById('inputFTRIDN').value = '';
    }

    DrawSelector.destroy();
    tileLayer.setSource(null);
    searchLayer.setSource(null);
    clearSelectedInfoList(); //추가 angularjs Scope 로 gisConstroller 와 통신 선택 표시 데이터 지음
  });

  // 선택 대상 변경
  document.getElementById('selectTarget').addEventListener('change', function(e) {
	wtl_pipe_lm_Style=false;
    const tileLayerName = getSelectVectorLayerName();

    if (tileLayerName === '' || tileLayerName === undefined || tileLayerName === ':undefined' || tileLayerName == null) {
      tileLayer.setSource(null);
    } else {
      // console.log('tileLayerName: ' + tileLayerName);
      var selectVectorLayerCQL = getSelectVectorLayerCQL();
      if (selectVectorLayerCQL === '') {
        tileLayer.setSource(getTileLayer(tileLayerName));
      } else {
        tileLayer.setSource(getTileLayerCQL(tileLayerName, selectVectorLayerCQL));
      }
    }

    if (document.getElementById('inputFTRIDN')) {
      document.getElementById('inputFTRIDN').value = '';
    }
  });

  function getSelectVectorLayerNameTempSplit() {
    var element = document.getElementById('selectTarget');

    return element.options[element.selectedIndex].value.split(':');
  }

  function getSelectVectorLayerName() {
    var vectorLayerNameTempSplit = getSelectVectorLayerNameTempSplit();

    return vectorLayerNameTempSplit[0] + ':' + vectorLayerNameTempSplit[1];
  }

  function getSelectVectorLayerNameSpace() {
    var vectorLayerNameTempSplit = getSelectVectorLayerNameTempSplit();

    return vectorLayerNameTempSplit[0];
  }

  function getSelectVectorLayerOnlyName() {
    var vectorLayerNameTempSplit = getSelectVectorLayerNameTempSplit();

    return vectorLayerNameTempSplit[1];
  }

  function getSelectVectorLayerCQL() {
    var vectorLayerNameTempSplit = getSelectVectorLayerNameTempSplit();

    return vectorLayerNameTempSplit[2];
  }

  function enableAssetSelection() {
    //document.getElementById('select_mode').style.display = 'block';

    const tileLayerName = getSelectVectorLayerName();

    if (tileLayerName === '' || tileLayerName === undefined || tileLayerName === ':undefined' || tileLayerName == null) {
      tileLayer.setSource(null);
    } else {
      var selectVectorLayerCQL = getSelectVectorLayerCQL();

      if (selectVectorLayerCQL === '') {
        tileLayer.setSource(getTileLayer(tileLayerName));
      } else {
        tileLayer.setSource(getTileLayerCQL(tileLayerName, selectVectorLayerCQL));
      }
    }
  }

  if (document.getElementById('inputFTRIDN')) {
    document.getElementById('inputFTRIDN').addEventListener('keydown', (e) => {
      if (e.key === 'Enter') {
        const tileLayerName = getSelectVectorLayerName();
        if (tileLayerName === '' || tileLayerName === undefined || tileLayerName === ':undefined' || tileLayerName == null) {
          alertify.alert('info','대상을 먼저 선택해주세요.',function(){});
          if (document.getElementById('inputFTRIDN')) {
              document.getElementById('inputFTRIDN').value = '';
            }
        } else {
          if (document.getElementById('inputFTRIDN').value === '') {
            searchLayer.setSource(null);
            tileLayer.setSource(getTileLayer(tileLayerName));
          } else {
            searchLayer.setSource(null);
            const CQL = 'ftr_idn in (' + document.getElementById('inputFTRIDN').value + ')';
            tileLayer.setSource(null);
            searchLayer.setSource(getTileLayerCQL(tileLayerName, CQL));
          }
        }
      }
    });
  }

  return {
    SingleSelector: SingleSelector,
    BoxSelector: BoxSelector,
    PolygonSelector: PolygonSelector,
    DrawSelector: DrawSelector,
    SearchSelector: SearchSelector,
    enableAssetSelection: enableAssetSelection
  };
}