var NonAssetLayerInfo = (function() {
  var pixel;
  var vworldInfoGroup = [];
  var _queryFields = [
    {
      fields: [
        'jibun', 'bubun', 'bonbun', 'addr', 'ctp_nm', 'sig_nm', 'emd_nm', 'li_nm', 'gosi_year', 'gosi_month', 'jiga'
      ],
      layer: 'lp_pa_cbnd_bubun',
      titles: ['지번', '본번', '부번', '주소', '시도', '시군구', '읍면동', '리', '기준년', '기준월', '지가']
    }, // 연속지적도
    { fields: ['sig_cd', 'full_nm', 'sig_kor_nm', 'sig_eng_nm'], layer: 'lt_c_adsigg', titles: ['행정구역코드', '행정구역명', '시군구명', '시군구영문명'] }, // 시군구
    { fields: ['emd_cd', 'full_nm', 'emd_kor_nm', 'emd_eng_nm'], layer: 'lt_c_ademd', titles: ['행정구역코드', '행정구역명', '읍면동명', '읍면동영문명'] }, // 읍면동
    { fields: ['cat_nam', 'riv_nm'], layer: 'lt_c_wkmstrm', titles: ['하천분류', '하천명'] }, // 하천
    {
      fields: ['buld_nm', 'bul_eng_nm', 'gro_flo_co', 'buld_nm_dc', 'sido', 'sigungu', 'rd_nm', 'buld_no', 'gu'],
      layer: 'lt_c_spbd',
      titles: ['건물명칭', '건물영문명칭', '건물층수(지상)', '건물부명칭', '시도명', '시군구명', '도로명', '건물번호', '읍면명칭']
    }, // 도로명 주소
  ];
  _queryFields.getList = function(layername) {
    for (var i = 0; i < this.length; i++) {
      if (this[i].layer.toLowerCase() == layername.toLowerCase()) {
        this[i].getTitle = function(fieldname) {
          for (var j = 0; j < this.fields.length; j++) {
            if (this.fields[j].toLowerCase() == fieldname.toLowerCase()) {
              return this.titles[j];
            }
          }
          return null;
        };
        return this[i];
      }
    }
    return null;
  };

  function getSearchPoint(lonLatPosition, isVworld) {
    var lon = lonLatPosition[0];
    var lat = lonLatPosition[1];
    var min = [Number(lon), Number(lat)];
    var max = [Number(lon), Number(lat)];
    var MinX = Math.abs(min[0]);
    var MinY = Math.abs(min[1]);
    var MaxX = Math.abs(max[0]);
    var MaxY = Math.abs(max[1]);

    if (MinX > MaxX) {
      var maxX = MaxX;
      MaxX = MinX;
      MinX = maxX;
    }
    if (MinY > MaxY) {
      var maxY = MaxY;
      MaxY = MinY;
      MinY = maxY;
    }

    if (isVworld) {
      return MinY + ',' + MinX + ',' + MaxY + ',' + MaxX;
    }

    return MinX + ',' + MinY + ',' + MaxX + ',' + MaxY;
  }

  function getStringFromCheckedBlockLayers() {
    var scope = getScope();
    var str = '';

    $.each(scope.layers, function(idx, item) {
      if (item.PCODE === 'B' && item.isActive === true) {
        str += item.C_MEMO.toLowerCase() + ',';
      }
    });

    return str.substring(0, str.length - 1);
  }

  function getStringFromCheckedOtherLayers() {
    var scope = getScope();
    var arr = [];
    var str = '';

    $.each(scope.layers, function(idx, item) {
      if (item.PCODE === 'E' && item.isActive === true) {
        arr.push(item.C_MEMO);
      }
    });

    facilityLayerGroup.getLayers().forEach(function(element, index, array) {
      if (arr.includes(element.get('title'))) {
        str += element.get('source').params_.LAYERS + ',';
      }
    });

    return str.substring(0, str.length - 1);
  }

  function treatResponse(response) {
    var features = response.features;

    if (response.totalFeatures < 1) {
      return;
    }

    /*try {
      //검색관련 초기화
      _initInfos();
    } catch (e) {
    }
    var bounds;
    var hideinfo = true;*/

    features.sort(function(a, b) {
      var aval = a.id.split('.')[0];
      var bval = b.id.split('.')[0];
      return aval < bval ? -1 : aval == bval ? 0 : 1;
    });

    vworldInfoGroup = [];

    var initGroup = false;
    var curAlias = '';
    var curFeats = [];
    var curGroup = '';
    var tabCount = 0;
    for (var i = 0; i < features.length; i++) {
      initGroup = false;
      var tmpgroup = features[i].id.split('.')[0];
      if (curGroup == '' || curGroup != tmpgroup) {
        if (curGroup != '') {
          //피쳐그룹 추가
          vworldInfoGroup.push({ 'alias': curAlias, 'layer': curGroup, 'features': curFeats });
          tabCount++;
        }
        initGroup = true;
        curFeats = [];
        curAlias = '';
        curGroup = tmpgroup;
      }
      //모든 건수를 목록으로..
      if (initGroup) {
        //그룹이 초기화된경우 한건만 등록하도록..
        // curAlias = allLayerList.getLayerKoNameFromName(curGroup);
        if (curAlias == '') {
          curAlias = curGroup;
        }
      }
      curFeats.push(features[i]);
      if (i == features.length - 1) {
        //피쳐그룹 추가
        vworldInfoGroup.push({ 'alias': curAlias, 'layer': curGroup, 'features': curFeats });
        tabCount++;
      }
    }
    _information(0, 0, response);
    features = null;
  }

  function _information(groupid, idx, response) {
    if (groupid >= vworldInfoGroup.length || groupid == null) {
      groupid = 0;
    }
    if (idx >= vworldInfoGroup[groupid].length || idx == null) {
      idx = 0;
    }

    var feature = vworldInfoGroup[groupid].features[idx];
    // var is3D = false;
    var canShow = true;
    var layername = feature.id.split('.')[0];
    var callbackurl = null;
    if ((typeof _queryCallBacks == 'object') && _queryCallBacks.length > 0) {
      callbackurl = _queryCallBacks.getList(layername);
    }
    if (callbackurl != null) {
      var lkey = callbackurl.key.toUpperCase();
      var lurls = callbackurl.urls;
      canShow = false;
      //var layername = feature.fid.split('.')[0];
      var key = feature.get([lkey.toLowerCase()]) || feature.get([lkey.toUpperCase()]);
      for (var i = 0; i < lurls.length; i++) {
        var url = lurls[i] + key;
        /*건축물 20181018*/
        if (i > 0) {
          _popupView(url, 'layerView' + i, 500, 500, 'hidden', true, '');
        } else {
          _popupView(url, 'layerView' + i, 500, 500, 'hidden', false, '');
        }
      }
    }

    //popup
    //1. 레이어의 첫번째 객체정보 보여주기
    var content = '',
        subcontent = '';
    var Titles = null;
    var lcnt = 0;
    // var alias = allLayerList.getLayerKoNameFromName(layername);
    //console.log("alias :" , alias);
    if (alias == '' || alias == 'undefined' || alias == undefined) {
      alias = layername;
    }
    var sizeW = 140;
    var sizeH = 0;
    if (canShow || vworldInfoGroup[groupid].features.length > 1 || vworldInfoGroup.length > 1) {
      content = '<div class="olInfolayer olInfoboard" style="border:0!important;width:100%">';
      content += '  <div class="title"><h2 style="color: #ffffff;font-size: 14px;font-weight: bold;margin: 0 0 0 7px;padding:2px 0 3px 0;"><span style="vertical-align:top">' + alias + '</span></h2>';
      // content += '    <a href="#" style="position: absolute;right: 45px;top: 0;margin-top: 9px;" onclick="_zoomToFeature(\'' + groupid + '\',\'' + idx + '\');"><img style="vertical-align:middle" src="/images/v2map/map/icon/icon_viewplus.gif"></a>';
      // content += '    <a href="#" style="position: absolute;right: 30px;top: 0;margin-top: 9px;" onclick="_prevMap();"><img style="vertical-align:middle" src="/images/v2map/map/icon/icon_viewback.gif"></a>';
      // content += '    <a href="#" class="layerX btnClose" onclick="_initInfos();parent.mapController.clearAll();"><img style="vertical-align:middle" src="/images/v2map/map/btn/btn-layer-x.png"></a>';
      content += '    <a href="#" class="layerX btnClose" onclick="NonAssetLayerInfo._initInfos();"><i class="xi-close-circle"></i></a>';
      content += '  </div>';
      content += '</div>';
      sizeH += 45;
      var tmpW = (alias.length * 24) + 45;
      if (sizeW < tmpW) {
        sizeW = tmpW;
      }
    }

    var height = 0;
    if (canShow) {
      if ((typeof _queryFields == 'object') && _queryFields.length > 0) {
        Titles = _queryFields.getList(layername);
      }

      if (Titles != null) {
        for (var k = 0; k < Titles.fields.length; k++) {
          var fieldname = Titles.fields[k].toUpperCase();
          var title = Titles.titles[k];
          var properties = feature.properties;
          var value = properties[fieldname.toLowerCase()] || properties[fieldname.toUpperCase()];
          if (value === null || value === undefined || value === 'undefined' || value == '0000') {
            value = '-';
          }

          var topBorder = '';
          var tmp1 = '' + k + '';
          var tmp2 = '' + value + '';
          var tmpW = 90 + (tmp2.length * 13) + 50;
          if (sizeW < tmpW) {
            sizeW = tmpW;
          }
          if (lcnt == 0) {
            topBorder = 'style="border-top: 1px #a8a8a8 solid;"';
          }
          // 2020-12-17. 조현석 부장. 글자 길이가 길어짐으로 UI가 깨지는 현상. 테이블이 아니라 아쉬움.
          //console.log("value length :" , value, value.length);
          var dataValue = value;
          if (dataValue != null && dataValue.length > 36) {
            dataValue = dataValue.substring(0, 34) + '...';
          }
          subcontent += '<div class=\'nam\' ' + topBorder + '>' + title + '</div><div class=\'val\' ' + topBorder + ' title=\'' + value + '\'>' + dataValue + '</div>';
          lcnt++;
        }
      } else {
        for (var k in feature.properties) {
          // k = feature.getKeys()[k];
          if ((k.toUpperCase() == 'BOUNDEDBY') || (k.toUpperCase() == 'OBJECTID')
              || (k.toUpperCase() == 'GID') || (k.toUpperCase() == 'SHAPE_AREA')
              || (k.toUpperCase() == 'SHAPE_LEN') || (k.toUpperCase() == 'AG_GEOM')) {
            continue;
          }
          var title = k;
          var value = feature.properties[k];
          if (value == null || value == 'undefined' || value == '0000') {
            value = '-';
          }

          var topBorder = '';
          var tmp1 = '' + k + '';
          var tmp2 = '' + value + '';
          var tmpW = 90 + (tmp2.length * 13) + 50;
          if (sizeW < tmpW) {
            sizeW = tmpW;
          }
          if (lcnt == 0) {
            topBorder = 'style="border-top: 1px #a8a8a8 solid;"';
          }
          subcontent += '<div class=\'nam\' ' + topBorder + '>' + title + '</div><div class=\'val\' ' + topBorder + ' title=\'' + value + '\'>' + value + '</div>';
          lcnt++;
        }
      }
      height = lcnt * 26;
      sizeH += height + 7;
    }

    if (height > 300) {
      sizeH = 350; //팝업 높이
      height = 301; //속성목록 영역 높이
    }

    var paging = '';
    if (vworldInfoGroup[groupid].features.length > 1) {
      paging = '<div class=\'no_list\' style=\'height:5px;\'>&nbsp;</div>';
      paging += '<div class=\'navi\' style=\'padding-left:calc(50% - ' + (vworldInfoGroup[groupid].features.length * 10) + 'px); margin-top:30px;\'>';
      for (var ei = 0; ei < vworldInfoGroup[groupid].features.length; ei++) {
        var ci = ei + 1;
        var classname = '';
        if (ei == idx) {
          classname = 'navi_focus';
        } else {
          classname = 'navi';
        }
        paging += '<a class=\'' + classname + '\' href=\'javascript:NonAssetLayerInfo._information(' + groupid + ',' + ei + ');\'>' + ci + '</a>';
      }
      paging += '</div>';
      var tmpW = vworldInfoGroup[groupid].features.length * 21;
      if (sizeW < tmpW) {
        sizeW = tmpW;
      }
      sizeH += 37;//수정
    }

    //20181204 - 속성창 크기
    //if (height > 0) {content += "<div class='list' style=\'margin-top:42px;height:"+ height + "px;\'>" + subcontent + "</div>";}
    if (height > 0) {
    	height = height + 10;
      content += '<div class=\'list\' style=\'margin-top:10px;height:' + height + 'px;overflow: hidden; overflow-y: auto;\'>' + subcontent + '</div>';
    }

    if (content.indexOf('class=\'list\'') > -1) { //팝업창 view조정
      paging = paging.replace('margin-top:30px;', '');
    }
    //페이징 추가
    content += paging;

    //3. 다른 레이어 리스트 생성
    var nidx = idx + 1;
    if (nidx >= vworldInfoGroup.length) {
      nidx = 0;
    }

    // 선택된 레이어가 두개 이상일 경우,,,
    if (vworldInfoGroup.length > 1) {
      var str = '[지도 선택]';
      if (content.indexOf('class=\'list\'') > -1) { //팝업창 view 조정
        content += '<div class=\'etc\'><div class=\'name\'> ' + str + '</div>';
      } else {
        content += '<div class=\'etc\' style=\'margin-top:30px;\'><div class=\'name\'> ' + str + '</div>';
      }
      sizeH += 95;
    }

    var tmpW = 0;
    for (var ni = 0; ni < vworldInfoGroup.length; ni++) {
      if (ni != groupid) {
        var alias = vworldInfoGroup[ni].alias;
        content += '<a class=\'etc\' href=\'javascript:NonAssetLayerInfo._information(' + ni + ',0);\'>' + alias + '</a>';
        tmpW += alias.length * 12 + 10;
        if (tmpW > 250) {
          tmpW = 250;
          sizeH += 30;
        }
        if (sizeW < tmpW) {
          sizeW = tmpW;
        }
        if (tmpW >= 250) {
          tmpW = 0;
        }
      }
    }

    // if (canShow && (vworldInfoGroup[groupid].features.length >= 1)) {
    //
    //   //20181204 - 속성창크기
    //   //content += '    <a href="#" style="position: absolute;left: 15px;bottom: 0px;margin-bottom: 10px;" onclick="_getFeatureInfos(\'' + groupid+ '\');"><img style="vertical-align:middle" src="/images/v2map/map/btn/btn-excel.gif"></a>';
    //   if (vworldInfoGroup.length > 1) {// 선택된 레이어가 두개 이상일 경우... 엑셀다운로드 위치 변경
    //     content += '    <a href="#" style="position: absolute;top: 63px;left: 15px;bottom: 0px;margin-bottom: 10px;" onclick="_getFeatureInfos(\'' + groupid + '\');"><img style="vertical-align:middle" src="/images/v2map/map/btn/btn-excel.gif"></a>';
    //   } else {
    //     content += '    <div><a href="#" style="padding-left: 15px;" onclick="_getFeatureInfos(\'' + groupid + '\');"><img style="vertical-align:middle" src="/images/v2map/map/btn/btn-excel.gif"></a></div>';
    //     sizeH += 30; //지도선택이 없는경우,, 레이어가 하나인경우,,, 페이징 밑으로 버튼이 들어가기 때문에 높이 20정도가 더 필요하다..
    //   }
    // }
    if (vworldInfoGroup.length > 1) {
      content += '</div>';
    }

    var finalX = finalY = 0;
    var curCenter = view.getCenter();
    var maxw = document.getElementById('gis_map').offsetWidth;
    var maxh = document.getElementById('gis_map').offsetHeight;

    //위치계산해서 지적도나 건축물인경우 600px 밖으로 지도 이동
    if (!canShow && (content != '')) {
      var top = document.getElementById('gis_map').offsetTop;
      if (maxw >= 600 && maxh >= 600) {
        if (jsGetMapsMode() != 2) {
          pixel = map.getPixelFromCoordinate(curCenter);
        }
        finalX = 630 - pixel[0];
        if (finalX > 0) {
          pixel[0] = 630;
          for (var i in map.getControls().getArray()) {
            if (map.getControls().getArray()[i].pan != null) {
              map.getControls().getArray()[i].pan.panTo('etc', -finalX, 0);
              break;
            }
          }
        }
      }
    }

    if (content != '') {
      //_zoomToFeature(groupid,idx);
      var width = sizeW * 1.3;
      var height = sizeH + 15;

      //20181204 - 속성창크기
      if (width > 600) {
        width = 600;
      }

      var tmpw = pixel[0] + width;
      var tmph = pixel[1] + height;

      if (tmpw > maxw) {
        pixel[0] -= (tmpw - maxw);
      }
      if (tmph > maxh) {
        pixel[1] -= (tmph - maxh);
      }

      var left = pixel[0];
      var top = pixel[1];
      var divpop = document.getElementById('mappop_html');
      if (divpop == null) {
        divpop = document.createElement('div');
      }
      divpop.style.display = 'none';
      divpop.setAttribute('id', 'mappop_html');
      divpop.frameBorder = '0';
      divpop.scrolling = 'no';
      divpop.style.position = 'absolute';
      divpop.style.overflow = 'hidden';
      divpop.style.width = width + 'px';
      divpop.style.height = height + 'px';
      divpop.style.left = left + 'px';
      divpop.style.top = top + 'px';
      divpop.style.border = '2px solid #696969';	//add
      divpop.className = 'olFramedCloudPopupContent';
      divpop.style.zIndex = 10001;

      var infopop = document.getElementById('mappop_info');
      if (infopop == null) {
        infopop = document.createElement('iframe');
      }
      infopop.style.display = 'none';
      infopop.setAttribute('id', 'mappop_info');
      infopop.frameBorder = '0';
      infopop.scrolling = 'no';
      infopop.style.position = 'absolute';
      infopop.style.overflow = 'hidden';
      infopop.style.width = width + 'px';
      infopop.style.height = height + 'px';
      infopop.style.left = left + 'px';
      infopop.style.top = top + 'px';
      infopop.style.zIndex = 10000;

      document.getElementById('gis_map').appendChild(infopop);
      document.getElementById('gis_map').appendChild(divpop);
      divpop.innerHTML = content;

      divpop.style.display = 'inline-block';
      infopop.style.display = 'inline-block';

      var ifdocDrag = false;
      var originXtitle = 0;
      var originYtitle = 0;
      var tempX = 0;
      var tempY = 0;

      var dragFunc = function() {
        return {
          move: function(divid, xpos, ypos) {
            divid.style.cursor = 'move';
            divid.style.left = xpos + 'px';
            divid.style.top = ypos + 'px';
            infopop.style.left = xpos + 'px';
            infopop.style.top = ypos + 'px';
          },
          startMoving: function(divid, container, evt) {
            evt = evt || window.event;
            var posX = evt.clientX,
                posY = evt.clientY,
                divTop = divid.style.top,
                divLeft = divid.style.left,
                eWi = parseInt(divid.style.width),
                eHe = parseInt(divid.style.height),
                cWi = parseInt(document.getElementById(container).style.width),
                cHe = parseInt(document.getElementById(container).style.height);
            divTop = divTop.replace('px', '');
            divLeft = divLeft.replace('px', '');
            var diffX = posX - divLeft,
                diffY = posY - divTop;
            document.onmousemove = function(evt) {
              evt = evt || window.event;
              var posX = evt.clientX,
                  posY = evt.clientY,
                  aX = posX - diffX,
                  aY = posY - diffY;
              if (aX < 0) {
                aX = 0;
              }
              if (aY < 0) {
                aY = 0;
              }
              if (aX + eWi > cWi) {
                aX = cWi - eWi;
              }
              if (aY + eHe > cHe) {
                aY = cHe - eHe;
              }
              dragFunc.move(divid, aX, aY);
            };
          },
          stopMoving: function(container) {
            var a = document.createElement('script');
            document.getElementById(container).style.cursor = 'default';
            document.onmousemove = function() {
            };
          }
        };
      }();

      divpop.addEventListener('mousedown', function(evt) {
        dragFunc.startMoving(divpop, 'gis_map', evt);
      }, false);
      divpop.addEventListener('mouseup', function() {
        dragFunc.stopMoving('gis_map');
      }, false);
      // if (commonUtil.isNotEmpty(checkTimeout)) {
      //   clearTimeout(checkTimeout);
      // }
      // clearVectorLayer();
    } else {
      if (commonUtil.isNotEmpty(checkTimeout)) {
        clearTimeout(checkTimeout);
      }
      checkTimeout = setTimeout(function() {
        clearVectorLayer();
      }, 4000);
    }

    // parent.mapController.clearAll();

    // var xml;
    // if (response != null) {
    //   extentCollection = new ol.Collection();
    //   pointCollection = new ol.Collection();
    //   xml = response.replace(/\gml:/g, '');
    //   //console.log("xml : " , xml);
    //   var count = 0;
    //   $(xml).find('featureMember').each(function() {
    //     var extent = new ol.Collection();
    //     $($(this).find('outerBoundaryIs')).each(function() {
    //       $($(this).find('LinearRing')).each(function() {
    //         var text = $(this).find('coordinates').text();
    //         //console.log("outer :" , count , text);
    //         extent.add(text);
    //       });
    //     });
    //     $($(this).find('innerBoundaryIs')).each(function() {
    //       $($(this).find('LinearRing')).each(function() {
    //         var text = $(this).find('coordinates').text();
    //         //console.log("inner :" , count, text);
    //         extent.add(text);
    //       });
    //     });
    //     $($(this).find('MultiLineString')).each(function() {
    //       $($(this).find('LineString')).each(function() {
    //         var text = $(this).find('coordinates').text();
    //         //console.log("LineString :" , count, text);
    //         extent.add(text);
    //       });
    //     });
    //     $($(this).find('Point')).each(function() {
    //       var _xy = $(this).find('coordinates').text();
    //       //console.log("point :" , count, _xy);
    //       pointCollection.add(_xy);
    //     });
    //     extentCollection.add(extent);
    //     count++;
    //   });
    //   //console.log("count :" , count , extentCollection);
    // }
    // _addFeature(feature, idx);
  }

  return {
    setPixel: function(_pixel) {
      pixel = _pixel;
    },

    /**
     * @param {ol.interaction.Select.Event} event
     * @param {ol.Collection.<ol.Feature>} features
     */
    getFeaturePropertiesFromGeoServer: function(event, features) {
      var lonLatPosition = ol.proj.transform(event.mapBrowserEvent.coordinate, 'EPSG:3857', 'EPSG:4326');
      var SearchPoint = getSearchPoint(lonLatPosition);
      var feature = features[0];
      var fid = feature.getId();
      var typeName = fid.substr(0, fid.indexOf('.'));

      if (commonUtil.isNotEmpty(typeName)) {
        $.ajax(WFS_URL, {
          type: 'GET',
          data: {
            service: 'WFS',
            version: '1.1.0',
            request: 'GetFeature',
            typeName: typeName,
            // outputFormat: 'text/plain',
            outputFormat: 'application/json',
            srsName: 'EPSG:4326',
            bbox: SearchPoint + ',EPSG:4326'
          }
        }).done(function(data) {
          treatResponse(data);
        });
      }
    },

    getBlockBoundaryInformation: function(event) {
      var typeName = getStringFromCheckedBlockLayers();

      if (commonUtil.isNotEmpty(typeName)) {
        $.ajax(WFS_URL, {
          type: 'GET',
          data: {
            service: 'WFS',
            version: '1.1.0',
            request: 'GetFeature',
            typeName: typeName,
            outputFormat: 'application/json',
            cql_filter: 'INTERSECTS(geom, SRID=3857;POINT(' + event.coordinate.join(' ') + '))'
          }
        }).done(function(data) {
          treatResponse(data);
        });
      }
    },

    getOtherInformation: function(event) {
      var lonLatPosition = ol.proj.transform(event.coordinate, 'EPSG:3857', 'EPSG:4326');
      var SearchPoint = getSearchPoint(lonLatPosition, true);
      var typeName = getStringFromCheckedOtherLayers();
      //var url = 'http://api.vworld.kr/req/wfs';
      var url = '/gis/getVWorldApi';

      if (commonUtil.isNotEmpty(typeName)) {
        $.ajax(url, {
          type: 'GET',
          data: {
            service: 'WFS',
            version: '1.1.0',
            request: 'GetFeature',
            output: 'application/json',
            typeName: typeName,
            bbox: SearchPoint,
            srsName: 'EPSG:4326',
            key: vworld_apikey,
            domain: location.hostname
          },
          dataType: 'json'
        }).done(function(data) {
          treatResponse(data);
        });
      }
    },

    _initInfos: function() {
      vworldInfoGroup = null;
      // for (var i = 0; i < vw.ol3.pops.length; i++) {
      var pop = document.getElementById('mappop_html');
      if (pop != null) {
        pop.style.display = 'none';
        $("#mappop_html").remove();
        $("#mappop_info").remove();
      }
      // }
      //map_addressInfo
      // var _pop = document.getElementById('map_addressInfo');
      // if (_pop != null) {
      //   _pop.style.display = 'none';
      //   document.getElementById(vw.vworldIDs.idpanel).removeChild(_pop);
      // }

      // var is3D = false;
      // var objects = [];
      // objects = vw.vworldInfo.objects2d.slice();
      // vw.vworldInfo.objects2d = [];
      // clearVectorLayer();
      // objects = [];
    },

    _information: _information
  };
})();
