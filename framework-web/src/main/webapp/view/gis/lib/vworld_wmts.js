// VWorld API KEY
let vworld_apikey;

(() => {
  const hostname = location.hostname;

  if (hostname === 'localhost' || hostname === '127.0.0.1') {	// 개발 PC
    vworld_apikey = '8916C8CC-BDD9-3D6D-AB02-20126ED75011';
  } else if (hostname === '14.35.198.58') {						// 평촌 개발 서버 (외부)
    vworld_apikey = '678651E1-A5EB-301C-B3CD-39E4142DF073';
  } else if (hostname === '192.168.1.123') {						// 평촌 개발 PC (내부)
    vworld_apikey = '0A1E79AF-A9CA-38E8-8B68-928AC7566F99';
  } else if (hostname === '109.14.96.230') {						// 부안군 행망, 망연계
    vworld_apikey = 'CABC5348-BE3B-3442-B736-B2DAC092B28F';
  } else {  // 부안군 폐쇄망 운영서버
    vworld_apikey = 'B11D097B-16B6-3A2B-AFDE-69C751C0320E';
  }
})();

// VWorld WMTS Sources s
// 기본 배경 지도
const defaultBackgroundSource = new ol.source.XYZ({
  url: 'http://api.vworld.kr/req/wmts/1.0.0/' + vworld_apikey + '/Base/{z}/{y}/{x}.png',
  crossOrigin: 'Anonymous'
});

// 위성 배경 지도
const satelliteBackgroundSource = new ol.source.XYZ({
  url: 'http://api.vworld.kr/req/wmts/1.0.0/' + vworld_apikey + '/Satellite/{z}/{y}/{x}.jpeg',
  crossOrigin: 'Anonymous'
});

// 하이브리드 배경 지도
const hybridBackgroundSource = new ol.source.XYZ({
  url: 'http://api.vworld.kr/req/wmts/1.0.0/' + vworld_apikey + '/Hybrid/{z}/{y}/{x}.png',
  crossOrigin: 'Anonymous'
});

// 하이브리드 배경 지도
const grayBackgroundSource = new ol.source.XYZ({
  url: 'http://api.vworld.kr/req/wmts/1.0.0/' + vworld_apikey + '/gray/{z}/{y}/{x}.png',
  crossOrigin: 'Anonymous'
});
// VWorld WMTS Sources e
