var facilityLayerGroup;
var gisWindowId = "";

function removeLayers(){
	  
    var layer = map.getLayers().getArray();
    $.each(layer,function(idx,Item){
    	//debugger;
    	console.log(Item);
    	if(typeof Item.type=='undefined'){
    		//map.removeLayer(Item);
    		
    		$.each(Item.values_.layers.array_,function(idx2,Item2){
    			console.log(Item2);
    			if(Item2.values_.name=='BASE_LAYER'){
    				//map.removeLayer(Item2);
        			var params = Item2.getSource().getParams();
        			//params.CQL_FILTER = params.CQL_FILTER + " and use_yn='Y' ";
        			params.current_time = (new Date()).getTime();
        			console.log(params);
        			Item2.getSource().updateParams(params);    				
    			}	

    		});
    		
        	return false;
    	}
    });
    var layer = map.getLayers().getArray();
    console.log(layer);
}

function initLayers() {
  // 대블록
  var WTL_BLK1_AS = new ol.layer.Image({
    source: getImageLayer('wtl_blk1_as'),
    visible: false,
    title: 'WTL_BLK1_AS'
    ,name : 'BASE_LAYER'
  });

  // 중블록
  var WTL_BLK2_AS = new ol.layer.Image({
    source: getImageLayer('wtl_blk2_as'),
    visible: false,
    title: 'WTL_BLK2_AS'
   	,name : 'BASE_LAYER'
  });

  // 소블록
  var WTL_BLK3_AS = new ol.layer.Image({
    source: getImageLayer('wtl_blk3_as'),
    visible: false,
    title: 'WTL_BLK3_AS'
   	,name : 'BASE_LAYER'
  });

  // 관리블록
  var WTL_BLK3_SUB_AS = new ol.layer.Image({
    source: getImageLayer('wtl_blk3_sub_as'),
    visible: false,
    title: 'WTL_BLK3_SUB_AS'
    	,name : 'BASE_LAYER'    	
  });

  // 취수장
  var WTL_GAIN_PS_SA112 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_gain_ps', 'ftr_cde=\'SA112\''),
    visible: false,
    title: 'WTL_GAIN_PS_SA112'
    	,name : 'BASE_LAYER'
  });

  // 정수장
  var WTL_PURI_AS_SA113 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_puri_as', 'ftr_cde=\'SA113\''),
    visible: false,
    title: 'WTL_PURI_AS_SA113'
    	,name : 'BASE_LAYER'
  });

  // 배수지
  var WTL_SERV_PS_SA114 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_serv_ps', 'ftr_cde=\'SA114\''),
    visible: false,
    title: 'WTL_SERV_PS_SA114'
    	,name : 'BASE_LAYER'
  });

  // 가압장
  var WTL_PRES_PS_SA206 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_pres_ps', 'use_yn=true'),
    visible: false,
    title: 'WTL_PRES_PS_SA206'
    	,name : 'BASE_LAYER'
  });

  // 저수조
  var WTL_RSRV_PS_SA120 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_rsrv_ps', 'use_yn=true'),
    visible: false,
    title: 'WTL_RSRV_PS_SA120'
    	,name : 'BASE_LAYER'
  });

  // 유량계
  var WTL_FLOW_PS_SA117 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_flow_ps', 'use_yn=true'),
    visible: false,
    title: 'WTL_FLOW_PS_SA117'
    	,name : 'BASE_LAYER'
  });

  // 수압계
  var WTL_PRGA_PS_SA121 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_prga_ps', 'ftr_cde=\'SA121\''),
    visible: false,
    title: 'WTL_PRGA_PS_SA121'
    	,name : 'BASE_LAYER'
  });

  // 상수맨홀
  var WTL_MANH_PS_SA100 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_manh_ps', 'use_yn=true'),
    visible: false,
    title: 'WTL_MANH_PS_SA100'
    	,name : 'BASE_LAYER'
  });

  // 변류시설
  var WTL_VALV_PS_SA200 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_valv_ps', 'use_yn=true'),
    visible: false,
    title: 'WTL_VALV_PS_SA200'
    	,name : 'BASE_LAYER'
  });

  // 소방시설
  var WTL_FIRE_PS_SA119 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_fire_ps', 'use_yn=true'),
    visible: false,
    title: 'WTL_FIRE_PS_SA119'
    	,name : 'BASE_LAYER'
  });

  // 급수전계량기
  var WTL_META_PS_SA122 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_meta_ps', 'use_yn=true'),
    visible: false,
    title: 'WTL_META_PS_SA122'
    	,name : 'BASE_LAYER'
  });

  // 스텐드파이프
  var WTL_STPI_PS_SA003 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_stpi_ps', 'use_yn=true'),
    visible: false,
    title: 'WTL_STPI_PS_SA003'
    	,name : 'BASE_LAYER'
  });

  // 배수관로
  var WTL_PIPE_LM_SAA004 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_pipe_lm_p4', 'use_yn=true AND saa_cde=\'SAA004\''),
    visible: false,
    title: 'WTL_PIPE_LM_SAA004'
    	,name : 'BASE_LAYER'
  });

  // 송수관로
  var WTL_PIPE_LM_SAA003 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_pipe_lm_p3', 'use_yn=true AND saa_cde=\'SAA003\''),
    visible: false,
    title: 'WTL_PIPE_LM_SAA003'
    	,name : 'BASE_LAYER'
  });

  // 도수관로
  var WTL_PIPE_LM_SAA002 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_pipe_lm_p2', 'use_yn=true AND saa_cde=\'SAA002\''),
    visible: false,
    title: 'WTL_PIPE_LM_SAA002'
    	,name : 'BASE_LAYER'
  });

  // 취수관로
  var WTL_PIPE_LM_SAA001 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_pipe_lm_p1', 'use_yn=true AND saa_cde=\'SAA001\''),
    visible: false,
    title: 'WTL_PIPE_LM_SAA001'
    	,name : 'BASE_LAYER'
  });

  // 급수관로
  var WTL_PIPE_LM_SAA005 = new ol.layer.Tile({
    source: getTileLayerCQL('wtl_pipe_lm_p5', 'use_yn=true AND saa_cde=\'SAA005\''),
    visible: false,
    title: 'WTL_PIPE_LM_SAA005'
    	,name : 'BASE_LAYER'
  });

  // 기타 - 지적편집도
  var BML_PARC_AS = new ol.layer.Tile({
    source: getWmsFromVworld('lp_pa_cbnd_bubun'),
    crossOrigin: 'Anonymous',
    opacity: 1.1,
    visible: false,
    title: 'BML_PARC_AS'
    	,name : 'BASE_LAYER'
  });

  // 기타 - 시군구
  var SIGUNGU = new ol.layer.Tile({
    source: getWmsFromVworld('lt_c_adsigg'),
    crossOrigin: 'Anonymous',
    opacity: 1.1,
    visible: false,
    title: 'SIGUNGU(TEMP)'
  });

  // 기타 - 읍면동
  var BML_ADMB_AS = new ol.layer.Tile({
    source: getWmsFromVworld('lt_c_ademd'),
    crossOrigin: 'Anonymous',
    opacity: 1.1,
    visible: false,
    title: 'BML_ADMB_AS'
  });

  // 기타 - 하천
  var BML_RIVR_AS = new ol.layer.Tile({
    source: getWmsFromVworld('lt_c_wkmstrm'),
    crossOrigin: 'Anonymous',
    opacity: 1.1,
    visible: false,
    title: 'BML_RIVR_AS'
  });

  // 기타 - 도로명주소 도로
  var BML_ROAD_LS = new ol.layer.Tile({
    source: getWmsFromVworld('lt_l_sprd'),
    crossOrigin: 'Anonymous',
    opacity: 1.1,
    visible: false,
    title: 'BML_ROAD_LS'
  });

  // 기타 - 도로명주소 건물
  var BML_BLDG_AS = new ol.layer.Tile({
    source: getWmsFromVworld('lt_c_spbd'),
    crossOrigin: 'Anonymous',
    opacity: 1.1,
    visible: false,
    title: 'BML_BLDG_AS'
  });

  // Add Layer Group
  facilityLayerGroup = new ol.layer.Group({
    layers: [
      WTL_BLK1_AS, WTL_BLK2_AS, WTL_BLK3_AS, WTL_BLK3_SUB_AS,
      WTL_GAIN_PS_SA112, WTL_PURI_AS_SA113, WTL_SERV_PS_SA114, WTL_PRES_PS_SA206, WTL_RSRV_PS_SA120, WTL_FLOW_PS_SA117,
      WTL_PRGA_PS_SA121, WTL_MANH_PS_SA100, WTL_VALV_PS_SA200, WTL_FIRE_PS_SA119, WTL_META_PS_SA122, WTL_STPI_PS_SA003,
      WTL_PIPE_LM_SAA004, WTL_PIPE_LM_SAA003, WTL_PIPE_LM_SAA002, WTL_PIPE_LM_SAA001, WTL_PIPE_LM_SAA005,
      BML_PARC_AS, SIGUNGU, BML_ADMB_AS, BML_RIVR_AS, BML_ROAD_LS, BML_BLDG_AS
    ]
  });
  map.addLayer(facilityLayerGroup);
}

function layerUpdate() {
  var Scope = getScope();

  setTimeout(function() {
    console.log('in layers.js');
    console.log(Scope.layers);
    for (var i = 0; i < Scope.layers.length; i++) {
      var layer = Scope.layers[i];
      var C_VALUE1 = layer.C_VALUE1;            // 부모 인지 아닌지 NULL 이면 부모가 아님
      var C_CVALUE1 = layer.C_CVALUE1;          // 레이어 카테고리
      var C_MEMO = layer.C_MEMO;                // 레이어 이름
      var IS_ACTIVE = layer.isActive;
      if (C_VALUE1 == null && C_MEMO != null) {
        var WMS_LAYER_NAME = C_MEMO;
        // console.log(`WMS_LAYER_NAME: ${WMS_LAYER_NAME}, C_CVALUE1: ${C_CVALUE1}, IS_ACTIVE: ${IS_ACTIVE}`);
        var CQL_VALUE = (C_CVALUE1 === null) ? '' : C_CVALUE1;
        var wmsLayersName = WMS_LAYER_NAME + ((CQL_VALUE === '') ? '' : '_' + CQL_VALUE);
        // var wmsLayersName = WMS_LAYER_NAME + "_" + CQL_VALUE;
        //console.log('wmsLayersName: ' + wmsLayersName);

        facilityLayerGroup.getLayers().forEach(function(element, index, array) {
          var baseLayerTitle = element.get('title');
          if (baseLayerTitle === wmsLayersName) {
            element.setVisible(IS_ACTIVE);
          }
        });
      }
    }
  }, 500);
}

function getScope() {
  var isNotPopup = document.getElementById('gisPopup') === null ? true : document.getElementById('gisPopup').style.display === 'none';
  var id = isNotPopup ? 'gis' : 'gisPopup';

  return angular.element($('#' + id)).scope();
}

function getWmsFromVworld(layer) {
  return new ol.source.TileWMS({
    url: 'http://api.vworld.kr/req/wms',
    params: {
      'SERVICE': 'WMS',
      'VERSION': '1.3.0',
      'REQUEST': 'GetMap',
      'KEY': vworld_apikey,
      'FORMAT': 'image/png',
      'LAYERS': layer,
      'STYLES': layer,
      'BBOX': '126.45729521883543,35.56652119866147,126.84021791543418,35.79747553472864',
      'WIDTH': '256',
      'HEIGHT': '256',
      'TRANSPARENT': 'TRUE',
      'CRS': 'EPSG:4326',
      'DOMAIN': location.hostname
    }
  });
}