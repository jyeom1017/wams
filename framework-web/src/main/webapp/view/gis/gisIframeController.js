angular.module('app.gis').controller('GisIframeController', gisIframeController);
angular.module('app.gis').controller('GisController', gisController);

function gisIframeController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
  var initSelect1; // 자산정보 자산선택모드
  var initEdit1;
  
  $scope.showAssetItemPP = function(data,event){
	  console.log(data);
	  //event.stopPropagation();
  var param = {
          'ftr_idn': data.FTR_IDN, 'ftr_cde': data['\'10010\''], 'layer_cd': data.LAYER_CD
        };
        getAssetInfoWithGisInfo(mainDataService, param).success(function(data) {
        	console.log("getAssetInfoWithGisInfo");
        	console.log(data);
          parent.showAssetItemPP(data);
        });
        
  }
  
  $scope.closePopupEdit = function(event){
	  console.log(event);
	  if(event.offsetX<670) return;	  
	  document.getElementById('close_edit_mode').click();
  }
  
  $scope.closePopupSelect = function(event){
	  console.log(event);
	  if(event.offsetX<152) return; 
	  document.getElementById('finish_select_mode').click();
  }
  
  $scope.windowSizeToggle = function(event){
	  console.log(event);
	  var _this = $(event.currentTarget);
	  var txt = _this.text();
	  switch(txt){
	  case '보기' :
		  if($scope.selectedInfoList.length >10){
			  $("#tableWindowSize").css("height","300px");  
		  }else{
			  $("#tableWindowSize").css("height","" + ($scope.selectedInfoList.length * 60) +"px");
		  } 
		  _this.text('닫기');
		  break;
	  case '닫기' : 
		  $("#tableWindowSize").css("height","150px");
		  _this.text('보기');
		  break;
	  }
	  
	  
  }
  
  $(document).on('keyup', function(event) {
    var key = event.key || event.keyCode;

    // TODO: 블록분할, 편집모드 활성활일 때
    if (key === 'Backspace' || key === 8) {
      var dirty = initEdit1.DrawClass.dirty;
      var history = dirty.history;

      if (history.length > 0) {
        var last = history[history.length - 1];
        var lt = last.target;

        switch (last.action) {
          case 'add':
            editVectorLayer.getSource().removeFeature(lt);
            break;
          case 'update':
            var feature = editVectorLayer.getSource().getFeatureById(lt.getId());
            editVectorLayer.getSource().removeFeature(feature);
            editVectorLayer.getSource().addFeature(lt);
            break;
          case 'remove':
            editVectorLayer.getSource().addFeature(lt);
            break;
        }

        var array = [];
        switch (last.action) {
        	case 'add':
        		array = dirty.add;break;
        	case 'update':
        		array = dirty.update;break;
        	case 'remove':
        		array = dirty.ldelete;break;
        }
        for (var i = array.length - 1; i >= 0; i--) {
          if (array[i].getId() === lt.getId()) {
            array.splice(i, 1);
            break;
          }
        }
        history.pop();
      }
    }
  });

  $scope.showGisPopup = function() {
    parent.showGisPP();
  };

  $scope.$on('$viewContentLoaded', function(event, url) {
    $scope.initGis();
  });

  $scope.initGis = function() {
    initMap();
    initEdit1 = $scope.initEdit1 = initEdit();
    initSelect1 = $scope.initSelect1 = initSelect();
    initMeasure();
    initLayers();
    getLayers(mainDataService, $scope);
    $scope.clearSelectedInfoList();
    //$(".ol-overlaycontainer-stopevent").remove();
    $(".ol-zoom.ol-unselectable.ol-control").remove();
    $(".ol-rotate.ol-unselectable.ol-control.ol-hidden").remove();
    $(".ol-attribution.ol-unselectable.ol-control.ol-collapsed").remove();
  };

  $scope.checkParent = checkParent($scope);
  $scope.checkChild = checkChild($scope);

  $scope.selectedInfoList = [];
  $scope.moveToTheClickedAssetLocationAndBlink = function(item) {
	  $scope.selected_asset_sid = item.ASSET_SID;	  
    moveToTheClickedAssetLocationAndBlink(item)(initSelect1);
  };
  $scope.toggleTreeNode = toggleTreeNode($scope);
  $scope.toggleNavLayer = toggleNavLayer();
  $scope.reloadLayers = reloadLayers(mainDataService, $scope);
  $scope.checkAllLayers = checkAllLayers($scope);
  $scope.uncheckAllLayers = uncheckAllLayers($scope);
  $scope.showAssetPathPopup = function() {
    $scope.$broadcast('disableAllGISMenuFunctions');
    var scope = parent.angular.element(parent.document.querySelector('#gisFrame')).scope();
    scope.$broadcast('showAssetPathPopup');
  };
  $scope.assetInformation = function() {
    assetInformation(mainDataService, $scope)(initSelect1);
  };

  $scope.$on('SetDisabledAsset', function(event, data) {
    var layerCdMap = { 'wtl_pipe_lm': 'P0', 'wtl_fire_ps': 'FA', 'wtl_valv_ps': 'F9', 'wtl_manh_ps': 'F8', 'wtl_meta_ps': 'FB', 'wtl_flow_ps': 'F6' };
    var param1 = { layer_cd: layerCdMap[data.layer_cd], ftr_idn: data.ftr_idn };
    mainDataService.getAssetInfoWithGisInfo(param1).success(function(data1) {
      if (data1[0].ASSET_SID != null) {
        var param = { ASSET_SID: data1[0].ASSET_SID };
        mainDataService.deleteAssetItemInfo(param).success(function(data2) {

        });
      }
    });
  });

  $scope.$on('ShowSelectedInfoList', ShowSelectedInfoList(mainDataService, $scope));
  $scope.$on('ClearSelectedInfoList', ClearSelectedInfoList($scope)); // Called from select.js
  // 취소버튼 클릭
  $scope.clearSelectedInfoList = function() {
    $scope.selected_asset_sid = 0;
    clearSelectedInfoListBroadcast($scope)();
    featureBlink.clear();
    // 자산선택, 자산정보
    try {
      initSelect1.DrawSelector.unselect();
    } catch (e) {
      // console.log(e);
    }
    // 자산경로, 자산검색
    try {
      searchLayer.setSource(null);
      initSelect1.SearchSelector.destroy();
    } catch (e) {
      // console.log(e);
    }
  };

  $scope.searchAsset = function() {
    searchAsset(mainDataService, $scope)(initSelect1);
  };

  $scope.searchAddress = searchAddress();

  $scope.showDiagnosticAreaPopup = function() {
    $scope.$broadcast('disableAllGISMenuFunctions');

    parent.angular.element(parent.document.querySelector('#diagnosticAreaPopupWrap')).scope().$emit('showDiagnosticAreaPopup');
  };

  $scope.writeExcel = writeExcel($scope);

  $scope.$on('GIS_selectAssetPath1', function(event, data) {
    var param = data;

    $scope.clearSelectedInfoList();

    mainDataService.getGISInfoByAssetPath(param).then(function(response) {
      var responseData = response.data;

      if (Array.isArray(responseData) && responseData.length > 0) {
        var groupedLayerName = commonUtil.groupBy(responseData, 'C_MEMO');

        for (var layerName in groupedLayerName) {
          var tileLayerName = layerName.toLowerCase();
          var ftrIdnStringWithComma = groupedLayerName[layerName].map(function(obj) {
            return obj.FTR_IDN;
          }).join();
          var CQL = 'ftr_idn IN (' + ftrIdnStringWithComma + ')';

          // 화면에 표시
          searchLayer.setSource(getTileLayerCQL(tileLayerName, CQL));

          // 시설물 목록이 뜬다
          initSelect1.SearchSelector.enable();

          return $.ajax(WFS_URL, {
            type: 'POST', data: {
              service: 'WFS',
              version: '1.1.0',
              request: 'GetFeature',
              typeName: tileLayerName,
              srsname: 'EPSG:3857',
              outputFormat: 'application/json',
              CQL_FILTER: CQL
            }
          });
        }
      }
    }).then(function(response) {
      if (!response) {
        return;
      }
      var features = angular.copy(response.features);
      var featuresLength = features.length;

      if (featuresLength > 0) {
    	  var selected_polygon_style = new ol.style.Style({
    			image: new ol.style.Circle({
    			    radius: 6,
    			    fill: new ol.style.Fill({color: 'rgba(255,0,0,1.0)'}),
    			    stroke: new ol.style.Stroke({color: 'rgba(255,255,255,1.0)',width: 1.5})
    				}),            	  
    			stroke: new ol.style.Stroke({color: '#ff0000',width: 3})
    		  });
    	  
        for (var i = 0; i < featuresLength; i++) {
          var feature = features[i];
          var validFeature = initSelect1.SearchSelector.el.source.getFeatureById(feature.id);
          //새로운 feature만 생성하여 준다.
          if (validFeature == null) {
            var features1 = new ol.format.GeoJSON().readFeatures(feature);
            features1.forEach(function(fe){
            	fe.setStyle(selected_polygon_style);
            });
            
            initSelect1.SearchSelector.el.source.addFeatures(features1);
            initSelect1.SearchSelector.el.select.getFeatures().push(features1[0]);
          }
        }

        initSelect1.SearchSelector.el.select.dispatchEvent('select');
      }
    });
  });

  $scope.$on('addGisAsset', addGisAsset(mainDataService, $rootScope));
  $scope.$on('removeGisAsset', deleteTemporaryAsset(mainDataService,$rootScope));

  $scope.elevationDifferenceAnalysis = function() {
    $scope.$broadcast('disableAllGISMenuFunctions1');
    //alert('단차분석');
    $scope.initDanchaList();
    $('.dancha_mode').show();
    $('#dancha_new').hide();
    $scope.elevationDifferenceAnalysisClose();
    $scope.temp_pipe_list = [];
    $scope.temp_start_pipe = '';
  };
  $scope.elevationDifferenceAnalysisCancel = function($event) {
	  if(typeof $event != 'undefined'){
		  if(($event.clientX - parseInt($("#dancha_new").css('left'))) < 235) return;
	  }	  
    $('.dancha_mode').hide();
    $scope.temp_pipe_list = [];
    $scope.temp_start_pipe = '';
  };
  $scope.elevationDifferenceAnalysisDelete = function() {
    $scope.temp_pipe_list = [];
    $scope.temp_start_pipe = '';
  };
  $scope.elevationDifferenceAnalysisOpen = function() {
    $scope.insertTempPipeList();
  };
  $scope.elevationDifferenceAnalysisClose = function($event) {
	  //console.log($event);
	  if(typeof $event != 'undefined'){
		  if(($event.clientX - parseInt($($event.currentTarget).css('left'))) < 1100 - 377) return;
	  }
    $('.dancha_mode2').hide();
  };

  $scope.clickConfirmEvaluation = function() {
    $scope.$broadcast('disableAllGISMenuFunctions');
    var container = parent.angular.element(parent.document.querySelector('#diagnosticAreaPopupWrap'));
    var scope = container.scope().$parent;
    var element = document.getElementById('select__evaluationAnalysis');
    var value = element.options[element.selectedIndex].value;

    scope.$broadcast('evaluationAnalysisSettings', value);
  };

  $scope.temp_start_pipe = '';
  $scope.temp_pipe_list = [];
  $scope.nodeList = [];
  $scope.nodeInfo = [];
  var checkNodeList = [];

  function getPipeLength(ftrIdn) {
    var ln = 0;
    $.each($scope.nodeInfo, function(idx, Item) {
      if (Item.gid1 == ftrIdn) {
        ln = parseInt(Item.l1);
        return false;
      }
      if (Item.gid2 == ftrIdn) {
        ln = parseInt(Item.l2);
        return false;
      }
    });
    return ln;
  }

  var maxPipeLength = 0;
  var maxPipeLengthPath = '';

  function getPipePathLength(path) {
    var pipeLength = 0;
    var pathList = path.split(',');
    $.each(pathList, function(idx, Item) {
      pipeLength += getPipeLength(Item);
    });
    console.log(path, pipeLength);
    if (pipeLength > maxPipeLength) {
      maxPipeLength = pipeLength;
      maxPipeLengthPath = path;
    }
    return pipeLength;
  }

  function makeNodeList(idx, ftrIdn, opt, path) {
    getPipePathLength(path);
    if (ftrIdn == '') {
      return ftrIdn;
    }

    if (idx == 0) {
      checkList = [];
      $scope.nodeList = [];
      maxPipeLength = 0;
      maxPipeLengthPath = '';
    } else {
      checkList = [];
      $.each(path.split(','), function(idx1, item1) {
        if (item1 != '') {
          checkList.push(parseInt(item1));
        }
      });
    }
    $scope.nodeList.push({ idx: idx, ftrIdn: ftrIdn, opt: opt });
    if (idx > $scope.temp_pipe_list.length) {
      return;
    }
    var nextFtrIdn = '';
    var dir = 0;
    //$.each($scope.temp_pipe_list,function(idx,Item){
    if ($.inArray(ftrIdn, checkList) < 0) {
      checkList.push(ftrIdn);
      var checkList1 = angular.copy(checkList);
      var checkList2 = angular.copy(checkList);
      console.log(ftrIdn);
      var r1 = '';
      var r2 = '';
      $.each($scope.nodeInfo, function(idx2, Item2) {
        var r0 = '';
        if (Item2.gid1 == ftrIdn && parseInt(Item2.l1) > 5.0 && Item2.d1 < 0.001) {
          nextFtrIdn = Item2.gid2;
          if ($.inArray(nextFtrIdn, checkList1) < 0) {
            dir = 1;
            r0 = ftrIdn + ',' + makeNodeList(idx + 1, nextFtrIdn, dir, checkList1.toString());
            checkList1.push(nextFtrIdn);
            if (getPipePathLength(r0) > getPipePathLength(r1)) {
              r1 = r0;
            }
            //return false;
          }
        }
      });
      $.each($scope.nodeInfo, function(idx2, Item2) {
        if (Item2.gid2 == ftrIdn && parseInt(Item2.l2) > 5.0 && Item2.d1 < 0.001) {
          nextFtrIdn = Item2.gid1;
          if ($.inArray(nextFtrIdn, checkList2) < 0) {
            dir = 2;
            r0 = ftrIdn + ',' + makeNodeList(idx + 1, nextFtrIdn, dir, checkList2.toString());
            checkList2.push(nextFtrIdn);
            if (getPipePathLength(r0) > getPipePathLength(r2)) {
              r2 = r0;
            }
            //return false;
          }
        }
      });

      if (r1 != '' || r2 != '') {
        return (getPipePathLength(r1) > getPipePathLength(r2)) ? r1 : r2;
      } else {
        return ftrIdn;
      }
    } else {
      /*
      for(a in $scope.temp_pipe_list){
        var Item = $scope.temp_pipe_list[a];
        if($.inArray(Item.FTR_IDN,checkNodeList)>=0 && $.inArray(Item.FTR_IDN,checkList)<0){
          checkList.push(Item.FTR_IDN);
          return ftrIdn +',' + makeNodeList(idx+1,Item.FTR_IDN,dir,checkList.toString());
        }
      }*/
      return ftrIdn;
    }
    //});
  }

  function getDistance(p1, p2) {
    var p01 = p1.replace('POINT(', '').replace(')', '').split(' ');
    var p02 = p2.replace('POINT(', '').replace(')', '').split(' ');
    result = (parseFloat(p01[0]) - parseFloat(p02[0])) * (parseFloat(p01[0]) - parseFloat(p02[0])) + (parseFloat(p01[1]) - parseFloat(p02[1])) * (parseFloat(p01[1]) - parseFloat(p02[1]));
    return Math.sqrt(result);
  }

  function checkDistance(p0, p1, p2) {
    //(getDistance(nodePointList[nodePointList.length-1].point,Item2.a2)<getDistance(nodePointList[nodePointList.length-1].point,Item2.a1))
    return (getDistance(p0, p1) < getDistance(p0, p2)) ? p1 : p2;
  }

  $scope.getTempPipeList = function() {
    var param = {};
    mainDataService.getElevationDifferenceAnalysisInfo(param).success(function(data) {
      console.log(data);
      $scope.nodeInfo = data.nodeInfo;
      $scope.temp_pipe_list = data.list;
      checkNodeList = [];
      $.each($scope.nodeInfo, function(idx, Item) {
        if ($.inArray(Item.gid1, checkNodeList) < 0) {
          checkNodeList.push(Item.gid1);
        }
        if ($.inArray(Item.gid2, checkNodeList) < 0) {
          checkNodeList.push(Item.gid2);
        }
      });
      //debugger;
      $scope.nodeList = [];
      var path = makeNodeList(0, parseInt($scope.temp_start_pipe), 0, '');
      console.log(path);
      console.log($scope.nodeList);
      //console.log(checkList);
      //$scope.nodeInfo.size() + 2 개의포인트 좌표가 필요함
      //찾기
      var nodePointList = [];
      var checkPath = maxPipeLengthPath.split(',');
      var list = [];
      $.each(checkPath, function(idx, Item) {
        $.each($scope.nodeList, function(idx2, Item2) {
          if (Item == Item2.ftrIdn && Item2.idx == list.length) {
            list.push(Item2);
            return false;
          }
        });

      });
      $scope.nodeList = list;

      $.each($scope.nodeList, function(idx, Item) { // idx : 인덱스 숫자 / Item : 데이터

        /*$.each($scope.temp_pipe_list,function(idx1,Item1){
        if(Item.ftrIdn==Item1.FTR_IDN){
          return false;
        }
      });*/
        $.each($scope.nodeInfo, function(idx2, Item2) {
          //debugger;
          if (idx < $scope.nodeList.length - 1) {
            if ($scope.nodeList[idx + 1].opt == 1 && Item.ftrIdn == Item2.gid1 && $scope.nodeList[idx + 1].ftrIdn == Item2.gid2) {
              if (idx == 0) {
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: null, a1: Item2.a1, a2: Item2.a2 });
              }
              if (getDistance(nodePointList[nodePointList.length - 1].a1, Item2.b2) <= Item2.d1) {
                if (idx == 0) {
                  nodePointList[0].point = nodePointList[0].a2;
                }
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: Item2.b2, a1: Item2.b1, a2: Item2.b2 });
              } else if (getDistance(nodePointList[nodePointList.length - 1].a2, Item2.b2) <= Item2.d1) {
                if (idx == 0) {
                  nodePointList[0].point = nodePointList[0].a1;
                }
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: Item2.b2, a1: Item2.b1, a2: Item2.b2 });
              } else if (getDistance(nodePointList[nodePointList.length - 1].a2, Item2.b1) <= Item2.d1) {
                if (idx == 0) {
                  nodePointList[0].point = nodePointList[0].a1;
                }
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: Item2.b1, a1: Item2.b1, a2: Item2.b2 });
              } else {
                if (idx == 0) {
                  nodePointList[0].point = nodePointList[0].a2;
                }
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: Item2.b1, a1: Item2.b1, a2: Item2.b2 });
              }
              return false;
            }
            if ($scope.nodeList[idx + 1].opt == 2 && Item.ftrIdn == Item2.gid2 && $scope.nodeList[idx + 1].ftrIdn == Item2.gid1) {
              if (idx == 0) {
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: null, a1: Item2.b1, a2: Item2.b2 });
              }
              if (getDistance(nodePointList[nodePointList.length - 1].a2, Item2.a2) <= Item2.d1) {
                if (idx == 0) {
                  nodePointList[0].point = nodePointList[0].a1;
                }
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: Item2.a2, a1: Item2.a1, a2: Item2.a2 });
              } else if (getDistance(nodePointList[nodePointList.length - 1].a1, Item2.a2) <= Item2.d1) {
                if (idx == 0) {
                  nodePointList[0].point = nodePointList[0].a2;
                }
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: Item2.a2, a1: Item2.a1, a2: Item2.a2 });
              } else if (getDistance(nodePointList[nodePointList.length - 1].a1, Item2.a1) <= Item2.d1) {
                if (idx == 0) {
                  nodePointList[0].point = nodePointList[0].a2;
                }
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: Item2.a1, a1: Item2.a1, a2: Item2.a2 });
              } else {
                if (idx == 0) {
                  nodePointList[0].point = nodePointList[0].a1;
                }
                nodePointList.push({ ftrIdn: $scope.nodeList[idx].ftrIdn, point: Item2.a1, a1: Item2.a1, a2: Item2.a2 });
              }
              return false;
            }
            if ($scope.nodeList[idx + 1].opt == 0 && Item.ftrIdn == Item2.gid1) {
              if (idx > 1) {
                nodePointList.push({ ftrIdn: Item.ftrIdn, point: Item2.a1, a1: Item2.a1, a2: Item2.a2 });
              }
              return false;
            }
          } else {
            //끝점 처리
            if (Item.ftrIdn == Item2.gid1 && $scope.nodeList[idx - 1].ftrIdn == Item2.gid2) {
              if (getDistance(nodePointList[nodePointList.length - 1].point, Item2.a1) <= Item2.d1) {
                nodePointList.push({ ftrIdn: Item.ftrIdn, point: Item2.a1, a1: Item2.a1, a2: Item2.a2 });
                nodePointList.push({ ftrIdn: Item.ftrIdn, point: Item2.a2, a1: Item2.a1, a2: Item2.a2 });
              } else {
                nodePointList.push({ ftrIdn: Item.ftrIdn, point: Item2.a2, a1: Item2.a1, a2: Item2.a2 });
                nodePointList.push({ ftrIdn: Item.ftrIdn, point: Item2.a1, a1: Item2.a1, a2: Item2.a2 });
              }
              return false;
            } else if (Item.ftrIdn == Item2.gid2 && $scope.nodeList[idx - 1].ftrIdn == Item2.gid1) {
              if (getDistance(nodePointList[nodePointList.length - 1].point, Item2.b1) <= Item2.d1) {
                nodePointList.push({ ftrIdn: Item.ftrIdn, point: Item2.b1, a1: Item2.b1, a2: Item2.b2 });
                nodePointList.push({ ftrIdn: Item.ftrIdn, point: Item2.b2, a1: Item2.b1, a2: Item2.b2 });
              } else {
                nodePointList.push({ ftrIdn: Item.ftrIdn, point: Item2.b2, a1: Item2.b1, a2: Item2.b2 });
                nodePointList.push({ ftrIdn: Item.ftrIdn, point: Item2.b1, a1: Item2.b1, a2: Item2.b2 });
              }
              return false;
            }
            /*else if(Item.ftrIdn==Item2.gid1 && $scope.nodeList[idx-1].ftrIdn == Item2.gid1){
              if(getDistance(nodePointList[nodePointList.length-1].point,Item2.a1)<=Item2.d1){
                nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a1,a1:Item2.a1,a2:Item2.a2});
                nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a2,a1:Item2.a1,a2:Item2.a2});
              }else{
                nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a2,a1:Item2.a1,a2:Item2.a2});
                nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a1,a1:Item2.a1,a2:Item2.a2});
              }
              return false;
            }else if(Item.ftrIdn==Item2.gid2 && $scope.nodeList[idx-1].ftrIdn == Item2.gid2){
              if(getDistance(nodePointList[nodePointList.length-1].point,Item2.b2)<=Item2.d1){
                nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b2,a1:Item2.b1,a2:Item2.b2});
                nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b1,a1:Item2.b1,a2:Item2.b2});
              }else{
                nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b1,a1:Item2.b1,a2:Item2.b2});
                nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b2,a1:Item2.b1,a2:Item2.b2});
              }
              return false;
            }*/
          }
        });
      });
      console.log(nodePointList);
      $.each(nodePointList, function(idx, Item) {
        var loc = Item.point.replace('POINT(', '').replace(')', '').split(' ');
        Item.lv = null;
        Item.lat = loc[1].substring(0, 17);
        Item.lng = loc[0].substring(0, 17);
      });

      var cnt = 0;
      $.each(nodePointList, function(idx, Item) {
        var param = Item.point;
        mainDataService.getGoogleGeoElevationApi(param).success(function(data) {
          cnt++;
          console.log(data);
          //data.results.elevation;
          $.each(nodePointList, function(idx2, Item2) {
            //debugger;
            if (parseFloat(Item2.lng) == data.results[0].location.lng && parseFloat(Item2.lat) == data.results[0].location.lat && Item2.lv == null) {
              Item2.lv = (Math.floor(parseFloat(data.results[0].elevation) * 10 + 0.01) / 10);
              return false;
            }
          });
          if (cnt >= nodePointList.length) {
            //alert(JSON.stringify(nodePointList));
            console.log(nodePointList);
            monthGraph(nodePointList);
          }
        });
      });
      console.log($scope.nodeInfo);

    });
  };

  $scope.insertTempPipeList = function() {
    if ($scope.temp_pipe_list.length < 2) {
      alertify.alert('단차분석을 위해서는 2개이상의 관로를 선택하세요');
      return;
    }
    $('.dancha_mode').hide();
    $('.dancha_mode2').show();
    var param = { ITEM_LIST: [] };
    $.each($scope.temp_pipe_list, function(idx, Item) {
      param.ITEM_LIST.push(Item.FTR_IDN);
    });
    //monthGraph();
    //$scope.getTempPipeList();

    mainDataService.saveTempPipeList(param).success(function(data) {
      $scope.getTempPipeList();
      //$scope.temp_pipe_list = [];
    });

  };

  $scope.SelectStartPipe = function() {
    //debugger;

    var checkTempPipeList = [];
    $.each($scope.temp_pipe_list, function(idx, Item) {
      if ($.inArray(Item.FTR_IDN, checkTempPipeList)) {
        checkTempPipeList.push(Item.FTR_IDN);
      }
    });

    $.each($scope.selectedInfoList, function(idx, Item) {
      $scope.temp_start_pipe = Item['\'10009\''];
      if ($.inArray($scope.temp_start_pipe, checkTempPipeList)) {
        $scope.temp_pipe_list.push({ FTR_IDN: Item['\'10009\''] });
      }
      return false;
    });
  };

  $scope.SelectLinkedPipe = function() {
    var checkTempPipeList = [];
    $.each($scope.temp_pipe_list, function(idx, Item) {
      if ($.inArray(Item.FTR_IDN, checkTempPipeList)) {
        checkTempPipeList.push(Item.FTR_IDN);
      }
    });

    $.each($scope.selectedInfoList, function(idx, Item) {
      if ($.inArray(Item['\'10009\''], checkTempPipeList)) {
        $scope.temp_pipe_list.push({ FTR_IDN: Item['\'10009\''] });
      }
    });
  };

  $scope.danchaAnalysisList = [];

  $scope.initDanchaList = function() {
    if (localStorage.getItem('danchaList') != null && localStorage.getItem('danchaList') != '') {
      $scope.danchaAnalysisList = JSON.parse(localStorage.getItem('danchaList'));
    }
  };

  $scope.danchaSelect = function(item) {
    $scope.selectedDanchaResult = item;
  };

  $scope.showSelectedDanchaResult = function() {
    if ($scope.selectedDanchaResult == null) {
      alertify.alert('Info', '표시할 단차 분석명을 선택하세요', function() {

      });
      return;
    }
    $('.dancha_mode2').show();
    $scope.danchaTitle = $scope.selectedDanchaResult.name;
    var nodePointList = JSON.parse($scope.selectedDanchaResult.nodePointList);
    $scope.temp_pipe_list = JSON.parse($scope.selectedDanchaResult.temp_pipe_list);
    $scope.nodeList = JSON.parse($scope.selectedDanchaResult.nodeList);
    monthGraph(nodePointList);
  };
  $scope.showDancha_new = function() {
    $('#dancha_list').hide();
    $('#dancha_new').show();
    $scope.selectedDanchaResult = null;
    $scope.danchaTitle = '';
  };

  $scope.danchaListDelete = function() {
    var list = [];
    $.each($scope.danchaAnalysisList, function(idx, Item) {
      if ($scope.selectedDanchaResult.sid == Item.sid) {

      } else {
        list.push(angular.copy(Item));
      }
    });
    $scope.danchaAnalysisList = list;
    localStorage.setItem('danchaList', JSON.stringify(list));
  };
  $scope.saveDanchaResult = function() {
    if ($scope.selectedDanchaResult == null) {
      var max_sid = 0;
      $.each($scope.danchaAnalysisList, function(idx, Item) {
        if (max_sid < Item.sid) {
          max_sid = Item.sid;
        }
      });
      var yyyymmdd = '';
      //var name = prompt('분석명을 입력하세요');
      var name = $scope.danchaTitle;
      var yyyy = (new Date()).getFullYear();
      var month = (new Date()).getMonth() + 1;
      var day = (new Date()).getDate();
      var sid = max_sid + 1;
      yyyymmdd = '' + yyyy + '-' + ('0' + month).substr(-2) + '-' + ('0' + day).substr(-2);

      $scope.danchaAnalysisList.push({
        sid: sid
        , name: name
        , analysis_dt: yyyymmdd
        , nodePointList: JSON.stringify($scope.DanChaResult)
        , nodeList: JSON.stringify($scope.nodeList)
        , temp_pipe_list: JSON.stringify($scope.temp_pipe_list)
      });
    } else {
      $.each($scope.danchaAnalysisList, function(idx, Item) {
        if ($scope.selectedDanchaResult.sid == Item.sid) {
          Item.name = $scope.danchaTitle;
        }
      });
    }
    var list = angular.copy($scope.danchaAnalysisList);
    localStorage.setItem('danchaList', JSON.stringify(list));
    alertify.success('저장 되었습니다.');
  };
  $scope.danchaListClose = function($event) {
	  if(typeof $event != 'undefined'){
		  if(($event.clientX - parseInt($("#dancha_list").css('left'))) < 375) return;
	  }	  
    $scope.elevationDifferenceAnalysisCancel();
  };

  var monthGraph = function(nodePointList) {
    console.log(nodePointList);
    $scope.DanChaResult = nodePointList;
    // var dateData = []; // 측정일시 데이터
    // var fluxData = []; // 유량 데이터
    // var waterPressureData = []; // 수압 데이터
    //debugger;
    var dataArray = [];

    if ($scope.nodeList != undefined && nodePointList.length > 0) {

      $.each(nodePointList, function(idx, Item) { // idx : 인덱스 숫자 / Item : 데이터

        var dataObj = {};
        dataObj.ftrIdn = Item.ftrIdn;
        dataObj.level = (Math.floor(Item.lv * 10.0 + 0.01) / 10.0);
        dataObj.depth = (Math.floor(1.5 * 10.0 + 0.01) / 10.0);
        $.each($scope.temp_pipe_list, function(idx1, Item1) {
          if (Item.ftrIdn == Item1.FTR_IDN) {
            dataObj.level = (Math.floor((Item.lv - parseFloat(Item1.DEP)) * 10.0 + 0.01) / 10.0);
            dataObj.depth = (Math.floor(parseFloat(Item1.DEP) * 10.0 + 0.01) / 10.0);
            return false;
          }
        });
        dataObj.level1 = (Math.floor((dataObj.level - dataObj.depth) * 10.0 + 0.01) / 10.0);
        dataArray.push(dataObj);
      });
    }

    console.log(dataArray);

    var chartData = dataArray;

    rMateChartH5.create('chart1', 'levelGraph', '', '100%', '100%');

    // 스트링 형식으로 레이아웃 정의.
    var layoutStr =
        '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
        + '<Options>'
        + '<Caption text="단차분석"/>'
        + '<SubCaption text="단차분석" textAlign="center" />'
        + '<Legend defaultMouseOverAction="false" useVisibleCheck="false"/>'
        + '</Options>'
        + '<NumberFormatter id="numfmt" useThousandsSeparator="true"/>'
        //+ '<Column2DChart showDataTips="true" columnWidthRatio="0.4" itemClickJsFunction="chartItemClickHandler"  dataTipJsFunction="dataTipFunc">'
        + '<Column2DChart showDataTips="true" columnWidthRatio="0.4" itemClickJsFunction="chartItemClickHandler" dataTipJsFunction="dataTipFuncDanCha1">'
        + '<horizontalAxis>'
        + '<CategoryAxis categoryField="ftrIdn"/>'
        + '</horizontalAxis>'
        + '<verticalAxis>'
        + '<LinearAxis formatter="{numfmt}" minimum="0" title="해발고도(m)" />'
        + '</verticalAxis>'
        + '<series>'
        /*
       type 속성을 stacked로 변경
         type속성으로는
        clustered : 일반적인 다중데이터(차트의 멀티시리즈)방식으로 데이터를 표현합니다.(Default)
       stacked : 데이터를 위에 쌓아 올린 방식으로 표현 합니다.
         overlaid : 수치 데이터 값을 겹쳐서 표현 합니다. 주로 목표 위치와 현재 위치를 나타낼 때 많이 쓰입니다.
         100% : 차트의 수치 데이터를 퍼센티지로 계산 후 값을 퍼센티지로 나타냅니다.
        */
        + '<Column2DSet type="stacked" showTotalLabel="false" totalLabelJsFunction="totalFunc" labelYOffset="-5">'
        + '<series>'
        + '<Column2DSeries  yField="level1" displayName="매설관 해발고도"  labelPosition="inside" insideLabelJsFunction="seriesLabelFunc2" lineToEachItems="true" showLinkLabels="false" >'
        /* lineToEachItems : True로 설정 하면 Series간에 선을 이어준다 */
        + '<showDataEffect>'
        + '<SeriesInterpolate/>'
        + '</showDataEffect>'
        + '</Column2DSeries>'
        + '<Column2DSeries  yField="depth" displayName="매설깊이"  labelPosition="inside" insideLabelJsFunction="seriesLabelFunc1" lineToEachItems="true" showLinkLabels="false" >'
        + '<showDataEffect>'
        + '<SeriesInterpolate/>'
        + '</showDataEffect>'
        + '</Column2DSeries>'
        + '</series>'
        + '</Column2DSet>'
        + '</series>'
        + '</Column2DChart>'
        + '</rMateChart>';

    // 차트 데이터
    /*
    var chartData =
     [{"Month":"Jan", "phone":12, "tv":11, "tablet":12},
     {"Month":"Feb", "phone":14, "tv":19, "tablet":17},
      {"Month":"Mar", "phone":23, "tv":25, "tablet":20},
      {"Month":"Apr", "phone":20, "tv":20, "tablet":18},
      {"Month":"May", "phone":35, "tv":25, "tablet":25},
      {"Month":"Jun", "phone":20, "tv":22, "tablet":23},
      {"Month":"Jul", "phone":17, "tv":20, "tablet":17},
      {"Month":"Aug", "phone":23, "tv":21, "tablet":21},
      {"Month":"Sep", "phone":18, "tv":17, "tablet":10}];
     */
    // rMateChartH5.calls 함수를 이용하여 차트의 준비가 끝나면 실행할 함수를 등록합니다.
    //
    // argument 1 - rMateChartH5.create시 설정한 차트 객체 아이디 값
    // argument 2 - 차트준비가 완료되면 실행할 함수 명(key)과 설정될 전달인자 값(value)
    //
    // 아래 내용은
    // 1. 차트 준비가 완료되면 첫 전달인자 값을 가진 차트 객체에 접근하여
    // 2. 두 번째 전달인자 값의 key 명으로 정의된 함수에 value값을 전달인자로 설정하여 실행합니다.
    rMateChartH5.calls('chart1', {
      'setLayout': layoutStr,
      'setData': chartData
    });

    // 스택 수치 합 사용자 정의 함수
    function totalFunc(index, data, value) {
      //if (index == 3) {
      return value;
      //}
      //return '';
    }

  };

  function dataTipFunc(seriesId, seriesName, index, xName, yName, data, values) {
      //console.log(seriesId, seriesName, index, xName, yName, data, values);
    	console.log(data);
    	return "";
    	/*
      return '<table cellpadding=\'0\' cellspacing=\'1\'>'
          + '<tr>'
          + '<td align=\'center\' colspan=\'2\' style=\'border-bottom:solid 1px #8b8b8b;\'><img	src=\'../rMateChartH5/Assets/Images/monitor.png\'></td>'
          + '</tr><tr>'
          + '<td >series ID</td><td align=\'center\'>' + "" + '</td>'
          + '</tr></table>';
          */
  }
  
  $scope.$on('disableAllGISMenuFunctions', function(event,data) {
	  
	  if(typeof data == 'undefined'){
		  //document.getElementById('removeMeasure').click(); //측정도구 취소
		  document.getElementById('cancelMeasure').click(); //측정도구 취소
	  }else{
		  //if(data.menuid != 'measure') document.getElementById('removeMeasure').click(); //측정도구 취소
		  if(data.menuid != 'measure') document.getElementById('cancelMeasure').click(); //측정도구 취소
	  }
	  
    var phase = $scope.$root.$$phase;

    if (phase === '$apply' || phase === '$digest') {
      f();
    } else {
      $scope.$apply(function() {
        f();
      });
    }

    function f() {
      $scope.clearSelectedInfoList(); // 자산경로, 자산선택, 자산정보, 자산검색
      document.getElementById('finish_select_mode').click(); // 자산선택, 자산정보
      $scope.elevationDifferenceAnalysisCancel(); // 단차분석
      $scope.elevationDifferenceAnalysisClose(); // 단차분석

      document.getElementById('btnCloseBlockDivision').click(); // 블록분할
      //document.getElementById('finish_edit_mode').click(); // 자산편집
      document.getElementById('close_edit_mode').click(); // 자산편집

      parent.document.getElementById('indirectConditionEvaluationPopup').style.display = 'none'; // 평가분석 > 간접상태평가
      parent.document.getElementById('directConditionEvaluationPopup').style.display = 'none'; // 평가분석 > 직접상태평가

      featureBlink.clear(); // 벡터 깜박임 제거
      
      map.getLayers().getArray()
      .filter(layer => layer.get('name') === 'Marker')
      .forEach(layer => map.removeLayer(layer));
    }
  });

  // 자산선택, 블록분할
  $scope.$on('disableAllGISMenuFunctions1', function() {
    var phase = $scope.$root.$$phase;

    if (phase === '$apply' || phase === '$digest') {
      f();
    } else {
      $scope.$apply(function() {
        f();
      });
    }
    
    function f() {
      // $scope.clearSelectedInfoList(); // 자산경로, 자산선택, 자산정보, 자산검색
      // document.getElementById('finish_select_mode').click(); // 자산선택, 자산정보
      // $scope.elevationDifferenceAnalysisCancel(); // 단차분석
      // $scope.elevationDifferenceAnalysisClose(); // 단차분석
      document.getElementById('btnCloseBlockDivision').click(); // 블록분할
      //document.getElementById('finish_edit_mode').click(); // 자산편집
      document.getElementById('close_edit_mode').click(); // 자산편집

      parent.document.getElementById('indirectConditionEvaluationPopup').style.display = 'none'; // 평가분석 > 간접상태평가
      parent.document.getElementById('directConditionEvaluationPopup').style.display = 'none'; // 평가분석 > 직접상태평가

      featureBlink.clear(); // 벡터 깜박임 제거
      
      //document.getElementById('removeMeasure').click(); //측정도구 취소
      document.getElementById('cancelMeasure').click(); //측정도구 취소
      
      map.getLayers().getArray()
      .filter(layer => layer.get('name') === 'Marker')
      .forEach(layer => map.removeLayer(layer));
    }
  });
}

function gisController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	
  $scope.tabid = $stateParams.tabid;

  $scope.showSelectedInfoList = function() {
    $scope.selectedInfoList = getSelectGisObj();
  };

  $scope.showGisPopup = function() {
    $rootScope.$broadcast('showGisPopup', { callbackId: 'gisController_testpopup' });

    /*		$("#gisPopup").show();
            $timeout(function(){
              $(".side_nav_title").on("click",function(){
              $(".side_nav").stop().toggle();
              $(this).toggleClass("active");
            });
            initMap();
            },1000);*/
    /*
    $("#gisPopup").dialog({
            title: 'GIS팝업',
            autoOpen: true,
            height: '1600px',
            width: '800px',
            modal: true,
            close: function( event, ui ) {
                //deleteTemp(); //임시글 삭제 함수 실행
            }
        });*/
  };
  var assetInfoPopupTimer = null;
  var errorPopupTimer = null;
  $scope.$on('showAssetItemPP', function(event, args) {
	  if (typeof args.AssetItemInfo != 'undefined') {
	      assetInfoPopupTimer = $timeout(function() {
	    	$scope.$broadcast('ShowAssetItemPopup', args);
	        // GIS 팝업보다 위에 나오기 위함
	        document.getElementById('dialog-AssetItemPopup').style.zIndex = '9991';
	      }, 500);
	      if(errorPopupTimer!=null)
	      $timeout.cancel(errorPopupTimer);
	    } else {
	      errorPopupTimer = $timeout(function() {
	        alertify.alert('오류', '등록된 자산이 없습니다.', function() {
	          /*
	          $rootScope.$broadcast('RegistAssetItemPopup',
	              {asset_info:{},ItemList:{},callback_Id:'',callback_Fn : function(){
	                alertify.success('자산등록');
	              }});
	              */
	        });
	      }, 500);
	      if(assetInfoPopupTimer!=null)
	      $timeout.cancel(assetInfoPopupTimer);
    }
  });

  $scope.$on('showDiagnosticAreaPopup', (function() {
    var initialized = false;

    return function() {
      $scope.diagnosticGroupSelectionButtonFlag = false;

      if (!initialized) {
        setDatePicker();
        $scope.diagnosticAreaPopup.grid.setGrid();
        $scope.stateDiagnosticGroupPopup.grid.setGrid();
        //initialized = true;
      }

      $('#diagnosticAreaPopup form input').each(function(index, element) {
        element.value = '';
      });

      document.getElementById('diagnosticAreaPopup').style.display = 'block';
    };
  })());

  var DIAG_AREA_SID = 0;

  $scope.diagnosticAreaTabClick = function($event) {
    if ($event.target.tagName !== 'BUTTON') {
      return;
    }

    var element = $event.target; // button

    deactivateAndHideAllTabs(element);
    activateAndShowThisTab(element);

    function deactivateAndHideAllTabs(element) {
      var grandParent = element.parentElement.parentElement; // XXX: ul > li > button
      var nodeListOf = grandParent.querySelectorAll('button[data-tab]');

      nodeListOf.forEach(function(el) {
        el.classList.remove('active');
        tab(el).setDisplay('none');
      });
    }

    function activateAndShowThisTab(element) {
      element.classList.add('active');
      tab(element).setDisplay('block');
    }

    function tab(el) {
      var element = document.getElementById('diagnosticAreaTab' + el.dataset.tab);

      return {
        setDisplay: function(mode) {
          element.style.display = mode;
        }
      };
    }
  };

  $scope.diagnosticAreaPopup = {
    grid: {
      'id': 'diagnosticAreaList1', 'page': 1, 'size': GridConfig.sizeSS, 'selRowData': {}, setGrid: function() {
        $('#diagnosticAreaList1').jqGrid('GridUnload');
        return pagerJsonGrid({
          grid_id: this.id,
          pager_id: 'diagnosticAreaListPager1',
          url: '/gis/getDiagnosticAreaList.json',
          condition: { page: this.page, rows: this.size },
          rowNum: this.size,
          colNames: ['DIAG_AREA_SID', '순번', '진단구역명', '관로수', '등록일', '등록자'],
          colModel: [
            { name: 'DIAG_AREA_SID', hidden: true },
            { name: 'RNUM', width: 40 },
            { name: 'DIAG_AREA_NM', width: 110 },
            { name: 'PIPELINE_CNT', width: 80 },
            { name: 'WRT_YMD', width: 110 },
            { name: 'WRTR_NM', width: 80 }
          ],
          onSelectRow: function(rowid) {
            $scope.diagnosticAreaPopup.grid.selRowData = $(this).jqGrid('getRowData', rowid);
          },
          gridComplete: function() {
            var $this = $(this);
            var ids = $this.jqGrid('getDataIDs');
            $this.setSelection(ids[0]);
            $scope.diagnosticAreaPopup.grid.page = $this.getGridParam('page');
            // $scope.diagnosticAreaPopup.grid.count = $this.getGridParam('records');
          }
        });
      }
    }, search: function() {
      var form = document.querySelector('#diagnosticAreaPopup #form1');

      $('#' + this.grid.id).jqGrid('setGridParam', {
        page: 1,
        postData: {
          s_date: form.querySelector('input[name="s_date"]').value,
          e_date: form.querySelector('input[name="e_date"]').value,
          diag_area_nm: form.querySelector('input[name="diag_area_nm"]').value
        }
      }).trigger('reloadGrid');
    }, delete: function() {
      var selRowData = this.grid.selRowData;
      var diagAreaSid = commonUtil.isEmpty(selRowData) ? undefined : selRowData.DIAG_AREA_SID;

      if (commonUtil.isEmpty(selRowData) || commonUtil.isEmpty(diagAreaSid)) {
        alertify.alert('항목을 선택해주세요.');
        return;
      }
      alertify.confirm('삭제 확인', '정말 삭제하시겠습니까?', function() {
        mainDataService.deleteDiagnosticArea({ 'diag_area_sid': diagAreaSid }).success(function(data) {
          if (data > 0) {
            alertify.success('삭제되었습니다.');
          } else {
            alertify.success('지정된 테이블에서 삭제할 수 없습니다.');
          }
          $scope.diagnosticAreaPopup.search();
        });
      }, function() {
      }); // alertify.confirm에서 실패 시 동작하는 함수를 선언해줘야 정상 작동함
    }
  };

  $scope.stateDiagnosticGroupPopup = {
    grid: {
      'id': 'diagnosticAreaList2', 'page': 1, 'size': GridConfig.sizeSS, 'selRowData': {}, setGrid: function() {
        $('#diagnosticAreaList2').jqGrid('GridUnload');
        return pagerJsonGrid({
          grid_id: this.id,
          pager_id: 'diagnosticAreaListPager2',
          url: '/gis/getStateDiagnosisGroupList.json',
          condition: { page: this.page, rows: this.size },
          rowNum: this.size,
          colNames: ['STATE_DIAG_GROUP_SID', '자산경로', '관재질', '관경', '관용도', '급수용도', '순번', '상태진단 그룹명', '관로수', '등록일', '등록자'],
          colModel: [
            { name: 'STATE_DIAG_GROUP_SID', hidden: true },
            { name: 'ASSET_PATH_SID', hidden: true },
            { name: 'PIPE_MATERIAL', hidden: true },
            { name: 'PIPE_DIAMETER', hidden: true },
            { name: 'PIPE_PURPOSE', hidden: true },
            { name: 'WATER_PURPOSE', hidden: true },
            { name: 'RNUM', width: 40 },
            { name: 'STATE_DIAG_GROUP_NM', width: 110 },
            { name: 'PIPELINE_CNT', width: 80 },
            { name: 'WRT_YMD', width: 110 },
            { name: 'WRTR_NM', width: 80 }
          ],
          onSelectRow: function(rowid) {
            var rowData = $(this).jqGrid('getRowData', rowid);
            $scope.stateDiagnosticGroupPopup.grid.selRowData = rowData;
            if ($scope.diagnosticGroupSelectionButtonFlag === true) {
              alertify.confirm('선택 확인', '선택하시겠습니까?', function() {
                $scope.$broadcast('confirmSelectionOfDiagnosticGroup', { 'rowData': rowData });
                $rootScope.$broadcast('setDiagnosticGroupSelectionButtonFlag', { 'flag': false });
              }, function() {
              });
            }
          },
          gridComplete: function() {
            var $this = $(this);
            var ids = $this.jqGrid('getDataIDs');
            $this.setSelection(ids[0]);
            $scope.stateDiagnosticGroupPopup.grid.page = $this.getGridParam('page');
            // $scope.stateDiagnosticGroupPopup.grid.count = $this.getGridParam('records');
            $scope.$apply();
          }
        });
      }
    }, search: function() {
      var form = document.querySelector('#diagnosticAreaPopup #form2');

      $('#' + this.grid.id).jqGrid('setGridParam', {
        postData: {
          s_date: form.querySelector('input[name="s_date"]').value,
          e_date: form.querySelector('input[name="e_date"]').value,
          state_diag_group_nm: form.querySelector('input[name="state_diag_group_nm"]').value
        }
      }).trigger('reloadGrid');
    }, delete: function() {
      var selRowData = this.grid.selRowData;
      var stateDiagGroupSid = commonUtil.isEmpty(selRowData) ? undefined : selRowData.STATE_DIAG_GROUP_SID;

      if (commonUtil.isEmpty(selRowData) || commonUtil.isEmpty(stateDiagGroupSid)) {
        alertify.alert('항목을 선택해주세요.');
        return;
      }

      alertify.confirm('삭제 확인', '정말 삭제하시겠습니까?', function() {
        var param = { 'state_diag_group_sid': stateDiagGroupSid };

        mainDataService.deleteStateDiagnosisGroup(param).success(function(data) {
          if (data > 0) {
            alertify.success('삭제되었습니다.');
          } else {
            alertify.success('지정된 테이블에서 삭제할 수 없습니다.');
          }
          $scope.stateDiagnosticGroupPopup.search();
        });
      }, function() {
      }); // alertify.confirm에서 실패 시 동작하는 함수를 선언해줘야 정상 작동함
    }
  };

  function getActiveTabNumber() {
    return $('#diagnosticAreaTab').find('button.active').data('tab');
  }

  $scope.performDiagnosticAreaNewPopup = function() {
    var id = getActiveTabNumber();

    if (id === 1) {
      showDiagnosticAreaDetailNewPopup();
    } else if (id === 2) {
      showStateDiagnosticGroupDetailNewPopup();
    }
  };

  $scope.performDiagnosticAreaEditPopup = function() {
    var id = getActiveTabNumber();

    if (id === 1) {
      showDiagnosticAreaDetailEditPopup();
    } else if (id === 2) {
      showStateDiagnosticGroupDetailEditPopup();
    }
  };

  function showDiagnosticAreaDetailNewPopup() {
    showDiagnosticAreaDetailPopup(0);
  }

  function showDiagnosticAreaDetailEditPopup() {
    var selRowData = $scope.diagnosticAreaPopup.grid.selRowData;

    if (commonUtil.isEmptyObject(selRowData)) {
      return;
    }

    showDiagnosticAreaDetailPopup(selRowData.DIAG_AREA_SID, selRowData);
  }

  function showDiagnosticAreaDetailPopup(diagAreaSid, selRowData) {
    $scope.$broadcast('showDiagnosticAreaDetailPopup', { 'DIAG_AREA_SID': diagAreaSid });
    $scope.$broadcast('setDiagnosticAreaDetailPopupInfo', selRowData);
  }

  $scope.$on('showDiagnosticAreaDetailPopup', (function() {
    var initialized = false;

    return function(event, data) {
      DIAG_AREA_SID = data.DIAG_AREA_SID;
      var url = $scope.diagnosticAreaDetailPopup.grid.getUrl();

      //if (!initialized) {
      $scope.diagnosticAreaDetailPopup.grid.setGrid();
      //initialized = true;
      //}

      $scope.diagnosticAreaDetailPopup.search(url);

      document.getElementById('diagnosticAreaDetailPopup').style.display = 'block';
    };
  })());

  $scope.$on('setDiagnosticAreaDetailPopupInfo', function(event, data) {
    var scope = angular.element($('#diagnosticAreaPopupWrap')).scope();
    scope.DIAG_AREA_NM = commonUtil.isEmpty(data) ? undefined : data.DIAG_AREA_NM;
    scope.WRTR_NM = commonUtil.isEmpty(data) ? $scope.loginNm : data.WRTR_NM;
    scope.WRT_YMD = commonUtil.isEmpty(data) ? new Date().toISOString().substring(0, 10) : data.WRT_YMD;
  });

  $scope.diagnosticAreaDetailPopup = {
    grid: {
      'id': 'diagnosticAreaList3',
      'page': 1,
      'size': GridConfig.sizeSS,
      'selRowData': {},
      'url': { 'new': '/gis/getDiagnosticAreaDetailNewList.json', 'edit': '/gis/getDiagnosticAreaDetailEditList.json' },
      setGrid: function() {
        $('#diagnosticAreaList3').jqGrid('GridUnload');
        nonPagerGrid({
          grid_id: this.id,
          rownumbers:true,          
          height: 150,
          colNames: [
            'ASSET_SID',
            'DIAG_AREA_SID',
            '순번',
            '자산코드',
            '자산명',
            '레벨1',
            '레벨2',
            '레벨3',
            '레벨4',
            '레벨5',
            '레벨6',
            '레벨7',
            '레벨8',
            '레벨9',
            '레벨10',
            '레벨11',
            '레벨12',
            '레벨13',
            '레벨14',
            '레벨15',
            'PATH_CD',
            '분석정보',
            '관재질',
            '관경'
          ],
          colModel: [
            { name: 'ASSET_SID', hidden: true },
            { name: 'DIAG_AREA_SID', hidden: true },
            { name: 'RNUM', width: 40 ,hidden: true },
            { name: 'ASSET_CD', width: 120 },
            { name: 'ASSET_NM', width: 120 },
            { name: 'CLASS1_NM', width: 40 },
            { name: 'CLASS2_NM', width: 40 },
            { name: 'CLASS3_NM', width: 40 },
            { name: 'CLASS4_NM', width: 40 },
            { name: 'CLASS5_NM', width: 40 },
            { name: 'CLASS6_NM', width: 40 },
            { name: 'CLASS7_NM', width: 40 },
            { name: 'CLASS8_NM', width: 40 },
            { name: 'CLASS9_NM', width: 40 },
            { name: 'CLASS10_NM', width: 40, hidden: true },
            { name: 'CLASS11_NM', width: 40, hidden: true },
            { name: 'CLASS12_NM', width: 40, hidden: true },
            { name: 'CLASS13_NM', width: 40, hidden: true },
            { name: 'CLASS14_NM', width: 40, hidden: true },
            { name: 'CLASS15_NM', width: 40, hidden: true },
            { name: 'PATH_CD', width: 40, hidden: true },
            { name: '분석정보', width: 110 },
            { name: '관재질', width: 110 },
            { name: '관경', width: 80 }
          ],
          onSelectRow: function(rowid) {
            $scope.diagnosticAreaDetailPopup.grid.selRowData = $(this).jqGrid('getRowData', rowid);
          },
          gridComplete: function() {
            var $this = $(this);
            var ids = $this.jqGrid('getDataIDs');
            $this.setSelection(ids[0]);
            $scope.diagnosticAreaDetailPopup.grid.page = $this.getGridParam('page');
            // $scope.diagnosticAreaDetailPopup.grid.count = $this.getGridParam('records');
            $("#jqgh_diagnosticAreaList3_rn").text("순번");
          }
        });
      },
      getUrl: function() {
        var url = $scope.diagnosticAreaDetailPopup.grid.url;

        return DIAG_AREA_SID > 0 ? url.edit : url.new;
      }
    }, search: function(url) {
      var _url = url === undefined ? this.grid.url.new : url;
      var $grid = $('#' + this.grid.id);
      $grid.jqGrid('clearGridData');

      if (_url === this.grid.url.edit) {
        mainDataService.getDiagnosticAreaDetailEditList({ 'diag_area_sid': DIAG_AREA_SID }).success(function(data) {
          $grid.jqGrid('addRowData', 1, data.rows, 'last');
        });
      }
    }, save: function() {
      var scope = angular.element($('#diagnosticAreaPopupWrap')).scope();

      if (commonUtil.isEmpty(scope.DIAG_AREA_NM)) {
        alertify.alert();
        return;
      }

      var rowData = $('#' + $scope.diagnosticAreaDetailPopup.grid.id).jqGrid('getRowData');

      // 중복제거
      var source = rowData.filter(function(obj1, index1, array1) {
        return array1.findIndex(function(obj2) {
          return obj2.ASSET_SID === obj1.ASSET_SID;
        }) === index1;
      }).map(function(obj) {
        return obj.ASSET_SID;
      });

      var param = Object.entries({
        'diag_area_sid': DIAG_AREA_SID,
        'diag_area_nm': scope.DIAG_AREA_NM,
        'wrt_ymd': commonUtil.isEmpty(scope.WRT_YMD) ? null : scope.WRT_YMD.replace(/\D/g, ''),
        'item_list': source
      }).map(function(e) {
        return e.join('=');
      }).join('&');

      mainDataService.saveDiagnosticAreaDetail(param).success(function(data) {
        if (data > 0) {
          alertify.success('저장되었습니다.');
        } else {
          console.log(data);
          alertify.error('오류');
        }
        $scope.diagnosticAreaPopup.search();
        document.getElementById('diagnosticAreaDetailPopup').style.display = 'none';
      });
    }, delete: function() {
      var grid = this.grid;
      var selRowData = grid.selRowData;
      var diagAreaSid = commonUtil.isEmpty(selRowData) ? undefined : selRowData.DIAG_AREA_SID;
      var assetSid = commonUtil.isEmpty(selRowData) ? undefined : selRowData.ASSET_SID;

      if (commonUtil.isEmpty(selRowData)) {
        alertify.alert('항목을 선택해주세요.');
        return;
      }

      var $grid = $('#' + grid.id);
      var sel = $grid.jqGrid('getGridParam', 'selrow');
      $grid.jqGrid('delRowData', sel);
      /*
      if (commonUtil.isNotEmpty(diagAreaSid) && commonUtil.isNotEmpty(assetSid)) {
        alertify.confirm('삭제 확인', '정말 삭제하시겠습니까?', function() {
          var param = { 'diag_area_sid': diagAreaSid, 'asset_sid': assetSid };

          mainDataService.deleteDiagnosticAreaAsset(param).success(function(data) {
            if (data > 0) {
              alertify.success('삭제되었습니다.');
              $scope.diagnosticAreaPopup.search();
              $scope.diagnosticAreaDetailPopup.search($scope.diagnosticAreaDetailPopup.grid.url.edit);
            } else {
              alertify.success('지정된 테이블에서 삭제할 수 없습니다.');
            }
          });
        }, function() {
        }); // alertify.confirm에서 실패 시 동작하는 함수를 선언해줘야 정상 작동함
      } else {
        var $grid = $('#' + grid.id);
        var sel = $grid.jqGrid('getGridParam', 'selrow');

        $grid.jqGrid('delRowData', sel);
      }
      */
    }
  };

  function showStateDiagnosticGroupDetailNewPopup() {
    showStateDiagnosticGroupDetailPopup(0);
  }

  function showStateDiagnosticGroupDetailEditPopup() {
    var selRowData = $scope.stateDiagnosticGroupPopup.grid.selRowData;

    if (commonUtil.isEmptyObject(selRowData)) {
      return;
    }

    showStateDiagnosticGroupDetailPopup(selRowData.STATE_DIAG_GROUP_SID, selRowData);
  }

  function showStateDiagnosticGroupDetailPopup(stateDiagGroupSid, selRowData) {
    $scope.$broadcast('setStateDiagnosticGroupDetailPopupInfo', { 'stateDiagGroupSid': stateDiagGroupSid, 'selRowData': selRowData });
    $scope.$broadcast('showStateDiagnosticGroupDetailPopup');
  }

  $scope.$on('showStateDiagnosticGroupDetailPopup', (function() {
    var initialized = false;

    return function(event, data) {
      if (initialized) {
        var url = $scope.stateDiagnosticGroupDetailPopup.grid.getUrl();
        $scope.stateDiagnosticGroupDetailPopup.search(url);
      } else {
        $scope.stateDiagnosticGroupDetailPopup.grid.setGrid();
        //initialized = true;
      }

      document.getElementById('stateDiagnosticGroupDetailPopup').style.display = 'block';
    };
  })());

  $scope.$on('setStateDiagnosticGroupDetailPopupInfo', function(event, data) {
    $scope.STATE_DIAG_GROUP_SID = data.stateDiagGroupSid;
    if (data.stateDiagGroupSid > 0) {
      var param = { 'state_diag_group_sid': data.selRowData.STATE_DIAG_GROUP_SID };

      mainDataService.getStateDiagnosticGroupDetailPopupInfo(param).success(function(data1) {
        $scope.STATE_DIAG_GROUP_NM = data1.STATE_DIAG_GROUP_NM;
        $scope.WRTR_NM = data1.WRTR_NM;
        $scope.WRT_YMD = data1.WRT_YMD;
        $scope.assetPathCd = data1.ASSET_PATH_CD;
        $scope.assetPathSidNm = data1.ASSET_PATH_NM;
        $scope.pipeMaterialCd = data1.PIPE_MATERIAL_CD;
        $scope.pipeMaterialNm = data1.PIPE_MATERIAL_NM;
        $scope.pipeDiameterCd = data1.PIPE_DIAMETER_CD;
        $scope.pipeDiameterNm = data1.PIPE_DIAMETER_NM;
        $scope.pipePurposeCd = data1.PIPE_PURPOSE_CD;
        $scope.pipePurposeNm = data1.PIPE_PURPOSE_NM;
        $scope.waterPurposeCd = data1.WATER_PURPOSE_CD;
        $scope.waterPurposeNm = data1.WATER_PURPOSE_NM;
      });
    } else {
      $scope.STATE_DIAG_GROUP_NM = undefined;
      $scope.WRTR_NM = $scope.loginNm;
      $scope.WRT_YMD = new Date().toISOString().substring(0, 10);
      $scope.assetPathCd = undefined;
      $scope.assetPathSidNm = undefined;
      $scope.pipeMaterialCd = undefined;
      $scope.pipeMaterialNm = undefined;
      $scope.pipeDiameterCd = undefined;
      $scope.pipeDiameterNm = undefined;
      $scope.pipePurposeCd = undefined;
      $scope.pipePurposeNm = undefined;
      $scope.waterPurposeCd = undefined;
      $scope.waterPurposeNm = undefined;
    }

  });

  $scope.stateDiagnosticGroupDetailPopup = {
    grid: {
      'id': 'diagnosticAreaList4',
      'page': 1,
      'size': GridConfig.sizeSS,
      'selRowData': {},
      'url': { 'new': '/gis/getStateDiagnosisGroupDetailNewList.json', 'edit': '/gis/getStateDiagnosisGroupDetailEditList.json' },
      setGrid: function() {
        $('#diagnosticAreaList4').jqGrid('GridUnload');
        return pagerJsonGrid({
          grid_id: this.id, pager_id: 'diagnosticAreaListPager4', url: this.getUrl(), condition: {
            page: this.page, rows: this.size, state_diag_group_sid: $scope.STATE_DIAG_GROUP_SID
          }, rowNum: this.size, colNames: [
            'ASSET_SID',
            'STATE_DIAG_GROUP_SID',
            '순번',
            '자산코드',
            '자산명',
            '레벨1',
            '레벨2',
            '레벨3',
            '레벨4',
            '레벨5',
            '레벨6',
            '레벨7',
            '레벨8',
            '레벨9',
            '레벨10',
            '레벨11',
            '레벨12',
            '레벨13',
            '레벨14',
            '레벨15',
            '분석정보',
            '관재질',
            '관경'
          ], colModel: [
            { name: 'ASSET_SID', hidden: true },
            { name: 'STATE_DIAG_GROUP_SID', hidden: true },
            { name: 'RNUM', width: 40 },
            { name: 'ASSET_CD', width: 120 },
            { name: 'ASSET_NM', width: 120 },
            { name: 'CLASS1_NM', width: 40 },
            { name: 'CLASS2_NM', width: 40 },
            { name: 'CLASS3_NM', width: 40 },
            { name: 'CLASS4_NM', width: 40 },
            { name: 'CLASS5_NM', width: 40 },
            { name: 'CLASS6_NM', width: 40 },
            { name: 'CLASS7_NM', width: 40 },
            { name: 'CLASS8_NM', width: 40 },
            { name: 'CLASS9_NM', width: 40 },
            { name: 'CLASS10_NM', width: 40, hidden: true },
            { name: 'CLASS11_NM', width: 40, hidden: true },
            { name: 'CLASS12_NM', width: 40, hidden: true },
            { name: 'CLASS13_NM', width: 40, hidden: true },
            { name: 'CLASS14_NM', width: 40, hidden: true },
            { name: 'CLASS15_NM', width: 40, hidden: true },
            { name: '분석정보', width: 110 },
            { name: '관재질', width: 110 },
            { name: '관경', width: 80 }
          ], onSelectRow: function(rowid) {
            $scope.stateDiagnosticGroupDetailPopup.grid.selRowData = $(this).jqGrid('getRowData', rowid);
          }, gridComplete: function() {
            var $this = $(this);
            var ids = $this.jqGrid('getDataIDs');
            $this.setSelection(ids[0]);
            $scope.stateDiagnosticGroupDetailPopup.grid.page = $this.getGridParam('page');
            // $scope.stateDiagnosticGroupDetailPopup.grid.count = $this.getGridParam('records');
            $scope.$apply();
          }
        });
      },
      getUrl: function() {
        var url = $scope.stateDiagnosticGroupDetailPopup.grid.url;

        return $scope.STATE_DIAG_GROUP_SID > 0 ? url.edit : url.new;
      }
    }, search: function(url) {
      var _url;
      var postData;

      if (url === undefined || url === this.grid.url.new) {
        _url = this.grid.url.new;
        postData = {
          //'asset_path_sid': $scope.assetPathSidCd,
          'asset_path_cd': $scope.assetPathCd,
          'pipe_material': $scope.pipeMaterialCd,
          'pipe_diameter': $scope.pipeDiameterCd,
          'pipe_purpose': $scope.pipePurposeCd,
          'water_purpose': $scope.waterPurposeCd
        };
      } else {
        _url = url;
        postData = { 'state_diag_group_sid': $scope.STATE_DIAG_GROUP_SID };
      }

      $('#' + this.grid.id).jqGrid('setGridParam', {
        url: _url,
        page: 1,
        postData: postData
      }).trigger('reloadGrid');
    }, save: function() {
      var scope = angular.element($('#diagnosticAreaPopupWrap')).scope();

      if (commonUtil.isEmpty(scope.STATE_DIAG_GROUP_NM)) {
        alertify.alert();
        return;
      }

      var param = Object.entries({
        'state_diag_group_sid': scope.STATE_DIAG_GROUP_SID,
        'state_diag_group_nm': scope.STATE_DIAG_GROUP_NM,
        'wrt_ymd': commonUtil.isEmpty(scope.WRT_YMD) ? null : scope.WRT_YMD.replace(/\D/g, ''),
        //'asset_path_sid': scope.assetPathSidCd,
        'asset_path_cd': scope.assetPathCd,
        'pipe_material': scope.pipeMaterialCd,
        'pipe_diameter': scope.pipeDiameterCd,
        'pipe_purpose': scope.pipePurposeCd,
        'water_purpose': scope.waterPurposeCd
      }).map(function(e) {
        return e.join('=');
      }).join('&');

      mainDataService.saveStateDiagnosisGroup(param).success(function(data) {
        if (data > 0) {
          alertify.success('저장되었습니다.');
          $scope.stateDiagnosticGroupPopup.search();
          document.getElementById('stateDiagnosticGroupDetailPopup').style.display = 'none';
        } else {
          console.log(data);
          alertify.success('오류');
        }
      });
    }
  };

  // 상태진단 그룹 신규/수정
  $scope.pop4 = {
    items: [], ae3171ab: null, open: function(value) {
      if (commonUtil.isEmpty(value)) {
        return;
      }

      $scope.ae3171ab = value;
      var gcode;
      var title;

      switch (value) {
        case 'pipeMaterial':
          gcode = 'MOP';
          title = '관재질';
          break;
        case 'pipeDiameter':
          gcode = 'CN21';
          title = '관경';
          break;
        case 'pipePurpose':
          gcode = 'SAA';
          title = '관용도';
          break;
        case 'waterPurpose':
          gcode = 'FWP';
          title = '급수용도';
          break;
      }

      if (commonUtil.isEmpty(gcode)) {
        console.info('gcode 없음. search() 파라미터 확인!');
        return false;
      }

      this.titleChange(title);
      this.search(gcode);
      this.toggleDisplay();
    }, titleChange: function(text) {
      document.getElementById('burkinaFaso').innerText = text || '';
    }, toggleDisplay: function() {
      var element = document.getElementById('diagnosisGroupDetail2Popup');
      element.style.display = element.style.display === 'none' ? 'block' : 'none';
    }, search: function(gcode) {
      mainDataService.getCommonCodeList({ 'gcode': gcode }).success(function(data) {
        $scope.$broadcast('setDiagnosisGroupDetail2PopupList', data);
      });
    }, save: function() {
      var element = document.querySelectorAll('input[name=Monaville]:checked'); // TODO: attribute 변경
      var array = [];

      element.forEach(function(node) {
        array.push({
          'C_NAME': node.dataset.name, 'C_SCODE': node.value
        });
      });

      $scope.$emit('Kazakhstan', array);

      this.toggleDisplay();
    }
  };

  $scope.$on('setDiagnosisGroupDetail2PopupList', function(event, data) {
    $scope.pop4.items = data;
  });

  // TODO: 선택한거 ->
  $scope.$on('Kazakhstan', function(event, data) {
    var scodeArray = [];
    var nameArray = [];

    for (var i = 0; i < data.length; i++) {
      scodeArray[i] = data[i].C_SCODE;
      nameArray[i] = data[i].C_NAME;
    }

    $scope[$scope.ae3171ab + 'Cd'] = '\'' + scodeArray.join('\',\'') + '\'';
    $scope[$scope.ae3171ab + 'Nm'] = nameArray.join('/');
    $scope.ae3171ab = null;

    $scope.stateDiagnosticGroupDetailPopup.search();
  });

  $scope.closePopup = function($event) {
    var element = $event.currentTarget;
    closePopup(element);
  };

  function removeAssetPathPopupStyleAttribute() {
    var element = document.getElementById('dialog-assetlevelpath');
    element.style.removeProperty('position');
    element.style.removeProperty('z-index');
  }

  $scope.showPopupLevelPath = function() {
    // 최상위로 이동
    // XXX: 닫기 버튼 또는 취소 버튼으로 닫았을 때 핸들링 불가
    addAssetPathPopupStyleAttribute();

    $rootScope.$broadcast('loadAssetLevelPathList', {
      callbackId: 'stateDiagnosticGroupAssetItem_selectAssetPath', namefilter: '', returnType: 'path'
    });
  };

  $scope.showPopupLevelPath1 = function() {
    $rootScope.$broadcast('showGisPopup', {
      init: function() {
        document.getElementById('gisPopup').querySelectorAll('#selectTarget > option').forEach(function(element) {
          if (element.innerText.indexOf('관로') < 0) {
            element.disabled = true;
          }
        });
      },
      callbackId: 'stateDiagnosticGroupAssetItem_selectAssetPath1'
    });
  };

  $scope.$on('showAssetPathPopup', function(event, data) {
    // 최상위로 이동
    // XXX: 닫기 버튼 또는 취소 버튼으로 닫았을 때 핸들링 불가
    addAssetPathPopupStyleAttribute();

    $rootScope.$broadcast('loadAssetLevelPathList', { callbackId: 'GIS_selectAssetPath1', namefilter: '', returnType: 'path' });
  });

  $scope.$on('confirmSelectionOfDiagnosticGroup', function(event, data) {
    var rowData = data.rowData;
    var param = { state_diag_group_sid: rowData.STATE_DIAG_GROUP_SID };
    param = Object.entries(param).map(function(e) {
      return e.join('=');
    }).join('&');

    document.getElementById('diagnosticAreaDetailPopup').style.display = 'block';

    mainDataService.getStateDiagnosisGroupDetailEditList(param).success(function(data1) {
      //$('#' + $scope.diagnosticAreaDetailPopup.grid.id)[0].addJSONData(data1.rows);
    	var rownum = $('#' + $scope.diagnosticAreaDetailPopup.grid.id).getGridParam("reccount"); 
    	$.each(data1.rows,function(idx1,Item){
    		$('#' + $scope.diagnosticAreaDetailPopup.grid.id).jqGrid('addRowData', rownum + idx1, Item, 'last');	
    	});
    	
    });
  });

  $scope.selectDiagnosticGroup = function() {
    document.getElementById('diagnosticAreaDetailPopup').style.display = 'none';

    $rootScope.$broadcast('setDiagnosticGroupSelectionButtonFlag', { 'flag': true });

    $timeout(function() {
      $('#BtnDiagnosticAreaTab2').trigger('click');
    }, 0);
  };

  $scope.$on('setDiagnosticGroupSelectionButtonFlag', function(event, data) {
    $scope.diagnosticGroupSelectionButtonFlag = data.flag;
  });

  $scope.$watch('diagnosticGroupSelectionButtonFlag', function(newValue, oldValue) {
    if (newValue && !oldValue) {
   	if(document.getElementById('BtnDiagnosticAreaTab1'))
    	document.getElementById('BtnDiagnosticAreaTab1').setAttribute('disabled', 'disabled');
   	if(document.getElementById('BtnDiagnosticAreaTab2')){
  		document.getElementById('BtnDiagnosticAreaTab2').setAttribute('disabled', 'disabled');
   		document.getElementById('BtnDiagnosticAreaTab2').removeAttribute('disabled');   		
   	}
  	if(document.getElementById('BtnDeleteStateDiagnosticGroupPopup'))   		
      document.getElementById('BtnDeleteStateDiagnosticGroupPopup').setAttribute('disabled', 'disabled');
  	if(document.getElementById('ross2'))
      document.getElementById('ross2').setAttribute('disabled', 'disabled');
    if(document.getElementById('subsidiaries3'))
      document.getElementById('subsidiaries3').setAttribute('disabled', 'disabled');
    if(document.getElementById('farm7326981'))
      document.getElementById('farm7326981').setAttribute('disabled', 'disabled');
    } else {
   	if(document.getElementById('BtnDiagnosticAreaTab1'))
      document.getElementById('BtnDiagnosticAreaTab1').removeAttribute('disabled');
   	if(document.getElementById('BtnDiagnosticAreaTab2'))   	
      document.getElementById('BtnDiagnosticAreaTab2').removeAttribute('disabled');
   	if(document.getElementById('BtnDeleteStateDiagnosticGroupPopup'))   	
      document.getElementById('BtnDeleteStateDiagnosticGroupPopup').removeAttribute('disabled');
   	if(document.getElementById('ross2'))
      document.getElementById('ross2').removeAttribute('disabled');
   	if(document.getElementById('subsidiaries3'))
      document.getElementById('subsidiaries3').removeAttribute('disabled');
   	if(document.getElementById('farm7326981'))
      document.getElementById('farm7326981').removeAttribute('disabled');
    }
  });

  $scope.$on('GIS_selectAssetPath1', function(event, data) {
    document.getElementById('gisFrame').contentWindow.showGISInfoByAssetPath(data);
  });

  $scope.$on('stateDiagnosticGroupAssetItem_selectAssetPath', function(event, data) {
    removeAssetPathPopupStyleAttribute();

    $scope.assetPathSidNm = data.LEVEL_PATH_NM;

    var param = { 'item_list': [{ ASSET_PATH_SID: data.ASSET_PATH_SID, LEVEL_PATH_CD: data.LEVEL_PATH_CD, LEVEL_PATH_NM: data.LEVEL_PATH_NM }] };
    $scope.assetPathCd = data.LEVEL_PATH_CD;

    //    mainDataService.getAssetsByAssetPath(param).success(function(data1) {
    //$scope.assetPathSidCd = data1[0].ASSET_PATH_SID;
    $scope.stateDiagnosticGroupDetailPopup.search($scope.stateDiagnosticGroupDetailPopup.grid.url.new);
    //});
  });

  $scope.$on('stateDiagnosticGroupAssetItem_selectAssetPath1', function(event, data) {
    var itemList = data.data;

    if (!Array.isArray(itemList) || itemList.length === 0) {
      return;
    }

    mainDataService.getAssetsByGISProperty({ 'item_list': itemList }).success(function(data1) {
      var $grid = $('#' + $scope.diagnosticAreaDetailPopup.grid.id);
      var rowId = $grid.getGridParam('records');
      var rowData = $grid.jqGrid('getRowData');
      var newArray = data1.filter(function(obj, index, array) {
        return rowData.findIndex(function(element) {
          return Number(element.ASSET_SID) === Number(obj.ASSET_SID);
        }) < 0;
      }).map(function(obj, index) {
        obj.RNUM = rowId + index + 1;
        return obj;
      });

      $grid.jqGrid('addRowData', rowId + 1, newArray, 'last');
    });
  });

  $scope.indirectConditionEvaluationPopup = {
    startDt: '',
    endDt: '',
    grid: {
      id: 'indirectConditionEvaluationGrid',
      page: 1,
      size: GridConfig.sizeT,
      selRowData: {},
      setGrid: function() {
        $('#indirectConditionEvaluationGrid').jqGrid('GridUnload');
        return pagerJsonGrid({
          grid_id: this.id,
          pager_id: 'indirectConditionEvaluationGridPager',
          url: '/est/getList.json',
          condition: {
            page: 1,
            rows: this.size,
            category: '1'
          }, rowNum: this.size,
          colNames: ['순번', 'EST_SID', '평가명', '평가일', '작성자', '작성일'],
          colModel: [
            { name: 'RNUM', width: 53 },
            { name: 'EST_SID', width: 0, hidden: true },
            { name: 'EST_NM', width: 0 },
            { name: 'EST_START_DT', width: 0 },
            { name: 'INSERT_ID', width: 0 },
            { name: 'INSERT_DT', width: 0 }
          ],
          gridComplete: function() {

          },
          onSelectRow: function(rowid) {
            $scope.indirectConditionEvaluationPopup.grid.selRowData = $(this).jqGrid('getRowData', rowid);
          }
        });
      }
    },
    search: function() {
      $('#' + this.grid.id).jqGrid('setGridParam', {
        postData: {
          'START_DT': $scope.indirectConditionEvaluationPopup.startDt,
          'END_DT': $scope.indirectConditionEvaluationPopup.endDt
        }
      }).trigger('reloadGrid');
    },
    confirm: function() {
      var selRowData = $scope.indirectConditionEvaluationPopup.grid.selRowData;

      viewEvaluationAnalysisResults('1', selRowData.EST_SID);
    }
  };

  $scope.directConditionEvaluationPopup = {
    startDt: '',
    endDt: '',
    grid: {
      id: 'directConditionEvaluationGrid',
      page: 1,
      size: GridConfig.sizeT,
      selRowData: {},
      setGrid: function() {
        $('#directConditionEvaluationGrid').jqGrid('GridUnload');
        return pagerJsonGrid({
          grid_id: this.id,
          pager_id: 'directConditionEvaluationGridPager',
          url: '/est/getList.json',
          condition: {
            page: 1,
            rows: this.size,
            category: '2'
          }, rowNum: this.size,
          colNames: ['순번', 'EST_SID', '평가명', '평가시작일', '평가종료일', '작성자', '작성일'],
          colModel: [
            { name: 'RNUM', width: 53 },
            { name: 'EST_SID', width: 0, hidden: true },
            { name: 'EST_NM', width: 0 },
            { name: 'EST_START_DT', width: 0 },
            { name: 'EST_END_DT', width: 0, hidden: true },
            { name: 'INSERT_ID', width: 0 },
            { name: 'INSERT_DT', width: 0 }
          ],
          gridComplete: function() {

          },
          onSelectRow: function(rowid) {
            $scope.directConditionEvaluationPopup.grid.selRowData = $(this).jqGrid('getRowData', rowid);
          }
        });
      }
    },
    search: function() {
      $('#' + this.grid.id).jqGrid('setGridParam', {
        postData: {
          'START_DT': $scope.directConditionEvaluationPopup.startDt.replace(/-/gi, ''),
          'END_DT': $scope.directConditionEvaluationPopup.endDt.replace(/-/gi, '')
        }
      }).trigger('reloadGrid');
    },
    confirm: function() {
      var selRowData = $scope.directConditionEvaluationPopup.grid.selRowData;

      viewEvaluationAnalysisResults('2', selRowData.EST_SID);
    }
  };

  $scope.$on('evaluationAnalysisSettings', function(event, target) {
    var popupObj;

    if (target === 'indirectConditionEvaluation') {
      popupObj = $scope.indirectConditionEvaluationPopup;
    } else if (target === 'directConditionEvaluation') {
      popupObj = $scope.directConditionEvaluationPopup;
    }

    variableInitialization(popupObj);
    setDatePicker();
    setGrid(popupObj);
    showPopup(target);

    function variableInitialization(popupObj) {
      $scope.$apply(function() {
        popupObj.startDt = '';
        popupObj.endDt = '';
      });
    }

    function setGrid(popupObj) {
      popupObj.grid.setGrid();
    }

    function showPopup(target) {
      var popupId;

      if (target === 'indirectConditionEvaluation') {
        popupId = 'indirectConditionEvaluationPopup';
      } else if (target === 'directConditionEvaluation') {
        popupId = 'directConditionEvaluationPopup';
      }

      $('#diagnosticAreaPopupWrap').find('#' + popupId).show();
    }
  });

  function viewEvaluationAnalysisResults(category, estSid) {
    var param = { 'category': category, 'est_sid': estSid };

    mainDataService.getGISInformationAndConditionAssessmentResults(param).success(function(data) {
      // console.log(data);
      var ftrIdnStringWithComma = data.map(function(obj) {
        return obj.FTR_IDN;
      }).join();
      var layerNameStringWithComma = [
        ...new Set(data.map(function(obj) {
          return obj.LAYER_NM;
        }))
      ].join();
      // console.log(ftrIdnStringWithComma);
      // console.log(layerNameStringWithComma);
      $rootScope.$broadcast('showGisPopup', {
        init: function() {
          var defalutMap = [
            'rgb(  0,   0, 255)', // default
            'rgb(255,   0,   0)', //red
            'rgb(255, 255,   0)', //yellow
            'rgb(  0, 255,   0)', // green
            'rgb(  0,   0, 255)', // not defined
            'rgb(  0,   0, 255)' // not defined
          ];
          //간접평가
          var colorMap1 = [
            'rgb(  0,   0, 255)', // default
            'rgb(  0,   0, 255)', // blue  1
            'rgb(  0, 255,   0)', // green 2
            'rgb(255,   0,   0)', // red 3
            'rgb(  0,   0, 255)', // not defined
            'rgb(  0,   0, 255)' // not defined
          ];
          //직접평가
          var colorMap2 = [
            'rgb(  0,   0, 255)', // default
            'rgb(  0,   0, 255)', // blue  1
            'rgb(  0, 255, 255)', // skey blue  2
            'rgb(  0, 255,   0)', // green  3
            'rgb(255, 128,   0)', // orange
            'rgb(255,   0,   0)', //red
            'rgb(  0,   0, 255)', // not defined
            'rgb(  0,   0, 255)' // not defined
          ];
          var getMatchingColorForTheGrade = function(feature, data, opt) {
            var colorMap = defalutMap;
            if (typeof opt != 'undefined') {
              colorMap = (opt == '1') ? colorMap1 : colorMap2;
            }
            for (var i = 0; i < data.length; i++) {
              if (feature.get('ftr_idn') === data[i].FTR_IDN.toString()) {
                return commonUtil.isNotEmpty(data[i].STATE_GRADE) ? colorMap[Number(data[i].STATE_GRADE)] : colorMap[0]; // matched
              }
            }

            return colorMap[0]; // not matched
          };

          var style = function(color) {
            var fill = new ol.style.Fill({ 'color': color });
            var stroke = new ol.style.Stroke({
              color: color,
              width: 1.25
            });

            return new ol.style.Style({
              image: new ol.style.Circle({
                fill: fill,
                stroke: stroke,
                radius: 5
              }),
              fill: fill,
              stroke: stroke
            });
          };

          var vectorSource = new ol.source.Vector({
            format: new ol.format.WFS(),
            loader: function(extent) {
              $.ajax(WFS_URL, {
                type: 'POST',
                data: {
                  service: 'WFS',
                  version: '1.1.0',
                  request: 'GetFeature',
                  typeName: layerNameStringWithComma,
                  srsname: 'EPSG:3857',
                  cql_filter: 'ftr_idn in (' + ftrIdnStringWithComma + ')'
                }
              }).done(function(response) {
                var features = vectorSource.getFormat().readFeatures(response);
                features.forEach(function(fe){
                	fe.setStyle()
                });
                vectorSource.addFeatures(features);
                vectorSource.forEachFeature(function(feature) {
                  feature.setStyle(style(getMatchingColorForTheGrade(feature, data, param.category)));
                });
              });
            },
            projection: 'EPSG:3857',
            crossOrigin: 'Anonymous'
          });

          var vectorLayer = new ol.layer.Vector({
            source: vectorSource,
            style: ol.style.Style.defaultFunction
          });

          map.addLayer(vectorLayer);

          var select = new ol.interaction.Select({
            layers: [vectorLayer]
          });
          map.addInteraction(select);

          select.on('select', function(event) {
            var features = event.selected;

            if (features === undefined || features.length === 0) {
              return;
            }

            var feature = features[0];
            var properties = feature.getProperties();
            var param = {
              'ftr_idn': properties.ftr_idn, 'ftr_cde': properties.ftr_cde
            };
            getAssetInfoWithGisInfo(mainDataService, param).success(function(data) {
              parent.showAssetItemPP(data);
            });
          });
        }
        // callbackId: '',
      });
    });

    mainDataService.getAssetNonePage(param).success(function(data) {
      var interval = $interval(function() {
        var scope = angular.element($('#gisPopup')).scope();

        if (scope) {
          scope.evaluationResultsList = data;
          clearInterval();
        }
      }, 200, 10);

      var clearInterval = function() {
        if (angular.isDefined(interval)) {
          $interval.cancel(interval);
          interval = undefined;
        }
      };
    });
  }
}

