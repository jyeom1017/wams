angular.module('app.gis').controller('GisPopupController', gisPopupController);

function gisPopupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
  var initSelect1; // 자산정보 자산선택모드
  var initEdit1;
  var gisPopupMode;
  var ASSET_INFO;
  
  $scope.showAssetItemPP = function(data,event){
	  console.log(data);
	  event.stopPropagation();
  var param = {
          'ftr_idn': data.FTR_IDN, 'ftr_cde': data['\'10010\''], 'layer_cd': data.LAYER_CD
        };
        getAssetInfoWithGisInfo(mainDataService, param).success(function(data) {
        	console.log("getAssetInfoWithGisInfo");
        	console.log(data);
          parent.showAssetItemPP(data);
        });
        
  }
  
  $scope.gisPopupMode;
  //$scope.searchLongitude = 127.0;
  //$scope.searchLatitude = 36.0
  $scope.locationCancel = function(){
	  $("#getLocationConfirm").hide();
	  $("#display_LocationCoord").text('');
	  document.getElementById('removeMeasure').click();
	 // alert('최초위치값 삭제 ');
  }
  
  $scope.locationConfirm =function(){
	  
	    $("#getLocationConfirm").hide();
	    $("#display_LocationCoord").text('');
	    
	    $("#start_edit_mode").show();
	    $("#gotoLocationBtn").hide();
	    
	    $("#dispLocationInfo2").val('saved');
	    $("#dispLocationInfo2Span").hide();
	    
	   // alert('편집모드 활성화 + 최초위치 값 저장')
	    
	    var coordinate = [];
		  $.each(map.getLayers().getArray(),function(idx,Item){
	  		if(Item.values_.name=='Measure'){
	  			//console.log(Item);
	  			$.each(Item.values_.source.featuresRtree_.items_,function(idx2,Item2){
	  				console.log(Item2.value.values_.geometry.flatCoordinates);
	  				var l_coord = ol.proj.transform(Item2.value.values_.geometry.flatCoordinates, 'EPSG:3857', 'EPSG:4326');
	  				coordinate.push(l_coord[0]);
	  				coordinate.push(l_coord[1]);
	  			});
	  		}
		    });
		  	var geom_text = "";
			console.log('RegistAssetItem_ItemCoordinate');
	    	var coord_type='POINT';
	    	var coord_string = '';
	    	if(ASSET_INFO.LAYER.toUpperCase() == 'WTL_PIPE_LM' ){
	    		coord_type='MULTILINESTRING';
	    		if(coordinate.length<4){
	    			coordinate.push(coordinate[0]+0.001);
	    			coordinate.push(coordinate[1]+0.001);
	    		}
	    	}
	    	$.each(coordinate,function(idx,value){
	    		if(idx>0 && idx%2==0) coord_string +=",";
	    		coord_string += "" + value + " ";
	    	});
	    	if(coord_type == 'POINT' ){
	    		var list = coord_string.split(",");
	    		coord_string = coord_type + "(" + list[list.length-1] + ")";
	    	}else{
	    		
	    		coord_string = coord_type + "((" + coord_string + "))";	
	    	}
	    	geom_text = coord_string;
	    	console.log(ASSET_INFO);
		  	var param = {LAYER:ASSET_INFO.LAYER,FTR_IDN:ASSET_INFO.FTR_IDN,geom_text:geom_text}; 
		  	mainDataService.setPostgisGeoInfo(param)
		  	.success(function(data){
		  		showAsset(ASSET_INFO.LAYER, ASSET_INFO.FTR_IDN);
		  	});
		  
		  document.getElementById('removeMeasure').click();  
	    return;
	    
	    closeGisPopup();
	  
	  /*
	  var long = ($("#searchLongitude").val()!="") ? parseFloat($("#searchLongitude").val()) : 126.6961;
      var lat = ($("#searchLatitude").val()!="") ? parseFloat($("#searchLatitude").val()) : 35.6889;
      var coordinate = [long,lat];
      */
	  var coordinate = [];
	  $.each(map.getLayers().getArray(),function(idx,Item){
  		if(Item.values_.name=='Measure'){
  			//console.log(Item);
  			$.each(Item.values_.source.featuresRtree_.items_,function(idx2,Item2){
  				console.log(Item2.value.values_.geometry.flatCoordinates);
  				var l_coord = ol.proj.transform(Item2.value.values_.geometry.flatCoordinates, 'EPSG:3857', 'EPSG:4326');
  				coordinate.push(l_coord[0]);
  				coordinate.push(l_coord[1]);
  			});
  		}
	    });	  
	  $rootScope.$broadcast($scope.callbackId, { RESULT: 'ID_OK', data: {coordinate : coordinate } });
  }
  
  $scope.gotoLocation = function(){
	    map.getLayers().getArray()
	    .filter(layer => layer.get('name') === 'Marker')
	    .forEach(layer => map.removeLayer(layer));
	    /*
	    $.each(map.getLayers().getArray(),function(idx,Item){
    		if(Item.values_.name=='Measure'){
    			//console.log(Item);
    			$.each(Item.values_.source.featuresRtree_.items_,function(idx2,Item2){
    				console.log(Item2.value.values_.geometry.flatCoordinates);
    			});
    		}
	    });*/
	    
	    
        var long = ($("#searchLongitude").val()!="") ? parseFloat($("#searchLongitude").val()) : 126.6961;
        var lat = ($("#searchLatitude").val()!="") ? parseFloat($("#searchLatitude").val()) : 35.6889;

        map.getView().setCenter(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
        console.log(long,lat);
        //console.log(DEFAULT_CENTER_POINT);
        //console.log(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
        var pointFeature = new ol.Feature({
        	geometry: new ol.geom.Point(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'))
        });
        
        let featureSource = new ol.source.Vector({});
        
        featureSource.addFeature(pointFeature);
        
        let featureLayer = new ol.layer.Vector({
        	name: 'Marker',
        	source: featureSource 
        });
        
        map.addLayer(featureLayer);
	    
  }
  
  $scope.closePopupEdit1 = function(event){
	  console.log(event);
	  if(event.offsetX<170) return;	  
	  document.getElementById('close_edit_mode').click();
	  showAsset(ASSET_INFO.LAYER, ASSET_INFO.FTR_IDN);  
  }
  
  $scope.closePopupEdit = function(event){
	  console.log(event);
	  if(event.offsetX<670) return;	  
	  document.getElementById('close_edit_mode').click();
  }
  
  $scope.closePopupSelect = function(event){
	  console.log(event);
	  if(event.offsetX<152) return; 
	  document.getElementById('finish_select_mode').click();
  }  

  $(document).on('keyup', function(event) {
    var key = event.key || event.keyCode;

    // TODO: 블록분할, 편집모드 활성활일 때
    if (key === 'Backspace' || key === 8) {
      var dirty = initEdit1.DrawClass.dirty;
      var history = dirty.history;

      if (history.length > 0) {
        var last = history[history.length - 1];
        var lt = last.target;

        switch (last.action) {
          case 'add':
            editVectorLayer.getSource().removeFeature(lt);
            break;
          case 'update':
            var feature = editVectorLayer.getSource().getFeatureById(lt.getId());
            editVectorLayer.getSource().removeFeature(feature);
            editVectorLayer.getSource().addFeature(lt);
            break;
          case 'remove':
            editVectorLayer.getSource().addFeature(lt);
            break;
        }

        var array = [];
        switch (last.action) {
        	case 'add':
        		array = dirty.add;break;
        	case 'update':
        		array = dirty.update;break;
        	case 'remove':
        		array = dirty.ldelete;break;
        }
        for (var i = array.length - 1; i >= 0; i--) {
          if (array[i].getId() === lt.getId()) {
            array.splice(i, 1);
            break;
          }
        }
        history.pop();
      }
    }
  });
  function assetEditShow(flag,Mode){
	  setTimeout(function(){
		  if(flag){
			  $("#assetEdit").show();
			  $("#topToolbar").find(".btn_wrap").hide();
		  		$("#topToolbar").find(".input_info_1").hide();
		  		$("#topToolbar").find(".input_info_2").hide();			  
		  }
	  	  else{
	  		$("#assetEdit").hide();
	  		$("#topToolbar").find(".btn_wrap").show();
	  		$("#topToolbar").find(".input_info_1").show();
	  		$("#topToolbar").find(".input_info_2").show();
	  	  }
	  	  if(Mode=='gisView'){
	  		$("#topToolbar").hide();
	  	  }else{
	  		$("#topToolbar").show();  
	  	  }
	  },200);
  }
  $scope.$on('showGisPopup', function(event, data) {
    $scope.gisPopupPath = './view/gis/gis_popup.html?q=' + new Date().getMilliseconds();
    gisPopupMode = 'gisSelect';
    ASSET_INFO = {};
    gis_edit_selected_ftr_idn = '';
    //debugger;
    if(typeof data.gisPopupMode !='undefined'){
    	gisPopupMode = data.gisPopupMode;
    	ASSET_INFO = data.ASSET_INFO;
    	gis_edit_selected_ftr_idn = data.ASSET_INFO.FTR_IDN;
    	if(gisPopupMode=='gisEdit') assetEditShow(true,gisPopupMode);
    	else{
    		assetEditShow(false,gisPopupMode);
    	}
    }else{
    	assetEditShow(false,gisPopupMode);
    }
    $scope.gisPopupMode = gisPopupMode;
    
    if (commonUtil.isObject(data.init)) {
      $timeout(data.init, 1000);
    }

    if (commonUtil.isNotEmpty(data.callbackId)) {
      $scope.callbackId = data.callbackId;
    }
  });

  $scope.$on('$includeContentLoaded', function(event, url) {
    if (url.includes('./view/gis/gis_popup.html')) {
      $scope.initGis();
    }
  });
var errorPopupTimer = null;
  $scope.$on('showAssetItemPP', function(event, args) {
    if (typeof args.AssetItemInfo != 'undefined') {
      assetInfoPopupTimer = $timeout(function() {
        $rootScope.$broadcast('ShowAssetItemPopup', args);
        // GIS 팝업보다 위에 나오기 위함
        document.getElementById('dialog-AssetItemPopup').style.zIndex = '9991';
      }, 500);
      if(errorPopupTimer!=null)
      $timeout.cancel(errorPopupTimer);
    } else {
      errorPopupTimer = $timeout(function() {
        alertify.alert('오류', '등록된 자산이 없습니다.', function() {
          /*
          $rootScope.$broadcast('RegistAssetItemPopup',
              {asset_info:{},ItemList:{},callback_Id:'',callback_Fn : function(){
                alertify.success('자산등록');
              }});
              */
        });
      }, 500);
      $timeout.cancel(assetInfoPopupTimer);
    }
  });

  $scope.initGis = function() {
    initMap();
    initEdit1 = $scope.initEdit1 = initEdit();
    initSelect1 = $scope.initSelect1 = initSelect();
    initMeasure();
    initLayers();
    getLayers(mainDataService, $scope);
    $scope.clearSelectedInfoList();
    //$(".ol-overlaycontainer-stopevent").remove();
    $(".ol-zoom.ol-unselectable.ol-control").remove();
    $(".ol-rotate.ol-unselectable.ol-control.ol-hidden").remove();
    $(".ol-attribution.ol-unselectable.ol-control.ol-collapsed").remove();
    
  };

  $scope.gisPopupPath = undefined;
  $scope.checkParent = checkParent($scope);
  $scope.checkChild = checkChild($scope);

  // Deprecated
  $scope.popupClose = function() {
    closeGisPopup();
  };

  $scope.closePopup = function() {
    closeGisPopup();
  };

  $scope.selectConfirm = function() {
	if('gisSelect' != gisPopupMode) {
		$scope.clearSelectedInfoList();
		return;
	}
    //console.log(getSelectGisObj());
    document.getElementById('finish_select_mode').click();
    closeGisPopup();
    $rootScope.$broadcast($scope.callbackId, { RESULT: 'ID_OK', data: getSelectGisObj() });
  };

  $scope.selectedInfoList = [];
  $scope.moveToTheClickedAssetLocationAndBlink = function(item) {
	  $scope.selected_asset_sid = item.ASSET_SID;
    moveToTheClickedAssetLocationAndBlink(item)(initSelect1);
  };
  $scope.toggleTreeNode = toggleTreeNode($scope);
  $scope.toggleNavLayer = toggleNavLayer();
  $scope.reloadLayers = reloadLayers(mainDataService, $scope);
  $scope.checkAllLayers = checkAllLayers($scope);
  $scope.uncheckAllLayers = uncheckAllLayers($scope);
  $scope.showAssetPathPopup = function() {
    $scope.$broadcast('disableAllGISMenuFunctions');
    $scope.$broadcast('showAssetPathPopup');
  };
  $scope.assetInformation = function() {
    assetInformation(mainDataService, $scope)(initSelect1);
  };

  $scope.$on('ShowSelectedInfoList', ShowSelectedInfoList(mainDataService, $scope));
  $scope.$on('ClearSelectedInfoList', ClearSelectedInfoList($scope)); // Called from select.js
  // 취소버튼 클릭
  $scope.clearSelectedInfoList = function() {
	  $scope.selected_asset_sid = 0;
    clearSelectedInfoListBroadcast($scope)();
    featureBlink.clear();
    // 자산선택, 자산정보
    try {
      initSelect1.DrawSelector.unselect();
    } catch (e) {
      // console.log(e);
    }
    // 자산검색
    try {
      searchLayer.setSource(null);
      initSelect1.SearchSelector.destroy();
    } catch (e) {
      // console.log(e);
    }
  };

  $scope.searchAsset = function() {
    searchAsset(mainDataService, $scope)(initSelect1);
  };

  $scope.searchAddress = searchAddress();

  $scope.writeExcel = writeExcel($scope);

  //$scope.$on('addGisAsset', addGisAsset(mainDataService, $rootScope));
  //$scope.$on('removeGisAsset', deleteTemporaryAsset(mainDataService,$rootScope));

  $scope.$on('showAssetPathPopup', function(event, data) {
    // 최상위로 이동
    // XXX: 닫기 버튼 또는 취소 버튼으로 닫았을 때 핸들링 불가
    addAssetPathPopupStyleAttribute();

    $rootScope.$broadcast('loadAssetLevelPathList', { callbackId: 'GIS_selectAssetPath2', namefilter: '', returnType: 'path' });
  });

  $scope.$on('GIS_selectAssetPath2', function(event, data) {
    var param = data;

    $scope.clearSelectedInfoList();

    mainDataService.getGISInfoByAssetPath(param).then(function(response) {
      var responseData = response.data;

      if (Array.isArray(responseData) && responseData.length > 0) {
        var groupedLayerName = commonUtil.groupBy(responseData, 'C_MEMO');

        for (var layerName in groupedLayerName) {
          var tileLayerName = layerName.toLowerCase();
          var ftrIdnStringWithComma = groupedLayerName[layerName].map(function(obj) {
            return obj.FTR_IDN;
          }).join();
          var CQL = 'ftr_idn IN (' + ftrIdnStringWithComma + ')';

          // 화면에 표시
          searchLayer.setSource(getTileLayerCQL(tileLayerName, CQL));

          // 시설물 목록이 뜬다
          initSelect1.SearchSelector.enable();

          return $.ajax(WFS_URL, {
            type: 'POST', data: {
              service: 'WFS',
              version: '1.1.0',
              request: 'GetFeature',
              typeName: tileLayerName,
              srsname: 'EPSG:3857',
              outputFormat: 'application/json',
              CQL_FILTER: CQL
            }
          });
        }
      }
    }).then(function(response) {
      var features = angular.copy(response.features);
      var featuresLength = features.length;

      if (features.length > 0) {
        for (var i = 0; i < featuresLength; i++) {
          var feature = features[i];
          var validFeature = initSelect1.SearchSelector.el.source.getFeatureById(feature.id);
          //새로운 feature만 생성하여 준다.
          if (validFeature == null) {
            var features1 = new ol.format.GeoJSON().readFeatures(feature);
            initSelect1.SearchSelector.el.source.addFeatures(features1);
            initSelect1.SearchSelector.el.select.getFeatures().push(features1[0]);
          }
        }

        initSelect1.SearchSelector.el.select.dispatchEvent('select');
      }
    });
  });

  $scope.$on('disableAllGISMenuFunctions', function(event,data) {
	  
	  if(typeof data == 'undefined'){
		  //document.getElementById('removeMeasure').click(); //측정도구 취소
		  document.getElementById('cancelMeasure').click(); //측정도구 취소
	  }else{
		  //if(data.menuid != 'measure') document.getElementById('removeMeasure').click(); //측정도구 취소 
		  if(data.menuid != 'measure') document.getElementById('cancelMeasure').click(); //측정도구 취소
	  }
	  
    var phase = $scope.$root.$$phase;

    if (phase === '$apply' || phase === '$digest') {
      f();
    } else {
      $scope.$apply(function() {
        f();
      });
    }

    function f() {
      $scope.clearSelectedInfoList(); // 자산경로, 자산선택, 자산정보, 자산검색
      document.getElementById('finish_select_mode').click(); // 자산선택, 자산정보
      // $scope.elevationDifferenceAnalysisCancel(); // 단차분석
      // $scope.elevationDifferenceAnalysisClose(); // 단차분석
      // document.getElementById('btnCloseBlockDivision').click(); // 블록분할
      //document.getElementById('finish_edit_mode').click(); // 자산편집
      document.getElementById('close_edit_mode').click(); // 자산편집
      // document.getElementById('indirectConditionEvaluationPopup').style.display = 'none'; // 평가분석 > 간접상태평가
      // document.getElementById('directConditionEvaluationPopup').style.display = 'none'; // 평가분석 > 직접상태평가

      featureBlink.clear(); // 벡터 깜박임 제거
      
      //document.getElementById('removeMeasure').click(); //측정도구 취소
    }
  });
}

