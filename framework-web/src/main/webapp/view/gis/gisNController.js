angular.module('app.gis').controller('gisNController', gisNController);
angular.module('app.gis').controller('gisExample1Controller', gisExample1Controller);
angular.module('app.gis').controller('gisExample2Controller', gisExample2Controller);
angular.module('app.gis').controller('gisExample3Controller', gisExample3Controller);

function gisExample1Controller($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	$scope.$on('$viewContentLoaded', function() {
		getLayers(mainDataService, $scope);
	});

	$scope.checkParent = checkParent($scope);
	$scope.checkChild = checkChild($scope);
	$scope.toggleTreeNode = toggleTreeNode($scope);
	$scope.toggleNavLayer = toggleNavLayer();
}

function gisExample2Controller($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	$scope.$on('$viewContentLoaded', function() {
		getLayers(mainDataService, $scope);
	});
}

function gisExample3Controller($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	$scope.$on('$viewContentLoaded', function() {
		getLayers(mainDataService, $scope);
	});
}

function gisNController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	
	$scope.$on('$viewContentLoaded', function() {
		if($state.current.url!='/gis10'){
		    initMap();
		    initEdit();
		    initSelect();
		    initMeasure();
		    initLayers();
		    getLayers(mainDataService, $scope);			
		}else{
			initgis10();	
		}

	  });

	  $scope.showGisPopup = function() {
	    //$rootScope.$broadcast('showGisPopup',{callbackId:'gisController_testpopup'});
	    parent.showGisPP();
	  };

	  $scope.checkParent = checkParent($scope);
	  $scope.checkChild = checkChild($scope);

	  $scope.selectedInfoList = [];
	  $scope.toggleTreeNode = toggleTreeNode($scope);
	  $scope.toggleNavLayer = toggleNavLayer();
	  $scope.reloadLayers = reloadLayers(mainDataService, $scope);
	  $scope.checkAllLayers = checkAllLayers($scope);
	  $scope.uncheckAllLayers = uncheckAllLayers($scope);
	  $scope.$on('ShowSelectedInfoList', ShowSelectedInfoList(mainDataService, $scope));
	  $scope.$on('ClearSelectedInfoList', ClearSelectedInfoList($scope));

	  $scope.assetInformation = function() {
	    const selectTargetEl = document.getElementById('selectTarget');
	    const drawSelectorEl = document.getElementById('drawSelector');

	    selectTargetEl.selectedIndex = 1;
	    drawSelectorEl.selectedIndex = 1;
	    drawSelectorEl.disabled = true;

	    const initSelect1 = initSelect();
	    initSelect1.DrawSelector.enableSelector(drawSelectorEl.value);
	    initSelect1.enableAssetSelection();
	    const select = initSelect1.SingleSelector.el.select;
	    select.on('select', (e) => {
	      const features = e.target.getFeatures();

	      if (features.getLength() > 0) {
	        const feature = features.item(0);
	        const properties = feature.getProperties();

	        getLayerCode(mainDataService).success(function(data) {
	          const param = {
	            'ftr_idn': properties.ftr_idn, 'ftr_cde': properties.ftr_cde, 'layer_cd': data.C_SCODE,
	          };
	          getAssetInfoWithGisInfo(mainDataService, param).success(function(data) {
	            parent.showAssetItemPP(data);
	          });
	        });
	      }
	    });
	  };

	  $scope.showDiagnosticAreaPopup = function() {
	    parent.showDiagnosticAreaPopup();
	  };
	  
	  $scope.elevationDifferenceAnalysis = function(){
		  //alert('단차분석');
		  $(".dancha_mode").show();
		  $scope.elevationDifferenceAnalysisClose();
		  $scope.temp_pipe_list = [];
		  $scope.temp_start_pipe = "";		  
	  }
	  $scope.elevationDifferenceAnalysisCancel = function(){
		  $(".dancha_mode").hide();
		  $scope.temp_pipe_list = [];
		  $scope.temp_start_pipe = "";
	  }
	  $scope.elevationDifferenceAnalysisOpen = function(){
		  $(".dancha_mode").hide();
		  $(".dancha_mode2").show();
		  $scope.insertTempPipeList();
	  }
	  $scope.elevationDifferenceAnalysisClose = function(){
		  $(".dancha_mode2").hide();
	  }
	  
	  $scope.temp_start_pipe = "";
	  $scope.temp_pipe_list = [];
	  $scope.nodeList = [];
	  $scope.nodeInfo = [];
	  var checkNodeList = [];
	  
	  
	  function getPipeLength(ftrIdn){
		  var ln = 0;
		  $.each($scope.nodeInfo,function(idx,Item){
			 if(Item.gid1==ftrIdn){
				 ln = parseInt(Item.l1);
				 return false;
			 }
			 if(Item.gid2==ftrIdn){
				 ln = parseInt(Item.l2);
				 return false;
			 } 			 
		  });
		  return ln;
	  }
	  var maxPipeLength=0;
	  var maxPipeLengthPath="";
	  function getPipePathLength (path){
		  var pipeLength = 0;
		  var pathList = path.split(',');
		  $.each(pathList,function(idx,Item){
			  pipeLength += getPipeLength(Item);
		  });
		  console.log(path,pipeLength);
		  if(pipeLength>maxPipeLength){
			  maxPipeLength=pipeLength;
			  maxPipeLengthPath=path;
		  }
		  return pipeLength;
	  }
	  
	  function makeNodeList(idx,ftrIdn,opt,path){
		  getPipePathLength(path);
		  if(ftrIdn=='') return ftrIdn;
		  
		  if(idx==0){
			  checkList = [];
			  $scope.nodeList = [];
			  maxPipeLength=0;
			  maxPipeLengthPath="";
		  }else{
			  checkList = [];
			  $.each(path.split(','),function(idx1,item1){
				  if(item1!='')  checkList.push(parseInt(item1));
			  });
		  }
		  $scope.nodeList.push({idx:idx,ftrIdn:ftrIdn,opt:opt});
		  if(idx > $scope.temp_pipe_list.length) return;
		  var nextFtrIdn = '';
		  var dir = 0;
		  //$.each($scope.temp_pipe_list,function(idx,Item){
			  if($.inArray(ftrIdn,checkList) < 0 ){
				  checkList.push(ftrIdn);
				  var checkList1 = angular.copy(checkList);
				  var checkList2 = angular.copy(checkList);
				  console.log(ftrIdn);
				  var r1="";
				  var r2="";
				  $.each($scope.nodeInfo,function(idx2,Item2){
					  var r0 = "";
					  if(Item2.gid1==ftrIdn && parseInt(Item2.l1) > 5.0 && Item2.d1 < 0.001){
						  nextFtrIdn = Item2.gid2;
						  if($.inArray(nextFtrIdn,checkList1)<0){
						  dir = 1;
						  r0 = ftrIdn +',' + makeNodeList(idx+1,nextFtrIdn,dir,checkList1.toString());
						  checkList1.push(nextFtrIdn);
						  if(getPipePathLength(r0) > getPipePathLength(r1)) r1=r0;
						  //return false;
						  }
					  }
				  });
				  $.each($scope.nodeInfo,function(idx2,Item2){
					  if(Item2.gid2==ftrIdn && parseInt(Item2.l2) > 5.0 && Item2.d1 < 0.001){
						  nextFtrIdn = Item2.gid1
						  if($.inArray(nextFtrIdn,checkList2)<0){
						  dir = 2;
						  r0 = ftrIdn +',' + makeNodeList(idx+1,nextFtrIdn,dir,checkList2.toString());
						  checkList2.push(nextFtrIdn);
						  if(getPipePathLength(r0) > getPipePathLength(r2)) r2=r0;
						  //return false;
						  }
					  }
				  });
				  
				  if(r1!="" || r2!="")
					  return (getPipePathLength(r1) > getPipePathLength(r2))?r1:r2;
				  else
					  return ftrIdn;
			  }else{
				  /*
				  for(a in $scope.temp_pipe_list){
					  var Item = $scope.temp_pipe_list[a];
					  if($.inArray(Item.FTR_IDN,checkNodeList)>=0 && $.inArray(Item.FTR_IDN,checkList)<0){
						  checkList.push(Item.FTR_IDN);
						  return ftrIdn +',' + makeNodeList(idx+1,Item.FTR_IDN,dir,checkList.toString());
					  }
				  }*/
				  return ftrIdn;
			  }
		  //});
	  }
	  
	  function getDistance(p1,p2){
		  var p01 = p1.replace('POINT(','').replace(')','').split(' ');
		  var p02 = p2.replace('POINT(','').replace(')','').split(' ');
		  result = (parseFloat(p01[0]) - parseFloat(p02[0])) * (parseFloat(p01[0]) - parseFloat(p02[0])) + (parseFloat(p01[1]) - parseFloat(p02[1])) * (parseFloat(p01[1]) - parseFloat(p02[1])) ;
		  return Math.sqrt(result);
	  }
	  function checkDistance(p0,p1,p2){
		  //(getDistance(nodePointList[nodePointList.length-1].point,Item2.a2)<getDistance(nodePointList[nodePointList.length-1].point,Item2.a1))
		  return (getDistance(p0,p1)<getDistance(p0,p2))?p1:p2;
	  }
	  $scope.getTempPipeList = function(){
		  var param  = {};
		  mainDataService.getElevationDifferenceAnalysisInfo(param)
		  .success(function(data){
			  console.log(data);
			  $scope.nodeInfo = data.nodeInfo;
			  $scope.temp_pipe_list = data.list;
			  checkNodeList = [];
			  $.each($scope.nodeInfo,function(idx,Item){
				  if($.inArray(Item.gid1,checkNodeList)<0)  checkNodeList.push(Item.gid1);
				  if($.inArray(Item.gid2,checkNodeList)<0)  checkNodeList.push(Item.gid2);
			  });
			  //debugger;
			  $scope.nodeList = [];
			  var path = makeNodeList(0,parseInt($scope.temp_start_pipe),0,"");
			  console.log(path);
			  console.log($scope.nodeList);
			  //console.log(checkList);
			  //$scope.nodeInfo.size() + 2 개의포인트 좌표가 필요함 
			  //찾기
			  var nodePointList = [];
			  var checkPath = maxPipeLengthPath.split(',');
			  var list = [];
			  $.each(checkPath, function(idx, Item) {
				  $.each($scope.nodeList,function(idx2,Item2){
					  if(Item==Item2.ftrIdn && Item2.idx == list.length){
						  list.push(Item2);
						  return false;
					  }
				  });
				  
			  });
			  $scope.nodeList = list;
				  
			  	$.each($scope.nodeList, function(idx, Item) { // idx : 인덱스 숫자 / Item : 데이터
					
			  		/*$.each($scope.temp_pipe_list,function(idx1,Item1){
						if(Item.ftrIdn==Item1.FTR_IDN){
							return false;
						}
					});*/
					$.each($scope.nodeInfo,function(idx2,Item2){
						//debugger;
						if(idx < $scope.nodeList.length -1){
							if($scope.nodeList[idx+1].opt==1 &&  Item.ftrIdn==Item2.gid1 && $scope.nodeList[idx+1].ftrIdn==Item2.gid2 ){
								if(idx==0) nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point : null,a1:Item2.a1,a2:Item2.a2});
								if(getDistance(nodePointList[nodePointList.length-1].a1,Item2.b2)<=Item2.d1){
									if(idx==0)nodePointList[0].point = nodePointList[0].a2;
									nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point :Item2.b2, a1:Item2.b1,a2:Item2.b2});	
								}else if(getDistance(nodePointList[nodePointList.length-1].a2,Item2.b2)<=Item2.d1){
									if(idx==0)nodePointList[0].point = nodePointList[0].a1;
									nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point :Item2.b2, a1:Item2.b1,a2:Item2.b2});
								}else if(getDistance(nodePointList[nodePointList.length-1].a2,Item2.b1)<=Item2.d1){
									if(idx==0)nodePointList[0].point = nodePointList[0].a1;
									nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point : Item2.b1, a1:Item2.b1,a2:Item2.b2});								
								}else{
									if(idx==0)nodePointList[0].point = nodePointList[0].a2;
									nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point : Item2.b1, a1:Item2.b1,a2:Item2.b2});								
								}
								return false;
							}
							if($scope.nodeList[idx+1].opt==2 &&  Item.ftrIdn==Item2.gid2 && $scope.nodeList[idx+1].ftrIdn==Item2.gid1 ){
								if(idx==0) nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point : null, a1:Item2.b1,a2:Item2.b2});
								if(getDistance(nodePointList[nodePointList.length-1].a2,Item2.a2)<=Item2.d1){
									if(idx==0)nodePointList[0].point = nodePointList[0].a1;
									nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point : Item2.a2, a1:Item2.a1,a2:Item2.a2});
								}else if(getDistance(nodePointList[nodePointList.length-1].a1,Item2.a2)<=Item2.d1){
									if(idx==0)nodePointList[0].point = nodePointList[0].a2;
									nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point : Item2.a2, a1:Item2.a1,a2:Item2.a2});
								}else if(getDistance(nodePointList[nodePointList.length-1].a1,Item2.a1)<=Item2.d1){
									if(idx==0)nodePointList[0].point = nodePointList[0].a2;
									nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point : Item2.a1, a1:Item2.a1,a2:Item2.a2});								
								}else{
									if(idx==0)nodePointList[0].point = nodePointList[0].a1;
									nodePointList.push({ftrIdn : $scope.nodeList[idx].ftrIdn , point : Item2.a1, a1:Item2.a1,a2:Item2.a2});								
								}
								return false;
							}
							if($scope.nodeList[idx+1].opt==0 &&  Item.ftrIdn==Item2.gid1){
								if(idx>1)
								nodePointList.push({ftrIdn : Item.ftrIdn , point : Item2.a1,a1:Item2.a1,a2:Item2.a2});
								return false;
							}
						}else{
							//끝점 처리
							if(Item.ftrIdn==Item2.gid1 && $scope.nodeList[idx-1].ftrIdn == Item2.gid2){
								if(getDistance(nodePointList[nodePointList.length-1].point,Item2.a1)<=Item2.d1){
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a1,a1:Item2.a1,a2:Item2.a2});
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a2,a1:Item2.a1,a2:Item2.a2});
								}else{
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a2,a1:Item2.a1,a2:Item2.a2});
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a1,a1:Item2.a1,a2:Item2.a2});
								}
								return false;
							}else if(Item.ftrIdn==Item2.gid2 && $scope.nodeList[idx-1].ftrIdn == Item2.gid1){
								if(getDistance(nodePointList[nodePointList.length-1].point,Item2.b1)<=Item2.d1){
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b1,a1:Item2.b1,a2:Item2.b2});
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b2,a1:Item2.b1,a2:Item2.b2});
								}else{
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b2,a1:Item2.b1,a2:Item2.b2});
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b1,a1:Item2.b1,a2:Item2.b2});
								}
								return false;
							}
							/*else if(Item.ftrIdn==Item2.gid1 && $scope.nodeList[idx-1].ftrIdn == Item2.gid1){
								if(getDistance(nodePointList[nodePointList.length-1].point,Item2.a1)<=Item2.d1){
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a1,a1:Item2.a1,a2:Item2.a2});
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a2,a1:Item2.a1,a2:Item2.a2});
								}else{
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a2,a1:Item2.a1,a2:Item2.a2});
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.a1,a1:Item2.a1,a2:Item2.a2});
								}
								return false;
							}else if(Item.ftrIdn==Item2.gid2 && $scope.nodeList[idx-1].ftrIdn == Item2.gid2){
								if(getDistance(nodePointList[nodePointList.length-1].point,Item2.b2)<=Item2.d1){
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b2,a1:Item2.b1,a2:Item2.b2});
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b1,a1:Item2.b1,a2:Item2.b2});
								}else{
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b1,a1:Item2.b1,a2:Item2.b2});
									nodePointList.push({ftrIdn: Item.ftrIdn, point : Item2.b2,a1:Item2.b1,a2:Item2.b2});
								}
								return false;
							}*/
						}
					});	
				});
			  	console.log(nodePointList);
			  	$.each(nodePointList,function(idx,Item){
			  		var loc = Item.point.replace('POINT(','').replace(')','').split(' ');
			  		Item.lv = null;
			  		Item.lat = loc[1].substring(0,17);
			  		Item.lng = loc[0].substring(0,17);
			  	});
			  	
			  	var cnt = 0;
			  	$.each(nodePointList,function(idx,Item){
			  		var param = Item.point;
					mainDataService.getGoogleGeoElevationApi(param)
					.success(function(data){
						cnt ++;
						console.log(data);
						//data.results.elevation;
						$.each(nodePointList,function(idx2,Item2){
							//debugger;
							if(parseFloat(Item2.lng)==data.results[0].location.lng && parseFloat(Item2.lat)==data.results[0].location.lat && Item2.lv==null){
								Item2.lv = data.results[0].elevation;
								return false;
							}
						});
						if(cnt>=nodePointList.length){
							//alert(JSON.stringify(nodePointList));
							console.log(nodePointList);
							monthGraph(nodePointList);
						}
					});			  		
			  	});
			  console.log($scope.nodeInfo);
			  
		  });
	  }
	  
	  $scope.insertTempPipeList = function(){
		  if($scope.temp_pipe_list.length < 2){
			  alertify.alert('단차분석을 위해서는 2개이상의 관로를 선택하세요');
			return;  
		  }
		  var param = {ITEM_LIST:[]};
		  $.each($scope.temp_pipe_list,function(idx,Item){
			  param.ITEM_LIST.push(Item.FTR_IDN); 
		  });
		  //monthGraph();
		  //$scope.getTempPipeList();
		  
		  mainDataService.saveTempPipeList(param)
		  .success(function(data){
			 $scope.getTempPipeList();
			 //$scope.temp_pipe_list = [];
		  });
		  
	  }
	  
	  $scope.SelectStartPipe = function(){
		  //debugger;
		  
		  var checkTempPipeList = [];
		  $.each($scope.temp_pipe_list,function(idx,Item){
			  if($.inArray(Item.FTR_IDN,checkTempPipeList)){
				  checkTempPipeList.push(Item.FTR_IDN);
			  }
		  });
		  
		  $.each($scope.selectedInfoList,function(idx,Item){
			  $scope.temp_start_pipe = Item['\'10009\''];
			  if($.inArray($scope.temp_start_pipe,checkTempPipeList)){
				  $scope.temp_pipe_list.push({FTR_IDN : Item['\'10009\'']});  
			  }
			  return false;
		  });		  
	  }
	  
	  $scope.SelectLinkedPipe = function(){
		  var checkTempPipeList = [];
		  $.each($scope.temp_pipe_list,function(idx,Item){
			  if($.inArray(Item.FTR_IDN,checkTempPipeList)){
				  checkTempPipeList.push(Item.FTR_IDN);
			  }
		  });
		  
		  $.each($scope.selectedInfoList,function(idx,Item){
			  if($.inArray(Item['\'10009\''],checkTempPipeList)){
			  $scope.temp_pipe_list.push({FTR_IDN : Item['\'10009\'']});
			  }
		  });		  
	  }
	  
	  var monthGraph = function (nodePointList) {
			console.log(nodePointList);

			// var dateData = []; // 측정일시 데이터
			// var fluxData = []; // 유량 데이터
			// var waterPressureData = []; // 수압 데이터
			//debugger;
			var dataArray = [];

			if ($scope.nodeList != undefined && nodePointList.length > 0) {

				$.each(nodePointList, function(idx, Item) { // idx : 인덱스 숫자 / Item : 데이터

					var dataObj = {};
					dataObj.ftrIdn = Item.ftrIdn;
					dataObj.level = Item.lv.toFixed(1);
					$.each($scope.temp_pipe_list,function(idx1,Item1){
						if(Item.ftrIdn==Item1.FTR_IDN){
							dataObj.level = (Item.lv.toFixed(1) - parseFloat(Item1.DEP).toFixed(1)).toFixed(1);
							dataObj.depth = parseFloat(Item1.DEP).toFixed(1);
							return false;
						}
					});
					dataObj.level1 = (dataObj.level - dataObj.depth).toFixed(1);  
					dataArray.push(dataObj);
				});
			}
				
			console.log(dataArray);
			
			var chartData = dataArray; 


			rMateChartH5.create("chart1", "levelGraph", "", "100%", "100%");
			 
			// 스트링 형식으로 레이아웃 정의.
			var layoutStr =
			             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
			                   +'<Options>'
			                      +'<Caption text="단차분석"/>'
			                       +'<SubCaption text="단차분석" textAlign="center" />'
			                     +'<Legend defaultMouseOverAction="false" useVisibleCheck="false"/>'
			                    +'</Options>'
			                 +'<Column2DChart showDataTips="true" columnWidthRatio="0.4">'
			                     +'<horizontalAxis>'
			                           +'<CategoryAxis categoryField="ftrIdn"/>'
			                      +'</horizontalAxis>'
			                      +'<verticalAxis>'
			                         +'<LinearAxis />'
			                      +'</verticalAxis>'
			                        +'<series>'
			               /*
			              type 속성을 stacked로 변경
			                type속성으로는
			               clustered : 일반적인 다중데이터(차트의 멀티시리즈)방식으로 데이터를 표현합니다.(Default)
			              stacked : 데이터를 위에 쌓아 올린 방식으로 표현 합니다.
			                overlaid : 수치 데이터 값을 겹쳐서 표현 합니다. 주로 목표 위치와 현재 위치를 나타낼 때 많이 쓰입니다.
			                100% : 차트의 수치 데이터를 퍼센티지로 계산 후 값을 퍼센티지로 나타냅니다.
			               */
			                          +'<Column2DSet type="stacked" showTotalLabel="true" totalLabelJsFunction="totalFunc" labelYOffset="-5">'
			                              +'<series>'
			                                   +'<Column2DSeries yField="level1" displayName="지표고도" lineToEachItems="true" showLinkLabels="true">'
			                                 /* lineToEachItems : True로 설정 하면 Series간에 선을 이어준다 */
			                                        +'<showDataEffect>'
			                                           +'<SeriesInterpolate/>'
			                                       +'</showDataEffect>'
			                                  +'</Column2DSeries>'
			                                  +'<Column2DSeries yField="depth" displayName="심도" lineToEachItems="true" showLinkLabels="true">'
			                                     +'<showDataEffect>'
			                                           +'<SeriesInterpolate/>'
			                                       +'</showDataEffect>'
			                                  +'</Column2DSeries>'
			                              +'</series>'
			                          +'</Column2DSet>'
			                     +'</series>'
			                  +'</Column2DChart>'
			               +'</rMateChart>';
			 
			// 차트 데이터
			/*
			var chartData =
			 [{"Month":"Jan", "phone":12, "tv":11, "tablet":12},
			 {"Month":"Feb", "phone":14, "tv":19, "tablet":17},
			  {"Month":"Mar", "phone":23, "tv":25, "tablet":20},
			  {"Month":"Apr", "phone":20, "tv":20, "tablet":18},
			  {"Month":"May", "phone":35, "tv":25, "tablet":25},
			  {"Month":"Jun", "phone":20, "tv":22, "tablet":23},
			  {"Month":"Jul", "phone":17, "tv":20, "tablet":17},
			  {"Month":"Aug", "phone":23, "tv":21, "tablet":21},
			  {"Month":"Sep", "phone":18, "tv":17, "tablet":10}];
			 */
			// rMateChartH5.calls 함수를 이용하여 차트의 준비가 끝나면 실행할 함수를 등록합니다.
			//
			// argument 1 - rMateChartH5.create시 설정한 차트 객체 아이디 값
			// argument 2 - 차트준비가 완료되면 실행할 함수 명(key)과 설정될 전달인자 값(value)
			// 
			// 아래 내용은 
			// 1. 차트 준비가 완료되면 첫 전달인자 값을 가진 차트 객체에 접근하여
			// 2. 두 번째 전달인자 값의 key 명으로 정의된 함수에 value값을 전달인자로 설정하여 실행합니다.
			rMateChartH5.calls("chart1", {
			  "setLayout" : layoutStr,
			    "setData" : chartData
			});
			 
			// 스택 수치 합 사용자 정의 함수
			function totalFunc(index, data, value){
			 if(index == 3)
			      return value;
			  return "";
			}
			 

		};
		
		var map;
		var loadJS = function(url, implementationCode, location){
		    //url is URL of external file, implementationCode is the code
		    //to be called from the file, location is the location to 
		    //insert the <script> element

		    var scriptTag = document.createElement('script');
		    scriptTag.src = url;

		    scriptTag.onload = implementationCode;
		    scriptTag.onreadystatechange = implementationCode;

		    location.appendChild(scriptTag);
		};
		function map3dInit(){
			var mapOptions = new vw.MapOptions(vw.BasemapType.GRAPHIC, "",
				    vw.DensityType.BASIC,
				    vw.DensityType.BASIC,
				    false,
				    new vw.CameraPosition(
				    	      new vw.CoordZ(126.5616087502912, 35.67168551823694, 83487000),
				    	      new vw.Direction(-90, 0, 0)
		    	    ),
		    	    // 12300 Z값은 meter단위입니다. 지면으로부터 40-50meter 오차간격이 있습니다.
		    	    new vw.CameraPosition(
		    	      new vw.CoordZ(126.5616087502912, 35.67168551823694, 62300),
		    	      new vw.Direction(0, -90, 0)
		    	    )
				  );
				  
				  map = new vw.Map("vmap", mapOptions);        

				    // 높이 측정
				  $scope.measureHeight = function () {
				    if( map != null ) {
				      // static 메서드만 있음. start(), stop().
				      vw.MeasureHeight.start();
				      
				      // 우클릭시 종료 이벤트 설정
				      var evtMeasureRightHandler = function(event) {
				           vw.MeasureHeight.stop();
				         }      
				      vw.EventProcess.add( vw.MapController.RIGHTUPDNCLICK, map.onMouseRightDown, evtMeasureRightHandler);
				    }
				    }
				    
				    // 거리 측정
				  $scope.measureLine = function () {
				      if ( map != null ) {
				        // static 메서드만 있음. start(), stop().
				      vw.MeasureLine.start();
				      
				      // 우클릭시 종료 이벤트 설정
				      var evtMeasureRightHandler = function(event) {
				        vw.MeasureLine.stop();
				         }
				      
				        vw.EventProcess.add( vw.MapController.RIGHTUPDNCLICK, map.onMouseRightDown, evtMeasureRightHandler);
				      }
				    }
				    
				    // 면적 측정
				    $scope.measureArea = function () {
				      if ( map != null ) {
				        // static 메서드만 있음. start(), stop().
				        vw.MeasureArea.start();
				        
				      // 우클릭시 종료 이벤트 설정
				      var evtMeasureRightHandler = function(event) {
				        vw.MeasureArea.stop();
				         }
				      
				        vw.EventProcess.add( vw.MapController.RIGHTUPDNCLICK, map.onMouseRightDown, evtMeasureRightHandler);  
				      }
				    }
				    
				    // 측정 지우기.
				    $scope.erase = function () {
				      console.log("erase()~~");
				      map.clear();
				    }
				    
				    // 버튼 이벤트로 종료.
				    $scope.stop = function () {
				      console.log("stop()~~");
				      vw.MeasureHeight.stop();
				      vw.MeasureLine.stop();
				      vw.MeasureArea.stop();
				    }
		}
		
		function initgis10(){
			//console.log('test');
			if($state.current.name=='gis10'){
				//<script type="text/javascript" src="https://map.vworld.kr/js/webglMapInit.js.do?version=2.0&apiKey=BDAF35DE-72A2-39BA-AFB2-C5ABDF926045&domain=localhost"></script>
				//loadJS('https://map.vworld.kr/js/webglMapInit.js.do?version=2.0&apiKey=BDAF35DE-72A2-39BA-AFB2-C5ABDF926045&domain=localhost', function(){
					v_protocol="https://";
					vworldUrl='https://map.vworld.kr';
					vworld2DCache='https://2d.vworld.kr/2DCache';
					vworldBaseMapUrl='https://xdworld.vworld.kr/2d';
					vworldStyledMapUrl='https://2d.vworld.kr/stmap';
					vworldIsValid = 'true';
					vworldErrMsg = '';
					vworldApiKey = 'BDAF35DE-72A2-39BA-AFB2-C5ABDF926045';
					vworld3DUrl = '/js/webglMapInit.js.do';					

					loadJS('https://map.vworld.kr/js/ws3dmap/WS3DRelease/WSViewerStartup.js', function(){
						//alert('1');
						loadJS('https://map.vworld.kr/js/ws3dmap/VWViewerStartup.min.js', function(){
							//alert('2');
							loadJS('https://map.vworld.kr/js/vw.ol3WebGL_v.10.js', function(){
								//alert('3');
								map3dInit();
							},document.getElementById('loadScript'));						
						},document.getElementById('loadScript'));						
					},document.getElementById('loadScript'));
				//},document.getElementById('loadScript'));
			}
		
		}
		
		
	}


