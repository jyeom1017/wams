
function moveToTheClickedAssetLocationAndBlink(obj) {
  return function(initSelect1) {
    $.ajax(WFS_URL, {
      data: {
        service: 'WFS',
        version: '1.1.0',
        request: 'GetFeature',
        typeName: obj.LAYER_NM,
        srsname: 'EPSG:3857',
        outputFormat: 'application/json',
        exceptions: 'application/json',
        cql_filter: 'ftr_idn in (' + obj.FTR_IDN + ')'
      }
    }).done(function(response) {
      var totalFeatures = response.totalFeatures;

      if (totalFeatures === undefined || totalFeatures === 0) {
        return;
      }

      // 이동
      var feature = new ol.format.GeoJSON().readFeature((response.features)[0]);
      var extent = feature.getGeometry().getExtent();
      map.getView().fit(extent, { duration: 1000, maxZoom: 18 });
      
      

      // 반짝임-1
      featureBlink.clear();
      var type = document.getElementById('drawSelector').value;
      var selected_polygon_style = new ol.style.Style({
  		image: new ol.style.Circle({
  		    radius: 6,
  		    fill: new ol.style.Fill({color: 'rgba(255,0,0,1.0)'}),
  		    stroke: new ol.style.Stroke({color: 'rgba(255,255,255,1.0)',width: 1.5})
  			}),            	  
  		stroke: new ol.style.Stroke({color: '#ff0000',width: 3})
  	  });
      
      var selector;
      if (type === 'Polygon') {
        selector = initSelect1.PolygonSelector;
      } else if (type === 'Box') {
        selector = initSelect1.BoxSelector;
      } else if (type === 'Single') {
        selector = initSelect1.SingleSelector;
      }else{
      	selector = initSelect1.SearchSelector;
      }
      selector.el.source.getFeatures().forEach(function(f) {
        f.setStyle(selected_polygon_style);
      });

      // 반짝임-2
      featureBlink.enable(initSelect1, feature);
    });
  };
}

function toggleTreeNode($scope) {
  return function(level, nId, ap_sid) {
    console.log(ap_sid);
    if (typeof ap_sid == 'undefined') {
      if ($('#node' + nId).find('li').length == 0) {
        $('#node' + nId).append(MakeTreeHTML(level, nId));
      }

      $('#node' + nId).collapse('toggle');
      if ($('#li_' + nId).find('a').attr('aria-expanded') === 'false') {
        $('#li_' + nId).find('a').attr('aria-expanded', 'true');
      } else {
        $('#li_' + nId).find('a').attr('aria-expanded', 'false');
      }
    } else {
      var last_list = $('.last_list');
      last_list.find('.blockName').removeClass('active_color');
      $('#li_' + nId).find('.blockName').addClass('active_color');
      $scope.ap_sid = ap_sid;
    }
  };
}

function checkAllLayers($scope) {
  return function() {
    $.each($scope.layers, function(idx, item) {
      item.isActive = true;
    });

    layerUpdate();
  };
}

function uncheckAllLayers($scope) {
  return function() {
    $.each($scope.layers, function(idx, item) {
      item.isActive = false;
    });

    layerUpdate();
  };
}

function assetInformation(mainDataService, $scope) {
  return function(initSelect1) {
    $scope.$broadcast('disableAllGISMenuFunctions');
    document.querySelector('#select_mode p').innerText = '자산정보 모드';
    var selectTargetEl = document.getElementById('selectTarget');
    var drawSelectorEl = document.getElementById('drawSelector');

    selectTargetEl.selectedIndex = 1;
    drawSelectorEl.selectedIndex = 1;
    drawSelectorEl.disabled = true;

    initSelect1.DrawSelector.enableSelector(drawSelectorEl.value);
    initSelect1.enableAssetSelection();
    var select = initSelect1.SingleSelector.el.select;
    //select.on('')
    select.on('select', function(event) {
      var features = event.selected;
      if (features === undefined || features.length === 0) {
        return;
      }

      if (event.deselected !== undefined && event.deselected.length > 0) {
        featureBlink.clear();
        initSelect1.SingleSelector.el.source.getFeatures().forEach(function(f) {
          f.setStyle(null);
        });
      }
      
      var feature = features[0];
      var properties = feature.getProperties();
      
      if(properties.ftr_idn == undefined){
    	  //ftr_idn값이 없으면 취소
    	  return;
      }

      getLayerCode(mainDataService).success(function(data) {
        var param = {
          'ftr_idn': properties.ftr_idn, 'ftr_cde': properties.ftr_cde, 'layer_cd': data.C_SCODE
        };
        getAssetInfoWithGisInfo(mainDataService, param).success(function(data) {
          parent.showAssetItemPP(data);
        });
      });
    });
  };
}

function toggleNavLayer(event) {
  return function(event) {
	  //console.log(event);
	  //event.preventDefault();
	  //event.stopPropagation();
	  $('.side_nav_title').toggleClass('active');
	  $('.side_nav').stop().toggle();
  };
}

function ShowSelectedInfoList(mainDataService, $scope) {
  return function(event, data) {
	  $scope.selectedInfoList = [];
		if(parent.getSelectGisObj()==null || parent.getSelectGisObj()==undefined || parent.getSelectGisObj().length==0){
			return; 
		}
	    var selectGisObj = angular.copy(parent.getSelectGisObj());
		
	  
    $("#tableWindowSize").css("height","150px");
	$("#tableWindowSize").pageYOffset  = 0;
    $("#tableWindowSize").next().find(".btn.btn-info").text('보기');
	  
    getAssetInfoWithGisList(mainDataService, selectGisObj).success(function(data2) {
      $scope.selectedInfoList = data2;
    });
  };
}

function ClearSelectedInfoList($scope) {
  return function(event, data) {
	  featureBlink.clear();
	  
	  var type = document.getElementById('drawSelector').value;
      var selector;
      if (type === 'Polygon') {
        selector = $scope.initSelect1.PolygonSelector;
      } else if (type === 'Box') {
        selector = $scope.initSelect1.BoxSelector;
      } else if (type === 'Single') {
        selector = $scope.initSelect1.SingleSelector;
      } else {
    	  selector = $scope.initSelect1.SearchSelector;  
      }
      if(typeof selector != 'undefined' && typeof selector.el != 'undefined' && selector.el.source != null){
    	  var features = selector.el.source.getFeatures();
    	  if(features.length > 0)
    	  features.forEach(function(f) {
    	        f.setStyle(null);
   	      });    	  
      }
      
      
    $scope.selectedInfoList = [];
  };
}

function clearSelectedInfoListBroadcast($scope) {
  return function() {
    $scope.$broadcast('ClearSelectedInfoList');
  };
}

function closePopup(element) {
  $(element).parents('.popupSection').hide();
}

function closeGisPopup() {
  $('#gisPopup').hide();
}

function getLayers(mainDataService, $scope) {
  mainDataService.getCommonCodeList({ gcode: 'LAYR' }).success(function(obj) {
    var copy = angular.copy(obj);

    $.each(copy, function(idx, item) {
      if (item.C_SCODE.length == 1) {
        item.level = 1;
      } else {
        item.level = 2;
        item.PCODE = item.C_SCODE.substring(0, 1);
      }
    });

    $scope.layers = copy;
  });
}

function reloadLayers(mainDataService, $scope) {
  return function() {
    //gis layer 코드
    //clearAllBrowserCache();
	  //debugger;
	  //var layers = map.getLayers().getArray();
	  //console.log(layers);
	  
	  removeLayers();
	  //initLayers();
	  
    //getLayers(mainDataService, $scope);
    //setTimeout(layerUpdate, 0);
    
  };
}

function getLayerCode(mainDataService) {
  var target = document.getElementById('selectTarget');
  var text = target.options[target.selectedIndex].text;
  var param = { 'gcode': 'LAYR', 'name': text };

  return mainDataService.getCommonCodeInfo(param);
}

function getAssetInfoWithGisInfo(mainDataService, param) {
  return mainDataService.getAssetInfoWithGisInfo(param);
}

function getAssetInfoWithGisList(mainDataService, param) {
  return mainDataService.getAssetInfoWithGisList(param);
}

function checkParent($scope) {
  return function(item) {
    var code = item.C_SCODE.substring(0, 1);
    var count1 = 0;
    var count2 = 0;
    var pItem = null;

    $.each($scope.layers, function(idx, Item) {
      if (Item.C_VALUE1 == 1 && Item.C_SCODE == code) {
        pItem = Item;
      }

      if (Item.PCODE == code && Item.isActive == true && Item.C_VALUE1 != 1) {
        count1++;
      }

      if (Item.PCODE == code) {
        count2++;
      }
    });

    pItem.isActive = count1 == count2;
  };
}

function checkChild($scope) {
  return function(item) {
    var code = item.C_SCODE.substring(0, 1);
    var count1 = 0;
    var count2 = 0;

    $.each($scope.layers, function(idx, Item) {
      if (Item.PCODE == code && Item.isActive == true && Item.C_VALUE1 != 1) {
        count1++;
      }

      if (Item.PCODE == code) {
        count2++;
        Item.isActive = item.isActive;
      }
    });
  };
}

function searchAsset(mainDataService, $scope) {
  return function(initSelect1) {
    angular.element(document.querySelector('#MainController')).scope().$broadcast('disableAllGISMenuFunctions');

    var assetCode = document.getElementById('assetCode').value;
    var selectTarget = document.getElementById('assetSearchTarget');
    var vectorLayerNameTemp = selectTarget.options[selectTarget.selectedIndex].value;
    var vectorLayerNameTempSplit = vectorLayerNameTemp.split(':');
    assetCode = assetCode.trim();
    document.getElementById('assetCode').value = assetCode;
    if(assetCode==''){
    	alertify.alert('info','자산코드를 입력하세요',function(){});
    	return;
    }

    if (commonUtil.isEmpty(vectorLayerNameTemp)) {
      alertify.alert('Error','검색 대상을 먼저 선택해주세요.',function(){});
      return;
    }

    var parameter = {
      'asset_cd': assetCode,
      'memo': vectorLayerNameTempSplit[1].toUpperCase(),
      'cvalue1': new URLSearchParams(vectorLayerNameTempSplit[2].replace(/ /gi,'').replace('AND','&')).get('saa_cde')?.replaceAll('\'', '') || '',
      'cvalue2': vectorLayerNameTempSplit[3]
    };

    $scope.clearSelectedInfoList();

    mainDataService.getAssetByAssetCode(parameter).then(function(response) {
    	console.log(response);
    	if(response.data.length==0){
    		alertify.alert('info','검색된 자산이 없습니다.');
    		return;
    	}
      var responseData = response.data;

      if (Array.isArray(responseData) && responseData.length > 0) {
        var ftrIdnStringWithComma = responseData.map(function(obj) {
          return obj.FTR_IDN;
        }).join();
        var CQL = 'ftr_idn IN (' + ftrIdnStringWithComma + ')';

        // 화면에 표시
        searchLayer.setSource(getTileLayerCQL(vectorLayerNameTempSplit[1], CQL));

        // 시설물 목록이 뜬다
        initSelect1.SearchSelector.enable();

        return $.ajax(WFS_URL, {
          type: 'GET', data: {
            service: 'WFS',
            version: '1.1.0',
            request: 'GetFeature',
            typeName: vectorLayerNameTempSplit[1],
            srsname: 'EPSG:3857',
            outputFormat: 'application/json',
            CQL_FILTER: CQL
          }
        });
      }
    }).then(function(response) {
    	if(response == undefined || response.features==undefined || response.features==null){
    		return;
    	}    	
      var features = angular.copy(response.features);
      var featuresLength = features.length;
      
      var selected_polygon_style = new ol.style.Style({
  		image: new ol.style.Circle({
  		    radius: 6,
  		    fill: new ol.style.Fill({color: 'rgba(255,0,0,1.0)'}),
  		    stroke: new ol.style.Stroke({color: 'rgba(255,255,255,1.0)',width: 1.5})
  			}),            	  
  		stroke: new ol.style.Stroke({color: '#ff0000',width: 3})
  	  });      

      if (featuresLength > 0) {
        for (var i = 0; i < featuresLength; i++) {
          var feature = features[i];
          var validFeature = initSelect1.SearchSelector.el.source.getFeatureById(feature.id);
          //새로운 feature만 생성하여 준다.
          if (validFeature == null) {
            var features1 = new ol.format.GeoJSON().readFeatures(feature);
            var extent = features1[0].getGeometry().getExtent();
            map.getView().fit(extent, { duration: 1000, maxZoom: 18 });
        	features1.forEach(function(fe){
        		fe.setStyle(selected_polygon_style);
        	})
            initSelect1.SearchSelector.el.source.addFeatures(features1);
            initSelect1.SearchSelector.el.select.getFeatures().push(features1[0]);
          }
        }

        initSelect1.SearchSelector.el.select.dispatchEvent('select');
      }
    });
  };
}



function searchAddress() {
  return function() {
    var address = document.getElementById('address').value;
    var url = 'http://api.vworld.kr/req/address';
    var type = document.getElementById('searchAddrOptions').value;
    if(address.trim()==''){
    	alertify.alert('info','주소를 입력하세요',function(){});
    	return;
    }
    var param = {
      service: 'address', version: '2.0', request: 'GetCoord', key: vworld_apikey, format: 'json', errorFormat: 'json', type: type, // PARCEL : 지번주소, ROAD : 도로명주소
      address: address, refine: true, // 정제되어 있는 주소의 경우 false로 설정하여 주소 정제 없이 빠르게 처리 true(기본값), false
      simple: false, // 응답결과 간략 출력 여부 true, false(기본값)
      crs: 'EPSG:4326'
    };
    
    map.getLayers().getArray()
    .filter(layer => layer.get('name') === 'Marker')
    .forEach(layer => map.removeLayer(layer));
    
    $.ajax(url, { data: param, dataType: 'jsonp' }).done(function(data, textStatus, jqXHR) {
      var response = data.response;
      var status = response.status;

      if (status === 'NOT_FOUND') {
        alertify.alert('Info', '검색조건에 맞는 자료가 없습니다.');
      } else if (status === 'OK') {
        var point = response.result.point;
        var long = parseFloat(point.x);
        var lat = parseFloat(point.y);

        map.getView().setCenter(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
        
        var pointFeature = new ol.Feature({
        	geometry: new ol.geom.Point(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'))
        });
        
        let featureSource = new ol.source.Vector({});
        
        featureSource.addFeature(pointFeature);
        
        let featureLayer = new ol.layer.Vector({
        	name: 'Marker',
        	source: featureSource 
        });
        
        map.addLayer(featureLayer);
        
        
      } else if (status === 'ERROR') {
        console.log(data);
        alertify.error('알수없는 오류가 발생했습니다. 잠시후 다시 시도해 주세요.');
      }
    });
  };
}

function writeExcel($scope) {
  return function() {
    var colIndex = [
      { name: '순번', id: 'RNUM' },
      { name: '자산코드', id: '\'10001\'' },
      { name: '자산명', id: '\'10002\'' },
      { name: '지형지물관리번호', id: '\'10009\'' },
      { name: '지형지물관리부호', id: '\'10010\'' },
      { name: '대블록', id: '\'10016\'' },
      { name: '중블록', id: '\'10017\'' },
      { name: '소블록', id: '\'10018\'' },
      { name: '관로구분', id: '\'10021\'' },
      { name: '급수용도', id: '\'10020\'' },
      { name: '위치(주소)', id: '\'10005\'' },
      { name: '관종구분', id: '\'10022\'' },
      { name: '관경', id: '\'10025\'' },
      { name: '관로길이', id: '\'10026\'' },
      { name: '부설일', id: '\'10004\'' },
      { name: '관내부도장제', id: '\'10023\'' },
      { name: '관외부도장제', id: '\'10024\'' },
      { name: '지형조건', id: '\'10027\'' },
      { name: '지형조건세부', id: '\'10028\'' },
      { name: '관보호공', id: '\'10029\'' }
    ];

    console.log($scope.selectedInfoList);
    var rows = [];
    $.each($scope.selectedInfoList, function(idx, Item) {
      var collumns = [];
      if (idx === 0) {
        var headers = [];
        $.each(colIndex, function(idx1, item) {
          headers.push(item.name);
        });
        rows.push(headers);
      }
      $.each(colIndex, function(idx1, item) {
        collumns.push(Item[item.id]);
      });
      rows.push(collumns);
    });

    var workSheetData = rows;
    var workSheet = XLSX.utils.aoa_to_sheet(workSheetData);
    //workSheet['!autofilter'] = {ref : "A1:R11"};
    var workBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, '선택자산');
    XLSX.writeFile(workBook, "선택자산목록_"+ $scope.getToday()+".xlsx");
  };
}

function addGisAsset(mainDataService, $rootScope) {
  return function(event, data) {
    var fid = data;
    var editTarget = document.getElementById('editTarget');
    var vectorLayerNameTemp = editTarget.options[editTarget.selectedIndex].value;
    var vectorLayerNameTempSplit = vectorLayerNameTemp.split(':');
    var layerCd = vectorLayerNameTempSplit[1];
    var layerCde = new URLSearchParams(vectorLayerNameTempSplit[2].replace(/ /gi,'').replace('AND','&')).get('saa_cde')?.replaceAll('\'', '') || '';
    var layerType = vectorLayerNameTempSplit[3];

    var responseData;
    var param = { 'table_nm': fid[0], 'gid': fid[1] };
    $rootScope.newGisAssetSid = 0; 
    // 관로 일 떄, saa_cde 업데이트
    if (layerCde) {
      param.saa_cde = layerCde;
    }

    var ftrCde;
    var ftrIdn;

    mainDataService.updateFeatureCodeWithLastData(param).then(function(response) {
      responseData = response.data;
      ftrCde = responseData.ftr_cde;
      ftrIdn = responseData.ftr_idn;

      param = { 'layer_cd': layerCd, 'layer_cde': layerCde, 'layer_type': layerType ,'ftr_idn' : ftrIdn,'ftr_cde':ftrCde};

      return mainDataService.getAssetInfoWithLayer(param);
    }).then(function(response) {
      responseData = response.data;
      $rootScope.newGisAssetSid = responseData.AssetItemInfo.ASSET_SID;
      angular.element(document.getElementById('dialog-registAssetItemPopup')).scope().$parent.newMode = 'new';
      angular.element(document.getElementById('dialog-registAssetItemPopup')).scope().$parent.saveMode = 'newGisAssetItem';

      $rootScope.$broadcast('RegistAssetItemPopup', {
        'asset_info': responseData.AssetItemInfo, 'ItemList': responseData.ItemList, 'callback_Id': 'ItemList_Add', 'callback_Fn': function() {
        	var message = '저장되었습니다';
            alertify.success(message);
            gen.next();
        	/*
          param = { 'asset_sid': responseData.ASSET_SID, 'ftr_idn': ftrIdn, 'layer_cd': layerCd };
          mainDataService.addAssetGis(param)
              .then(function(response) {
                responseData = response.data;
                var message = responseData > 0 ? '저장되었습니다' : '저장에 실패했습니다.';

                alertify.success(message);
              })
              .then(gen.next());
              */
        }
      });
      	/*
      var scope = angular.element(document.querySelector('#dialog-registAssetItemPopup')).scope().$parent;
      // FIXME:
      var listener = scope.$watch('ItemList', function(newValue, oldValue) {
        scope.ItemList.find(function(obj) {
          return obj.ITEM_CD === '10009';
        }).ITEM_VAL = ftrIdn;

        scope.ItemList.find(function(obj) {
          return obj.ITEM_CD === '10010';
        }).ITEM_VAL = ftrCde;
      });

      setTimeout(function() {
        listener(); //unbind watch
      }, 5000);
      */
    });
  };
}

function addAssetPathPopupStyleAttribute() {
  var element = document.getElementById('dialog-assetlevelpath');
  element.style.position = 'fixed';
  element.style.zIndex = '9991';
}

// FIXME:
// 스택 수치 합 사용자 정의 함수
function totalFunc(index, data, value) {
  //if (index == 3) {
  return value;
  //}
  //return '';
}

function seriesLabelFunc1(index, data, value) {
  console.log(index, data, value);
  return value.depth;
}

function seriesLabelFunc2(index, data, value) {
  console.log(index, data, value);
  return value.level1;
}

function dataTipFunc(seriesId, seriesName, index, xName, yName, data, values) {
	  //console.log(seriesId, seriesName, index, xName, yName, data, values);
	console.log(data);
	  return '<table cellpadding=\'0\' cellspacing=\'1\' style=\'width:250px;\' >'
	      + '<tr>'
	      + '<td align=\'center\' colspan=\'2\' style=\'border-bottom:solid 1px #8b8b8b;\'>' + data.CATEGORY + '</td>'
	      + '</tr><tr>'
	      + '<td >가장최근년도이전</td><td align=\'center\'>' + data.YEARBEFOR + '</td>'
	      + '</tr><tr>'
	      + '<td >최근년도</td><td align=\'center\'>' + data.YEARLATEST + '</td>'
	      + '</tr></table>';
}

function dataTipFuncDanCha1(seriesId, seriesName, index, xName, yName, data, values) {
  //console.log(seriesId, seriesName, index, xName, yName, data, values);
  return '<table cellpadding=\'0\' cellspacing=\'1\' style=\'width:250px;\' >'
      + '<tr>'
      + '<td align=\'center\' colspan=\'2\' style=\'border-bottom:solid 1px #8b8b8b;\'>' + data.ftrIdn + '</td>'
      + '</tr><tr>'
      + '<td >지표고도(m)</td><td align=\'center\'>' + data.level + '</td>'
      + '</tr><tr>'
      + '<td >매설깊이(m)</td><td align=\'center\'>' + data.depth + '</td>'
      + '</tr><tr>'
      + '<td >매설관해발고도(m)</td><td align=\'center\'>' + data.level1 + '</td>'
      + '</tr></table>';
}

function chartItemClickHandler(seriesId, displayText, index, data, values) {
  var _values = [...values];
  var parameter = {
    'ftr_idn': _values[0], 'ftr_cde': 'SA001', 'layer_cd': 'P'
  };
  var param = new URLSearchParams(parameter).toString();
  var url = '/gis/getAssetInfoWithGisInfo.json?' + param;

  $.ajax(url, {
    dataType: 'json',
    contentType: 'application/json;'
  }).done(function(data) {
    parent.showAssetItemPP(data);
  });
}

function deleteTemporaryAsset(mainDataService,$rootScope) {
  return function(event, data) {
    //$('.GIS-registerAssetItemPopupClose').one('click', function() {
	  if(data.FLAG){
		  var param = { 'table_nm': data.LAYER, 'gid': data.GID };
	      mainDataService.deleteTemporaryAsset(param)
	      .success(function(data1){
	    	  alertify.success('gis삭제');
	      });
	      var param2 = { ASSET_SID: data.ASSET_SID };
	      mainDataService.deleteAssetItemInfo(param2)
	      .success(function(data2) {
	    	  alertify.success('asset삭제');
	      });  
	  }
      gen.next();      
    //});
  };
}

var featureBlink = (function() {
  var intervalArray = [];
  var interval;

  return {
    enable: function(initSelect1, feature) {
      var type = document.getElementById('drawSelector').value;
      var selector;

      if (type === 'Polygon') {
        selector = initSelect1.PolygonSelector;
      } else if (type === 'Box') {
        selector = initSelect1.BoxSelector;
      } else if (type === 'Single') {
        selector = initSelect1.SingleSelector;
      }

      var colors = ['#212121', '#ff0000'];
      var colorsIndex = 0;
      var validFeature = selector.el.source.getFeatureById(feature.getId());

      interval = setInterval(function() {
        var fill = new ol.style.Fill({ color: colors[colorsIndex] });
        var stroke = new ol.style.Stroke({ color: colors[colorsIndex], width: 1.25 });

        validFeature.setStyle(new ol.style.Style({
          image: new ol.style.Circle({
            fill: fill,
            stroke: stroke,
            radius: 5
          }),
          fill: fill,
          stroke: stroke
        }));

        colorsIndex++;

        if (colorsIndex > colors.length - 1) {
          colorsIndex = 0;
        }
      }, 1000);

      intervalArray.push(interval);

      return interval;
    },
    clear: function() {
      for (var i = 0; i < intervalArray.length; i++) {
        clearInterval(intervalArray[i]);
      }
    }
  };
})();

function addMetaTag(name,content){
	var meta = document.createElement('meta');
	meta.httpEquiv = name;
	meta.content = content;
	document.getElementsByTagName('head')[0].appendChild(meta);
	}

var clearAllBrowserCache = function(){
	//caches.keys().then((keyList) => Promise.all(keyList.map((key) => caches.delete(key))));
	addMetaTag("pragma","no-cache");
	addMetaTag("expires","0");
	addMetaTag("cache-control","no-cache");
	
	location.reload();
}