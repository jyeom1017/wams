angular.module('app.statistics').controller('dataAnalysisController',dataAnalysisController);

function dataAnalysisController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	
	$scope.DisplayYearMonth = function(val){
		return val.substring(0,4) + ((val.length>4)?("." + val.substring(4,6)):"");
	}
	$scope.ExcelDownLoad = function(data,colIndex,filename,sheetName){
		//alert('test1');
		var data2 = [];
		var list2 = [];
		if(sheetName=='사용율'){
			list2.push({name:sheetName,id:'MSG'});
		}else if(sheetName=='파괴심각도' || sheetName=='고장확률'){
			list2.push({name:sheetName,id:'GRADE'});
		}else if(sheetName=='서비스수준' ){
			list2.push({name:'항목',id:'C_NAME'});
			list2.push({name:'항목1',id:'EVALUE_NM'});
			list2.push({name:'목표',id:'GOAL'});
			list2.push({name:'최근년도',id:'LATEST'});
			list2.push({name:'이전년도',id:'BEFOR'});
		}else if(sheetName=='개보수' || sheetName=='조사및탐사'){
			list2.push({name:'항목',id:'T'});
		}else if(sheetName=='민원'){
			list2.push({name:'민원종류',id:'C_NAME'});			
		}else if(sheetName=='생애주기'){
			list2.push({name:'항목',id:'C_NAME'});
		}else if(sheetName=='재정수지'){
			list2.push({name:'구분',id:'FIN_ANLS_GUBUN_NM'});
			list2.push({name:'항목',id:'FIN_ANLS_ITEM_NM'});
			list2.push({name:'상세',id:'ACCOUNT_NM'});
		}else if(sheetName=='A' || sheetName=='B' ){ //최척투자 자산,부서
			list2.push({name:'항목',id:'LEVEL_NM'});
			for(year in $scope.yearList2){
				list2.push({name:$scope.yearList2[year] + '갯수',id:'cnt_'+year});
				list2.push({name:$scope.yearList2[year] + '비용',id:'amt_'+year});
			}			
		}else if(sheetName=='D' ){ //최척투자 추세분석
			list2.push({name:'항목',id:'C_NAME'});
			for(year in $scope.yearList2){
				list2.push({name:$scope.yearList2[year] + '갯수',id:'cnt_'+year});
				list2.push({name:$scope.yearList2[year] + '비용',id:'amt_'+year});
			}
		}else if(sheetName=='C' ){ //최척투자 주기
			list2.push({name:'주기분석',id:'C_NAME'});
			for(var i=0;i<5;i++){
				list2.push({name:$scope.yearGradeInfo(''+i)+ '갯수',id:'cnt_'+i});
				list2.push({name:$scope.yearGradeInfo(''+i)+ '비용',id:'amt_'+i});
			}
		}else if(sheetName=='상태평가'){
			list2.push({name:'평가등급',id:'EST_GRADE'});	
		}else
			list2.push({name:'항목명',id:'COL_NM'});
		
		
		if(sheetName=='A' || sheetName=='B' || sheetName=='D' ){
			$.each(data,function(idx,Item){
			var item = {};
			item['C_NAME'] = Item['C_NAME'];
			item['LEVEL_NM'] = Item['LEVEL_NM'];
			var i=0;
			for(year in $scope.yearList2){
				item['cnt_'+year] = Item['\''+ $scope.yearList2[year] +'\''][1];
				item['amt_'+year] = Item['\''+ $scope.yearList2[year] +'\''][0];
				i++;
			}
			data2.push(item);
			});
		}else if(sheetName=='C' ){
			var item = {};
				for(var i=0;i<5;i++){
					item['cnt_'+i] = data[0]['\''+ i +'\''][1];
					item['amt_'+i] = data[0]['\''+ i +'\''][0];
				}
			data2.push(item);
		}else{
			data2 = angular.copy(data);
		}
		
		if(sheetName!='서비스수준' && sheetName!='C' && sheetName!='A' && sheetName!='B' && sheetName!='D' ){
			if(typeof colIndex[0].name=='undefined' || typeof colIndex[0].id=='undefined'){
				var list = angular.copy(colIndex);
				for(id in list){
					if(sheetName=='잔존수명'){
						list2.push({name:list[id] +'년',id: list[id] });
					}else
						list2.push({name:list[id],id:'\'' + list[id] + '\''});
				}
			}
		}
		
		exportReportToExcel(data2,list2,filename,sheetName);
		//alert('test');
	}
	
	$scope.searchUNIT = 'YEAR';
	$scope.monthList = []; // 월선택
	{
		for(var i = 0;i<12;i++)
			$scope.monthList.push({isChecked:true,name:''+(i+1)});
	}
	$scope.checkAllMonth = function(checked){
			console.log(checked);
			$.each($scope.monthList,function(idx,Item){
				Item.isChecked = checked;
			});
	}
	$scope.selectComplain = ''; //민원 통계항목 기본값
	$scope.selectLos = '01'; //서비스 통계항목 기본값 
	$scope.storeComplainCategory = []; // 민원유형 카테고리 (공통코드에서 가져옴)
	$scope.selectFN = ''; //시설관망 기본값
	
	$scope.selectRiskCategory =''; //위험도 통계항목 기본값
	
	$scope.selectFinanceCategory ='FINANCIAL_BALANCE'; //재정수지 통계항목 기본값
	$scope.searchDateType = '2'; // 날짜검색 : 연도(1) or 기간선택(2)
	$scope.searchDate = {}; // 기간선택
	
	$scope.selectSTAT = 'A';
	$scope.selectSTAT_service = 'A';
	
	$scope.changeRadio = function (unit) {
		$scope.searchUNIT = unit;
	};
	
	$scope.itemList = {CNT:'사업(개)', TOT: '금액 (천원)'}

	console.log($state);
	$scope.TabId = $state.params.tabId; //탭
	
	if($scope.TabId == '1' ){
		$scope.searchDate.S_DATE ='19920101';
		//$scope.searchDate.E_DATE ='20220101';
		$scope.searchDate.E_DATE = $scope.ComCodeYearList[0].C_SCODE;
		//$scope.searchDate.E_DATE = "20230101";

	}
	/*else if($scope.TabId == '6' ){
		$scope.searchDate.S_DATE ='20210101';
		$scope.searchDate.E_DATE ='20211231';		
	}*/
	else{
		//$scope.searchDate.E_DATE ='20220101';
		$scope.searchDate.E_DATE = $scope.ComCodeYearList[0].C_SCODE;
		$scope.searchDate.S_DATE = '' + (parseInt($scope.ComCodeYearList[0].C_SCODE) -4 ) + '0101';
	}
	
	$scope.changestr = function(){
		if($scope.searchUNIT === 'YEAR'){
			$scope.searchDate.S_DATE = $scope.searchDate.S_DATE.substr('0','4');
			$scope.searchDate.E_DATE = $scope.searchDate.E_DATE.substr('0','4');
		}else {
			$scope.searchDate.S_DATE = $scope.searchDate.S_DATE.substr('0','7');
			$scope.searchDate.E_DATE = $scope.searchDate.E_DATE.substr('0','7');
		}
	};
	$scope.changestr2 = function(){
		if($scope.searchUNIT === 'YEAR'){
			$scope.searchDate.S_DATE = $scope.searchDate.S_DATE.substr('0','4');
			$scope.searchDate.E_DATE = $scope.searchDate.E_DATE.substr('0','4');
		}else {
			$scope.searchDate.S_DATE = $scope.searchDate.S_DATE.substr('0','4')+'-01';
			$scope.searchDate.E_DATE = $scope.searchDate.E_DATE.substr('0','4')+'-01';
		}
	};
	
	if($scope.searchUNIT === 'YEAR'){
		$scope.searchDate.S_DATE = $scope.searchDate.S_DATE.substr('0','4');
		$scope.searchDate.E_DATE = $scope.searchDate.E_DATE.substr('0','4');
	}else {
		$scope.searchDate.S_DATE = $scope.searchDate.S_DATE.substr('0','7');
		$scope.searchDate.E_DATE = $scope.searchDate.E_DATE.substr('0','7');
	}
	
	
	//블록선택
	$scope.Block_PO_PCODE = 'block1';
	$scope.blockList = [];
	
	$scope.getBlockList = function (block) {
		var PO_PCODE = '01';
		mainDataService.getBlockCodeList({
		}).success(function (obj) {
			if (!common.isEmpty(obj.errorMessage)) {
				alertify.error('errMessage : ' + obj.errorMessage);
			} else {
				$scope.blockList = obj;
				console.log($scope.blockList);
			}
		});
	};

	$scope.searchMonth = ''; //월 선택 기본값
	$scope.daeBlock = ''; //대블록 초기값
	$scope.jungBlock = ''; //중블록 초기값
	$scope.soBlock = ''; //소블록 초기값
	$scope.subBlock = ''; //관리블록 초기값
	$scope.searchEstimation = '간접평가'; //상태평가 통계항목 기본값 간접평가
	
	$scope.daeblock = function (val){
		$scope.daeBlock = val;
	}
	$scope.jungblock = function (val){
		$scope.jungBlock = val;
	}
	$scope.soblock = function (val){
		$scope.soBlock = val;
	}
	$scope.subblock = function (val){
		$scope.subBlock = val;
	}
	$scope.selectComplainVal = function(val){
		$scope.selectComplain = val; //민원통계항목
	}
	$scope.selectMonth = function(val){
		$scope.searchMonth = val;
	}
	$scope.selectEstimation = function(val){
		$scope.searchEstimation = val; //상태평가 통계항목
	}
	$scope.selectLosVal = function(val){
		$scope.selectLos = val; //서비스분석 통계항목
		console.log($scope.selectLosVal);
	}
	$scope.selectRiskCategory_Name = "사용률";
	var riskTitle = "사용률";
	$scope.selectRisk = function(val){
		$scope.selectRiskCategory = val;
		console.log($scope.selectRiskCategory);
		$scope.selectRiskCategory_Name = $("#selectRiskCategory option:checked").text();
		if($scope.selectRiskCategory == 'SERIOUS_GRADE'){
			riskTitle = '파괴심각도';
		}else if($scope.selectRiskCategory == 'PROB_GRADE') {
			riskTitle = '고장확률';
		}else {
			riskTitle = '사용률';
		}
	};
	
	$scope.selectFinance = function(val){
		$scope.selectFinanceCategory = val;
		console.log($scope.selectFinanceCategory);
	};
	
    //시점 설정 팝업
    $scope.showViewPointMonth = function(){
    	$("#viewPointMonth").show();
    };
    //시점 설정 닫기
    $scope.closeViewPointMonth = function(){
    	$("#viewPointMonth").hide();
    };
    //시점 설정 적용
    $scope.saveViewPointMonth = function(){
    	$("#viewPointMonth").hide();
    };
    
	
	$scope.$on('$viewContentLoaded', function () {
		$scope.getBlockList(); //블록정보 가져오기
		setDatePicker();
		$scope.setDatePicker = setDatePicker;
    });
	
	//민원 통계항목 카테고리
	mainDataService.getCommonCodeList({
        gcode : '503',
        gname : ''
    }).success(function(data) {
        if (!common.isEmpty(data.errMessage)) {
            alertify.error('errMessage : ' + data.errMessage);
        } else {
            $scope.storeComplainCategory = data;
        }
    });
	$scope.selectST_Name = "전체";
	
	$scope.changeSelectST = function(selectFN)
	{
		$scope.selectST  = selectFN;
		if(selectFN == '' || selectFN == 'N' || selectFN == 'F' ){
			$("#selectST").val("N");
			$("#selectST").find("option[value=N]").attr("selected",true);
			//$scope.
		}
		$scope.selectST_Name = $("#selectST option:checked").text();
	}
	
	$scope.yearTot = [];
	$scope.yearTot2 = [];
	$scope.total = 0;
	$scope.statItemList = [];
	
	
	$("#loadingSpinner").hide();
	
	//----------------------------조회-----------------------------------
	
	
	//자산 조회하기
	$scope.searchAsset = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		if($scope.selectST=='N'){
			$scope.level_path_cd = "";
			$scope.level_path_nm = "";
		}
		if($scope.selectST=='F' || $scope.selectST==''){
			$scope.daeBlock = "";
			$scope.jungBlock = "";
			$scope.soBlock = "";
			$scope.subBlock = "";
		}
			
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				CHK_MONTH_LIST:chkMonth,
				//selectFN : $scope.selectFN,
				selectST : $scope.selectST || '', //자산 통계항목
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock, //관리블록
				PATH_CD : $scope.level_path_cd
		};
		mainDataService.getStatAsset(param)
		.success(function(data){
			$("#loadingSpinner").hide();
			$scope.statItemList = data;
			var list = ['ROWNUM','관종','관경','구경','재질','계통','용도','tot','COL_NM'];
			$scope.yearList2 = [];

			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//민원유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();

//			if($scope.searchUNIT === 'MONTH'){
//				for(var i=0; i<$scope.yearList2.length; i++){
//					$scope.yearList2[i] = $scope.yearList2[i].substr('0','4')+'-'+$scope.yearList2[i].substr('4','6')
//				}
//			}
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}
				});
			});
			chartAsset($scope.yearTot,$scope.yearList2);
		});				
	};
	//개보수 조회하기
	$scope.searchRenovation = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				CHK_MONTH_LIST:chkMonth,
				//CHK_MONTH_LIST:$scope.searchMonth,
				COMPLAIN_TYPE : $scope.selectComplain, //민원 통계항목
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock //관리블록
		};//,'04','05','06','07','08','09','10','11','12'
		
		console.log(param);
		mainDataService.getStatRenovation(param)
		.success(function(data){
			$scope.statMinwonList = data;
			var list = ['ROWNUM','T','tot'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//민원유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}
				});
			});
			$("#loadingSpinner").hide();
			chartRenovation($scope.statMinwonList,$scope.yearList2);
			
		});		
	};
	
	//조사 및 탐사 조회하기
	$scope.searchInvestigation = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				CHK_MONTH_LIST:chkMonth,
				//CHK_MONTH_LIST:$scope.searchMonth,
				COMPLAIN_TYPE : $scope.selectComplain, //민원 통계항목
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock //관리블록
		};//,'04','05','06','07','08','09','10','11','12'
		mainDataService.getStatInspect(param)
		.success(function(data){
			$scope.statMinwonList = data;
			var list = ['ROWNUM','T','tot'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}
				});
			});
			
			chartInspect($scope.statMinwonList,$scope.yearList2);
			$("#loadingSpinner").hide();
		});		
	};
	
	

	//민원 조회하기
	$scope.searchComplain = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				CHK_MONTH_LIST:chkMonth,
				//CHK_MONTH_LIST:$scope.searchMonth,
				COMPLAIN_TYPE : $scope.selectComplain, //민원 통계항목
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock //관리블록
		};//,'04','05','06','07','08','09','10','11','12'
		mainDataService.getStatMinwon(param)
		.success(function(data){
			$scope.statMinwonList = data;
			var list = ['ROWNUM','COMPLAIN_TYPE','C_NAME','tot'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//민원유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}
				});
			});
			
			chartComplain($scope.yearTot,$scope.yearList2,data);
			$("#loadingSpinner").hide();
		});
	};

	//상태평가 조회하기
	$scope.searchState = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		//alert($scope.searchEstimation);
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				CHK_MONTH_LIST:chkMonth,
				CATEGORY : $scope.searchEstimation,
				//COMPLAIN_TYPE : $scope.selectComplain, //민원 통계항목
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock //관리블록
		};//,'04','05','06','07','08','09','10','11','12'
		
		mainDataService.getStatIndirEst(param)
		.success(function(data){
			$scope.statMinwonList = data;
			var list = ['ROWNUM','EST_GRADE','GRADE','tot'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}
				});
			});
			
			chartState($scope.yearTot,$scope.yearList2,data);
			$("#loadingSpinner").hide();
		});		
	};
	//잔존수명 조회하기
	$scope.searchRemain = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				CHK_MONTH_LIST:chkMonth,
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock, //관리블록
				PATH_CD : $scope.level_path_cd  //자산경로
		};
		mainDataService.getStatRemainLife(param)
		.success(function(data){
			$scope.statMinwonList = data;
			console.log($scope.statMinwonList);
			var list = ['ROWNUM','COMPLAIN_TYPE','C_NAME','tot'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//민원유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			//$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					/*if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}*/

					$scope.yearTot[idx2] = parseInt(Item[yyyy]); //...
				});
			});

			$("#loadingSpinner").hide();
			console.log($scope.yearTot);
			chartRemain($scope.yearTot,$scope.yearList2);
		});		
	};
	
	//위험도 조회하기
	$scope.searchRisk = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				ITEM_GB : $scope.selectRiskCategory,
				CHK_MONTH_LIST:chkMonth,
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock, //관리블록
				PATH_CD : $scope.level_path_cd  //자산경로
		};
		mainDataService.getStatRisk(param)
		.success(function(data){
			$scope.statMinwonList = data;
			var list = ['ROWNUM','EST_GRADE','GRADE','GRADE2','MSG','tot'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}
				});
			});
			$("#loadingSpinner").hide();

			var chartData = [];
			$.each($scope.yearList2,function(idx,Item){
				var data1 = {};
				data1.yyyy = Item;
				$.each([...$scope.statMinwonList].reverse(), function(idx2, Item2) {
					data1['CATEGORY'+idx2] = Item2['\''+Item+'\''];
				});
				chartData.push(data1);
			});
			
			 //var legend = ["1등급","2등급","3등급","4등급","5등급","6등급","7등급","8등급","9등급","10등급"];
			 var legend = ["10등급","9등급","8등급","7등급","6등급","5등급","4등급","3등급","2등급","1등급"];
			 if($scope.selectRiskCategory === ''){
				 legend = ["0~10%","10~20%","20~30%","30~40%","40~50%","50~60%","60~70%","70~80%","80~90%","90~100%"];
				 //legend.reverse();
			 }
			
			//chartRisk($scope.yearTot,$scope.yearList2);
			chartRisk(chartData,legend);
		
		});		
	};
	//서비스 조회하기
	$scope.searchLos = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				LOS_ITEM_CD: $scope.selectLos, //서비스 통계항목
				CHK_MONTH_LIST:chkMonth,
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock, //관리블록
				los_item_cd : $scope.selectLos 
		};//,'04','05','06','07','08','09','10','11','12'
		mainDataService.getStatLoS(param)
		.success(function(data){
			if(data.length==0){
				$scope.statMinwonList=[];
				var chartData = [];
				$("#loadingSpinner").hide();
				chartLos(chartData);				
				return;
			}
			$scope.statMinwonList = data;
			$scope.latest_year = data[0].LATEST_YEAR;
			$scope.previous_year = data[0].PREVIOUS_YEAR;
			console.log($scope.statMinwonList);
			var list = ['ROWNUM','PREVIOUS','C_NAME','EVALUE_CD','EVALUE_NM','GOAL','LATEST','LOS_ITEM_CD','tot','LATEST_YEAR','PREVIOUS_YEAR'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//민원유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}
				});
			});
			var chartData = [];
			$.each($scope.statMinwonList,function(idx,Item){
				chartData.push({"CATEGORY":Item.EVALUE_NM, "YEARLATEST":Item.LATEST, "YEARBEFOR":Item.PREVIOUS});	
			});
			$("#loadingSpinner").hide();
			chartLos(chartData);
			
		});		
	};
	//생애주기 조회하기
	$scope.searchLcc = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		console.log($scope.selectST);
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				selectST : $scope.selectST || '', // 통계항목
				//selectFN : $scope.selectFN,
				CHK_MONTH_LIST:chkMonth,
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock, //관리블록
				PATH_CD : $scope.level_path_cd  //자산경로
		};//,'04','05','06','07','08','09','10','11','12'
		mainDataService.getStatLcc(param)
		.success(function(data){
			$scope.statMinwonList = data;
			console.log($scope.statMinwonList);
			var list = ['ROWNUM','C_NAME','COST_CD','tot'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}
				});
			});
			$("#loadingSpinner").hide();
			chartLcc($scope.yearTot,$scope.yearList2);
			
		});		
	};
	$scope.yearList3 = [];
	//최적투자 조회하기
	$scope.selectStateVal = function(item){
		$scope.selectSTAT = item;
	}
	$scope.yearGradeInfo = function(grade){
		var msg ="";
		var year = (new Date()).getFullYear();
		switch(grade){
			case '0' : msg = "우선투자분 (" + (year +1) + '년)'; break;
			case '1' : msg = "" + (year +2 ) +"~" + (year +6) + "년"; break;
			case '2' : msg = "" + (year +7 ) +"~" + (year +11) + "년"; break;
			case '3' : msg = "" + (year +12 ) +"~" + (year +16) + "년"; break;
			case '4' : msg = "" + (year +17 ) +"~" + (year +21) + "년"; break;
			case '5' : msg = "" + (year +22 ) +"~" + (year +26) + "년"; break;
			case '6' : msg = "" + (year +27 ) +"~" + (year +31) + "년"; break;
			case '7' : msg = "" + (year +32 ) +"~" + (year +36) + "년"; break;
			case '8' : msg = "" + (year +37 ) +"~" + (year +41) + "년"; break;
		}
		return msg;
	}
	$scope.searchFinance = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				CHK_MONTH_LIST:chkMonth,
				STAT_TYPE : $scope.selectSTAT, //통계항목
				DAE_CODE: $scope.daeBlock, //대블록
				JUNG_CODE: $scope.jungBlock, //중블록
				SO_CODE: $scope.soBlock, //소블록
				SUB_CODE: $scope.subBlock //관리블록
		};//,'04','05','06','07','08','09','10','11','12'
		mainDataService.getStatOIP(param)
		.success(function(data){
			$scope.selectSTAT_service = $scope.selectSTAT; 
			var CLASS3_CD = data[0].CLASS3_CD; // 컬럼 이름 맞추기 //rowspan
	 		$scope.GB_ARRAY = new Array(); //array scope안해도됨
	 		$scope.GB_ARRAY.push({name:data[0].FIN_ANLS_GUBUN_NM,cnt:1,idx:0});
	 		$.each(data,function(idx,item){
	 			if(data[idx].CLASS3_CD == CLASS3_CD){
	 				if(idx>0)
 					$scope.GB_ARRAY[$scope.GB_ARRAY.length-1].cnt +=1;
	 			}
	 			else{
	 				$scope.GB_ARRAY.push({name:data[idx].CLASS3_CD,cnt:1,idx:idx});	 				
	 			}
	 			CLASS3_CD = data[idx].CLASS3_CD;
	 			item.GB_Idx = $scope.GB_ARRAY.length-1;
	 			item.row_idx = $scope.GB_ARRAY[$scope.GB_ARRAY.length-1].idx;
	 			item.idx = idx;
	 		});
	 		$.each(data,function(idx,item){
	 			item.GB_cnt = $scope.GB_ARRAY[item.GB_Idx].cnt; 
	 		});	
	 		
			var list = ['C_SCODE','C_NAME','FN_CD','CLASS_CD','CLASS3_CD','CLASS4_CD','LEVEL_NM','GB_cnt','GB_Idx','row_idx','idx'];
			$scope.yearList2 = [];
			$scope.yearList3 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//민원유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				//console.log(Item);
				
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						//console.log(Item['\''+yyyy+'\'']);
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = ''; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0;
						if(idx==0) $scope.yearTot2[idx2] = 0;
						var item = (Item['\''+yyyy+'\'']).split("::");
							Item['\''+yyyy+'\''] = item;
							if(idx==1 && $scope.selectSTAT=='D') return;							
							$scope.yearTot[idx2] += parseInt(item[0] || '0'); //문자열을 숫자로
							$scope.yearTot2[idx2] += parseInt(item[1] || '0'); //문자열을 숫자로
							$scope.total += parseInt(item[0]);
							
					}
				});
			});
			
			$.each($scope.yearList2,function(idx2,yyyy){
				$scope.yearList3.push({y:yyyy,t:'cnt'});
				$scope.yearList3.push({y:yyyy,t:'amt'});
			});
			$scope.DataList = data;
			chartFinance($scope.yearTot,$scope.yearList2);
			$("#loadingSpinner").hide();
		});		
	};
	//재정수지 조회하기
	$scope.searchFinancialBalance = function(){
		$("#loadingSpinner").show();
		var chkMonth = [];
		$.each($scope.monthList,function(idx,Item){
			if(Item.isChecked)
			chkMonth.push(('0'+Item.name).substr(-2));
		});
		var param = {
				UNIT:$scope.searchUNIT,// MONTH or YEAR, 
				START_DT: $scope.searchDate.S_DATE.replace(/-/gi,''), // 연단위 시작
				END_DT: ((($scope.searchDate.E_DATE.length>=6)?$scope.searchDate.E_DATE + '31':$scope.searchDate.E_DATE + '1231')).replace(/-/gi,'').substr(0,8),	//연단위 종료
				ITEM_GB : $scope.selectFinanceCategory,
		};
		mainDataService.getStatFinance(param)
		.success(function(data){
			var FIN_ANLS_GUBUN_NM = data[0].FIN_ANLS_GUBUN_NM; // 컬럼 이름 맞추기 //rowspan
	 		$scope.GB_ARRAY = new Array(); //array scope안해도됨
	 		$scope.GB_ARRAY.push({name:data[0].FIN_ANLS_GUBUN_NM,cnt:1,idx:0});
	 		$.each(data,function(idx,item){
	 			if(data[idx].FIN_ANLS_GUBUN_NM == FIN_ANLS_GUBUN_NM){
	 				if(idx>0)
 					$scope.GB_ARRAY[$scope.GB_ARRAY.length-1].cnt +=1;
	 			}
	 			else{
	 				$scope.GB_ARRAY.push({name:data[idx].FIN_ANLS_GUBUN_NM,cnt:1,idx:idx});	 				
	 			}
	 			FIN_ANLS_GUBUN_NM = data[idx].FIN_ANLS_GUBUN_NM;
	 			item.GB_Idx = $scope.GB_ARRAY.length-1;
	 			item.row_idx = $scope.GB_ARRAY[$scope.GB_ARRAY.length-1].idx;
	 			item.idx = idx;
	 		});
	 		$.each(data,function(idx,item){
	 			item.GB_cnt = $scope.GB_ARRAY[item.GB_Idx].cnt; 
	 		});	
	 		
			$scope.statMinwonList = data;
			var list = ['ROWNUM','FIN_ANLS_GUBUN_NM','FIN_ANLS_ITEM_CD','FIN_ANLS_ITEM_NM','FIN_ANLS_TYPE','ACCOUNT_CD','ACCOUNT_NM','tot','GB_Idx','GB_cnt','idx','row_idx'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						$scope.yearList2.push(idx2.replace(/'/gi,''));
				});
				//민원유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			$scope.yearList2.sort();
			$scope.total = 0; // 합계 초기화
			//연도별합계
			$.each(data,function(idx,Item){
				$.each($scope.yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) $scope.yearTot[idx2] = 0
							$scope.yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
							$scope.total += parseInt(Item['\''+yyyy+'\'']);
					}
				});
			});
			$("#loadingSpinner").hide();
			chartFinancialBalance($scope.yearTot,$scope.yearList2);
			
		});		
	};
	
	//자산경로선택팝업
	$scope.showPopupLevelPath = function ()
    {
    	//alert('showpopup')
    	$scope.level_path_nm = "";
    	$("#dialog-assetlevelpath").show();
    	//$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'multi'});
    	$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'path'});
    }
	 $scope.$on('itemlist_selectAssetLevelPath',function(event,data){
	    	console.log(data);
	    	$scope.asset_path_sid = data.ASSET_PATH_SID;
	    	$scope.level_path_cd = data.LEVEL_PATH_CD;
	    	$scope.level_path_nm = data.LEVEL_PATH_NM;
	    });
	 
	 
	 
	 
	 
	//---------------------------------차트------------------------------------- 
	 
	 
	 
	 //자산 통계 차트
	 var chartAsset = function (yearTot,yearList2) {
		 	// 차트 이름, 차트 영역 id
			rMateChartH5.create("chart4", "chartAsset", "", "100%", "100%");

			// 스트링 형식으로 레이아웃 정의.
			var layoutStr =
	             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
	                 +'<Options>'
	                      +'<Legend useVisibleCheck="true" horizontalGap="0" position="bottom" horizontalScrollPolicy="off"/>'
	                 +'</Options>'
	                 +'<NumberFormatter id="numfmt" useThousandsSeparator="true"/>'
	                 +'<SeriesInterpolate id="ss"/>'
	                   +'<Column2DChart showDataTips="true" selectionMode="multiple" columnWidthRatio="0.48">'
	                       +'<horizontalAxis>'
	                           +'<CategoryAxis categoryField="YYYYMM"/>'
	                        +'</horizontalAxis>'
	                      +'<verticalAxis>'
	                         +'<LinearAxis id="vAxis1" interval="NaN" title="자산개수" formatter="{numfmt}"/>'
	                      +'</verticalAxis>'
	                        +'<series>'
	                           +'<Column2DSeries labelPosition="outside" yField="TOTAL" displayName="합계" showDataEffect="{ss}" strokeJsFunction="strokeFunction"/>'
	                       +'</series>'
	                  +'</Column2DChart>'
	               +'</rMateChart>';

			// 차트 데이터
			var chartData = [];
			
			for(var i=0; i<yearList2.length; i++){
				chartData.push({YYYYMM:$scope.DisplayYearMonth(yearList2[i]), TOTAL:yearTot[i]});
			}

			rMateChartH5.calls("chart4", {
				"setLayout" : layoutStr,
				"setData" : chartData
			});
			
			function legendAllCheck(value)
			{
			   document.getElementById("chart4").legendAllCheck(value);
			}
			
			rMateChartH5.registerTheme(rMateChartH5.themes);
			function rMateChartH5ChangeTheme(theme){
			    document.getElementById("chart4").setTheme(theme);
			}
		};
	 
	 
	 
	 //개보수 통계 차트
	 var chartRenovation = function (statMinwonList,yearList2) {
		 	// 차트 이름, 차트 영역 id
			rMateChartH5.create("chart2", "chartRenovation", "", "100%", "100%");

			// 스트링 형식으로 레이아웃 정의.
			var layoutStr =

				'<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
					+'<Options>'
					+'<Legend useVisibleCheck="true"/>' // Legend : 범례 / 체크박스 선택 true
					+'</Options>'
					+'<NumberFormatter id="numFmt1" useThousandsSeparator="true"/>'
					+'<NumberFormatter id="numFmt2" useThousandsSeparator="true"/>'
					+'<Combination2DChart showDataTips="false" dataTipJsFunction="dataTipJsFunc">'
					/*
		                 showDataTips : 데이터에 마우스를 가져갔을 때 나오는 Tip을 보이기/안보이기 속성입니다
		            	true 설정했을 때 에러 발생해서 false 해둠.
		            */
						+'<horizontalAxis>' // 가로축 설정
							+'<CategoryAxis categoryField="YYYYMM" padding="0.5"/>' 
						+'</horizontalAxis>'
						+'<verticalAxis>' // 세로축 설정
							+'<LinearAxis id="vAxis1" minimum="0" interval="NaN" title="사업개수"/>'
						+'</verticalAxis>'

						+'<series>' // series => Column2DSeries(막대), Line2DSeries(라인)
					// 1. Column2DSeries : 차트에 설정된 데이터 처리, 세로 막대 표현
							+'<Column2DSeries labelPosition="outside" yField="TOTAL1" displayName="사업 수">' // displayName : 시리즈 이름 설정, 툴팁에 보임
								+'<fill>'
									+'<SolidColor color="#41b2e6"/>'
								+'</fill>'
								+'<showDataEffect>' // 애니메이션 효과
									+'<SeriesInterpolate/>'
								+'</showDataEffect>'
							+'</Column2DSeries>'
					// 2. Line2DSeries : 차트에 설정된 데이터 처리, 라인 차트 표현
							+'<Line2DSeries form="curve" labelPosition="up" radius="5" yField="TOTAL2" displayName="금액(천원)" itemRenderer="CircleItemRenderer">'
								+'<verticalAxis>'
									+'<LinearAxis id="vAxis2" title="금액(천원)"/>'
								+'</verticalAxis>'
								+'<showDataEffect>'
									+'<SeriesInterpolate/>'
								+'</showDataEffect>'
								+'<lineStroke>'
									+'<Stroke color="#f9bd03" weight="4"/>'
								+'</lineStroke>'
								+'<stroke>'
									+'<Stroke color="#f9bd03" weight="3"/>'
								+'</stroke>'
							+'</Line2DSeries>'
						+'</series>'
					+'<verticalAxisRenderers>'
					+'<Axis2DRenderer axis="{vAxis1}" showLine="false"/>'
					+'<Axis2DRenderer axis="{vAxis2}" showLine="false"/>'
					+'</verticalAxisRenderers>'
					+'</Combination2DChart>'
					+'</rMateChart>';

			// 차트 데이터
			var chartData = [];
			
			for(var i=0; i<yearList2.length; i++){
				chartData.push({YYYYMM:$scope.DisplayYearMonth(yearList2[i]), TOTAL1:statMinwonList[0]['\''+yearList2[i]+'\''], TOTAL2:statMinwonList[1]['\''+yearList2[i]+'\'']});
			}
			
			var maxValue1 = Math.max(chartData.TOTAL1);
			var maxValue2 = Math.max(chartData.TOTAL2);

			rMateChartH5.calls("chart2", {
				"setLayout" : layoutStr,
				"setData" : chartData
			});
			
			function legendAllCheck(value)
			{
			   document.getElementById("chart2").legendAllCheck(value);
			}
			
			rMateChartH5.registerTheme(rMateChartH5.themes);
			function rMateChartH5ChangeTheme(theme){
			    document.getElementById("chart2").setTheme(theme);
			}
		};
	 
		 //조사 및 탐사 차트
		 var chartInspect = function (statMinwonList,yearList2) {
			 	// 차트 이름, 차트 영역 id
				rMateChartH5.create("chart3", "chartInspect", "", "100%", "100%");

				// 스트링 형식으로 레이아웃 정의.
				var layoutStr =

					'<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
						+'<Options>'
						+'<Legend useVisibleCheck="true"/>' // Legend : 범례 / 체크박스 선택 true
						+'</Options>'
						+'<NumberFormatter id="numFmt1"/>'
						+'<NumberFormatter id="numFmt2"/>'
						+'<Combination2DChart showDataTips="false" dataTipJsFunction="dataTipJsFunc">'
						/*
			                 showDataTips : 데이터에 마우스를 가져갔을 때 나오는 Tip을 보이기/안보이기 속성입니다
			            	true 설정했을 때 에러 발생해서 false 해둠.
			            */
							+'<horizontalAxis>' // 가로축 설정
								+'<CategoryAxis categoryField="YYYYMM" padding="0.5"/>' 
							+'</horizontalAxis>'
							+'<verticalAxis>' // 세로축 설정
								+'<LinearAxis id="vAxis1" minimum="0"  interval="NaN" title="사업개수"/>'
							+'</verticalAxis>'

							+'<series>' // series => Column2DSeries(막대), Line2DSeries(라인)
						// 1. Column2DSeries : 차트에 설정된 데이터 처리, 세로 막대 표현
								+'<Column2DSeries labelPosition="outside" yField="TOTAL1" displayName="사업 수">' // displayName : 시리즈 이름 설정, 툴팁에 보임
									+'<fill>'
										+'<SolidColor color="#41b2e6"/>'
									+'</fill>'
									+'<showDataEffect>' // 애니메이션 효과
										+'<SeriesInterpolate/>'
									+'</showDataEffect>'
								+'</Column2DSeries>'
						// 2. Line2DSeries : 차트에 설정된 데이터 처리, 라인 차트 표현
								+'<Line2DSeries form="curve" labelPosition="up" radius="5" yField="TOTAL2" displayName="금액(천원)" itemRenderer="CircleItemRenderer">'
									+'<verticalAxis>'
										+'<LinearAxis id="vAxis2" title="금액(천원)"/>'
									+'</verticalAxis>'
									+'<showDataEffect>'
										+'<SeriesInterpolate/>'
									+'</showDataEffect>'
									+'<lineStroke>'
										+'<Stroke color="#f9bd03" weight="4"/>'
									+'</lineStroke>'
									+'<stroke>'
										+'<Stroke color="#f9bd03" weight="3"/>'
									+'</stroke>'
								+'</Line2DSeries>'
							+'</series>'
						+'<verticalAxisRenderers>'
						+'<Axis2DRenderer axis="{vAxis1}" showLine="false"/>'
						+'<Axis2DRenderer axis="{vAxis2}" showLine="false"/>'
						+'</verticalAxisRenderers>'
						+'</Combination2DChart>'
						+'</rMateChart>';

				// 차트 데이터
				var chartData = [];
				
					for(var i=0; i<yearList2.length; i++){
						chartData.push({YYYYMM:$scope.DisplayYearMonth(yearList2[i]), TOTAL1:statMinwonList[0]['\''+yearList2[i]+'\''], TOTAL2:statMinwonList[1]['\''+yearList2[i]+'\'']});
					}

				rMateChartH5.calls("chart3", {
					"setLayout" : layoutStr,
					"setData" : chartData
				});
				
				function legendAllCheck(value)
				{
				   document.getElementById("chart3").legendAllCheck(value);
				}
				
				rMateChartH5.registerTheme(rMateChartH5.themes);
				function rMateChartH5ChangeTheme(theme){
				    document.getElementById("chart3").setTheme(theme);
				}
			};
		
	 //민원통계차트
	 var chartComplain = function (yearTot,yearList2,data) {
		 	// 차트 이름, 차트 영역 id
			rMateChartH5.create("chart1", "chartComplain", "", "95%", "100%");
			
			// 차트 데이터
			var chartData = [];
			var category = [];
			
			$.each(data,function(idx,Item){
				category[idx] = Item['C_NAME'];
			});			

			// 스트링 형식으로 레이아웃 정의.
			/*
			var layoutStr =
	             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
	                 +'<Options>'
	                      +'<Legend useVisibleCheck="true" horizontalGap="0" position="bottom" horizontalScrollPolicy="off"/>'
	                 +'</Options>'
	                 +'<SeriesInterpolate id="ss"/>'
	                   +'<Column2DChart showDataTips="true" selectionMode="multiple" columnWidthRatio="0.48">'
	                       +'<horizontalAxis>'
	                           +'<CategoryAxis categoryField="YYYYMM"/>'
	                        +'</horizontalAxis>'
	                      +'<verticalAxis>'
	                         +'<LinearAxis maximum="100" interval="10"/>'
	                         +'<LinearAxis id="vAxis1" interval="NaN" title="민원수"/>'
	                      +'</verticalAxis>'
	                        +'<series>'
	                           +'<Column2DSeries labelPosition="outside" yField="TOTAL" displayName="합계" showDataEffect="{ss}" strokeJsFunction="strokeFunction"/>'
	                       +'</series>'
	                  +'</Column2DChart>'
	               +'</rMateChart>';
			*/
			var layoutStr =
	             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
	                   +'<Options>'
	                      +'<Caption text="민원수"/>'
	                        +'<Legend useVisibleCheck="true"/>'
	                   +'</Options>'
	                 +'<NumberFormatter id="numFmt1" useThousandsSeparator="true"/>'
	                   +'<NumberFormatter id="numFmt2" useThousandsSeparator="true"/>'
	                 +'<Combination2DChart showDataTips="true" dataTipJsFunction="dataTipJsFunc">'
	                     +'<horizontalAxis>'
	                           +'<CategoryAxis categoryField="YYYYMM" padding="0.5"/>'
	                        +'</horizontalAxis>'
	                      +'<verticalAxis>'
	                         +'<LinearAxis title="항목별 민원수" id="vAxis1" formatter="{numFmt1}"/>'
	                        +'</verticalAxis>'
	                        +'<series>'
	                           +'<Column2DSet type="clustered">'
	                            +'<series>';

					var shtml ="";
					
					for(var i=0;i<category.length;i++){
                        shtml +='<Column2DSeries yField="type'+i+'" displayName="'+ category[i] + '" labelPosition="outside" showValueLabels="[2]">';
                       	//shtml +='<fill>';
                   		//shtml +='<SolidColor color="#41b2e6"/>';
               			//shtml +='</fill>';
           				shtml +='<showDataEffect>';
       					shtml +='<SeriesInterpolate/>';
   						shtml +='</showDataEffect>';
						shtml +='</Column2DSeries>';						
					}	
            
                     layoutStr += shtml;
                    	 
                   	 layoutStr +='</series>'
	                      +'</Column2DSet>'
	                     +'<Line2DSeries selectable="true" yField="tot" displayName="총계" radius="4.5" form="curve" itemRenderer="CircleItemRenderer">'
	                                +'<stroke>'
	                                   +'<Stroke color="#5587a2" weight="3"/>'
	                               +'</stroke>'
	                              +'<lineStroke>'
	                                   +'<Stroke color="#5587a2" weight="3"/>'
	                               +'</lineStroke>'
	                              +'<verticalAxis  >'
	                                 +'<LinearAxis title="총계 민원수" id="vAxis2" formatter="{numFmt2}" minimum="-5"  interval="10" />'
	                                +'</verticalAxis>'
	                                +'<showDataEffect>'
	                                   +'<SeriesInterpolate/>'
	                               +'</showDataEffect>'
	                          +'</Line2DSeries>'
	                        +'</series>'
	                      +'<verticalAxisRenderers>'
	                            +'<Axis2DRenderer axis="{vAxis1}" showLine="false"/>'
	                         +'<Axis2DRenderer axis="{vAxis2}" showLine="false"/>'
	                     +'</verticalAxisRenderers>'
	                   +'</Combination2DChart>'
	              +'</rMateChart>';

			
			for(var i=0; i<yearList2.length; i++){
				var item = {YYYYMM:$scope.DisplayYearMonth(yearList2[i]), tot:yearTot[i]};
				$.each(data,function(idx,Item){
					item['type'+idx] = Item['\''+yearList2[i]+'\''];
				});
				chartData.push(item);
			}
			var maxValue = Math.max(chartData.tot);
			console.log(maxValue);

			rMateChartH5.calls("chart1", {
				"setLayout" : layoutStr,
				"setData" : chartData
			});
			
			function legendAllCheck(value)
			{
			   document.getElementById("chart1").legendAllCheck(value);
			}
			
			rMateChartH5.registerTheme(rMateChartH5.themes);
			function rMateChartH5ChangeTheme(theme){
			    document.getElementById("chart1").setTheme(theme);
			}
		};
		
		//상태평가 통계 차트
		 var chartState = function (yearTot,yearList2,data) {
			 	// 차트 이름, 차트 영역 id
			 
			 
			 console.log(yearTot);
			 console.log(data);
				// 차트 데이터
				var chartData = [];
				var category = [];
				
				
				$.each(data,function(idx,Item){
					category[idx] = "" + Item['GRADE'] +"등급";
				});	
				
				rMateChartH5.create("chart5", "chartState", "", "100%", "100%");

				// 스트링 형식으로 레이아웃 정의.
				var layoutStr =
		             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
		                 +'<Options>'
		                      +'<Legend useVisibleCheck="true" horizontalGap="0" position="bottom" horizontalScrollPolicy="off"/>'
		                 +'</Options>'
		                 +'<NumberFormatter id="numfmt1" useThousandsSeparator="true"/>'
		                 +'<Combination2DChart showDataTips="true" >'
	                     +'<horizontalAxis>'
						 +'<CategoryAxis categoryField="YYYYMM" padding="0.5"/>'
						 +'</horizontalAxis>'
						 +'<verticalAxis>'
						 +'<LinearAxis title="평가수" id="vAxis1" formatter="{numfmt1}"/>'
						 +'</verticalAxis>'
						 +'<series>'
                         +'<Column2DSet type="clustered">'
                         +'<series>';

						var shtml ="";
						for(var i=0;i<category.length;i++){
	                        shtml +='<Column2DSeries yField="type'+i+'" displayName="'+ category[i] + '" labelPosition="outside" showValueLabels="[2]">';
	                       	//shtml +='<fill>';
	                   		//shtml +='<SolidColor color="#41b2e6"/>';
	               			//shtml +='</fill>';
	           				shtml +='<showDataEffect>';
	       					shtml +='<SeriesInterpolate/>';
	   						shtml +='</showDataEffect>';
							shtml +='</Column2DSeries>';						
						}	
	            
	                     layoutStr += shtml;
	                    	 
	                   	 layoutStr +='</series>'
		                      +'</Column2DSet>'
		                     +'<Line2DSeries selectable="true" yField="tot" displayName="총계" radius="4.5" form="curve" itemRenderer="CircleItemRenderer">'
		                                +'<stroke>'
		                                   +'<Stroke color="#5587a2" weight="3"/>'
		                               +'</stroke>'
		                              +'<lineStroke>'
		                                   +'<Stroke color="#5587a2" weight="3"/>'
		                               +'</lineStroke>'
		                              +'<verticalAxis  >'
		                                 +'<LinearAxis title="총계" id="vAxis2" formatter="{numfmt1}" minimum="-5"  interval="10" />'
		                                +'</verticalAxis>'
		                                +'<showDataEffect>'
		                                   +'<SeriesInterpolate/>'
		                               +'</showDataEffect>'
		                          +'</Line2DSeries>'
		                        +'</series>'
		                      +'<verticalAxisRenderers>'
		                            +'<Axis2DRenderer axis="{vAxis1}" showLine="false"/>'
		                         +'<Axis2DRenderer axis="{vAxis2}" showLine="false"/>'
		                     +'</verticalAxisRenderers>'
		                   +'</Combination2DChart>'
		              +'</rMateChart>';
	                   	 

				// 차트 데이터

	         			for(var i=0; i<yearList2.length; i++){
	        				var item = {YYYYMM:$scope.DisplayYearMonth(yearList2[i]), tot:yearTot[i]};
	        				$.each(data,function(idx,Item){
	        					item['type'+idx] = Item['\''+yearList2[i]+'\''];
	        				});
	        				chartData.push(item);
	        			}
	        			var maxValue = Math.max(chartData.tot);
	        			console.log(maxValue);

				rMateChartH5.calls("chart5", {
					"setLayout" : layoutStr,
					"setData" : chartData
				});
				
				function legendAllCheck(value)
				{
				   document.getElementById("chart5").legendAllCheck(value);
				}
				
				rMateChartH5.registerTheme(rMateChartH5.themes);
				function rMateChartH5ChangeTheme(theme){
				    document.getElementById("chart5").setTheme(theme);
				}
			};
			
			
			//잔존수명 통계 차트
			 var chartRemain = function (yearTot,yearList2) {
				 	// 차트 이름, 차트 영역 id
					rMateChartH5.create("chart6", "chartRemain", "", "100%", "100%");

					// 스트링 형식으로 레이아웃 정의.
					var layoutStr =
			             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
			                 +'<Options>'
			                      //+'<Legend useVisibleCheck="false" horizontalGap="0" position="bottom" horizontalScrollPolicy="off"/>'
			                 +'</Options>'
			                 +'<NumberFormatter id="numfmt4" useThousandsSeparator="true"/>'
			                 +'<SeriesInterpolate id="ss"/>'
			                   +'<Column2DChart showDataTips="true" selectionMode="multiple" columnWidthRatio="0.48">'
			                       +'<horizontalAxis>'
			                           +'<CategoryAxis categoryField="YYYYMM" title="잔존수명"/>'
			                        +'</horizontalAxis>'
			                      +'<verticalAxis>'
			                         +'<LinearAxis maximum="10000" interval="10"/>'
			                         +'<LinearAxis id="vAxis1" interval="NaN" title="자산개수" formatter="{numfmt4}"/>'
			                      +'</verticalAxis>'
			                        +'<series>'
			                           +'<Column2DSeries labelPosition="outside" yField="TOTAL" displayName="합계" showDataEffect="{ss}" strokeJsFunction="strokeFunction"/>'
			                       +'</series>'
			                  +'</Column2DChart>'
			               +'</rMateChart>';

					// 차트 데이터
					var chartData = [];
					
					for(var i=0; i<yearList2.length; i++){
						chartData.push({YYYYMM:yearList2[i], TOTAL:yearTot[i]});
					}
					var maxValue = Math.max(chartData.TOTAL);
					console.log(maxValue);

					rMateChartH5.calls("chart6", {
						"setLayout" : layoutStr,
						"setData" : chartData
					});
					
					function legendAllCheck(value)
					{
					   document.getElementById("chart6").legendAllCheck(value);
					}
					
					rMateChartH5.registerTheme(rMateChartH5.themes);
					function rMateChartH5ChangeTheme(theme){
					    document.getElementById("chart6").setTheme(theme);
					}
				};
				
				
			//위험도 통계 차트2
				
				 var chartRisk = function (chartData,legend) {
				//var chartRisk = function (yearTot,yearList2) {
					 	// 차트 이름, 차트 영역 id
						rMateChartH5.create("chart7", "chartRisk", "", "100%", "100%");

						// 스트링 형식으로 레이아웃 정의.
						var layoutStr =
							 '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
							       +'<Options>'
							         +'<Legend useVisibleCheck="true" defaultMouseOverAction="false" />'
							       +'</Options>'
							       +'<NumberFormatter id="numfmt4" useThousandsSeparator="true"/>'
							     +'<Column2DChart showDataTips="true" columnWidthRatio="0.4">'
							         +'<horizontalAxis>'
							               +'<CategoryAxis categoryField="yyyy"/>'
							          +'</horizontalAxis>'
							          +'<verticalAxis>'
							          	+'<LinearAxis id="vAxis1" interval="NaN" title="자산개수" formatter="{numfmt4}"/>'
							          +'</verticalAxis>'
							            +'<series>'
							               /*
							              type 속성을 stacked로 변경
							                type속성으로는
							               clustered : 일반적인 다중데이터(차트의 멀티시리즈)방식으로 데이터를 표현합니다.(Default)
							              stacked : 데이터를 위에 쌓아 올린 방식으로 표현 합니다.
							                overlaid : 수치 데이터 값을 겹쳐서 표현 합니다. 주로 목표 위치와 현재 위치를 나타낼 때 많이 쓰입니다.
							                100% : 차트의 수치 데이터를 퍼센티지로 계산 후 값을 퍼센티지로 나타냅니다.
							               */
							            +'<Column2DSet type="clustered">'
							              //+'<Column2DSet type="stacked" showTotalLabel="true" totalLabelJsFunction="totalFunc">'
							                    +'<series>';
							               /*  Column2D Stacked 를 생성시에는 Column2DSeries를 최소 2개 정의합니다 */
											for (var i = 0; i < legend.length; i++) {
												layoutStr += 
												'<Column2DSeries labelPosition="outside" yField="CATEGORY'+i+'" displayName="'+legend[i]+'" color="#000000" strokeJsFunction="strokeFunction">'
								                      +'<showDataEffect>'
								                      		+'<SeriesInterpolate/>'
								                      +'</showDataEffect>'
							                      +'</Column2DSeries>';
											}
							                      /*+'<Column2DSeries labelPosition="inside" yField="CATEGORY0" displayName="'+MSG0+'" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      +'<showDataEffect>'
							                      +'<SeriesInterpolate/>'
							                      +'</showDataEffect>'
							                      +'</Column2DSeries>'*/
							                      /*+'<Column2DSeries labelPosition="inside" yField="CATEGORY1" displayName="CATEGORY.MSG" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      		+'<showDataEffect>'
							                      			+'<SeriesInterpolate/>'
							                      		+'</showDataEffect>'
							                      +'</Column2DSeries>'
							                      +'<Column2DSeries labelPosition="inside" yField="CATEGORY2" displayName="CATEGORY.MSG" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      		+'<showDataEffect>'
							                      			+'<SeriesInterpolate/>'
							                      		+'</showDataEffect>'
							                      +'</Column2DSeries>'
							                      +'<Column2DSeries labelPosition="inside" yField="CATEGORY3" displayName="CATEGORY.MSG" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      +'<showDataEffect>'
							                      +'<SeriesInterpolate/>'
							                      +'</showDataEffect>'
							                      +'</Column2DSeries>'
							                      +'<Column2DSeries labelPosition="inside" yField="CATEGORY4" displayName="CATEGORY.MSG" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      +'<showDataEffect>'
							                      +'<SeriesInterpolate/>'
							                      +'</showDataEffect>'
							                      +'</Column2DSeries>'
							                      +'<Column2DSeries labelPosition="inside" yField="CATEGORY5" displayName="CATEGORY.MSG" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      +'<showDataEffect>'
							                      +'<SeriesInterpolate/>'
							                      +'</showDataEffect>'
							                      +'</Column2DSeries>'
							                      +'<Column2DSeries labelPosition="inside" yField="CATEGORY6" displayName="CATEGORY.MSG" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      +'<showDataEffect>'
							                      +'<SeriesInterpolate/>'
							                      +'</showDataEffect>'
							                      +'</Column2DSeries>'
							                      +'<Column2DSeries labelPosition="inside" yField="CATEGORY7" displayName="CATEGORY.MSG" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      +'<showDataEffect>'
							                      +'<SeriesInterpolate/>'
							                      +'</showDataEffect>'
							                      +'</Column2DSeries>'
							                      +'<Column2DSeries labelPosition="inside" yField="CATEGORY8" displayName="CATEGORY.MSG" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      +'<showDataEffect>'
							                      +'<SeriesInterpolate/>'
							                      +'</showDataEffect>'
							                      +'</Column2DSeries>'
							                      +'<Column2DSeries labelPosition="inside" yField="CATEGORY9" displayName="CATEGORY.MSG" color="#ffffff" strokeJsFunction="strokeFunction">'
							                      +'<showDataEffect>'
							                      +'<SeriesInterpolate/>'
							                      +'</showDataEffect>'
							                      +'</Column2DSeries>'*/
							                      layoutStr += ''
							                  +'</series>'
							              +'</Column2DSet>'
							         +'</series>'
							      +'</Column2DChart>'
							   +'</rMateChart>';


						// 스택 수치 합 사용자 정의 함수
						function totalFunc(index, data, value){
						  if(index == 6)
						      return insertComma(value);
						  return "";
						}
						 
						//숫자에 천단위 콤마 찍어 반환하는 함수.
						function insertComma(n) {
						    var reg = /(^[+-]?\d+)(\d{3})/; // 정규식
						  n += "";
						    while (reg.test(n))
						 n = n.replace(reg, '$1' + "," + '$2');
						  return n;
						}

						rMateChartH5.calls("chart7", {
							"setLayout" : layoutStr,
							"setData" : chartData
						});
						
						function legendAllCheck(value)
						{
						   document.getElementById("chart7").legendAllCheck(value);
						}
						
						rMateChartH5.registerTheme(rMateChartH5.themes);
						function rMateChartH5ChangeTheme(theme){
						    document.getElementById("chart7").setTheme(theme);
						}
					};
				
				
			//위험도 통계 차트
			/*
				 var chartRisk = function (yearTot,yearList2) {
					 	// 차트 이름, 차트 영역 id
						rMateChartH5.create("chart7", "chartRisk", "", "100%", "100%");

						// 스트링 형식으로 레이아웃 정의.
						var layoutStr =
				             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
				                 +'<Options>'
				                      +'<Legend useVisibleCheck="true" horizontalGap="0" position="bottom" horizontalScrollPolicy="off"/>'
				                 +'</Options>'
				                 +'<NumberFormatter id="numfmt4" useThousandsSeparator="true"/>'
				                 +'<SeriesInterpolate id="ss"/>'
				                   +'<Column2DChart showDataTips="true" selectionMode="multiple" columnWidthRatio="0.48">'
				                       +'<horizontalAxis>'
				                           +'<CategoryAxis categoryField="YYYYMM"/>'
				                        +'</horizontalAxis>'
				                      +'<verticalAxis>'
				                         +'<LinearAxis maximum="100000" interval="10"/>'
				                         +'<LinearAxis id="vAxis1" interval="NaN" title="자산개수" formatter="{numfmt4}"/>'
				                      +'</verticalAxis>'
				                        +'<series>'
				                           +'<Column2DSeries labelPosition="outside" yField="TOTAL" displayName="합계" showDataEffect="{ss}" strokeJsFunction="strokeFunction"/>'
				                       +'</series>'
				                  +'</Column2DChart>'
				               +'</rMateChart>';

						// 차트 데이터
						var chartData = [];
						
						for(var i=0; i<yearList2.length; i++){
							chartData.push({YYYYMM:yearList2[i], TOTAL:yearTot[i]});
						}
						var maxValue = Math.max(chartData.TOTAL);
						console.log(maxValue);

						rMateChartH5.calls("chart7", {
							"setLayout" : layoutStr,
							"setData" : chartData
						});
						
						function legendAllCheck(value)
						{
						   document.getElementById("chart7").legendAllCheck(value);
						}
						
						rMateChartH5.registerTheme(rMateChartH5.themes);
						function rMateChartH5ChangeTheme(theme){
						    document.getElementById("chart7").setTheme(theme);
						}
					};*/
				
			//서비스 수준 통계 차트
					var chartLos = function (chartData) {
					 	// 차트 이름, 차트 영역 id
						rMateChartH5.create("chart8", "chartLos", "", "100%", "100%");

						// 스트링 형식으로 레이아웃 정의.
						var layoutStr =
							  '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
			                   +'<Options>'
			                      +'<Legend  defaultMouseOverAction="true"/>'
			                   +'</Options>' /*
			              레이더차트의 시리즈 개수에 맞게 보기 좋게 보이기 위해 시작각도를 조정합니다.
			             시작각도(startAngle)는 원점을 기준으로 3시 방향이 0 도로 시계 방향으로 진행됩니다.
			               5각형 모양의 레이더이므로 보기 좋게 하기 위해 270도 회전시켜 출력함
			                */
			                  +'<RadarChart isSeriesOnAxis="true" type="polygon" paddingTop="20" paddingBottom="20" showDataTips="true" startAngle="270" dataTipJsFunction="dataTipFunc">'
			              
			                              +'<radialAxis>'
			                                   +'<LinearAxis id="rAxis"/>'
			                               +'</radialAxis>'
			                              +'<angularAxis>'
			                                  +'<CategoryAxis id="aAxis" categoryField="CATEGORY" displayName="Category"/>'
			                              +'</angularAxis>'
			                              +'<radialAxisRenderers>'
			                              /* radialAxis렌더러 정의 */
			                              /* 세로축만 표시 */
			                                  +'<Axis2DRenderer axis="{rAxis}" horizontal="false" visible="true" tickPlacement="outside" tickLength="4">'
			                                       +'<axisStroke>'
			                                           +'<Stroke color="#555555" weight="1"/>'
			                                       +'</axisStroke>'
			                                  +'</Axis2DRenderer>'
			                              +'</radialAxisRenderers>'
			                             +'<angularAxisRenderers>'
			                                 +'<AngularAxisRenderer axis="{aAxis}"/>'
			                              +'</angularAxisRenderers>'
			                              +'<series>'
			                             +'<RadarSeries id="rs1" field="YEARLATEST" displayName="가장최근년도" labelPosition="outside">'
			                                 +'<showDataEffect>'
			                                       +'<SeriesInterpolate/>'
			                                  +'</showDataEffect>'
			                              +'</RadarSeries>'
			                             +'<RadarSeries id="rs2" field="YEARBEFOR" displayName="가장최근년도이전" labelPosition="outside">'
			                                    +'<showDataEffect>'
			                                       +'<SeriesInterpolate/>'
			                                   +'</showDataEffect>'
			                              +'</RadarSeries>'
			                           +'</series>'
			                        +'</RadarChart>'
			              +'</rMateChart>';

						rMateChartH5.calls("chart8", {
							"setLayout" : layoutStr,
							"setData" : chartData
						});
						
						function legendAllCheck(value)
						{
						   document.getElementById("chart8").legendAllCheck(value);
						}
						
						rMateChartH5.registerTheme(rMateChartH5.themes);
						function rMateChartH5ChangeTheme(theme){
						    document.getElementById("chart8").setTheme(theme);
						}
					};
		
			//생애주기관리 통계 차트
				
			var chartLcc = function (yearTot,yearList2) {
				// 차트 이름, 차트 영역 id
				rMateChartH5.create("chart9", "chartLcc", "", "100%", "100%");

				// 스트링 형식으로 레이아웃 정의.
				var layoutStr =
		             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
		                 +'<Options>'
		                      +'<Legend useVisibleCheck="true" horizontalGap="0" position="bottom" horizontalScrollPolicy="off"/>'
		                 +'</Options>'
		                 +'<NumberFormatter id="numfmt4" useThousandsSeparator="true"/>'
		                 +'<SeriesInterpolate id="ss"/>'
		                   +'<Column2DChart showDataTips="true" selectionMode="multiple" columnWidthRatio="0.48">'
		                       +'<horizontalAxis>'
		                           +'<CategoryAxis categoryField="YYYYMM"/>'
		                        +'</horizontalAxis>'
		                      +'<verticalAxis>'
		                         +'<LinearAxis maximum="1000000000" interval="10"/>'
		                         +'<LinearAxis id="vAxis1" interval="NaN" title="금액" formatter="{numfmt4}"/>'
		                      +'</verticalAxis>'
		                        +'<series>'
		                           +'<Column2DSeries labelPosition="outside" yField="TOTAL" displayName="합계" showDataEffect="{ss}" strokeJsFunction="strokeFunction"/>'
		                       +'</series>'
		                  +'</Column2DChart>'
		               +'</rMateChart>';

				// 차트 데이터
				var chartData = [];
				
				for(var i=0; i<yearList2.length; i++){
					chartData.push({YYYYMM:yearList2[i], TOTAL:yearTot[i]});
				}
				var maxValue = Math.max(chartData.TOTAL);
				console.log(maxValue);

				rMateChartH5.calls("chart9", {
					"setLayout" : layoutStr,
					"setData" : chartData
				});
				
				function legendAllCheck(value)
				{
				   document.getElementById("chart9").legendAllCheck(value);
				}
				
				rMateChartH5.registerTheme(rMateChartH5.themes);
				function rMateChartH5ChangeTheme(theme){
				    document.getElementById("chart9").setTheme(theme);
				}
			};
			
			//최적투자 통계 차트
			
			var chartFinance = function (yearTot,yearList2) {
				// 차트 이름, 차트 영역 id
				rMateChartH5.create("chart10", "chartFinance", "", "100%", "100%");

				// 스트링 형식으로 레이아웃 정의.
				var layoutStr =
		             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
		                 +'<Options>'
		                      +'<Legend useVisibleCheck="true" horizontalGap="0" position="bottom" horizontalScrollPolicy="off"/>'
		                 +'</Options>'
		                 +'<NumberFormatter id="numfmt4" useThousandsSeparator="true"/>'
		                 +'<SeriesInterpolate id="ss"/>'
		                   +'<Column2DChart showDataTips="true" selectionMode="multiple" columnWidthRatio="0.48">'
		                       +'<horizontalAxis>'
		                           +'<CategoryAxis categoryField="YYYYMM"/>'
		                        +'</horizontalAxis>'
		                      +'<verticalAxis>'
		                         +'<LinearAxis maximum="500000" interval="10"/>'
		                         +'<LinearAxis id="vAxis1" interval="NaN" title="개대체비용(천원)" formatter="{numfmt4}"/>'
		                      +'</verticalAxis>'
		                        +'<series>'
		                           +'<Column2DSeries labelPosition="outside" yField="TOTAL" displayName="합계" showDataEffect="{ss}" strokeJsFunction="strokeFunction"/>'
		                       +'</series>'
		                  +'</Column2DChart>'
		               +'</rMateChart>';

				// 차트 데이터
				var chartData = [];
				console.log($scope.yearTot);
				for(var i=0; i<yearList2.length; i++){
					chartData.push({YYYYMM:yearList2[i], TOTAL:yearTot[i]});
				}
				var maxValue = Math.max(chartData.TOTAL);
				console.log(maxValue);
				/*var verticalTitle = '자산개수';
				if($scope.selectSTAT == 'D'){
					verticalTitle = '금액';
				}
				console.log(verticalTitle);*/

				rMateChartH5.calls("chart10", {
					"setLayout" : layoutStr,
					"setData" : chartData
				});
				
				function legendAllCheck(value)
				{
				   document.getElementById("chart10").legendAllCheck(value);
				}
				
				rMateChartH5.registerTheme(rMateChartH5.themes);
				function rMateChartH5ChangeTheme(theme){
				    document.getElementById("chart10").setTheme(theme);
				}
			};			
			//재정수지 통계 차트
	    
			var chartFinancialBalance = function (yearTot,yearList2) {
				// 차트 이름, 차트 영역 id
				rMateChartH5.create("chart11", "chartFinancialBalance", "", "100%", "100%");

				// 스트링 형식으로 레이아웃 정의.
				var layoutStr =
		             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
		                 +'<Options>'
		                      +'<Legend useVisibleCheck="true" horizontalGap="0" position="bottom" horizontalScrollPolicy="off"/>'
		                 +'</Options>'
		                 +'<NumberFormatter id="numfmt4" useThousandsSeparator="true"/>'
		                 +'<SeriesInterpolate id="ss"/>'
		                   +'<Column2DChart showDataTips="true" selectionMode="multiple" columnWidthRatio="0.48">'
		                       +'<horizontalAxis>'
		                           +'<CategoryAxis categoryField="YYYYMM"/>'
		                        +'</horizontalAxis>'
		                      +'<verticalAxis>'
		                         +'<LinearAxis maximum="500000" interval="10"/>'
		                         +'<LinearAxis id="vAxis1" interval="NaN" title="금액" formatter="{numfmt4}"/>'
		                      +'</verticalAxis>'
		                        +'<series>'
		                           +'<Column2DSeries labelPosition="outside" yField="TOTAL" displayName="합계" showDataEffect="{ss}" strokeJsFunction="strokeFunction"/>'
		                       +'</series>'
		                  +'</Column2DChart>'
		               +'</rMateChart>';

				// 차트 데이터
				var chartData = [];
				
				for(var i=0; i<yearList2.length; i++){
					chartData.push({YYYYMM:yearList2[i], TOTAL:yearTot[i]});
				}
				var maxValue = Math.max(chartData.TOTAL);
				console.log(maxValue);

				rMateChartH5.calls("chart11", {
					"setLayout" : layoutStr,
					"setData" : chartData
				});
				
				function legendAllCheck(value)
				{
				   document.getElementById("chart11").legendAllCheck(value);
				}
				
				rMateChartH5.registerTheme(rMateChartH5.themes);
				function rMateChartH5ChangeTheme(theme){
				    document.getElementById("chart11").setTheme(theme);
				}
			};
}