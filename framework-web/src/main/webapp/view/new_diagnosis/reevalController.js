angular.module('app.diagnosis').controller('reevalController', reevalController);
angular.module('app.diagnosis').controller('reeval_popupController', reeval_popupController);
angular.module('app.diagnosis').controller('reeval_popup3Controller', reeval_popup3Controller);
angular.module('app.diagnosis').controller('reeval_popup4Controller', reeval_popup4Controller);
angular.module('app.diagnosis').controller('basic_popupController', basic_popupController);

function reevalController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, $filter, MaxUploadSize) {

	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 6개월
        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.Serch.Info.START_DT = s_yyyymmdd;
        $('#Serch_START_DT').val(s_yyyymmdd);
        $scope.Serch.Info.END_DT = e_yyyymmdd;
        $('#Serch_END_DT').val(e_yyyymmdd);
    }
	
	 $scope.$on('$viewContentLoaded', function () {
	    	$("#tab1_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab3_btn").removeClass("active");
	    		$("#tab1").show();
	    		$("#tab2, #tab3").hide();
	    	});
	    	$("#tab2_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab1_btn, #tab3_btn").removeClass("active");
	    		$("#tab2").show();
	    		$("#tab1, #tab3").hide();
	    	});
	    	$("#tab3_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab1_btn").removeClass("active");
	    		$("#tab3").show();
	    		$("#tab1, #tab2").hide();
	    	});
	    	$("#tab1_1_btn").on("click", function() {
		   		$(this).addClass("active");
		   		$("#tab1_2_btn, #tab1_3_btn").removeClass("active");
		   		$("#tab1_1").show();
		   		$("#tab1_2, #tab1_3").hide();
		   	});
		   	$("#tab1_2_btn").on("click", function() {
		   		$(this).addClass("active");
		   		$("#tab1_1_btn, #tab1_3_btn").removeClass("active");
		   		$("#tab1_2").show();
		   		$("#tab1_1, #tab1_3").hide();
		   	});
		   	$("#tab1_3_btn").on("click", function() {
		   		$(this).addClass("active");
		   		$("#tab1_2_btn, #tab1_1_btn").removeClass("active");
		   		$("#tab1_3").show();
		   		$("#tab1_1, #tab1_2").hide();
		   	});
	    	gridResize();
//	    	$rootScope.setBtnAuth($scope.MCODE);
//	    	$scope.MCODE = '0307';
//	    	console.log(sessionStorage);
	    	//달력
	    	setDatePicker();
	    	$scope.setWeek();
	    	
	    	//class_cd 정보
 			mainDataService.getAssetLevelList({}).success(function(data){
 	 	 		 $scope.assetLevelList  = data
 	 	 		 
 	 	 		 //대,중,소 정보
 	 	 		 mainDataService.getBlockCodeList({}).success(function (obj) {
 	 	  			if (!common.isEmpty(obj.errorMessage)) {
 	 	  				alertify.error('errMessage : ' + obj.errorMessage);
 	 	  				return;
 	 	  			}
 	  				$scope.blockList = obj;
 	  				
 	  				//그리드 초기화
 	  				pagerJsonGrid(Models[0]);
 	 	  		});
 	 	 	 });
	    	
	    	//조회 
	    	Common_Serch.Init('#list', $state.current.name, Common_Time.toDay(),function() {
				 let Model = Models[0];
				 let id = '#'+ Model.grid_id;
				 alertify.confirm('삭제 확인','작성하신 자료를 삭제 하시겠습니까?', function() {
					 //Evaluation_Management.Info.DEL_YN = 'Y';
					 let param = angular.copy(Evaluation_Management.Info);
					 param.DEL_YN = 'Y';
					 mainDataService
						 .CommunicationRelay("/reeval/delete2.json",param)
						 .success(function(SID) {
							 if(Common_AlertStr(SID)) return;
							 
							 $(id).jqGrid('setGridParam', {postData : Common_Serch.Info, page : 1}).trigger('reloadGrid');
						 });
				 }, function() {});
			 });
	    	//첫화면 초기화
	    	mainDataService.CommunicationRelay("/reeval/getList.json",Common_Serch.Info).success(function(SID) {
	    		Models[1].condition.RE_EST_SID = -1;
	    		if(SID.rows[0]){
	    			Models[1].condition.RE_EST_SID = SID.rows[0].RE_EST_SID;
	    		}
	    		pagerJsonGrid(Models[1]);
	    	});
	    	
	    	//물가지수 초기화
	    	Price_Index.Trigger();
	    	//가중치 기준 관리
	    	Weight_Standard.Trigger(0);
	    	  
	 	   Common_Modal.Init($compile, $scope, {
	 			id: '#list2',
	 			layerId: '#modal',
	 			hidenSize: 0
	 	 	});
	    });
	  //숫자 0 
	 let intFormat =  function( cellvalue , options , rowdata )  { 
			return  cellvalue === null ? '' : $filter('number')(cellvalue,0);
	  }
	 //숫자 0.0 
	 let doubleFormat =  function( cellvalue , options , rowdata )  { 
			return  cellvalue === null ? '' : $filter('number')(cellvalue, 2);
	 }
	 let Models = [
		 {
            grid_id : 'list',
            pager_id : 'listPager',
            url : '/reeval/getList.json',
            condition : {
                page : 1,
                rows : GridConfig.sizeL
            },
            rowNum : 20,
            colNames : ['순번', '평가명', '평가기간', '가중치이름', '가중치sid', '작성자', '작성일자','sid', 'temp', '년도', '월'],
            colModel : [
	            {name : 'RNUM', width : "40px", sortable:false, resizable: false}, 
	            {name : 'EST_NM', width : 150, resizable: false}, 
	            {name : 'EST_YYYYMM', width : 80, resizable: false}, 
	            {name : 'WEIGHT_SPEC_NM', width : 0,hidden : true}, 
	            {name : 'WEIGHT_SPEC_SID', width : 0,hidden : true}, 
	            {name : 'INSERT_ID', width : 80, resizable: false}, 
	            {name : 'INSERT_DT', width : 80, resizable: false}, 
	            {name : 'RE_EST_SID', width : 0, hidden : true},
	            {name : 'TEMP_YN', width : 0, hidden : true},
	            {name : 'EST_YYYY', width : 0, hidden : true},
	            {name : 'EST_MM', width : 0, hidden : true},
            ],
            onSelectRow : function(id) {
                Common_Swap.B_A($('#list').jqGrid('getRowData', id), Evaluation_Management.Info);
                if($('#list').jqGrid('getRowData', id).WEIGHT_SPEC_NM == '')
                Evaluation_Management.Info.WEIGHT_SPEC_NM = '선택없음';
                
                let Model = Models[1];
                Common_Swap.B_A(Evaluation_Management.Info, Model.condition);
                $('#'+ Model.grid_id).jqGrid('setGridParam', {postData : Model.condition, page : 1}).trigger('reloadGrid');	
                $scope.$apply();
                
                var param1 = {TAB_ID:1,RE_EST_SID:Evaluation_Management.Info.RE_EST_SID};
                var param2 = {TAB_ID:2,RE_EST_SID:Evaluation_Management.Info.RE_EST_SID};
                $scope.tot1G1 = 0;
                $scope.tot1G2 = 0;
                $scope.tot1G3 = 0;
                $scope.tot1G4 = 0;
                $scope.tot2G1 = 0;
                $scope.tot2G2 = 0;
                $scope.tot2G3 = 0;
                $scope.tot2G4 = 0;
                mainDataService.getReEvalAssetGroup(param1)
                .success(function(data){
                	console.log(data);
                	$scope.AssetGroupList1 = data;
                	$.each(data,function(idx,Item){
                		$scope.tot1G1 += Item.G1;
                		$scope.tot1G2 += Item.G2;
                		$scope.tot1G3 += Item.G3;
                		$scope.tot1G4 += Item.G4;
                	});
                });

                mainDataService.getReEvalAssetGroup(param2)
                .success(function(data){
                	console.log(data);
                	$scope.AssetGroupList2 = data;
                	$.each(data,function(idx,Item){
                		$scope.tot2G1 += Item.G1;
                		$scope.tot2G2 += Item.G2;
                		$scope.tot2G3 += Item.G3;
                		$scope.tot2G4 += Item.G4;
                	});                	
                });                
            },
            loadComplete: function(data) {
        		//첫번째 로우 선택
        		var rowIds = $('#list').jqGrid('getDataIDs');
        		if(rowIds && rowIds.length > 0){
        			$('#list').jqGrid("setSelection", rowIds[0]);
        		}
        	}
        },
        {
            grid_id : 'list2',
            pager_id : 'listPager2',
            url : '/reeval/getListAsset.json',
            condition : {
            	RE_EST_SID : 0,
            	CLASS3_CD: '',
            	CLASS4_CD:'', 
                page : 1,
                rows : GridConfig.sizeS
            },
            rowNum : 11,
            colNames : [ '순번','자산코드','자산명','재평가','재조달원가','잔존수명','내용연수','물가배수','가중치','가중치SID','재평가SID','자산SID'],
            colModel : [
            {name : 'RNUM', width : 53, resizable: false}, 
            {name : 'ASSET_CD', width : "200px", resizable: false}, 
            {name : 'ASSET_NM', width : 0, resizable: false}, 
            {name : 'RE_EST_AMT', width : 0, resizable: false, formatter: intFormat}, 
            {name : 'REPURCHASE_AMT',width : 0, resizable: false, formatter: intFormat}, 
            {name : 'LIFE_NUM', width : 50, resizable: false, formatter: intFormat}, 
            {name : 'USEFUL_LIFE',width : 50, resizable: false, formatter: intFormat}, 
            {name : 'PRICE_INDEX',width : 50, resizable: false, formatter: doubleFormat},
            {name : 'WEIGHT_NUM',width : 50, resizable: false, formatter: doubleFormat},
            {name : 'WEIGHT_SPEC_SID',width : 0,hidden : true},
            {name : 'RE_EST_SID',width : 0,hidden : true},
            {name : 'ASSET_SID',width : 0,hidden : true}
            ],
            loadComplete: function() {
            	$("#loadingSpinner").hide();
            }
        }
	]
	 
	 
	 let Evaluation_Management = {
		 Info : {
			 RE_EST_SID : '',
			 EST_NM : '',
			 EST_YYYY : '',
			 EST_MM : '',
			 WEIGHT_SPEC_NM : '',
			 WEIGHT_SPEC_SID : '',
			 DEL_YN : '',
			 INSERT_ID : '',
			 INSERT_DT : '',
			 CLASS3_CD: '',
			 CLASS4_CD: ''
		 },
		 Triger : function(year) {
			 //화면 초기화
			 let Model = Models[0];
			 let id = '#'+ Model.grid_id;
			 console.log(year)
			 Common_Serch.Info.YEAR = '';//year.substr(0,4);
			 console.log(Common_Serch.Info.YEAR)
			 $(id).jqGrid('setGridParam', {postData : Common_Serch.Info, page : 1}).trigger('reloadGrid');
		 },
		 New : function() {
			 //추가 팝업 오픈
			 $rootScope.$broadcast("reeval_popup",{
				 "OptionList" : Weight_Standard.WeightList,
				 "Triger" : this.Triger
			 });
		 },
		 Report : function() {
			 let jobType = $state.current.name + "_big"
			 console.log(jobType);
			 window.open('/bigexcel/downloadBigExcel.do?EXCEL_ID='+jobType+'&RE_EST_SID=' + this.Info.RE_EST_SID,'_blank');
		 },
		 Check: function() {
	 			$('#list2').jqGrid('setGridParam', {postData : Evaluation_Management.Info, page : 1}).trigger('reloadGrid');
		 }
	 }
	 
	 let Price_Index = {
		Info : {
			BASIC_ITEM_NM: "",
			BASIC_NM: "",
			BASIC_YEAR: "",
			INSERT_DT: "",
			INSERT_ID: ""
		},
		List : null,
		Trigger : function() {
			//화면 초기화
			mainDataService.CommunicationRelay("/reeval/getPriceIndex.json").success(function(map) {
	    		Price_Index.List = map.array;
	    		Price_Index.Info = map.basic;
	    	});
		},
		//수정/등록
	 	Modify : function() {
	 		//수정 팝업 오픈
	 		$rootScope.$broadcast("reeval_popup3",{
	 			"TITLE" : "물가지수 수정",
	 			"Info" : this.Info,
	 			"List" : this.List,
	 			"Trigger" : this.Trigger
			});
	 	}
	 }
	 
	 let Weight_Standard = {
		Info : {
			 WEIGHT_SPEC_SID : '',
			 WEIGHT_SPEC_NM : '',
		},
		List : null,
		WeightList : null,
		Trigger : function(index) {
			console.log(index)
			//화면 초기화
			mainDataService.CommunicationRelay("/reeval/getWeightSpec.json",{"index" : index}).success(function(map) {
	    		Weight_Standard.List = map.List;
	    		Weight_Standard.Info = map.Info;
	    		Weight_Standard.Info.INDEX = index;
	    		Weight_Standard.WeightList = map.WeightList;
	    		let Model = Models[1];
				let id = '#'+ Model.grid_id;
				$(id).jqGrid('setGridParam', {postData : Model.condition, page : 1}).trigger('reloadGrid');
	    	});
		},
	 	New : function() {
	 		//추가 팝업
	 		$rootScope.$broadcast("reeval_popup4",{
	 			"TITLE":"가중치 평가기준 등록",
	 			"List" : [],
	 			"Info" : {WEIGHT_SPEC_NM: '', WEIGHT_SPEC_SID : -1,INSERT_ID : sessionStorage.getItem('loginId'), INSERT_DT : Common_Time.toDay().START_DT , INDEX : 0},
	 			"Trigger" : this.Trigger
			});
	 	},
	 	Modify : function() {
	 		//수정 팝업
	 		let info = angular.copy(this.Info);
	 		info.UPDATE_ID = sessionStorage.getItem('loginId');
	 		info.UPDATE_DT = Common_Time.toDay().START_DT;
	 		
	 		$rootScope.$broadcast("reeval_popup4", {
	 			"TITLE":"가중치 평가기준 수정",
	 			"List" : this.List,
	 			"Info" : info,
	 			"Trigger" : this.Trigger
	 		});
	 	},
	 	Output : function() {
	 		//리스트 출력
	 		Common_Excel.ExcelDown($state.current.name + '_3', this.Info);
	 	},
	 	WeightListClick : function(index) {
	 		//리스트 클릭
	 		let param = this.WeightList[index];
	 		//화면 정보 
	 		Weight_Standard.Info = param
	 		//화면 인덱스 
	 		Weight_Standard.Info.INDEX = index;
	 		//서버 호출
	 		mainDataService.CommunicationRelay("/reeval/getWeightSpecItem.json",param).success(function(List) {
	 			//변경된 리스트 등록
	 			Weight_Standard.List = List;
	 		});
	 	}
	 }
	 
	$scope.Serch =  Common_Serch;
	$timeout(function(){$scope.Serch.Info.YEAR='';},500);
	$scope.Evaluation_Management = Evaluation_Management;
	$scope.Price_Index = Price_Index;
	$scope.Weight_Standard = Weight_Standard;
	//검색창 앤터
	$scope.onKeyPress = function(event){
	        if (event.key === 'Enter') {
	        	$scope.Serch.Lookup();
	        }
	}
	
	$scope.clickTable = function($event){
    	$($event.currentTarget).addClass('active').siblings().removeClass('active');
    };
}
//자산가치 재평가
function reeval_popupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, MaxUploadSize) {
	$scope.$on("reeval_popup", function(e,data) {
		let time = Common_Time.toDay();
		 
		mainDataService.CommunicationRelay("/reeval/insert.json").success(function(Info) {
			reeval_popup.Info = Info;
			console.log(reeval_popup.Info)
			Model.condition.RE_EST_SID = reeval_popup.Info.RE_EST_SID;
			if(Once) {
				pagerJsonGrid(Model);
				Once = false;
			}
			$('#'+ Model.grid_id).jqGrid('setGridParam', {postData : Model.condition, page : 1}).trigger('reloadGrid');
 		});
		Triger = data.Triger;
		reeval_popup.OptionList = angular.copy(data.OptionList);
		reeval_popup.OptionList.unshift({WEIGHT_SPEC_SID: -1, WEIGHT_SPEC_NM : '선택안함'});
	
		$scope.isReevaluation_popup = true;
	});	
	
	let Once = true;
	let Model = {
            grid_id : 'list3',
            pager_id : 'listPager3',
            url : '/reeval/getListAsset.json',
            //multiselect : true,
            condition : {
            	RE_EST_SID : 0,
                page : 1,
                rows : GridConfig.sizeS
            },
            rowNum : 15,
            colNames : [ '순번','자산코드','자산명','재평가','재조달원가','잔존수명','내용연수','물가배수','가중치','가중치SID','재평가SID','자산SID'],
            colModel : [
            {name : 'RNUM', width : "40px", resizable: false}, 
            {name : 'ASSET_CD', width : "230px", resizable: false}, 
            {name : 'ASSET_NM', width : "120px", resizable: false}, 
            {name : 'RE_EST_AMT', width : 50, resizable: false}, 
            {name : 'REPURCHASE_AMT',width : 50, resizable: false, hidden: true}, 
            {name : 'LIFE_NUM', width : 50, resizable: false}, 
            {name : 'USEFUL_LIFE',width : 50, resizable: false}, 
            {name : 'PRICE_INDEX',width : 50, resizable: false},
            {name : 'WEIGHT_SPEC_NM',width : 50, resizable: false},
            {name : 'WEIGHT_SPEC_SID',width : 0,hidden : true},
            {name : 'RE_EST_SID',width : 0,hidden : true},
            {name : 'ASSET_SID',width : 0,hidden : true}
            ],
            loadComplete: function() {
            	$("#loadingSpinner").hide();
            	
            }
        }
    let Triger = null;
	let reeval_popup = {
		Info : {
			RE_EST_SID : -1,
			WEIGHT_SPEC_SID : -1,
			EST_NM : '',
			EST_YYYY : '',
			EST_MM : '',
			WEIGHT_YN : '',
			WEIGHT_NM : '선택 안함',
			DEL_YN : '',
			TEMP_YN : '',
			INSERT_ID : '',
			INSERT_DT : ''
		},
		OptionList : null,
		//화면 초기화
		Triger : function(yyyymm) {
			 $('#'+ Model.grid_id).jqGrid('setGridParam', {postData : Model.condition, page : 1}).trigger('reloadGrid');	
			 let Array = yyyymm.split('-');
			 reeval_popup.Info.EST_YYYY = Array[0] + '년';
			 reeval_popup.Info.EST_MM = Array[1]+ '월';
		},
		//리스트 팝업 오픈
		Import : function(info = this.Info) {
			$rootScope.$broadcast('basic_popup',{
				"TITLE" : '잔존수명 리스트',
				"RE_EST_SID" :this.Info.RE_EST_SID,
				"Triger" : this.Triger
			});
		},
		//삭제
		Delete : function() {
			/*//모델 아이디
			const cID = '#'+ Model.grid_id;
			//그리드 선택된 셀id들
			let ids = jQuery(cID).jqGrid("getGridParam","selarrrow");
			//선택 된 셀 탐색
        	for(let i = 0; i < ids.length; i++) {
        		//셀에 정보
        		let rowObject = jQuery(cID).jqGrid("getRowData",ids[i]);
        		//셀이 없는 거면 통과
        		if(rowObject.RNUM =='')continue;
        		//셀에 정보가 있으면 서버를 부른다 
        		mainDataService.CommunicationRelay('/reeval/deleteAsset.json',rowObject).success(function(life) {
        			$(cID).jqGrid('setGridParam', {postData : Model.condition, page : 1}).trigger('reloadGrid');
        		});
        	}*/
		},
		Start : function() {
			if(!reeval_popup.Info.EST_NM || reeval_popup.Info.EST_NM == ''){
				alertify.alert('','평가명은 필수 입니다.');
				return;
			}
			reeval_popup.Info.EST_NM = xssFilter(reeval_popup.Info.EST_NM);
			$scope.isReevaluation_popup = false;
			$("#loadingSpinner").show();
			//서버 호출
			mainDataService.CommunicationRelay('/reeval/update.json',this.Info).success(function(count) {
				if(Common_AlertStr(count)) return;
				Triger(reeval_popup.Info.EST_YYYY);
    		});
		},
		Cancel : function() {
			//취소 서버에 정보제거
			//mainDataService.CommunicationRelay('/reeval/delete.json',this.Info).success(function(count) {});
			$scope.isReevaluation_popup = false;
		}
	}
	
	$scope.reeval_popup = reeval_popup;
}
//리스트 팝업
function basic_popupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, $filter, MaxUploadSize) {
	$scope.$on("basic_popup", function(e,data) {
		//초기화
		$scope.TITLE = data.TITLE;
		basic_popup.Info.RE_EST_SID = data.RE_EST_SID;
		Triger = data.Triger;
		if(!basic_popup.isInit){
			console.log('xx')
			pagerJsonGrid(Model);
			basic_popup.isInit = true;
		}
		$scope.isBasic_popup = true;
	});
	let Model = {
            grid_id : 'list4',
            pager_id : 'listPager4',
            url : '/diag/getList.json',
            condition : {
                page : 1,
                rows : GridConfig.sizeL
            },
            rowNum : 10,
            colNames : ['순번', '평가명', '평가기간', '진단결과', '작성자', '작성일자','sid', 'temp', '년도', '월'],
            colModel : [
	            {name : 'RNUM', width : 6, sortable:false, resizable: false}, 
	            {name : 'EST_NM', width : 44, resizable: false}, 
	            {name : 'EST_YYYYMM', width : 18, resizable: false}, 
	            {name : 'EST_MODEL_TYPE', width : 0,hidden : true}, 
	            {name : 'INSERT_ID', width : 18, resizable: false}, 
	            {name : 'INSERT_DT', width : 18, resizable: false}, 
	            {name : 'LIFE_SID', width : 0, hidden : true},
	            {name : 'TEMP_YN', width : 0, hidden : true},
	            {name : 'YEAR', width : 0, hidden : true},
	            {name : 'MONTH', width : 0, hidden : true},
            ],
            onSelectRow : function(id) {
            	 Common_Swap.B_A($('#list4').jqGrid('getRowData', id), basic_popup.Info);
            	 $scope.$apply();
            }
        };
	let Triger = null;
	let basic_popup = {
		Info : {
			EST_NM : '',
			LIFE_SID : -1,
			RE_EST_SID: -1,
			EST_YYYYMM :'2021-01'
		},
		isInit: false,
		Cancel : function() {
			$scope.isBasic_popup = false;
		},
		Select : function() {
			if(this.Info.EST_NM == '')return;
			$scope.isBasic_popup = false;
			$("#loadingSpinner").show();
			mainDataService.CommunicationRelay("/reeval/insertAsset.json", this.Info).success(function(Info) {
				Triger(basic_popup.Info.EST_YYYYMM);
			});
		}
	}
	
	$scope.basic_popup = basic_popup;
}
//물가지수 팝업
function reeval_popup3Controller($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, $filter, MaxUploadSize) {
	$scope.$on("reeval_popup3", function(e,data) {
		//초기화
		$scope.TITLE = data.TITLE;
		reeval_popup3.Info = angular.copy(data.Info); 
		reeval_popup3.List = angular.copy(data.List);
		Trigger = data.Trigger
		$scope.isReevaluation_popup3 = true;
	});	
	
	let Trigger = null;
	//년도 입력
	let reeval_popup3_YYYY = {
			YYYY : '',
			IsReeval_popup3_YYYY: false,
			Ok: function() {
				//숫자에 잘못된 수 추가 제거
				this.YYYY = this.YYYY.split('.')[0];
				let list = reeval_popup3.List;
				//예외 처리
				if(isNaN(this.YYYY) == true || this.YYYY == '' || this.YYYY.length < 4) return; 
				//추가
				list.unshift({YYYY: this.YYYY ,M01 : '',M02 : '',M03 : '',M04 : '',M05 : '',M06 : '',M07 : '',M08 : '',M09 : '',M10 : '',M11 : '',M12 : ''})
			    //취소
				this.IsReeval_popup3_YYYY = false;
			},//취소
			Cancel : function() {
				this.IsReeval_popup3_YYYY = false;
			}
	}
	
	let reeval_popup3 = {
		//화면 정보
		Info : {
			BASIC_ITEM_NM: "",
			BASIC_NM: "",
			BASIC_YEAR: "",
			INSERT_DT: "",
			INSERT_ID: ""
		},
		//화면의 리스트
		List : null,
		//추가
		Add : function() {
			let list = this.List;
			if(list.length == 0){
				//년도 입력 팝업 오픈
				reeval_popup3_YYYY.IsReeval_popup3_YYYY = true;
			}else{
				//년도 추가
				list.unshift({YYYY: (Number(list[0].YYYY) + 1).toString() ,M01 : '',M02 : '',M03 : '',M04 : '',M05 : '',M06 : '',M07 : '',M08 : '',M09 : '',M10 : '',M11 : '',M12 : ''})	
			}
		},
		//양식출력
		FormOutput : function() {
			console.log(this.List)
			let array1 = []
			array1.push({"YYYY":"YYYY", "M01":"M01", "M02":"M02", "M03":"M03", "M04":"M04", "M05":"M05", "M06":"M06", "M07":"M07", "M08":"M08", "M09":"M09", "M10":"M10", "M11":"M11", "M12":"M12"});
    		const workSheet = XLSX.utils.json_to_sheet(array1.concat(this.List),{ header : ["YYYY", "M01", "M02", "M03", "M04", "M05", "M06", "M07", "M08", "M09", "M10", "M11", "M12"], skipHeader : true });
    		const workBook = XLSX.utils.book_new();
    		XLSX.utils.book_append_sheet(workBook, workSheet, '물가지수');
    		XLSX.writeFile(workBook, "물가지수_양식_"+ $scope.getToday()+".xlsx");
		},
		//파일등록
		OnFileSelect : function($files, cmd) {
			if(FileCheck($files, cmd, MaxUploadSize)) return;
			var reader = new FileReader();
			reader.onload = function(e) {
				const workBook =XLSX.read(reader.result, {type :'binary'});
				workBook.SheetNames.forEach(sheetName => {
					const rows =XLSX.utils.sheet_to_json(workBook.Sheets[sheetName]);
					reeval_popup3.List = rows
					$scope.$apply();
				});
			};
			
			reader.readAsBinaryString($files[0]);
		},
		//서버등록
		Save : function() {
			
			if(reeval_popup3.Info.BASIC_NM == ''){
				alertify.alert('','항목은 필수 입니다.');
				return;
			}
			
			if(reeval_popup3.Info.BASIC_ITEM_NM == '' ){
				alertify.alert('','기준지수는 필수 입니다.');
				return;
			}
			
			//파라미터
			let param = {
				List : this.List,
				Info : this.Info
			}
			//서버호출
	    	mainDataService.CommunicationRelay("/reeval/insertPriceIndex.json", param).success(function(count) {
	    		if(Common_AlertStr(count)) return;
	    		//화면 갱신
	    		Trigger();
	    	});
			$scope.isReevaluation_popup3 = false;
		},
		//취소
		Cancel : function() {
			this.Info = null;
			this.List = null;
			Trigger = null;
			$scope.isReevaluation_popup3 = false;
		}
	}
	//숫자 포멧
	$scope.FormatFloat = function(obj, index) {
		let text = obj[index];
		obj[index] =  $filter('number')(text, 1);
	}
	
	$scope.reeval_popup3_YYYY = reeval_popup3_YYYY;
	$scope.reeval_popup3 = reeval_popup3;
}
//가중치 평가기준 관리 팝업
function reeval_popup4Controller($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, $filter, MaxUploadSize) {
	$scope.$on("reeval_popup4", function(e,data) {
		$scope.TITLE = data.TITLE;
		let List = new Array(data.List.length)
		for(i in data.List){
			List[i] = angular.copy(data.List[i])
			List[i].INPUT_VAL1 = $filter('number')(List[i].INPUT_VAL1,0)
			List[i].INPUT_VAL2 = $filter('number')(List[i].INPUT_VAL2,0)
		}
		
		reeval_popup4.List = List;
		reeval_popup4.Info = angular.copy(data.Info);
		Trigger = data.Trigger;
		$scope.isReevaluation_popup4 = true;
	});	
	let Trigger = null;
	let reeval_popup4 = {
		Info : {
			 WEIGHT_SPEC_SID : -1,
			 WEIGHT_SPEC_NM : '',
			 INSERT_ID : '',
			 INSERT_DT : ''
		},
		List : null,
		Add : function() {
			//추가
			this.List.push({WEIGHT_NUM : '0.0', INPUT_VAL1 : '0', INPUT_VAL2 :'0'})
		},
		Save : function() {
			if(reeval_popup4.Info.WEIGHT_SPEC_NM == '') {
				alertify.alert('','평가 기준명은 필수 입니다.');
				return;
			}
			
			//저장
			let param = {
					List : this.List,
					Info : this.Info
			}
			mainDataService.CommunicationRelay("/reeval/insertWeightSpec.json",param).success(function(count) {
				if(Common_AlertStr(count)) return;
	    		//화면 갱신
	    		Trigger(reeval_popup4.Info.INDEX);
	    		//팝업창 off
	    		$scope.isReevaluation_popup4 = false;
	    	});
		},
		Cancel : function() {
			//취소
			$scope.isReevaluation_popup4 = false;
		}
	}
	
	$scope.signStore = ["<","=",">",">=","<="];
	
	//숫자포맷
	$scope.FormatFloat = function(obj, index, decimal) {
		let text = Common_sts(obj[index].toString());
		obj[index] =  $filter('number')(text, decimal);
	}
	
	$scope.reeval_popup4 = reeval_popup4;
}
