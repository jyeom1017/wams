angular.module('app.diagnosis').controller('stateDiagController', stateDiagController);//간접상태평가

function stateDiagController($scope, $state, $stateParams, mainDataService, $rootScope, $compile,$filter, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	$scope.asset_totalCount = 0;
	$scope.EstimationInfo = {EST_NM:'',EST_START_DT:'',INSERT_ID:'',INSERT_DT:''};
	$scope.EditInfo = {EST_NM:'',EST_START_DT:'',INSERT_ID:'',INSERT_DT:''};
	$scope.AllInfoItemList = [];
	$scope.saveType ='';
	$scope.MCODE = '0302';
	$scope.searchOption = 1;
	$scope.SearchOption = function(opt)
	{
		$scope.searchOption = opt;
	}
	 $scope.selectYear="";

	$scope.popupShowGrade = function(PIPE_GB,btn_id){
	 	var obj = $("#"+btn_id).parent().parent();
	 	obj.find(".active").removeClass("active");
	 	$("#"+btn_id).addClass("active");
	}	 
	
	$scope.selectedTab3Id = 'S';
	$scope.showGradeTab = function(PIPE_GB,btn_id){
	 	var obj = $("#"+btn_id).parent().parent();
	 	obj.find(".active").removeClass("active");
	 	$("#"+btn_id).addClass("active");
	 	$scope.selectedTab3Id = PIPE_GB;
	}
	
	$scope.popupSelectedTab3Id = 'S'; 	    
	$scope.popupShowGrade = function(PIPE_GB,btn_id){
	 	var obj = $("#"+btn_id).parent().parent();
	 	obj.find(".active").removeClass("active");
	 	$("#"+btn_id).addClass("active");
	 	$scope.popupSelectedTab3Id = PIPE_GB;
	 }
	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 6개월
        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.StartDt = s_yyyymmdd;
        $('#start_dt').val(s_yyyymmdd);
        $scope.EndDt = e_yyyymmdd;
        $('#end_dt').val(e_yyyymmdd);
    }
	 
	 $scope.$on('$viewContentLoaded', function () {
	    	$("#tab1_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab3_btn").removeClass("active");
	    		$("#tab1").show();
	    		$("#tab2, #tab3").hide();
	    	});
	    	$("#tab2_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab1_btn, #tab3_btn").removeClass("active");
	    		$("#tab2").show();
	    		$("#tab1, #tab3").hide();
	    	});
	    	$("#tab3_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab1_btn").removeClass("active");
	    		$("#tab3").show();
	    		$("#tab1, #tab2").hide();
	    		getGradeList();
	    	});
	    	$("#tab1_1_btn").on("click", function() {
		   		$(this).addClass("active");
		   		$("#tab1_2_btn, #tab1_3_btn").removeClass("active");
		   		$("#tab1_1").show();
		   		$("#tab1_2, #tab1_3").hide();
		   		$scope.tab1Id=1;
		   		$scope.$apply();
		   	});
		   	$("#tab1_2_btn").on("click", function() {
		   		$(this).addClass("active");
		   		$("#tab1_1_btn, #tab1_3_btn").removeClass("active");
		   		$("#tab1_2").show();
		   		$("#tab1_1, #tab1_3").hide();
		   		$scope.tab1Id=2;
		   		$scope.$apply();
		   	});
		   	$("#tab1_3_btn").on("click", function() {
		   		$(this).addClass("active");
		   		$("#tab1_2_btn, #tab1_1_btn").removeClass("active");
		   		$("#tab1_3").show();
		   		$("#tab1_1, #tab1_2").hide();
		   		$scope.tab1Id=3;
		   		$scope.$apply();
		   	});
	    	
	    	setGrid();
	    	setGrid2();
	    	$scope.setWeek();
	    	getFactorList(function(data){
	    		$scope.factorSelect($scope.factorList[0]);
	    	});
	    	getGradeList(function(data){
	    		$scope.gradeSelect($scope.gradeList[0]);
	    	});
	    	setDatePicker();
	    	setGrid2_Popup();
	    	$rootScope.setBtnAuth($scope.MCODE);
	    	
	    	mainDataService.getAssetLevelList({}).success(function(data){
	            $scope.assetLevelList  = data
	            mainDataService.getBlockCodeList({}).success(function (obj) {
	               if (!common.isEmpty(obj.errorMessage)) {
	                  alertify.error('errMessage : ' + obj.errorMessage);
	               } else {
	                  $scope.blockList = obj;
	               }
	            });
	         });
	    	
	    	$scope.searchModal = Common_Modal.Init($compile, $scope, {
		 			id: '#list2',
		 			layerId: '#modal',
		 			hidenSize: 4
		 	});

	    });
	 $scope.factorList = [];
	 $scope.gradeList = [];
	 
	 mainDataService.getAllInfoItemList()
	 .success(function(data){
		 $scope.AllInfoItemList = data;
	 });

	 //$scope.signStore = ["&lt;","&#61;","&#62;","&#62; &#61;","&lt; &#61;"];
	 $scope.signStore = ["","<","=",">",">=","<="];

	 $scope.setUseItem = function(detail){
		 var weight_val = 0.0;
		 var ITEM_CD = detail.ITEM_CD;
		 var PIPE_TYPE= detail.PIPE_TYPE;
		 var use_yn = 'N';
		 if(detail.IsActive){
			 weight_val = 0.1;
			 use_yn = 'Y';
		 }
		 else{
			 weight_val = 0.0;
			 use_yn = 'N';
		 }
		var list = [];
		 $.each($scope.popupFactorDetail,function(idx,Item){
		 	var data = angular.copy(Item);
		 	if(data.PIPE_TYPE==PIPE_TYPE && data.ITEM_CD==ITEM_CD){
		 	data.WEIGHT_VAL = weight_val;
		 	data.USE_YN = use_yn;
		 	}
		 	list.push(data);
		 });
		$scope.popupFactorDetail = list; 			 
	 }
	 
	 $scope.factorCategory = [
	 {name:'SP(송수)',GB:'S',cd:'0'}
	 ,{name:'CML-DIP(송수)',GB:'S',cd:'1'}
	 ,{name:'CIP/DIP(송수)',GB:'S',cd:'2'}
	 ,{name:'비금속(송수)',GB:'S',cd:'3'}
	 ,{name:'강관(배수)',GB:'D',cd:'4'}
	 ,{name:'주철관종(배수)',GB:'D',cd:'5'}
	 ,{name:'플라스틱관(배수)',GB:'D',cd:'6'}
	 ];
	 
	 $scope.selectedTab2Id = 'S0';
	 $scope.tab2Click = function(cd){
		 //alert(cd);
	 	$scope.selectedTab2Id = cd;
	 	/*
	 	var param = {EST_SPEC_SID: $scope.selectedFactor.EST_SPEC_SID ,PIPE_TYPE:cd};
	 	//alert($scope.selectedFactor.EST_SPEC_SID);
		mainDataService.getIndirEstFactorDetail(param)
	 	.success(function(data){
	 		$scope.factorDetail = data;
	 	});*/	 	
	 }
	 function getFactorList(callback){
	 	var param = {category:'1'};
	 	mainDataService.getIndirEstFactorList(param)
	 	.success(function(data){
	 		$scope.factorList =data.rows;
	 		if(typeof callback=='function'){
	 		callback(data);
	 		}
	 	});
	 	
	 }
	 function getGradeList(callback){
	 	var param = {category:'1'};
	 	mainDataService.getIndirEstGradeList(param)
	 	.success(function(data){
	 		$scope.gradeList =data.rows;
	 		if(typeof callback=='function'){
	 		callback(data);
	 		}	 		
	 	});	 
	 }
	 function setGrid() {
	        return pagerJsonGrid({
	            grid_id : 'list',
	            pager_id : 'listPager',
	            url : '/est/getList.json',
	            condition : {
	                page : 1,
	                rows : GridConfig.sizeL,
	                category : '1'
	            },
	            rowNum : 20,
	            colNames : [ '순번','EST_SID','평가명','평가일','작성자','작성일'],
	            colModel : [
	            {
	                name : 'RNUM',
	                width : "40px",
	                resizable: false
	            }, {
	                name : 'EST_SID',
	                width : 0,
	                hidden: true,
	                resizable: false
	            }, {
	                name : 'EST_NM',
	                width : 150,
	                resizable: false
	            }, {
	                name : 'EST_START_DT',
	                width : 80,
	                resizable: false
	            }, {
	                name : 'INSERT_ID',
	                width : 80,
	                resizable: false
	            }, {
	                name : 'INSERT_DT',
	                width : 80,
	                resizable: false
	            }],

	            onSelectRow : function(id) {
	            	
	            	$scope.EstimationInfo = $('#list').jqGrid('getRowData', id);
	            	$scope.$apply('EstimationInfo');
	            	$timeout(function(){
	    			$("#list2").setGridParam({
	        			datatype : 'json',
	        			page : 1,
	        			postData : {
	    					category : '1',
	    	                EST_SID : $scope.EstimationInfo.EST_SID,
	    	                CLASS3_CD: '',
	    	                CLASS4_CD: $scope.CLASS4_CD
	        			}
	        		}).trigger('reloadGrid', {
	        			current : true
	        		});
	        		},500);	            	

	            	var param1 = {category : '1', EST_SID : $scope.EstimationInfo.EST_SID , tabId : 1};
	            	var param2 = {category : '1', EST_SID : $scope.EstimationInfo.EST_SID , tabId : 2};
	            	
	            	
	            	mainDataService.getEstAssetGroup(param1)
	            	.success(function(data){
	            		$scope.Tab1Tot = {G1:0,G2:0,G3:0};
						$scope.EstGroupList1 = data;
						$.each(data,function(idx,Item){
							Item.tot = Item.G1 + Item.G2 + Item.G3;
							$scope.Tab1Tot.G1 += Item.G1;
							$scope.Tab1Tot.G2 += Item.G2;
							$scope.Tab1Tot.G3 += Item.G3;
						});
	            	});
	            	
	            	mainDataService.getEstAssetGroup(param2)
	            	.success(function(data){
	            		$scope.Tab2Tot = {G1:0,G2:0,G3:0};
						$scope.EstGroupList2 = data;
						$.each(data,function(idx,Item){
							var bcode = Item.GROUP_CD.split("_");
							Item.GROUP_NM1 = $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:bcode[0]})[0].C_NAME + '(' + bcode[0] + ')';
							Item.GROUP_NM2 = $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:bcode[1]})[0].C_NAME + '(' + bcode[1] + ')';
							
							Item.tot = Item.G1 + Item.G2 + Item.G3;
							$scope.Tab2Tot.G1 += Item.G1;
							$scope.Tab2Tot.G2 += Item.G2;
							$scope.Tab2Tot.G3 += Item.G3;
						});						
	            	});	            	
	            }
            ,
            gridComplete : function() {
				var ids = $(this).jqGrid('getDataIDs');
				
				//if ($.isEmptyObject($scope.PathInfo)) {
					$(this).setSelection(ids[0]);
				//}
				//alert($('#list').getGridParam('page'));
				$scope.currentPageNo=$('#list').getGridParam('page');
				$scope.count = $('#list').getGridParam('records');
				$scope.$apply();
			}	            
	        });
	    }
	 
	 function setGrid2() {
	        return pagerGrid({
	            grid_id : 'list2',
	            pager_id : 'listPager2',
	            url : '/est/assetPage.json',
	            condition : {
	                page : 1,
	                rows : 10,
	                category : '1',
	                EST_SID : $scope.EstimationInfo.EST_SID
	            },
	            rowNum : 10,
	            colNames : [ '순번','자산코드','자산명','대불록','중불록','소불록','관로계통','관종','평가등급','평가결과','EST_SID','자산번호','STATE_GRADE'],
	            colModel : [
	            {
	                name : 'RNUM',
	                width : 53,
	                resizable:false,
	            }, {
	                name : 'ASSET_CD',
	                width : 120,
	                resizable:false,
	            }, {
	                name : 'ASSET_NM',
	                width : 100,
	                resizable:false,
	            }, {
	                name : 'DAE',
	                width : 50,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
	                	//return cellvalue;
                		try{
                			return $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:cellvalue})[0].C_NAME;
                		}catch(ex){
                			return "";
                		}
	                }	                
	            }, {
	                name : 'JUNG',
	                width : 50,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
                		try{
                			return $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:cellvalue})[0].C_NAME;
                		}catch(ex){
                			return "";
                		}
	                }	                
	            }, {
	                name : 'SO',
	                width : 50,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
	                	try{
	                		return $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:cellvalue})[0].C_NAME;
                		}catch(ex){
                			return "";
                		}	                		
	                }	                
	            }, {
	                name : 'SAA',
	                width : 50,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
	                	try{
	                		return $filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0].C_NAME;
                		}catch(ex){
                			return "";
                		}	                		
	                }
	            }, {
	                name : 'MOP',
	                width : 80,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['MOP'],{C_SCODE:cellvalue})[0])
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
	                	return $filter("filter")($scope.ComCodeList['MOP'],{C_SCODE:cellvalue})[0].C_NAME;
	                }	                
	            }, {
	                name : 'GRADE',
	                width : 50,
	                resizable:false,
	                formatter : function(cellvalue, options, rowdata, action){
	                	if(cellvalue==null || cellvalue=="" ) return ""; 
	                	var stateGrade=["","1","2","3"];
	                	return stateGrade[parseInt(cellvalue)];
	                }
	            }, {
	                name : 'RESULT',
	                width : 50,
	                resizable:false,
	                formatter : function(cellvalue, options, rowdata, action){
	                	var rowid= rowdata['ASSET_SID'];
	                	var est_sid= rowdata['EST_SID'];
	                	var asset_sid= rowdata['ASSET_SID'];
						var stategrade= rowdata['STATE_GRADE'];
						if(typeof stategrade=='undefined' || stategrade==null || stategrade=="" ) return ""; 
             		return "<div id='button"+rowid+"' ><button onclick=\"showDetailResultPopup("+est_sid+","+asset_sid+","+stategrade+")\">상세</button></div>";
	                }
	            }, {
	                name : 'EST_SID',
	                width : 0,
	                hidden:true,
	            }, {
	                name : 'ASSET_SID',
	                width : 0,
	                hidden:true,
	            }, {
	            	name : 'STATE_GRADE',
	                width : 0,
	                hidden:true,
	            
	            }],

	            onSelectRow : function(id) {
	            	
	                $scope.AssetItemInfo = $('#list2').jqGrid('getRowData', id);
	                console.log($scope.AssetItemInfo);
	            }
	        });
	    }

	 function setGrid2_Popup() {
	        return pagerGrid({
	            grid_id : 'list2popup',
	            pager_id : 'list2popupPager',
	            url : '/est/assetPage.json',
	            condition : {
	                page : 1,
	                rows : 10,
	                category : '1',
	                EST_SID : $scope.EditInfo.EST_SID
	            },
	            rowNum : 15,
	            multiselect : true,
	            colNames : [ '순번','EST_SID','자산번호','자산코드','자산명','대불록','중불록','소불록','관로계통','관종','평가등급','평가결과'],
	            colModel : [
	            {
	                name : 'RNUM',
	                width : "40px"
	            }, {
	                name : 'EST_SID',
	                width : 0,
	                hidden:true,
	            }, {
	                name : 'ASSET_SID',
	                width : 0,
	                hidden:true,
	            }, {	            	
	                name : 'ASSET_CD',
	                width : "180px",
	            }, {
	                name : 'ASSET_NM',
	                width : 100,
	            }, {
	                name : 'DAE',
	                width : 50,
	                hidden: true,
	            }, {
	                name : 'JUNG',
	                width : 50,
	            }, {
	                name : 'SO',
	                width : 50,
	            }, {
	                name : 'SAA',
	                width : 50,
	            }, {
	                name : 'MOP',
	                width : 50,
	            }, {
	                name : 'GRADE',
	                width : 50,
	                hidden: true,
	            }, {
	                name : '평가결과',
	                width : 50,
	                hidden: true,
	                formatter : function(cellvalue, options, rowdata, action){
	                	var rowid= rowdata['ASSET_SID'];
	                	var est_sid= rowdata['EST_SID'];
	                	var asset_sid= rowdata['ASSET_SID'];
	                	var stategrade= rowdata['ASSET_SID'];	                	
             		return "<div id='button"+rowid+"' ><button onclick=\"showDetailResultPopup("+est_sid+","+asset_sid+","+stategrade+")\">상세</button></div>";
	                }
	            }],

	            onSelectRow : function(id) {
	            	
	                //$scope.AssetItemInfo = $('#list2popup').jqGrid('getRowData', id);
	                //console.log($scope.AssetItemInfo);
	            },
				gridComplete : function() {
					//debugger;
					$scope.asset_totalCount = $('#list2popup').getGridParam("records")
					$scope.$apply();
				}	            
	        });
	    }
	 
	 $scope.onKeyPress = function(event){
	        if (event.key === 'Enter') {
	        	$scope.SearchList();
	        }
	    }
	 
	 $scope.currentPageNo = 1;
	 $scope.SearchList = function(){
		if($scope.searchOption==1){
    		if($scope.p1SearchYear==""){
    			//alert('기간을 입력하 세요');
    			alertify.alert('오류','기간을 입력하세요');
    			$("#selectYear").focus();
    			return;    			
    		}
    		if($scope.selectYear==""){
	        	$scope.sOptionStartDt = ""; 
	           	$scope.sOptionEndDt =  "";
    		}else{
	        	$scope.sOptionStartDt = $scope.selectYear + "0101"; 
	           	$scope.sOptionEndDt =  $scope.selectYear + "1231";
           	}  		
    	}else{
    		if($("#start_dt").val()==""){
    			//alert('기간을 입력하 세요');
    			alertify.alert('오류','기간을 입력하세요');
    			$("#start_dt").focus();
    			return;
    		}
    		if($("#end_dt").val()==""){
    			//alert('기간을 입력하 세요');
    			alertify.alert('오류','기간을 입력하세요');
    			$("#end_dt").focus();
    			return;
    		}
    		if($("#end_dt").val() < $("#start_dt").val()){
    			alertify.alert('오류','기간범위를  확인하세요.시작날짜가 종료날짜 보다 클수 없습니다.');
    			$("#start_dt").focus();
    			return;
    		}
        	$scope.sOptionStartDt = $("#start_dt").val().replace(/-/gi,''); 
           	$scope.sOptionEndDt =  $("#end_dt").val().replace(/-/gi,'');    		
    	}
    		
    		 
			$("#list").setGridParam({
    			datatype : 'json',
    			page : $scope.currentPageNo,
    			rows : GridConfig.sizeS,
    			postData : {
    				searchText : $scope.searchText,
					category : '1',
					START_DT : $scope.sOptionStartDt,
					END_DT : $scope.sOptionEndDt					
    			}
    		}).trigger('reloadGrid', {
    			current : true
    		});	 
	 }
	 
	 function renderGridButton(){
     	var sHTML = "<input type=button value=\"상세결과\" ng-click=\"showDetailResultPopup()\" >";
    	var template = angular.element(sHTML);
		var linkFunction = $compile(template);
		linkFunction($scope);
		return template;		 
	 }
	 
	 $scope.DownLoadExcel=function(Type){
	    	const jobType = $state.current.name + "_" + Type;
	        //const levelNm = $scope.levelNm;

	        $('#exportForm').find('[name=jobType]').val(jobType);
	        switch(Type){
	        case 0 : //간접평가 목록
	        	$('#exportForm').find('input[name=start_dt]').val($scope.sOptionStartDt);
	        	$('#exportForm').find('input[name=end_dt]').val($scope.sOptionEndDt);
	        	$('#exportForm').find('input[name=searchText]').val($scope.searchText);
	        	break;
	        case 1 : //간접평가 항목
	        	$('#exportForm').find('[name=jnum]').val( $scope.selectedFactor.EST_SPEC_SID );
	        	break;
	        case 2 : // 개량방안
	        	$('#exportForm').find('[name=jnum]').val($scope.gradeDetail[0].EST_IMP_SID);
	        	break;	        	
	        }
	        
	        $('#exportForm').submit();   
	 }
	 
	 $scope.DownloadReport = function(){
	    	const jobType = $state.current.name ;
	        $('#exportForm').find('[name=jobType]').val(jobType);
        	$('#exportForm').find('[name=jnum]').val( $scope.EstimationInfo.EST_SID );
        	$('#exportForm').submit();
	 }
	 $scope.DeleteItem = function(){
		 if (!($scope.EstimationInfo.EST_SID > 0)) {
			 alertify.alert('오류','항목을 선택해주세요');
			 return;
		 }

		 // 삭제 권한 체크
		 if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
			 alertify.alert('오류','권한이 없습니다.');
			 return;
		 }

		 alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?',
				 function () {
			 mainDataService.updateIndirEst({
				 category: '1' ,
				 del_yn: 'Y' , 
				 est_sid : $scope.EstimationInfo.EST_SID
				 // del_yn: 'Y',
			 }).success(function (obj) {
				 if (!common.isEmpty(obj.errMessage)) {
					 alertify.error('errMessage : ' + obj.errMessage);
				 } else {
					 alertify.success('삭제 되었습니다.');
					 $scope.SearchList();
				 }
			 });
		 },
		 function () {
		 }
		 ).set('basic', false);
		 /*if($scope.EstimationInfo.EST_SID > 0){
	 	if(confirm('삭제확인','삭제하시겠습니까?')){
	 	var param = {category: '1' ,del_yn: 'Y' , est_sid : $scope.EstimationInfo.EST_SID};
		 	mainDataService.updateIndirEst(param)
		 	.success(function(data){
		 		alertify.success('삭제되었습니다.');
				$scope.SearchList();
		 	});
	 	}
	 	}else{
	 		alertify.alert('오류','삭제할 간접 평가가 없습니다.');
	 	}*/	 
	 };
	 
	 $scope.NewItem = function(){
	 	var param = {category: '1' ,temp_yn: 'Y' };
	 	$scope.saveType = 'new';
	 	mainDataService.insertIndirEst(param)
	 	.success(function(est_sid){
	 		console.log(est_sid);
	 		$scope.EditInfo = {
	 			Mode : 'new',
	 			EST_SID : est_sid
	 		}
		 	$("#newIndirEstimationPopup").show();
			$("#list2popup").setGridParam({
    			datatype : 'json',
    			page : 1,
    			postData : {
					category : '1',
	                EST_SID : $scope.EditInfo.EST_SID,
	                CLASS3_CD: '',
	                CLASS4_CD: ''	                
    			}
    		}).trigger('reloadGrid', {
    			current : true
    		});	 
			 //alert('신규평가');	
	 	});
	 }
	
	 $scope.showDetailResultPopup = function(est_sid,asset_sid,stategrade){
		 $("#DetailResultIndirEstimationPopup").show();	 
		 //alert('test');
		 var param = {category:'1',EST_SID:est_sid,ASSET_SID:asset_sid};
		 mainDataService.getIndirEstAssetResult(param)
		 .success(function(data){
		 	console.log(data);
		 	$scope.EstIndirAssetInfo = data;
		 	$scope.result = 0;
		 	$scope.sum_fw = 0;
		 	$scope.sum_weight = 0;
		 	$scope.dp = 0;
		 	$scope.state_grade = parseInt(stategrade);
		 	switch($scope.state_grade){
			 	case 1 : $scope.stateGrade = "I노후진행(소)               ................  "; break;
			 	case 2 : $scope.stateGrade = "II노후진행(중)               ...............   "; break;
			 	case 3 : $scope.stateGrade = "III노후진행(대)               ...............  "; break;
			 	default : 
			 		$scope.stateGrade = "III상태노후화                     ........            "; break;
		 	}
						 	
		 	$.each(data,function(idx,Item){
		 		if(idx==0) $scope.dp = Item.DP
		 		$scope.sum_weight += Item.WEIGHT;
		 		$scope.sum_fw += Item.FW; 
		 	});
		 	
		 	$scope.result = ( $scope.sum_fw / $scope.sum_weight ) * $scope.dp ;
		 	
		 });
		 
	 }
	 
	//결과보기
	$scope.ShowResult = function() {
		var param = { 'category': '1', 'est_sid': $scope.EstimationInfo.EST_SID };

		mainDataService.getGISInformationAndConditionAssessmentResults(param).success(function(data) {
			// console.log(data);
			var ftrIdnStringWithComma = data.map(obj => obj.FTR_IDN).join();
			var layerNameStringWithComma = [...new Set(data.map(obj => obj.LAYER_NM))].join();
			// console.log(ftrIdnStringWithComma);
			// console.log(layerNameStringWithComma);
			$rootScope.$broadcast('showGisPopup', {
				init: function() {
			          var colorMap = [
			              'rgb(  0,   0, 255)', // default
			              'rgb(  0,   0, 255)', // blue  1
			              'rgb(  0, 255,   0)', // green 2
			              'rgb(255,   0,   0)', // red 3
			              'rgb(  0,   0, 255)', // not defined
			              'rgb(  0,   0, 255)' // not defined
			            ];

					var getMatchingColorForTheGrade = function(feature, data) {
						for (var i = 0; i < data.length; i++) {
							if (feature.get('ftr_idn') === data[i].FTR_IDN.toString()) {
								return commonUtil.isNotEmpty(data[i].STATE_GRADE) ? colorMap[Number(data[i].STATE_GRADE)] : colorMap[0]; // matched
							}
						}

						return colorMap[0]; // not matched
					};

					var style = function(color) {
						var fill = new ol.style.Fill({ 'color': color });
						var stroke = new ol.style.Stroke({
							color: color,
							width: 1.25
						});

						return new ol.style.Style({
							image: new ol.style.Circle({
								fill: fill,
								stroke: stroke,
								radius: 5
							}),
							fill: fill,
							stroke: stroke
						});
					};

					var vectorSource = new ol.source.Vector({
						format: new ol.format.WFS(),
						loader: function(extent) {
							$.ajax(WFS_URL, {
								type: 'POST',
								data: {
									service: 'WFS',
									version: '1.1.0',
									request: 'GetFeature',
									typeName: layerNameStringWithComma,
									srsname: 'EPSG:3857',
									cql_filter: 'ftr_idn in (' + ftrIdnStringWithComma + ')'
								}
							}).done(function(response) {
								console.log('response', response);

								var features = vectorSource.getFormat().readFeatures(response);
								vectorSource.addFeatures(features);
								vectorSource.forEachFeature(function(feature) {
									feature.setStyle(style(getMatchingColorForTheGrade(feature, data)));
								});
							});
						},
						projection: 'EPSG:3857',
						crossOrigin: 'Anonymous'
					});

					var vectorLayer = new ol.layer.Vector({
						source: vectorSource,
						style: ol.style.Style.defaultFunction
					});

					map.addLayer(vectorLayer);

					var select = new ol.interaction.Select({
						layers: [vectorLayer]
					});
					map.addInteraction(select);

					select.on('select', function(event) {
						var features = event.selected;

						if (features === undefined || features.length === 0) {
							return;
						}

						var feature = features[0];
						var properties = feature.getProperties();
						var param = {
							'ftr_idn': properties.ftr_idn, 'ftr_cde': properties.ftr_cde
						};

						getAssetInfoWithGisInfo(mainDataService, param).success(function(data) {
							parent.showAssetItemPP(data);
						});
					});
				}
				// callbackId: '',
			});
		});

		mainDataService.getAssetNonePage(param).success(function(data) {
			var interval = $interval(function() {
				var scope = angular.element($('#gisPopup')).scope();

				if (scope) {
					$.each(data,function(idx,Item){
						try{
							Item.DAE = $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:Item.DAE})[0].C_NAME;
						}catch(e){}
						try{
							Item.JUNG = $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:Item.JUNG})[0].C_NAME;
						}catch(e){}
						try{						
							Item.SO =  $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:Item.SO})[0].C_NAME;
						}catch(e){}
						try{						
							Item.SAA =  $filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:Item.SAA})[0].C_NAME;
						}catch(e){}
						try{						
							Item.MOP =  $filter("filter")($scope.ComCodeList['MOP'],{C_SCODE:Item.MOP})[0].C_NAME;
						}catch(e){}						
					});
					scope.evaluationResultsList = data;
					clearInterval();
				}
			}, 1000, 10);

			var clearInterval = function() {
				if (angular.isDefined(interval)) {
					$interval.cancel(interval);
					interval = undefined;
				}
			};
		});
	};
	 
	 $scope.closeNewItemPopup = function(){
	 	if($scope.EditInfo.Mode=='new'){
	 	var param = {category: '1' ,temp_yn: 'Y', est_sid : $scope.EditInfo.EST_SID };
			mainDataService.deleteIndirEst(param)
			.success(function(data){
			
			});
	 	}
		 $("#newIndirEstimationPopup").hide();
	 }
	 
	 //관로선택
	$scope.showGisPopup = function() {
		$rootScope.$broadcast('showGisPopup', {
			init: function() {
				var gisPopup = document.getElementById('gisPopup');
				var event = new Event('change');
				var selectTarget;
				var drawSelector;

				gisPopup.querySelector('#start_select_mode').click();

				selectTarget = gisPopup.querySelector('#selectTarget');
				selectTarget.value = 'ams:wtl_pipe_lm:use_yn=true AND saa_cde=\'SAA003\'';
				selectTarget.dispatchEvent(event);

				drawSelector = gisPopup.querySelector('#drawSelector');
				drawSelector.value = 'Box';
				drawSelector.dispatchEvent(event);
			},
			callbackId: 'IndirEstAsset_selectGisInfo'
		});
		$('newIndirEstimationPopup').hide();
	};
	 
	 //선택관로 삭제
	 $scope.deleteSelectedAsset = function(){
		 //alert('선택 관로삭제');
		 //debugger;
		 var list = [];
		 var selrow = "";
		 var s = jQuery("#list2popup").jqGrid('getGridParam','selarrrow');
		  if(s.length<=0) {
		   //alert("항목을 선택해 주세요.");
		   alertify.alert('오류','항목을 선택해 주세요');
		   return;
		  }else{
		   s = ""+s;
		   sArr = s.split(",");
		    for(var i=0; i<sArr.length; i++){
		    var ret = jQuery("#list2popup").getRowData(sArr[i]);
		    list.push(parseInt(ret.ASSET_SID));
		    //if(i == 0) selrow = "'"+ret.ASSET_SID+"'";
		    //else selrow += ",'"+ret.ASSET_SID+"'";
		   }
		  }
		  //alert(selrow);
		  var param = {category:'1',EST_SID:$scope.EditInfo.EST_SID,ASSET_LIST:list};
		  mainDataService.deleteEstAsset(param)
		  .success(function(data){
			  reloadPopupAssetList();
		  });
	 }	 
	 
	 //평가시작
	 $scope.SaveAssetResult = function(){
	 	//alert('평가시작');
	 	//debugger;
	 	if(typeof $scope.EditInfo.EST_NM=='undefined' || $scope.EditInfo.EST_NM=='' ){
	 		//alert("평가명을 입력하세요");
	 		alertify.alert('오류','평가명을 입력하세요');
	 	 	return;
	 	 }
	 	if(typeof $scope.EditInfo.EST_START_DT=='undefined' || $scope.EditInfo.EST_START_DT=='' ){
	 		//alert("평가일을 입력하세요");
	 		alertify.alert('오류','평가일을 입력하세요');
	 	 	return;
	 	 }	 	 	 	
	 	if($scope.asset_totalCount==0){
	 		//alert("관로를 선택하세요");
	 		alertify.alert('오류','관로를 선택하세요');
	 	 	return;
	 	 }
	 	var param = angular.copy($scope.EditInfo);
	 	console.log($scope.EditInfo);
	 	param.est_sid = $scope.EditInfo.EST_SID;
	 	param.est_nm = xssFilter($scope.EditInfo.EST_NM);
	 	param.temp_yn='N';
	 	param.category="1";
	 	mainDataService.updateIndirEst(param)
	 	.success(function(data){
		//저장 성공
			mainDataService.saveIndirEstAssetResult(param)
			.success(function(data){
			 	console.log(data);
			 	$("#newIndirEstimationPopup").hide();
				 	$("#list").setGridParam({
		    			datatype : 'json',
		    			page : $scope.currentPageNo,
		    			rows : GridConfig.sizeS,
		    			postData : {
	
		    			}
		    		}).trigger('reloadGrid', {
		    			current : true
		    		});
			});
	 	});
		 
	 }
	 
	 $scope.AssetList = [{},{},{},{}];
	 
	 $scope.$on('IndirEstAsset_selectGisInfo',function(event,data){
	    	//debugger;
	    	//console.log(data);

	    	var obj = angular.copy(getSelectGisObj());
	    	//console.log(obj);
	    	
	    	var list = [];
			$.each(obj,function(idx, Item) {
				list.push(Item.ftr_idn);	
			});
			
			var param = {FTR_IDN : list.toString(), LAYER_CD: obj[0].fid};
			mainDataService.getAssetFromGisInfo(param).success(function(data) {
				console.log(data);
				var list2= [];
				$.each(data,function(idx,Item){
					if(Item.LAYER_CD=='P4' || Item.LAYER_CD=='P3')
					list2.push(Item.ASSET_SID);
				});
		    	var param2 = {EST_SID:$scope.EditInfo.EST_SID,ASSET_LIST : list2  }; 
		    	mainDataService.saveIndirEstAsset(param2)
		    	.success(function(data){
		    		//console.log(data);
		    		
		    		
	    			reloadPopupAssetList();
		    		
		    	});
				
			});

	    	$("newIndirEstimationPopup").show();
	    });
    
    $scope.closeDetailResultIndirEstimation = function (){
    	$("#DetailResultIndirEstimationPopup").hide();
    }

    function reloadPopupAssetList(){
		$("#list2popup").setGridParam({
			datatype : 'json',
			postData : {
				category : '1',
                EST_SID : $scope.EditInfo.EST_SID,
                CLASS3_CD: '',
                CLASS4_CD: ''                
			}
		}).trigger('reloadGrid', {
			current : true
		});    	
    }
    
    $scope.clickTable = function($event){
    	$($event.currentTarget).addClass('active').siblings().removeClass('active');
    };
    
    $scope.factorSelect = function (factor){
    	console.log(factor);
    	$scope.selectedFactor = factor;
    	
    	//$scope.tab2Click('0');
    	//debugger;
    	$scope.EditFactorInfo = angular.copy(factor);
	 	var param = {category:'1',EST_SPEC_SID: $scope.selectedFactor.EST_SPEC_SID };
		mainDataService.getIndirEstFactorDetail(param)
	 	.success(function(data){
	 		$scope.popupFactorDetail = angular.copy(data);
	 		$.each($scope.popupFactorDetail,function(idx,Item){
	 			Item.IsActive = (Item.USE_YN=='Y')?true:false;
	 		});
	 	});
	 	var param2 = {category:'1',EST_SPEC_SID: $scope.selectedFactor.EST_SPEC_SID ,USE_YN:'Y'};
		mainDataService.getIndirEstFactorDetail(param2)
	 	.success(function(data){
	 		$scope.factorDetail = angular.copy(data);
	 	});	 	    	
    }
    
    //신규 평가항목 팝업
    $scope.newFactorItem=function(){
    	//debugger;
    		$scope.saveType = 'new';
        	var param ={category:'1',EST_SPEC_NM:'',APPLY_DT:'',ITEM_LIST : [] };
        	$scope.popupEditFactorInfo = angular.copy($scope.EditFactorInfo);
        	mainDataService.insertIndirEstFactorList(param)
        	.success(function(data){
        		//$scope.selectedFactor = {EST_SPEC_SID : parseInt(data)};
        		$scope.popupEditFactorInfo = {EST_SPEC_SID : parseInt(data)};
				 	var param2 = {category:'1',EST_SPEC_SID: parseInt(data) ,PIPE_TYPE:'0',USE_YN : 'Y'};
					mainDataService.getIndirEstFactorDetail(param2)
				 	.success(function(data2){
				 		$scope.popupFactorDetail = data2;
				 		$("#newIndirEstFactorPopup").show();
				 	});        		
        	});    	
    }
    //수정 평가항목 팝업     
    $scope.editFactorItem=function(){
    	$("#newIndirEstFactorPopup").show();
    	$scope.saveType = '';
    	//$scope.EditFactorInfo = angular.copy($scope.selectedFactor);
    	$scope.popupEditFactorInfo = angular.copy($scope.selectedFactor);
    	$scope.factorSelect($scope.selectedFactor);
    }
    $scope.closeIndirEstFactor = function(){
		$("#newIndirEstFactorPopup").hide();
		$scope.factorSelect($scope.selectedFactor);
    }    
    //신규 개량방안 팝업
    $scope.newGradeItem=function(){
    	$("#newIndirEstGradePopup").show();
    	$scope.popupGradeDetail = angular.copy($scope.gradeDetail);
    	$scope.EditGradeInfo = angular.copy($scope.SelectedGradeInfo);
    	$scope.EditGradeInfo.EST_IMP_SID=0;
    	$scope.saveType = 'new';
    	$scope.EditGradeInfo.Mode='new';
    	$scope.EditGradeInfo.EST_IMP_NM='';
    	$scope.EditGradeInfo.APPLY_DT='';
    }

    //수정 개량방안 팝업    
    $scope.editGradeItem=function(){
    	$("#newIndirEstGradePopup").show();
    	$scope.popupGradeDetail = angular.copy($scope.gradeDetail);
    	$scope.EditGradeInfo = angular.copy($scope.SelectedGradeInfo);
    	$scope.saveType = '';
    	$scope.EditGradeInfo.Mode='';
    }
    
    $scope.closeIndirEstGrade = function(){
		$("#newIndirEstGradePopup").hide();    
    }
    
    $scope.popupSelectedTab2Id = 'S0';
    $scope.popupTab2Click = function(cd){
	 	$scope.popupSelectedTab2Id = cd;
	
	 }
    $scope.EditFactorInfo = {};
    
    $scope.saveIndirEstFactor = function(){
    	//debugger;
    	if($scope.popupEditFactorInfo.EST_SPEC_NM =='' || $scope.popupEditFactorInfo.EST_SPEC_NM==null){
    		alertify.alert('오류','평가기준명을 입력하세요');
    		return false;
    	}
    	if($scope.popupEditFactorInfo.APPLY_DT =='' || $scope.popupEditFactorInfo.APPLY_DT==null){
    		alertify.alert('오류','적용일자를 입력하세요');
    		return false;
    	}
    	
    	for(var i=0;i<7;i++){
    		var f1 = $filter("filter")($scope.popupFactorDetail,{PIPE_TYPE:''+i});
    		var item_list = ['10025','10004','10011','10012','10013','10014','10400','10401','10402'];
    			$.each($scope.popupFactorDetail,function(idx,Item){
    				if($.inArray(Item.ITEM_CD,item_list)<0){
    					item_list.push(Item.ITEM_CD);
    				}
    			});
    		for(key in item_list){
	    		var f2 = $filter("filter")(f1,{ITEM_CD:item_list[key]});
				if(!validateSignValueRange(f2)){
					if((''+f2[0].INPUT_SIGN2.trim())=='' && (''+f2[0].INPUT_SIGN1.trim())=='' && (''+f2[0].INPUT_VAL1.trim())=='' && (''+f2[0].INPUT_VAL2).trim() =='' ){
					}else if(((''+f2[0].INPUT_VAL1.trim())!='' &&  (''+f2[0].INPUT_SIGN1.trim())=='') || ((''+f2[0].INPUT_VAL1.trim())=='' &&  (''+f2[0].INPUT_SIGN1.trim())!='')){
					}else if(((''+f2[0].INPUT_VAL2).trim() !='' && (''+f2[0].INPUT_SIGN2.trim())=='') || ((''+f2[0].INPUT_VAL2).trim() =='' && (''+f2[0].INPUT_SIGN2.trim())!='')){
					}else{
						//alertify.alert('오류','등급 구간값을 확인하세요. 각 등급별 구간이 중복될 수 없습니다.');	
					}
		    		return false;
				}
    		}
    	}
    	
    	if(typeof $scope.popupEditFactorInfo.EST_SPEC_SID =='undefined'){
    		/*
    		var list = [];
        	var param ={EST_SPEC_NM:$scope.EditFactorInfo.EST_SPEC_NM,APPLY_DT:$scope.EditFactorInfo.APPLY_DT,ITEM_LIST : $scope.popupFactorDetail };
        	mainDataService.insertIndirEstFactorList(param)
        	.success(function(data){
        		getFactorList();
        	});
        	*/
    	}else{
    		var list = [];
    		$.each($scope.popupFactorDetail,function(idx,Item){
    			Item.INSERT_ID='';
    			Item.USE_YN = (Item.USE_YN=='Y')?'Y':'N';
    		});
        	var param ={category:'1'
        			,EST_SPEC_SID: $scope.popupEditFactorInfo.EST_SPEC_SID
        			,EST_SPEC_NM:$scope.popupEditFactorInfo.EST_SPEC_NM
        			,APPLY_DT:$scope.popupEditFactorInfo.APPLY_DT.replace(/-/gi,'')
        			,ITEM_LIST : $scope.popupFactorDetail };
        	mainDataService.updateIndirEstFactorList(param)
        	.success(function(data){
        		$timeout(function(){
        		getFactorList(function(data){
        			$.each(data.rows,function(idx,Item){
        				if(Item.EST_SPEC_SID == $scope.popupEditFactorInfo.EST_SPEC_SID ){
        					$scope.factorSelect(Item);
        					return false;
        				}
        			});
        		});
        		},500);
        		$("#newIndirEstFactorPopup").hide();
        		//$scope.closeIndirEstFactor();
        		alertify.success("저장되었습니다.");
        	});    		
    	}
    	
    }
    $scope.showAddIndirEstFactorPopup = function(){
    	//debugger;
    	$("#AddIndirEstFactorPopup").show();
    	var list = [];
    	var EST_SPEC_SID = $scope.popupEditFactorInfo.EST_SPEC_SID;
    	var PIPE_GB = ($scope.popupSelectedTab2Id<4)?'S':'D';
    	var PIPE_TYPE = $scope.popupSelectedTab2Id;
    	var ITEM_GCD = 'ETC';
    	var ITEM_CD = '';
    	$.each($scope.BlankFactorList,function(idx,Item){
        	var param = {CASE_SID:idx, EST_SPEC_SID:EST_SPEC_SID,PIPE_GB:PIPE_GB,PIPE_TYPE:PIPE_TYPE,ITEM_GCD:ITEM_GCD,ITEM_CD:ITEM_CD,INPUT_VAL1:'A',INPUT_SIGN1:'<',INPUT_SIGN2:'<=',INPUT_VAL2:'B',CONDITION_VAL:0,WEIGHT_VAL:0.1,IsActive:true};
        	list.push(param);
    	});    	
    	$scope.AddFactorList = angular.copy(list);
    }
    $scope.closeAddIndirEstFactorPopup = function(){
    	$("#AddIndirEstFactorPopup").hide();
    }
    $scope.AddIndirEstFactorPopup = function(){
    	var checkError = false;
    	$.each($scope.popupFactorDetail,function(idx,Item){
    		if($scope.popupSelectedTab2Id == ('' + Item.PIPE_GB + Item.PIPE_TYPE)){
    			if(Item.ITEM_CD == $scope.AddFactorList[0].ITEM_CD){
    				checkError = true;
    				return false;
    			}
    		}
    	});
    	if(checkError){
    		alertify.alert('info','중복항목입니다.',function(){})
    		return ;
    	}
    	
    	$("#AddIndirEstFactorPopup").hide();
    	//debugger;
    	var list = [];
    	var EST_SPEC_SID = $scope.popupEditFactorInfo.EST_SPEC_SID;
    	var PIPE_GB = $scope.popupSelectedTab2Id.substr(0,1);
    	var PIPE_TYPE = $scope.popupSelectedTab2Id.substr(1);
    	var ITEM_GCD = 'ETC';
    	var ITEM_CD = $scope.AddFactorList[0].ITEM_CD;
    	var GROUP_RN = 0;
    	
    	var ITEM_NM = $scope.AddFactorList[0].ITEM_CD;
    	$.each($scope.AllInfoItemList,function(idx,Item){
    		if(Item.ITEM_CD==ITEM_NM){
    			ITEM_NM = Item.ITEM_NM;
    			return false;
    		}
    	});
    	var ROW_NUMBER = $scope.popupFactorDetail.length;
    	var GROUP_ROW_SPAN = 0;
    	var LAST_RN = $scope.popupFactorDetail.length;
    	$.each($scope.popupFactorDetail,function(idx,Item){
    		if($scope.popupSelectedTab2Id == ('' + Item.PIPE_GB + Item.PIPE_TYPE)){
	    		if(Item.ITEM_GCD=='ETC'){
	    			GROUP_RN = Item.RN;
	    			return false;
	    		}
    		}
    	});    	
    	if(GROUP_RN==0) GROUP_RN = LAST_RN +1;
    	
    	$.each($scope.popupFactorDetail,function(idx,Item){
    		if($scope.popupSelectedTab2Id == ('' + Item.PIPE_GB + Item.PIPE_TYPE)){
	    		if(Item.ITEM_GCD=='ETC'){
	    			GROUP_ROW_SPAN++;
	    		}
    		}
    	});    	
    	
    	$.each($scope.AddFactorList,function(idx,Item){
    		Item.ITEM_NM = ITEM_NM;
    		Item.ITEM_CD = ITEM_CD;
    		Item.CASE_SID = idx+1;
    		Item.ITEM_RN = LAST_RN +1;
    		Item.GROUP_RN = LAST_RN +1;
    		Item.GROUP_ROW_SPAN = $scope.AddFactorList.length;
    		Item.ROW_SPAN = $scope.AddFactorList.length;
    		Item.RN = ROW_NUMBER + idx +1;
    		var newitem = {PIPE_GB:PIPE_GB,PIPE_TYPE:PIPE_TYPE
    						,ITEM_NM:ITEM_NM
    						,ITEM_CD:ITEM_CD
    						,CASE_SID:Item.CASE_SID
    						,ITEM_RN:Item.ITEM_RN
    						,GROUP_RN: Item.GROUP_RN 
    						,GROUP_ROW_SPAN:Item.GROUP_ROW_SPAN
    						,ROW_SPAN : Item.ROW_SPAN
    						,RN:Item.RN};
    		$scope.popupFactorDetail.push(angular.copy(newitem));	
    	});
    	
    }
    
    $scope.BlankFactorList= [];
    for(var i=1;i<=5;i++)
    	$scope.BlankFactorList.push({CASE_SID:i, EST_SPECT_SID:'',PIPE_GB:'',PIPE_TYPE:'',ITEM_GCD:'',ITEM_CD:'',INPUT_VAL1:'',INPUT_SIGN1:'',INPUT_SIGN2:'',INPUT_VAL2:'',CONDITION_VAL:'',WEIGHT_VAL:''});
    
    $scope.removeRowItem = function(){
    	$scope.AddFactorList.pop();
    	console.log($scope.AddFactorList);
    }
    
    $scope.addRowItem = function(){
    	$scope.AddFactorList.push({CASE_SID:$scope.AddFactorList.length+1, EST_SPECT_SID:'',PIPE_GB:'',PIPE_TYPE:'',ITEM_GCD:'',ITEM_CD:'',INPUT_VAL1:'',INPUT_SIGN1:'',INPUT_SIGN2:'',INPUT_VAL2:'',CONDITION_VAL:'',WEIGHT_VAL:''});
    	console.log($scope.AddFactorList);
    }
    
    $scope.gradeSelect = function(grade){
    	console.log(grade);
    	$scope.SelectedGradeInfo = grade;
    	$scope.EditGradeInfo = angular.copy(grade);
    	var param = {EST_IMP_SID:grade.EST_IMP_SID };
    	mainDataService.getIndirEstGradeDetail(param)
    	.success(function(data){
    		console.log(data);
    		$scope.gradeDetail = data;
    	});
    }
    
    $scope.saveIndirEstGrade = function(){
    	if(1){
    		if($scope.EditGradeInfo.EST_IMP_NM == null || $scope.EditGradeInfo.EST_IMP_NM == ""){
    		//alert('개량방안명 필수 항목');
    		alertify.alert('오류','개량방안명을 입력하세요');
    		return false;
    		}
    		if($scope.EditGradeInfo.APPLY_DT == null || $scope.EditGradeInfo.APPLY_DT == ""){
    		//alert('적용일자 필수 항목');
    		alertify.alert('오류','적용일자를 입력하세요');
    		return false;
    		}
    		var item_list = ['0','1','2','3','4','5','6'];
			for(key in item_list){
	    		var f2 = $filter("filter")($scope.popupGradeDetail,{PIPE_TYPE:item_list[key]});    		
				if(!validateSignValueRange(f2)){
		    		return false;
				}
			}
    	}
    
    	console.log($scope.popupGradeDetail);
    	var list = [];
    	$.each($scope.popupGradeDetail,function(idx,Item){
    		for(key in Item){
    			if(Item[key]==null) Item[key]="";
    		}
    		list.push(Item);
    	});
    	console.log(list);
    	$("#newIndirEstGradePopup").hide();
    	var param = {EST_IMP_SID: $scope.EditGradeInfo.EST_IMP_SID
    			,EST_IMP_NM : $scope.EditGradeInfo.EST_IMP_NM
    			,APPLY_DT : $scope.EditGradeInfo.APPLY_DT.replace(/-/gi,'')
    			,ITEM_LIST : list
    			,category : '1'
    			};
    			 
    	if($scope.EditGradeInfo.Mode=='new'){
	    	mainDataService.insertIndirEstGrade(param)
	    	.success(function(data){
	    		console.log(data);
	    		getGradeList();
	    		alertify.success("저장되었습니다.");
	    	});    	
    	}else{
    		$scope.SelectedGradeInfo = angular.copy($scope.EditGradeInfo);
    		$scope.gradeDetail = angular.copy($scope.popupGradeDetail);
	    	mainDataService.updateIndirEstGrade(param)
	    	.success(function(data){
	    		console.log(data);
	    		getGradeList();
	    		alertify.success("저장되었습니다.");
	    	});
    	} 
    }
    
    
    $scope.PipeTypeName = {0:"SP",1:"CML-DIP",2:"CIP/DIP",3:"PE/PVC/GRP",4:"SP",5:"CIP/DIP",6:"PE/PVC/GRP",7:"DCIP"};
    
    $scope.imp_plan_pipe_nm = function(pipe_type){
    	return $scope.PipeTypeName[pipe_type];
    } 
    
    $scope.StatusGrade = {1:"I 노후진행(소)",2:"II 노후진행(주)",3:"III 노후진행(대)"};
    
    $scope.imp_plan_grade_nm = function(status_grade){
    
    	return $scope.StatusGrade[status_grade];
    }
    
    $scope.status_grade = [];
    
    $scope.getStatusGradeNm = function(){
    	
    	
    	$.each($scope.status_grade,function(idx,item){
    		//if(item.sign
    	});
    	return $scope.imp_plan_grade_nm(1); 
    }     
    
}

function showDetailResultPopup(est_sid,asset_sid,stategrade){
	if(parseInt(stategrade) > 0 ){
	//$("#DetailResultIndirEstimationPopup").show();
	var scope = angular.element(document.getElementById('DetailResultIndirEstimationPopup')).scope();
	scope.showDetailResultPopup(est_sid,asset_sid,stategrade);
	}		 
}