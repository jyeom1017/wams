angular.module('app.diagnosis').controller('lccController', lccController);

function lccController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message,$filter) {

	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 6개월
        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.Serch.Info.START_DT = s_yyyymmdd;
        $('#Serch_START_DT').val(s_yyyymmdd);
        $scope.Serch.Info.END_DT = e_yyyymmdd;
        $('#Serch_END_DT').val(e_yyyymmdd);
    }
	
	$scope.$on('$viewContentLoaded', function () {
		$scope.setWeek();
		 setDatePicker();
		 grid.setGrid();
	     mainDataService.CommunicationRelay("/lcc/getList.json", {page : 1,  rows: 1}).success(function(map) {
	    	 console.log(map)
	    	 Common_Swap.B_A(map.rows[0], lcc.Info);
	    	 lccNew.Info.LCC_NM = '';
	    	 mainDataService.getAssetLevelList({}).success(function(data){
	 	 		 $scope.assetLevelList  = data
	 	 		 mainDataService.getBlockCodeList({}).success(function (obj) {
	 	  			if (!common.isEmpty(obj.errorMessage)) {
	 	  				alertify.error('errMessage : ' + obj.errorMessage);
	 	  			} else {
	 	  				$scope.blockList = obj;
	 	  				grid.setGrid1(map.rows[0].LCC_SID);
	 	  				grid.setGrid2(map.rows[0].LCC_SID);
	 	  				grid.setGrid3(map.rows[0].LCC_SID);
	 	  			}
	 	  		});
	 	 	 });
	     });
	   
	     Common_Modal.Init($compile, $scope, {
			id: '#list3',
			layerId: '#modal',
			hidenSize: 0
 		 });
	   
	    $scope.MCODE = '0306';
	    $rootScope.setBtnAuth($scope.MCODE);
	   
	    GraphModal.Init([]);
 	});
	
	
	let Serch = {
			Info : {
				OPTION : 1, 
				YEAR : "", 
				START_DT : "", 
				END_DT : "",
				EST_NM : ''
			},
			Lookup : function() {
				if(this.Info.OPTION == 2 && this.Info.START_DT > this.Info.END_DT){
					alertify.alert("오류","시작날짜와 종료날짜를 확인해 주세요.");
					return;
				}
				console.log(this.Info)
				$('#list').jqGrid('setGridParam', {postData : this.Info, page : 1}).trigger('reloadGrid');	
			},
			Delete : function() {
				alertify.confirm('삭제 확인','작성하신 자료를 삭제 하시겠습니까?', function() {
					mainDataService.CommunicationRelay("/lcc/deleteLcc.json", lcc.Info).success(function(param) {
						if(Common_AlertStr(param)) return;
						
						$('#list').jqGrid('setGridParam', Serch.Info).trigger('reloadGrid');
					});
				}, function() {});
			},
			Output : function() {
				let jobType = $state.current.name+ "_" + 1;
				Common_Excel.ExcelDown(jobType, Serch.Info);
			},
			Select : function(option) {
				console.log(option)
				this.Info["OPTION"] = option;
			}
	}
	 
	let GraphModal = {
				//id
				ID : 'Graph',
				//초기화
				Init : function(dataArray = []) {
					rMateChartH5.create("chart1", this.ID, "", "100%", "100%");
					
					this.Set(dataArray);
					 
					rMateChartH5.registerTheme(rMateChartH5.themes);
				},
				//셋팅
				Set : function(dataArray = []) {
					console.log(dataArray)
					let min = dataArray.length > 0 ? dataArray[0].TOTAL : 0;
					let max = dataArray.length > 0 ? dataArray[dataArray.length-1].TOTAL : 0;
					min =  (max - min) != 0 ? Math.round(min - (max - min) * 0.4) : 0
					console.log(min);
					$('#chart1').height($('#Graph').height());
					$('#chart1').width($('#Graph').width());
					rMateChartH5.calls("chart1", {
		 				"setLayout" : this.GetLayer(min),
		 				"setData" : dataArray
		 				//"setData" : [{YYYY: 1, TOTAL_COST:2 , TOTAL: 3}, {YYYY: 2, TOTAL_COST:3 , TOTAL: 4}]
					});
				},
				//레이어
				GetLayer : function(min){
		 			return '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
		            +'<Options>'
		            +'<Caption text=""/>'
		              +'<Legend useVisibleCheck="true"/>'
		         +'</Options>'
		       +'<NumberFormatter id="numFmt1" useThousandsSeparator="true"/>'
		       +'<Combination2DChart showDataTips="true">'
		       /*
		            Combination2D 차트 생성시에 필요한 Combination2DChart 정의합니다
		            showDataTips : 데이터에 마우스를 가져갔을 때 나오는 Tip을 보이기/안보이기 속성입니다
		       */
		            +'<horizontalAxis>'
		                 +'<CategoryAxis categoryField="YYYY" padding="0.5"/>'
		              +'</horizontalAxis>'
		            +'<verticalAxis>'
		               +'<LinearAxis id="vAxis1"  formatter="{numFmt1}"/>'
		               //+'<LinearAxis id="vAxis2"  formatter="{numFmt1}"/>'
		              +'</verticalAxis>'
		              +'<series>'
		                 +'<Column2DSeries yField="TOTAL_COST" displayName="연도 별합계">'
		                    +'<fill>'
		                       +'<SolidColor color="#41b2e6"/>'
		                    +'</fill>'
		                      +'<showDataEffect>'
		                         +'<SeriesInterpolate/>'
		                     +'</showDataEffect>'
		                +'</Column2DSeries>'
		                +'<Line2DSeries yField="TOTAL" displayName="연간누적">'
		                   +'<verticalAxis>'
		                   //title="연간 누적"
		                       +'<LinearAxis id="vAxis2" minimum="'+min+'" formatter="{numFmt1}" />'
		                      +'</verticalAxis>'
		                      +'<showDataEffect>'
		                         +'<SeriesInterpolate/>'
		                     +'</showDataEffect>'
		                    +'<lineStroke>'
		                         +'<Stroke color="#f9bd03" weight="4"/>'
		                     +'</lineStroke>'
		                    +'<stroke>'
		                         +'<Stroke color="#f9bd03" weight="3"/>'
		                     +'</stroke>'
		                +'</Line2DSeries>'
		              +'</series>'
		            +'<verticalAxisRenderers>'
		                  +'<Axis2DRenderer axis="{vAxis1}"   placement="left" showLine="false"/>'
		               +'<Axis2DRenderer axis="{vAxis2}" placement="right" showLine="true"/>'
		           +'</verticalAxisRenderers>'
		         +'</Combination2DChart>'
		    +'</rMateChart>';
			}
	}
				
	var chkcell={cellId:undefined, chkval:undefined};
    let NumberFormat =  function ( cellvalue , options , rowdata )  { 
        	return  cellvalue === null ? '' : $filter('number')(cellvalue,0);
	}
	    
    let totalFormat = function(cellvalue , options , rowdata){
    	let INIT_COST = Number(rowdata["INIT_COST"]);
 		let OP_COST = Number(rowdata["OP_COST"]);
 		let MT_COST = Number(rowdata["MT_COST"]);
 		let RE_COST = Number(rowdata["RE_COST"]);
 		let IMP_COST = Number(rowdata["IMP_COST"]);
 		let DI_COST = Number(rowdata["DI_COST"]);
 		let total = INIT_COST + OP_COST + MT_COST + RE_COST + IMP_COST + DI_COST;
 		return total === 0 ? 0 : $filter('number')(total,0)
    }
     
	let grid = {
		 setGrid: function(){
			 return pagerJsonGrid({
		            grid_id : 'list',
		            pager_id : 'listPager',
		            url : '/lcc/getList.json',
		            condition : {
		                page : 1,
		                rows : GridConfig.sizeL
		            },
		            rowNum : 20,
		            colNames : [ '순번', '분석명', '분석기간', '작성자','작성일자','sid','LCC_DT'],
		            colModel : [
		            {name : 'RNUM', width : "40px", resizable: false}, 
		            {name : 'LCC_NM',width : 150, resizable: false},
		            {name : 'LCC_DT',width : 80, resizable: false},
		            {name : 'INSERT_ID', width : 80, resizable: false}, 
		            {name : 'INSERT_DT', width : 80, resizable: false}, 
		            {name : 'LCC_SID', width : 0, hidden : true}, 
		            {name : 'LCC_DT', width : 0, hidden : true}, 
		            ],
	            	onSelectRow : function(id) {
	            		lccDetails.Info.ASSET_SID='';
	            		Common_Swap.B_A($('#list').jqGrid('getRowData', id), lcc.Info);
	            		lcc.Info.FN_CD = 'F'
	            		$('#list1').jqGrid('setGridParam', {postData : lcc.Info, page : 1}).trigger('reloadGrid');
	            		lcc.Info.FN_CD = 'N'
	            		$('#list2').jqGrid('setGridParam', {postData : lcc.Info, page : 1}).trigger('reloadGrid');
		            	
		            	let info = angular.copy(Common_Modal.GetSQL());
		            	if(!info) info = {};
		            	info["CLASS3_CD"] = $scope.CLASS3_CD;
		            	info["CLASS4_CD"] = $scope.CLASS4_CD;
		            	info["LCC_SID"] = lcc.Info.LCC_SID;
		            	mainDataService
			            	.CommunicationRelay("/lcc/getTotal.json", info)
			            	.success(function(map) {
			            		$('#list3').jqGrid('setGridParam', {postData : lcc.Info, page : 1}).trigger('reloadGrid');
			            		$scope.TempParam = map;
			            	});
		            	
		            	$scope.$apply();
		                
		            },
		            loadComplete: function(){
						var grid = $('#list')
						var ids = grid.jqGrid("getDataIDs");
						if(ids && ids.length > 0){
			    			grid.jqGrid("setSelection", ids[0]);
						}
					}
		        });
		 },
		 setGrid1: function(sid){
			 return pagerJsonGrid({
		            grid_id : 'list1',
		            pager_id : 'list1Pager',
		            url : '/lcc/getResultList.json',
		            condition : {
		            	page : 1,
		            	FN_CD:'F',
		            	LCC_SID: sid,
			   			rows : GridConfig.sizeXS-4
		            },
		            footerrow: true, 
		            userDataOnFooter : true,
		            rowNum : GridConfig.sizeXS-4,
		            colNames : [ '구분','초기투자비','운영비','유지비','수리비','개량비','해체폐기비','합계','asid','sid'],
		            colModel : [
		            {name : 'BL_01', width : 0, resizable: false,
		            	formatter:  function ( cellvalue , options , rowdata )  {
		            		if(cellvalue === '합계') return '합계';
		            		
		            		let Array = [];
		            		
	                		Array = $filter('filter')( $scope.assetLevelList,{FN_CD:'F', LEVEL_STEP: 4, LEVEL_CD : cellvalue});
		            		
	                		let result = Array.length === 0 ? cellvalue :  Array[0].LEVEL_NM;
	                		
		            		return result ? result:'';
		            	}
		            }, 
		            {name : 'INIT_COST',width : 0, formatter: NumberFormat, resizable: false}, 
		            {name : 'OP_COST',width : 0, formatter: NumberFormat, resizable: false},
		            {name : 'MT_COST',width : 0, formatter: NumberFormat, resizable: false},
		            {name : 'RE_COST',width : 0, formatter: NumberFormat, resizable: false},
		            {name : 'IMP_COST',width : 0, formatter: NumberFormat, resizable: false},
		            {name : 'DI_COST',width : 0, formatter: NumberFormat, resizable: false},
		            {name : 'TOTAL_COST',width : 0, formatter: totalFormat, resizable: false},
		            {name : 'ASSET_SID',width : 0,hidden : true},
		            {name : 'LCC_SID',width : 0,hidden : true},
		            ],
		            loadComplete: function(){
		            	//계속 호출 이상함 느려지면 변경
		            	mainDataService.CommunicationRelay("/lcc/getResultTotal.json", {FN_CD: 'F', LCC_SID: lcc.Info.LCC_SID}).success(function(map) {
		            		//console.log(map)
		            		let INIT_COST = map["INIT_COST"];
		            		let OP_COST = map["OP_COST"];
		            		let MT_COST = map["MT_COST"];
		            		let RE_COST = map["RE_COST"];
		            		let IMP_COST = map["IMP_COST"];
		            		let DI_COST = map["DI_COST"];
		            		
		            		$("#gview_list1 table.ui-jqgrid-ftable td").css("text-align", "center");
		            		
		            		 $('#list1').jqGrid('footerData', 'set', {
		            			 BL_01:'합계', 
		            			 INIT_COST: INIT_COST,
		            			 OP_COST: OP_COST,
		            			 MT_COST: MT_COST,
		            			 RE_COST: RE_COST,
		            			 IMP_COST: IMP_COST, 
		            			 DI_COST: DI_COST,
		            			 TOTAL_COST:(INIT_COST + OP_COST + MT_COST + RE_COST + IMP_COST + DI_COST)
	            			 });
		            	});
		            },
	            	onSelectRow : function(id) {
	            		let param = $('#list1').jqGrid('getRowData', id);
	            		Common_Swap.B_A(param, lccDetails.Info);
	            		lccDetails.Info.TITLE = param.BL_01; 
	            		lccDetails.Info.GB_CD = $scope
	            								.assetLevelList
	            								.filter(a=>a.LEVEL_NM == param.BL_01)[0]
	            								.LEVEL_CD
		            },
		            ondblClickRow: function (rowid, iRow, iCol) {
		            	lccDetails.GraphShow();
		            }
		        });
		 },
		 setGrid2: function(sid){
			 return pagerJsonGrid({
		            grid_id : 'list2',
		            pager_id : 'list2Pager',
		            url : '/lcc/getResultList.json',
		            condition : {
		            	page : 0,
		            	FN_CD:'N',
		            	LCC_SID: sid,
			   			rows : GridConfig.sizeXS-4
		            },
		            footerrow: true, 
		            userDataOnFooter : true,
		            rowNum : GridConfig.sizeXS-4,
		            colNames : ['중블록', '소블록','초기투자비','운영비','유지비','수리비','개량비','해체폐기비','합계','asid','sid'],
		            colModel : [
	            	{name : 'BL_01', width : 15, resizable: false,
	            		cellattr: function(rowid, val, rowObject, cm, rdata){
		                    var result = "";
		                    
		                    if(!val) return result;
		                    
		                    result = 'style="display:none"  rowspanid="'+chkcell.cellId+'"';
		                    
		                    if(chkcell.chkval != val){ //check 값이랑 비교값이 다른 경우
		                        var cellId = this.id + '_row_'+rowid+'-'+cm.name;
		                        result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
		                        chkcell = {cellId:cellId, chkval:val};
		                    }
		                    
		                    return result;
	            		},
	            		formatter:  function ( cellvalue , options , rowdata )  { 
		            		if(cellvalue === '합계') return '합계';
		            		
		            		if(cellvalue == 0) return cellvalue;
		            		
		            		let Array = [];
		            		
	                		Array = $filter('filter')( $scope.blockList,{BL_BCODE: cellvalue});
		            		
	                		let result = Array.length === 0 ? cellvalue :  Array[0].BL_NAME;
	                		
		            		return result ? result:'';
		            	}
            		}, 
		            {name : 'BL_02', width : 15, resizable: false,
		            	formatter:  function ( cellvalue , options , rowdata )  { 
		            		if(cellvalue === '합계') return '합계';
		            		if(cellvalue == 0) return cellvalue;
		            		
		            		let Array = [];
		            		
	                		Array = $filter('filter')( $scope.blockList,{BL_BCODE: cellvalue});
		            		
	                		let result = Array.length === 0 ? cellvalue :  Array[0].BL_NAME;
	                		
		            		return result ? result:'';
		            	}
            		}, 
		            {name : 'INIT_COST',width : 30, formatter: NumberFormat, resizable: false}, 
		            {name : 'OP_COST',width : 30, formatter: NumberFormat, resizable: false},
		            {name : 'MT_COST',width : 30, formatter: NumberFormat, resizable: false},
		            {name : 'RE_COST',width : 30, formatter: NumberFormat, resizable: false},
		            {name : 'IMP_COST',width : 30, formatter: NumberFormat, resizable: false},
		            {name : 'DI_COST',width : 30, formatter: NumberFormat, resizable: false},
		            {name : 'TOTAL_COST',width : 30, formatter: totalFormat, resizable: false},
		            {name : 'ASSET_SID',width : 0,hidden : true},
		            {name : 'LCC_SID',width : 0,hidden : true},
		            ],
		            loadComplete: function(){
		            	//계속 호출 이상함 느려지면 변경
		            	mainDataService.CommunicationRelay("/lcc/getResultTotal.json", {FN_CD: 'N', LCC_SID: lcc.Info.LCC_SID}).success(function(map) {
		            		console.log(map)
		            		let INIT_COST = map["INIT_COST"];
		            		let OP_COST = map["OP_COST"];
		            		let MT_COST = map["MT_COST"];
		            		let RE_COST = map["RE_COST"];
		            		let IMP_COST = map["IMP_COST"];
		            		let DI_COST = map["DI_COST"];
		            		let TOTAL_COST = (INIT_COST + OP_COST + MT_COST + RE_COST + IMP_COST + DI_COST);
		            		let models = $('#list2').jqGrid('getGridParam', 'colModel');
		            		let width = models[0].width + models[1].width;
		            		
		            		$("#gview_list2 table.ui-jqgrid-ftable td").css("text-align", "center");
		            		$('#gview_list2 table.ui-jqgrid-ftable td:eq(0)').hide();
		            		$('#gview_list2 table.ui-jqgrid-ftable td:eq(1)').width(width-5);
		            		$('#list2').jqGrid('footerData', 'set', {
		            			 BL_02:'합계', 
		            			 INIT_COST: INIT_COST,
		            			 OP_COST: OP_COST,
		            			 MT_COST: MT_COST,
		            			 RE_COST: RE_COST,
		            			 IMP_COST: IMP_COST, 
		            			 DI_COST: DI_COST,
		            			 TOTAL_COST: TOTAL_COST
	            			 });
		            	});
		            	chkcell.chkval = undefined;
		            },
		            gridComplete: function() {  /** 데이터 로딩시 함수 **/
		                var grid = this;
		                $('td', grid).each(function() {
		                    var spans = $('td[rowspanid="'+this.id+'"]',grid).length+1;
		                    if(spans>1){
		                     $(this).attr('rowspan',spans);
		                    }
		                });    
		            },  
	            	onSelectRow : function(id) {
	            		let param = $('#list2').jqGrid('getRowData', id);
	            		Common_Swap.B_A(param, lccDetails.Info);
	            		lccDetails.Info.TITLE = param.BL_02; 
	            		lccDetails.Info.GB_CD = $scope
	            								.blockList
	            								.filter(a=>a.BL_NAME == param.BL_02 && a.LEV == '3')
	            								.map(a=>a.BL_PBCODE + '_'+ a.BL_BCODE)[0]
	            		
		            },
		            ondblClickRow: function (rowid, iRow, iCol) {
		            	lccDetails.GraphShow();
		            }
		        });
		 },
		 setGrid3: function(sid){
			 return pagerJsonGrid({
		            grid_id : 'list3',
		            pager_id : 'list3Pager',
		            url : '/lcc/getAssetList.json',
		            condition : {
		            	page : 1,
		            	LCC_SID: sid,
		            	CLASS4_CD:'',
		            	CLASS3_CD:'',
		            	OPTION: '0',
			   			rows : GridConfig.sizeXS-5
		            },
		            footerrow: true, 
		            userDataOnFooter : true,
		            rowNum : GridConfig.sizeXS-5,
		            colNames : ['순번', '자산명','초기투자비','운영비','유지비','수리비','개량비','해체폐기비','합계','asid','LCC_SID'],
		            colModel : [
	            	{ name : 'RNUM', width : 30 , resizable: false},
		            {name : 'ASSET_NM', width : 200, resizable: false}, 
		            {name : 'INIT_COST',width : 80, formatter: NumberFormat, resizable: false}, 
		            {name : 'OP_COST',width : 80, formatter: NumberFormat, resizable: false},
		            {name : 'MT_COST',width : 80, formatter: NumberFormat, resizable: false},
		            {name : 'RE_COST',width : 80, formatter: NumberFormat, resizable: false},
		            {name : 'IMP_COST',width : 80, formatter: NumberFormat, resizable: false},
		            {name : 'DI_COST',width : 80, formatter: NumberFormat, resizable: false},
		            {name : 'TOTAL_COST_AMT',width : 80, formatter: totalFormat, resizable: false},
		            {name : 'ASSET_SID',width : 0,hidden : true},
		            {name : 'LCC_SID',width : 0,hidden : true},
		            ],
	            	onSelectRow : function(id) {
	            		Common_Swap.B_A($('#list3').jqGrid('getRowData', id), lccDetails.Info);
	            		lccDetails.Info.TITLE = lccDetails.Info.ASSET_NM; 
	            		console.log(lccDetails.Info)
		            },
		            ondblClickRow: function (rowid, iRow, iCol) {
		            	 lccDetails.GraphShow();
		            },
		            beforeProcessing: function(data,status,xhr){
		            	if(data.errMessage) return;
		            	$scope.TempParam = data.rows.filter(a => a.ASSET_NM == '합계')[0];
		            	const nm = data.rows[data.rows.length -1].ASSET_NM;
		            	data.rows = (nm == '합계') ? data.rows.slice(0, data.rows.length -1) : data.rows.filter(a => a.ASSET_NM != '합계');
		            },
		            loadComplete:function(){
		            	//console.log(map)
	            		let models = $('#list3').jqGrid('getGridParam', 'colModel');
	            		let width = models[0].width + models[1].width;
	            		let map = $scope.TempParam;
	            		$("#gview_list3 table.ui-jqgrid-ftable td").css("text-align", "center");
	            		$('#gview_list3 table.ui-jqgrid-ftable td:eq(0)').hide();
	            		$('#gview_list3 table.ui-jqgrid-ftable td:eq(1)').width(width-5);
	            		$('#list3').jqGrid('footerData', 'set', {
	            			 RNUM:'합계',
	            			 ASSET_NM:'합계', 
	            			 INIT_COST: map["INIT_COST"],
	            			 OP_COST: map["OP_COST"],
	            			 MT_COST: map["MT_COST"],
	            			 RE_COST: map["RE_COST"],
	            			 IMP_COST: map["IMP_COST"], 
	            			 DI_COST: map["DI_COST"], 
	            			 TOTAL_COST_AMT: map["TOTAL_COST_AMT"]
            			 });
		            }
		        });
		 },
	}
	 	 
	let lcc = {
		 Info: {
			 page : 1,
			 LCC_SID: 0,
			 LCC_DT:'',
			 LCC_NM: '',
			 INSERT_ID: '',
			 INSERT_DT:'',
			 OPTION: '0',
			 CLASS3_CD: '',
			 CLASS4_CD: '',
			 LEVEL_PATH_CD:'',
			 rows : GridConfig.sizeXS-4
		 },
		 facilityList : null, 
		 pipelineList : null,
		 New: function() {
			 lccNew.Init();
		 },
		 Report: function() {
			 let jobType = $state.current.name + "_big"
			 console.log(jobType);
			 //Common_Excel.ExcelDown(jobType, this.Info);
			 window.open('/bigexcel/downloadBigExcel.do?EXCEL_ID='+jobType+'&LCC_SID=' + this.Info.LCC_SID,'_blank');
			 
			/*let jobType = $state.current.name+ "_" + 2;
			window.open('/excel/downloadExcel.do?EXCEL_ID=' + jobType + '&LCC_SID=' + this.Info.LCC_SID, '_blank');
			//Common_Excel.ExcelDown(jobType, this.Info);
*/		 },
		 Details: function() {
			 if($scope.Page == 2 && lccDetails.Info.ASSET_SID == '')return;
			 if($scope.Page != 2 && lccDetails.Info.GB_CD == '')return;
			 lccDetails.GraphShow();
		},
	}
	 
	let lccDetails = {
		Info:{
			TITLE: '',
			ASSET_SID: '',
			LCC_SID: '',
			ASSET_NM: '',
			GB_CD:'',
		},
		isShow : false,
		List: null,
		GraphShow: function() {
			let param = angular.copy(lccDetails.Info);
			param.page = $scope.Page;
			mainDataService.CommunicationRelay("/lcc/getDetails.json", param).success(function(map) {
				console.log(map)
				lccDetails.List = map.List;
				lccDetails.Sum = lccDetails.List[lccDetails.List.length -1];
				lccDetails.List.splice(lccDetails.List.length -1);
				GraphModal.Set(lccDetails.List);
				lccDetails.isShow = true;
			});
		},
		Cancel: function() {
			this.isShow = false;
		},
		Report : function() {
			if($scope.Page == 2)
				window.open('/excel/downloadExcel.do?EXCEL_ID=010306&ASSET_SID=' + this.Info.ASSET_SID+'&LCC_SID='+ this.Info.LCC_SID,'_blank');
			else 
				window.open('/excel/downloadExcel.do?EXCEL_ID=010306&GB_CD=' + this.Info.GB_CD+'&TITLE=' + this.Info.TITLE+'&LCC_SID='+ this.Info.LCC_SID,'_blank');
		},
		Confirm: function() {
			this.Cancel();
		}
	}
	 
	let lccNew = {
		 Info:{
			 LCC_NM: '',
			 INSERT_ID: '',
			 INSERT_DT: '',
			 LCC_DT: '',
			 LCC_DATE: new Date("2022-01-01"),
			 Msg:'시작버튼을 눌러주세요.',
			 PauseMsg:''
		 },
		 isShow: false,
		 isPause: true,
		 isPrepare: true,
		 ServiceInfo: {},
		 Init: function() {
			 this.isShow = true;
			 mainDataService.CommunicationRelay("/lcc/insertList.json").success(function(info) {
				 console.log(info)
				 lccNew.Info = info;
				 lccNew.Set(info.LCC_SID, '시작버튼을 눌러주세요.');
				 lccNew.Info.LCC_DATE = new Date(info.LCC_DT);
	 		 });
		 },
		 Set: function(sid, msg) {
			 /*lccNew.ServiceInfo = {
					 LCC_SID: 91,
					 FN_CD: 'N',
					 GB_CD: undefined,
					 LCC_DT: lccNew.Info.LCC_DT,
					 PERCENT: 0,
					 TICK: 100,
					 ASSET_A: 0,
					 ASSET_B: 0,
					 totalCount: 66,
					 Loading_index: 3,
					 Loading_ins: msg 
			 };*/
			 lccNew.ServiceInfo = {
					 LCC_SID: sid,
					 FN_CD: undefined,
					 GB_CD: undefined,
					 LCC_DT: lccNew.Info.LCC_DT,
					 PERCENT: 0,
					 TICK: 100,
					 ASSET_A: 0,
					 ASSET_B: 100,
					 totalCount: 75000,
					 Loading_index: 0,
					 Loading_ins: msg 
			 };
			 lccNew.Info.Msg = msg;
			 $("#progressbar").progressbar({value: 0 });
			
			 lccNew.isPause = true;
			 lccNew.isPrepare = true;
		 },
	 	 Start: function() {
	 		if(lccNew.Info.LCC_NM === '' || lccNew.Info.LCC_NM == null || lccNew.Info.LCC_NM == undefined){
	 			alertify.alert('',"분석명은 필수 입니다.", function(){});
	 			return;
	 		}
	 		let date =  this.Info.LCC_DATE.toISOString();
	 		this.Info.LCC_DT = date.substr(0,10);
	 		lccNew.Info.PauseMsg = '';
	 		lccNew.Info.LCC_NM = xssFilter(lccNew.Info.LCC_NM);
	 		
	 		mainDataService.CommunicationRelay("/lcc/startList.json", this.Info).success(function(info) {
	 			lccNew.Set(info.LCC_SID, "ASSET (" + lccNew.ServiceInfo.ASSET_A +'/' + lccNew.ServiceInfo.totalCount + ")");
	 			lccNew.isPause = false;
	 			lccNew.Service(lccNew.ServiceInfo);
	 		});
	 	 },
	 	 Service: function(info){
	 		if(lccNew.isPause) return;
	 		lccNew.isPrepare = false;
	 		//lccNew.Info.LCC_NM = xssFilter(lccNew.Info.LCC_NM);
	 		mainDataService
	 		.CommunicationRelay("/lcc/progressbarLcc.json", info)
	 		.success(function(param) {
	 			lccNew.isPrepare = true;
	 			if(param.errMessage)return;
	 			
	 			param.ASSET_A = param.ASSET_B;
 				param.ASSET_B = param.ASSET_A + param.TICK;
 				
	 			if(param.Loading_index == 0)
	 				param.PERCENT = param.ASSET_A / param.totalCount * 100;
	 			else
	 				param.PERCENT = (param.ASSET_A / param.TICK) / param.totalCount* 100;
	 			
 				if(param.PERCENT >= 100) param.PERCENT = 100;
 				
		 		$("#progressbar").progressbar({value: (Number(param.PERCENT) + (100 * param.Loading_index)) / 3});
		 		
	 			if(param.PERCENT === 100) {
	 				param.Loading_index++;
	 				param.ASSET_A = 0;
	 				param.ASSET_B = param.ASSET_A + param.TICK
	 				param.FN_CD = undefined;
	 				 if(param.Loading_index === 1) {
	 					param.FN_CD = 'F'
		 				param.totalCount = $scope
				 					 	   .assetLevelList
					 					   .filter(a=>a.LEVEL_STEP == 4 && a.FN_CD == 'F')
					 					   .length - 1;
	 					param.GB_CD = $scope
						 .assetLevelList
	 					 .filter(a=>a.LEVEL_STEP == 4 && a.FN_CD == 'F')[param.ASSET_A / param.TICK]
	 					 .LEVEL_CD;
	 					
		 			}else if(param.Loading_index === 2) {
		 				param.FN_CD = 'N'
		 				param.totalCount = $scope
							 			   .blockList
							 			   .filter(a=>a.LEV == 3)
							 			   .length - 1;
		 				param.GB_CD = $scope
	 					.blockList
	 					.filter(a=>a.LEV == 3)
	 					.map(a=> a.BL_PBCODE +'_' + a.BL_BCODE)[param.ASSET_A / param.TICK]
		 				
		 			}else if(param.Loading_index === 3) {
		 				alertify
		 				.alert('',"성공적으로 작업이 완료되었습니다.", function(){lccNew.Reset()});
		 				return;
		 			}
	 				
	 				lccNew.Service(param);
	 				return;
 				}
	 			
	 			lccNew.Info.Msg = param.Loading_ins + lccNew.Info.PauseMsg;
 				
 				if(param.Loading_index === 1) {
 					param.GB_CD = $scope
								 .assetLevelList
			 					 .filter(a=>a.LEVEL_STEP == 4 && a.FN_CD == 'F')[param.ASSET_A / param.TICK]
 								 .LEVEL_CD;
 					
 				}else if(param.Loading_index === 2) {
 					param.GB_CD = $scope
			 					  .blockList
			 					  .filter(a=>a.LEV == 3)
			 					  .map(a=> a.BL_PBCODE +'_' + a.BL_BCODE)[param.ASSET_A / param.TICK]
 				}
	 			
	 			lccNew.Service(param);
	 		});
	 	 },
	 	 Pause: function(){
	 		if(this.isPause)return;
	 		this.isPause = true;
	 		lccNew.Info.PauseMsg = ' (정지중...)';
	 		lccNew.Info.Msg = '잠시만 기다려주세요.';
	 	 },
	 	 X_Cancel:function() {
	 		if(lccNew.isPause && lccNew.isPrepare) {
	 			lccNew.isPause = false;
	 			lccNew.ServiceInfo.PERCENT = 100;
		 		lccNew.ServiceInfo.Loading_index = 1;
		 		lccNew.ServiceInfo.ASSET_A = lccNew.ServiceInfo.totalCount;
		 		lccNew.Service(lccNew.ServiceInfo);
	 		}else{
	 			if(!lccNew.isPause){
	 				alertify.alert('',"프로그램을 정지 시킨 후 이용하세요.", function(){});
	 			}else{
	 				alertify.alert('','잠시만 기다려주세요.', function(){});
	 			}
	 		}
	 	 },
	 	 Cancel:function() {
	 		if(lccNew.isPause && lccNew.isPrepare){
	 			mainDataService.CommunicationRelay("/lcc/deleteLcc.json", lccNew.Info).success(function(param) {});
		 		this.isShow = false;
	 		}else{
	 			if(!lccNew.isPause){
	 				alertify.alert('',"프로그램을 정지 시킨 후 이용하세요.", function(){});
	 			}else{
	 				alertify.alert('','잠시만 기다려주세요.', function(){});
	 			}
	 		}
	 	 },
	 	 Reset: function() {
	 		alertify.success('성공');
	 		$('#list').jqGrid('setGridParam', {page : 1}).trigger('reloadGrid');
	 		lccNew.isShow = false; 
	 		$scope.$apply();
	 	 }
	}
	 
	$scope.lccNew = lccNew;
	$scope.Serch = Serch;
	$scope.Page = 0;
	$scope.lcc = lcc;
	$scope.lccDetails = lccDetails;
}
