angular.module('app.diagnosis').controller('state2DiagController', state2DiagController);//직접상태평가

function state2DiagController($scope, $state, $stateParams, mainDataService, $rootScope, $compile,$filter, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	
	$scope.EstimationInfo = {};
	$scope.EditInfo = {};
	$scope.gradePipeCategory = [{name:'SP(강관)',cd:'0'}
		,{name:'CML-CIP',cd:'1'}
		,{name:'CIP/DIP',cd:'2'}
		,{name:'비금속관',cd:'3'}
		,{name:'유리섬유복합관',cd:'4'}
		,{name:'콘크리트관',cd:'5'}
	];
	
	$scope.saveType = '';
	$scope.MCODE = '1506';
	$scope.selectedGradePipeCd = "0";
	$scope.selectGradePipe = function (cd){
		$scope.selectedGradePipeCd = cd;
	}
	$scope.factorDetail = [{RN:3,ITEM_CD:'00001',ITEM_GB:'00001',ITEM_NM:'00001',TYP0:'Y',TYP1:'Y',TYP2:'Y',TYP3:'Y',TYP4:'Y',TYP5:'Y'}
	,{RN:0,ITEM_CD:'00002',ITEM_GB:'00001',ITEM_NM:'00002',TYP0:'Y',TYP1:'Y',TYP2:'Y',TYP3:'Y',TYP4:'Y',TYP5:'Y'}
	,{RN:0,ITEM_CD:'00003',ITEM_GB:'00001',ITEM_NM:'00003',TYP0:'Y',TYP1:'Y',TYP2:'Y',TYP3:'Y',TYP4:'Y',TYP5:'Y'}
	,{RN:1,ITEM_CD:'00004',ITEM_GB:'00001',ITEM_NM:'00004',TYP0:'Y',TYP1:'Y',TYP2:'Y',TYP3:'Y',TYP4:'Y',TYP5:'Y'}
	]; 
	//$scope.factorSelectOptions = [{name:"O",value:"Y"},{name:"X",value:"N"}];
	$scope.factorSelectOptions = ["Y","N"];
	$scope.dataGradeList =[];
	/*
	{EST_IMP_SID:0,PIPE_TYPE:'0',STATUS_GRADE:'1',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'0',STATUS_GRADE:'2',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'0',STATUS_GRADE:'3',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'0',STATUS_GRADE:'4',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'0',STATUS_GRADE:'5',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'1',STATUS_GRADE:'1',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'1',STATUS_GRADE:'2',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'1',STATUS_GRADE:'3',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'1',STATUS_GRADE:'4',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'1',STATUS_GRADE:'5',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'2',STATUS_GRADE:'1',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'2',STATUS_GRADE:'2',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'2',STATUS_GRADE:'3',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'2',STATUS_GRADE:'4',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'2',STATUS_GRADE:'5',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'3',STATUS_GRADE:'1',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'3',STATUS_GRADE:'2',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'3',STATUS_GRADE:'3',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'3',STATUS_GRADE:'4',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'3',STATUS_GRADE:'5',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'4',STATUS_GRADE:'1',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'4',STATUS_GRADE:'2',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'4',STATUS_GRADE:'3',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'4',STATUS_GRADE:'4',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'4',STATUS_GRADE:'5',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'5',STATUS_GRADE:'1',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'5',STATUS_GRADE:'2',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'5',STATUS_GRADE:'3',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'5',STATUS_GRADE:'4',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}
	,{EST_IMP_SID:0,PIPE_TYPE:'5',STATUS_GRADE:'5',STATUS_CASE:'상태조건',PLAN_TXT:'개랑방안내용',C_MEMO:'비고'}	
	*/
	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 6개월
        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.StartDt = s_yyyymmdd;
        $('#start_dt').val(s_yyyymmdd);
        $scope.EndDt = e_yyyymmdd;
        $('#end_dt').val(e_yyyymmdd);
    }
		
	 $scope.$on('$viewContentLoaded', function () {
	    	$("#tab1_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab3_btn").removeClass("active");
	    		$("#tab1").show();
	    		$("#tab2, #tab3").hide();
	    	});
	    	$("#tab2_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab1_btn, #tab3_btn").removeClass("active");
	    		$("#tab2").show();
	    		$("#tab1, #tab3").hide();
	    	});
	    	$("#tab3_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab1_btn").removeClass("active");
	    		$("#tab3").show();
	    		$("#tab1, #tab2").hide();
	    	});
	    	
	    	$("#tab2_1_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_btn, #tab2_3_btn, #tab2_4_btn, #tab2_5_btn, #tab2_6_btn").removeClass("active");
	    		$("#tab2_1").show();
	    		$("#tab2_2, #tab2_3, #tab2_4, #tab2_5, #tab2_6").hide();
	    	});
	    	$("#tab2_2_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_1_btn, #tab2_3_btn, #tab2_4_btn, #tab2_5_btn, #tab2_6_btn").removeClass("active");
	    		$("#tab2_2").show();
	    		$("#tab2_1, #tab2_3, #tabv4, #tab2_5, #tab2_6").hide();
	    	});
	    	$("#tab2_3_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_btn, #tab2_1_btn, #tab2_4_btn, #tab2_5_btn, #tab2_6_btn").removeClass("active");
	    		$("#tab2_3").show();
	    		$("#tab2_1, #tab2_2, #tab2_4, #tab2_5, #tab2_6").hide();
	    	});
	    	$("#tab2_4_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_btn, #tab2_1_btn, #tab2_3_btn, #tab2_5_btn, #tab2_6_btn").removeClass("active");
	    		$("#tab2_4").show();
	    		$("#tab2_1, #tab2_2, #tab2_3, #tab2_5, #tab2_6").hide();
	    	});
	    	$("#tab2_5_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_btn, #tab2_1_btn, #tab2_4_btn, #tab2_3_btn, #tab2_6_btn").removeClass("active");
	    		$("#tab2_5").show();
	    		$("#tab2_1, #tab2_2, #tab2_4, #tab2_3, #tab2_6").hide();
	    	});
	    	$("#tab2_6_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_btn, #tab2_1_btn, #tab2_4_btn, #tab2_5_btn, #tab2_3_btn").removeClass("active");
	    		$("#tab2_6").show();
	    		$("#tab2_1, #tab2_2, #tab2_4, #tab2_5, #tab2_3").hide();
	    	});
	    	
	    	$("#tab2_1_p_btn_p").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_p_btn_p, #tab2_3_p_btn_p, #tab2_4_p_btn_p, #tab2_5_p_btn_p, #tab2_6_p_btn_p").removeClass("active");
	    		$("#tab2_1_p").show();
	    		$("#tab2_2_p, #tab2_3_p, #tab2_4_p, #tab2_5_p, #tab2_6_p").hide();
	    	});
	    	$("#tab2_2_p_btn_p").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_1_p_btn_p, #tab2_3_p_btn_p, #tab2_4_p_btn_p, #tab2_5_p_btn_p, #tab2_6_p_btn_p").removeClass("active");
	    		$("#tab2_2_p").show();
	    		$("#tab2_1_p, #tab2_3_p, #tabv4, #tab2_5_p, #tab2_6_p").hide();
	    	});
	    	$("#tab2_3_p_btn_p").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_p_btn_p, #tab2_1_p_btn_p, #tab2_4_p_btn_p, #tab2_5_p_btn_p, #tab2_6_p_btn_p").removeClass("active");
	    		$("#tab2_3_p").show();
	    		$("#tab2_1_p, #tab2_2_p, #tab2_4_p, #tab2_5_p, #tab2_6_p").hide();
	    	});
	    	$("#tab2_4_p_btn_p").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_p_btn_p, #tab2_1_p_btn_p, #tab2_3_p_btn_p, #tab2_5_p_btn_p, #tab2_6_p_btn_p").removeClass("active");
	    		$("#tab2_4_p").show();
	    		$("#tab2_1_p, #tab2_2_p, #tab2_3_p, #tab2_5_p, #tab2_6_p").hide();
	    	});
	    	$("#tab2_5_p_btn_p").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_p_btn_p, #tab2_1_p_btn_p, #tab2_4_p_btn_p, #tab2_3_p_btn_p, #tab2_6_p_btn_p").removeClass("active");
	    		$("#tab2_5_p").show();
	    		$("#tab2_1_p, #tab2_2_p, #tab2_4_p, #tab2_3_p, #tab2_6_p").hide();
	    	});
	    	$("#tab2_6_p_btn_p").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_2_p_btn_p, #tab2_1_p_btn_p, #tab2_4_p_btn_p, #tab2_5_p_btn_p, #tab2_3_p_btn_p").removeClass("active");
	    		$("#tab2_6_p").show();
	    		$("#tab2_1_p, #tab2_2_p, #tab2_4_p, #tab2_5_p, #tab2_3_p").hide();
	    	});
	    	$("#tab1_1_btn").on("click", function() {
		   		$(this).addClass("active");
		   		$("#tab1_2_btn, #tab1_3_btn").removeClass("active");
		   		$("#tab1_1").show();
		   		$("#tab1_2, #tab1_3").hide();
		   		$scope.tab1Id=1;
		   		$scope.$apply();
		   	});
		   	$("#tab1_2_btn").on("click", function() {
		   		$(this).addClass("active");
		   		$("#tab1_1_btn, #tab1_3_btn").removeClass("active");
		   		$("#tab1_2").show();
		   		$("#tab1_1, #tab1_3").hide();
		   		$scope.tab1Id=2;
		   		$scope.$apply();
		   	});
		   	$("#tab1_3_btn").on("click", function() {
		   		$(this).addClass("active");
		   		$("#tab1_2_btn, #tab1_1_btn").removeClass("active");
		   		$("#tab1_3").show();
		   		$("#tab1_1, #tab1_2").hide();
		   		$scope.tab1Id=3;
		   		$scope.$apply();
		   	});
	    	
	    	setGrid();
	    	setGrid2();
	    	$scope.setWeek();
	    	getFactorList(function(data){
	    		$scope.factorSelect($scope.factorList[0]);
	    	});
	    	getGradeList(function(data){
	    		$scope.gradeSelect($scope.gradeList[0]);
	    	});
	    	
	    	$rootScope.setBtnAuth($scope.MCODE);
	    	setDatePicker();
	    	setGrid2_Popup();
	    	
/*	    	mainDataService.getCommonCodeList({
	            gcode : 'YEAR',
	            gname : ''
	        }).success(function(data) {
	            if (!common.isEmpty(data.errMessage)) {
	                alertify.error('errMessage : ' + data.errMessage);
	            } else {
	            	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
	                $scope.ComCodeYearList = data;
	            }
	        });*/
	    	
	    	mainDataService.getAssetLevelList({}).success(function(data){
		 		 $scope.assetLevelList  = data
		 		 mainDataService.getBlockCodeList({}).success(function (obj) {
		  			if (!common.isEmpty(obj.errorMessage)) {
		  				alertify.error('errMessage : ' + obj.errorMessage);
		  			} else {
		  				$scope.blockList = obj;
		  			}
		  		});
		 	 });
	    	
	    	$scope.searchModal = Common_Modal.Init($compile, $scope, {
	 			id: '#list2',
	 			layerId: '#modal',
	 			hidenSize: 1
	 	 	});
	    });
	 
	 $scope.searchOption = 1;
		$scope.SearchOption = function(opt)
		{
			$scope.searchOption = opt;
		}
	 $scope.selectYear="";
	 $scope.yearsList = [2022,2021,2020,2019,2018]; 
	 
	 $scope.factorList = [];
	 $scope.gradeList = [];
	 
	 $scope.factorCategory = [
	 {name:'SP(송수)',cd:'0'}
	 ,{name:'CML-CIP(송수)',cd:'1'}
	 ,{name:'CIP/DIP(송수)',cd:'2'}
	 ,{name:'비금속(송수)',cd:'3'}
	 ,{name:'비금속관',cd:'4'}
	 ,{name:'유리섬유복합판',cd:'5'}
	 ,{name:'콘크리트관',cd:'6'}];
	 
	 $scope.selectedTab2Id = '0';
	 $scope.tab2Click = function(cd){
		 //alert(cd);
	 	$scope.selectedTab2Id = cd;
	 	//alert($scope.selectedFactor.EST_SPEC_SID);
	 	var param = {EST_SPEC_SID: $scope.selectedFactor.EST_SPEC_SID ,PIPE_TYPE:cd};
		mainDataService.getIndirEstFactorDetail(param)
	 	.success(function(data){
	 		var ITEM_GB = data[0].ITEM_GB;
	 		$scope.GB_ARRAY = [];
	 		$scope.GB_ARRAY.push({name:data[0].ITEM_GB,cnt:1,idx:0});
	 		$.each(data,function(idx,item){
	 			if(data[idx].ITEM_GB == ITEM_GB){
	 				if(idx>0)
 					$scope.GB_ARRAY[$scope.GB_ARRAY.length-1].cnt +=1;
	 			}
	 			else{
	 				$scope.GB_ARRAY.push({name:data[idx].ITEM_GB,cnt:1,idx:idx});	 				
	 			}
	 			item.GB_Idx = $scope.GB_ARRAY.length-1;
	 			item.row_idx = $scope.GB_ARRAY[$scope.GB_ARRAY.length-1].idx;
	 			item.idx = idx;
	 		});
	 		$.each(data,function(idx,item){
	 			item.GB_cnt = $scope.GB_ARRAY[item.GB_Idx].cnt; 
	 		});	
	 		
	 		$scope.factorDetail = data;
	 		console.log(data);
	 	});	 	
	 }
	 function getFactorList(callback){

	 	var param = {category:'2'};
	 	mainDataService.getDirEstFactorList(param)
	 	.success(function(data){
	 		$scope.factorList =data.rows;
	 		if(typeof callback=='function'){
		 		callback(data);
	 		}
	 	});
	 	
	 }
	 function getGradeList(callback){
	 	var param = {category:'2'};
	 	mainDataService.getDirEstGradeList(param)
	 	.success(function(data){
	 		$scope.gradeList =data.rows;
	 		if(typeof callback=='function'){
		 		callback(data);
	 		}	 		
	 	});	 
	 }
	 function setGrid() {
	        return pagerJsonGrid({
	            grid_id : 'list',
	            pager_id : 'listPager',
	            url : '/est/getList.json',
	            condition : {
	                page : 1,
	                rows : GridConfig.sizeL,
	                category : '2' 
	            },
	            rowNum : 20,
	            colNames : [ '순번','EST_SID','평가명','평가시작일','평가종료일','작성자','작성일'],
	            colModel : [
	            {
	                name : 'RNUM',
	                width : "40px",
	                resizable: false
	            }, {
	                name : 'EST_SID',
	                width : 0,
	                hidden: true,
	                resizable: false
	            }, {
	                name : 'EST_NM',
	                width : 150,
	                resizable: false
	            }, {
	                name : 'EST_START_DT',
	                width : 80,
	                resizable: false
	            }, {
	                name : 'EST_END_DT',
	                width : 0,
	                hidden : true,
	                resizable: false
	            }, {
	                name : 'INSERT_ID',
	                width : 80,
	                resizable: false
	            }, {
	                name : 'INSERT_DT',
	                width : 80,
	                resizable: false
	            }],

	            onSelectRow : function(id) {
	            	
	            	$scope.EstimationInfo = $('#list').jqGrid('getRowData', id);
	            	$scope.$apply('EstimationInfo');
	            	$timeout(function(){
	    			$("#list2").setGridParam({
	        			datatype : 'json',
	        			page : 1,
	        			postData : {
	    					category : '2',
	    	                EST_SID : $scope.EstimationInfo.EST_SID, 
	    	                CLASS3_CD: $scope.CLASS3_CD,
	    	                CLASS4_CD: $scope.CLASS4_CD
	        			}
	        		}).trigger('reloadGrid', {
	        			current : true
	        		});
	        		},500);
	            	
	            	var param1 = {category : '2', EST_SID : $scope.EstimationInfo.EST_SID , tabId : 1};
	            	var param2 = {category : '2', EST_SID : $scope.EstimationInfo.EST_SID , tabId : 2};
	            	
	            	
	            	
	            	
	            	mainDataService.getEstAssetGroup(param1)
	            	.success(function(data){
	            		$scope.Tab1Tot = {G1:0,G2:0,G3:0,G4:0,G5:0};
						$scope.EstGroupList1 = data;
						$.each(data,function(idx,Item){
							Item.tot = Item.G1 + Item.G2 + Item.G3+ Item.G4+ Item.G5;
							$scope.Tab1Tot.G1 += Item.G1;
							$scope.Tab1Tot.G2 += Item.G2;
							$scope.Tab1Tot.G3 += Item.G3;
							$scope.Tab1Tot.G4 += Item.G4;
							$scope.Tab1Tot.G5 += Item.G5;
						});						
	            	});
	            	
	            	mainDataService.getEstAssetGroup(param2)
	            	.success(function(data){
	            		$scope.Tab2Tot = {G1:0,G2:0,G3:0,G4:0,G5:0};
						$scope.EstGroupList2 = data;
						$.each(data,function(idx,Item){
							var bcode = Item.GROUP_CD.split("_");
							Item.GROUP_NM1 = $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:bcode[0]})[0].C_NAME + '(' + bcode[0] + ')';
							Item.GROUP_NM2 = $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:bcode[1]})[0].C_NAME + '(' + bcode[1] + ')';
							
							Item.tot = Item.G1 + Item.G2 + Item.G3 + Item.G4 + Item.G5;
							$scope.Tab2Tot.G1 += Item.G1;
							$scope.Tab2Tot.G2 += Item.G2;
							$scope.Tab2Tot.G3 += Item.G3;
							$scope.Tab2Tot.G4 += Item.G4;
							$scope.Tab2Tot.G5 += Item.G5;
						});												
	            	});
	            }
            ,
            gridComplete : function() {
				var ids = $(this).jqGrid('getDataIDs');
				
				//if ($.isEmptyObject($scope.PathInfo)) {
					$(this).setSelection(ids[0]);
				//}
				//alert($('#list').getGridParam('page'));
				$scope.currentPageNo=$('#list').getGridParam('page');
				$scope.count = $('#list').getGridParam('records');
				$scope.$apply();
			}
	        });
	    }
	 
	 function setGrid2() {
	        return pagerGrid({
	            grid_id : 'list2',
	            pager_id : 'listPager2',
	            url : '/est/assetPage.json',
	            condition : {
	                page : 1,
	                rows : 10,
	                category : '2',
	                EST_SID : $scope.EstimationInfo.EST_SID
	            },
	            rowNum : 10,
	            colNames : [ '순번','자산코드','자산명','대불록','중불록','소불록','관로계통','관종','평가등급','평가결과'],
	            colModel : [
	            {
	                name : 'RNUM',
	                width : 53,
	                resizable:false,
	            }, {
	                name : 'ASSET_CD',
	                width : 120,
	                resizable:false,
	            }, {
	                name : 'ASSET_NM',
	                width : 100,
	                resizable:false,
	            }, {
	                name : 'DAE',
	                width : 50,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
	                	try{
	                		return $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:cellvalue})[0].C_NAME;
                		}catch(ex){
                			return "";
                		}
	                }	                
	            }, {
	                name : 'JUNG',
	                width : 50,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
	                	try{
	                		return $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:cellvalue})[0].C_NAME;
                		}catch(ex){
                			return "";
                		}	                		
	                }	                
	            }, {
	                name : 'SO',
	                width : 50,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
	                	try{
	                		return $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:cellvalue})[0].C_NAME;
                		}catch(ex){
                			return "";
                		}	                		
	                }		                
	            }, {
	                name : 'SAA',
	                width : 50,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0]);
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
	                	try{
	                		return $filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:cellvalue})[0].C_NAME;
                		}catch(ex){
                			return "";
                		}	                		
	                }	                
	            }, {
	                name : 'MOP',
	                width : 80,
	                resizable:false,
	                formatter:function(cellvalue, options, rowdata, action){
	                	//console.log($filter("filter")($scope.ComCodeList['MOP'],{C_SCODE:cellvalue})[0])
	                	if(typeof cellvalue=='undefined' || cellvalue==null) return "";
	                	try{
	                		return $filter("filter")($scope.ComCodeList['MOP'],{C_SCODE:cellvalue})[0].C_NAME;
                		}catch(ex){
                			return "";
                		}
	                }	                
	            }, {
	                name : 'GRADE',
	                width : 50,
	                resizable:false,
	            }, {
	                name : '평가결과',
	                width : 50,
	                resizable:false,
	                formatter : function(cellvalue, options, rowdata, action){
	                	var rowid= rowdata['ASSET_SID'];
	                	var est_sid= rowdata['EST_SID'];
	                	var asset_sid= rowdata['ASSET_SID'];
	                	if(typeof asset_sid=='undefined' || asset_sid==null || asset_sid=="" ) return ""; 
             		return "<div id='button"+rowid+"' ><button onclick=\"showDirEstResultPopup("+est_sid+","+asset_sid+")\">상세</button></div>";                		
	                }
	            }],

	            onSelectRow : function(id) {
	            	
	                $scope.AssetItemInfo = $('#list2').jqGrid('getRowData', id);
	                console.log($scope.AssetItemInfo);
	            }
	        });
	    }
	 
	 function setGrid2_Popup() {
	        return pagerGrid({
	            grid_id : 'list2popup',
	            pager_id : 'list2popupPager',
	            url : '/est/assetPage.json',
	            condition : {
	                page : 1,
	                rows : 10,
	                category : '2',
	                EST_SID : $scope.EditInfo.EST_SID
	            },
	            rowNum : 15,
	            multiselect : true,
	            colNames : [ '순번','EST_SID','자산번호','자산코드','자산명','대불록','중불록','소불록','관로계통','관종','평가등급','평가결과'],
	            colModel : [
	            {
	                name : 'RNUM',
	                width : "40px"
	            }, {
	                name : 'EST_SID',
	                width : 0,
	                hidden:true,
	            }, {
	                name : 'ASSET_SID',
	                width : 0,
	                hidden:true,
	            }, {	            	
	                name : 'ASSET_CD',
	                width : "180px",
	            }, {
	                name : 'ASSET_NM',
	                width : 100,
	            }, {
	                name : 'DAE',
	                width : 50,
	                hidden: true,
	            }, {
	                name : 'JUNG',
	                width : 50,
	            }, {
	                name : 'SO',
	                width : 50,
	            }, {
	                name : 'SAA',
	                width : 50,
	            }, {
	                name : 'MOP',
	                width : 50,
	            }, {
	                name : 'GRADE',
	                width : 50,
	                hidden: true,
	            }, {
	                name : '평가결과',
	                width : 50,
	                hidden: false,
	                formatter : function(cellvalue, options, rowdata, action){
	                	var rowid= rowdata['ASSET_SID'];
	                	var est_sid= rowdata['EST_SID'];
	                	var asset_sid= rowdata['ASSET_SID'];	                	
          		return "<div id='button"+rowid+"' ><button onclick=\"showDirEstResultPopup("+est_sid+","+asset_sid+")\">상세</button></div>";
	                }
	            }],

	            onSelectRow : function(id) {
	            	
	                //$scope.AssetItemInfo = $('#list2').jqGrid('getRowData', id);
	                //console.log($scope.AssetItemInfo);
	            }
	        });
	    }	 
	 
	 $scope.onKeyPress = function(event){
	        if (event.key === 'Enter') {
	        	$scope.SearchList();
	        }
	    }
	 
	 $scope.SearchList = function(){
		 
			if($scope.searchOption==1){
	    		if($scope.p1SearchYear==""){
	    			alertify.alert('오류','기간을 입력하 세요');
	    			$("#selectYear").focus();
	    			return;    			
	    		}
	    		if($scope.selectYear==""){
		        	$scope.sOptionStartDt = ""; 
		           	$scope.sOptionEndDt =  "";
	    		}else{
		        	$scope.sOptionStartDt = $scope.selectYear + "0101"; 
		           	$scope.sOptionEndDt =  $scope.selectYear + "1231";
	           	}  		
	    	}else{
	    		if($("#start_dt").val()==""){
	    			alertify.alert('오류','기간을 입력하 세요');
	    			$("#start_dt").focus();
	    			return;
	    		}
	    		if($("#end_dt").val()==""){
	    			alertify.alert('오류','기간을 입력하 세요');
	    			$("#end_dt").focus();
	    			return;
	    		}
	    		if($("#end_dt").val() < $("#start_dt").val()){
	    			alertify.alert('오류','기간범위를  확인하세요.시작날짜가 종료날짜 보다 클수 없습니다.');
	    			$("#start_dt").focus();
	    			return;
	    		}	    		
	        	$scope.sOptionStartDt = $("#start_dt").val().replace(/-/gi,''); 
	           	$scope.sOptionEndDt =  $("#end_dt").val().replace(/-/gi,'');    		
	    	}
	    		
	    		 
				$("#list").setGridParam({
	    			datatype : 'json',
	    			page : $scope.currentPageNo,
	    			rows : GridConfig.sizeS,
	    			postData : {
	    				searchText : $scope.searchText,
						category : '2',
						START_DT : $scope.sOptionStartDt,
						END_DT : $scope.sOptionEndDt					
	    			}
	    		}).trigger('reloadGrid', {
	    			current : true
	    		});	 
		 }
	 
	 function renderGridButton(){
     	var sHTML = "<input type=button value=\"상세결과\" ng-click=\"showDirEstResultPopup()\" >";
    	var template = angular.element(sHTML);
		var linkFunction = $compile(template);
		linkFunction($scope);
		return template;		 
	 }
	 $scope.UpLoadExcel = function(){
		 $("#input_excel_file").trigger('click');
	 }
	 $scope.DownLoadExcel=function(Type){
	    	const jobType = $state.current.name + "_" + Type;
	        //const levelNm = $scope.levelNm;

	        $('#exportForm').find('[name=jobType]').val(jobType);
	        switch(Type){
	        case 0 : //직접평가 목록
	        	$('#exportForm').find('input[name=start_dt]').val($scope.sOptionStartDt);
	        	$('#exportForm').find('input[name=end_dt]').val($scope.sOptionEndDt);
	        	$('#exportForm').find('input[name=searchText]').val($scope.searchText);        	
	        	break;
	        case 1 : //직접평가 항목
	        	$('#exportForm').find('[name=jnum]').val( $scope.factorDetail[0].EST_SPEC_SID );
	        	break;
	        case 2 : // 개량방안
	        	$('#exportForm').find('[name=jnum]').val($scope.gradeDetail[0].EST_IMP_SID);
	        	break;
	        case 3 : // 양식출력
	        	$('#exportForm').find('[name=jnum]').val($scope.EditInfo.EST_SID);
	        	//$('#exportForm').find('#PARAM').val(JSON.stringify());
	        	break;		        	
	        }
	        
	        $('#exportForm').submit();   
	 }
	 
	 $scope.DownloadReport = function(){
	    	const jobType = $state.current.name ;
	        $('#exportForm').find('[name=jobType]').val(jobType);
     	$('#exportForm').find('[name=jnum]').val( $scope.EstimationInfo.EST_SID );
     	$('#exportForm').submit();
	 }
	 
	 $scope.gradeSelect = function(grade){
	    	console.log(grade);
	    	$scope.SelectedGradeInfo = grade;
	    	$scope.EditGradeInfo = angular.copy(grade);
	    	var param = {EST_IMP_SID:grade.EST_IMP_SID ,category : 2 };
	    	mainDataService.getIndirEstGradeDetail(param)
	    	.success(function(data){
	    		console.log(data);
	    		$scope.gradeDetail = data;
	    		$scope.dataGradeList = data;
	    	});
	 }
	 
	 /*$scope.DeleteItem = function(){
		 	if($scope.EstimationInfo.EST_SID > 0){
		 	if(confirm('삭제하시겠습니까?')){
		 	var param = {category: '2' ,del_yn: 'Y' , est_sid : $scope.EstimationInfo.EST_SID};
			 	mainDataService.updateDirEst(param)
			 	.success(function(data){
					 alert('삭제되었습니다.');	
					 $scope.SearchList();
			 	});
		 	}
		 	}else{
		 		alert('삭제할 간접 평가가 없습니다.');
		 	}	 
		 }
	 */
	 $scope.DeleteItem = function(){
		 if (!($scope.EstimationInfo.EST_SID > 0)) {
			 alertify.alert('항목을 선택해주세요').set('basic', true);
			 return;
		 }

		 // 삭제 권한 체크
		 if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
			 alertify.alert('권한이 없습니다.').set('basic', true);
			 return;
		 }

		 alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?',
				 function () {
			 mainDataService.updateIndirEst({
				 category: '2' ,
				 del_yn: 'Y' , 
				 est_sid : $scope.EstimationInfo.EST_SID
				 // del_yn: 'Y',
			 }).success(function (obj) {
				 if (!common.isEmpty(obj.errMessage)) {
					 alertify.error('errMessage : ' + obj.errMessage);
				 } else {
					 alertify.success('삭제 되었습니다.');
					 $scope.SearchList();
				 }
			 });
		 },
		 function () {
		 }
		 ).set('basic', false);
	 };
	 $scope.NewItem = function(){
		 	var param = {category: '2' ,temp_yn: 'Y' };
		 	
		 	mainDataService.insertDirEst(param)
		 	.success(function(est_sid){
		 		console.log(est_sid);
		 		$scope.EditInfo = {
		 			Mode : 'new',
		 			EST_SID : est_sid,
		 			INSERT_ID: sessionStorage.getItem("loginId"),
		 			INSERT_DT: Common_Time.toDay().START_DT
		 		}
			 	$("#newDirEstimationPopup").show();
				$("#list2popup").setGridParam({
	    			datatype : 'json',
	    			postData : {
						category : '2',
		                EST_SID : $scope.EditInfo.EST_SID,
    	                CLASS3_CD: $scope.CLASS3_CD,
    	                CLASS4_CD: $scope.CLASS4_CD		                
	    			}
	    		}).trigger('reloadGrid', {
	    			current : true
	    		});	 
				 //alert('신규평가');	
		 	});
	 }

	//결과보기
	$scope.ShowResult = function() {
		var param = { 'category': '2', 'est_sid': $scope.EstimationInfo.EST_SID };

		mainDataService.getGISInformationAndConditionAssessmentResults(param).success(function(data) {
			// console.log(data);
			var ftrIdnStringWithComma = data.map(obj => obj.FTR_IDN).join();
			var layerNameStringWithComma = [...new Set(data.map(obj => obj.LAYER_NM))].join();
			// console.log(ftrIdnStringWithComma);
			// console.log(layerNameStringWithComma);
			$rootScope.$broadcast('showGisPopup', {
				init: function() {
					var colorMap = [
			              'rgb(  0,   0, 255)', // default
			              'rgb(  0,   0, 255)', // blue  1
			              'rgb(  0, 255, 255)', // skey blue  2
			              'rgb(  0, 255,   0)', // green  3
			              'rgb(255, 128,   0)', // orange
			              'rgb(255,   0,   0)', //red
			              'rgb(  0,   0, 255)', // not defined
			              'rgb(  0,   0, 255)' // not defined
			            ];   

					var getMatchingColorForTheGrade = function(feature, data) {
						for (var i = 0; i < data.length; i++) {
							if (feature.get('ftr_idn') === data[i].FTR_IDN.toString()) {
								return commonUtil.isNotEmpty(data[i].STATE_GRADE) ? colorMap[Number(data[i].STATE_GRADE)] : colorMap[0]; // matched
							}
						}

						return colorMap[0]; // not matched
					};

					var style = function(color) {
						var fill = new ol.style.Fill({ 'color': color });
						var stroke = new ol.style.Stroke({
							color: color,
							width: 1.25
						});

						return new ol.style.Style({
							image: new ol.style.Circle({
								fill: fill,
								stroke: stroke,
								radius: 5
							}),
							fill: fill,
							stroke: stroke
						});
					};

					var vectorSource = new ol.source.Vector({
						format: new ol.format.WFS(),
						loader: function(extent) {
							$.ajax(WFS_URL, {
								type: 'POST',
								data: {
									service: 'WFS',
									version: '1.1.0',
									request: 'GetFeature',
									typeName: layerNameStringWithComma,
									srsname: 'EPSG:3857',
									cql_filter: 'ftr_idn in (' + ftrIdnStringWithComma + ')'
								}
							}).done(function(response) {
								console.log('response', response);

								var features = vectorSource.getFormat().readFeatures(response);
								vectorSource.addFeatures(features);
								vectorSource.forEachFeature(function(feature) {
									feature.setStyle(style(getMatchingColorForTheGrade(feature, data)));
								});
							});
						},
						projection: 'EPSG:3857',
						crossOrigin: 'Anonymous'
					});

					var vectorLayer = new ol.layer.Vector({
						source: vectorSource,
						style: ol.style.Style.defaultFunction
					});

					map.addLayer(vectorLayer);

					var select = new ol.interaction.Select({
						layers: [vectorLayer]
					});
					map.addInteraction(select);

					select.on('select', function(event) {
						var features = event.selected;

						if (features === undefined || features.length === 0) {
							return;
						}

						var feature = features[0];
						var properties = feature.getProperties();
						var param = {
							'ftr_idn': properties.ftr_idn, 'ftr_cde': properties.ftr_cde
						};

						getAssetInfoWithGisInfo(mainDataService, param).success(function(data) {
							parent.showAssetItemPP(data);
						});
					});
				}
				// callbackId: '',
			});
		});

		mainDataService.getAssetNonePage(param).success(function(data) {
			var interval = $interval(function() {
				var scope = angular.element($('#gisPopup')).scope();

				if (scope) {
					$.each(data,function(idx,Item){
						try{
							Item.DAE = $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:Item.DAE})[0].C_NAME;
						}catch(e){}
						try{
							Item.JUNG = $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:Item.JUNG})[0].C_NAME;
						}catch(e){}
						try{						
							Item.SO =  $filter("filter")($scope.ComCodeList['BLOC'],{C_SCODE:Item.SO})[0].C_NAME;
						}catch(e){}
						try{						
							Item.SAA =  $filter("filter")($scope.ComCodeList['SAA'],{C_SCODE:Item.SAA})[0].C_NAME;
						}catch(e){}
						try{						
							Item.MOP =  $filter("filter")($scope.ComCodeList['MOP'],{C_SCODE:Item.MOP})[0].C_NAME;
						}catch(e){}						
					});					
					scope.evaluationResultsList = data;
					clearInterval();
				}
			}, 1000, 10);

			var clearInterval = function() {
				if (angular.isDefined(interval)) {
					$interval.cancel(interval);
					interval = undefined;
				}
			};
		});
	};
	 
	 
	 $scope.closeNewItemPopup = function(){
		 $("#newDirEstimationPopup").hide();
	 }
	 
	 //관로선택
	$scope.showGisPopup = function() {
		$rootScope.$broadcast('showGisPopup', {
			init: function() {
				var gisPopup = document.getElementById('gisPopup');
				var event = new Event('change');
				var selectTarget;
				var drawSelector;

				gisPopup.querySelector('#start_select_mode').click();

				selectTarget = gisPopup.querySelector('#selectTarget');
				selectTarget.value = 'ams:wtl_pipe_lm:use_yn=true AND saa_cde=\'SAA003\'';
				selectTarget.dispatchEvent(event);

				drawSelector = gisPopup.querySelector('#drawSelector');
				drawSelector.value = 'Box';
				drawSelector.dispatchEvent(event);
			},
			callbackId: 'DirEstAsset_selectGisInfo'
		});
		$('newDirEstimationPopup').hide();
	};
	 
	//선택관로 삭제
	 $scope.deleteSelectedAsset = function(){
		 //alert('선택 관로삭제');
		 //debugger;
		 var list = [];
		 var selrow = "";
		 var s = jQuery("#list2popup").jqGrid('getGridParam','selarrrow');
		  if(s.length<=0) {
		   alert("항목을 선택해 주세요.");
		   return;
		  }else{
		   s = ""+s;
		   sArr = s.split(",");
		    for(var i=0; i<sArr.length; i++){
		    var ret = jQuery("#list2popup").getRowData(sArr[i]);
		    list.push(parseInt(ret.ASSET_SID));
		    //if(i == 0) selrow = "'"+ret.ASSET_SID+"'";
		    //else selrow += ",'"+ret.ASSET_SID+"'";
		   }
		  }
		  //alert(selrow);
		  var param = {category:'2',EST_SID:$scope.EditInfo.EST_SID,ASSET_LIST:list};
		  mainDataService.deleteEstAsset(param)
		  .success(function(data){
			  reloadPopupAssetList();
		  });
	 }	 
	 
	 
	//평가시작
	 $scope.SaveAssetResult = function(){
    	if(1){
    		if($scope.EditInfo.EST_NM == null || $scope.EditInfo.EST_NM == ""){
    			alertify.alert('오류','평가명을 입력하세요.',function(){
    				$timeout(function(){$("#EST_NM").focus();},500);
    			});
    		return false;
    		}
    		if($scope.EditInfo.EST_START_DT == null || $scope.EditInfo.EST_START_DT == ""){
    			alertify.alert('오류','평가 시작일을 입력하세요.',function(){
    				$("#EST_START_DT").focus();
    			});
    		return false;
    		}
    		if($scope.EditInfo.EST_END_DT == null || $scope.EditInfo.EST_END_DT == ""){
    			alertify.alert('오류','평가 종료일을 입력하세요.',function(){
    				$("#EST_END_DT").focus();
    			});
        		return false;
       		}
    		if($scope.EditInfo.EST_END_DT < $scope.EditInfo.EST_START_DT){
    			alertify.alert('오류','평가 기간을 확인하세요. 시작일이 종료일보다 클수 없습니다.',function(){
    				$("#EST_START_DT").focus();
    			});
        		return false;
       		}    		
    	}
		 //alert('평가시작');
	 	var param = angular.copy($scope.EditInfo);
	 	//debugger;
	 	console.log($scope.EditInfo);
	 	param.est_sid = $scope.EditInfo.EST_SID;
	 	param.est_nm = xssFilter($scope.EditInfo.EST_NM);
	 	param.temp_yn='N';
	 	param.category="2";
	 	mainDataService.updateDirEst(param)
	 	.success(function(data){
		//저장 성공
	 		/*
			mainDataService.saveDirEstAssetResult(param)
			.success(function(data){
			 	console.log(data);

			});
			*/
		 	$("#newDirEstimationPopup").hide();
		 	$("#list").setGridParam({
    			datatype : 'json',
    			page : $scope.currentPageNo,
    			rows : GridConfig.sizeS,
    			postData : {

    			}
    		}).trigger('reloadGrid', {
    			current : true
    		});
		 	alertify.success("저장되었습니다.");
	 	});
		 
	 }
	 
	 $scope.AssetList = [{},{},{},{}];

    $scope.$on('DirEstAsset_selectGisInfo',function(event,data){
    	//debugger;
    	console.log(data);

    	var obj = angular.copy(getSelectGisObj());
    	console.log(obj);
    	
    	var list = [];
		$.each(obj,function(idx, Item) {
			list.push(Item.ftr_idn);	
		});
		
		var param = {FTR_IDN : list.toString(), LAYER_CD: obj[0].fid};
		mainDataService.getAssetFromGisInfo(param).success(function(data) {
			var list2= [];
			$.each(data,function(idx,Item){
				if(Item.LAYER_CD=='P4' || Item.LAYER_CD=='P3' || Item.LAYER_CD=='P5' || Item.LAYER_CD=='P1')
				list2.push(Item.ASSET_SID);
			});
	    	var param2 = {category:'2',EST_SID:$scope.EditInfo.EST_SID,ASSET_LIST : list2  }; 
	    	mainDataService.saveDirEstAsset(param2)
	    	.success(function(data){
	    		//console.log(data);
	    		
	    		
    			reloadPopupAssetList();
	    		
	    	});			
			//--------------------------------
	    	/*
			let jqGridList = getListAll(Model);
			let mASSET_SID = {}
			for(let i = 0 ; i < jqGridList.length; i++) {
				mASSET_SID[jqGridList[i].ASSET_SID] = true;
			}				
		
			let info = {}
			for(let i=0; i < data.length; i++) {
				if(mASSET_SID[data[i].ASSET_SID])continue;
				
				mASSET_SID[data[i].ASSET_SID] = true;
				let list = data[i];
				info = {}
				info.ASSET_CD = list.ASSET_CD;
				info.ASSET_SID = list.ASSET_SID;
				info.ASSET_NM = list.ASSET_NM;
				info.ASSET_PATH_CD = list.ASSET_PATH_CD;
				info.FIRST_PRICE = list.FIRST_PRICE;
				Model.TOTAL_PRICE += stringToIntiger(list.FIRST_PRICE);
				
				addData(Model, info);
			    jQuery("#"+Model.grid_id).trigger('reloadGrid');
			}
			*/
		});
		
    	
    	
    	/*var param = {EST_SID:$scope.EditInfo.EST_SID, FTR_IDN:obj[0].ftr_idn,LAYER_CD : obj[0].fid}; 
    	mainDataService.saveIndirEstAsset(param)
    	.success(function(data){
    		console.log(data);
    	});
    	$("newDirEstimationPopup").show();*/
    });
    
   /* $scope.closeDetailResultIndirEstimation = function (){
    	$("#DetailResultIndirEstimationPopup").hide();
    }*/
    
    function reloadPopupAssetList(){
		$("#list2popup").setGridParam({
			datatype : 'json',
			postData : {
				category:'2',
                EST_SID : $scope.EditInfo.EST_SID,
                CLASS3_CD: $scope.CLASS3_CD,
                CLASS4_CD: $scope.CLASS4_CD                
			}
		}).trigger('reloadGrid', {
			current : true
		});    	
    }
    
    $scope.clickTable = function($event){
    	$($event.currentTarget).addClass('active').siblings().removeClass('active');
    };
    
    $scope.factorSelect = function (factor){
    	console.log(factor);
    	$scope.SelectedFactorInfo = factor;
    	$scope.selectedFactor = factor;
    	//$scope.tab2Click('0');
    	
	 	var param = {EST_SPEC_SID: $scope.selectedFactor.EST_SPEC_SID ,category:'2'};
		mainDataService.getDirEstFactorDetail(param)
	 	.success(function(data){
	 		
	 		var ITEM_GB = data[0].ITEM_GB;
	 		$scope.GB_ARRAY = [];
	 		$scope.GB_ARRAY.push({name:data[0].ITEM_GB,cnt:1,idx:0});
	 		$.each(data,function(idx,item){
	 			if(data[idx].ITEM_GB == ITEM_GB){
	 				if(idx>0)
 					$scope.GB_ARRAY[$scope.GB_ARRAY.length-1].cnt +=1;
	 			}
	 			else{
	 				$scope.GB_ARRAY.push({name:data[idx].ITEM_GB,cnt:1,idx:idx});	 				
	 			}
	 			ITEM_GB = data[idx].ITEM_GB;
	 			item.GB_Idx = $scope.GB_ARRAY.length-1;
	 			item.row_idx = $scope.GB_ARRAY[$scope.GB_ARRAY.length-1].idx;
	 			item.idx = idx;
	 		});
	 		$.each(data,function(idx,item){
	 			item.GB_cnt = $scope.GB_ARRAY[item.GB_Idx].cnt; 
	 		});	
	 		
	 		console.log(data);
	 		
	 		$scope.popupFactorDetail = angular.copy(data);
	 		$scope.factorDetail = angular.copy(data);
	 		$.each($scope.popupFactorDetail,function(idx,Item){
	 			//Item.IsActive = (Item.USE_YN=='Y')?true:false;
	 		});
	 	});
 	    	
    }
    $scope.EditFactorInfo ={};
    //신규 평가항목 팝업
    $scope.newFactorItem = function(){
    	$("#newDirEstFactorPopup").show();
    	$scope.popupfactorDetail = angular.copy($scope.factorDetail);
    	$scope.EditFactorInfo = angular.copy($scope.SelectedFactorInfo);
    	$scope.EditFactorInfo.EST_SPEC_SID=0;
    	$scope.EditFactorInfo.Mode='new';
    	$scope.saveType = 'new';
    	$scope.EditFactorInfo.EST_SPEC_NM='';
    	$scope.EditFactorInfo.APPLY_DT='';    	
    }   
    
    //수정 평가항목 팝업 
    $scope.editFactorItem = function(){
    	$("#newDirEstFactorPopup").show();
    	$scope.popupfactorDetail = angular.copy($scope.factorDetail);
    	$scope.EditFactorInfo = angular.copy($scope.SelectedFactorInfo);
    	$scope.EditFactorInfo.Mode=''; 
    	$scope.saveType = '';
    } 
    $scope.closeDirEstFactor = function(){
		$("#newDirEstFactorPopup").hide();    
    }    

    //신규 개량방안 팝업
    $scope.newGradeItem=function(){
    	$("#newDirEstGradePopup").show();
    	$scope.popupGradeDetail = angular.copy($scope.gradeDetail);
    	$scope.EditGradeInfo = angular.copy($scope.SelectedGradeInfo);
    	$scope.EditGradeInfo.EST_IMP_SID=0;
    	$scope.EditGradeInfo.Mode='new';
    	$scope.saveType = 'new';
    	$scope.EditGradeInfo.EST_IMP_NM='';
    	$scope.EditGradeInfo.APPLY_DT='';
    }

    //수정 개량방안 팝업    
    $scope.editGradeItem=function(){
    	$("#newDirEstGradePopup").show();
    	$scope.popupGradeDetail = angular.copy($scope.gradeDetail);
    	$scope.EditGradeInfo = angular.copy($scope.SelectedGradeInfo);
    	$scope.EditGradeInfo.Mode='';
    	$scope.saveType = '';
    }
    
    $scope.closeDirEstGrade = function(){
		$("#newDirEstGradePopup").hide();    
    }
    
    $scope.saveDirEstGrade = function(){
    	if(1){
    		if($scope.EditGradeInfo.EST_IMP_NM == null || $scope.EditGradeInfo.EST_IMP_NM == ""){
    		//alert('개량방안명 필수 항목');
    		alertify.alert('오류','개량방안명을 입력하세요');
    		return false;
    		}
    		if($scope.EditGradeInfo.APPLY_DT == null || $scope.EditGradeInfo.APPLY_DT == ""){
    		//alert('적용일자 필수 항목');
    		alertify.alert('오류','적용일자를 입력하세요');
    		return false;
    		}
    	}
    
    	console.log($scope.popupGradeDetail);
    	var list = [];
    	$.each($scope.popupGradeDetail,function(idx,Item){
    		for(key in Item){
    			if(Item[key]==null) Item[key]="";
    		}
    		list.push(Item);
    	});
    	console.log(list);
    	$("#newDirEstGradePopup").hide();
    	var param = {category : '2'
    			,EST_IMP_SID: $scope.EditGradeInfo.EST_IMP_SID
    			,EST_IMP_NM : $scope.EditGradeInfo.EST_IMP_NM
    			,APPLY_DT : $scope.EditGradeInfo.APPLY_DT.replace(/-/gi,'')
    			,ITEM_LIST : list
    			};
    			 
    	if($scope.EditGradeInfo.Mode=='new'){
	    	mainDataService.insertDirEstGrade(param)
	    	.success(function(data){
	    		console.log(data);
	    		getGradeList();
	    		alertify.success("저장되었습니다.");
	    	});    	
    	}else{
    		$scope.SelectedGradeInfo = angular.copy($scope.EditGradeInfo);
    		$scope.gradeDetail = angular.copy($scope.popupGradeDetail);
	    	mainDataService.updateDirEstGrade(param)
	    	.success(function(data){
	    		console.log(data);
	    		getGradeList();
	    		alertify.success("저장되었습니다.");
	    	});
    	} 
    }
    
    $scope.saveDirEstFactor = function(){
    	
    	if(1){
    		if($scope.EditFactorInfo.EST_SPEC_NM == null || $scope.EditFactorInfo.EST_SPEC_NM == ""){
    		//alert('평가항목 필수');
    		alertify.alert('오류','평가항목을 입력하세요');
    		return false;
    		}
    		if($scope.EditFactorInfo.APPLY_DT == null || $scope.EditFactorInfo.APPLY_DT == ""){
    		//alert('적용일자 필수');
    		alertify.alert('오류','적용일자를 입력하세요');
    		return false;
    		}
    	}
    	
    	if($scope.EditFactorInfo.Mode=='new'){
    		var list = [];
    		var param ={category:"2",EST_SPEC_SID: 0,EST_SPEC_NM:$scope.EditFactorInfo.EST_SPEC_NM,APPLY_DT:$scope.EditFactorInfo.APPLY_DT.replace(/-/gi,''),ITEM_LIST : $scope.popupFactorDetail };
        	mainDataService.insertDirEstFactorList(param)
        	.success(function(data){
        		getFactorList(function(data){
        			$.each(data.rows,function(idx,Item){
        				if(Item.EST_SPEC_SID == $scope.EditFactorInfo.EST_SPEC_SID ){
        					$scope.factorSelect(Item);
        					return false;
        				}
        			});
        		});
        		$scope.closeDirEstFactor();
        		alertify.success("저장되었습니다.");
        	});

    	}else{
    		var list = [];
    		$.each($scope.popupFactorDetail,function(idx,Item){
    			Item.INSERT_ID='';
    		});
        	var param ={category:"2",EST_SPEC_SID: $scope.EditFactorInfo.EST_SPEC_SID,EST_SPEC_NM:$scope.EditFactorInfo.EST_SPEC_NM,APPLY_DT:$scope.EditFactorInfo.APPLY_DT.replace(/-/gi,''),ITEM_LIST : $scope.popupFactorDetail };
        	mainDataService.updateDirEstFactorList(param)
        	.success(function(data){
        		getFactorList(function(data){
        			$.each(data.rows,function(idx,Item){
        				if(Item.EST_SPEC_SID == $scope.EditFactorInfo.EST_SPEC_SID ){
        					$scope.factorSelect(Item);
        					return false;
        				}
        			});
        		});
        		$scope.closeDirEstFactor();
        		$scope.factorSelect($scope.selectedFactor);
        		alertify.success("저장되었습니다.");
        	});    		
    	}
    	
    }
    $scope.showResultPopup = function(){
    	$("#resultDirEstInfoPopup").show();
    }
    $scope.closeResultPopup = function(){
    	$("#resultDirEstInfoPopup").hide();
    } 
    $scope.saveResultPopup = function(){
    	$.each($scope.EstDirAssetInfo,function(idx,Item){
    		if(Item.INPUT_VAL==null) Item.INPUT_VAL='';  
    	});  	
    	var param = {EST_SID:$scope.EstDirAssetInfo[0].EST_SID,ASSET_SID:$scope.EstDirAssetInfo[0].ASSET_SID, ITEM_LIST: $scope.EstDirAssetInfo};
    	mainDataService.saveDirEstAssetResult(param)
    	.success(function(data){
    		console.log(data);
    		alertify.success("저장되었습니다.");
    	});
    	$scope.closeResultPopup();
    }
	 
    $scope.showDetailResultPopup = function(est_sid,asset_sid){
    	$("#resultDirEstInfoPopup").show();	 
		//alert('test');
		 var param = {EST_SID:est_sid,ASSET_SID:asset_sid,category:'2'};
		 mainDataService.getDirEstAssetResult(param)
		 .success(function(data){
		 	console.log(data);
		 	//debugger;
		 	var ITEM_GB = data[0].ITEM_GB;
	 		$scope.GB_ARRAY = [];
	 		$scope.GB_ARRAY.push({name:data[0].ITEM_GB,cnt:1,idx:0});
	 		$.each(data,function(idx,item){
	 			if(data[idx].ITEM_GB == ITEM_GB){
	 				if(idx>0)
 					$scope.GB_ARRAY[$scope.GB_ARRAY.length-1].cnt +=1;
	 			}
	 			else{
	 				$scope.GB_ARRAY.push({name:data[idx].ITEM_GB,cnt:1,idx:idx});	 				
	 			}
	 			item.GB_Idx = $scope.GB_ARRAY.length-1;
	 			item.row_idx = $scope.GB_ARRAY[$scope.GB_ARRAY.length-1].idx;
	 			item.idx = idx;
	 			ITEM_GB = item.ITEM_GB;
	 		});
	 		$.each(data,function(idx,item){
	 			item.GB_cnt = $scope.GB_ARRAY[item.GB_Idx].cnt; 
	 		});	
	 		$scope.EstDirAssetInfo = data;
		 	
		 });
		 
	 }
    
    $scope.fileSelect = function($files, cmd, index){ 			
       	var reader = new FileReader();
    		reader.onload = function(e) {
    			const workBook =XLSX.read(reader.result, {type :'binary'});
    			workBook.SheetNames.forEach(sheetName => {
    				const rows =XLSX.utils.sheet_to_json(workBook.Sheets[sheetName]);
    				//rows.splice(0,1);
    				//console.log(rows);
    				console.log(rows);
    				var list = [];
    				if(sheetName=='Sheet1'){
    					//$scope.CostList = [];
    					$.each(rows,function(idx,Item){
    						if(idx>=4){
    							list.push(angular.copy(Item));
    						}
    					});
    					var param2 = {category:'2',EST_SID:$scope.EditInfo.EST_SID,ASSET_LIST : list  };
    					//debugger;
    			    	mainDataService.saveDirEstAssetResult2(param2)
    			    	.success(function(data){
    		    			reloadPopupAssetList();
    			    	});    					
					
    				}
    				//mainDataService.CommunicationRelay(PopupView.URLS.InsertAsset, {List : rows, SID : PopupView.Info.Popup_SID}).success(function(sInfo) {
    					//Common_Swap.B_A(sInfo, Model.condition);
    					//$('#'+ Model.grid_id).jqGrid('setGridParam', {postData : Model.condition, page : 1}).trigger('reloadGrid');	
    				//});
    			});
    		};
    		reader.readAsBinaryString($files[0]);
    	}
    
}

function showDirEstResultPopup(est_sid,asset_sid){
	//$("#DetailResultIndirEstimationPopup").show();
	var scope = angular.element(document.getElementById('resultDirEstInfoPopup')).scope();
	scope.showDetailResultPopup(est_sid,asset_sid);		 
}