angular.module('app.diagnosis').controller('technicalDiagPopupController', technicalDiagPopupController);
angular.module('app.diagnosis').controller('technicalDiagController', technicalDiagController);

function technicalDiagPopupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, $sce, MaxUploadSize) {
    // $scope.grid_item = {}; // 신규, 수정 등 팝업창
    $scope.answerPlaceholder = ""; // 응답 게시글 제목 (질의/응답)
    $scope.popupStoreFileList = []; // 파일 리스트 (신규, 수정 팝업창)
    $scope.grid_item = {};
    $scope.loadFileList_popup = loadFileList_popup;
    // $scope.file2  = [];
    $scope.uploadfilename = "";
    $scope.fileListLimit = '4'; // 파일 최대 업로드 개수
    $scope.MaxUploadSize = MaxUploadSize.size;
    $scope.MCODE = '0301';
    // console.log(sessionStorage);
    
    $scope.$on('$viewContentLoaded', function () {
        $rootScope.setBtnAuth($scope.MCODE);
    });
   
    //validation 체크에 사용
    var BbsItem = [{
        'name'     : 'SUBJECT',
        'title'    : '자료명',
        'required' : true,
        'maxlength': 100
    }, {
        'name'     : 'CONTENT',
        'title'    : '내용',
        'required' : true,
        'maxlength': 64000
    }, {
        'name'     : 'CATEGORY',
        'title'    : '대상년도',
        'required' : true,
        'maxlength': 4
    }];

    //글자수 바이트에 맞게 자르기
    function cutByLen(str, maxByte) { //문자열, 최대바이트
        for (b = i = 0; c = str.charCodeAt(i);) {
            b += c >> 7 ? 2 : 1;
            if (b > maxByte)
                break;
            i++;
        }
        return str.substring(0, i);
    }

    $scope.newBbsResultInfo = function () {
        $scope.ResultInfo = {};
        // $scope.IsNewRecord = true;
        $scope.trustedHtml = "";
    };

    $rootScope.isEditorInit2 = false;

    function initEditor(){
        console.log('init editor ', $rootScope.isEditorInit2);
        if ($rootScope.isEditorInit2 ) return;
        // 팝업 영역 보여준 후 에디터 생성
        //$timeout(function(){
        //$(".conts_box .main_conts").show();
        $("#test_id_visible").css("display","block"); // 이 영역이 display="none" 되어서 에디터가 적용 안 되는 문제 있음.
        $rootScope.oEditors2 = [];
        console.log('에디터 생성');
        nhn.husky.EZCreator.createInIFrame({
            oAppRef      : $rootScope.oEditors2,
            elPlaceHolder: "TECHDIAG_CONTENT_EDITOR",
            sSkinURI     : "./assets/js/smarteditor2/SmartEditor2Skin.html",
            fCreator     : "createSEditor3",
            htParams : {fOnBeforeUnload : function(){}}
            // fOnAppLoad: function () {
                // $rootScope.oEditors.getById["CONTENT_EDITOR"].exec("SET_IR", [""]); // 초기화
            // }
        });
        $rootScope.isEditorInit2 = true;
        //},100);
    }

    $scope.$on('technicalDiagPopupOpen', function (event, data) {
        // if($scope.page_id == 'parentWindow') return;

        // console.log($rootScope);
        // console.log(data);
        // console.log($scope.page_id);
    	$scope.file2 = null;
    	$scope.uploadfilename = ""; 
        $scope.bbsType = data.bbsType;
        $scope.bbs_group_cd = data.bbs_group_cd;
        $scope.bbs_current_sid = data.bbs_current_sid;
        $scope.parent_bbs_sid = data.parent_bbs_sid;
        $scope.grid_item = data.grid_item;
        $scope.popupStoreFileList = data.storeFileList;
        $scope.ResultInfo = data.ResultInfo;
        $scope.answerPlaceholder = data.answerPlaceholder;
        $scope.MCODE = data.mcode;
        $scope.ResultInfo = data.ResultInfo;
        $scope.reply = data.reply;

        $('#dialog-technicalDiagPopup').show(); // 팝업 보이게 하기
        initEditor(); // 에디터 생성

        $timeout(function(){
            $rootScope.oEditors2.getById["TECHDIAG_CONTENT_EDITOR"].exec("SET_IR", [""]); // 초기화
            if ($scope.bbsType === 'update') {

                //  수정일 때
                if ($scope.reply != undefined) {
                    console.log(' 수정');
                    console.log($scope.reply.CONTENT);
                    $rootScope.oEditors2.getById["TECHDIAG_CONTENT_EDITOR"].exec("PASTE_HTML", [$scope.reply.CONTENT.toString()]);

                } else {
                    console.log($scope.ResultInfo.CONTENT);
                    $rootScope.oEditors2.getById["TECHDIAG_CONTENT_EDITOR"].exec("PASTE_HTML", [$scope.ResultInfo.CONTENT]);
                }
            }
        },500);
    });

    // 신규 또는 수정 모달창에서 취소 버튼 클릭 시
    $scope.cancelPop = function () {
        var subject = $scope.grid_item.SUBJECT;
        var contentEditor = $rootScope.oEditors2.getById["TECHDIAG_CONTENT_EDITOR"].getIR(); // 에디터의 내용 얻기
        var isNotEmpty = false; // 제목 또는 내용에 값이 있으면 true, 없으면 false. 값이 있으면 confirm을 띄워야 한다.

        // 질의/응답 게시판의 '응답'이 아닌 모든 경우 (공지사항, 자료실 게시판이거나 질의/응답의 '질의'일때)
        if ($scope.answerPlaceholder === '') {
            //제목 값이 있으면 isNotEmpty = true
            if (subject != '' && subject != undefined) {
                isNotEmpty = true;
            }
        }

        // 내용 값이 있으면 isNotEmpty = true
        // 질의/응답 게시판의 '응답'인 경우는 위의 제목 체크를 건너뛰고 내용만 체크한다. answerPlaceholder = 're: 부모글제목'이 있기 때문
        if (contentEditor != '' && contentEditor != '<p><br></p>' && contentEditor != '<p></p>' && contentEditor != '<br>') {
            isNotEmpty = true;
        }

        var msg = ($scope.bbsType === 'new') ? '작성' : '수정';
        alertify.confirm('취소 확인', msg + ' 중인 내용이 저장되지 않았습니다. 정말 취소하시겠습니까?',
            function () {
                $('#dialog-technicalDiagPopup').hide();
                if ($scope.bbsType === 'new') {
                    deleteTemp(); // 작성중인 글일 경우 임시글 삭제
                }
                $scope.grid_item = {}; // 모달창 내용 삭제
            },
            function () {
                return;
            }).set('basic', false);

        // 제목 또는 내용에 값이 있을 때는 confirm 창을 띄운다.
        // if (isNotEmpty) {
        //     alertify.confirm('취소 확인', msg + ' 중인 내용이 저장되지 않았습니다. 정말 취소하시겠습니까?',
        //         function () {
        //             $('#dialog-technicalDiagPopup').hide();
        //             if ($scope.bbsType === 'new') {
        //                 deleteTemp(); // 작성중인 글일 경우 임시글 삭제
        //             }
        //             $scope.grid_item = {}; // 모달창 내용 삭제
        //         },
        //         function () {
        //             return;
        //         }).set('basic', false);
        //
        //     // 제목 또는 내용에 값이 없을 때
        // } else {
        //     // $('#bbsPopup').dialog('destroy'); // 다이얼로그 삭제
        //     $('#dialog-technicalDiagPopup').hide();
        //     if ($scope.bbsType === 'new') {
        //         deleteTemp(); // 작성중인 글일 경우 임시글 삭제
        //     }
        //     $scope.grid_item = {}; // 모달창 내용 삭제
        // }
    }

    // 임시글 삭제
    // 신규 모달창이 저장되지 않고 종료(취소 또는 창 닫기)되었을 때 실행
    // M2_BBS에 저장된 임시글 삭제하고, 파일이 있는 경우엔 파일도 삭제함
    function deleteTemp() {
        mainDataService.deleteBbsTempItem({
            bbs_group_cd: $scope.bbs_group_cd,
            bbs_sid     : $scope.bbs_current_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                if (obj > 0) {
                    if ($scope.bbs_group_cd === 'notice' || $scope.bbs_group_cd === 'pbb') {
                        $scope.deleteAllFiles($scope.popupStoreFileList, $scope.bbs_current_sid);
                    }
                    //firstPage("list"); //목록 새로고침
                    $rootScope.$broadcast('refresh_bbs_list',{});
                }
                $scope.grid_item = {};
                $scope.parent_bbs_sid = "";
                $scope.bbs_current_sid = "";
            }
        });
    }

    // 1. M2_BBS_FILE에서 삭제
    // 2. M2_FILE에서 삭제, 실제 경로에서 삭제
    $scope.deleteAllFiles = function (files, bbs_sid) {
        var files = files; // storeFileList 또는 popupStoreFileList
        console.log(files);
        console.log(bbs_sid);

        if (files != undefined && files.length != 0) {
            mainDataService.deleteBbsFile({ // 1. M2_BBS_FILE에서 삭제
                bbs_group_cd: $scope.bbs_group_cd,
                bbs_sid     : bbs_sid
            }).success(function (data) {
                if (!common.isEmpty(data.errMessage)) {
                    alertify.error('errMessage : ' + data.errMessage);
                } else {
                    // 반복문 돌면서 파일 하나씩 삭제
                    for (var i = 0; i < files.length; i++) {
                        console.log(i);
                        // 2. M2_FILE에서 삭제, 실제 경로에서 삭제
                        mainDataService.fileDelete(files[i].F_NUM).success(function (obj) {
                            if (!common.isEmpty(obj.errMessage)) {
                                alertify.error('errMessage : ' + obj.errMessage);
                            } else {
                            }
                        });
                    }
                }
            });
        }
    };

    // 파일삭제 (X 버튼 눌렀을 때 하나씩 삭제)
    $scope.deleteFile = function (file) {
        // console.log(sessionStorage);
        // console.log($scope.MCODE);

        if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
            alertify.alert('Info', '권한이 없습니다.');
            return;
        }

        console.log("file=== ", file);
        console.log($scope.grid_item);

        var bbs_sid = '';

        if ($scope.bbsType === 'new') {
            bbs_sid = $scope.bbs_current_sid;
        } else if ($scope.bbsType === 'update') {
            bbs_sid = $scope.grid_item.BBS_SID;
        }

        console.log("bbs_sid", bbs_sid);
        if (!(bbs_sid > 0)) {
            alertify.alert('Info', '게시물을 선택해주세요');
            return;
        }

        alertify.confirm('삭제 확인', '정말 삭제하시겠습니까?',
            function () {
                //1. 경로에서 파일 삭제, 2. M2_FILE 삭제
                mainDataService.fileDelete(file.F_NUM).success(function (obj) {
                    if (!common.isEmpty(obj.errMessage)) {
                        alertify.error('errMessage : ' + obj.errMessage);
                    } else {
                        console.log(obj);
                        if (parseInt(obj) > 0) {
                            mainDataService.deleteBbsFileOne({ //M2_BBS_FILE 삭제
                                bbs_sid     : bbs_sid,
                                f_num       : file.F_NUM,
                                bbs_group_cd: $scope.bbs_group_cd
                            }).success(function (obj) {
                                if (!common.isEmpty(obj.errMessage)) {
                                    alertify.error('errMessage : ' + obj.errMessage);
                                } else {
                                    if (obj == '1') {
                                        alertify.success('삭제되었습니다.');
                                        loadFileList_popup(bbs_sid);
                                    } else {
                                        alertify.alert('error' + obj).set('basic', true);;
                                    }
                                }
                            });
                        }
                    }
                });
            }, function () {}).set('basic', false);
    };

    // 파일리스트 가져오기 (팝업)
    function loadFileList_popup(bbs_sid) {
        mainDataService.getBbsFileList({
            bbs_group_cd : $scope.bbs_group_cd,
            bbs_sid: bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log("popupStoreFileList=== ", obj);
                $scope.popupStoreFileList = obj;
            }
        });
    }

    // 팝업에서 저장 or 수정 클릭했을 때
    $scope.saveItem = function () {
        // console.log($scope.popupStoreFileList);
        var msg1 = ($scope.bbsType === 'new')? '저장' : '수정';
        var msg2 = ($scope.bbsType === 'new')? '작성' : '수정';

        var contentEditor = $rootScope.oEditors2.getById["TECHDIAG_CONTENT_EDITOR"].getIR(); // 에디터의 내용 얻기
        // console.log(contentEditor);
        contentEditor = contentEditor.replace(/<(\/p|p)([^>]*)>/gi,""); // 내용의 태그 제거
        console.log(contentEditor);
        $scope.grid_item.CONTENT = contentEditor;
        console.log($scope.grid_item);
        console.log($scope.ResultInfo);

        // content에서 html 태그를 제거한 값
        var removeHtmlTags = $scope.grid_item.CONTENT.replace(/(<([^>]+)>)/ig, "");
        console.log(removeHtmlTags);

        // 유효성 검사
        if (!ItemValidation($scope.grid_item, BbsItem)) {
            return;
        }

        // 자료실 -> 첨부파일 없을 시 리턴
        if ($scope.bbs_group_cd === 'pbb') {
            if ($scope.popupStoreFileList === undefined || $scope.popupStoreFileList.length == 0) {
                alertify.alert('Info', '저장된 첨부파일이 없습니다.');
                return;
            }
        }

        alertify.confirm(msg1 + ' 확인', msg2 + '하신 자료를 저장 하시겠습니까?',
            // ok
            function () {
                $scope.grid_item.BBS_GROUP_CD = $scope.bbs_group_cd;
                $scope.grid_item.TYPE = 'update'; // type은 기본으로 update를 설정함

                // 신규 글 작성
                if ($scope.bbsType === 'new') {
                    $scope.grid_item.BBS_SID = $scope.bbs_current_sid; //등록한 임시글의 글 번호
                    $scope.grid_item.TEMP_YN = 'N'; //임시글 해제
                    $scope.grid_item.TYPE = 'new';
                    $scope.grid_item.PARENT_BBS_SID = $scope.parent_bbs_sid; // 질의/응답의 '응답'의 경우에는 부모글 번호가 들어감
                    $scope.grid_item.GUBUN = '';
                    //$scope.grid_item.CATEGORY = "";
                    $scope.grid_item.EVALUE1 = $scope.grid_item.E_VALUE1;

                    // 질의응답 게시판
                    if ($scope.bbs_group_cd === 'qna') {

                        // (응답)일 경우
                        if ($scope.grid_item.PARENT_BBS_SID != null && $scope.grid_item.PARENT_BBS_SID != ""
                            && $scope.grid_item.PARENT_BBS_SID != $scope.grid_item.BBS_SID) {
                            $scope.grid_item.GUBUN = '응답';

                            // var subject_content = $scope.grid_item.SUBJECT + " - " + removeHtmlTags;
                            var subject_content = $scope.ResultInfo.SUBJECT + " - " + removeHtmlTags;
                            subject_content = cutByLen(subject_content, 100);

                            // console.log($scope.ResultInfo.SUBJECT);


                            console.log(subject_content);

                            $scope.grid_item.subject_content = subject_content;
                        } else {
                            $scope.grid_item.GUBUN = '질의';
                        }
                    }
                    // 게시글 수정
                } else if ($scope.bbsType === 'update') {
                	$scope.grid_item.EVALUE1 = $scope.grid_item.E_VALUE1;
                    if ($scope.bbs_group_cd === 'qna') {
                        if ($scope.grid_item.GUBUN === '응답') {
                            // var subject_content = $scope.answerPlaceholder.substr(4) + " - " + removeHtmlTags;
                            var subject_content = $scope.ResultInfo.SUBJECT + " - " + removeHtmlTags;
                            subject_content = cutByLen(subject_content, 100);
                            console.log(subject_content);

                            $scope.grid_item.subject_content = subject_content;

                        } else {
                            $scope.grid_item.GUBUN = '';
                        }
                    }
                }

                var item = angular.copy($scope.grid_item);
                console.log(item);
                console.log($scope.parent_bbs_sid);
                $rootScope.oEditors2.getById["TECHDIAG_CONTENT_EDITOR"].exec("UPDATE_CONTENTS_FIELD", []);
                item.SUBJECT = xssFilter(item.SUBJECT);
                mainDataService.insertBbsItem(item).success(function (obj) { // 신규, 또는 수정
                    if (!common.isEmpty(obj.errMessage)) {
                        alertify.error('errMessage : ' + obj.errMessage);
                    } else {
                        console.log(item);

                        alertify.success('게시글이 ' + msg2 + ' 되었습니다.');
                        if ($scope.bbsType === 'update') {
                            if (item.GUBUN != '응답') {
                            	console.log(item);
                                $rootScope.$broadcast('save_edit_bbs',
                                    {
                                        'subject' : xssFilter(item.SUBJECT),
                                        'content' : item.CONTENT,
                                        'popupStoreFileList' : $scope.popupStoreFileList,
                                        'category' : item.CATEGORY,
                                        'e_value1' : item.E_VALUE1,
                                    });

                            } else if (item.GUBUN === '응답') {
                                $rootScope.$broadcast('save_edit_reply',
                                    {
                                        'parent_bbs_sid' : item.PARENT_BBS_SID
                                    });
                            }
                        }
                        //firstPage("list"); //목록 새로고침
                        $rootScope.$broadcast('refresh_bbs_list',{});
                        $scope.grid_item = {}; //팝업창 리셋
                        $scope.parent_bbs_sid = '';
                        $scope.bbs_sid = '';
                        $('#dialog-technicalDiagPopup').hide();
                    }
                });
            },
            function () {
            }).set('basic', false);
    };

    // 질의/응답 게시판의 댓글 불러오기
    function getReplyList(parent_bbs_sid) {
        mainDataService.getReplyList({
            bbs_group_cd  : $scope.bbs_group_cd,
            parent_bbs_sid: parent_bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errorMessage)) {
                alertify.error('errMessage : ' + obj.errorMessage);
            } else {
                $scope.ResultInfo = obj[0]; // obj의 첫번째 요소를 ResultInfo에 넣음 (부모글)
                $scope.trustedHtml = $sce.trustAsHtml($scope.ResultInfo.CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
                obj.shift(); // obj에서 첫번째 항목 삭제함 (부모글 삭제하고 obj에는 댓글만 남는다)

                $scope.replyList = obj; // 부모글 제거 후 나머지를 댓글에 넣음

                // console.log($scope.replyList);

                // 댓글 개수만큼 반복문 돌면서 html 태그 적용 가능하게 만든다
                for (i = 0; i < $scope.replyList.length; i++) {
                    $scope.replyList[i].CONTENT = $sce.trustAsHtml($scope.replyList[i].CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
                }
            }
        });
    }

    $scope.fileUpload = function (mode) {
    	console.log($scope.popupStoreFileList);
        console.log($scope.fileListLimit);
        if ($scope.popupStoreFileList != undefined && $scope.popupStoreFileList.length >= $scope.fileListLimit) {
            alertify.alert('Info', '첨부파일은 ' + $scope.fileListLimit + '개까지 업로드 가능합니다.');
            return;
        }

        var bbs_sid = $scope.grid_item.BBS_SID;

        // var bbs_current_sid = $scope.bbs_current_sid; //임시글 번호
        if ($scope.grid_item.BBS_SID == null || $scope.grid_item.BBS_SID == '') {
            bbs_sid = $scope.bbs_current_sid; //임시글 번호
        }

        $scope.uploadfilename = ""; // 업로드 하기 전에 빈 값으로 바꾼다.

        if ($scope.file2 == null || $scope.file2.length == 0) {
            alertify.alert('Info', '파일을 선택하세요');
            return;
        }

        if ($scope.file2[0].size === 0) {
            alertify.alert('Info', '사이즈가 0인 파일은 업로드 할 수 없습니다.');
            return;
        }

        if ($scope.file2[0].size > MaxUploadSize.size) {
            alertify.alert('Info', '파일 최대 업로드 용량을 초과하였습니다.');
            return;
        }

        var fileDesc = $scope.file2[0].name;

        if ($scope.file2 != null) {
            // 로딩바
            $('#isLoading').css('display', '');
            var target = document.getElementById('spinnerContainer');
            var spinner = new Spinner().spin(target);

            mainDataService.fileUpload(fileDesc, 'bbs', $scope.file2, '', '').success(function (obj) {
                if (!common.isEmpty(obj.errMessage)) {
                    alertify.error('errMessage : ' + obj.errMessage);
                } else {
                    console.log("m2_file에 insert====", obj);
                    if (parseInt(obj.f_num) > 0) {
                        if (!common.isEmpty(obj.errMessage)) {
                            alertify.alert(obj.errMessage).set('basic', true);
                        } else {
                            mainDataService.saveBbsFile({ // M2_BBS_FILE에 넣는다
                                bbs_group_cd: $scope.bbs_group_cd,
                                f_num       : obj.f_num,
                                bbs_sid     : bbs_sid
                            }).success(function (data) {
                                $('#isLoading').css('display', 'none');
                                spinner.stop();

                                if (!common.isEmpty(data.errMessage)) {
                                    alertify.error('errMessage : ' + data.errMessage);
                                } else {
                                    alertify.success('파일업로드성공');
                                    $scope.loadFileList_popup(bbs_sid); //현재 글 번호 넘겨서 파일 리스트 불러오기
                                    $scope.file2[0].name = '파일을 입력해주세요.';
                                    console.log($scope.file2);
                                }
                                $files = null;
                                $scope.file2 = null;
                                $scope.fileSelected = false;
                            });
                        }
                    } else {
                        alertify.alert('파일업로드 실패').set('basic', true);
                        $('#isLoading').css('display', 'none');
                        spinner.stop();
                    }
                }
            });
        }
    };

    // 파일 업로드 validation check
    // 파일 선택했을 때. 업로드 하기 전
    $scope.onFileSelect = function ($files, cmd) {
        if ($files.length === 0) {
            return;
        }
        var str = '\.(' + cmd + ')$';
        var FileFilter = new RegExp(str);
        var ext = cmd.split('|');
        if ($files[0].name.toLowerCase().match(FileFilter)) {
            $scope.file2 = $files;
            $scope.uploadfilename = $files[0].name; // 파일의 이름을 넣고 화면에서 불러오기 위함.
            // $scope.fileSelected = true;
        } else {
            $files = null;
            $scope.file2 = null;
            $scope.fileSelected = false;
            alertify.alert('Info', '확장자가 ' + ext + ' 인 파일만 선택가능합니다.');
        }
        if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

        } else {
            $scope.$apply();
        }
    };
}

/****** bbs controller ***********/
function technicalDiagController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, $sce) {
	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 6개월
        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.searchStartDt = s_yyyymmdd;
        $('#tech_start_dt').val(s_yyyymmdd);
        $scope.searchEndDt = e_yyyymmdd;
        $('#tech_end_dt').val(e_yyyymmdd);
    }
	
    var Category = '';
    var Category_kr = '';
    $scope.MCODE = '0301';
    Category = 'techdiag';
    Category_kr = '기술진단자료관리';
    $scope.bbs_group_cd = "techdiag";
            
    

    var jopt = '00'; //임의로 설정

    $scope.bbs_sid = '';
    $scope.gubun = '';
    $scope.bbs_current_sid = ''; // 현재 글 번호 (임시글)
    $scope.parent_bbs_sid = ""; // 부모게시글 글 번호 (질의/응답)
    $scope.answerPlaceholder = ""; // 응답 게시글 제목 (질의/응답)
    $scope.ResultInfo = {}; // 화면 우측 게시물 상세
    $scope.grid_item = {}; // 신규, 수정 등 팝업창
    $scope.storeFileList = []; // 파일 리스트
    // $scope.popupStoreFileList = []; // 파일 리스트 (신규, 수정 팝업창)
    $scope.replyList = []; // 화면 우측 게시물 아래 댓글창
    $scope.bbsType = ""; //new(신규) 또는 update(수정)
    $scope.loginId = sessionStorage.getItem('loginId'); // 현재 로그인한 사용자의 아이디


    $scope.$on('$viewContentLoaded', function () {
        console.log($scope.MCODE);
        setDatePicker();
        setGrid();
        gridResize();
        $rootScope.setBtnAuth($scope.MCODE); // 버튼 권한
        $scope.setWeek();
        $scope.searchOption = 1;
        
        
    	/*mainDataService.getCommonCodeList({
            gcode : 'YEAR',
            gname : ''
        }).success(function(data) {
            if (!common.isEmpty(data.errMessage)) {
                alertify.error('errMessage : ' + data.errMessage);
            } else {
            	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
                $scope.ComCodeYearList = data;
            }
        });  */  	
                
    });

    $scope.newBbsResultInfo = function () {
        $scope.ResultInfo = {};
        $scope.trustedHtml = "";
    };
    $scope.setSearchOption = function(val){
    	$scope.searchOption	= val;
    }
    
    $scope.downloadList = function(){
    	var category="";
    	var start_dt="";
    	var end_dt="";
        
    	if($scope.searchOption==1){
    		category= $scope.searchCategory;
    	}else{
    		start_dt = $scope.searchStartDt;
    		end_dt = $scope.searchEndDt;
    		if(start_dt==''|| start_dt==undefined || end_dt=='' || end_dt==undefined){
    			alertify.alert('오류','기간을 선택하세요');
    			return false;
    		}
    	}
    	$('#exportForm').find('[name=jobType]').val("010301");
        $('#exportForm').find('[name=bbs_group_cd]').val("techdiag");
    	$('#exportForm').find('[name=temp_yn]').val("N");
    	$('#exportForm').find('[name=delete_yn]').val("N");
        $('#exportForm').find('[name=category]').val(category);
        $('#exportForm').find('[name=start_dt]').val(start_dt);
        $('#exportForm').find('[name=end_dt]').val(end_dt);
        $('#exportForm').find('[name=searchText]').val($scope.searchSubject || '');
        $('#exportForm').submit();    	
    }
    
    $scope.onKeyPress = function(event){
        if (event.key === 'Enter') {
            $scope.search();
        }
    }
    $scope.$on('refresh_bbs_list',function(event,data){
    	$scope.search(data.opt);
    });    
    $scope.search = function (opt) {
    	console.log($scope.searchOption);
    	var category= "";
    	var start_dt="";
    	var end_dt = "";
    	if($scope.searchOption==1){
    		category= $scope.searchCategory;
    	}else{
    		start_dt = $scope.searchStartDt;
    		end_dt = $scope.searchEndDt;
    		if(start_dt==''|| start_dt==undefined || end_dt=='' || end_dt==undefined){
    			alertify.alert('Info','기간을 선택하세요');
    			return false;
    		}
    		if(end_dt < start_dt ){
    			alertify.alert('Info','기간범위를  확인하세요.시작날짜가 종료날짜 보다 클수 없습니다.');
    			$("#tech_start_dt").focus();
    			return;
    		}    		
    	}
    	
        $('#list').jqGrid('setGridParam', {
        	page    : (opt==1)?1:$scope.currentPageNo,
            rows    : GridConfig.sizeXL,        	
            postData: {
                category: category,
                start_dt : start_dt, 
                end_dt :  end_dt,
                subject : $scope.searchSubject || ''
            }
        }).trigger('reloadGrid', {
            current: true
        });
    };

    function setResultInfo(param) {
        // $scope.ResultInfo = param;


            $scope.ResultInfo = param; // 우측 화면 ResultInfo에 param 넣음
            $scope.trustedHtml = $sce.trustAsHtml($scope.ResultInfo.CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;")); // html 태그 적용하기 위해 ng-bing-html 사용
            loadFileList(param.BBS_SID); // 파일 리스트 불러오기

        if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

        } else {
            $scope.$apply();
        }
    }

    function setGrid() {
        var colNames = [];
        var colModel = [];

        // colNames.push(
        //     '순번', '제목'
        // );

        colNames.push(
            '순번','글번호', '자료명','대상년도','진단비용'
        );

        colModel.push(
            {
                name : 'RNUM',
                width: "40px",
                index: 'RNU',
                resizable: false
            },
            {
                name : 'BBS_SID',
                hidden : true,
                width : 0,
                index : 'BBS_SID'
            },            
            {
                name : 'SUBJECT',
                width: 150,
                align    : 'center',
                index    : 'SUBJECT',
                resizable: false
            },
            {
            	name : 'CATEGORY',
            	width : 80,
            	index    : 'CATEGORY',
            	resizable: false
            },
            {
            	name : 'E_VALUE1',
            	hidden:true,
            	width : 0,
            	index    : 'E_VALUE1'
            }            
        );


        colModel.push(
            {
                name : 'CREATEUSER',
                // width: 70,
                width: 80,
                index: 'CREATEUSER',
                resizable: false

            }, {
                name  : 'CONTENT',
                width : 0,
                index : 'CONTENT',
                hidden: true,
            }, {
                name : 'CREATEDT',
                // width: 100,
                width: 80,
                index: 'CREATEDT',
                resizable: false
            // }, {
            //     name  : 'BBS_SID', // '글번호'로 화면에 보이게 처리함
            //     width : 0,
            //     index : 'BBS_SID',
            //     hidden: true
            }, {
                name  : 'PARENT_BBS_SID',
                width : 0,
                index : 'PARENT_BBS_SID',
                hidden: true
            }, {
                name  : 'E_ID',
                width : 0,
                index : 'E_ID',
                hidden: true
            }
        );

        colNames.push('등록자', '내용', '등록일자',
            // '글번호',
            '부모글번호', '작성자아이디');

        return pagerJsonGrid({
            grid_id    : 'list',
            pager_id   : 'listPager',
            url        : '/bbs/getBBSList.json',
            condition  : {
                page        : 1,
                rows        : GridConfig.sizeM,
                bbs_group_cd: $scope.bbs_group_cd,
                temp_yn     : 'N',
                delete_yn   : 'N',
            },
            rowNum     : GridConfig.sizeM,
            colNames   : colNames,
            colModel   : colModel,
            onSelectRow: function (rowid, status, e) {
                var param = $(this).jqGrid('getRowData', rowid);
                console.log("selectParam === ", param);
                $scope.replyList = {};

                if (parseInt(param.BBS_SID) > 0) {
                    // $scope.bbs_current_sid = param.BBS_SID; //현재 글 번호
                    $scope.bbs_sid = param.BBS_SID;
                    // 부모 글 번호. 공지사항, 자료실 게시판에서는 빈 값이 들어간다.
                    // $scope.bbs_current_sid = param.PARENT_BBS_SID;
                    $scope.gubun = param.GUBUN;

                    updateReadCount(param.BBS_SID); // 조회수 증가
                    setResultInfo(param); // 오른쪽에 글 세팅

                } else {
                    $scope.newBbsResultInfo(); // 오른쪽 영역 비어있게 함.
                    $scope.storeFileList = []; //첨부파일 초기화
                }
                if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

                } else {
                    $scope.$apply();
                }
            },
            gridComplete : function() {
            	if($scope.bbs_sid==0 || typeof  $scope.bbs_sid =='undefined'){
    				var ids = $(this).jqGrid('getDataIDs');
    				$(this).setSelection(ids[0]);
            	}
				//alert($('#list').getGridParam('page'));
				$scope.currentPageNo=$('#list').getGridParam('page');
				$scope.totalCount = $('#list').getGridParam("records")
				if($('#list').jqGrid('getRowData')[0].RNUM=="") $scope.totalCount = 0; 
				$scope.$apply();
				// if ($.isEmptyObject($stateParams.info)) {
				// $('#facility').setSelection(ids[0]);
				// }
			}             
        });

    }

    // 파일리스트 가져오기
    function loadFileList(bbs_sid) {
        mainDataService.getBbsFileList({
            bbs_group_cd: $scope.bbs_group_cd,
            bbs_sid: bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                console.log("storeFileList=== ", obj);
                $scope.storeFileList = obj;
            }
        });
    }

    // 질의/응답 게시판의 댓글 불러오기
    function getReplyList(parent_bbs_sid) {
        mainDataService.getReplyList({
            bbs_group_cd  : $scope.bbs_group_cd,
            parent_bbs_sid: parent_bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errorMessage)) {
                alertify.error('errMessage : ' + obj.errorMessage);
            } else {
                $scope.ResultInfo = obj[0]; // obj의 첫번째 요소를 ResultInfo에 넣음 (부모글)
                $scope.trustedHtml = $sce.trustAsHtml($scope.ResultInfo.CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
                obj.shift(); // obj에서 첫번째 항목 삭제함 (부모글 삭제하고 obj에는 댓글만 남는다)

                $scope.replyList = obj; // 부모글 제거 후 나머지를 댓글에 넣음

                // console.log($scope.replyList);

                // 댓글 개수만큼 반복문 돌면서 html 태그 적용 가능하게 만든다
                for (i = 0; i < $scope.replyList.length; i++) {
                    $scope.replyList[i].CONTENT = $sce.trustAsHtml($scope.replyList[i].CONTENT.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
                }
            }
        })
    }

    $scope.loadFileList = loadFileList;
    // $scope.loadFileList_popup = loadFileList_popup;

    $scope.newTempItem = function () {
        $scope.newBbsResultInfo(); //item 초기화
        $scope.storeFileList = []; //첨부파일 초기화
        $scope.popupStoreFileList = []; // 팝업 첨부파일 초기화
        $scope.grid_item = {}; //팝업창 초기화
        $scope.replyList = {}; //댓글창 초기화
        $scope.answerPlaceholder = ""; //댓글창의 제목 초기화
        $scope.bbsType = "new";

        //신규 버튼 클릭 시
        //DB에 임시값을 저장
        mainDataService.insertBbsTempItem({
            BBS_GROUP_CD  : $scope.bbs_group_cd,
            parent_bbs_sid: $scope.parent_bbs_sid // 매퍼 로직이 복잡해서 수정함. 빈 값으로 넘긴다.
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                $scope.newBbsResultInfo(); // resultInfo 초기화
                $scope.bbs_current_sid = obj; // obj : insert한 임시글 번호

                console.log("현재글번호?", $scope.bbs_current_sid);

                //DB 임시값 저장 성공 했을 경우 신규 팝업 띄우기
                $rootScope.$broadcast('technicalDiagPopupOpen',
                    {
                        'bbsType': $scope.bbsType,
                        'bbs_current_sid': $scope.bbs_current_sid,
                        'bbs_group_cd': $scope.bbs_group_cd,
                        'parent_bbs_sid': $scope.parent_bbs_sid,
                        'grid_item': $scope.grid_item,
                        'mcode': $scope.MCODE
                    });
            }
        });
    };

    $scope.deleteItem = function (reply) {
        // 삭제 권한 체크
        if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
            alertify.alert('권한이 없습니다.').set('basic', true);
            return;
        }

        var writerId = "";
        var gubun = '';

        // 댓글 삭제일때
        if (reply != undefined) {
            $scope.bbs_sid = reply.BBS_SID;
            gubun = reply.GUBUN;
            writerId = reply.E_ID;

            // 원글 삭제일때 (공지사항, 자료실 게시판의 글 삭제 또는 질의/응답 게시판의 부모글 삭제)
        } else {
            $scope.bbs_sid = $scope.ResultInfo.BBS_SID;
            writerId = $scope.ResultInfo.E_ID;
            gubun = $scope.ResultInfo.GUBUN;
            // parent_bbs_sid = $scope.ResultInfo.PARENT_BBS_SID;
        }

        if (!($scope.bbs_sid > 0)) {
            console.log($scope.bbs_sid);
            alertify.alert('Info', '게시물을 선택해주세요');
            return;
        }

        if (sessionStorage.getItem($scope.MCODE + 'E') === 'true' && $scope.loginId != writerId) {
//            alertify.alert('권한이 없습니다.').set('basic', true); // 문구 수정?
            alertify.alert('Info', '권한이 없습니다.'); // 문구 수정?
            return;
        }

        alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?',
            // ok
            function () {
                // 1. 공지사항, 자료실 => 첨부파일 O
                if ($scope.bbs_group_cd === 'notice' || $scope.bbs_group_cd === 'pbb' || $scope.bbs_group_cd === 'techdiag') {
                    // M2_BBS 삭제
                    mainDataService.deleteBbsItem({
                        bbs_group_cd: $scope.bbs_group_cd,
                        // bbs_sid: $scope.ResultInfo.BBS_SID,
                        bbs_sid       : $scope.bbs_sid,
                        parent_bbs_sid: $scope.ResultInfo.PARENT_BBS_SID,
                        // parent_bbs_sid: parent_bbs_sid,
                        delete_yn: 'Y',
                        // gubun    : $scope.gubun
                        gubun: gubun
                    }).success(function (obj) {
                        if (!common.isEmpty(obj.errMessage)) {
                            alertify.error('errMessage : ' + obj.errMessage);
                        } else {
                            //firstPage("list"); //목록 새로고침
                        	$rootScope.$broadcast('refresh_bbs_list',{opt:1});
                            alertify.success('삭제되었습니다.');
                            console.log('삭제');
                            $scope.deleteAllFiles($scope.storeFileList, $scope.ResultInfo.BBS_SID); // 파일 삭제 (M2_BBS_FILE, M2_FILE, 실제 경로에서 삭제)
                            $scope.newBbsResultInfo(); // ResultInfo 초기화
                            $scope.storeFileList = {};
                        }
                    });

                    // 2. 질의/응답 => 첨부파일 X
                } else if ($scope.bbs_group_cd === 'qna') {
                    mainDataService.deleteBbsItem({
                        bbs_group_cd: $scope.bbs_group_cd,
                        // bbs_sid: $scope.ResultInfo.BBS_SID,
                        bbs_sid       : $scope.bbs_sid,
                        parent_bbs_sid: $scope.ResultInfo.PARENT_BBS_SID,
                        // parent_bbs_sid: parent_bbs_sid,
                        delete_yn: 'Y',
                        // gubun    : $scope.gubun
                        gubun: gubun
                    }).success(function (obj) {
                        if (!common.isEmpty(obj.errMessage)) {
                            alertify.error('errMessage : ' + obj.errMessage);
                        } else {
                            //firstPage("list"); //목록 새로고침
                        	$rootScope.$broadcast('refresh_bbs_list',{});
                            alertify.success('삭제되었습니다.');

                            // 부모글 삭제의 경우 우측 ResultInfo, 댓글창 초기화
                            if (reply == undefined) {
                                $scope.newBbsResultInfo();
                                $scope.replyList = {};

                                // 댓글 삭제의 경우 댓글창 새로고침
                            } else {
                                getReplyList($scope.ResultInfo.PARENT_BBS_SID);
                            }
                        }
                    });
                }
            },

            // cancel
            function () {
            }
        ).set('basic', false);
    };

    $scope.$on("save_edit_bbs", function(event, data){
    	console.log(data)
        console.log(data.subject);
        // alert('반영');

        $scope.ResultInfo.SUBJECT = data.subject;
        $scope.trustedHtml = $sce.trustAsHtml(data.content.replace(/<script[^>]*?>/gi,"&lt;script&gt;").replace(/<(\/?)script>/gi,"&lt;/script&gt;"));
        $scope.storeFileList = data.popupStoreFileList;
        $scope.ResultInfo.CATEGORY = data.category;
        $scope.ResultInfo.E_VALUE1 = data.e_value1;

    });

    $scope.$on("save_edit_reply", function (event, data) {
        console.log(data);
        getReplyList(data.parent_bbs_sid);
    });

    //수정 클릭 시 게시물 수정 팝업 띄우기
    $scope.updateItemPopup = function (reply) {
        // $scope.ResultInfo = angular.copy();
        var param = angular.copy($scope.ResultInfo);
        $scope.popupStoreFileList = $scope.storeFileList; // 수정 팝업창의 첨부파일에 메인 화면의 첨부파일 넣기
        // $scope.storeFileList = {}; // 첨부파일 초기화
        // $scope.replyList = {}; //댓글창 초기화
        $scope.bbsType = "update";

        var loginId = sessionStorage.getItem('loginId'); // 현재 로그인한 사용자의 아이디

        if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
            alertify.alert('Info', '권한이 없습니다.');
            return;
        }

        if (!($scope.isAdmin || loginId === $scope.ResultInfo.E_ID)) {
            alertify.alert('Info', '권한이 없습니다.'); // 문구 수정?
            return;
        }

        console.log(reply);
        var bbs_sid = "";

        // 댓글일 때
        if (reply != undefined) {
            bbs_sid = reply.BBS_SID;

            // 댓글 아닐때
        } else {
            // bbs_sid = $scope.ResultInfo.BBS_SID;
            bbs_sid = param.BBS_SID;
            $scope.answerPlaceholder = "";
        }

        console.log("수정할 글 번호", bbs_sid);

        if (!(bbs_sid > 0)) {
//            alertify.alert('게시물을 선택해주세요').set('basic', true);
            alertify.alert('Info', '게시물을 선택해주세요');
            return;
        }

        if (reply != undefined) {
            $scope.grid_item = reply;
            // $scope.answerPlaceholder = 're: ' + $scope.ResultInfo.SUBJECT;
            $scope.answerPlaceholder = 're: ' + reply.SUBJECT; // 2021-10-22 수정. 댓글 수정 팝업에서 제목 수정

            console.log($scope.answerPlaceholder);
        } else {
            $scope.grid_item = param; //수정 팝업창에 선택된 글의 제목, 내용 넣는다.
            console.log($scope.grid_item);
        }

        $rootScope.$broadcast('technicalDiagPopupOpen', /* bbsUpdatePopupOpen */
            {
                'bbsType': $scope.bbsType,
                'bbs_current_sid': $scope.bbs_current_sid,
                'bbs_group_cd': $scope.bbs_group_cd,
                'parent_bbs_sid': $scope.parent_bbs_sid,
                'grid_item': $scope.grid_item,
                'storeFileList': $scope.storeFileList,
                'answerPlaceholder': $scope.answerPlaceholder,
                'mcode': $scope.MCODE,
                'ResultInfo': $scope.ResultInfo,
                'reply': reply
            });
    }

    $scope.insertReply = function (rowid) {
        $scope.parent_bbs_sid = $scope.ResultInfo.BBS_SID;
        $scope.bbsType = "new";

        console.log("부모글번호", $scope.parent_bbs_sid);

        if ($scope.parent_bbs_sid == null || $scope.parent_bbs_sid == "") {
            alertify.alert('Info', '게시물을 선택해주세요');
            return;
        }

        if ($scope.ResultInfo.GUBUN != '질의') {
            alertify.alert('Info', '댓글 작성은 질의 게시물에만 가능합니다.'); //문구 수정
            return;
        }

        //신규 버튼 클릭 시
        //DB에 임시값을 저장
        mainDataService.insertBbsTempItem({
            BBS_GROUP_CD  : $scope.bbs_group_cd,
            PARENT_BBS_SID: $scope.parent_bbs_sid, //부모글 번호
            // QNA_TYPE      : 'answer' //응답
            gubun: '응답'
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                $scope.bbs_current_sid = obj; // obj : insert한 임시글 번호
                $scope.answerPlaceholder = 're: ' + $scope.ResultInfo.SUBJECT;
                $scope.replyList = {}; //댓글창 초기화

                //DB 임시값 저장 성공 했을 경우 신규 팝업 띄우기
                $scope.grid_item.SUBJECT = $scope.ResultInfo.SUBJECT; //제목은 고정값으로
                $scope.newBbsResultInfo(); //resultInfo 초기화
                $scope.grid_item.CONTENT = '';

                $rootScope.$broadcast('technicalDiagPopupOpen',
                    {
                        'bbsType': $scope.bbsType,
                        'bbs_current_sid': $scope.bbs_current_sid,
                        'bbs_group_cd': $scope.bbs_group_cd,
                        'parent_bbs_sid': $scope.parent_bbs_sid,
                        'grid_item': $scope.grid_item,
                        'ResultInfo': $scope.ResultInfo,
                        'answerPlaceholder': $scope.answerPlaceholder,
                        'mcode': $scope.MCODE
                    });
            }
        });
    }

    function updateReadCount(bbs_sid) {
        mainDataService.updateReadCount({
            bbs_group_cd: $scope.bbs_group_cd,
            bbs_sid     : bbs_sid
        }).success(function (obj) {
            if (!common.isEmpty(obj.errorMessage)) {
                alertify.error('errMessage : ' + obj.errorMessage);
            } else {
            }
        });
    };

    // 1. M2_BBS_FILE에서 삭제
    // 2. M2_FILE에서 삭제, 실제 경로에서 삭제
    $scope.deleteAllFiles = function (files, bbs_sid) {
        var files = files; // storeFileList 또는 popupStoreFileList
        console.log(files);
        console.log(bbs_sid);

        if (files != undefined && files.length != 0) {
            mainDataService.deleteBbsFile({ // 1. M2_BBS_FILE에서 삭제
                bbs_group_cd: $scope.bbs_group_cd,
                bbs_sid     : bbs_sid
            }).success(function (data) {
                if (!common.isEmpty(data.errMessage)) {
                    alertify.error('errMessage : ' + data.errMessage);
                } else {
                    // 반복문 돌면서 파일 하나씩 삭제
                    for (var i = 0; i < files.length; i++) {
                        console.log(i);
                        // 2. M2_FILE에서 삭제, 실제 경로에서 삭제
                        mainDataService.fileDelete(files[i].F_NUM).success(function (obj) {
                            if (!common.isEmpty(obj.errMessage)) {
                                alertify.error('errMessage : ' + obj.errMessage);
                            } else {
                            }
                        });
                    }
                }
            });
        }
    };



}