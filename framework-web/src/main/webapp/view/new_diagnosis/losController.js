angular.module('app.diagnosis').controller('losController', losController);
angular.module('app.diagnosis').controller('diagnosis_popupController', diagnosis_popupController);
angular.module('app.diagnosis').controller('import_popupController', import_popupController);
angular.module('app.diagnosis').controller('los_popup2Controller', los_popup2Controller);


function losController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 6개월
        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.Serch.Info.BEFORE = s_yyyymmdd;
        $('#Serch_BEFORE').val(s_yyyymmdd);
        $scope.Serch.Info.AFTER = e_yyyymmdd;
        $('#Serch_AFTER').val(e_yyyymmdd);
    }
	
	 $scope.$on('$viewContentLoaded', function () {
    	$("#tab1_btn_M").on("click", function() {
    		$(this).addClass("active");
    		$("#tab2_btn_M").removeClass("active");
    		$("#tab1_M").show();
    		$("#tab2_M").hide();
    	});
    	$("#tab2_btn_M").on("click", function() {
    		$(this).addClass("active");
    		$("#tab1_btn_M").removeClass("active");
    		$("#tab2_M").show();
    		$("#tab1_M").hide();
    	});
    	$scope.setWeek();
    	 pagerJsonGrid(Model);
    	 setDatePicker();
    	 $scope.MCODE = '0305';
    	 $rootScope.setBtnAuth($scope.MCODE);
		 //평가관리 
		 los.Set(function(chartData){
			 console.log(chartData)
			 //dataArray[0].LOS_YEAR, Number(dataArray[0].LOS_YEAR) - 1
			 GraphModal.Init('인력',chartData, los.Info.LOS_YEAR, Number(los.Info.LOS_YEAR) - 1);
		 });
		 //가중치 분석 초기화면
		 weight.Init(0);
    });
	 
	 $scope.clickTable = function($event){
	    	$($event.currentTarget).addClass('active').siblings().removeClass('active');
	 };
	 
	let Serch = {
		Info: {
			LOS_YEAR: '',
            BEFORE: '',
            AFTER:'',
            OPTION: 1
		},
		Lookup : function() {
			if(this.Info.OPTION == 2 &&this.Info.BEFORE > this.Info.AFTER){
				alertify.alert("오류","시작날짜와 종료날짜를 확인해 주세요.");
				return;
			}
			Common_Swap.B_A(Serch.Info, Model.condition);
			 $('#'+ Model.grid_id).jqGrid('setGridParam', {postData : Serch.Info, page : 1}).trigger('reloadGrid');
		},
		Delete : function() {
			//알러트
			alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?',function () {
					mainDataService.CommunicationRelay("/los/deleteLos.json",los.Info).success(function(obj) {
						if(Common_AlertStr(obj)) return;
						
						$('#'+ Model.grid_id).jqGrid('setGridParam', {postData : Model.condition, page : 1}).trigger('reloadGrid');
					});
					},function () {}).set('basic', false);
		},
		Output : function() {
			let jobType = $state.current.name+ "_" + 1;
			let info = Model.condition;
			Common_Excel.ExcelDown(jobType, info);
		},
		Select : function(option) {
			console.log(option)
			this.Info["OPTION"] = option;
		}
	}
	
	let OptionList = null;
	let WeightOptionList = null;
	 
	let Model = {
            grid_id : 'list',
            pager_id : 'listPager',
            url : '/los/getList.json',
            condition : {
                page : 1,
                LOS_YEAR: '',
                BEFORE: '2022-01-01',
                AFTER:'2022-02-28',
                OPTION: 1,
                rows : GridConfig.sizeL
            },
            rowNum : 20,
            colNames : [ '순번','평가 기준 년도','작성자','작성 일자','진행 사항','평가 번호','수정자','수정일','사업소','가중치sid','sid'],
            colModel : [
            	{name : 'RNUM', width : "40px", sortable:false, resizable: false}, 
 	            {name : 'LOS_YEAR', width : 97, resizable: false}, 
 	            {name : 'INSERT_ID', width : 97, resizable: false},
	            {name : 'INSERT_DT', width : 97, resizable: false},
	            {name : 'COMPLETE_YN', width : 97, resizable: false},
	            {name : 'LOS_ITEM_CD', width : 0, hidden : true},
 	            {name : 'UPDATE_ID', width : 0, hidden : true},
 	            {name : 'UPDATE_DT', width : 0, hidden : true},
 	            {name : 'LOS_MEMO', width : 0, hidden : true},
 	            {name : 'LOS_WEIGHT_DT', width : 0, hidden : true},
 	            {name : 'LOS_EVALUE_SID', width : 0, hidden : true}
            ],
            onSelectRow : function(id) {
            	Common_Swap.B_A($('#list').jqGrid('getRowData', id), los.Info);
            	
            	los.Set(function(){
            		tep.Click(0);
            	});
            	$scope.$apply();
            },
            //평가항목 숫자를 문자로
            loadComplete: function(){
            	var grid = $("#list"),
            	ids = grid.jqGrid("getDataIDs");
            	if(ids && ids.length > 0){
            	    grid.jqGrid("setSelection", ids[0]);
            	}
			},
    }
	let GraphModal = {
			//id
			ID : 'Graph',
			//초기화
			Init : function(caption , dataArray = [], year1, year2) {
				rMateChartH5.create("chart1", this.ID, "", "100%", "100%");
				
				this.Set(caption, dataArray, year1, year2);
				 
				rMateChartH5.registerTheme(rMateChartH5.themes);
			},
			//셋팅
			Set : function(caption ='' , dataArray = [], year1, year2) {
				rMateChartH5.calls("chart1", {
	 				"setLayout" : this.GetLayer(caption, year1, year2),
	 				"setData" : dataArray
				});
			},
			//레이어
	 		GetLayer : function(caption ='', year1, year2) {
			 			return '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
		                   +'<Options>'
		                      +'<Caption text="' + caption + '" />'
		                     +'<Legend useVisibleCheck="true"/>'
		                   +'</Options>'
		                 +'<RadarChart isSeriesOnAxis="true" type="polygon" paddingTop="25" paddingBottom="25" showDataTips="true">'
		                           +'<radialAxis>'
		                               +'<LinearAxis id="rAxis"/>'
		                           +'</radialAxis>'
		                          +'<angularAxis>'
		                              +'<CategoryAxis id="aAxis" categoryField="EVALUE_NM" displayName="EVALUE_NM"/>'
		                         +'</angularAxis>'
		                         /* radial Axis 세로축만 표시 */
		                           +'<radialAxisRenderers>'
		                              +'<Axis2DRenderer axis="{rAxis}" horizontal="true" visible="false" tickPlacement="outside" tickLength="1">'
		                                   +'<axisStroke>'
		                                       +'<Stroke color="#555555" weight="0.1"/>'
		                                   +'</axisStroke>'
		                              +'</Axis2DRenderer>'
		                              +'<Axis2DRenderer axis="{rAxis}" horizontal="false" visible="true" tickPlacement="outside" tickLength="1">'
		                                   +'<axisStroke>'
		                                       +'<Stroke color="#555555" weight="0.1"/>'
		                                   +'</axisStroke>'
		                              +'</Axis2DRenderer>'
		                          +'</radialAxisRenderers>'
		                         +'<angularAxisRenderers>'
		                             +'<AngularAxisRenderer axis="{aAxis}"/>'
		                          +'</angularAxisRenderers>'
		                          +'<series>'
		                         +'<RadarSeries field="LOS_RESULT_VAL" radius="4" displayName="'+year1+'" fillLineArea="false">'
		                               +'<fill>'
		                                 +'<SolidColor color="#064d81"/>'
		                              +'</fill>'
		                                +'<lineStroke>'
		                                   +'<Stroke color="#064d81" weight="2"/>'
		                               +'</lineStroke>'
		                              +'<showDataEffect>'
		                                   +'<SeriesInterpolate/>'
		                               +'</showDataEffect>'
		                          +'</RadarSeries>'
		                         +'<RadarSeries field="LOS_RESULT_VAL2" radius="4" displayName="'+year2+'" fillLineArea="false">'
		                               +'<fill>'
		                                 +'<SolidColor color="#21cbc0"/>'
		                              +'</fill>'
		                                +'<lineStroke>'
		                                   +'<Stroke color="#21cbc0" weight="2"/>'
		                               +'</lineStroke>'
		                              +'<showDataEffect>'
		                                   +'<SeriesInterpolate/>'
		                               +'</showDataEffect>'
		                          +'</RadarSeries>'
		                       +'</series>'
		                    +'</RadarChart>'
		              +'</rMateChart>';
			 		}
			}
	
	let tep = {
			 //페이지
			 Page : 0,
			 //리스트
			 List : null,
			 //텝 이름
			 PageStrList : ["인력","시설", "운영", "서비스", "환경","재정"],
			 //텝 선택
			 Click : function(page) {
				 this.Page = page;
				 let Array = this.List[page];
				 //종합 리스트 묶음
				 if(page == 6){
					 Array = [];
					 for(let i=0; i < tep.List.length -1; i++){
						 Array.push(...tep.List[i]);
					 }
				 }
				 //화면에 등록
				 los.List = Array;
				 //그래프 출력?
				 los.isGraphShow = (this.List[page].length ==0)? false: true;
				 
				 //변경된 그래프
				 GraphModal.Set(this.PageStrList[page], this.List[page], los.Info.LOS_YEAR, Number(los.Info.LOS_YEAR) - 1);
			 }
	}
	
	let los = {// 평과 관리
		Info : {
			 LOS_EVALUE_SID: 0,
			 LOS_MEMO : '',
			 LOS_YEAR : '',
			 UPDATE_ID : '',
			 UPDATE_DT : '',
			 INSERT_ID : '',
			 INSERT_DT : ''
		},
		List :null,
		//자료 입력
		Triger : function() {
			 //화면 초기화
			 $('#'+ Model.grid_id).jqGrid('setGridParam', {postData : Model.condition, page : 1}).trigger('reloadGrid');
		},
		Set: function(delegate) {
			mainDataService.CommunicationRelay("/los/getEvalue.json",los.Info).success(function(map) {
				console.log(map)
				 tep.List = new Array([],[],[],[],[],[],[])
				 let array = map.Array;
				 for(i in array){
					 tep.List[Number(array[i].LOS_ITEM_CD) - 1].push(array[i])
				 }
				 los.List = tep.List[0];
				 let chartData = tep.List[0];
				 los.Info = map.Info;
				 delegate(chartData);
				
			 });
		},
		New : function() {
			$rootScope.$broadcast("diagnosis_popup",{
				Triger : this.Triger, 
				WeightInfo: weight.WeightInfo,
				Info : {LOS_YEAR: ''}
			});
		},
		Modify : function() {
			$rootScope.$broadcast("diagnosis_popup",{
				Triger : this.Triger, 
				WeightInfo: weight.WeightInfo, 
				Info : this.Info
			});
		},
		Report: function(){
			let jobType = $state.current.name;
			window.open('/excel/downloadExcel.do?EXCEL_ID=' + jobType + '&LOS_EVALUE_SID='+ this.Info.LOS_EVALUE_SID,'_blank');
			//Common_Excel.ExcelDown(jobType, this.Info);
		}
	}
	
	let weight = {// 가중치 관리
		 Info: {
			 LOS_DT: '',
			 CHOICE_YN: '',
			 INSERT_ID: '',
			 INSERT_DT: '',
			 UPDATE_ID: '',
			 UPDATE_DT: ''
		 },
		 WeightInfo: null,
		 WeightOptionList: null,
		 List: null,
		 Init: function(index) {
			 mainDataService.CommunicationRelay("/los/getWeightList.json",{index: index}).success(function(map) {
				 console.log(map)
				 let opt = map.OptionList;
				 OptionList = [];
				 WeightOptionList = [];
				 for(let i =0; i < 6; i++){
					 OptionList.push({LOS_ITEM_CD: opt[i].C_SCODE, LOS_ITEM_NM : opt[i].C_NAME});
					 WeightOptionList[opt[i + 6].C_SCODE] =  opt[i + 6].C_NAME;
				 }
				 console.log(OptionList)
				 console.log(WeightOptionList)
				 weight.Info = map.Info;
				 weight.Info.CHOICE_NM = weight.Info.CHOICE_YN === 'Y'?  "가중치 적용" : "가중치 비적용";
				 weight.WeightInfo = map.WeightInfo;
				 weight.WeightOptionList = map.WeightOptionList;
				 let option = weight.WeightOptionList;
				 for(i in option){
					 option[i].WEIGHT_YN = [false,false,false,false,false];
					 option[i].WEIGHT_YN[option[i].CHK_VAL - 1] = true;
					 option[i].LOS_ITEM_NM = WeightOptionList[option[i].LOS_ITEM_CD];
					 option[i].COMPARE_ITEM_NM = WeightOptionList[option[i].COMPARE_ITEM_CD];
				 }
				 weight.List = map.List;
			 });
		 },
		 Triger: function() {
			 weight.Init(0);
		 },
		 New: function(){
			 
			 let WeightOptionList =[
				    {
				    	"COMPARE_ITEM_CD": "02",
				        "LOS_ITEM_CD": "01",
				        "CHK_VAL": "",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도인력",
				        "COMPARE_ITEM_NM": "상수도시설 점검"
				    },
				    {
				        "COMPARE_ITEM_CD": "03",
				        "LOS_ITEM_CD": "01",
				        "CHK_VAL": "2",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도인력",
				        "COMPARE_ITEM_NM": "상수도 운영 효율"
				    },
				    {
				        "COMPARE_ITEM_CD": "04",
				        "LOS_ITEM_CD": "01",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도인력",
				        "COMPARE_ITEM_NM": "서비스"
				    },
				    {
				        "COMPARE_ITEM_CD": "05",
				        "LOS_ITEM_CD": "01",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도인력",
				        "COMPARE_ITEM_NM": "환경"
				    },
				    {
				        "COMPARE_ITEM_CD": "06",
				        "LOS_ITEM_CD": "01",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도인력",
				        "COMPARE_ITEM_NM": "수도사업소 재정 확보"
				    },
				    {
				        "COMPARE_ITEM_CD": "03",
				        "LOS_ITEM_CD": "02",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도시설 점검",
				        "COMPARE_ITEM_NM": "상수도 운영 효율"
				    },
				    {
				        "COMPARE_ITEM_CD": "04",
				        "LOS_ITEM_CD": "02",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도시설 점검",
				        "COMPARE_ITEM_NM": "서비스"
				    },
				    {
				        "COMPARE_ITEM_CD": "05",
				        "LOS_ITEM_CD": "02",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도시설 점검",
				        "COMPARE_ITEM_NM": "환경"
				    },
				    {
				        "COMPARE_ITEM_CD": "06",
				        "LOS_ITEM_CD": "02",
				        "CHK_VAL": "4",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도시설 점검",
				        "COMPARE_ITEM_NM": "수도사업소 재정 확보"
				    },
				    {
				        "COMPARE_ITEM_CD": "04",
				        "LOS_ITEM_CD": "03",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도 운영 효율",
				        "COMPARE_ITEM_NM": "서비스"
				    },
				    {
				        "COMPARE_ITEM_CD": "05",
				        "LOS_ITEM_CD": "03",
				        "CHK_VAL": "4",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도 운영 효율",
				        "COMPARE_ITEM_NM": "환경"
				    },
				    {
				        "COMPARE_ITEM_CD": "06",
				        "LOS_ITEM_CD": "03",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "상수도 운영 효율",
				        "COMPARE_ITEM_NM": "수도사업소 재정 확보"
				    },
				    {
				        "COMPARE_ITEM_CD": "05",
				        "LOS_ITEM_CD": "04",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "서비스",
				        "COMPARE_ITEM_NM": "환경"
				    },
				    {
				        "COMPARE_ITEM_CD": "06",
				        "LOS_ITEM_CD": "04",
				        "CHK_VAL": "3",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "서비스",
				        "COMPARE_ITEM_NM": "수도사업소 재정 확보"
				    },
				    {
				        "COMPARE_ITEM_CD": "06",
				        "LOS_ITEM_CD": "05",
				        "CHK_VAL": "4",
				        "LOS_DT": "20220101",
				        "WEIGHT_YN": [ false, false, false, false,false],
				        "LOS_ITEM_NM": "환경",
				        "COMPARE_ITEM_NM": "수도사업소 재정 확보"
				    }
				]
			 
			 let dt = Common_Time.toDay().START_DT;
			 
			 $rootScope.$broadcast("los_popup2",{
				 Info: {
					 LOS_DT: dt.split('-').join(''), 
					 WEIGHT_NM: '',
					 CHOICE_YN: 'N', 
					 INSERT_ID: sessionStorage.getItem('loginId'), 
					 INSERT_DT: dt
				 },
				 WeightInfo: [],
				 WeightOptionList: WeightOptionList,
				 SaveSet: new Set(),
				 Triger: this.Triger,
				 saveType:'등록',
				 isConsistency: false,
				 isSelect: true
			 });
		 },
		 Modify: function(){
			 let WeightOptionList = angular.copy(this.WeightOptionList);
			 let dt = Common_Time.toDay().START_DT;
			 
			 let info = angular.copy(this.Info);
			 info.UPDATE_ID = sessionStorage.getItem('loginId');
			 info.UPDATE_DT = dt;
			 
			 let constVal = [3,2,1,1/2,1/3]
			 let SaveSet = new Set();
			 for(let i=0; i< 15; i++) {
				 WeightOptionList[i].WEIGHT_VAL = constVal[WeightOptionList[i].CHK_VAL - 1];
				 SaveSet.add(i);
			 }
			 
			 mainDataService
				 .CommunicationRelay("/los/getWeight.json",{LOS_DT: info.LOS_DT})
				 .success(function(array) {
					 
					 $rootScope.$broadcast("los_popup2",{
						 Info: info,
						 WeightInfo: angular.copy(array),
						 WeightOptionList: WeightOptionList,
						 SaveSet: SaveSet,
						 Triger: weight.Triger,
						 saveType:'수정',
						 isConsistency: false,
						 isSelect: false
					 });
				 });
			
		 },
		 Print: function(){
			let jobType = $state.current.name+ "_" + 3;
			Common_Excel.ExcelDown(jobType, {index:0});
		 },
		 ListClick: function(index) {
			 weight.Init(index);
		 }
	 }
	
	$scope.Serch = Serch;
	$scope.los = los;
	$scope.weight = weight;
	$scope.tep = tep;
}

function diagnosis_popupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	$scope.$on("diagnosis_popup", function(e,data) {
		Triger = data.Triger;
		WeightInfo = data.WeightInfo;
		diagnosis_popup.Info = angular.copy(data.Info);
		diagnosis_popup.Init(false, data.Info.LOS_YEAR);
		$scope.isDiagnosis_popup = true;
	});
	let Triger = null;
	//리스트를 배열로 분배 시키는 함수
	function Distribution(list, size, map, variable = 0){
		let result = new Array(size);
		console.log(result)
		let type = param_popup.PageStrList;
		for(let i = 0; i < size; i++){
			result[i] = []
		}
		
		let param = list;
		for(i in param){
			if(!param[i])continue;
			let index = Number(param[i].LOS_ITEM_CD);
			param[i].LOS_ITEM_NM = type[index].LOS_ITEM_CD;
			param[i].EVALUE_USE_YN = param[i].EVALUE_USE_YN === "1" ? true: false;
			param[i]['set'] = new Set();
			
			if(param[i].EVALUE_CD && param[i].EVALUE_USE_YN) 
				evalueResul.set(param[i].EVALUE_CD, param[i]);
				
			result[index - variable].push(param[i]);
			if(param[i].PARAM_CD != undefined)
				map[param[i].PARAM_CD] = param[i];
		}
		return result;
	}
	
	//eval 함수와 동일 
	function looseJsonParse(obj){
	    return Function('"use strict";return (' + obj + ')')();
	}
	
	function ParamSet(param,  selector) {
		//let param = param_popup.ParamMap[index];
		/*// 셋이 없다면
		if(!param.set)
			//셋을 추가
			param['set'] = new Set();*/
		//화면에서 체크를 v 상태로
		//포인터로 연결되어 있음
		//console.log(param)
		param.EVALUE_USE_YN = true;
		//셋에 선택한 자신을 저장한다
		console.log(param.set)
		param.set.add(selector);
	}
	
	let diagnosis_popupITEM  = [ 
		{'name' : 'LOS_YEAR', 'title' : '평가년도', 'required' : true, 'maxlength' : 30}, 
		{'name' : 'LOS_MEMO', 'title' : '사업장', 'required' : true, 'maxlength' : 10}
	]
	
	
	//Page => 0:인력, 1:시설, 2:운영, 3:서비스, 4:환경, 5:재정, 6:설정 및 목표
	//css에서 작동할 변수들
	let tep = {
		 Page : 0,
		 Click : function(page) {
			 this.Page = page;
			 
			 if(diagnosis_popup.selectorElemental)
				 diagnosis_popup.selectorElemental.Destroy();
		 },
		//추가
		Add : function() {
			let nodes = new Array(7);
			let index = this.Page
			
			if(index === 0) {
				let tempList = diagnosis_popup.ListArray[index];
				for(let i =1; i < nodes.length; i++){
					nodes[i] = tempList[i - 1];
				}
			} else {
				nodes[index] = diagnosis_popup.ListArray[index];
			}
			
			param_popup.Init(index, nodes);
		},
	}
	let WeightInfo = null;
	let evalueResul = null;
	let paramResul = null;
	let diagnosis_popup = {
			Info : {
				LOS_YEAR : '',
				UPDATE_ID : '',
				LOS_MEMO : ''
			},
			selectorElemental: null,
			SelectOption : [{Name : 1, value : 1},{Name : 2, value : 2},{Name : 3, value : 3}],
			//Array => 0:인력, 1:시설, 2:운영, 3:서비스, 4:환경, 5:재정, 6:설정 및 목표
			ListArray :null,
			Import : function() {
				$rootScope.$broadcast('import_popup',{
					"TITLE" : '서비스 수준 분석 리스트',
					"RE_EST_SID" :this.Info.LOS_YEAR,
					"Triger" : this.Init
				});
			},
			Init : function(isImport, LOS_YEAR) {
				diagnosis_popup.Info.LOS_YEAR = LOS_YEAR;
				evalueResul = new Map();
				paramResul = new Map();
				mainDataService.CommunicationRelay("/los/getItemList.json",diagnosis_popup.Info).success(function(map) {
					console.log(map)
					//순서 중요 
					//{0,1-1,1-2,2} => [[{0}],[{1-1},{1-2}]]
					//형식으로 분배 시킴
					param_popup.ParamMap = {};
					let usemap = param_popup.ParamMap;
					//param
					let list = Distribution(map.PARAM, 7, usemap);
					//evalue
					list[0] = Distribution(map.EVALUE,6, usemap, 1);
					//리스트 등록
					diagnosis_popup.ListArray = list;
					//정보 초기화
					let info = diagnosis_popup.Info;
					if(!isImport){
						if(info.INSERT_ID){
							info.UPDATE_ID = map.INSERT_ID;
							info.UPDATE_DT = map.INSERT_DT;
							info.LOS_YEAR = info.LOS_YEAR + "년";
						}else{
							info.INSERT_ID = map.INSERT_ID;
							info.INSERT_DT = map.INSERT_DT;
						}
					}
					
					
					for(v of evalueResul.values()) {
						let selector = v.EVALUE_CD;
						// 구분문자열 제거
						let StrArray = v.FUNCTION_FORMULA.split(/[/|+|\-|(|)|*]/);
						//'pe01', 'pe02' 배열
						for(i in StrArray){
							//인덱스 'pe01'
							let index = StrArray[i];
							// 숫자이면 통과
							if(!isNaN(index))continue;
							//파람을 받는다
							
							let param = param_popup.ParamMap[index]
							if(!param)continue;
							paramResul.set(index, param);
							
							ParamSet(param, selector);
						}
					}
				});
			},
			Analyze : function() {
				if(evalueResul.size == 0 || paramResul.size == 0){
					alertify.alert('',"설정을 하나 이상 하세요.");
					return;
				}
				
				for(v of evalueResul.values()) {
					  if(!v.GOAL_VAL || v.GOAL_VAL ==='' || v.GOAL_VAL ==0) {
						  alertify.alert('',"목표 값을 확인하세요.");
						  return; 
					  }
					  if(v.GOAL_VAL < 0){
						  alertify.alert('',"음수는 입력할 수 없습니다.");
						  return; 
					  }
				}
				
				for(v of paramResul.values()) {
					  if(!v.PARAM_VAL || v.PARAM_VAL == '' || v.PARAM_VAL == 0) {
						  alertify.alert('',"변수 값을 확인하세요.");
						  return;
					  }
					  
					  if(v.GOAL_VAL < 0){
						  alertify.alert('',"음수는 입력할 수 없습니다.");
						  return; 
					  }
				}
					
				this.MinSave(function(sid){
					let reusltArray = Array.from(evalueResul.values());
					let losReuslt = null;
					for(i in reusltArray){
						losReuslt = reusltArray[i];
						losReuslt.RESULT_VAL = diagnosis_popup.StrResult(param_popup.ParamMap, losReuslt.FUNCTION_FORMULA);
						losReuslt.WEIGHT_VAL = WeightInfo[Number(losReuslt.LOS_ITEM_CD) - 1].WEIGHT_VAL,
						//(목표/결과)*신뢰도*가중치=LoS결과
						losReuslt.LOS_RESULT_VAL = (losReuslt.GOAL_VAL / losReuslt.RESULT_VAL) * losReuslt.TRUST_DEGREE * losReuslt.WEIGHT_VAL;
					}
					diagnosis_popup.Info.LOS_EVALUE_SID = sid;
					
					let info = {
							Info : diagnosis_popup.Info,
							ReusltArray: reusltArray
					}
					console.log(info)
					//통신
					mainDataService.CommunicationRelay("/los/Analyze.json", info).success(function(map) {
						if(Common_AlertStr(map)) return;
						//화면 정리
						Triger();
					});
					$scope.isDiagnosis_popup = false;
				});
			},
			Save : function() {
				console.log(diagnosis_popup.ListArray);
				this.MinSave(function(sid){
					let reusltArray = Array.from(evalueResul.values());
					diagnosis_popup.Info.LOS_EVALUE_SID = sid;
					diagnosis_popup.Info.LOS_MEMO = xssFilter(diagnosis_popup.Info.LOS_MEMO);
					let info = {
							Info : diagnosis_popup.Info,
							ReusltArray: reusltArray
					}
					//통신
					mainDataService.CommunicationRelay("/los/Analyze.json", info).success(function(map) {
						if(Common_AlertStr(map)) return;
					});
				});
			},
			MinSave: function(delegate){
				//예외처리
				if(!ItemValidation(this.Info, diagnosis_popupITEM)) return;
				//이발류 리스트 정리
				let evalu = this.ListArray[0];
				let evaluList = [];
				for(i in evalu){
					evaluList.push(...evalu[i])
				}
				//파람 리스트 정리
				let param = this.ListArray;
				let paramList = [];
				for(let i = 1; i < param.length; i++){
					paramList.push(...param[i])
				}
				
				if(!this.Info.LOS_EVALUE_SID){
					this.Info.LOS_EVALUE_SID = 0;
				}
				//인포 정리
				let info = {
					EvaluList : evaluList,
					ParamList : paramList,
					Info : this.Info
				}
				//통신
				mainDataService.CommunicationRelay("/los/save.json",info).success(function(sid) {
					delegate(sid);
				});
			},
			Cancel : function() {
				Triger();
				$scope.isDiagnosis_popup = false;
			},
			StrResult : function(paramTo, FUN) {
				console.log(paramTo)
				console.log(FUN)
			    let fromStr = Array.from(FUN);
	            let count = 0;
	            let array = [];
	            //수식을 배열형식으로 저장
			    for (let i = 0; i < fromStr.length; i++) {
			    	// /*-+()는 바로 배열에 저장
			    	if(fromStr[i] === '+' || fromStr[i] === '-' || fromStr[i] === '/' || fromStr[i] === '*' || fromStr[i] === '(' || fromStr[i] === ')') {
			    		array.push(fromStr[i]);	
			    	}
			    	// /*-+()가 아닌 문자열 이라면
	                else {
	                	let str = '';
	                	count = i;
	                	//문자열 '1','0','0'=> '100' 문자열로 합쳐서 저장
	                	for(let j = i; j < fromStr.length; j++) {
	                		  if(fromStr[j] === '+' || fromStr[j] === '-' || fromStr[j] === '/' || fromStr[j] === '*' || fromStr[j] === '(' || fromStr[j] === ')') {
	                		      count = j;
	                		  	  break;
	                		  }
	                		  count++;
	                		  //문자열로 합침
							  str += fromStr[j];
	                	}
	                    //count의 시점이 j의 종료 시점이 여서 i입장에서 +1시점이다 그래서 -1로 시점을 조종
	                	i = count - 1;
	                    //문자 변수를 숫자로 변경 
	                	array.push(paramTo[str] ? paramTo[str].PARAM_VAL : str);
	                }
			    }
			    console.log(array.join(''))
			   //eval 호출로 수식 계산
			   return looseJsonParse(array.join(''));
			},
			ParamShow : function(power) {
				//'P01' 선택한 자신
				let selector = power.EVALUE_CD;
				// 구분문자열 제거
				let StrArray = power.FUNCTION_FORMULA.split(/[\-|/|+|(|)|*]/);
				console.log(StrArray)
				//체크가 v 인지 x 인지 확인
				if(power.EVALUE_USE_YN){
					//결과 판별용 맵
					evalueResul.set(selector, power);
					//'pe01', 'pe02' 배열
					for(i in StrArray){
						//인덱스 'pe01'
						let index = StrArray[i];
						// 숫자이면 통과
						if(!isNaN(index))continue;
						//파람을 받는다
						let param = param_popup.ParamMap[index]
						console.log(index)
						console.log(param_popup.ParamMap)
						paramResul.set(index, param);
						ParamSet(param, selector);
					}
				}else{
					//결과 판별용 맵
					evalueResul.delete(selector);
					//'pe01', 'pe02' 배열
					for(i in StrArray){
						//인덱스 'pe01'
						let index = StrArray[i];
						// 숫자이면 통과
						if(!isNaN(index))continue;
						//셋을 받는다
						let paramSet = param_popup.ParamMap[index].set;
						//셋에 저장된 자기자신을 지운다
						paramSet.delete(selector);
						//셋에 더이상 참조가 없으면
						if(paramSet.size === 0)
							//체크를 x로 변경한다
							//포인터로 연결되어 있음
							param_popup.ParamMap[index].EVALUE_USE_YN = false;
							//결과 판별용 맵
							paramResul.delete(index);
					}
				}
			},
			Revision : function(bool) {//파람 수정
				if(!this.selectorElemental){// 설정 및 목표
					alertify.alert('','평가 항목을 선택하세요.');
					return;
				}
				
				if(bool){
					this.power.EVALUE_USE_YN = false;
					
					this.ParamShow(this.power); 
				}
				
				//수정 한다
				console.log(this.selectorElemental);
				
				let nodes = new Array(7);
				let index = tep.Page;
				
				if(index === 0) {//트리 만들기
					let tempList = diagnosis_popup.ListArray[index];
					for(let i =1; i < nodes.length; i++){
						nodes[i] = tempList[i - 1];
					}
				} else {
					nodes[index] = diagnosis_popup.ListArray[index];
				}
				
				param_popup.Init(index, nodes, false, this.selectorElemental);
				
			},
			ParamClick: function(parant, index, power){
				//id 
				const ID = "#param_body_" + parant;
				let list = $(ID).find("tr");
				for(let j = 0; j < list.length; j++){
					//색을 날린다
					list.eq(j).css('backgroundColor', '');
				}
				
				console.log($(ID).find('tr').eq(index).css('backgroundColor', '#e6eff6'));
				
				let info = {
						index: index + 1,
						Page: parant,
						LOS_ITEM_CD: power.LOS_ITEM_CD,
						PARAM_CD: power.PARAM_CD,
						PARAM_UNIT: power.PARAM_UNIT,
						C_MEMO: power.C_MEMO,
						TRUST_DEGREE: power.TRUST_DEGREE,
						FUNCTION_FORMULA: power.FUNCTION_FORMULA,
						PARAM_NM: power.PARAM_NM,
						Destroy: function(){
							$(ID).find('tr').eq(index).css('backgroundColor', '');
							diagnosis_popup.selectorElemental = null;
						}
					};
				
				this.power = power;
				
				this.selectorElemental = info;
				
			},
			Click: function(parant, index, power) {// 색 변경
				//id 
				const ID = "#service_body_";
				
				//전체 리스트를 조회 한다.
				let list = null;
				for(let i = 0; i < 6; i++){
					//#id 로 리스트를 받는다
					let list = $(ID + i).find('tr');
					for(let j = 1; j < list.length; j++){
						//색을 날린다
						list.eq(j).css('backgroundColor', '');
					}
				}
				
				//색을 입힌다
				console.log($(ID + parant).find('tr').eq(index).css('backgroundColor', '#e6eff6'));
				//사용할 단서를 기억한다.
				let info = {
						index: index,
						Page: parant + 1,
						LOS_ITEM_CD: power.LOS_ITEM_CD,
						PARAM_CD: power.EVALUE_CD,
						PARAM_UNIT: power.EVALUE_UNIT,
						C_MEMO: power.C_MEMO,
						TRUST_DEGREE: power.TRUST_DEGREE,
						FUNCTION_FORMULA: power.FUNCTION_FORMULA,
						PARAM_NM: power.EVALUE_NM,
						Destroy: function(){
							$(ID + parant).find('tr').eq(index).css('backgroundColor', '');
							diagnosis_popup.selectorElemental = null;
						}
					};
				this.power = power;
				this.selectorElemental = info;
			}
	}
	
	let param_popupItem = [ 
		{'name' : 'PARAM_CD', 'title' : '항목', 'required' : true }, 
		{'name' : 'PARAM_UNIT', 'title' : '단위', 'required' : true, 'maxlength' : 50}, 
		{'name' : 'PARAM_NM', 'title' : '평가명', 'required' : true, 'maxlength' : 50},
		//{'name' : 'FUNCTION_FORMULA', 'title' : '연산식', 'required' : true, 'maxlength' : 50},
	]
	
	let param_popup = {
			Info : {
				Page :'2',
				LOS_ITEM_CD :  '',
				PARAM_CD : '',
				PARAM_UNIT : '',
				C_MEMO : '',
				TRUST_DEGREE : '',
				PARAM_NM : ''
			},
			PageStrList : [
				{LOS_ITEM_CD_SID:0, LOS_ITEM_CD : "설정하기", node : null, LOS_ITEM_FIRST:""},
				{LOS_ITEM_CD_SID:1, LOS_ITEM_CD : "인력", node : null, LOS_ITEM_FIRST:"PE"},
				{LOS_ITEM_CD_SID:2, LOS_ITEM_CD : "시설", node : null, LOS_ITEM_FIRST:"FA"},
				{LOS_ITEM_CD_SID:3, LOS_ITEM_CD : "운영", node : null, LOS_ITEM_FIRST:"OP"},
				{LOS_ITEM_CD_SID:4, LOS_ITEM_CD : "서비스", node : null, LOS_ITEM_FIRST:"SE"},
				{LOS_ITEM_CD_SID:5, LOS_ITEM_CD : "환경", node : null, LOS_ITEM_FIRST:"EV"},
				{LOS_ITEM_CD_SID:6, LOS_ITEM_CD : "재정", node : null, LOS_ITEM_FIRST:"AS"}
			],
			ParamMap: null,
			isShow : false,
			//초기화
			Init : function(page, nodes, isnew = true, param = null) {
				
						
				//해당 정보를 가저오는 수식
				for(i in this.PageStrList) {
					this.PageStrList[i].node = nodes[i];
				}
				//변수 초기화
				this.Info = {Page : page};
				this.isNew = !isnew;
				if(isnew) {
					param_popup.TITLE = (tep.Page == 0) ? "설정 추가" : "변수 추가" 
					//코드 생성
					this.UpdateCode(page);
					function_popup.Init();
					this.isShow = true;
				} else {
					param_popup.TITLE = (tep.Page == 0) ? "설정 수정": "변수 수정"
					//정보 전달	
					param_popup.Info = angular.copy(param);
					function_popup.Init();
					this.isShow = true;
				}
			},
			//항목을 선택시 마지막 인덱스에 있는 정수에 + 1 을하여 코드에 출력
			UpdateCode : function(page) {
				let list = this.PageStrList[page].node;
				this.Info.PARAM_CD = '';
				if(list) {
					//인덱스 총 사이즈를 찾는다
					let lastIndex = list.length - 1;
					console.log(lastIndex)
					let param_cd= '';
					if(lastIndex === -1){
						param_cd = param_popup.PageStrList[page].LOS_ITEM_FIRST + '01';
					}else{
						//마지막 인덱스로 이름을 받아 온다
						param_cd = list[lastIndex].PARAM_CD ? list[lastIndex].PARAM_CD : list[lastIndex].EVALUE_CD;
						//숫자를 자른다
					}
					
					let str = '';
					let tempStr=''
					for(let i = 0; i <param_cd.length; i++){
						tempStr = param_cd[i]
						if(isNaN(tempStr))
							str += tempStr;
					}
					//마지막 인덱스로 최종 숫자를 구하고 
					let number = (Number(lastIndex) + 2);
					//숫자가 1자리 이면 숫자 앞에 0을 추가로 입력 해준다
					if(number.toString().length === 1){
						number = '0' + number;
					}
					//정보를 저장한다
					this.Info.PARAM_CD = str + number;
				}
			},
			Add : function() {
				function_popup.Info.PARAM_CD = this.Info.PARAM_CD;
				function_popup.Info.PARAM_NM = this.Info.PARAM_NM;
				function_popup.Info.FUNCTION_FORMULA = this.Info.FUNCTION_FORMULA;
				
				if(this.Info.FUNCTION_FORMULA)
					function_popup.FormulaList = function_popup.SetFormulaList(this.Info.FUNCTION_FORMULA);
				
				//트리에 적용할 어레이 추가
				let index = this.Info.Page ===0 ? 0 :  this.Info.Page - 1;
				//트리 호출
				tree.Init(index, diagnosis_popup.ListArray);
				//함수 팝업 호출
				function_popup.isShow = true;
			},
			Reliability: function() {
				$scope.isReliability = true;
			},
			Save : function() {
				//this.Info.Page = 항목 option LOS_ITEM_CD_SID
				//노드는 해당 리스트를 포인터로 받음
				let list = this.PageStrList[this.Info.Page].node
				
				if(!ItemValidation(this.Info, param_popupItem)) return;
				
				//정보 복사
				let tempInfo = this.Info
				
				//스위치
				switch(tep.Page){
				case 0: 
					let info = {
						//설정 및 변수 였으면
						//이름을 다시 정의
						C_MEMO: tempInfo.C_MEMO,
						LOS_ITEM_CD : tempInfo.LOS_ITEM_CD,
						EVALUE_CD: tempInfo.PARAM_CD,
						EVALUE_NM: tempInfo.PARAM_NM,
						EVALUE_UNIT: tempInfo.PARAM_UNIT,
						GOAL_VAL: tempInfo.PARAM_VAL,
						FUNCTION_FORMULA: tempInfo.FUNCTION_FORMULA,
						FUNCTION_YN: "Y",
						LOS_ITEM_CD: "0" + tempInfo.Page,
						TRUST_DEGREE: 1
					}
					//정보 복사
					tempInfo = info;
					break;
					//변수 일 경우 
					//파람 변수 테이블에 정보를 입력 포인터 전달
				default: 
					tempInfo.LOS_ITEM_CD = "0" + tempInfo.Page;
					tempInfo.FUNCTION_YN = "N";
					param_popup.ParamMap[tempInfo.PARAM_CD] = tempInfo; 
				break;
				}
				
				tempInfo['set'] = new Set();
				
				if(this.Info.index) {
					let findIndex={P:0, F:1, O:2, S:3, E:4, A:5, }
					if(tempInfo.FUNCTION_YN == 'N'){
						let values = list[this.Info.index - 1].set.values();
						for( key of values){
							let  power = diagnosis_popup.ListArray[0][findIndex[key.slice(0,1)]][Number(key.slice(1)) -1];
							power.EVALUE_USE_YN = false;
							diagnosis_popup.ParamShow(power); 
						}
					}
					list[this.Info.index - 1] = tempInfo;
					
				}else{
					//화면에 출력
					list.push(tempInfo);
				}
				
				alertify.success('성공');
				this.isShow = false;
				diagnosis_popup.selectorElemental.Destroy();
			},
			Delete: function(){
				let info = param_popup.Info;
				alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?',function () {
					let key = info.PARAM_CD.substr(0,1);
					let index = Number(info.PARAM_CD.substr(1, info.PARAM_CD.length-1)) - 1;
					let list = diagnosis_popup.ListArray[0][Number(info.LOS_ITEM_CD) - 1];
					list.splice(index, 1);
					let num = 0;
					for(let i = index; i <list.length; i++){
						num = i + 1;
						list[i].EVALUE_CD = (num.toString().length == 1) ? 
							key + "0" + num : key + num;
					}

					param_popup.Cancel();
					diagnosis_popup.selectorElemental.Destroy();
					$scope.$apply();
					
				},function () {}).set('basic', false);
			},
			Cancel : function() {
				this.isShow = false;
			}
	}
	
	let tree = {
		Root : [
			{name : '인력', node : null},
			{name : '시설', node : null},
			{name : '운영', node : null},
			{name : '서비스', node : null},
			{name : '환경', node : null},
			{name : '재정', node : null},
		],
		List : null,
		//초기화 
		Init : function(index, list) {
			console.log(list)
			this.List = list;
			let root = this.Root;
			for(i in root) {
				root[i].node = null;
			}
			//선택된 탭의 노드의 위치를 찾아서 그위치로 스크롤 이동
			$('#treeScroll').animate({scrollTop : 29 * index + 24 }, 100);
			root[index].node = list[index + 1];
		},
		//트리를 접거나 피는 역할
		Click : function(index) {
			this.Root[index].node = this.Root[index].node  ?  null : this.List[index + 1];
		},
		//노드에 데이터를 받는 곳
		NodeClick : function(str) {
			 function_popup.Click(str);
		}
	}
	//함수 만들기 팝업
	let function_popup = {
			Info : {
				PARAM_CD : '',
				PARAM_NM : '',
				FUNCTION_FORMULA : '',
			},
			isShow : false,
			FormulaList : [],
			Init : function() {
				this.Info = {};
				this.FormulaList = [];
			},
			//연산식을 만들어 준다
			SetFormulaList: function(str) {
				console.log(str);
				//변수만 담는다
				let StrResult = str.split(/[/|+|\-|(|)|*]/);
				StrResult = StrResult.filter((element, i) => element !== "");
				//비교할 문자를 담아 둔다
				let Separator = ["+", "-", "*", "/", "(", ")"];
				//문자를 배열단위로 받는다 "ㅁㄴ" = ["ㅁ", "ㄴ"]
				const strArr = str.split('');
				//변수
				let arrayCount = 0;
				let boolTest = true;
				let result = [];
				//실행
				for(let i = 0; i < strArr.length; i++) {
					boolTest = true;
					for(j in Separator) {
						//비교해서  Separator를 담는다.
						if(strArr[i] == Separator[j]) {
							boolTest = false;
							result.push(Separator[j]);
							break;
						}
					}
					//문자가 아니면
					if(!boolTest) continue;
					
					//문자 이면 
					//다음 문자를 검사 한다 
					for(j in Separator) {
						if(strArr[i+1] == Separator[j]) {
							boolTest = false;
							break;
						}
					}
					//다음 문자가 문자이면
					if(boolTest){
						//담고
						result.push(StrResult[arrayCount]);
						arrayCount++;
					}
					//아니면 돌린다
					else continue;
					
					//다음 문자가 문자이면
					//문자의 끝으로 좌표를 이동한다
					for(i; i < strArr.length; i++) {
						for(j in Separator) {
							if(strArr[i] == Separator[j]) {
								boolTest = false;
								i--;
								break;
							}
						}
						//문자 배열끝으로 이동 했으면 포문을 나간다
						if(!boolTest) break;
					}
					//다음
				}//종료
				
				return result;
			},
			//함수를 만드는 연산자 클릭
			Click : function(str) {
				let list = this.FormulaList;
				//최근 정보 입력
				list.push(str);
				this.Info.FUNCTION_FORMULA = list.join('');
			},
			//함수를 만드는 함수를 순차적으로 삭제
			Delete : function() {
				let list = this.FormulaList;
				//최근 정보 삭제
				list.pop();
				this.Info.FUNCTION_FORMULA = list.join('');
			},
			//정보(변수 + 함수) 해서 올림
			Save : function() {
				try {
					//수식 검사 
					let list = this.FormulaList;
					let Separator = ["+", "-", "*", "/", "(", ")"];
					let testFormula = '';
					//0 숫자, 1 연산식 2 변수
					let boolTest = [false, false, false];
					//연속 변수를 확인 하는 용도
					let boolTemp = false;
					for(i in list) {
						//수식체크
						boolTemp = boolTest[2];
						//전에 변수를 넣고 다음에 숫자를 넣으면 수식이 잘못됨
						if(boolTest[2] && !isNaN(list[i])) {
							alertify.alert('','수식이 잘못되었습니다');
							return;
						}
						
						//불을 초기화
						for(index in boolTest) {
							boolTest[index] = false;
						}
						
						//숫자
						if(!isNaN(list[i])) {
							testFormula += list[i];
							boolTest[0]= true;
							continue;
						}
						
						//+-()
						for(j in Separator) {
							if(list[i] == Separator[j]) {
								boolTest[1]= true;
								testFormula += Separator[j];
								break;
							}
						}
						//변수가 연속으로 들어오는 경우
						if(boolTemp && !boolTest[1]) {
							alertify.alert('','수식이 잘못되었습니다');
							return;
						}
						
						//변수
						if(!boolTest[1]) {
							boolTest[2]= true;
							testFormula += Number(i+1);
						}
					}
					
					looseJsonParse(testFormula);
				}catch(err) {
					alertify.alert('','수식이 잘못되었습니다');
					return;
				}
				//정보 + 정보
				param_popup.Info = Object.assign(param_popup.Info, function_popup.Info);
				this.isShow = false;
			},
			Cancel : function() {
				this.isShow = false;
			}
	}
	// 취소 공통
	let popup_Scope = {
		Cancel: function(id) {
			$scope[id]= false;
		}
	}
	
	$scope.popup_Scope = popup_Scope;
	$scope.diagnosis_popup = diagnosis_popup;
	$scope.param_popup = param_popup;
	$scope.function_popup = function_popup;
	$scope.tep = tep;
	$scope.tree = tree;
}

function los_popup2Controller($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	$scope.$on("los_popup2", function(e, data) {
		console.log(data)
		console.log($scope.saveType)
		$scope.saveType = data.saveType;
		Los_popup2.WeightOptionList = data.WeightOptionList;
		Los_popup2.Info = data.Info;
		Los_popup2.WeightInfo= data.WeightInfo;
		Los_popup2.SaveSet = data.SaveSet
		Los_popup2.isConsistency = data.isConsistency;
		Los_popup2.isSelect = data.isSelect;
		Triger = data.Triger;
		$scope.isLos_popup2 = true;
	});
	
	const constVal = [3,2,1,1/2,1/3]
	
	let Triger = null;
	
	let Los_popup2={
			 Info: {
				 LOS_DT: '',
				 CHOICE_YN: 'N',
				 INSERT_ID: '',
				 INSERT_DT: '',
				 UPDATE_ID: '',
				 UPDATE_DT: ''
			 },
			 WeightInfo: null,
			 WeightOptionList: null,
			 SelectOption: [{Value: 'Y', Name: '가중치 적용'},{Value: 'N', Name: '가중치 비적용'}],
			 SaveSet : null,
			 Save: function(){
				 if(!this.WeightInfo[0]['WEIGHT_VAL']){
					 alertify.alert('','분석을 실행 해주세요.');
					 return;
				 }
				 
				 if(Los_popup2.Info.WEIGHT_NM == '') {
					 alertify.alert('','기준명은 필수 입니다.');
					 return;
				 }
				 
				 let info = {
						 Info: this.Info,
						 WeightInfo: this.WeightInfo,
						 WeightOptionList: this.WeightOptionList
				 }
				 mainDataService.CommunicationRelay("/los/insertWeight.json", info).success(function(map) {
					 if(Common_AlertStr(map)) return;
					 
					 Triger();
					 
					 $scope.isLos_popup2 = false;
				 });
			 },
			 Analysis: function(){
				 if(this.WeightInfo[0]['WEIGHT_VAL'])return;
				 let list = this.WeightInfo;
				 for(i in list){
					 list[i]={WEIGHT_VAL: list[i], LOS_ITEM_CD: '0'+ (Number(i)+1)}
				}
				this.WeightInfo = list
			 },
			 Cancel : function() {
				 $scope.isLos_popup2 = false;
			 },
			 Change : function(list, index, index2) {
					for(i in list.WEIGHT_YN) {
						list.WEIGHT_YN[i] = false;
					}
					list.WEIGHT_YN[index] = true;
					list.CHK_VAL = index + 1;
					list.WEIGHT_VAL = constVal[index];
					
					this.SaveSet.add(index2);
					console.log(index2)
					//선택창15개를 전부 선택 했다면 변경될 때마다 가중치 분석을 한다
					let len = this.SaveSet.size;
					if(len === 15){
						let info = this.WeightAnalysis();
						this.isConsistency = info.consistency;
						this.WeightInfo = info.weightInfo;
						this.isSelect = false;
						console.log(info)
					}
				},
			//가중치 분석
			WeightAnalysis: function(p){
				// Matrix 생성 
				let mat = this.CreateMatrix();
				// Matrix * Matrix
				let array = this.Eigen_Vector(this.MMULT(mat));
				// Matrix * 배열 
				let array2 = this.MVULT(mat, array);
				// 결과
				return {weightInfo : array, consistency : this.Consistency(array, array2)}
			},
			//유효성
			Consistency : function(a, b) { 
				//수식 기획서 참조
				let sum = 0;
				for(i in a) {
					sum += b[i] / a[i];
				}
				
				let len = a.length;
				let avg = (sum / len) - len;
				let ci = avg / (len -1);
				let ri = 1.25;
				let reuslt = ci / ri;
				
				return reuslt > 0.1
			},
			//수식 기획 참조 
			Eigen_Vector : function(mat) {
				let resultSum = 0;
				let sums = [];
				for(i in mat) {
					let sum = 0;
					for(j in mat[i]) {
						sum += mat[i][j];
					}
					sums.push(sum);
					resultSum += sum; 
				}
				let result = []
				for(i in sums) {
					result.push((sums[i]/resultSum).toFixed(4));
				}
				return result
			},
			//수식 기획 참조 
			MMULT : function(Matrix) {
				let resultMat = []
				for(i in Matrix) {
					let newMat = []
					for(j in Matrix[i]) {
						let val = 0;
						for(k in Matrix[i]) {
							val += Matrix[i][k] * Matrix[k][j];
						}
						newMat.push(val);
					}
					resultMat.push(newMat);
				}
				return resultMat;
			},
			//수식 기획 참조 
			MVULT : function(mat, array) {
				let result = [];
				for(i in mat) {
					let sum = 0;
					for(j in array) {
						sum += mat[i][j] * array[j];
					}
					result.push(sum.toFixed(4));
				}
				return result;
			},
			//메트릭스 구조
			CreateMatrix : function(list = this.WeightOptionList) {
				return [
					[1,                    list[0].WEIGHT_VAL,   list[1].WEIGHT_VAL,    list[2].WEIGHT_VAL,     list[3].WEIGHT_VAL,    list[4].WEIGHT_VAL ],
					[1/list[0].WEIGHT_VAL, 1,                    list[5].WEIGHT_VAL,    list[6].WEIGHT_VAL,     list[7].WEIGHT_VAL,    list[8].WEIGHT_VAL ],
					[1/list[1].WEIGHT_VAL, 1/list[5].WEIGHT_VAL, 1,                     list[9].WEIGHT_VAL,     list[10].WEIGHT_VAL,   list[11].WEIGHT_VAL],
					[1/list[2].WEIGHT_VAL, 1/list[6].WEIGHT_VAL, 1/list[9].WEIGHT_VAL,  1,                      list[12].WEIGHT_VAL,   list[13].WEIGHT_VAL],
					[1/list[3].WEIGHT_VAL, 1/list[7].WEIGHT_VAL, 1/list[10].WEIGHT_VAL, 1/list[12].WEIGHT_VAL,  1,                     list[14].WEIGHT_VAL],
					[1/list[4].WEIGHT_VAL, 1/list[8].WEIGHT_VAL, 1/list[11].WEIGHT_VAL, 1/list[13].WEIGHT_VAL,  1/list[14].WEIGHT_VAL, 1                  ]
				]
			}
	}
	
	$scope.Los_popup2 = Los_popup2;
}

function import_popupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, $filter) {
	$scope.$on("import_popup", function(e,data) {
		//초기화
		$scope.TITLE = data.TITLE;
		basic_popup.Info.EST_NM = '';
		Triger = data.Triger;
		if(!basic_popup.isInit){
			pagerJsonGrid(Model);
			basic_popup.isInit = true;
		}
	    $("#list4").trigger("reloadGrid");
		$scope.isImport_popup = true;
	});
	let Model = {
            grid_id : 'list4',
            pager_id : 'listPager4',
            url : '/los/getList.json',
            condition : {
                page : 1,
                LOS_YEAR: '',
                BEFORE: '',
                AFTER:'',
                OPTION: 1,
                rows : GridConfig.sizeT
            },
            rowNum : GridConfig.sizeT,
            colNames : [ '순번','평가 기준 년도','작성자','작성 일자','진행 사항','평가 번호','수정자','수정일','사업소','가중치sid','sid'],
            colModel : [
            	{name : 'RNUM', width : 10, sortable:false, resizable: false}, 
 	            {name : 'LOS_YEAR', width : 40, resizable: false}, 
 	            {name : 'INSERT_ID', width : 40, resizable: false},
	            {name : 'INSERT_DT', width : 40, resizable: false},
	            {name : 'COMPLETE_YN', width : 40, resizable: false},
	            {name : 'LOS_ITEM_CD', width : 0, hidden : true},
 	            {name : 'UPDATE_ID', width : 40, hidden : true},
 	            {name : 'UPDATE_DT', width : 40, hidden : true},
 	            {name : 'LOS_MEMO', width : 0, hidden : true},
 	            {name : 'LOS_WEIGHT_DT', width : 0, hidden : true},
 	            {name : 'LOS_EVALUE_SID', width : 0, hidden : true}
            ],
            onSelectRow : function(id) {
            	Common_Swap.B_A($('#list4').jqGrid('getRowData', id), basic_popup.Info);
            	basic_popup.Info.EST_NM = '사업소 : ' + basic_popup.Info.LOS_MEMO + ', ' + '평가년 : ' + basic_popup.Info.LOS_YEAR;
            	$scope.$apply();
            }
        };
	let Triger = null;
	let basic_popup = {
		Info : {
			EST_NM : '',
			LOS_MEMO:'',
			LOS_YEAR: '',
			LOS_EVALUE_SID:'',
		},
		isInit: false,
		Cancel : function() {
			$scope.isImport_popup = false;
		},
		Select : function() {
			if(this.Info.EST_NM == '')return;
			Triger(true, this.Info.LOS_YEAR);
			this.Cancel();
		}
	}
	
	$scope.basic_popup = basic_popup;
}
