angular.module('app.diagnosis').controller('newLosController', newLosController);

//angular.module('app.diagnosis').controller('state2DiagController', state2DiagController);//직접상태평가

function newLosController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	 $scope.$on('$viewContentLoaded', function () {
	    	$("#tab1_btn_M").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn_M").removeClass("active");
	    		$("#tab1_M").show();
	    		$("#tab2_M").hide();
	    	});
	    	$("#tab2_btn_M").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab1_btn_M").removeClass("active");
	    		$("#tab2_M").show();
	    		$("#tab1_M").hide();
	    	}); 
	 });
	
	 $scope.$on('$viewContentLoaded', function () {
	    	$("#tab1_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab3_btn, #tab4_btn, #tab5_btn, #tab6_btn").removeClass("active");
	    		$("#tab1").show();
	    		$("#tab2, #tab3, #tab4, #tab5, #tab6").hide();
	    	});
	    	$("#tab2_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab1_btn, #tab3_btn, #tab4_btn, #tab5_btn, #tab6_btn").removeClass("active");
	    		$("#tab2").show();
	    		$("#tab1, #tab3, #tab4, #tab5, #tab6").hide();
	    	});
	    	$("#tab3_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab1_btn, #tab4_btn, #tab5_btn, #tab6_btn").removeClass("active");
	    		$("#tab3").show();
	    		$("#tab1, #tab2, #tab4, #tab5, #tab6").hide();
	    	});
	    	$("#tab4_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab1_btn, #tab3_btn, #tab5_btn, #tab6_btn").removeClass("active");
	    		$("#tab4").show();
	    		$("#tab1, #tab2, #tab3, #tab5, #tab6").hide();
	    	});
	    	$("#tab5_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab1_btn, #tab4_btn, #tab3_btn, #tab6_btn").removeClass("active");
	    		$("#tab5").show();
	    		$("#tab1, #tab2, #tab4, #tab3, #tab6").hide();
	    	});
	    	$("#tab6_btn").on("click", function() {
	    		$(this).addClass("active");
	    		$("#tab2_btn, #tab1_btn, #tab4_btn, #tab5_btn, #tab3_btn").removeClass("active");
	    		$("#tab6").show();
	    		$("#tab1, #tab2, #tab4, #tab5, #tab3").hide();
	    	});
	    	
	    	setGrid();
	    });
	 
	 function setGrid() {
	        return pagerJsonGrid({
	            grid_id : 'list',
	            pager_id : 'listPager',
	            url : '/asset/getSelectAssetItemList.json',
	            condition : {
	                page : 1,
	                rows : GridConfig.sizeL
	            },
	            rowNum : 20,
	            colNames : [ '순번','평가 기준 년도','평가 항목','작성자','평가일자'],
	            colModel : [
	            {
	                name : 'RNUM',
	                width : "40px"
	            }, {
	                name : 'ASSET_PATH_SID',
	                width : 0,
	            }, {
	                name : 'ASSET_SID',
	                width : 0,
	            }, {
	                name : 'ASSET_CD',
	                width : 0,
	            }, {
	                name : 'ASSET_NM',
	                width : 0,
	            }],

	            onSelectRow : function(id) {
	            	
	                $scope.AssetItemInfo = $('#list').jqGrid('getRowData', id);
	                console.log($scope.AssetItemInfo);
	            }
	        });
	    }
	
}

