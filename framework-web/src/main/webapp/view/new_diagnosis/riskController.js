angular.module('app.diagnosis').controller('riskController', riskController);

function riskController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	 $scope.$on('$viewContentLoaded', function () {
	 	$("#tab1_btn").on("click", function() {
	   		$(this).addClass("active");
	   		$("#tab2_btn, #tab3_btn").removeClass("active");
	   		$("#tab1").show();
	   		$("#tab2, #tab3").hide();
	   	});
	   	$("#tab2_btn").on("click", function() {
	   		$(this).addClass("active");
	   		$("#tab1_btn, #tab3_btn").removeClass("active");
	   		$("#tab2").show();
	   		$("#tab1, #tab3").hide();
	   	});
	   	$("#tab3_btn").on("click", function() {
	   		$(this).addClass("active");
	   		$("#tab2_btn, #tab1_btn").removeClass("active");
	   		$("#tab3").show();
	   		$("#tab1, #tab2").hide();
	   	});
	   	$("#tab1_1_btn").on("click", function() {
	   		$(this).addClass("active");
	   		$("#tab1_2_btn, #tab1_3_btn").removeClass("active");
	   		$("#tab1_1").show();
	   		$("#tab1_2, #tab1_3").hide();
	   	});
	   	$("#tab1_2_btn").on("click", function() {
	   		$(this).addClass("active");
	   		$("#tab1_1_btn, #tab1_3_btn").removeClass("active");
	   		$("#tab1_2").show();
	   		$("#tab1_1, #tab1_3").hide();
	   	});
	   	$("#tab1_3_btn").on("click", function() {
	   		$(this).addClass("active");
	   		$("#tab1_2_btn, #tab1_1_btn").removeClass("active");
	   		$("#tab1_3").show();
	   		$("#tab1_1, #tab1_2").hide();
	   	});

	   	$scope.setWeek();
		setGrid();
		setGrid2();
		setGrid2_Popup();
		setGrid3_Popup();
		
		 setDatePicker();
		 //setLastWeek();
		 $rootScope.setBtnAuth($scope.MCODE);
		 
		getFactorRiskList(function(data){
    		$scope.factorSelect($scope.factorList[0]);
	    });
	    getGradeRiskList(function(data){
	    	$scope.gradeSelect($scope.gradeList[0]);
	    });
	    
	 	/*mainDataService.getCommonCodeList({
	        gcode : 'YEAR',
	        gname : ''
	    }).success(function(data) {
	        if (!common.isEmpty(data.errMessage)) {
	            alertify.error('errMessage : ' + data.errMessage);
	        } else {
	        	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
	            $scope.ComCodeYearList = data;
	        }
	    });*/
	 	
	 	mainDataService.getAssetLevelList({}).success(function(data){
            $scope.assetLevelList  = data
            mainDataService.getBlockCodeList({}).success(function (obj) {
               if (!common.isEmpty(obj.errorMessage)) {
                  alertify.error('errMessage : ' + obj.errorMessage);
               } else {
                  $scope.blockList = obj;
               }
            });
         });
	 	
	 	 Common_Modal.Init($compile, $scope, {
			id: '#list2',
			layerId: '#modal',
			hidenSize: 0
	 	});
	 });
	 
	 $scope.setWeek = function setLastWeek() {
	        var sday = new Date(); // 6개월
	        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

	        var today = new Date(); // 오늘

	        var sYear = sday.getFullYear();
	        var eYear = today.getFullYear();
	        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
	        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

	        $scope.searchDate.S_DATE = s_yyyymmdd;
	        $('#searchDate.S_DATE').val(s_yyyymmdd);
	        $scope.searchDate.E_DATE = e_yyyymmdd;
	        $('#searchDate.E_DATE').val(e_yyyymmdd);
	 }
	 
 	 $scope.MonthList = ["01","02","03","04","05","06","07","08","09","10","11","12"];
	 $scope.SelectedRiskInfo = {};
	 $scope.EditInfo = {};
	 
	 $scope.factorList = [];
	 $scope.gradeList = [];
	 
	 $scope.EditFactorInfo ={};
	 
	 $scope.signStore = ["","<","=",">",">=","<="];
	 
	$scope.MCODE = '0304';
	$scope.storeYear = getYears(YearSet.baseYear, YearSet.plus);
	$scope.selectedCurrYear = $scope.selectedYear = '' + (new Date()).getFullYear();
	$scope.searchDateType = '1'; // 날짜검색 : 연도(1) or 기간선택(2)
	$scope.searchDate = {}; // 기간선택
	$scope.searchCategory = '';
	$scope.risk_current_sid = '';

	$scope.loadAssetList = function(risk_sid){
		$("#list2").setGridParam({
			datatype : 'json',
			page : $scope.currentPageNo,
			rows : GridConfig.sizeS,
			postData : {
                CLASS3_CD: '',
   			 	CLASS4_CD: '',				
				RISK_SID : risk_sid
			}
		}).trigger('reloadGrid', {
			current : true
		});		
	}
	
	$scope.onKeyPress = function(event){
        if (event.key === 'Enter') {
        	$scope.search();
        }
    }
	
	$scope.loadRiskList = function(){
		$("#list_risk").setGridParam({
			datatype : 'json',
			page : $scope.currentPageNo,
			postData : {

			}
		}).trigger('reloadGrid', {
			current : true
		});		
	}
	function setLastWeek() {
		var sday = new Date(); // 일주일전
		sday.setDate(sday.getDate() - 7); // 일주일전으로 set

		var today = new Date(); // 오늘

		var sYear = sday.getFullYear();
		var eYear = today.getFullYear();
		var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
		var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

		$scope.searchDate.S_DATE = s_yyyymmdd;
		$('#searchDate_S_DATE').val(s_yyyymmdd);
		$scope.searchDate.E_DATE = e_yyyymmdd;
		$('#searchDate_E_DATE').val(e_yyyymmdd);
	}

	$scope.search = function () {
		var data = {};

		data.page = 1;
		data.rows = GridConfig.sizeL;
		data.searchText = $scope.searchText;
		data.type = $scope.searchDateType || '';

		// 연도 검색
		if ($scope.searchDateType == "1") {
			data.year = $scope.selectedCurrYear || '';

			// 기간선택 검색
		} else {
			if(($scope.searchDate.S_DATE || '')=='' || ($scope.searchDate.E_DATE || '')==''){
				alertify.alert('오류','조회기간을 입력하세요.',function(){
					$("#searchDate_S_DATE").focus();
				});
				
				return;
			}
			data.s_date = $scope.searchDate.S_DATE || '';
			data.e_date = $scope.searchDate.E_DATE || '';

			// 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
			if (!checkDate($scope.searchDate.S_DATE || '', $scope.searchDate.E_DATE || '', '')) {
				return;
			}
		}

		$('#list_risk').jqGrid('setGridParam', {
			postData: data
		}).trigger('reloadGrid', {
			current: true
		});
	};

	$scope.changeRadio = function (type) {
		$scope.searchDateType = (type === 1) ? 1 : 2;
	};

	 function getFactorRiskList(callback){
		 	var param={};
		 	mainDataService.getFactorRiskList(param)
		 	.success(function(data){
		 		$scope.factorList =data;
		 		if(typeof callback=='function'){
			 		callback(data);
		 		}
		 	});
		 	
		 }
		 function getGradeRiskList(callback){
		 	var param = {};
		 	mainDataService.getGradeRiskList(param)
		 	.success(function(data){
		 		$scope.gradeList =data;
		 		if(typeof callback=='function'){
			 		callback(data);
		 		}	 		
		 	});	 
		 }
	
		 $scope.gradeSelect = function(grade){
		    	console.log(grade);
		    	$scope.SelectedGradeInfo = grade;
		    	$scope.EditGradeInfo = angular.copy(grade);
		    	var param = {RISK_SPEC_SID:grade.RISK_SPEC_SID};
		    	mainDataService.getRiskGradeDetail(param)
		    	.success(function(data){
		    		var GRADE_CD = data[0].GRADE_CD;
			 		$scope.GB_ARRAY = new Array();
			 		$scope.GB_ARRAY.push({name:data[0].GRADE_CD,cnt:1,idx:0});
			 		$.each(data,function(idx,item){
			 			if(data[idx].GRADE_CD == GRADE_CD){
			 				if(idx>0)
		 					$scope.GB_ARRAY[$scope.GB_ARRAY.length-1].cnt +=1;
			 			}
			 			else{
			 				$scope.GB_ARRAY.push({name:data[idx].GRADE_CD,cnt:1,idx:idx});	 				
			 			}
			 			item.GB_Idx = $scope.GB_ARRAY.length-1;
			 			item.row_idx = $scope.GB_ARRAY[$scope.GB_ARRAY.length-1].idx;
			 			item.idx = idx;
			 			GRADE_CD = item.GRADE_CD;
			 		});
			 		$.each(data,function(idx,item){
			 			item.GB_cnt = $scope.GB_ARRAY[item.GB_Idx].cnt; 
			 		});		    		
		    		console.log(data);
		    		$scope.gradeDetail = data;
		    		$scope.dataGradeList = data;
		    	});
		 }
		 
		 $scope.clickTable = function($event){
		    	$($event.currentTarget).addClass('active').siblings().removeClass('active');
		 };
		 
		 $scope.factorSelect = function (factor){
		    	console.log(factor);
		    	$scope.SelectedFactorInfo = factor;
		    	$scope.selectedFactor = factor;
		    	//$scope.tab2Click('0');
		    	
			 	var param = {RISK_PROB_SID:factor.RISK_PROB_SID};
				mainDataService.getFactorRiskDetail(param)
			 	.success(function(data){
			 		console.log(data);
			 		$scope.popupFactorDetail = angular.copy(data);
			 		$scope.factorDetail = angular.copy(data);
			 		$.each($scope.popupFactorDetail,function(idx,Item){
			 			//Item.IsActive = (Item.USE_YN=='Y')?true:false;
			 		});
			 	});
		 	    	
		    }
		 
	function setGrid() {
	        return pagerJsonGrid({
	            grid_id : 'list_risk',
	            pager_id : 'listPager',
	            url : '/risk/getList.json',
	            condition : {
	                page : 1,
	                rows : GridConfig.sizeL,
					del_yn: 'N'
	            },
	            rowNum : 20,
	            colNames : [ '순번','평가번호','평가명','평가기간','작성자','작성일자','수정일자'],
	            colModel : [
	            {
	                name : 'RNUM',
	                width : "40px",
	                resizable: false
	            }, {
	                name : 'RISK_SID',
	                width : 0,
					hidden : true
	            }, {
	                name : 'EST_NM',
	                width : 150,
	                resizable: false
	            }, {
	                name : 'EST_YYYYMM',
	                width : 80,
	                resizable: false
	            }, {
	                name : 'E_NAME',
	                width : 80,
	                resizable: false
	            }, {
					name : 'INSERT_DT',
					width : 80,
					resizable: false
				}, {
					name : 'UPDATE_DT',
					width : 0,
					hidden : true
				}],
				
				
	            onSelectRow : function(rowid, status, e) {
	            	$scope.SelectedRiskInfo = $('#list_risk').jqGrid('getRowData', rowid);
						//alert(JSON.stringify(param));
					if (parseInt($scope.SelectedRiskInfo.RISK_SID) > 0) {
						$scope.risk_current_sid = $scope.SelectedRiskInfo.RISK_SID;
					}
					$scope.loadAssetList($scope.SelectedRiskInfo.RISK_SID);
					
					var param1 = {RISK_SID : $scope.SelectedRiskInfo.RISK_SID,tabId : 1};
					var param2 = {RISK_SID : $scope.SelectedRiskInfo.RISK_SID,tabId : 2};
					
					mainDataService.getRiskAssetGroup(param1)
					.success(function(data){
						$scope.Asset1Group = data;
					});
					mainDataService.getRiskAssetGroup(param2)
					.success(function(data){
						$scope.Asset2Group = data;
					});
					
					$scope.$apply();
				},
	            gridComplete : function() {
					var ids = $(this).jqGrid('getDataIDs');
					
					//if ($.isEmptyObject($scope.PathInfo)) {
						$(this).setSelection(ids[0]);
					//}
					//alert($('#list').getGridParam('page'));
					$scope.currentPageNo=$('#list').getGridParam('page');
					$scope.count = $('#list').getGridParam('records');
					$scope.$apply();
				}		            
	        });
	    }
	 
	 function setGrid2() {
		return pagerGrid({
			grid_id : 'list2',
			pager_id : 'listPager2',
			url : '/risk/getAssetList.json',
            condition : {
                page : 1,
                rows : GridConfig.sizeS,
                RISK_SID : $scope.EditInfo.RISK_SID
            },			
			rowNum : 10,
			colNames : [ '순번','자산코드','자산명','고장확률등급','파괴심각도','여용률','BRE','RISK_SID','ASSET_SID','INSPECT_RESULT','사용률',],
			colModel : [
					{name : 'RNUM',width : 53 ,resizable:false}, 
	                {name : 'ASSET_CD',width : 180, resizable:false}, 
	                {name : 'ASSET_NM',width : 80, resizable:false}, 
	                {name : 'PROB_GRADE',width : 50,  resizable:false},
	                {name : 'SERIOUS_GRADE',width : 50,  resizable:false}, 
	                {name : 'SPARE_RATIO',width : 50,  resizable:false},
	                {name : 'BRE',width : 50,  resizable:false},
	                {name : 'RISK_SID', width : 0, hidden:true,},
	                {name : 'ASSET_SID',width : 0,hidden:true,},
	                {name : 'INSPECT_RESULT',width : 50,hidden: true,}, 
	                {name : 'USE_RATIO',width : 50,hidden: true,}, 
           ]
		});
	 }
	 function setGrid2_Popup() {
	        return pagerGrid({
	            grid_id : 'list2popup',
	            pager_id : 'list2popupPager',
	            url : '/risk/getAssetList.json',
	            condition : {
	                page : 1,
	                rows : 10,
	                RISK_SID : $scope.EditInfo.RISK_SID
	            },
	            rowNum : 10,
	            multiselect : true,
	            colNames : [ '순번','RISK_SID','ASSET_SID','자산코드','자산명','INSPECT_RESULT','사용률','고장확률<br>등급','SERIOUS_GRADE','파괴심각도','spare_ratio','여용률','BRE'],
	            colModel : [
	            {
	                name : 'RNUM',
	                width : "40px",
	            }, {
	                name : 'RISK_SID',
	                width : 40,
	                hidden:true,
	            }, {
	                name : 'ASSET_SID',
	                width : 0,
	                hidden:true,
	            }, {	            	
	                name : 'ASSET_CD',
	                width : "200px",
	            }, {
	                name : 'ASSET_NM',
	                width : 100,
	            }, {	            	
	                name : 'INSPECT_RESULT',
	                width : 60,
	                hidden: true,
	            }, {
	                name : 'USE_RATIO',
	                width : 50,
	                hidden: false,	                
	            }, {
	                name : 'PROB_GRADE',
	                width : 50,
	            }, {
	                name : 'SERIOUS_GRADE',
	                hidden: true,	
	            }, {
	                name : 'SERIOUS_GRADE_EDIT',
	                width : 50,
	                formatter : function(cellvalue, options, rowdata, action){
	                	var risk_sid= rowdata['RISK_SID'];
	                	var asset_sid= rowdata['ASSET_SID'];
	                	var sHTML = "";
	                	console.log(rowdata);
	                	sHTML += '<select class="w100 my-1" value="'+rowdata.SERIOUS_GRADE+'" onchange="updateRiskAsset('+risk_sid+','+asset_sid+',1,this.value)">';
	                	//sHTML += '<option ></option>';
	                	for(var i=1;i<=10;i++){
		                	sHTML += '<option '+((i==rowdata.SERIOUS_GRADE)?'selected':'')+' >'+i+'</option>';	                		
	                	}
	                	sHTML += '</select>';
	                	return sHTML; 

	                }
	            }, {
	                name : 'SPARE_RATIO',
	                hidden: true	
	            }, {
	                name : 'SPARE_RATIO_EDIT',
	                width : 50,
	                formatter : function(cellvalue, options, rowdata, action){
	                	var risk_sid= rowdata['RISK_SID'];
	                	var asset_sid= rowdata['ASSET_SID'];
	                	console.log(rowdata);
	                	return '<input class="w100 my-1" value="' + ((rowdata.SPARE_RATIO)?rowdata.SPARE_RATIO:0) + '" onchange="updateRiskAsset('+risk_sid+','+asset_sid+',2,this.value)"></input>';
	                }	                
	            }, {
	                name : 'BRE',
	                width : 50
	            }],
	            onSelectRow : function(id) {
	                //$scope.AssetItemInfo = $('#list2popup').jqGrid('getRowData', id);
	                //console.log($scope.AssetItemInfo);
	            },
	            gridComplete : function() {
					var ids = $(this).jqGrid('getDataIDs');
					if($(this).jqGrid('getRowData', ids[0]).RNUM=="") $scope.assetCount = 0;
					else $scope.assetCount = $(this).getGridParam('records');
					$scope.$apply();
				}	            
	        });
	    }	 

	 function setGrid3_Popup() {
	        return pagerGrid({
	            grid_id : 'list3popup',
	            pager_id : 'list3popupPager',
	            url : '/diag/getList.json',
	            condition : {
	                page : 1,
	                rows : 10
	            },
	            rowNum : 10,
	            multiselect : false,
	            colNames : [ '순번', '평가명', '평가기간', '진단결과', '작성자', '작성일자','sid', 'temp', '년도', '월'],
	            colModel : [
	            	{name : 'RNUM', width : 6, sortable:false}, 
		            {name : 'EST_NM', width : 44,}, 
		            {name : 'EST_YYYYMM', width : 18,}, 
		            {name : 'EST_MODEL_TYPE', width : 0,hidden : true}, 
		            {name : 'INSERT_ID', width : 18,}, 
		            {name : 'INSERT_DT', width : 18}, 
		            {name : 'LIFE_SID', width : 0, hidden : true},
		            {name : 'TEMP_YN', width : 0, hidden : true},
		            {name : 'YEAR', width : 0, hidden : true},
		            {name : 'MONTH', width : 0, hidden : true}
	            ],

	            onSelectRow : function(id) {
	                $scope.EstLifeInfo = $('#list3popup').jqGrid('getRowData', id);
	                //console.log($scope.AssetItemInfo);
	            }
	        });
	    }
	 
	$scope.deleteRisk = function () {
		if (!($scope.risk_current_sid > 0)) {
			alertify.alert('평가를 선택해주세요').set('basic', true);
			return;
		}

		// 삭제 권한 체크
		if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
			alertify.alert('권한이 없습니다.').set('basic', true);
			return;
		}

		alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?',
			function () {
				mainDataService.deleteRisk({
					risk_sid: $scope.risk_current_sid,
					del_yn: 'Y',
				}).success(function (obj) {
					if (!common.isEmpty(obj.errMessage)) {
						alertify.error('errMessage : ' + obj.errMessage);
					} else {
						alertify.success('평가가 삭제 되었습니다.');
						firstPage("list_risk"); //목록 새로고침
						// $scope.MinwonInfo = {};
						// $scope.storeFileList = {};
					}
				});
			},
			function () {
			}
		).set('basic', false);
	};

	$scope.getExcelDownload = function() {
		var jobType = '010304_0';
		var year = '';
		var s_date = '';
		var e_date = '';
		var type = $scope.searchDateType || '';
		var searchText = $scope.searchText; // 민원유형

		// 연도 검색
		if ($scope.searchDateType === 1) {
			year = $scope.selectedCurrYear || '';

			// 기간선택 검색
		} else {
			s_date = $scope.searchDate.S_DATE || '';
			e_date = $scope.searchDate.E_DATE || '';

			// 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
			if (!checkDate($scope.searchDate.S_DATE || '', $scope.searchDate.E_DATE || '', '')) {
				return;
			}
		}

		$('#exportForm').find('[name=jobType]').val(jobType);
		$('#exportForm').find('#s_date').val(s_date);
		$('#exportForm').find('#e_date').val(e_date);
		$('#exportForm').find('#year').val(year);
		$('#exportForm').find('#searchText').val(searchText);
		$('#exportForm').find('#type').val(type);
		$('#exportForm').find('#delete_yn').val('N');
		$('#exportForm').submit();
	};
	
	$scope.DownLoadExcel=function(Type){
    	const jobType = $state.current.name + "_" + Type;
        //const levelNm = $scope.levelNm;

        $('#exportForm').find('[name=jobType]').val(jobType);
        switch(Type){
        case 0 : 
        	$scope.getExcelDownload();
        	return;
        	break;
        case 1 : //평가 항목
        	$('#exportForm').find('[name=jnum]').val( $scope.factorDetail[0].RISK_PROB_SID );
        	break;
        case 2 : //파괴심각도 관리
        	$('#exportForm').find('[name=jnum]').val($scope.gradeDetail[0].RISK_SPEC_SID);
        	break;
	        	
        }
        $('#exportForm').submit();   
	}
	
	//보고서
	 $scope.DownloadReport = function(){
		 	/*
	    	const jobType = $state.current.name ;
	        $('#exportForm').find('[name=jobType]').val(jobType);
        	$('#exportForm').find('[name=jnum]').val( $scope.SelectedRiskInfo.RISK_SID );
  			$('#exportForm').submit();
  			*/
	      	var searchOptions = {
	      			RISK_SID : $scope.SelectedRiskInfo.RISK_SID,
	      			MAX_REPORT_PRINT_LINE_CNT : 100000,
	      			EST_NM : $scope.SelectedRiskInfo.EST_NM,
	      			E_NAME : $scope.SelectedRiskInfo.E_NAME,
	      			INSERT_DT : $scope.SelectedRiskInfo.INSERT_DT,
	      			EST_YYYYMM : $scope.SelectedRiskInfo.EST_YYYYMM
	    		};
	        $('#excelDown').find('[name=selectName]').val("getRiskAssetListExcel1");//CommonDao.
	        //$('#excelDown').find('[name=searchOptions]').val("{RISK_SID:" + $scope.SelectedRiskInfo.RISK_SID + ",MAX_REPORT_PRINT_LINE_CNT:100000,EST_NM:\"test\",E_NAME:\"test1\",INSERT_DT:\"test3\",EST_YYYYMM:\"test2\"}");
	        $('#excelDown').find('[name=searchOptions]').val(JSON.stringify(searchOptions));
	        $('#excelDown').find('[name=outputFileName]').val(encodeURI("위험도평가보고서_"+ $scope.getToday()+".xlsx"));
	        $('#excelDown').find('[name=templateFileName]').val("010304_template");
	        $('#excelDown').find('[name=startRowIdx]').val('10');
	        $('#excelDown').find('[name=startColIdx]').val('0');
	        $('#excelDown').find('[name=columnIdList]').val("RNUM,ASSET_CD,1,2,ASSET_NM,L1,L2,L3,L4,L5,L6,L7,L8,L9,INSTALL_DT,LIFE_NUM,USE_LIFE,REMAIN_LIFE,BRE,PROB_GRADE,SEVIOR_CD,SPARE_RATIO,USE_RATIO");
	        $('#excelDown').find('[name=columnNameList]').val("RNUM,ASSET_CD,1,2,ASSET_NM,L1,L2,L3,L4,L5,L6,L7,L8,L9,INSTALL_DT,LIFE_NUM,USE_LIFE,REMAIN_LIFE,BRE,PROB_GRADE,SEVIOR_CD,SPARE_RATIO,USE_RATIO");
	        $('#excelDown').find('[name=columnWidthList]').val("3000,6000,0,0");
         $('#excelDown').submit();
	 }
	
	/*신규평가*/
	 $scope.NewItem = function(){
		 var param = {temp_yn: 'Y' };
		 	mainDataService.insertRisk(param)
		 	.success(function(risk_sid){
		 		console.log(risk_sid);
		 		$scope.EditInfo = {
		 			Mode : 'new',
		 			RISK_SID : risk_sid,
	                CLASS3_CD: '',
	   			 	CLASS4_CD: '',
		 			INSERT_ID: sessionStorage.getItem("loginId"),
		 			INSERT_DT: Common_Time.toDay().START_DT,
		 			YYYY: Common_Time.toDay().YEAR,
		 			MM: Common_Time.toDay().MONTH
		 		}
		 		$("#newRiskPopup").show();
				 //alert('신규평가');
	    		$("#list2popup").setGridParam({
	    			datatype : 'json',
	    			page : $scope.currentPageNo,
	    			postData : {
		                CLASS3_CD: '',
		   			 	CLASS4_CD: '',
	    				RISK_SID : $scope.EditInfo.RISK_SID
	    			}
	    		}).trigger('reloadGrid', {
	    			current : true
	    		});		 		
		 	});
		 	
	 }
	 
	 $scope.closeNewItemPopup = function(){
		 $("#newRiskPopup").hide();
	 }
	 
	 
		//평가시작
	 $scope.SaveRiskResult = function(){
	 	//alert('평가시작');
		 if($scope.EditInfo.EST_NM =='' || $scope.EditInfo.EST_NM== null ){
			  
			alertify.alert('오류','평가명을 입력하세요');
			return false;
		 }
		 if($scope.EditInfo.YYYY =='' || $scope.EditInfo.YYYY== null || ('' + $scope.EditInfo.YYYY).length != 4){
			alertify.alert('오류','평가년월을  입력하세요');
			return false;
		 }
		 if($scope.assetCount == 0){
				alertify.alert('오류','평가대상 자산이 없습니다.가져오기를 통해 자산을 등록하세요.');
				return false;
		 }		 
	 	var param = angular.copy($scope.EditInfo);
	 	console.log($scope.EditInfo);
	 	param.risk_sid = $scope.EditInfo.RISK_SID;
	 	param.est_nm = xssFilter($scope.EditInfo.EST_NM);
	 	param.temp_yn='N';
	 	$scope.SelectedRiskInfo = angular.copy($scope.EditInfo);
	 	mainDataService.updateRisk(param)
	 	.success(function(data){
		//저장 성공
 			/*
			mainDataService.saveRiskAssetResult(param)
			.success(function(data){
			 	console.log(data);
			 	$("#newRiskPopup").hide();
			 	$scope.loadAssetList($scope.EditInfo.RISK_SID);
			});
			*/
	 		$("#newRiskPopup").hide();
	 		$scope.loadRiskList();
	 	});
		 
	 }
	 

	 /*평가항목*/
	 
	 $scope.EditFactorInfo ={};
	 
	 $scope.factorDetail = []; 
	 
	    //신규 평가항목 팝업
	    $scope.newFactorItem = function(){
	    	$("#newRiskFactorPopup").show();
	    	$scope.popupfactorDetail = angular.copy($scope.factorDetail);
	    	$scope.EditFactorInfo = angular.copy($scope.SelectedFactorInfo);
	    	$scope.EditFactorInfo.Mode='new';
	    	$scope.saveType='new';
	    	$scope.EditFactorInfo.RISK_PROB_NM='';   	
	    }   
	    
	    //수정 평가항목 팝업 
	    $scope.editFactorItem = function(){
	    	$("#newRiskFactorPopup").show();
	    	$scope.popupfactorDetail = angular.copy($scope.factorDetail);
	    	$scope.EditFactorInfo = angular.copy($scope.SelectedFactorInfo);
	    	$scope.EditFactorInfo.Mode='';
	    	$scope.saveType='';
	    } 
	    $scope.closeRiskFactor = function(){
			$("#newRiskFactorPopup").hide();    
	    }    
	    
	    
	    $scope.saveRiskFactor = function(){
	    	
	    	if(1){
	    		if($scope.EditFactorInfo.RISK_PROB_NM == null || $scope.EditFactorInfo.RISK_PROB_NM == ""){
	    			alertify.alert('오류','평가기준명 필수을 입력하세요');
	    			return false;
	    		}
	    		
				if(!validateSignValueRange($scope.popupFactorDetail)){
		    		alertify.alert('오류','등급 구간값을 확인하세요. 각 등급별 구간이 중복될 수 없습니다.');
		    		return false;
	    		}

	    	}
    	
	    	if($scope.EditFactorInfo.Mode=='new'){
	    		var list = new Array();
	    		$.each($scope.popupFactorDetail,function(idx,Item){
	    			for(key in Item){
	        			if(Item[key]==null) Item[key]="";
	        		}
	    			list.push(Item);
	    		});
	    		var param ={
	    				RISK_PROB_SID : 0,
	    				RISK_PROB_NM : $scope.EditFactorInfo.RISK_PROB_NM,
	    				ITEM_LIST : list
	    		};
	        	mainDataService.insertRiskFactorList(param)
	        	.success(function(data){
	        		getFactorRiskList(function(){
	        			$scope.factorSelect($scope.factorList[0]);
	        		});
	        		$scope.closeRiskFactor();
	        	});

	    	}else{
	    		var list = new Array();
	    		$.each($scope.popupFactorDetail,function(idx,Item){
	    			for(key in Item){
	        			if(Item[key]==null) Item[key]="";
	        		}
	    			list.push(Item);
	    		});	    		
	    		$.each($scope.popupFactorDetail,function(idx,Item){
	    			Item.INSERT_ID='';
	    		});
	    		var param ={
	    				RISK_PROB_SID : $scope.EditFactorInfo.RISK_PROB_SID,
	    				RISK_PROB_NM : $scope.EditFactorInfo.RISK_PROB_NM,
	    				ITEM_LIST : list
	    		};
	        	mainDataService.updateRiskFactorList(param)
	        	.success(function(data){
	        		getFactorRiskList(function(){
	        			$scope.factorSelect($scope.factorList[0]);
	        			//$scope.factorSelect($scope.selectedFactor);
	        		});
	        		$scope.closeRiskFactor();
	        	});    		
	    	}
	    	
	    }
	    
	    /*파괴심각도*/
	    $scope.newGradeItem=function(){
	    	$("#newRiskGradePopup").show();
	    	$scope.popupGradeDetail = angular.copy($scope.gradeDetail);
	    	$scope.EditGradeInfo = angular.copy($scope.SelectedGradeInfo);
	    	//$scope.EditGradeInfo.EST_IMP_SID=0;
	    	$scope.EditGradeInfo.Mode='new';
	    	$scope.saveType='new';
	    	/*
	    	$.each(data,function(idx,item){
	 			item.GB_cnt = $scope.GB_ARRAY[item.GB_Idx].cnt; 
	 		});*/
	    }

	    //수정 파괴심각도   
	    $scope.editGradeItem=function(){
	    	$("#newRiskGradePopup").show();
	    	$scope.popupGradeDetail = angular.copy($scope.gradeDetail);
	    	$scope.EditGradeInfo = angular.copy($scope.SelectedGradeInfo);
	    	$scope.EditGradeInfo.Mode='';
	    	$scope.saveType='';
	    	/*
	    	$.each(data,function(idx,item){
	 			item.GB_cnt = $scope.GB_ARRAY[item.GB_Idx].cnt; 
	 		});*/
	    }
	    
	    $scope.closeRiskGrade = function(){
			$("#newRiskGradePopup").hide();    
	    }
	    
	    $scope.showRemainLifeEstListPopup= function(){
	    	$("#remainLifeEstListPopup").show();
	    }
	    $scope.closeRemainLifeEstListPopup = function(){
	    	$("#remainLifeEstListPopup").hide();
	    }
	    
	    $scope.setAssetList = function(){
	    	$("#loadingSpinner").show();
	    	var param = {RISK_SID:$scope.EditInfo.RISK_SID,
	    			LIFE_SID:$scope.EstLifeInfo.LIFE_SID
	    			};
	    	mainDataService.saveRiskAsset(param)
	    	.success(function(data){
	    		console.log(data);
	    		$("#loadingSpinner").hide();
	    		$("#list2popup").setGridParam({
	    			datatype : 'json',
	    			page : $scope.currentPageNo,
	    			postData : {
		                CLASS3_CD: '',
		   			 	CLASS4_CD: '',	    				
	    				RISK_SID : $scope.EditInfo.RISK_SID 
	    			}
	    		}).trigger('reloadGrid', {
	    			current : true
	    		});
	    		
	    	});
	    	$scope.closeRemainLifeEstListPopup();	
	    }
	    
	    $scope.saveRiskGrade = function(){
	    	if(1){
	    		if($scope.EditGradeInfo.EST_SPEC_NM == null || $scope.EditGradeInfo.EST_SPEC_NM == ""){
	    		alertify.alert('오류','평가기준명을 입력하세요 ');
    		return false;
	    		}
	    		/*if($scope.EditFactorInfo.APPLY_DT == null || $scope.EditFactorInfo.APPLY_DT == ""){
	    		alert('적용일자 필수');
	    		return false;
	    		}*/
	    	}
    	
	    	if($scope.EditGradeInfo.Mode=='new'){
	    		var list = new Array();
	    		$.each($scope.popupGradeDetail,function(idx,Item){
	    			for(key in Item){
	        			if(Item[key]==null) Item[key]="";
	        		}
	    			list.push(Item);
	    		});
	    		var param ={
	    				RISK_SPEC_SID : 0,
	    				EST_SPEC_NM : $scope.EditGradeInfo.EST_SPEC_NM,
	    				ITEM_LIST : list
	    		};
	        	mainDataService.insertRiskGradeList(param)
	        	.success(function(data){
	        		getGradeRiskList(function(){
	        			$scope.gradeSelect($scope.gradeList[0]);
	        		});
        			$scope.closeRiskGrade();
	        	});

	    	}else{
	    		var list = new Array();
	    		$.each($scope.popupGradeDetail,function(idx,Item){
	    			for(key in Item){
	        			if(Item[key]==null) Item[key]="";
	        		}
	    			list.push(Item);
	    		});	    		

	    		var param ={
	    				RISK_SPEC_SID : $scope.EditGradeInfo.RISK_SPEC_SID,
	    				EST_SPEC_NM : $scope.EditGradeInfo.EST_SPEC_NM,
	    				ITEM_LIST : list
	    		};
	        	mainDataService.updateRiskGradeList(param)
	        	.success(function(data){
	        		getGradeRiskList(function(data){
		        		$scope.gradeSelect($scope.gradeList[0]);	        			
	        		});
	        		$scope.closeRiskGrade();
	        	});    		
	    	}	    	
	    }
	    

	    
	    $scope.deleteSelectedAsset = function(){
	    	
	    	var list = new Array();
			 var selrow = "";
			 var s = jQuery("#list2popup").jqGrid('getGridParam','selarrrow');
			  if(s.length<=0) {
				  alertify.alert('항목을 선택해주세요');
				  return;
			  }else{
			   s = ""+s;
			   sArr = s.split(",");
			    for(var i=0; i<sArr.length; i++){
			    var ret = jQuery("#list2popup").getRowData(sArr[i]);
			    list.push(parseInt(ret.ASSET_SID));
			    //if(i == 0) selrow = "'"+ret.ASSET_SID+"'";
			    //else selrow += ",'"+ret.ASSET_SID+"'";
			   }
			  }
			  //alert(selrow);
	    	var param = {
		    			RISK_SID : $scope.EditInfo.RISK_SID,
		    			ASSET_LIST:list
		    			};
	    	mainDataService.deleteRiskAsset(param)
			  .success(function(data){
				  reloadPopupAssetList();
			  });
	    }
	    
	    function reloadPopupAssetList(){
			$("#list2popup").setGridParam({
				datatype : 'json',
				postData : {
	                CLASS3_CD: '',
	   			 	CLASS4_CD: '',					
    				RISK_SID : $scope.EditInfo.RISK_SID	    					
				}
			}).trigger('reloadGrid', {
				current : true
			});    	
	    }
	    
	    $scope.updateRiskAsset = function(risk_sid,asset_sid,opt,val){
	    	console.log(risk_sid,asset_sid,opt,val);
	    	var param = {RISK_SID:risk_sid,
	    				ASSET_SID : asset_sid,
	    				OPT : opt,
	    				VAL : val
	    			};
	    	mainDataService.updateRiskAsset(param)
	    	.success(function(data){
	    		console.log(data);
	    	});
	    	
	    }
	    
	    //결과보기 팝업
	    $scope.ShowResult = function (){
	    	console.log($scope.SelectedRiskInfo.RISK_SID);
	    	var param = {RISK_SID : $scope.SelectedRiskInfo.RISK_SID};
	    	mainDataService.getRiskResult(param)
			  .success(function(data){
			    $("#RiskResultPopup").show();
			    $scope.RiskResult = data;
			  });
	    }
	    
	    //결과보기 팝업 닫기
	    $scope.CloseResult = function (){
	    	$("#RiskResultPopup").hide();
	    }
	    
	    $scope.probList = ['P1','P2','P3','P4','P5','P6','P7','P8','P9','P10'];

}

function updateRiskAsset(risk_sid,asset_sid,opt,val){
		//$("#DetailResultIndirEstimationPopup").show();
		if(parseInt(asset_sid) > 0){
		var scope = angular.element(document.getElementById('newRiskPopup')).scope();
		scope.updateRiskAsset(risk_sid,asset_sid,opt,val);
		}
}