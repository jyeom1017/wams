angular.module('app.finance').controller('financialPlanController', financialPlanController);

function financialPlanController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	
	var tableInfo = new Array();
	var i=0;
	var j=0;
	
	$scope.$on('doAnalyze',function(event,eventData){
		var yearList = angular.copy($scope.PopupyearList2);
		var popupScope = angular.element(document.getElementById('totalPopup')).scope();
		var price_raise_ratio = parseFloat(popupScope.PopupFinanceInfo.PRICE_RAISE_RATIO);
		
		$timeout(function(){
			//합계를 구한다
			$.each(yearList,function(idx,Item){
				popupScope.CalculationFinancial(Item); 
			});
			//화면에 보이는 데이터를 tableInfo로 복사한다.
			$.each(yearList,function(idx,Item){
				for(i=0;i<50;i++)
				tableInfo[i][idx] = parseInt(popupScope.PopupFinanceDetailList[i]['\''+Item+'\'']||0);		
			});
			
			//자본적수지 과부족액
			/*자본적수지(수입부) 합계 – 자본적수지(지출부) 합계*/
			for(j=0;j<30;j++)
			tableInfo[35][j] = tableInfo[25][j] - tableInfo[34][j];
			
			for(j=0;j<30;j++)
			tableInfo[38][j] = tableInfo[35][j];
			
			//자금수지 과부족액
			/*손익계정유보자금+감가상각비+자본적수지과 부족액*/
			for(j=0;j<30;j++)
			tableInfo[39][j] = tableInfo[36][j] + tableInfo[37][j]+ tableInfo[38][j]; 
			
			//tableInfo를 화면으로   복사한다.
			$.each(yearList,function(idx,Item){
				for(i=0;i<50;i++)
				popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = parseInt(tableInfo[i][idx]||0);		
			});

			//요금인상
			/*자금수지 과부족액 – 급수수익 + 급수수익 * 요금인상 % */
				for(i=0;i<10;i++){
					for(j=0;j<30;j++){
					tableInfo[40+i][j] = parseInt(tableInfo[39][j]||0) + Math.round((0.1*i) * tableInfo[0][j]);
					}
				}
				
				$.each(yearList,function(idx,Item){
					for(i=0;i<10;i++){
					popupScope.PopupFinanceDetailList[40+i]['\''+Item+'\''] = parseInt(tableInfo[40+i][idx]||0);
					}
				});			
			
		},1000);
		
	});
	
	$scope.$on('doFinancialInit', function (event,eventData) {

		for(i=0;i<100;i++) 
			tableInfo[i]=new Array();

		for(j=0;j<50;j++)
		for(i=0;i<30;i++){ 
			tableInfo[j].push(0);
		}
		
		var popupScope = angular.element(document.getElementById('totalPopup')).scope();
		var price_raise_ratio = parseFloat(popupScope.PopupFinanceInfo.PRICE_RAISE_RATIO);
		
		//최근 5년에 대한 정의 
			console.log($scope.PopupFinanceInfo.FIN_ANLS_YMD);
			var lastYear = ('' + $scope.PopupFinanceInfo.FIN_ANLS_YMD).substring(0,4);
			var endYear = '' + ( parseInt(lastYear) -1); 
			var startYear = '' + ( parseInt(lastYear) -5);
			console.log(startYear,endYear);
		// 급수수익 4110000
		var yearList = angular.copy($scope.PopupyearList2);
		$scope.getDetail('FN10','01',function(data,fnCd,C_SCODE){
	 		$.each(data,function(idx,item){
	 			if(item.C_SCODE==C_SCODE){
	 				/*
	 				var i=0;
	 				var chklist = ['C_GCODE','C_SCODE','FIN_ANLS_GUBUN_NM','FIN_ANLS_ITEM_CD','FIN_ANLS_ITEM_NM','FIN_ANLS_SID','FIN_ANLS_TYPE'];
	 				$.each(item,function(key,item1){
	 					if($.inArray(key,chklist)<0){
		 					tableInfo[0][i] = item1;
		 					i++;	 						
	 					} 
	 				})*/
	 				$.each(yearList,function(idx,Item){
	 					var i=0;
	 						tableInfo[i][idx]=item['\''+Item+'\''];
	 						popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = parseInt(tableInfo[i][idx]||0);
	 				});
	 				return false;
	 			}
	 		});
		});
		//급수공사수익
		$scope.getPL(startYear,endYear,'4120000',function(data){
			tableInfo[1][0] = parseInt(data.average);
			console.log(data.average);
		});
		//기타영업수익
		$scope.getPL(startYear,endYear,'4130000',function(data){
			tableInfo[2][0] = parseInt(data.average);
			console.log(data.average);
		});
		//이자수익
		$scope.getPL(startYear,endYear,'4610000',function(data){
			tableInfo[3][0] = parseInt(data.average);
			console.log(data.average);
		});
		//기타영업외수익
		$scope.getPL(startYear,endYear,'4650000',function(data){
			tableInfo[4][0] = parseInt(data.average);
			console.log(data.average);
		});
		
		//인건비 합계 *인금상승률  ('00','01')
		$scope.getETC(endYear,endYear,'FN03','',function(data){
			tableInfo[7][0] = 0;
			var v1 = 0;
			var v2 = 0;
			console.log(data);
			try{
			$.each(data,function(idx,Item){
				for(i=parseInt(data.startYear);i<=parseInt(data.endYear);i++){
					if(Item.YYYY == '' + i){
						v1 = parseInt(Item['\'FN03_00\'']||0);
						v2 = parseInt(Item['\'FN03_01\'']||0);
					}
				}
			});
			tableInfo[7][0] = v1 * (100.0 + v2) / 100.0;
			
			} catch(e){
				console.log(e);
			}
			console.log(tableInfo[7][0]);
				$.each(yearList,function(idx,Item){
					var i=7;
					popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = parseInt(tableInfo[i][idx]||0);
				});	
		});
		
		//운영관리비
		$scope.getDetail('FN11','00',function(data,fnCd,C_SCODE){
	 		$.each(data,function(idx,item){
	 			if(item.C_SCODE==C_SCODE){
	 				$.each(yearList,function(idx,Item){
	 					var i=8;
	 						tableInfo[i][idx]=item['\''+Item+'\''];
	 						popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = parseInt(tableInfo[i][idx]||0);
	 				});	 				
	 				return false;
	 			}
	 		});
		});
		
		//충당부채
		$scope.getCF(endYear,endYear,'5120100',function(data){
			tableInfo[9][0] = parseInt(data.average||0);
			console.log(data.average);
		});
		
		//지급이자
		/*기타 비용관리>지급이자탭 내 최근 5년치 평균*물가상승률 + 차입금상환/지급이자탭 내 지급이자 합계, 수기입력 가능*/
		$scope.getETC(startYear,endYear,'FN06','',function(data){
			tableInfo[10][0] = 0;
			var v1 = 0;
			var v2 = 0;
			console.log(data);
			try{
			$.each(data,function(idx,Item){
				for(i=parseInt(data.startYear);i<=parseInt(data.endYear);i++){
					if(Item.YYYY == '' + i){
						v1 += parseInt(Item['\'FN06_10\'']||0);
					}
				}
			});
			tableInfo[10][0] = Math.round(v1/5,1);
			
			} catch(e){
				console.log(e);
			}
			
				$scope.getDetail('FN14','09',function(data,fnCd,C_SCODE){
			 		$.each(data,function(idx,item){
			 			if(item.LOAN_ITEM_CD==C_SCODE){
			 				$.each(yearList,function(idx,Item){
			 					var i=10;
			 						popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = tableInfo[10][0] * Math.pow((100 + 1.5)/100, idx) + parseInt(item['\''+Item+'\'']||0);
			 				});	 				
			 				return false;
			 			}
			 		});
				});
		});
		
		//감가상각비
		$scope.getDetail('FN12','00',function(data,fnCd,C_SCODE){
	 		$.each(data,function(idx,item){
	 			if(item.C_SCODE==C_SCODE){
	 				$.each(yearList,function(idx,Item){
	 					var i=11;
	 						tableInfo[i][idx]=item['\''+Item+'\''];
	 						popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = parseInt(tableInfo[i][idx]||0);
	 				});	 				
	 				return false;
	 			}
	 		});
		});
		
		//기타 영업외비용
		/*손익계산서 내 VI.영업외수익 최근 5년치 평균-VI.>1.이자수익, 수기입력 가능*/
		$scope.getPL(startYear,endYear,'4650000',function(data){
			tableInfo[12][0] = parseInt(data.average||0);
			console.log(data.average);
			$scope.getPL(startYear,endYear,'4610000',function(data){
				tableInfo[12][0] = tableInfo[12][0] - parseInt(data.average||0);
				console.log(data.average);
			});			
		});
		
		//유형자산취득
		/*통계분석  생애주기관리 통계항목 초기투자비 연도별 비용을 가져옴*/
		for(i=0;i<30;i++)
		tableInfo[26][i] = 4346749000;
		

		//금융기관차입금상환
		/*차입금상환/지급이자탭의 금융기관차입금 상환계획 연도별 합계*/
		//tableInfo[30][0] = 0;
		
		//공공자금관리기금상환
		/*차입금상환/지급이자탭의 공공자금관리기금상환계획   연도별 합계*/
		//tableInfo[31][0] = 0;
		
		//지역개발기금상환
		/*차입금상환/지급이자탭의 지역개발기금상환계획   연도별 합계*/
		//tableInfo[32][0] = 0;
		
		$scope.getLoanInfo(function(data,fnCd,C_SCODE){
	 		$.each(data,function(idx,item){
	 			if(item.LOAN_ITEM_CD=='03'){
	 				$.each(yearList,function(idx,Item){
	 					var i=30;
	 					if(parseInt(item.YYYY)==Item){
	 						tableInfo[i][idx]=item.OUTCOME;
	 						popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = parseInt(tableInfo[i][idx]||0);	 						
	 					}
	 				});	 				
	 			}
	 			if(item.LOAN_ITEM_CD=='05'){
	 				$.each(yearList,function(idx,Item){
	 					var i=31;
	 					if(parseInt(item.YYYY)==Item){
	 						tableInfo[i][idx]=item.OUTCOME;
	 						popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = parseInt(tableInfo[i][idx]||0);	 						
	 					}
	 				});	 				
	 			}
	 			if(item.LOAN_ITEM_CD=='07'){
	 				$.each(yearList,function(idx,Item){
	 					var i=32;
	 					if(parseInt(item.YYYY)==Item){
	 						tableInfo[i][idx]=item.OUTCOME;
	 						popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = parseInt(tableInfo[i][idx]||0);	 						
	 					}

	 				});	 				
	 			}	 			
	 		});
		});
		
		
		//자본적수지 과부족액
		/*자본적수지(수입부) 합계 – 자본적수지(지출부) 합계*/
		tableInfo[35][0] = tableInfo[25][0] - tableInfo[34][0];
		
		tableInfo[38][0] = tableInfo[35][0];
		
		//자금수지 과부족액
		/*손익계정유보자금+감가상각비+자본적수지과 부족액*/
		tableInfo[39][0] = tableInfo[36][0] + tableInfo[37][0]+ tableInfo[38][0];  

		//요금인상
		/*자금수지 과부족액 – 급수수익 + 급수수익 * 요금인상 % */
		/*for(i=0;i<10;i++){
			for(j=0;j<30;j++){
			tableInfo[40+i][j] = tableInfo[39][0] - Math.round((1 + 0.1*i) * tableInfo[0][j]);
			}
		}*/
		
		
		$.each(yearList,function(idx,Item){
			for(i=0;i<50;i++){
				popupScope.PopupFinanceDetailList[i]['\''+Item+'\''] = parseInt(tableInfo[i][idx]||0);
			}
		});
		
		
	});
	
	
	

	$scope.getPL = function (yyyy1,yyyy2,account_cd,callback){
		var result = {};
		var data = $scope.FinancialPLList;
		var tot = 0;
		var cnt = 0;			
		$.each(data,function(idx,Item){
			if(Item.ACCOUNT_CD == account_cd){
				var i = 0;
				for(i=parseInt(yyyy1);i<=parseInt(yyyy2);i++){
					try{
						if(typeof Item['\'' + i +'\''] !='undefined'){
							tot += parseInt(Item['\'' + i +'\'']||0);
							cnt ++;							
						}
					}catch(e){
						console.log(e);
					}
				}
				if(cnt==0) cnt = 1;
			}
		});
		var average = Math.floor(tot/cnt);
		result.average = average;
		if(typeof callback=='function')
			callback(result);
		/*
		mainDataService.getFinancialData(param)
		.success(function(data){
			if(typeof callback=='function')
				callback(data);
		});*/
	}
	$scope.getCF = function (yyyy1,yyyy2,account_cd,callback){
		var result = {};
		var data = $scope.FinancialCFList;
		var tot = 0;
		var cnt = 0;			
		$.each(data,function(idx,Item){
			if(Item.ACCOUNT_CD == account_cd){
				var i = 0;
				for(i=parseInt(yyyy1);i<=parseInt(yyyy2);i++){
					try{
						if(typeof Item['\'' + i +'\''] !='undefined'){
						tot += parseInt(Item['\'' + i +'\'']||0);
						cnt ++;
						}
					}catch(e){
						console.log(e);
					}
				}
				if(cnt==0) cnt = 1;
			}
		});
		var average = Math.floor(tot/cnt);
		result.average = average;
		if(typeof callback=='function')
			callback(result);		
		/*
		mainDataService.getFinancialData(param)
		.success(function(data){
			if(typeof callback=='function')
				callback(data);
		});
		*/		
	}
	$scope.getBS = function (yyyy1,yyyy2,account_cd,callback){
		var result = {};
		var data = $scope.FinancialBSList;
		var tot = 0;
		var cnt = 0;			
		$.each(data,function(idx,Item){
			if(Item.ACCOUNT_CD == account_cd){
				var i = 0;
				for(i=parseInt(yyyy1);i<=parseInt(yyyy2);i++){
					try{
						if(typeof Item['\'' + i +'\''] !='undefined'){
							tot += parseInt(Item['\'' + i +'\'']||0);
							cnt ++;
						}
					}catch(e){
						console.log(e);
					}
				}
				if(cnt==0) cnt = 1;
			}
		});
		var average = Math.floor(tot/cnt);
		result.average = average;
		if(typeof callback=='function')
			callback(result);		
		/*
		mainDataService.getFinancialData(param)
		.success(function(data){
			if(typeof callback=='function')
				callback(data);
		});
		*/		
	}
	$scope.getETC = function (yyyy1,yyyy2,cd1,cd2,callback){
		
		mainDataService.getOtherCostManagement({
			fin_anls_sid : 0,
			etc_cost_cd1 : cd1
	    }).success(function(data){
	    	data.startYear = yyyy1;
	    	data.endYear= yyyy2;
	    	data.cd1 = cd1;
	    	data.cd2 = cd2;
	    	if(typeof callback=='function')
				callback(data);
	    	
	    });
		/*
		mainDataService.getFinancialData(param)
		.success(function(data){
			if(typeof callback=='function')
				callback(data);
		});
		*/		
	}
	
	$scope.getDetail = function(fnCD,C_SCODE,callback){
		mainDataService.getFinancialPlanDetailList({
			fin_anls_sid : $scope.PopupFinanceInfo.FIN_ANLS_SID,
			fin_anls_ymd : $scope.PopupFinanceInfo.FIN_ANLS_YMD.replace(/-/gi,''),
			fin_anls_type : fnCD
	    }).success(function(data){
	    	if(typeof callback=='function')
	    		callback(data,fnCD,C_SCODE);
	    });		
	}
	
	$scope.getLoanInfo = function(callback){
		mainDataService.getLoanInfo({
			fin_anls_sid : $scope.PopupFinanceInfo.FIN_ANLS_SID,
	    }).success(function(data){
	    	if(typeof callback=='function')
	    		callback(data);
	    });		
	}
}