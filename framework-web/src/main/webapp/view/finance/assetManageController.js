angular.module('app.finance').controller('assetManageController', assetManageController);

function assetManageController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
    $scope.searchDateType = '1'; // 날짜검색 : 연도(1) or 기간선택(2)
    $scope.searchDate = {}; // 기간선택
    $scope.planName = ''; // 계획명
    $scope.changeRadio = function (type) {
        $scope.searchDateType = (type === 1) ? 1 : 2;
    };
    $scope.storeYear = getYears(YearSet.baseYear, YearSet.plus);
    $scope.selectedCurrYear = $scope.selectedYear = '' + (new Date()).getFullYear();
    $scope.MCODE = '0403';
    $scope.saveType = '';
    $scope.SelectedAMPInfo = {};
	$scope.amp_sid = '';
	$scope.AMPDetailList = [];
	setDatePicker();
	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 6개월
        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.searchDate.S_DATE = s_yyyymmdd;
        $('#searchDate_S_DATE').val(s_yyyymmdd);
        $scope.searchDate.E_DATE = e_yyyymmdd;
        $('#searchDate_E_DATE').val(e_yyyymmdd);
    }
	
	$scope.getExcelDownload = function(){
    	let jobType = $state.current.name+ "_" + 1;
		Common_Excel.ExcelDown(jobType, {});
	}
/*	$scope.displayDateFormat = function(str){
		if(str=='' || str==null) return '';
		var rstr = (str.replace(/-/gi,'') + '0101').substring(0,8);
		return rstr.substring(0,4) + '-' + rstr.substring(4,6) + '-' + rstr.substring(6,8);
	}*/
    $scope.$on('$viewContentLoaded', function () {
    	setGrid();
    	setDatePicker();
    	$scope.setWeek();
    	//getAMPDetailList($scope.SelectedAMPInfo.AMP_SID);
    	$rootScope.setBtnAuth($scope.MCODE);
    });

    function setGrid() {
        return pagerJsonGrid({
            grid_id : 'list',
            pager_id : 'listPager',
            url : '/finance/getAssetManagementPlanList.json',
            condition : {
                page : 1,
                rows : GridConfig.sizeL,
                temp_yn     : 'N',
                del_yn   : 'N',
            },
            rowNum : 20,
            colNames : [ '순번','SID', '계획명', '분석일', '작성자', '작성일자','temp','완료현황'],
            colModel : [
                { name : 'RNUM', width : "40px",resizable: false },
                { name : 'AMP_SID', width : 0, hidden : true },
                { name : 'AMP_NM', width : 150,resizable: false },
                { name : 'AMP_YMD', width : 70,resizable: false },
                { name : 'WRTR_NM', width : 70,resizable: false },
                { name : 'WRT_YMD', width : 70,resizable: false },
                { name : 'TEMP_YN', width : 0, hidden : true },
                { name : 'STAT', width : 30, edittype : 'select', formatter : 'select', editoptions : { value :' 0:미완료; 1:완료' }}
            ],
            onSelectRow : function(rowid, status, e) {
                var param = $('#list').jqGrid('getRowData', rowid);
                $scope.SelectedAMPInfo = $('#list').jqGrid('getRowData', rowid);
            	if (parseInt($scope.SelectedAMPInfo.AMP_SID) > 0) {
            		$scope.amp_sid = $scope.SelectedAMPInfo.AMP_SID;
            	}
            	getAMPDetailList($scope.SelectedAMPInfo.AMP_SID);
                $scope.$apply();
            },
            gridComplete : function() {
				var ids = $(this).jqGrid('getDataIDs');
				$(this).setSelection(ids[0]);
				$scope.currentPageNo = $('#list').getGridParam('page');
				$scope.count = $('#list').getGridParam('records');
				$scope.$apply();
			}
        });
    }
    
    $scope.loadAMPList = function(){
		$("#list").setGridParam({
			datatype : 'json',
			page : $scope.currentPageNo,
			postData : {

			}
		}).trigger('reloadGrid', {
			current : true
		});		
	}

    function getAMPDetailList(amp_sid){
	 	var param = {
	 			amp_sid : amp_sid //$scope.SelectedAMPInfo.AMP_SID
	 	};
	 	mainDataService.getAssetManagementPlanDetailList(param)
	 	.success(function(data){
	 		$scope.AMPDetailList =data;
	      	$.each($scope.AMPDetailList,function(idx,Item){
	      		//console.log(Item.CHECK_YN);
	      		Item.CHECK_YN_TRUE = (Item.CHECK_YN=='Y')?true:false;
	      		Item.CHECK_YN_FALSE =(Item.CHECK_YN=='N')?true:false;
	      	});
	      	var Rnum = 0;
	      	$.each($scope.AMPDetailList,function(idx,Item){
	      		if(Item.REPORT_NM !== null){
	      			Rnum ++;
	      			Item.RNUM = Rnum;
	      		}
	      	});
	      	console.log($scope.AMPDetailList);
	      	$scope.PopupAMPDetailList = angular.copy($scope.AMPDetailList); 
	      	//$scope.PopupAMPDetailList.sort();
	 		if(typeof callback=='function'){
		 		callback(data);
	 		}
	 		
	 		$scope.changeLastDT();
	 	});
	 	console.log($scope.AMPDetailList);
	}
    
    $scope.onKeyPress = function(event){
        if (event.key === 'Enter') {
            $scope.search();
        }
    }
    
    $scope.search = function () {
        var data = {};

        data.page = 1;
        data.rows = GridConfig.sizeL;
        data.type = $scope.searchDateType || '';

        // 연도 검색
        if ($scope.searchDateType === 1) {
            data.year = $scope.selectedCurrYear || '';
        // 기간선택 검색
        } else {
            data.s_date = $scope.searchDate.S_DATE || '';
            data.e_date = $scope.searchDate.E_DATE || '';

            // 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
            if (!checkDate($scope.searchDate.S_DATE, $scope.searchDate.E_DATE, '')) {
            	//alertify.alert('','기간을 다시 선택해주세요.');
                return;
            }
        }
        console.log(data);

        //if ($scope.planName !== null && $scope.planName !== '') {
        	data.amp_nm = $scope.planName || '';
        //}

        $('#list').jqGrid('setGridParam', {
            postData: data,
        }).trigger('reloadGrid', {
            current: true,
        });
    };

    $scope.deleteAssetManage = function () {
        if (!($scope.SelectedAMPInfo.AMP_SID > 0)) {
            alertify.alert('','항목을 선택해주세요');
            return;
        }

        // 삭제 권한 체크
        if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
            alertify.alert('','권한이 없습니다.');
            return;
        }

        alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?',
            function () {
                mainDataService.deleteAssetManagementPlan({
                    amp_sid: $scope.SelectedAMPInfo.AMP_SID,
                    // del_yn: 'Y',
                }).success(function (obj) {
                    if (!common.isEmpty(obj.errMessage)) {
                        alertify.error('errMessage : ' + obj.errMessage);
                    } else {
                        alertify.success('계획이 삭제 되었습니다.');
                        firstPage("list"); //목록 새로고침
                    }
                });
            },
            function () {
            }
        ).set('basic', false);
    };

    $scope.DatePick = function (){
    	setDatePicker();
    };
    
     $scope.PopupAMPInfo= [];
     $scope.PopupAMPDetailList = [];
     
 	 //자산관리계획수립 닫기
 	 $scope.CloseAssetManagePopup = function(){
 		 $("#AssetManagePlanPopup").hide();
 	 };
 	 

  	function setToday(){
        var today = new Date(); // 오늘
        var eYear = today.getFullYear();
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);
        $scope.PopupAMPInfo.WRT_YMD = e_yyyymmdd; // 작성일 오늘 날짜
        $('#Popup_WRT_YMD').val(e_yyyymmdd);
	 }
  	

    //최근시행일 포멧
    $scope.changeLastDT = function (){
  		$.each($scope.PopupAMPDetailList,function(idx,Item){
  			if(Item.LAST_EXE_DT !== null){
	  	  		var LastDTYear = Item.LAST_EXE_DT.substring(0, 4);
	  	  		var LastDTMonth = Item.LAST_EXE_DT.substring(4, 6);
	  	  		var LastDTDay = Item.LAST_EXE_DT.substring(6, 8);
	
	  	  		if(LastDTMonth != ''){
					LastDTMonth = '-' + LastDTMonth
				}
	  	  		
	  	  		if(LastDTDay != ''){
	  	  			LastDTDay = '-' + LastDTDay
	  	  		}
	  	  		
	
	  	  		Item.LAST_EXE_DT = LastDTYear + LastDTMonth + LastDTDay;
  			}
  		});
    }
  	
     //자산관리계획수립 팝업
     $scope.ShowAssetManagePopup = function(){
     	//$scope.PopupAMPDetailList = angular.copy($scope.AMPDetailList);
     	$scope.PopupAMPInfo = angular.copy($scope.SelectedAMPInfo);
     	$scope.PopupAMPInfo.AMP_NM='';
     	/*$scope.PopupAMPInfo.WRTR_NM='';
     	$scope.PopupAMPInfo.WRT_YMD='';*/
     	$scope.PopupAMPInfo.AMP_YMD='';
     	$scope.saveType = 'new';
     	setToday();
     	
     	
     	mainDataService.insertAssetManagementPlan({})
    	.success(function(data){
    		$scope.PopupAMPInfo.AMP_SID = data;
    		getAMPDetailList($scope.PopupAMPInfo.AMP_SID);
    		
    		$("#AssetManagePlanPopup").show();
    	});
     	console.log($scope.PopupAMPDetailList);
     };   
   
     //자산관리계획수립 팝업 수정
     $scope.ShowTemporaryAssetManagePopup = function(){
     	$("#AssetManagePlanPopup").show();
     	$scope.saveType = '';
     	$scope.PopupAMPInfo = angular.copy($scope.SelectedAMPInfo);
     	var  list= []; //angular.copy($scope.AMPDetailList);
     	$.each($scope.AMPDetailList,function(idx,Item){
      		Item.CHECK_YN_TRUE = (Item.CHECK_YN=='Y')?true:false;
      		Item.CHECK_YN_FALSE = (Item.CHECK_YN=='N')?true:false;
     		list.push(Item);
     	})
     	$scope.PopupAMPDetailList = list;
     	     	
     };

     //팝업 분석일 조회
     $scope.searchDetail = function(){
	 	var param = {
	 			amp_sid : $scope.PopupAMPInfo.AMP_SID,
	 			amp_dt : $scope.PopupAMPInfo.AMP_YMD.replace(/-/gi,'')
	 	};
	 	$("#loadingSpinner").show();
	 	mainDataService.getAssetManagementPlanDetailList(param)
	 	.success(function(data){
	 		$scope.PopupAMPDetailList =data;
	 		//$scope.PopupAMPDetailList = angular.copy($scope.PopupAMPDetailList);
	      	$.each($scope.PopupAMPDetailList,function(idx,Item){
	      		Item.CHECK_YN_TRUE = (Item.CHECK_YN=='Y')?true:false;
	      		Item.CHECK_YN_FALSE = (Item.CHECK_YN=='N')?true:false;
	      	});    		 		
	 		console.log(data);
	 		/*
	 		if(typeof callback=='function'){
		 		callback(data);
	 		}*/
	 		console.log($scope.PopupAMPDetailList);
	 		$scope.changeLastDT();
	 		$("#loadingSpinner").hide();
	 	});
	 	//console.log($scope.AMPDetailList);
     };
     
     //체크 이벤트
     $scope.SaveCheckPopup = function(Item,CheckYN){
    	 Item.CHECK_YN = CheckYN;
    	 Item.CHECK_YN_TRUE = (Item.CHECK_YN =='Y')?true:false;
    	 Item.CHECK_YN_FALSE = (Item.CHECK_YN =='N')?true:false;
    	 
    	 console.log(Item);
    	 var param = {
    	      		AMP_SID : Item.AMP_SID,
    	      		AMP_CHEK_ITEM_CD : Item.CD,
    	      		DATA_INFO : Item.DATA_INFO ||"",
    	      		LAST_EXE_DT : Item.LAST_EXE_DT||"",
    	      		CHECK_YN :  CheckYN 
    	      	};
  	      		
    	 console.log(param);
      	mainDataService.updateAssetManagementPlanDetail(param)
     	.success(function(data){
     		
     	});
     };
     
     //자산관리계획수립 저장
     $scope.SaveAssetManagePopup = function(){
    	if($scope.PopupAMPInfo.AMP_NM == null || $scope.PopupAMPInfo.AMP_NM == ""){
    		alertify.alert('','계획명을 입력하세요.');
    		return false;
	    };
 	    var itemList = [];
 	    $.each($scope.AMPDetailList,function(idx,Item){
 	    	var check_yn = (Item.CHECK_YN||'');
 	    	var last_exe_dt = Item.LAST_EXE_DT.replace(/-/gi,'') ||'';
 	    	var data_info = parseInt(Item.DATA_INFO)||0; 
 	    	itemList.push({AMP_CHECK_ITEM_CD :Item.AMP_CHECK_ITEM_CD, CHECK_YN:check_yn,LAST_EXE_DT:last_exe_dt,DATA_INFO:data_info});	
 	    });
 	    
     	mainDataService.updateAssetManagementPlan({
     		AMP_SID : $scope.PopupAMPInfo.AMP_SID,
     		AMP_NM : xssFilter($scope.PopupAMPInfo.AMP_NM), 		// 계획명
     		WRTR_NM : $scope.PopupAMPInfo.WRTR_NM,     // 작성자
     		WRT_YMD : $scope.PopupAMPInfo.WRT_YMD,		// 작성일
     		AMP_DT : $scope.PopupAMPInfo.AMP_YMD.replace(/-/gi,''),
     		STAT : '1',
     		ITEM_LIST : itemList,
     		TEMP_YN: 'N',
     	})
    	.success(function(data){
    		$scope.loadAMPList();
    		$scope.CloseAssetManagePopup();
    	});
     	
     	$scope.SelectedAMPInfo = angular.copy($scope.PopupAMPInfo);
     	$scope.AMPDetailList = angular.copy($scope.PopupAMPDetailList);
     	console.log($scope.SelectedAMPInfo);
     	console.log($scope.AMPDetailList);
     };
     
     //자산관리계획수립 임시저장
     $scope.TemporarySaveAssetManagePopup = function(){
    	 if($scope.PopupAMPInfo.AMP_NM == null || $scope.PopupAMPInfo.AMP_NM == ""){
     		alertify.alert('','계획명을 입력하세요.');
     		return false;
 	    };
 	    var itemList = [];
 	    $.each($scope.AMPDetailList,function(idx,Item){
 	    	var check_yn = (Item.CHECK_YN||'');
 	    	var last_exe_dt = Item.LAST_EXE_DT.replace(/-/gi,'') || '';
 	    	var data_info = parseInt(Item.DATA_INFO) || 0; 
 	    	itemList.push({AMP_CHECK_ITEM_CD :Item.AMP_CHECK_ITEM_CD, CHECK_YN:check_yn,LAST_EXE_DT:last_exe_dt,DATA_INFO:data_info});
 	    });
 	    
      	mainDataService.updateAssetManagementPlan({
      		AMP_SID : $scope.PopupAMPInfo.AMP_SID,
      		AMP_NM : xssFilter($scope.PopupAMPInfo.AMP_NM), 		// 계획명
      		WRTR_NM : $scope.PopupAMPInfo.WRTR_NM,     // 작성자
      		WRT_YMD : $scope.PopupAMPInfo.WRT_YMD,		// 작성일
      		AMP_DT : $scope.PopupAMPInfo.AMP_YMD.replace(/-/gi,''),
      		STAT : '0',
      		ITEM_LIST : itemList,
      		TEMP_YN: 'Y' 
      	})
     	.success(function(data){
     		$scope.loadAMPList();
     		$scope.CloseAssetManagePopup();
     	});

      	$scope.SelectedAMPInfo = angular.copy($scope.PopupAMPInfo);
      	$scope.AMPDetailList = angular.copy($scope.PopupAMPDetailList);
      	console.log($scope.SelectedAMPInfo);
     	console.log($scope.AMPDetailList);
     };
     

}