angular.module('app.finance').controller('oipController',oipController);
angular.module('app.diagnosis').controller('dscntController', dscntController);

function oipController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message,$filter, MaxUploadSize) {	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 6개월
        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.Serch.Info.START_DT = s_yyyymmdd;
        $('#start_dt').val(s_yyyymmdd);
        $scope.Serch.Info.END_DT = e_yyyymmdd;
        $('#end_dt').val(e_yyyymmdd);
    }
	
	$scope.$on('$viewContentLoaded', function () {
    	setDatePicker();
    	dscnt.Set();
    	grid.setGrid();
    	$scope.setWeek();

    	$scope.MCODE = '0401';
    	$rootScope.setBtnAuth($scope.MCODE);
    	
    	Common_Modal.Init($compile, $scope, {
  			id: '#list2',
  			layerId: '#modal',
  			hidenSize: 0
  	 	});
    });
	
	let Serch = {
			Info : {
				OPTION : 1, 
				YEAR : "", 
				START_DT : "", 
				END_DT : "",
				EST_NM : ''
			},
			Lookup : function() {
				if(this.Info.OPTION == 2 && this.Info.START_DT > this.Info.END_DT){
					alertify.alert("오류","시작날짜와 종료날짜를 확인해 주세요.");
					return;
				}
				$('#list').jqGrid('setGridParam', {postData : this.Info, page : 1}).trigger('reloadGrid');	
			},
			Delete : function() {
				alertify.confirm('삭제 확인','작성하신 자료를 삭제 하시겠습니까?', function() {
					mainDataService
						.CommunicationRelay("/oip/deleteOIP.json", oip.Info)
						.success(function(param) {
							if(Common_AlertStr(param)) return;
							
							$('#list').jqGrid('setGridParam', Serch.Info).trigger('reloadGrid');
						});
				}, function() {});
			},
			Output : function() {
				let jobType = $state.current.name+ "_" + 1;
				//console.log(jobType)
				Common_Excel.ExcelDown(jobType, Serch.Info);
			},
			Select : function(option) {
				this.Info["OPTION"] = option;
			}
	 }
	
	
	let GridHeader = {
			setGrid3: function(){
				$("#list3").jqGrid('setGroupHeaders', {
			        useColSpanStyle: true,
			        groupHeaders:[
			       {startColumnName: 'LOS_MANPOWER', numberOfColumns: 9, titleText: 'RPN 영향인자 및 가중치(%)'},
			        ]
			     });
		    	$("#list3").jqGrid('setGroupHeaders', {
			        useColSpanStyle: true,
			        groupHeaders:[
			        {startColumnName: 'LOS_MANPOWER', numberOfColumns: 7, titleText: 'LoS 연관성'},
			        {startColumnName: 'BRE_VALUE', numberOfColumns: 1, titleText: 'BRE'},
			        {startColumnName: 'ASSET_USAGE_RATE', numberOfColumns: 1, titleText: '자산사용률'},
			        ]
			     });
		    	  
		    	$("#list3").jqGrid('setGroupHeaders', {
			        useColSpanStyle: true,
			        groupHeaders:[
			        {startColumnName: 'LOS_MANPOWER', numberOfColumns: 1, titleText: '인력'},
			        {startColumnName: 'LOS_FACILITY', numberOfColumns: 1, titleText: '시설'},
			        {startColumnName: 'LOS_OPERATE', numberOfColumns: 1, titleText: '운영'},
			        {startColumnName: 'LOS_SERVICE', numberOfColumns: 1, titleText: '서비스'},
			        {startColumnName: 'LOS_ENV', numberOfColumns: 1, titleText: '환경'},
			        {startColumnName: 'LOS_FINANCE', numberOfColumns: 1, titleText: '재정'},
			        {startColumnName: 'LOS_TOTAL', numberOfColumns: 1, titleText: '합계'},
			        {startColumnName: 'BRE_VALUE', numberOfColumns: 1, titleText: '(점)'},
			        {startColumnName: 'ASSET_USAGE_RATE', numberOfColumns: 1, titleText: '(%)'},
			        ]
			     });
			},
			setGrid4: function(){
				$("#list4").jqGrid('setGroupHeaders', {
			        useColSpanStyle: true,
			        groupHeaders:[
			       {startColumnName: 'MNTN_FACILITY_AMT', numberOfColumns: 21, titleText: 'ORDM'}
			        ]
			     });
		    	$("#list4").jqGrid('setGroupHeaders', {
			        useColSpanStyle: true,
			        groupHeaders:[
			       {startColumnName: 'MNTN_FACILITY_AMT', numberOfColumns: 7, titleText: '수리/유지관리'},
			       {startColumnName: 'RB_FACILITY_AMT', numberOfColumns: 7, titleText: '갱셍'},
			       {startColumnName: 'REP_FACILITY_AMT', numberOfColumns: 7, titleText: '교체'}
			        ]
			     });
			},
			setGrid5: function(){
				
		    	$("#list5").jqGrid('setGroupHeaders', {
			        useColSpanStyle: true,
			        groupHeaders:[
			       {startColumnName: 'LOS_MANPOWER', numberOfColumns: 9, titleText: 'RPN 영향인자 및 가중치(%)'},
			        ]
			     });
		    	$("#list5").jqGrid('setGroupHeaders', {
			        useColSpanStyle: true,
			        groupHeaders:[
			        {startColumnName: 'LOS_MANPOWER', numberOfColumns: 7, titleText: 'LoS 연관성'},
			        {startColumnName: 'BRE_VALUE', numberOfColumns: 1, titleText: 'BRE'},
			        {startColumnName: 'ASSET_USAGE_RATE', numberOfColumns: 1, titleText: '자산사용률'},
			        ]
			     });
		    	
		    	$("#list5").jqGrid('setGroupHeaders', {
			        useColSpanStyle: true,
			        groupHeaders:[
			        {startColumnName: 'LOS_MANPOWER', numberOfColumns: 1, titleText: '인력'},
			        {startColumnName: 'LOS_FACILITY', numberOfColumns: 1, titleText: '시설'},
			        {startColumnName: 'LOS_OPERATE', numberOfColumns: 1, titleText: '운영'},
			        {startColumnName: 'LOS_SERVICE', numberOfColumns: 1, titleText: '서비스'},
			        {startColumnName: 'LOS_ENV', numberOfColumns: 1, titleText: '환경'},
			        {startColumnName: 'LOS_FINANCE', numberOfColumns: 1, titleText: '재정'},
			        {startColumnName: 'LOS_TOTAL', numberOfColumns: 1, titleText: '합계'},
			        {startColumnName: 'BRE_VALUE', numberOfColumns: 1, titleText: '(점)'},
			        {startColumnName: 'ASSET_USAGE_RATE', numberOfColumns: 1, titleText: '(%)'},
			        ]
			     });
			}
	}
	//숫자 포멧 + 합계
	let NumberFormat =  function( cellvalue , options , rowdata )  { 
		 let id = options.rowId;
		 let gridId = "#"+options.gid;
		 let thisName = options.colModel.name;
		 rowdata[thisName] = cellvalue;
		 let LOS_MANPOWER = Number(rowdata["LOS_MANPOWER"]);
 		 let LOS_FACILITY = Number(rowdata["LOS_FACILITY"]);
 		 let LOS_OPERATE = Number(rowdata["LOS_OPERATE"]);
 		 let LOS_SERVICE = Number(rowdata["LOS_SERVICE"]);
 	 	 let LOS_ENV = Number(rowdata["LOS_ENV"]);
 		 let LOS_FINANCE = Number(rowdata["LOS_FINANCE"]);
 		 let BRE_VALUE = Number(rowdata["BRE_VALUE"]);
	 	 let ASSET_USAGE_RATE = Number(rowdata["ASSET_USAGE_RATE"]);
	 	
	 	 rowdata["LOS_TOTAL"] = LOS_MANPOWER + LOS_FACILITY + LOS_OPERATE + LOS_SERVICE + LOS_ENV + LOS_FINANCE;
	 	 
 		jQuery(gridId).setRowData(id,{LOS_TOTAL: rowdata["LOS_TOTAL"]});
 		
      	return  cellvalue === null ? '' : $filter('number')(cellvalue,0);
	 }
	//숫자 포멧
	let intFormat =  function( cellvalue , options , rowdata )  { 
		return  cellvalue === null ? '' : $filter('number')(cellvalue,0);
	}
	//블록 포멧
	let blockFormat = function( cellvalue , options , rowdata )  { 
		if(cellvalue === '합계') return '합계';
		
		let Array = [];
		
		Array = $filter('filter')($scope.blockList, {BL_BCODE: cellvalue});
		
		let result = Array.length === 0 ? cellvalue :  Array[0].BL_NAME;
		
		return result ? result:'';
	}
	//level 포멧
	let levelFormat = function( cellvalue , options , rowdata )  { 
		if(cellvalue === '합계') return '합계';
		
		let Array = [];
		
		Array = $filter('filter')($scope.assetLevelList, {LEVEL_CD: cellvalue});
		
		let result = Array.length === 0 ? cellvalue :  Array[0].LEVEL_NM;
		
		return result ? result:'';
	}
	
    let grid = {
    	setGrid: function () {//리스트
    		return pagerJsonGrid({
                grid_id : 'list',
                pager_id : 'listPager',
                url : '/oip/getList.json',
                condition : {
                    page : 1,
                    rows : GridConfig.sizeL
                },
                shrinkToFit: false,
                rowNum : 20,
                colNames : [ 
                	'순번', '계획명','분석일자', '예산', '작성자', '작성일자'
                	, 'LOS_MANPOWER', 'LOS_FACILITY', 'LOS_OPERATE'
                	, 'LOS_SERVICE', 'LOS_ENV', 'LOS_FINANCE'
                	, 'LOS_TOTAL', 'BRE_VALUE', 'ASSET_USAGE_RATE'
                	,  'sid'
            	],
                colModel : [
                    { name : 'RNUM', width : "40px", resizable:false,},
                    { name : 'OIP_NM', width : 150, resizable:false, },
                    { name : 'OIP_DT', width : 60, resizable:false,},
                    { name : 'OIP_BUDGET_AMT', width : 60, formatter: NumberFormat, resizable: false,},
                    { name : 'INSERT_ID', width : 60, resizable:false,},
                    { name : 'INSERT_DT', width : 60, resizable:false,},
                    {name : 'LOS_MANPOWER', type: 'int', width : 20, hidden : true},
        			{name : 'LOS_FACILITY', ype: 'int', width : 20, hidden : true},
        			{name : 'LOS_OPERATE', type: 'int', width : 20, hidden : true},
        			{name : 'LOS_SERVICE', type: 'int', width : 20, hidden : true},
        			{name : 'LOS_ENV', type: 'int', width : 20, hidden : true},
        			{name : 'LOS_FINANCE', type: 'int', width : 20, hidden : true},
        			{name : 'LOS_TOTAL', type: 'int', width : 20, hidden : true},
        			{name : 'BRE_VALUE',width : 20, hidden : true},
        			{name : 'ASSET_USAGE_RATE',width : 20, hidden : true},
                    { name : 'OIP_SID', width : 80, hidden : true}
                ],
                onSelectRow : function(rowid, status, e) {
                	console.log($('#list').jqGrid('getRowData', rowid))
                    Common_Swap.B_A($('#list').jqGrid('getRowData', rowid), oip.Info);
                    $scope.$apply();
                    oip.Set();
                },
                loadComplete: function(){
					var grid = $('#list')
					var ids = grid.jqGrid("getDataIDs");
					if(ids && ids.length > 0) {
		    			grid.jqGrid("setSelection", ids[0]);
					}
					
					if(oip.isInit){
						Common_Swap.B_A($('#list').jqGrid('getRowData', ids[0]), oip.Info);
						oip.Init(oip.Info.OIP_SID, oip.Info.OIP_BUDGET_AMT);
					}
				}
            });
        },
        setGrid2: function (sid, amt) {//최적투자
            return pagerJsonGrid({
                grid_id : 'list2',
                pager_id : 'listPager2',
                url : '/oip/getOipOptimal.json',
                condition : {
                    page : 1,
                    OIP_BUDGET_AMT: amt,
                    OIP_SID: sid,
                    CLASS3_CD: '',
        			CLASS4_CD:'',
                    rows : GridConfig.sizeM
                },
                rowNum : 10,
                colNames : [ '순번', '자산코드', '자산명', 'RPN 점수', '우선순위','개대체방법', '개대체금액', '레벨3', '레벨4', '레벨5', '중블록', '소블록',
                	'OIP_SID','ASSET_SID'],
                	 colModel : [
                         { name : 'RNUM', width : "40px", resizable:false,},
                         { name : 'ASSET_CD', width : "200px", resizable:false,},
                         { name : 'ASSET_NM', width : 110, resizable:false,},
                         { name : 'RPN_SCORE', width : 80, resizable:false,},
                         { name : 'PRIORITY', width : 80, resizable:false,},
                         { name : 'MIN_NM', width : 80, resizable:false,},
                         { name : 'MIN_FACILITY_AMT', width : 80, formatter: intFormat, resizable:false,},
                         { name : 'CLASS3_CD', width : 80, resizable:false,},
                         { name : 'CLASS4_CD', width : 80, resizable:false,},
                         { name : 'CLASS5_CD', width : 80, resizable:false,},
                         { name : 'MED_BLOCK', width : 80, resizable:false,},
                         { name : 'SMALL_BLOCK', width : 80, resizable:false,},
                         { name : 'OIP_SID', width : 80 ,hidden : true},
                         { name : 'ASSET_SID', width : 80 ,hidden : true}
                     ],
                     ondblClickRow: function (rowid, iRow, iCol) {
                     	/*ordm.Set($('#list2').jqGrid('getRowData', rowid),true);*/
                     }
            });
        },
        setGrid3: function (sid) {//RPN
        	return pagerJsonGrid({
        		grid_id : 'list3',
        		pager_id : 'listPager3',
        		url : '/oip/getOipRPN.json',
        		condition : {
        			page : 1,
        			OIP_SID: sid,
        			rows : GridConfig.sizeS
        		},
        		rowNum : 10,
        		colNames : [ '순번','자산코드','자산명','10','10','10','5','5','10','50','30','30','RPN<br>점수','우선<br>순위',
        			'OIP_SID','ASSET_SID'],
        		colModel : [
        			{name : 'RNUM',width : 20, resizable:false,},
        			{name : 'ASSET_CD',width : 100, resizable:false,}, 
        			{name : 'ASSET_NM',width : 50, resizable:false,}, 
        			{name : 'LOS_MANPOWER', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_FACILITY', ype: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_OPERATE', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_SERVICE', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_ENV', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_FINANCE', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_TOTAL', type: 'int', width : 20, formatter: intFormat, resizable:false,}, 
        			{name : 'BRE_VALUE',width : 20, formatter: intFormat, resizable:false,}, 
        			{name : 'ASSET_USAGE_RATE',width : 20, formatter: intFormat, resizable:false,}, 
        			{name : 'RPN_SCORE', type: 'int', width : 20, resizable:false,}, 
        			{name : 'PRIORITY',width : 20, resizable:false,},
        			{name : 'OIP_SID',width : 20, hidden : true},
        			{name : 'ASSET_SID',width : 20, hidden : true},
        			],
        			loadComplete: function() {
        				let cheakList = RPNhaederPopup.Info;
        				let obj = oip.Info;
        				for(key in cheakList) {
        					$('#jqgh_list3_' + key).text(obj[key]);
        				}
        			}
        	});
        },
        setGrid4: function (sid) {//ORDM
        	return pagerJsonGrid({
        		grid_id : 'list4',
        		pager_id : 'listPager4',
        		url : '/oip/getORDMList.json',
        		condition : {
        			page : 1,
        			OIP_SID: sid,
        			rows : GridConfig.sizeS
        		},
        		rowNum : 10,
        		colNames : [
        			'순번','자산코드','자산명',
        			'시설비','운영비','유지비','총비용','연평균<br>(총비용)','현가','연평균<br>(현가)',
					'시설비','운영비','유지비','총비용','연평균<br>(총비용)','현가','연평균<br>(현가)',
					'시설비','운영비','유지비','총비용','연평균<br>(총비용)','현가','연평균<br>(현가)',
					'asset_sid','oip_sid'
    			],
        		colModel : [
        			{name : 'RNUM', width : 10, resizable:false,}, 
        			{name : 'ASSET_CD', width : 50, resizable:false,}, 
        			{name : 'ASSET_NM', width : 30, resizable:false,}, 
        			
        			{name : 'MNTN_FACILITY_AMT', width : 20,formatter: intFormat, resizable:false,}, 
        			{name : 'MNTN_OPERATE_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'MNTN_MAINTAIN_AMT', width : 20,formatter: intFormat, resizable:false,}, 
        			{name : 'MNTN_TOTAL_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'MNTN_TOTAL_AVG_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'MNTN_CURR_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'MNTN_CURR_AVG_AMT', width : 20,formatter: intFormat, resizable:false,},
        			
        			{name : 'RB_FACILITY_AMT', width : 20,formatter: intFormat, resizable:false,}, 
        			{name : 'RB_OPERATE_AMT', width : 20,formatter: intFormat, resizable:false,}, 
        			{name : 'RB_MAINTAIN_AMT', width : 20,formatter: intFormat, resizable:false,}, 
        			{name : 'RB_TOTAL_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'RB_TOTAL_AVG_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'RB_CURR_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'RB_CURR_AVG_AMT', width : 20,formatter: intFormat, resizable:false,},
        			
        			{name : 'REP_FACILITY_AMT', width : 20,formatter: intFormat, resizable:false,}, 
        			{name : 'REP_OPERATE_AMT', width : 20,formatter: intFormat, resizable:false,}, 
        			{name : 'REP_MAINTAIN_AMT', width : 20,formatter: intFormat, resizable:false,}, 
        			{name : 'REP_TOTAL_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'REP_TOTAL_AVG_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'REP_CURR_AMT', width : 20,formatter: intFormat, resizable:false,},
        			{name : 'REP_CURR_AVG_AMT', width : 20,formatter: intFormat, resizable:false,},
        			
        			{name : 'OIP_SID',width : 20, hidden : true},
        			{name : 'ASSET_SID',width : 20, hidden : true}
        			],
        			ondblClickRow: function (rowid, iRow, iCol) {
                    	ordm.Set($('#list4').jqGrid('getRowData', rowid), true);
                    },
        			loadComplete: function(){
        				
        			}
        	});
        },
        setGrid5: function (sid) {//new 최적투자
        	return pagerJsonGrid({
        		grid_id : 'list5',
        		pager_id : 'listPager5',
        		url : '/oip/getOipRPN.json',
        		condition : {
        			page : 1,
        			OIP_SID: sid,
        			rows : GridConfig.sizeXL//GridConfig.sizeS
        		},
        		rowNum : 10,
        		cellEdit : true,
        		colNames : [ '순번','자산코드','자산명','10','10','10','5','5','10','50','30','30','RPN<br>점수','우선<br>순위','필수<br>대상<br>체크',
        			'OIP_SID','ASSET_SID'],
        		colModel : [
        			{name : 'RNUM',width : "30px", resizable:false,},
        			{name : 'ASSET_CD',width : "150px", resizable:false,}, 
        			{name : 'ASSET_NM',width : 50, resizable:false,}, 
        			{name : 'LOS_MANPOWER', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_FACILITY', ype: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_OPERATE', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_SERVICE', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_ENV', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_FINANCE', type: 'int', width : 20, editable : true, formatter: NumberFormat, resizable:false,}, 
        			{name : 'LOS_TOTAL', type: 'int', width : 20, resizable:false,}, 
        			{name : 'BRE_VALUE',width : 20, resizable:false,}, 
        			{name : 'ASSET_USAGE_RATE',width : 20, resizable:false,}, 
        			{name : 'RPN_SCORE', type: 'int', width : 20, resizable:false,}, 
        			{name : 'PRIORITY',width : 20, resizable:false,},
        			{name : 'ESSENTIAL_YN',width : 20, resizable:false,},
        			{name : 'OIP_SID',width : 20, hidden : true},
        			{name : 'ASSET_SID',width : 20, hidden : true},
        			],
        			loadComplete: function() {
        				var ids = jQuery("#list5").getDataIDs();
        				let info = null;
        				for(var i=0;i<ids.length;i++){        				
        					info = $('#list5').jqGrid('getRowData',ids[i]);
        					if(!info.RNUM) break;
        					if(info.ESSENTIAL_YN)
        						jQuery("#list5").setRowData(ids[i],{ESSENTIAL_YN :'<div class = "sendbuttons"  ><input name="userInput" type="checkbox" checked value="'+info.ESSENTIAL_YN +'" ng-click="oipNew.InputBox(\''+ids[i]+'\',\''+i+'\')"></div>'});
        					else
        						jQuery("#list5").setRowData(ids[i],{ESSENTIAL_YN :'<div class = "sendbuttons"  ><input name="userInput" type="checkbox" value="'+info.ESSENTIAL_YN +'" ng-click="oipNew.InputBox(\''+ids[i]+'\',\''+i+'\')"></div>'});
        						
        				}
        				$compile(angular.element(".sendbuttons").contents())($scope);
        				$("#loadingSpinner").hide();
      				},
      				afterEditCell : function(id,name,val,iRow,iCol) {
		            	$("#"+iRow+"_"+name).bind('blur', function(e){
		                     $('#list5').saveCell(iRow,iCol);
		                 });
		            }
        	});
        },
        setGrid6: function (sid) {//new RPN
    		return pagerJsonGrid({
                grid_id : 'list6',
                pager_id : 'listPager6',
                url : '/oip/getOipOptimalNew.json',
                condition : {
                    page : 1,
                    OIP_BUDGET_AMT: 0,
                    OIP_SID: sid,
                    rows : GridConfig.sizeXL//GridConfig.sizeS
                },
                rowNum : 10,
                colNames : [ '순번', '자산코드', '자산명', 'RPN 점수', '우선순위','개대체방법', '개대체금액', '레벨3', '레벨4', '레벨5', '중블록', '소블록',
                	'OIP_SID','ASSET_SID'],
                colModel : [
                    { name : 'RNUM', width : "30px", resizable:false,},
                    { name : 'ASSET_CD', width : "180px", resizable:false,},
                    { name : 'ASSET_NM', width : 100, resizable:false,},
                    { name : 'RPN_SCORE', width : 80, resizable:false,},
                    { name : 'PRIORITY', width : 80, resizable:false,},
                    { name : 'MIN_NM', width : 80, resizable:false,},
                    { name : 'MIN_FACILITY_AMT', width : 80, formatter: NumberFormat, resizable:false,},
                    { name : 'CLASS3_CD', width : 80, resizable:false,},
                    { name : 'CLASS4_CD', width : 80, resizable:false,},
                    { name : 'CLASS5_CD', width : 80, resizable:false,},
                    { name : 'MED_BLOCK', width : 80, resizable:false,},
                    { name : 'SMALL_BLOCK', width : 80, resizable:false,},
                    { name : 'OIP_SID', width : 80 , hidden : true},
                    { name : 'ASSET_SID', width : 80 , hidden : true}
                ],
                ondblClickRow: function (rowid, iRow, iCol) {
                	ordm.Set($('#list6').jqGrid('getRowData', rowid), false);
                },
                loadComplete: function() {
                	$("#loadingSpinner").hide();
                }
            });
        },
    }
    
    //(String)1,000-> (int)1000
    let StringToNumber = function(str){
    	if(!str)return undefined ;
		let numArray = str.toString().split(',');
		let strTemp ='';
		for(j in numArray) {
			strTemp += numArray[j];
		}
    	return Number(strTemp);
    }
    //col 합계 평균
    let colSum_Avg = function(info, index){
    	//숫자 인지 문자인지 판당해서 문자이면 변경이 안된것으로 판단
    	if(!info[index])return;
    	if(isNaN(info[index]))return;
    	//숫자를  문자형으로 변경 1000-> "1,000"
    	info[index] = $filter('number')(info[index],0);
    	//리스트 가로 사이즈를 가져옴
		let sizeList = ordm.SizeList;
		let sum = 0;
		let maxCol=0;
		let value = 0;
		//col 계산 
		for(let i=1; i < sizeList.length +1; i++){
			if(info[i] === null)continue;
			maxCol = i;
			value = StringToNumber(info[i]); 
			sum += value? value : 0;
		}
		//값 저장
		info.SUM = sum;
		info.AVG = sum/maxCol;
    }
    //로우 합계
    let rowSum = function(index, index2){
    	//전체 리스트를 가져옴
    	let list = ordm.List;
    	//인덱스를 확인함
    	let rowIndex = (index2 - 11) * 5;
    	let colIndex = index;
    	//널이면 계산하지 않음
    	if(list[rowIndex][colIndex] === null) return;
    	//row 계산
    	let total = 0;
    	let value = 0;
    	for(let i =0; i < 3; i++){
    		value = StringToNumber(list[rowIndex + i][colIndex]); 
    		total += value ? value : 0;
    	}
    	//값을 저장
    	//총 합계
    	list[rowIndex + 3][colIndex] = total;//$filter('number')(total, 0);
    	colSum_Avg(list[rowIndex + 3], index);
    	//현가 합계    	
    	let presentValue = total/Math.pow((1+dscntTemp.Info.DSCNT),colIndex-1);
    	list[rowIndex + 4][colIndex] = presentValue//$filter('number')(presentValue, 0);
    	colSum_Avg(list[rowIndex + 4], index);
    }
    
    let oip = {
    		Page: 0,
    		Info:{
    			OIP_SID: '',
    			OIP_NM: '',
    			OIP_DT: '',
    			OIP_BUDGET_AMT: '',
    			OIP_BUDGET_AMT_VIW: '',
    			LOS_MANPOWER: '',
    			LOS_FACILITY: '',
				LOS_OPERATE: '',
				LOS_SERVICE: '',
				LOS_ENV: '',
				LOS_FINANCE: '',
				LOS_TOTAL: '',
				BRE_VALUE: '',
				ASSET_USAGE_RATE: '',
    			INSERT_ID: '',
    			INSERT_DT: '',
    			CLASS3_CD: '',
    			CLASS4_CD:''
    		},
    		FList: null,
    		FTotal: null,
    		NList:  null,
    		NTotal: null,
    		isInit: true,
    		Init: function(sid, amt){
    			this.isInit = false;
    			mainDataService.getAssetLevelList({}).success(function(data){
    	    		 //class_cd 정보
    	 	 		 $scope.assetLevelList  = data
    	 	 		 mainDataService.getBlockCodeList({}).success(function (obj) {
    	 	  			if (!common.isEmpty(obj.errorMessage)) {
    	 	  				alertify.error('errMessage : ' + obj.errorMessage);
    	 	  				return;
    	 	  			}
    	 	  			//대,중,소 정보
    	  				$scope.blockList = obj;
    	  				
    	  				grid.setGrid2(sid, amt);
    		    	    grid.setGrid3(sid);
    		    	    grid.setGrid4(sid);
    		    	    //헤더 정리
    			    	GridHeader.setGrid3();
    			    	GridHeader.setGrid4();
    	 	  		});
    	 	 	 });
	    		
    		},
    		Set: function() {
    			this.SetFN('N', function(array) {oip.NList = array;});
    			
    			this.SetFN('F', function(array) {
    				oip.FList = array.slice(0, (array.length-1));
    				oip.FTotal = array[array.length-1];
    			});
    			
    			$('#list2').jqGrid('setGridParam', {postData : this.Info, page : 1}).trigger('reloadGrid');
    			$('#list3').jqGrid('setGridParam', {postData : this.Info, page : 1}).trigger('reloadGrid');
    			$('#list4').jqGrid('setGridParam', {postData : this.Info, page : 1}).trigger('reloadGrid');
    		},
    		SetFN: function(FN, Setlist) {
    			let info = {
					OIP_SID: this.Info.OIP_SID, 
					OIP_BUDGET_AMT: this.Info.OIP_BUDGET_AMT, 
					CLASS3_CD: FN
				}
        		mainDataService.CommunicationRelay("/oip/getFNList.json", info).success(function(array){
        			Setlist(array);
        		});
        	},
    		New: function(){
        		oipNew.Init();
        	},
        	Report: function(){
        		 let jobType = $state.current.name + "_big"
    			 console.log(jobType);
    			 //Common_Excel.ExcelDown(jobType, this.Info);
    			 window.open('/bigexcel/downloadBigExcel.do?EXCEL_ID='+jobType+'&OIP_SID=' + this.Info.OIP_SID,'_blank');
    			 
        		/*let jobType = $state.current.name+ "_" + 2;
				Common_Excel.ExcelDown(jobType, this.Info);*/
        	},
	 		Check: function() {
	 			$('#list2').jqGrid('setGridParam', {postData : oip.Info, page : 1}).trigger('reloadGrid');
	 		},
	 		ModalSet: function(page){
	 			oip.Page = page;
	 		}
        }
        
    let oipNew = {
    		Page:0,
    		Info: {
    			OIP_NM:'',
    			OIP_DT:'',
    			OIP_BUDGET_AMT: 0,
    			OIP_SID:'',
    			INSERT_ID:'',
    			INSERT_DT:''
    		},
    		isFirst: true,
    		Init: function() {
    			mainDataService
	    			.CommunicationRelay("/oip/insertOIP.json")
	    			.success(function(info) {
	    				oipNew.Info = info;
	    				oipNew.Info.OIP_DT = '';
	    				oipNew.Info['Changed'] = false; 
	    				oipNew.Page = 0;
	    				
	    				if(oipNew.isFirst){
	    					//신규 그리드 생성
	        			    grid.setGrid5(oipNew.Info.OIP_SID);
	        			    GridHeader.setGrid5();
	        			    grid.setGrid6(oipNew.Info.OIP_SID);
	        			    oipNew.isFirst = false;
	        			    
	    				}
	    				else{
	    					oipNew.RPNtrigger(oipNew.Info.OIP_SID);
	    					oipNew.CostTrigger(oipNew.Info.OIP_SID, 0);
	    				}
	    				oipNew.isShow = true;
	    			});
    			
    			//$("#loadingSpinner").show();
    			/*mainDataService.CommunicationRelay("/oip/newOip.json").success(function(info) {
    				console.log(info);
    				
    				oipNew.Info = info;
    				oipNew.Info['Changed'] = false; 
    				oipNew.Page = 0;
    				
    				if(oipNew.isFirst){
    					//신규 그리드 생성
        			    grid.setGrid5(oipNew.Info.OIP_SID);
        			    GridHeader.setGrid5();
        			    grid.setGrid6(oipNew.Info.OIP_SID);
        			    oipNew.isFirst = false;
        			    
    				}
    				else{
    					oipNew.RPNtrigger(oipNew.Info.OIP_SID);
    					oipNew.CostTrigger(oipNew.Info.OIP_SID, 0);
    				}
    				oipNew.isShow = true;
    			});*/
    		},
    		InputBox: function(id,i) {
    			let info = $('#list5').jqGrid('getRowData',id);
    			let val = $(".sendbuttons input[name='userInput']").eq(i).val();
    			$(".sendbuttons input[name='userInput']").eq(i).val(val==1?0:1);
    		},
    		ApplyAsset: function() {
    			let ids = jQuery("#list5").getDataIDs();
    			let input = $(".sendbuttons input[name='userInput']");
    			let list = [];
    			let info = null;
    			//0 번째 로우넘이 없는 경우 새로만들기
    			if(this.assetInit()) return;
    			
    			for(var i=0;i<ids.length;i++) {
    				//로우넘이 없으면 탈출
    				info = $('#list5').jqGrid('getRowData',ids[i]);
    				if(!info.RNUM) break;
    				info.ESSENTIAL_YN = input.eq(i).val();
    				list.push(info);
    			}
    			
    			if(this.Info.OIP_BUDGET_AMT){
    				this.Info.OIP_BUDGET_AMT = Common_sts(this.Info.OIP_BUDGET_AMT.toString());
    			}
    			
    			mainDataService
	    			.CommunicationRelay("/oip/applyOipAsset.json",{List: list, Info: this.Info})
	    			.success(function(info) {
	    				if(Common_AlertStr(info)) return;
	    				
	    				oipNew.RPNtrigger(oipNew.Info.OIP_SID);
	    				oipNew.CostTrigger(oipNew.Info.OIP_SID, oipNew.Info.OIP_BUDGET_AMT);
	    				
	    				let value = $filter('number')(oipNew.Info.OIP_BUDGET_AMT,0);
	    				oipNew.Info.OIP_BUDGET_AMT = value === null ? '' : value;
	    			});
    		},
    		ApplyCost: function() {
    			if(!this.assetInit(this.updateCost)) {
    				this.updateCost();
    			}
    		},
    		assetInit: function(delegate) {
    			$("#loadingSpinner").show();
    			let ids = jQuery("#list5").getDataIDs();
    			let info = $('#list5').jqGrid('getRowData',ids[0]);
    			if(!info.RNUM) {
					this.Analysis(delegate);
					return true;
				}
    			return false;
    		},
    		Blur: function(){
    			let str = Common_sts(oipNew.Info.OIP_BUDGET_AMT.toString());
    			let value = $filter('number')(str, 0);
				oipNew.Info.OIP_BUDGET_AMT = value === null ? '' : value;
    		},
    		updateCost: function() {
    			if(!oipNew.Info.OIP_BUDGET_AMT || oipNew.Info.OIP_BUDGET_AMT === '') {
    	 			oipNew.Info.OIP_BUDGET_AMT = 0;
    	 		}
    			this.Info.OIP_BUDGET_AMT = Common_sts(oipNew.Info.OIP_BUDGET_AMT.toString());
    			mainDataService
	    			.CommunicationRelay("/oip/applyOipAssetCost.json", this.Info)
	    			.success(function(info) {
	    				if(Common_AlertStr(info)) return;
	    				
	    				oipNew.CostTrigger(oipNew.Info.OIP_SID, oipNew.Info.OIP_BUDGET_AMT);
	    				
	    				let value = $filter('number')(oipNew.Info.OIP_BUDGET_AMT,0);
	    				oipNew.Info.OIP_BUDGET_AMT = value === null ? '' : value;
	    			});
    		},
    		Analysis: function(delegate) {
    			if(!oipNew.Info.OIP_DT || oipNew.Info.OIP_DT =='' || oipNew.Info.OIP_DT =='--') {
    				alertify.alert('','분석일은 필수 입니다.');
    				$("#loadingSpinner").hide();
    				return;
    			}
    			
    			if(this.Info.OIP_BUDGET_AMT){
    				this.Info.OIP_BUDGET_AMT = Common_sts(this.Info.OIP_BUDGET_AMT.toString());
    			}
    			
    			mainDataService
	    			.CommunicationRelay("/oip/analysisOIP.json", oipNew.Info)
	    			.success(function(info) {
	    				$('#list5').jqGrid('setGridParam', oipNew.Info).trigger('reloadGrid');
	    				
	    				if(delegate) delegate();
	    			});
    		},
    		/*ApplyDT: function() {
    	 		$("#loadingSpinner").show();
    			mainDataService
	    			.CommunicationRelay("/oip/applyOipAssetDT.json",this.Info)
	    			.success(function(info) {
	    				if(Common_AlertStr(info)) return;
	    				
	    				oipNew.RPNtrigger(oipNew.Info.OIP_SID);
	    				
	    				$("#loadingSpinner").hide();
	    			});
    		},*/
    		RPNtrigger: function(sid) {
    			$('#list5').jqGrid('setGridParam', {postData : {
    				 page : 1,
    				 OIP_SID: sid,
    				 rows : GridConfig.sizeXL
      		 	},
      		 	page : 1
      		 	}).trigger('reloadGrid');
    		},
    		CostTrigger:function(sid,cost) {
    			$('#list6').jqGrid('setGridParam', {postData : {
    				 page : 1,
    				 OIP_SID: sid,
    				 OIP_BUDGET_AMT: cost,
    				 rows : GridConfig.sizeXL
     		 	}, page : 1}).trigger('reloadGrid');
    		},
    		
    		Save: function() {
    			oipNew.Info.OIP_NM = xssFilter(oipNew.Info.OIP_NM);
    			if(oipNew.Info.OIP_NM === ''|| oipNew.Info.OIP_NM === null|| oipNew.Info.OIP_NM === undefined){
    				alertify.alert('','계획명은 필수 입니다.');
    				return;
    			}
    			if(oipNew.Info.OIP_BUDGET_AMT === ''|| oipNew.Info.OIP_BUDGET_AMT === null|| oipNew.Info.OIP_BUDGET_AMT === undefined){
    				alertify.alert('','예산은 필수 입니다.');
    				return;
    			}
    			$("#loadingSpinner").show();
    			this.Info.OIP_BUDGET_AMT = Common_sts(this.Info.OIP_BUDGET_AMT.toString());
    			//수정 전
    			mainDataService
	    			.CommunicationRelay("/oip/saveOipAsset.json",this.Info)
	    			.success(function(info) {
	    				if(Common_AlertStr(info)) return;
	    				
	    				oipNew.CostTrigger(oipNew.Info.OIP_SID, oipNew.Info.OIP_BUDGET_AMT);
	    				oipNew.isShow = false;
	    				$('#list').jqGrid('setGridParam', {postData : {page : 1, rows : GridConfig.sizeL}, page : 1}).trigger('reloadGrid');
	    				$("#loadingSpinner").hide();
	    				
	    				let value = $filter('number')(oipNew.Info.OIP_BUDGET_AMT,0);
	    				oipNew.Info.OIP_BUDGET_AMT = value === null ? '' : value;
	    			});
    		},
    		RPNweight: function() {
    			RPNhaederPopup.Init(this.Info);
    		}
    }
    
    let RPNhaederPopup = {
    		Info:{
    			LOS_MANPOWER:'10',
    			LOS_FACILITY:'10',
    			LOS_OPERATE:'10',
    			LOS_SERVICE:'5',
    			LOS_ENV:'5',
    			LOS_FINANCE:'10',
    			LOS_TOTAL:'50',
    			BRE_VALUE:'30',
    			ASSET_USAGE_RATE:'30'
    		},
    		orInfo:{},
    		isShow: false,
    		Init: function(obj) {
    			let info = this.Info; 
    			let orInfo = this.orInfo;
    			let str = '';
    			for(key in info) {
    				str = obj[key].toString();
    				orInfo[key] = str;
    				info[key] = str;
    			}
    			
    			this.isShow = true;
    		},
    		Reset: function() {
    			this.orInfo = {};
    			this.isShow = false;
    		},
    		Cancel: function(){
    			this.Info = this.orInfo;
    			this.orInfo = {};
    			this.isShow = false;
    		},
    		Blur: function(index) {// 숫자 유효성
    			let info = this.Info;
    			let weight  = Number(info[index]); 
    			info[index] = $filter('number')(info[index], 0);
    			
    			//min max 기본 값으로 입력
				if(isNaN(weight)) info[index] = weight = $filter('number')(1, 0);  
    			if(weight < 1) info[index] = $filter('number')(1, 0);
    			if(weight > 100) info[index] = $filter('number')(100, 0);
    			
    			let sum = 0;
    			for(key in info) {
    				if(key == 'LOS_TOTAL' || key == 'BRE_VALUE' || key == 'ASSET_USAGE_RATE') continue;
    				
    				sum += Number(info[key]);
    			}
    			info['LOS_TOTAL'] = $filter('number')(sum, 0);
    		},
    		Save: function(param) {//저장
    			//value 전달 방식
    			let obj = this.Info;
    			let orInfo = this.orInfo;
    			for(key in obj) {
    				//변경 이력 검사
					if(orInfo[key] == obj[key]) continue;
					
					//값 변경
					param[key] = obj[key];
    				
    				//헤더 값 변경
    				$('#jqgh_list5_' + key).text(obj[key]);
    				
    				//param 수정
    				if(!param['Changed']) param['Changed'] = true; 
    			}
    			//초기화
    			this.Reset();
    		}
    }
    
    let ordm = {
		Info: {
			ASSET_CD:'',
			ASSET_NM:'',
			OIP_SID: 0,
			ASSET_SID: 0,
		},
		List: null,
		isReadonly: false,
		SizeList: null,
		Set: function(info, b) {
			if(info.ASSET_CD == '') return;
			$("#loadingSpinner").show();
			this.Info['isReadonly'] = b;
			this.isReadonly = b;
			dscntTemp.Init(dscnt.List); 
			Common_Swap.B_A(info, this.Info);
			mainDataService.CommunicationRelay("/oip/getORDMAssetList.json",this.Info).success(function(map) {
				ordm.List = map.List;
				ordm.SizeList = map.size.split(',');
				$("#loadingSpinner").hide();
				 //$scope.$apply();
				ordm.isShow = true;
			});
		},
		Blur: function(info, index, index2){
			if(this.isReadonly){
				if(isNaN(info[index])) return;
				info[index] = $filter('number')(info[index],0);
				return;
			}
			
			rowSum(index, index2);
			colSum_Avg(info, index);
		},
		DscntTemp: function(b) {
			dscntTemp.Set(dscntTemp.List);
			dscntTemp.isShow = true;
		},
		Reset: function() {
			this.Set(this.Info, false);
		},
		Save: function() {
			//sql 쿼리 양식만들기
			let select = '';
			let piovtIn ='';
			for(let i=1; i < this.SizeList.length +1; i++){
				select += "* AS \""+i+"\","
				piovtIn +="\""+i+"\","
			}
			select = select.substring(0,select.length-1);
			piovtIn = piovtIn.substring(0,piovtIn.length-1);
			//리스트 문자형에서 인트형으로 형변환 1,000 -> 1000
			let list = this.List;
			let SizeIndex = this.SizeList;
			let value = 0;
			for(i in list){
				for(let j = 1; j < SizeIndex.length + 1; j++){
					value = StringToNumber(list[i][j]);
					list[i][j] = value ? value : 0;
				}
			}
			//서비스 호출
			let info = {
				OIP_SID: this.Info.OIP_SID,
				ASSET_SID: this.Info.ASSET_SID,
				select: select,
				piovtIn: piovtIn
			}
			mainDataService
				.CommunicationRelay("/oip/saveORDMAssetList.json",{Info: info, List: this.List})
				.success(function(map) {
				ordm.isShow = false;
			});
		}
    }
    
    let dscntTemp = {
    	Info: {},
    	List: null,
    	BackUpList: null,
    	Init: function(list){
    		dscntTemp.List = angular.copy(list).slice(0,10);
    		dscntTemp.Info = {};
    		dscntTemp.Set(dscntTemp.List);
    	},
    	Set: function(list) {
    		dscntTemp.BackUpList = angular.copy(list);
    		let PRICE = 0;
    		let NOMINAL = 0;
    		let DSCNT = 0;
    		let value = 0;
    		for(i in list){
    			value = StringToNumber(list[i].PRICE_RAISE_RATIO);
    			PRICE += value ? value : 0;
    			value = StringToNumber(list[i].NOMINAL_INTEREST_RATIO);
    			NOMINAL += value ? value : 0;
    			value = StringToNumber(list[i].PRICE_RAISE_RATIO);
    			DSCNT += value ? value : 0;
    		}
    		dscntTemp.Info.PRICE =  $filter('number')(PRICE / 10, 3);  
    		dscntTemp.Info.NOMINAL =  $filter('number')(NOMINAL / 10, 2);
    		dscntTemp.Info.DSCNT = $filter('number')(DSCNT / 10, 3);
    	},
    	Blur: function(index){
			let info = this.List[index];
			info.PRICE_RAISE_RATIO = Number(info.PRICE_RAISE_RATIO).toFixed(3);
			info.NOMINAL_INTEREST_RATIO = Number(info.NOMINAL_INTEREST_RATIO).toFixed(2);
		},
		Change: function(index){
			let info = this.List[index];
			info.REAL_DSCNT_RATIO = (1+Number(info.NOMINAL_INTEREST_RATIO))/(1+Number(info.PRICE_RAISE_RATIO)) -1;
			info.REAL_DSCNT_RATIO = info.REAL_DSCNT_RATIO.toFixed(3);
			this.Set(this.List);
		},
		Save: function(){
			let list = ordm.List;
			for(let i=1; i < ordm.SizeList.length +1; i++){
				rowSum(i,'11');
				rowSum(i,'12');
				rowSum(i,'13');	
			}	
			dscntTemp.isShow = false;
		},
		Cancel: function(){
			dscntTemp.List = dscntTemp.BackUpList;
			dscntTemp.isShow = false;
		}
    }
    
    let dscnt = {
		Info:{
			DSCNT_NM: '',
			INSERT_ID: '',
			INSERT_DT: '',
			UPDATE_ID: '',
			UPDATE_DT: ''
		},
		List: [],
		Set: function(){
			mainDataService
				.CommunicationRelay("/oip/getDscntList.json")
				.success(function(map){
					dscnt.Info = map.Info;
					dscnt.List = map.List;
				});
		},
		Modified: function(){
			$rootScope.$broadcast("dscntPopup", {
				Info: this.Info,
				List: this.List,
				Triger: this.Set
			});
		}
	}
   
    $scope.Serch = Serch;
    $scope.dscnt = dscnt;
    $scope.oip = oip;
    $scope.oipNew = oipNew;
    $scope.RPNhaederPopup = RPNhaederPopup;
    $scope.ordm = ordm;
    $scope.dscntTemp = dscntTemp;
	$scope.Page = 0;
}

function dscntController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, MaxUploadSize) {
	$scope.$on("dscntPopup", function(e, data){
		dscntPopup.Info = angular.copy(data.Info);
		dscntPopup.List = angular.copy(data.List);
		Triger = data.Triger;
		dscntPopup.isShow = true;
	});
	let Triger = null;
	let dscntPopup = {
		isShow: false,
		List: null,
		Info: {
			DSCNT_NM: '',
			UPDATE_ID: '',
			UPDATE_DT: ''
		},
		Change: function(index){
			let info = this.List[index];
			info.REAL_DSCNT_RATIO = (1+Number(info.NOMINAL_INTEREST_RATIO))/(1+Number(info.PRICE_RAISE_RATIO)) -1;
			info.REAL_DSCNT_RATIO = info.REAL_DSCNT_RATIO.toFixed(3);
			
		},
		Blur: function(index){
			let info = this.List[index];
			info.PRICE_RAISE_RATIO = Number(info.PRICE_RAISE_RATIO).toFixed(3);
			info.NOMINAL_INTEREST_RATIO = Number(info.NOMINAL_INTEREST_RATIO).toFixed(2);
		},
		Delete: function(){
			this.List.shift();
		},
		Add: function(){
			let list = this.List;
			let yyyy = list.length === 0 ? this.Info.INSERT_DT.substr(0,4) : Number(list[0].YYYY) + 1;
			list.unshift({YYYY: yyyy, PRICE_RAISE_RATIO:"0.000", NOMINAL_INTEREST_RATIO: "0.00", REAL_DSCNT_RATIO: "0.000"})
		},
		Output: function(){
				let header = []
				header.push({"YYYY":"YYYY", "PRICE_RAISE_RATIO":"PRICE_RAISE_RATIO", "NOMINAL_INTEREST_RATIO":"NOMINAL_INTEREST_RATIO", "REAL_DSCNT_RATIO":"REAL_DSCNT_RATIO"});
	    		const workSheet = XLSX.utils.json_to_sheet(header.concat(this.List),{ header : ["YYYY", "PRICE_RAISE_RATIO", "NOMINAL_INTEREST_RATIO", "REAL_DSCNT_RATIO"], skipHeader : true });
	    		const workBook = XLSX.utils.book_new();
	    		workSheet["!cols"]=[{wpx:30},{wpx:130},{wpx:230},{wpx:130}]
	    		XLSX.utils.book_append_sheet(workBook, workSheet, '할인율');
	    		XLSX.writeFile(workBook, '실질할인율_양식_' + Common_Time.reportDay() + '.xlsx');
		},
		OnFileSelect : function($files, cmd){
			if(FileCheck($files, cmd, MaxUploadSize)) return;
			var reader = new FileReader();
			reader.onload = function(e) {
				const workBook =XLSX.read(reader.result, {type :'binary'});
				workBook.SheetNames.forEach(sheetName => {
					const rows =XLSX.utils.sheet_to_json(workBook.Sheets[sheetName]);
					dscntPopup.List = rows
					console.log(rows)
					$scope.$apply();
				});
			};
			reader.readAsBinaryString($files[0]);
		},
		Save: function(){
			mainDataService
				.CommunicationRelay("/oip/saveDscnt.json",{List: this.List, DSCNT_NM: this.Info.DSCNT_NM})
				.success(function(map) {
					if(Common_AlertStr(map)) return;
					
					Triger();
					
					dscntPopup.isShow = false;
				});
		},
		Cancel: function(){
			this.isShow = false;
		}
	}
	
	$scope.dscntPopup = dscntPopup;
}

