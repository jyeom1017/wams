angular.module('app.mypage').controller('MyPageController',MyPageController);

function MyPageController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	
	$scope.UserInfo2 = {};
	
	$scope.$on("popupMyPasswordChange",function(event,data){
		$("#myPasswordChangePopup").show();
		$scope.UserInfo2 = {E_PW:'',E_PW_NEW:'',E_PW_CONFIRM:''};
	});
	
	
	
	//비밀번호
	var chkkin4 = function (str, max){
        if(!max) max = 3; // 글자수를 지정하지 않으면 4로 지정
        var i, j, k, x, y;
        var buff = ["0123456789", "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
        var src, src2, ptn="";

        for(i=0; i<buff.length; i++){
            src = buff[i]; // 0123456789
            src2 = buff[i] + buff[i]; // 01234567890123456789
            for(j=0; j<src.length; j++){
                x = src.substr(j, 1); // 0
                y = src2.substr(j, max); // 0123
                ptn += "["+x+"]{"+max+",}|"; // [0]{4,}|0123|[1]{4,}|1234|...
                ptn += y+"|";
            }
        }
        ptn = new RegExp(ptn.replace(/.$/, "")); // 맨마지막의 글자를 하나 없애고 정규식으로 만든다.

        if(ptn.test(str)) return true;
        return false;
    }
    
	function passValidation(){
		if($scope.UserInfo2.E_PW==''){
			alertify.alert('기존 비밀번호를 입력하세요');
			return false;
		}
		if($scope.UserInfo2.E_PASS_NEW==''){
			alertify.alert('변경할 비밀번호를 입력하세요');
			return false;
		}
		
		if($scope.UserInfo2.E_PASS_CONFIRM==''){
			alertify.alert('변경할 비밀번호를 입력하세요');
			return false;
		}
		
		if($scope.UserInfo2.E_PW == $scope.UserInfo2.E_PASS_NEW){
			alertify.alert('새로운 비밀번호를 입력하세요');
			return false;
			}
		
		if($scope.UserInfo2.E_PASS_NEW != $scope.UserInfo2.E_PASS_CONFIRM ){
			alertify.alert('비밀번호가 일치하지 않습니다');
			return false;
			}
		
		if($scope.UserInfo2.E_PASS_NEW == sessionStorage.getItem("loginId") ){
			alertify.alert('','아이디와 동일한 비밀번호를 사용할 수 없습니다.');
			return false;
			}
		
            var requestInfo = {};
            requestInfo.password = $scope.UserInfo2.E_PASS_NEW || '';
            
            if(requestInfo.password.length == 0) {
                //$('.div_pwd_msg').show();
                return('비밀번호를 입력해주세요.');
                $('[name=loginPw]').focus();
                return false;
            }
            var num = requestInfo.password.search(/[0-9]/g);
            var eng = requestInfo.password.search(/[a-z]/ig);
            var spe = requestInfo.password.search(/[`~!@@#$%^&*|\\\'\";:\/?,.]/gi);
            var speWord = ['password', 'iloveyou', 'letmein', 'qwerty', 'trustno1', 'qazwsx'];
			var reg1 = /[A]{3}|[B]{3}|[C]{3}|[D]{3}|[E]{3}|[F]{3}|[G]{3}|[H]{3}|[I]{3}|[J]{3}|[K]{3}|[L]{3}|[M]{3}|[N]{3}|[O]{3}|[P]{3}|[Q]{3}|[R]{3}|[S]{3}|[T]{3}|[U]{3}|[V]{3}|[W]{3}|[X]{3}|[Y]{3}|[Z]{3}/
			var reg2 = /[a]{3}|[b]{3}|[c]{3}|[d]{3}|[e]{3}|[f]{3}|[g]{3}|[h]{3}|[i]{3}|[j]{3}|[k]{3}|[l]{3}|[m]{3}|[n]{3}|[o]{3}|[p]{3}|[q]{3}|[r]{3}|[s]{3}|[t]{3}|[u]{3}|[v]{3}|[w]{3}|[x]{3}|[y{3}|[z]{3}/
			var reg3 = /[1]{3}|[2]{3}|[3]{3}|[3]{3}|[5]{3}|[6]{3}|[7]{3}|[8]{3}|[9]{3}|[0]{3}/
			var reg4 = /[!]{4}|[@]{3}|[#]{3}|[\$]{3}|[%]{3}|[\^]{3}|[&]{3}|[*]{3}|[(]{3}|[)]{3}|[-]{3}|[+]{3}|[=]{3}|[_]{3}|[|]{3}|[<]{3|[>]{3}}|[\/]{3}|[\"]{4}|[\']{4}|[:]{4}|[;]{4}|[~]{4}|[`]{4}/

            var regexpN = /^[0-9]*$/
            var spcArr = ['<','>','(',')','#','\'','"','/','|','_','-','=','+','\\',';', ' ', '[', ']','{','}'];

            if(chkkin4(requestInfo.password)){
                //$('.div_pwd_msg').show();
                var pwdMessage = '동일한 문자 또는 숫자를 연속해서 3개 이상 사용할 수 없습니다. ';
                return(pwdMessage);
                $('[name=loginPw]').focus();
                return false;
            }
            if(reg1.test(requestInfo.password) || reg2.test(requestInfo.password) || reg3.test(requestInfo.password) || reg4.test(requestInfo.password)){
                //$('.div_pwd_msg').show();
                var pwdMessage = '동일한 문자 또는 숫자를 연속해서 3개 이상 사용할 수 없습니다. ';
                return(pwdMessage);
                $('[name=loginPw]').focus();
                return false;
            }


            for(var i=0;i<spcArr.length;i++){
               var spc = spcArr[i];
               if(requestInfo.password.indexOf(spc) >= 0){
                  //$('#div_pwd_msg').addClass('warning');
                  var pwdMessage = '비밀번호에 < > ( ) # \' " / | - _ = + \\ [ ] { }; 공백은 사용할 수 없습니다.';
                  return(pwdMessage);
                  $('[name=loginPw]').focus();
                  return false;
               }
            }
            if(requestInfo.password.length >= 6 && requestInfo.password.length <= 15){
            }else{
                var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                return(pwdMessage);
                $('[name=loginPw]').focus();
                return false;
            }


            if(eng < 0 && num < 0 && spe < 0){
                var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                return(pwdMessage);
                $('[name=loginPw]').focus();
                return false;
            }
            if(eng >= 0){
                if( (eng >= 0) &&  (num >= 0 || spe >= 0) && requestInfo.password.length >= 6 && requestInfo.password.length <= 15){
                    // 정상 , 영어-숫자-특수문자 셋중에 2개는 필수 / 6자이상 15자 이하
                } else {
                    //$('#div_pwd_msg').addClass('warning');
                    var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                    return(pwdMessage);
                    $('[name=loginPw]').focus();
                    //$('#userPwd').focus();
                    return false;
                }
            }else if(num >= 0){
                if( (num >= 0) &&  (eng >= 0 || spe >= 0) && requestInfo.password.length >= 6 && requestInfo.password.length <= 15){
                    // 정상 , 영어-숫자-특수문자 셋중에 2개는 필수 / 6자이상 15자 이하
                } else {
                    //$('#div_pwd_msg').addClass('warning');
                    var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                    return(pwdMessage);
                    $('[name=loginPw]').focus();
                    //$('#userPwd').focus();
                    return false;
                }
            }else if(spe >= 0){
                if( (spe >= 0) &&  (eng >= 0 || num >= 0) && requestInfo.password.length >= 6 && requestInfo.password.length <= 15){
                    // 정상 , 영어-숫자-특수문자 셋중에 2개는 필수 / 6자이상 15자 이하
                } else {
                    //$('#div_pwd_msg').addClass('warning');
                    var pwdMessage = '영문, 숫자, 특수문자 2가지 이상 6~15자로 입력해주세요.';
                        return(pwdMessage);
                        $('[name=loginPw]').focus();
                    //$('#userPwd').focus();
                    return false;
                }
            }
		return true;
	}
	
	$scope.passCancel= function(e){$(e).hide();}
	$scope.passCheck= function(e){
		
		var validResult = passValidation();
		if(validResult==true){
			var param = {E_ID : sessionStorage.getItem('loginId') , chkPassword:$scope.UserInfo2.E_PW };
			mainDataService.checkPassword(param)
			.success(function(data){
				if(data==true){
					var param2 = {E_ID : sessionStorage.getItem('loginId'), chkPassword:$scope.UserInfo2.E_PW ,E_PW_01 : $scope.UserInfo2.E_PASS_NEW };
					mainDataService.saveNewPassword(param2)
					.success(function(data){
						$scope.UserInfo2.E_PASS_NEW='';
						$scope.UserInfo2.E_PASS_CONFIRM = '';
						alertify.alert('정상적으로 처리되었습니다.');
						$(e).hide();						
					});					
				}else{
					alertify.alert('기존 패스워드가 맞지않습니다.');
				}
			});
		}else{
			if(validResult!=false)
				alertify.alert(validResult);
		}
	}

}