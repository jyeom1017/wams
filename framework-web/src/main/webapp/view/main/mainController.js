angular.module('app.main').controller('mainPageController', mainPageController);

function mainPageController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
		
	$state.go('010601');
	return;
	$scope.$on('$includeContentLoaded', function() {
		angular.extend(this, new gisIframeController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message));
		hide('assetPath');
		hide('elevationDifferenceAnalysis');
		hide('blockDivision');
		hide('zoneDivision');
		hide('diagnosticArea');
		hide('assetEdit');
		hide('evaluationAnalysis');

		setStyle('topToolbar', 'width', 'auto');

		setStyle('sideToolbar', 'top', '20px');

		// $timeout(function() {
			$scope.initGis();
		// }, 1000);

		function setStyle(id, propertyName, value) {
			try{
			document.getElementById(id).style[propertyName] = value;
			}catch(e){
				console.log(e.msg);
			}
		}

		function hide(id) {
			setStyle(id, 'display', 'none');
		}
	});


	$scope.$on('$viewContentLoaded', function () {
		setGrid();
		gridResize();
		setDatePicker();
		setToday();
		$scope.yearChange();
		//ChartStat1();
		//ChartStat2();
		//ChartStat3();
		//ChartError();
	});
	$scope.searchDate = '';
	
	function setToday(){
        var today = new Date(); // 오늘
        var eYear = today.getFullYear();
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);
        $scope.searchDate = e_yyyymmdd;
        $('#searchDate').val(e_yyyymmdd);
        console.log($scope.searchDate);
    }
	
	$scope.yearChange = function yearChange(){
		$scope.baseYear = '';
		var baseYear = $scope.searchDate.substring(0, 4);
		$scope.baseYear = Number(baseYear);
	};
	
	function setGrid() {
        return pagerGrid({
            grid_id : 'list',
            pager_id : 'listPager',
            url : '/operation/getRelayInfo.json',
            condition : {
                //page : 1,
                rows : GridConfig.sizeL,
                //type : '2',
                //s_date : '20220301',
                //e_date : '20220401',
                RELAY_ITEM_CD : $scope.SelectRelayItem || '',
            },
            //rowNum : 18,
            colNames : [ 
            	'날짜',
            	'대상 시스템',
            	'연계정보 구분', 
            	'연계 항목',
            	'연계 주기', 
            	'송신 구분',
            	'데이터 1',
            	'데이터 2',
            	'상태확인건수'
            	],
            colModel : [
                { name : 'LG_YMD', width : 50 ,index : 'LG_DT'},
                { name : 'RELAY_NM', width : 85 },
                { name : 'RELAY_GBN', width : 40, edittype : 'select', formatter : 'select', editoptions : {value :'0:활용;1:비활용;'} },
                { name : 'RELAY_ITEM_CD', width : 30, edittype : 'select', formatter : 'select', editoptions : {value :'1:유량;2:수압;3:수질;4:요금'} },
                { name : 'RELAY_CYCLE_CD', width : 30, edittype : 'select', formatter : 'select', editoptions : {value :'1:매일;2:매월'} },
                { name : 'TR_GBN', width : 30, edittype : 'select', formatter : 'select', editoptions : {value :'0:수신;1:송신'} },
                { name : 'TOTAL_CNT', width : 100, formatter : function(cellValue, options, rowObject) {
            	  if (rowObject.RELAY_ITEM_CD === '4' ) {
            		  return '전월건수 '+'('+rowObject.TOTAL_CNT+')';
            	  }else if(rowObject.RELAY_ITEM_CD != '4'){
            		  if(rowObject.RELAY_ITEM_CD == null){
            			  return '';
            		  }else{
	            		  return '예측건수 '+'('+rowObject.TOTAL_CNT+')';
            		  }
            	  }
                } 
              },
              { name : 'VALID_CNT', width : 100, formatter : function(cellValue, options, rowObject) {
            	  if (rowObject.RELAY_ITEM_CD === '4' ) {
            		  return '당월건수 '+'('+rowObject.VALID_CNT+')';
            	  }else if(rowObject.RELAY_ITEM_CD != '4'){
            		  if(rowObject.RELAY_ITEM_CD == null){
            			  return '';
            		  }else{
	            		  return '실측건수 '+'('+rowObject.VALID_CNT+')';
            		  }
            	  }
              	}  
              },
              { name : 'INVALID_CNT', width : 50 ,cellattr: function(rowId, e, rowObject, d, rdata) {
                    // rowObject 변수로 그리드 데이터에 접근
                	 // INVALID_CNT값이 0보다 크거나 작으면 글자 컬러 red 설정
                    if (rowObject.INVALID_CNT == 0 ) { 
                    	return 'style="color:blue;"' 
                    }else{
                    	return 'style="color:red;"' 
                    }
              },formatter : function(cellValue, options, rowObject) {
            	  if (rowObject.INVALID_CNT > 0 || rowObject.INVALID_CNT < 0 ) {
            		  return '불일치'+'('+rowObject.INVALID_CNT+')';
            	  }else if (rowObject.INVALID_CNT == 0){
            		  return '정상';
            	  }else if(rowObject.INVALID_CNT == null){
            		  return '';
            	  }
              }}
            ],
            onSelectRow : function(rowid, status, e) {
            	$scope.SelectedAssetBaseInfo = $('#list').jqGrid('getRowData', rowid);
            	$scope.$apply();
            },
            gridComplete : function() {
				var ids = $(this).jqGrid('getDataIDs');
				$(this).setSelection(ids[0]);
				$scope.currentPageNo=$('#list').getGridParam('page');
				$scope.count = $('#list').getGridParam('records');
				$scope.$apply();
			}	
        });
    }
	
	
	//----------------------------------------왼쪽 상단 그래프1-------------------------------------------
	$scope.changeOpt1 = function(val1){
		$scope.chartOption1 = val1;
		var start_dt = '20180101';
		if(val1 == '/asset/getStatAsset.json') start_dt = '19930101';
		var param = {url:val1,START_DT:start_dt,END_DT:'202201231',UNIT:'YEAR',COMPLAIN_TYPE:"",selectST:"",CHK_MONTH_LIST:[
			  "01",
			  "02",
			  "03",
			  "04",
			  "05",
			  "06",
			  "07",
			  "08",
			  "09",
			  "10",
			  "11",
			  "12"
			]};
		mainDataService.getChartData(param)
		.success(function(data){
			console.log(data);
			var yearList2 = [];
			var list = ['ROWNUM','T','tot','C_NAME','COPLAIN_TYPE','EST_GRADE','GRADE','GRADE2','MSG','EVALUE_CD','EVALUE_NM','LOS_ITEM_CD','GOAL','COST_CD'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						yearList2.push(idx2.replace(/'/gi,''));
				});
				//민원유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			yearList2.sort();
			var yearTot = [];
			//연도별합계
			$.each(data,function(idx,Item){
				if(idx==0){
				$.each(yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) yearTot[idx2] = 0
							yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
					}
				});
				}
			});
			
			var chartData = [];
			if(param.url=='/operation/getStatMinwon.json' || param.url=='/est/getStatLcc.json'){
				$.each(data,function(idx,Item){
					chartData.push({C_NAME:Item.C_NAME,totCnt:Item.tot});	
				});
			} else if(param.url=='/operation/getStatRenovation.json' || param.url=='/operation/getStatInspect.json' || param.url=='/asset/getStatAsset.json' ){
				$.each(yearList2,function(idx,Item){
					chartData.push({C_NAME:''+Item,totCnt:yearTot[idx]});	
				});
			} else if(param.url=='/est/getStatRemainLife.json'){
				$.each(data[0],function(idx,Item){
					if(idx!='tot')
					chartData.push({C_NAME:''+idx,totCnt:Item});	
				});	
				console.log(chartData);
			} else {
				var latestYear = yearList2[yearList2.length-1];
				$.each(data,function(idx,Item){
					
					chartData.push({C_NAME:Item.EST_GRADE,totCnt:Item['\'' + latestYear+ '\'']});	
				});				
			}
			//alert(JSON.stringify(chartData));
			ChartStat1(chartData);	
		});
		
	}
	$scope.chartOption1 = '/operation/getStatMinwon.json';
	$scope.chartOption2 = '/operation/getStatRenovation.json';
	$scope.changeOpt2 = function(val2){
		$scope.chartOption2 = val2;
		var start_dt = '20180101';
		if(val2 == '/asset/getStatAsset.json') start_dt = '19930101';
		var param = {url:val2,START_DT:start_dt,END_DT:'202201231',UNIT:'YEAR',COMPLAIN_TYPE:"",selectST:"",CHK_MONTH_LIST:[
			  "01",
			  "02",
			  "03",
			  "04",
			  "05",
			  "06",
			  "07",
			  "08",
			  "09",
			  "10",
			  "11",
			  "12"
			]};
		mainDataService.getChartData(param)
		.success(function(data){
			console.log(data);
			var yearList2 = [];
			var list = ['ROWNUM','T','tot','C_NAME','COPLAIN_TYPE','EST_GRADE','GRADE','GRADE2','MSG','EVALUE_CD','EVALUE_NM','LOS_ITEM_CD','GOAL','COST_CD'];
			$scope.yearList2 = [];
			$.each(data,function(idx,Item){
				if(idx==0)
				$.each(Item,function(idx2,Item2){
					if($.inArray(idx2,list)<0)
						yearList2.push(idx2.replace(/'/gi,''));
				});
				//민원유형별 합계
				Item.tot = 0;
				$.each(Item,function(idx2,Item2){
					if(Item2==null) Item2 = 0;
					if($.inArray(idx2,list)<0)
						Item.tot += parseInt(Item2);
				});
			});
			yearList2.sort();
			var yearTot = [];
			//연도별합계
			$.each(data,function(idx,Item){
				if(idx==0){
				$.each(yearList2,function(idx2,yyyy){
					if($.inArray(idx2,list)<0){
						if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = 0; //null일경우 0
						if(idx==0) yearTot[idx2] = 0
							yearTot[idx2] += parseInt(Item['\''+yyyy+'\'']); //문자열을 숫자로
					}
				});
				}
			});
			
			var chartData = [];
			if(param.url=='/operation/getStatMinwon.json' || param.url=='/est/getStatLcc.json'){
				$.each(data,function(idx,Item){
					chartData.push({C_NAME:Item.C_NAME,totCnt:Item.tot});	
				});
			} else if(param.url=='/operation/getStatRenovation.json' || param.url=='/operation/getStatInspect.json' || param.url=='/asset/getStatAsset.json' ){
				$.each(yearList2,function(idx,Item){
					chartData.push({C_NAME:''+Item,totCnt:yearTot[idx]});	
				});
			} else if(param.url=='/est/getStatRemainLife.json'){
				$.each(data[0],function(idx,Item){
					if(idx!='tot')
					chartData.push({C_NAME:''+idx,totCnt:Item});	
				});	
				console.log(chartData);
			} else {
				var latestYear = yearList2[yearList2.length-1];
				$.each(data,function(idx,Item){
					
					chartData.push({C_NAME:Item.EST_GRADE,totCnt:Item['\'' + latestYear+ '\'']});	
				});				
			}
			//alert(JSON.stringify(chartData));
			ChartStat2(chartData);	
		});
	}
	
	$scope.changeOpt1('/operation/getStatMinwon.json');
	$scope.changeOpt2('/operation/getStatRenovation.json');
	
	var param3 = {START_DT:'20180101',END_DT:'20221231',STAT_TYPE:'D',UNIT:'YEAR',CHK_MONTH_LIST:[
		  "01",
		  "02",
		  "03",
		  "04",
		  "05",
		  "06",
		  "07",
		  "08",
		  "09",
		  "10",
		  "11",
		  "12"
		]};
	mainDataService.getStatOIP(param3)
	.success(function(data){
		console.log(data);
		var list = ['C_SCODE','C_NAME','FN_CD','CLASS_CD','CLASS3_CD','CLASS4_CD','LEVEL_NM','GB_cnt','GB_Idx','row_idx','idx'];
		var yearList2 = [];
		var yearList3 = [];
		$.each(data,function(idx,Item){
			if(idx==0)
			$.each(Item,function(idx2,Item2){
				if($.inArray(idx2,list)<0)
					yearList2.push(idx2.replace(/'/gi,''));
			});
			//민원유형별 합계
			Item.tot = 0;
			$.each(Item,function(idx2,Item2){
				if(Item2==null) Item2 = 0;
				if($.inArray(idx2,list)<0)
					Item.tot += parseInt(Item2);
			});
		});
		yearList2.sort();
		var yearTot = [];
		var yearTot2 = [];
		$scope.total = 0; // 합계 초기화
		//연도별합계
		$.each(data,function(idx,Item){
			//console.log(Item);
			$.each(yearList2,function(idx2,yyyy){
				if($.inArray(idx2,list)<0){
					//console.log(Item['\''+yyyy+'\'']);
					if(Item['\''+yyyy+'\'']==null) Item['\''+yyyy+'\''] = ''; //null일경우 0
					if(idx==0) yearTot[idx2] = 0;
					if(idx==0) yearTot2[idx2] = 0;
					var item = (Item['\''+yyyy+'\'']).split("::");
						Item['\''+yyyy+'\''] = item;
						if(idx==1 ) return;
						yearTot[idx2] += parseInt(item[0] || '0'); //문자열을 숫자로
						yearTot2[idx2] += parseInt(item[1] || '0'); //문자열을 숫자로
						$scope.total += parseInt(item[0]);
						
				}
			});
		});
		
		$.each(yearList2,function(idx2,yyyy){
			yearList3.push({y:yyyy,t:'cnt'});
			yearList3.push({y:yyyy,t:'amt'});
		});
		
		var chartData = [];
		for(var i=0; i<yearList2.length; i++){
			chartData.push({YYYYMM:'' + yearList2[i], TOTAL:yearTot2[i]});
		}
		ChartStat3(chartData);
	});
	
	var param4 = {url:'/operation/getRelayInfoChart.json'};
	mainDataService.getChartData(param4)
	.success(function(data){
		console.log(data);
		var chartData=[];
		$.each(data.rows,function(idx,Item){
			if(Item.RELAY_ITEM_CD === '4'){
				chartData.push({Month:Item.LG_DT.substring(4,8),normal:Item.VALID_CNT,abnormal:0});	
			}else{
				chartData.push({Month:Item.LG_DT.substring(4,8),normal:Item.VALID_CNT,abnormal:( Item.TOTAL_CNT - Item.VALID_CNT) + 1 });
			}
			if(idx>4) return false;
		});
		chartData.sort(function(a,b){return (a.Month>b.Month)?1:-1;});
		
		ChartError(chartData);
		console.log(chartData);
		//{"Month":"Feb", "normal":3.8, "abnormal":4.6},
	});
	
	let ChartStat1 = function(chartData){
		rMateChartH5.create("chart1", "ChartStat1", "", "100%", "100%");
		 
		var layoutStr =
		             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none" >'
		                  +'<Options>'
		                      //+'<Caption text="Web Browser Market Share"/>'
		                     +'<Legend useVisibleCheck="true"/>'
		                   +'</Options>'
		                 +'<CurrencyFormatter id="numFmt" currencySymbol="%" alignSymbol="right"/>'
		                 	+'<Pie2DChart showDataTips="true">'
		               /* Pie2D 차트 생성시에 필요한 Pie2DChart 정의합니다. */
		               /* showDataTips : 데이터에 마우스를 가져갔을 때 나오는 Tip을 보이기/안보이기 속성입니다. */
		                      +'<series>'
		                           +'<Pie2DSeries nameField="C_NAME" field="totCnt" labelPosition="inside" color="#ffffff">'
		                         /* Pie2DChart 정의 후 Pie2DSeries labelPosition="inside"정의합니다 */
		                               +'<showDataEffect>'
		                               /* 차트 생성시에 Effect를 주고 싶을 때 showDataEffect정의합니다 */
		                                   +'<SeriesSlide duration="1000"/>'
		             /* SeriesSlide 효과는 시리즈 데이터가 데이터로 표시될 때 한쪽에서 미끄러지듯 나타나는 효과를 적용합니다. */
		                /* elementOffset : effect를 지연시키는 시간을 지정합니다 default:20 */
		                /* minimumElementDuration : 각 엘리먼트의 최소 지속 시간을 설정합니다 default:0 */
		                             /* 이 값보다 짧은 시간에 effect가 실행되지 않습니다. */
		              /* offset : effect개시의 지연시간을 설정합니다 default:0 */
		              /* perElementOffset : 각 엘리먼트의 개시 지연시간을 설정합니다. */
		                /* direction : left:왼쪽, right:오른쪽, up:위, down:아래 default는 left입니다. */
		                               +'</showDataEffect>'
		                          +'</Pie2DSeries>'
		                     +'</series>'
		                  +'</Pie2DChart>'
		              +'</rMateChart>';
		 
		// 차트 데이터
		/*
		var chartData =
		 [{"":"Chrome", "share":40},
		  {"browser":"Internet Explorer", "share":21},
		    {"browser":"Firefox", "share":19},
		  {"browser":"Safari", "share":15},
		   {"browser":"Opera", "share":5}];
		*/
		rMateChartH5.calls("chart1", {
		 "setLayout" : layoutStr,
		    "setData" : chartData
		});
		rMateChartH5.registerTheme(rMateChartH5.themes);
		function rMateChartH5ChangeTheme(theme){
		    document.getElementById("chart1").setTheme(theme);
		}
	};
	
	
	//----------------------------------------왼쪽 상단 그래프2-------------------------------------------
	
	let ChartStat2 = function(chartData){
		rMateChartH5.create("chart2", "ChartStat2", "", "100%", "100%");
		 
		var layoutStr =
		             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none" >'
		                  +'<Options>'
		                      //+'<Caption text="Web Browser Market Share"/>'
		                     +'<Legend useVisibleCheck="true"/>'
		                   +'</Options>'
		                 +'<CurrencyFormatter id="numFmt" currencySymbol="%" alignSymbol="right"/>'
		                 	+'<Pie2DChart showDataTips="true">'
		               /* Pie2D 차트 생성시에 필요한 Pie2DChart 정의합니다. */
		               /* showDataTips : 데이터에 마우스를 가져갔을 때 나오는 Tip을 보이기/안보이기 속성입니다. */
		                      +'<series>'
		                           +'<Pie2DSeries nameField="C_NAME" field="totCnt" labelPosition="inside" color="#ffffff">'
		                         /* Pie2DChart 정의 후 Pie2DSeries labelPosition="inside"정의합니다 */
		                               +'<showDataEffect>'
		                               /* 차트 생성시에 Effect를 주고 싶을 때 showDataEffect정의합니다 */
		                                   +'<SeriesSlide duration="1000"/>'
		             /* SeriesSlide 효과는 시리즈 데이터가 데이터로 표시될 때 한쪽에서 미끄러지듯 나타나는 효과를 적용합니다. */
		                /* elementOffset : effect를 지연시키는 시간을 지정합니다 default:20 */
		                /* minimumElementDuration : 각 엘리먼트의 최소 지속 시간을 설정합니다 default:0 */
		                             /* 이 값보다 짧은 시간에 effect가 실행되지 않습니다. */
		              /* offset : effect개시의 지연시간을 설정합니다 default:0 */
		              /* perElementOffset : 각 엘리먼트의 개시 지연시간을 설정합니다. */
		                /* direction : left:왼쪽, right:오른쪽, up:위, down:아래 default는 left입니다. */
		                               +'</showDataEffect>'
		                          +'</Pie2DSeries>'
		                     +'</series>'
		                  +'</Pie2DChart>'
		              +'</rMateChart>';
		 
		// 차트 데이터
		/*
		var chartData =
		 [{"browser":"Chrome", "share":40},
		  {"browser":"Internet Explorer", "share":21},
		    {"browser":"Firefox", "share":19},
		  {"browser":"Safari", "share":15},
		   {"browser":"Opera", "share":5}];
		*/
		rMateChartH5.calls("chart2", {
		 "setLayout" : layoutStr,
		    "setData" : chartData
		});
		rMateChartH5.registerTheme(rMateChartH5.themes);
		function rMateChartH5ChangeTheme(theme){
		    document.getElementById("chart2").setTheme(theme);
		}
	};
	
	
	//----------------------------------------왼쪽 하단 그래프-------------------------------------------
	
	let ChartStat3 = function(chartData){
		rMateChartH5.create("chart3", "ChartStat3", "", "100%", "100%");
		 
		// 스트링 형식으로 레이아웃 정의.
		var layoutStr =
		             '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
		                   +'<Options>'
		                      //+'<Caption text="Sales-Cost-Profit and ROI"/>'
		                        +'<Legend useVisibleCheck="false"/>'
		                   +'</Options>'
		                 +'<NumberFormatter id="numFmt1" useThousandsSeparator="true"/>'
		                   +'<CurrencyFormatter id="numFmt2" precision="0" currencySymbol="%" alignSymbol="right"/>'
		                 +'<Combination2DChart >'
		                 /*
		                  Combination2D 차트 생성시에 필요한 Combination2DChart 정의합니다
		                  showDataTips : 데이터에 마우스를 가져갔을 때 나오는 Tip을 보이기/안보이기 속성입니다
		                  */
		                     +'<horizontalAxis>'
		                           +'<CategoryAxis categoryField="YYYYMM" padding="0.5"/>'
		                        +'</horizontalAxis>'
		                      +'<verticalAxis>'
		                         +'<LinearAxis id="vAxis1" formatter="{numFmt1}"/>'
		                        +'</verticalAxis>'
		                        +'<series>'
		                           +'<Column2DSet type="clustered">'
		              /*
		             Combination 차트에서는 Column차트와 Bar차트를 사용할때 Column2DSet(Colum2DSet),Bar2DSet(Bar2DSet)을 사용합니다
		               예제로 Column2DSeries를 사용하려 할 때는 Column2DSet을 정의 후 +'<series>'와+'<Column2DSet>'을 정의합니다
		             Column2DSet(Colum2DSet),Bar2DSet(Bar2DSet)의 Type은 일반 Type과 동일 합니다
		               그러나 기본 Type은 overlaid입니다
		                  */
		                            +'<series>'
		                               +'<Column2DSeries yField="TOTAL" displayName="개대체 건수" labelPosition="outside">'
		                                  +'<fill>'
		                                     +'<SolidColor color="#41b2e6"/>'
		                                  +'</fill>'
		                                    +'<showDataEffect>'
		                                       +'<SeriesInterpolate/>'
		                                   +'</showDataEffect>'
		                              +'</Column2DSeries>'
		                              /*
		                              +'<Column2DSeries yField="cost" displayName="Cost" labelPosition="outside">'
		                                    +'<fill>'
		                                     +'<SolidColor color="#4452a8"/>'
		                                  +'</fill>'
		                                    +'<showDataEffect>'
		                                       +'<SeriesInterpolate/>'
		                                   +'</showDataEffect>'
		                              +'</Column2DSeries>'
		                              +'<Column2DSeries yField="profit" displayName="Profit" labelPosition="outside">'
		                                    +'<fill>'
		                                     +'<SolidColor color="#fabc05"/>'
		                                  +'</fill>'
		                                    +'<showDataEffect>'
		                                       +'<SeriesInterpolate/>'
		                                   +'</showDataEffect>'
		                              +'</Column2DSeries>'*/
		                          +'</series>'
		                      +'</Column2DSet>'
		                      /*
		                     +'<Line2DSeries selectable="true" yField="roi" displayName="ROI" radius="4.5" form="curve" itemRenderer="CircleItemRenderer">'
		                                +'<stroke>'
		                                   +'<Stroke color="#5587a2" weight="3"/>'
		                               +'</stroke>'
		                              +'<lineStroke>'
		                                   +'<Stroke color="#5587a2" weight="3"/>'
		                               +'</lineStroke>'
		                              +'<verticalAxis>'
		                                 +'<LinearAxis id="vAxis2" formatter="{numFmt2}"/>'
		                                +'</verticalAxis>'
		                                +'<showDataEffect>'
		                                   +'<SeriesInterpolate/>'
		                               +'</showDataEffect>'
		                          +'</Line2DSeries>'
		                          */
		                        +'</series>'
		                      +'<verticalAxisRenderers>'
		                            +'<Axis2DRenderer axis="{vAxis1}" showLine="false"/>'
		                         //+'<Axis2DRenderer axis="{vAxis2}" showLine="false"/>'
		                     +'</verticalAxisRenderers>'
		                   +'</Combination2DChart>'
		              +'</rMateChart>';
		 
		// 차트 데이터
		/*
		var chartData =
		 [{"Month":"Jan","sales":2000, "cost":1300, "profit":700, "roi":20},
		 {"Month":"Feb","sales":3000, "cost":1800, "profit":1200, "roi":45},
		 {"Month":"Mar","sales":4200, "cost":2500, "profit":1700, "roi":83},
		 {"Month":"Apr","sales":5500, "cost":3500, "profit":2000, "roi":45},
		 {"Month":"May","sales":6200, "cost":4000, "profit":2200, "roi":20},
		 {"Month":"Jun","sales":7200, "cost":4500, "profit":2700, "roi":10}];
		 */
		// rMateChartH5.calls 함수를 이용하여 차트의 준비가 끝나면 실행할 함수를 등록합니다.
		//
		// argument 1 - rMateChartH5.create시 설정한 차트 객체 아이디 값
		// argument 2 - 차트준비가 완료되면 실행할 함수 명(key)과 설정될 전달인자 값(value)
		// 
		// 아래 내용은 
		// 1. 차트 준비가 완료되면 첫 전달인자 값을 가진 차트 객체에 접근하여
		// 2. 두 번째 전달인자 값의 key 명으로 정의된 함수에 value값을 전달인자로 설정하여 실행합니다.
		rMateChartH5.calls("chart3", {
		 "setLayout" : layoutStr,
		    "setData" : chartData
		});
		 
		function dataTipJsFunc (seriesId, seriesName, index, xName, yName, data, values) {
		  if (seriesName == "ROI")
		        return data['Month'] + "" + seriesName + " : <b>" + values['1'] + "%</b>";
		    else
		        return data['Month'] + "" + seriesName + " : <b>$" + values['1'] + "M</b>";
		}
		rMateChartH5.registerTheme(rMateChartH5.themes);
		function rMateChartH5ChangeTheme(theme){
		  document.getElementById("chart3").setTheme(theme);
		}
	};
	
	
	//----------------------------------------데이터 오류 현황 그래프-------------------------------------------
	
	let ChartError = function(chartData){
		rMateChartH5.create("chart4", "ChartError", "", "100%", "100%"); 
		 
		// 스트링 형식으로 레이아웃 정의.
		var layoutStr =
		            '<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
		               +'<Options>'
		                 // +'<Caption text="GDP Growth of the BRICS Countries"/>'
		                   // +'<SubCaption text="From 2011 To 2013 (Annual %)" textAlign="center" />'
		                  +'<Legend defaultMouseOverAction="false" />'
		              +'</Options>'
		              +'<NumberFormatter id="numfmt" useThousandsSeparator="true"/>'
		             +'<Column2DChart showDataTips="false" columnWidthRatio="0.55" selectionMode="single">'
		                 +'<horizontalAxis>'
		                       +'<CategoryAxis categoryField="Month"/>'
		                  +'</horizontalAxis>'
		                  +'<verticalAxis>'
		                     +'<LogAxis maximum="1000000" minimum="0" formatter="{numfmt}"/>'
		                 +'</verticalAxis>'
		                    +'<series>'
		                       +'<Column2DSeries  yField="normal" displayName="정상">'
		                            +'<showDataEffect>'
		                               +'<SeriesInterpolate/>'
		                           +'</showDataEffect>'
		                      +'</Column2DSeries>'
		                      +'<Column2DSeries  yField="abnormal" displayName="비정상">'
		                      	+'<fill>'
                              		+'<SolidColor color="#fabc05"/>'
                              	+'</fill>'
		                            +'<showDataEffect>'
		                               +'<SeriesInterpolate/>'
		                           +'</showDataEffect>'
		                      +'</Column2DSeries>'
		                  +'</series>'
		              +'</Column2DChart>'
		           +'</rMateChart>';
		 
		// 차트 데이터
		/*
		var chartData = [
			{"Month":"Jan", "normal":2.4, "abnormal":3.1},
			{"Month":"Feb", "normal":3.8, "abnormal":4.6},
			{"Month":"Mar", "normal":8.1, "abnormal":9},
			{"Month":"Apr", "normal":5.1, "abnormal":4},
			{"Month":"May", "normal":2.1, "abnormal":3.9},
			{"Month":"Jun", "normal":5.2, "abnormal":4.9},
			{"Month":"Jul", "normal":4.2, "abnormal":3.2}
			];
		 */
		rMateChartH5.calls("chart4", {
		    "setLayout" : layoutStr,
		    "setData" : chartData
		});
		
		rMateChartH5.registerTheme(rMateChartH5.themes);
		
		function rMateChartH5ChangeTheme(theme){
		    document.getElementById("chart4").setTheme(theme);
		}
	};
}