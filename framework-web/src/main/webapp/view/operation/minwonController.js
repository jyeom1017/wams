angular.module('app.operation').controller('minwonController',minwonController);
//angular.module('app.operation').controller('image_PopupController',image_PopupController);

// 민원관리
function minwonController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, MaxUploadSize) {
    $scope.$on('$viewContentLoaded', function () {
        $("#tab1_btn").on("click", function() {
            $(this).addClass("active");
            $("#tab2_btn").removeClass("active");
            $("#tab1").show();
            $("#tab2").hide();
        });
        $("#tab2_btn").on("click", function() {
            $(this).addClass("active");
            $("#tab1_btn").removeClass("active");
            $("#tab2").show();
            $("#tab1").hide();
        });

        setGrid();
        setGrid2();
        setGrid3();
        setGrid4();
        
        $scope.setWeek();

        setDatePicker();
        //setLastWeek();
        
    	/*mainDataService.getCommonCodeList({
            gcode : 'YEAR',
            gname : ''
        }).success(function(data) {
            if (!common.isEmpty(data.errMessage)) {
                alertify.error('errMessage : ' + data.errMessage);
            } else {
            	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
                $scope.ComCodeYearList = data;
            }
        });*/
    });
	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 1개월
        sday.setMonth(sday.getMonth() - 1); // 1개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.searchDate.S_DATE = s_yyyymmdd;
        $('#searchDate_S_DATE').val(s_yyyymmdd);
        $scope.searchDate.E_DATE = e_yyyymmdd;
        $('#searchDate_E_DATE').val(e_yyyymmdd);
    }


    $scope.MCODE = '0205';
    $scope.storeYear = getYears(YearSet.baseYear, YearSet.plus);
    $scope.selectedCurrYear = $scope.selectedYear = '' + (new Date()).getFullYear();

    /** 스코프 초기화 **/
    $scope.storeFileList = []; // 첨부파일 리스트
    $scope.storeFileList_popup = []; // 팝업 첨부파일 리스트
    $scope.storeCategory = []; // 민원유형 카테고리 (공통코드에서 가져옴)
    $scope.saveType = ''; // 신규 or 수정
    $scope.minwon_current_sid = ''; // 현재 글 번호. 임시글 번호 또는 수정에 사용
    $scope.minwon_sid = '';
    $scope.MinwonInfo = {};
    $scope.MinwonInfoPopup = {}; // 팝업창 민원 내용
    $scope.searchDateType = '1'; // 날짜검색 : 연도(1) or 기간선택(2)
    $scope.searchDate = {}; // 기간선택
    $scope.searchCategory = '';
    $scope.uploadfilename = "";
    $scope.fileListLimit = '4'; // 파일 최대 업로드 개수
    $scope.MaxUploadSize = MaxUploadSize.size;
    $scope.assetInfo = {};
    $scope.pic_list = [];
    $scope.asset = {};
/*
    function setLastWeek() {
        var sday = new Date(); // 일주일전
        sday.setDate(sday.getDate() - 7); // 일주일전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        
        $scope.searchDate.S_DATE = s_yyyymmdd;
        $('#searchDate_S_DATE').val(s_yyyymmdd);
        $scope.searchDate.E_DATE = e_yyyymmdd;
        $('#searchDate_E_DATE').val(e_yyyymmdd);
        
    }*/

    function setLevelName (options, rowdata) {
        var index = options.colModel.index;
        var level = index.substr(index.length - 1, 1);

        return (rowdata.ASSET_PATH_CD === null) ? '' : getLevelName(rowdata.ASSET_PATH_CD, level);
    }

    // 메인 화면 좌측 민원목록 그리드
    function setGrid() {
        return pagerJsonGrid({
            grid_id : 'list_minwon',
            pager_id : 'listPager',
            url : '/operation/getMinwonList.json',
            condition : {
                page : 1,
                rows : GridConfig.sizeM,
                temp_yn     : 'N',
                del_yn   : 'N',
            },
            rowNum : GridConfig.sizeM,
            colNames : [ 
            	'순번', '민원유형_코드', '민원유형', '접수일', '처리일', '민원인', '주소', '연락처', '접수자', '처리자', '처리내용', '글번호'
            ],
            colModel : [
                { name : 'RNUM', width : "40px",resizable: false ,sortable:false},
                { name : 'COMPLAIN_TYPE', hidden : true },
                { name : 'COMPLAIN_TYPE_NAME', width : 55,resizable: false },
                { name : 'RECEIVE_DT', width : 55,resizable: false },
                { name : 'SETTLE_DT', width : 55,resizable: false },
                { name : 'COMPLAINER_NM', width : 55,resizable: false ,sortable:false},
                { name : 'COMPLAINER_ADDR', hidden : true ,sortable:false},
                { name : 'COMPLAINER_TEL', width : 55,resizable: false ,sortable:false},
                { name : 'RECEIVER_NM', width : 55,resizable: false },
                { name : 'SETTLER_NM', width : 55,resizable: false },
                { name : 'SETTLE_DESC', hidden : true }, // 처리내용
                { name : 'COMPLAIN_SID', hidden : true }
            ],
            onSelectRow : function(rowid, status, e) {
                var param = $('#list_minwon').jqGrid('getRowData', rowid);
                setGrid2();
                setAssetGrid(param.COMPLAIN_SID);
                $scope.pic_list = []; // 사진대지 초기화
                if (parseInt(param.COMPLAIN_SID) > 0) {
                    $scope.minwon_current_sid = param.COMPLAIN_SID;
                    $scope.MinwonInfo = param;
                    $scope.loadFileList(param.COMPLAIN_SID, 'N'); // 첨부파일 리스트 불러오기
                } else {
                    $scope.MinwonInfo = {};
                    $scope.storeFileList = {};
                }

                if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {
                } else {
                    $scope.$apply();
                }
            },
            gridComplete : function() {
				var ids = $(this).jqGrid('getDataIDs');
				$(this).setSelection(ids[0]);
				$scope.currentPageNo=$('#list_minwon').getGridParam('page');
				$scope.totalCount = $('#list_minwon').getGridParam('records');
				if($('#list_minwon').jqGrid('getRowData', ids[0]).RNUM=="") $scope.totalCount = 0;
				$scope.$apply();
			}	
        });
    }

    // grid2, grid4 세팅
    var setAssetGrid = function(parameter) {
        var param = {};
        param.crudURL = '/operation/getMinwonAssetList.json';
        param.COMPLAIN_SID = parameter;

        mainDataService.cmCRUD(param).success(function(obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.alert('errMessage : ' + obj.errMessage);
            } else {
                $scope.assetInfoData = obj;
                $scope.assetInfo.clearGridData();
                $scope.assetInfo.setGridParam({
                    datatype: 'local',
                    data: obj,
                }).trigger('reloadGrid');

                $scope.assetPicInfo.clearGridData();
                $scope.assetPicInfo.setGridParam({
                    datatype: 'local',
                    data: obj,
                }).trigger('reloadGrid');
            }
        });
        gridResize();
    }

    var setAssetGrid_popup = function(parameter) {
        $scope.assetInfo_popup.clearGridData();
        $scope.assetInfo_popup.setGridParam({
            datatype: 'local',
            data: $scope.assetInfoData,
        }).trigger('reloadGrid');
        gridResize();
    }

    // 메인 화면 우측 자산목록 그리드
    function setGrid2() {
        var gridInfo = {
            grid_id : 'list_asset',
            colNames : [
                '순번',
                '자산번호',
                '자산코드', '자산명',
                '레벨1', '레벨2', '레벨3', '레벨4', '레벨5', '레벨6', '레벨7', '레벨8'],
            colModel : [
                {name : 'NUM',width : 30, index: 'NUM',},
                {name : 'ASSET_SID', hidden: true, index : 'ASSET_SID'},
                {name : 'ASSET_CD', width :80, index : 'ASSET_CD'},
                {name : 'ASSET_NM', width :75, index : 'ASSET_NM'},
                {name : 'level_1', width : 50, index : 'level_1',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_2', width : 50, index : 'level_2',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_3', width : 50, index : 'level_3',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_4', width : 50, index : 'level_4',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_5', width : 50, index : 'level_5',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_6', width : 50, index : 'level_6',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_7', width : 50, index : 'level_7',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_8', width : 50, index : 'level_8',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
            ],
            onSelectRow : function(rowid, status, e) {
                var param = $('#list_asset').jqGrid('getRowData', rowid);
                $scope.asset = param;
            }
        };
        $scope.assetInfo = nonPagerGrid(gridInfo);
    }

    // 팝업창 자산목록 그리드
    function setGrid3() {
        var gridInfo = {
            grid_id : 'list_asset_popup',
            colNames : [
                '순번',
                '자산번호',
                '자산코드', '자산명',
                '레벨1', '레벨2', '레벨3', '레벨4', '레벨5', '레벨6', '레벨7', '레벨8'],
            colModel : [
                {name : 'NUM', width : 20, index: 'NUM',},
                {name : 'ASSET_SID', hidden: true, index : 'ASSET_SID'},
                {name : 'ASSET_CD', width :65, index : 'ASSET_CD'},
                {name : 'ASSET_NM', width :60, index : 'ASSET_NM'},
                {name : 'level_1', width : 45, index : 'level_1',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_2', width : 40, index : 'level_2',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_3', width : 40, index : 'level_3',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_4', width : 40, index : 'level_4',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_5', width : 40, index : 'level_5',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_6', width : 40, index : 'level_6',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_7', width : 40, index : 'level_7',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_8', width : 40, index : 'level_8',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
            ],
            multiselect : true,            
            onSelectRow : function(rowid, status, e) {
                var param = $('#list_asset_popup').jqGrid('getRowData', rowid);
                $scope.asset = param;
            },
            loadComplete: function (data) {
            }
        };
        $scope.assetInfo_popup = nonPagerGrid(gridInfo);
    }

    // 메인 화면 우측 사진대지 자산목록 그리드
    function setGrid4() {
        var gridInfo = {
            grid_id : 'list_asset_pic',
            colNames : [
                '순번',
                '자산번호',
                '자산코드', '자산명',
                '레벨1', '레벨2', '레벨3', '레벨4', '레벨5', '레벨6', '레벨7', '레벨8'],
            colModel : [
                {name : 'NUM',width : 30, index: 'NUM',},
                {name : 'ASSET_SID', hidden: true, index : 'ASSET_SID'},
                {name : 'ASSET_CD', width :80, index : 'ASSET_CD'},
                {name : 'ASSET_NM', width :75, index : 'ASSET_NM'},
                {name : 'level_1', width : 50, index : 'level_1',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_2', width : 50, index : 'level_2',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_3', width : 50, index : 'level_3',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_4', width : 50, index : 'level_4',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_5', width : 50, index : 'level_5',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_6', width : 50, index : 'level_6',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_7', width : 50, index : 'level_7',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
                {name : 'level_8', width : 50, index : 'level_8',
                    formatter : function(cellvalue, options, rowdata, action) {
                        return setLevelName(options, rowdata);
                    }},
            ],
            onSelectRow : function(rowid, status, e) {
                var param = $('#list_asset_pic').jqGrid('getRowData', rowid);
                $scope.asset = param;
                $scope.AssetSid = param.ASSET_SID;
                $scope.search_picture();
            },
	        loadComplete: function(){
				var ids = $('#list_asset_pic').jqGrid('getDataIDs');
				$('#list_asset_pic').jqGrid('setSelection', ids[0]);
			}
        };
        $scope.assetPicInfo = nonPagerGrid(gridInfo);
    }

    function getLevelName(pathCd,level){
        var LEVEL_CD = pathCd.split('_')[level-1];
        if(LEVEL_CD!=''){
            var LevelName = $rootScope.LevelName[''+(level -1) +'_'+ LEVEL_CD];
            return LevelName;
        }else{
            return "";
        }
    }

    // 민원유형 가져오기
    mainDataService.getCommonCodeList({
        gcode : '503',
        gname : ''
    }).success(function(data) {
        if (!common.isEmpty(data.errMessage)) {
            alertify.error('errMessage : ' + data.errMessage);
        } else {
            $scope.storeCategory = data;
        }
    });

    // 등록 클릭 시 팝업창 띄우기
    $scope.showNewMinwon = function () {
        $scope.saveType = 'new';

        mainDataService.insertMinwonTemp({
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                $scope.MinwonInfo = {}; // 메인화면 우측 초기화
                $scope.assetInfoData = [];
                setAssetGrid(0); // 자산 초기화
                setAssetGrid_popup(0); // 자산 팝업 초기화
                $scope.storeFileList = [];
                $scope.storeFileList_popup = [];
                $scope.minwon_current_sid = obj; // obj : insert한 임시글 번호

                //첨부파일 입력초기화
                $("#ex_filename2").val("");
                $scope.uploadfilename="";
                $scope.file2 = null;
                $scope.fileSelected = false;
                
                // 임시글 넣기 성공 시 팝업 띄운다
                $('#popup_minwon').show();
            }
        });
    };

    // 수정 클릭 시 팝업창 띄우기
    $scope.showEditMinwon = function () {
        if (!($scope.minwon_current_sid > 0)) {
            alertify.alert('','민원을 선택해주세요');
            return;
        }

        $scope.saveType = 'update';
        $scope.MinwonInfoPopup = angular.copy($scope.MinwonInfo); // 메인 민원 내용이 수정 팝업창에 뜨게 함
        $scope.MinwonInfoPopup.COMPLAINER_TEL = ($scope.MinwonInfoPopup.COMPLAINER_TEL.length<2)?"":$scope.MinwonInfoPopup.COMPLAINER_TEL;
        $scope.storeFileList_popup = angular.copy($scope.storeFileList); // 메인 첨부파일 수정 팝업창에 뜨게 함
        setAssetGrid_popup($scope.MinwonInfo.COMPLAIN_SID);

        //첨부파일 입력초기화
        $("#ex_filename2").val("");
        $scope.uploadfilename="";
        $scope.file2 = null;
        $scope.fileSelected = false;
        
        $('#popup_minwon').show();
    };

    // 취소 / x 클릭 시 팝업창 닫기
    $scope.closeMinwonPopup = function () {
        // 신규 등록 화면일 때
        // 임시글 지운다 (delete)
        alertify.confirm('Info','작성 중인 내용이 저장되지 않았습니다. 정말 취소하시겠습니까?'
            ,function(){
                if ($scope.saveType === 'new') {
                    mainDataService.deleteMinwonTemp({
                        complain_sid: $scope.minwon_current_sid
                    }).success(function (obj) {
                        if (!common.isEmpty(obj.errMessage)) {
                            alertify.error('errMessage : ' + obj.errMessage);
                        } else {
                            if (obj > 0) {
                                firstPage("list_minwon"); // 목록 새로고침
                            }
                            $scope.minwon_current_sid = "";
                        }
                    });
                }
                $scope.MinwonInfoPopup = {}; // 팝업창 민원 내용 새로고침
                $('#popup_minwon').hide();

                alertify.alert('Info','취소되었습니다.',function(){
                    return;
                });
                return;
            }
            ,function(){
                return;
            });
        $scope.assetInfo_popup.clearGridData();
    };

    //validation 체크에 사용
    var MinwonItem = [{
        'name'    : 'COMPLAIN_TYPE',
        'title'   : '민원유형',
        'required': true,
    }, {
        'name' : 'COMPLAINER_TEL',
        'title': '연락처',
        'type' : 'phone'
    }];

    $scope.clearDt = function(obj){
    	//alert(obj);
    	obj = "";
    	$scope.$apply();
    }
    // 등록(신규) 또는 수정
    $scope.saveMinwon = function () {
        // 유효성 검사
        if (!ItemValidation($scope.MinwonInfoPopup, MinwonItem)) {
            return;
        }
        if(($scope.MinwonInfoPopup.RECEIVE_DT||'')!='' && ($scope.MinwonInfoPopup.SETTLE_DT||'')!=''){
        	if(!checkDate($scope.MinwonInfoPopup.RECEIVE_DT, $scope.MinwonInfoPopup.SETTLE_DT, '')){
        		alertify.alert('Info','입력값을 확인하세요 . 접수날짜가 처리날짜보다 나중일수 없습니다.');
        		return;	
        	}	
        }

        var msg1 = ($scope.saveType === 'new')? '저장' : '수정';
        var msg2 = ($scope.saveType === 'new')? '작성' : '수정';

        alertify.confirm(msg1 + ' 확인', msg2 + '하신 자료를 저장 하시겠습니까?',
            function () {

	            $scope.MinwonInfoPopup.COMPLAINER_NM = xssFilter($scope.MinwonInfoPopup.COMPLAINER_NM);
	            $scope.MinwonInfoPopup.COMPLAINER_ADDR = xssFilter($scope.MinwonInfoPopup.COMPLAINER_ADDR);
	            $scope.MinwonInfoPopup.RECEIVER_NM = xssFilter($scope.MinwonInfoPopup.RECEIVER_NM);
	            $scope.MinwonInfoPopup.SETTLER_NM = xssFilter($scope.MinwonInfoPopup.SETTLER_NM);
	            $scope.MinwonInfoPopup.SETTLE_DESC = xssFilter($scope.MinwonInfoPopup.SETTLE_DESC);
	            
                var assetData = $("#list_asset_popup").getRowData();
                var data = angular.copy($scope.MinwonInfoPopup);
                data.complain_sid = $scope.minwon_current_sid; // 새 글 등록 시에 사용
                
                data.temp_yn = 'N';
                data.type = $scope.saveType;
                data.assetData = assetData;

                // 날짜에서 '-' 제거
                if (data.RECEIVE_DT != undefined && data.RECEIVE_DT != '') {
                    data.RECEIVE_DT = data.RECEIVE_DT.replace(/\-/g,'');
                }

                if (data.SETTLE_DT != undefined && data.SETTLE_DT != '') {
                    data.SETTLE_DT = data.SETTLE_DT.replace(/\-/g, '');
                }

                mainDataService.saveMinwon(data).success(function(data) {
                    if (!common.isEmpty(data.errMessage)) {
                        alertify.error('errMessage : ' + data.errMessage);
                    } else {
                        if ($scope.saveType === 'update') {
                            $scope.MinwonInfo = angular.copy($scope.MinwonInfoPopup); // 오른쪽 영역에 팝업 내용 넣는다
                            $scope.storeFileList = angular.copy($scope.storeFileList_popup);
                            // 민원유형은 텍스트를 가져와서 따로 넣어줌
                            $scope.MinwonInfo.COMPLAIN_TYPE_NAME = $('#MinwonInfoPopup_COMPLAIN_TYPE option:checked').text();
                            setAssetGrid($scope.minwon_current_sid); // 자산 팝업 초기화
                        }

                        alertify.success('민원이 ' + msg2 + ' 되었습니다.');
                        firstPage("list_minwon"); // 목록 새로고침
                        setAssetGrid_popup(0); // 자산 팝업 초기화
                        $scope.MinwonInfoPopup = {}; // 팝업창 내용 초기화
                        $scope.minwon_current_sid = ''; // 임시글 번호 초기화

                        if ($scope.saveType === 'new') {
                            $scope.assetInfoData = [];
                            setAssetGrid(0); // 자산 초기화
                        }
                        $('#popup_minwon').hide(); // 팝업 닫기
                    }
                });
            },function () {
        }).set('basic', false);
    };

    $scope.search = function () {
        var data = {};

//        data.page = 1;
//        data.rows = GridConfig.sizeL;
        data.category = $scope.searchCategory;
        data.type = $scope.searchDateType || '';

        // 연도 검색
        if ($scope.searchDateType == "1") {
            data.year = $scope.selectedCurrYear || '';

        // 기간선택 검색
        } else {
            data.s_date = $scope.searchDate.S_DATE || '';
            data.e_date = $scope.searchDate.E_DATE || '';
            if(($scope.searchDate.S_DATE || '')=='' || ($scope.searchDate.E_DATE || '')==''){
            	alertify.alert('Info','조회기간을 입력하세요');
            	return ;
            }
            // 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
            if (!checkDate($scope.searchDate.S_DATE, $scope.searchDate.E_DATE, '')) {
                return;
            }
        }

        $('#list_minwon').jqGrid('setGridParam', {
            page : 1,
            rows : GridConfig.sizeM,
            postData: data
        }).trigger('reloadGrid', {
            current: true
        });
    };

    $scope.changeRadio = function (type) {
        $scope.searchDateType = (type === 1) ? 1 : 2;
    };

    // 민원 삭제
    $scope.deleteMinwon = function () {
        if (!($scope.minwon_current_sid > 0)) {
            alertify.alert('','민원을 선택해주세요');
            return;
        }

        // 삭제 권한 체크
        if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
            alertify.alert('','권한이 없습니다.');
            return;
        }

        alertify.confirm('삭제 확인', '선택하신 자료를 삭제 하시겠습니까?',
            function () {
                mainDataService.deleteMinwon({
                    complain_sid: $scope.MinwonInfo.COMPLAIN_SID,
                    del_yn: 'Y',
                }).success(function (obj) {
                    if (!common.isEmpty(obj.errMessage)) {
                        alertify.error('errMessage : ' + obj.errMessage);
                    } else {
                        alertify.success('민원이 삭제 되었습니다.');
                        firstPage("list_minwon"); //목록 새로고침
                        $scope.MinwonInfo = {};
                        $scope.storeFileList = {};
                    }
                });
            },
            function () {
            }
        ).set('basic', false);
    };

    // 파일 업로드 validation check
    // 파일 선택했을 때. 업로드 하기 전
    $scope.onFileSelect = function ($files, cmd) {
    	
    	if(FileCheck($files, cmd, MaxUploadSize)){
            $files = null;
            $scope.file2 = null;
            $scope.fileSelected = false;
            $scope.uploadfilename = "";
    		return;
    	}
    	
        var str = '\.(' + cmd + ')$';
        var FileFilter = new RegExp(str);
        var ext = cmd.split('|');
        if ($files[0].name.toLowerCase().match(FileFilter)) {
            $scope.file2 = $files;
            $scope.uploadfilename = $files[0].name; // 파일의 이름을 넣고 화면에서 불러오기 위함.
        } else {
            $files = null;
            $scope.file2 = null;
            $scope.fileSelected = false;
            alertify.alert('Info', '확장자가 ' + ext + ' 인 파일만 선택가능합니다.');
        }
        if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {

        } else {
            $scope.$apply();
        }
    };

    // 업로드 버튼 클릭 시 실행
    $scope.fileUpload = function (mode) {

        if ($scope.storeFileList_popup != undefined && $scope.storeFileList_popup.length >= $scope.fileListLimit) {
            alertify.alert('첨부파일은 ' + $scope.fileListLimit + '개까지 업로드 가능합니다.');
            return;
        }

        var complain_sid = '';

        if ($scope.saveType === 'new') {
            complain_sid = $scope.minwon_current_sid;
        } else if ($scope.saveType === 'update') {
            complain_sid = $scope.MinwonInfoPopup.COMPLAIN_SID; // 수정일때
        }

        $scope.uploadfilename = ""; // 업로드 하기 전에 빈 값으로 바꾼다.

        if ($scope.file2 == null || $scope.file2.length == 0) {
            alertify.alert('Info','파일을 선택하세요');
            return;
        }

        if ($scope.file2[0].size === 0) {
            alertify.alert('Info','사이즈가 0인 파일은 업로드 할 수 없습니다.');
            return;
        }

        if ($scope.file2[0].size > MaxUploadSize.size) {
            alertify.alert('Info','파일 최대 업로드 용량을 초과하였습니다.');
            return;
        }

        var fileDesc = $scope.file2[0].name;

        if ($scope.file2 != null) {
            // 로딩바
            $('#isLoading').css('display', '');
            var target = document.getElementById('spinnerContainer');
            var spinner = new Spinner().spin(target);

            mainDataService.fileUpload(fileDesc, 'minwon', $scope.file2, '', '').success(function (obj) {
                if (!common.isEmpty(obj.errMessage)) {
                    alertify.error('errMessage : ' + obj.errMessage);
                } else {
                    if (parseInt(obj.f_num) > 0) {
                        if (!common.isEmpty(obj.errMessage)) {
                            alertify.alert(obj.errMessage).set('basic', true);
                        } else {
                            mainDataService.insertMinwonFile({ // M2_BBS_FILE에 넣는다
                                complain_sid : complain_sid,
                                f_num       : obj.f_num,
                                photo_yn : 'N'
                            }).success(function (data) {
                                $('#isLoading').css('display', 'none');
                                spinner.stop();

                                if (!common.isEmpty(data.errMessage)) {
                                    alertify.error('errMessage : ' + data.errMessage);
                                } else {
                                    alertify.success('파일 업로드 성공');
                                    $scope.loadFileList_popup(complain_sid, 'N'); //현재 글 번호 넘겨서 파일 리스트 불러오기
                                }
                                $files = null;
                                $scope.file2 = null;
                                $scope.fileSelected = false;                                
                            });
                        }
                    } else {
                        alertify.alert('','파일 업로드 실패');
                        $('#isLoading').css('display', 'none');
                        spinner.stop();
                    }
                }
            });
        }
    };

    // 파일삭제 (X 버튼 눌렀을 때 하나씩 삭제)
    $scope.deleteFile = function (file) {
        if (sessionStorage.getItem($scope.MCODE + 'E') != 'true' || sessionStorage.getItem($scope.MCODE + 'V') != 'true') {
            alertify.alert('','권한이 없습니다.');
            return;
        }

        var complain_sid = '';

        if ($scope.saveType === 'new') {
            complain_sid = $scope.minwon_current_sid;
        } else if ($scope.saveType === 'update') {
            complain_sid = $scope.MinwonInfoPopup.COMPLAIN_SID; // 수정일때
        }

        if (!(complain_sid > 0)) {
            alertify.alert('','민원을 선택해주세요');
            return;
        }

        alertify.confirm('삭제 확인', '정말 삭제하시겠습니까?',
            function () {
                //1. 경로에서 파일 삭제, 2. M2_FILE 삭제
                mainDataService.fileDelete(file.F_NUM).success(function (obj) {
                    if (!common.isEmpty(obj.errMessage)) {
                        alertify.error('errMessage : ' + obj.errMessage);
                    } else {
                        if (parseInt(obj) > 0) {
                            mainDataService.deleteMinwonFileOne({ //M2_COMPLAIN_FILE 삭제
                                complain_sid     : complain_sid,
                                f_num       : file.F_NUM
                            }).success(function (obj) {
                                if (!common.isEmpty(obj.errMessage)) {
                                    alertify.error('errMessage : ' + obj.errMessage);
                                } else {
                                    if (obj == '1') {
                                        alertify.success('삭제되었습니다.');
                                        $scope.loadFileList_popup(complain_sid, 'N');
                                    } else {
                                        alertify.alert('error' + obj).set('basic', true);
                                    }
                                }
                            });
                        }
                    }
                });
            }, function () {}).set('basic', false);
    };

    // 파일리스트 가져오기
    $scope.loadFileList = function (minwon_sid, isPhoto){
        mainDataService.getMinwonFileList({
            complain_sid: minwon_sid,
            photo_yn: isPhoto
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                $scope.storeFileList = obj;
            }
        });
    }

    // 파일리스트 가져오기 (팝업)
    $scope.loadFileList_popup = function (minwon_sid, isPhoto) {
        mainDataService.getMinwonFileList({
            complain_sid: minwon_sid,
            photo_yn: isPhoto
        }).success(function (obj) {
            if (!common.isEmpty(obj.errMessage)) {
                alertify.error('errMessage : ' + obj.errMessage);
            } else {
                $scope.storeFileList_popup = obj;
            }
        });
    }

    // 엑셀 파일 생성 및 다운로드. index.html에서 exportForm에 검색 대상을 지정해야 함
    $scope.getExcelDownload = function() {
        var jobType = '010205';
        var year = '';
        var s_date = '';
        var e_date = '';
        var type = $scope.searchDateType || '';
        var category = $scope.searchCategory; // 민원유형

        // 연도 검색
        if ($scope.searchDateType === 1) {
            year = $scope.selectedCurrYear || '';

            // 기간선택 검색
        } else {
            s_date = $scope.searchDate.S_DATE || '';
            e_date = $scope.searchDate.E_DATE || '';

            // 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
            if (!checkDate($scope.searchDate.S_DATE, $scope.searchDate.E_DATE, '')) {
                return;
            }
        }

        $('#exportForm').find('[name=jobType]').val(jobType);
        $('#exportForm').find('#s_date').val(s_date);
        $('#exportForm').find('#e_date').val(e_date);
        $('#exportForm').find('#year').val(year);
        $('#exportForm').find('#category').val(category);
        $('#exportForm').find('#type').val(type);
        $('#exportForm').submit();
    };

    //자산 선택
    $scope.assetSelection = function() {
        $rootScope.$broadcast('showGisPopup',{callbackId:'assetAddFromSelectGisInfo'});
    }

    $scope.$on('assetAddFromSelectGisInfo',function(event,data){
        var list = new Array();
        $.each(data.data,function(idx,Item){
            list.push(Item.ftr_idn);
        });
        var param = {FTR_IDN : list.toString() ,LAYER_CD: data.data[0].fid };

        // https://www.jigi.net/develop/5654/  jqgrid

        var lastRowNum = $("#list_asset_popup").getGridParam("reccount");

        mainDataService.getAssetFromGisInfo(param).success(function (data) {
            if (!common.isEmpty(data.errMessage)) {
                alertify.alert('errMessage : ' + obj.errMessage);
            } else {
                var rowData = $("#list_asset_popup").getRowData();
                var lastRowData = $("#list_asset_popup").getRowData();
                var lastRowId = $("#list_asset_popup").find('tbody tr:last').get(0).id;

                // 기존 데이터가 없다면 set grid param
                if ($scope.assetInfo_popup.getRowData().length === 0) {
                    $scope.assetInfo_popup.clearGridData();
                    $scope.assetInfo_popup.setGridParam({
                        datatype: 'local',
                        data    : data,
                    }).trigger('reloadGrid');

                    // setgrid 후에 첫번째 요소부터 인덱스 번호를 순차대로 넣는다 (1,2,3...)
                    var firstRowId = $("#list_asset_popup").find('tbody tr:eq(1)').get(0).id;
                    var firstRowId_onlyNum = Number(firstRowId.replace(/[^0-9]/g,''));
                    for (var i = 0; i < data.length; i++) {
                        $("#list_asset_popup").find("#jqg"+(firstRowId_onlyNum+i)).find("td[aria-describedby=list_asset_popup_NUM]").html(i+1);
                    }

                    // 기존 데이터가 있다면 add row data
                } else {
           			var assetData = $("#list_asset_popup").getRowData();
           			var asset_sid_list = [];
           			var add_asset_list = [];
           			$.each(assetData,function(idx,Item){
           				asset_sid_list.push(Item.ASSET_SID);
           			})
                	$.each(data,function(idx,Item){
                		if($.inArray(Item.ASSET_SID,asset_sid_list)<0) add_asset_list.push(Item);
                	});
                	
                    $("#list_asset_popup").jqGrid("addRowData", lastRowNum+1, add_asset_list, 'last'); // 마지막 row의  다음 행부터 add
                    var lastRowId_onlyNum = Number(lastRowId.replace(/[^0-9]/g,''));
                    for (var i = 0; i < add_asset_list.length; i++) {
                        $("#list_asset_popup").find("#jqg"+(lastRowId_onlyNum+i+1)).find("td[aria-describedby=list_asset_popup_NUM]").html(i+1+lastRowNum);
                    }
                }
            }
        });
    });

    $scope.assetDelete = function () {
        var rowIds = $('#list_asset_popup').getGridParam('selarrrow');
        console.log(rowIds);
        rowIds = rowIds.reverse();
        for(var idx=rowIds.length-1;idx>=0;idx--){
        	$('#list_asset_popup').jqGrid('delRowData', rowIds[idx]);	
        }
        
    	
    };

    $scope.search_picture = function(){
        mainDataService.getMinwonAssetPic({
            COMPLAIN_SID : $scope.minwon_current_sid,
            ASSET_SID : $scope.AssetSid,
            PHOTO_YN : 'Y',
        }).success(function(data){
            $scope.pic_list = data;
        });
    }

    $scope.uploadPic = function (picInfo){
        console.log(picInfo);
        $rootScope.$broadcast("showImageUploadPopup", {
            'picInfo' : picInfo //ASSET_SID, SEQ
            }
        );

        $rootScope.callback_savePicture = function(obj){
            console.log(picInfo);
            console.log(obj);
            var param = {
                complain_sid: $scope.minwon_current_sid,
                AssetSid: picInfo.ASSET_SID,
                F_NUM:obj.f_num,
                f_desc : obj.F_DESC,
                photo_yn: 'Y',
                photo_num: picInfo.SEQ
            };
            mainDataService.insertMinwonFile(param)
                .success(function(data){
                    console.log(data);
                    $scope.search_picture();
                });
        }
        $scope.image_popupInfo = [];
        $('#dialog-imageUpload-popup2').show();
    }

    $scope.showPic = function(pic){
    	$rootScope.$broadcast("popupShowPicture",{URL:pic.F_PATH,DESC:pic.F_DESC,pic,callbackDeletePic:function(data){
            console.log(data);
            var picInfo = data.pic;
            var param = {
                complain_sid: $scope.minwon_current_sid,
                // AssetSid: picInfo.ASSET_SID,
                F_NUM: picInfo.F_NUM,
            };
            mainDataService.deleteMinwonFileOne(param).success(function(data){
                console.log(data);
                alertify.success('삭제되었습니다.');
                $scope.search_picture();
            });
        },
        updateDlgId : '#dialog-imageUpload-popup2',
        callbackUpdatePic:function(data){

        	var picInfo = data;
            var param = {
                complain_sid: $scope.minwon_current_sid,
                F_NUM: (picInfo.f_num>0 && typeof picInfo.pic !='undefined' )?picInfo.pic.F_NUM:picInfo.F_NUM,
            };
            if(picInfo.f_num>0 && typeof picInfo.pic !='undefined' ){
            	picInfo.ASSET_SID = picInfo.pic.ASSET_SID;
            	picInfo.SEQ = picInfo.pic.SEQ;
            }

            mainDataService.deleteMinwonFileOne(param)
            .success(function(data2){
            	//console.log('삭제되었습니다.');
                var param = {
                    complain_sid: $scope.minwon_current_sid,
                    AssetSid: picInfo.ASSET_SID,
                    F_NUM:(picInfo.f_num>0 && typeof picInfo.pic !='undefined' )?picInfo.f_num:picInfo.F_NUM,
                    f_desc : picInfo.F_DESC,
                    photo_yn: 'Y',
                    photo_num: picInfo.SEQ
                };
                mainDataService.insertMinwonFile(param).success(function(data) {
                    console.log(data);
                    $scope.search_picture();
                });
                $scope.search_picture();
            });
            $('#dialog-imageUpload-popup2').hide();
        }
      });
    }

    $scope.ShowAssetGisInfoPopup = function() {
    	
        var param = { ASSET_SID: $scope.asset.ASSET_SID };
        mainDataService.getAssetGisInfo(param).success(function(data) {
        	console.log(data);
        	if(data==null|| data==''){
        		alertify.alert('info','시설자산은 위치정보가 없습니다.');
        		return;
        	}
			if(data.GEOM_TEXT=="") {
				alertify.alert("Info","표시할 위치정보가 없습니다.",function(){});
				return;
			}
            $rootScope.$broadcast('showGisPopup', {
				ASSET_INFO : {ASSET_SID : $scope.asset.ASSET_SID, FTR_IDN : data.FTR_IDN},
				gisPopupMode : 'gisView',            	
                init: function() {
                    var layer_name = data.LAYER;
                    var ftr_idn = '\'' + data.FTR_IDN + '\'';
                    showAsset(layer_name, ftr_idn);
                },
                callbackId: 'RegistAssetItem_selectGisInfo'
            });
        });
    };
    
    $scope.DownloadReport = function(){
    	window.open('/excel/downloadExcel.do?EXCEL_ID=010205&COMPLAIN_SID=' + $scope.minwon_current_sid, '_blank');
    }
    
    $scope.showPopupLevelPath = function (opt,idx)
    {
    	//alert('showpopup')
    	$scope.level_path_nm = "";
    	$("#dialog-assetlevelpath").show();
    	//$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'multi'});
    	$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'costlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'path',opt:opt,idx:idx});
    }
    $scope.$on('costlist_selectAssetLevelPath',function(event,data){
    	console.log(data);
    	$scope.asset_path_sid = data.ASSET_PATH_SID;
    	$scope.level_path_cd = data.LEVEL_PATH_CD;
    	$scope.level_path_nm = data.LEVEL_PATH_NM;
    	
    	//$("#dialog-AssetListPopup").show();
    	if(data.ASSET_PATH_SID==0){
    		data.ASSET_PATH_CD = data.LEVEL_PATH_CD;
    	}
    	//if(data.opt==2){
    		data.callbackId = 'costlist_selectedAssetList';
    		$rootScope.$broadcast("SelectAssetListPopup",data);
    	//}
    });
    
    $scope.$on('costlist_selectedAssetList',function(event,data){
    	console.log(data);
   		var lastRowNum = $("#list_asset_popup").getGridParam("reccount");
   		var lastRowId = $("#list_asset_popup").find('tbody tr:last').get(0).id;
   		var list = [];
    		
    		$.each(data.list,function(idx,Item){
    			var FN_CD = Item.ASSET_CD.substring(11,12);
    			var ASSET_PATH_CD = Item.ASSET_CD.substring(0,4) + '_'
						+ Item.ASSET_CD.substring(4,11) +'_'
						+ Item.ASSET_CD.substring(11,12) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(12,15):Item.ASSET_CD.substring(12,13)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(15,18):Item.ASSET_CD.substring(13,14)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(18,19):Item.ASSET_CD.substring(14,17)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(19,22):Item.ASSET_CD.substring(17,22)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(22,27):'_')+'_______';
				if(Item.ASSET_CD !='' && Item.ASSET_CD != undefined)	
    			list.push({NUM:lastRowNum +1+idx,ASSET_SID:Item.SID,ASSET_CD:Item.ASSET_CD,ASSET_NM:Item.ASSET_NM
    				,ASSET_PATH_CD: ASSET_PATH_CD  					
   					});
    		});
    		/*
            $("#list_asset_popup").jqGrid("addRowData", lastRowNum+1, list, 'last');
            var lastRowId_onlyNum = Number(lastRowId.replace(/[^0-9]/g,''));
            for (var i = 0; i < data.length; i++) {
                $("#list_asset_popup").find("#jqg"+(lastRowId_onlyNum+i+1)).find("td[aria-describedby=list_asset_popup_NUM]").html(i+1+lastRowNum);
            }*/
   			var assetData = $("#list_asset_popup").getRowData();
   			var asset_sid_list = [];
   			var add_asset_list = [];
   			$.each(assetData,function(idx,Item){
   				asset_sid_list.push(Item.ASSET_SID);
   			})
        	$.each(list,function(idx,Item){
        		if($.inArray(Item.ASSET_SID,asset_sid_list)<0) add_asset_list.push(Item);
        	});
        	
            $("#list_asset_popup").jqGrid("addRowData", lastRowNum+1, add_asset_list, 'last'); // 마지막 row의  다음 행부터 add
            var lastRowId_onlyNum = Number(lastRowId.replace(/[^0-9]/g,''));
            for (var i = 0; i < add_asset_list.length; i++) {
                $("#list_asset_popup").find("#jqg"+(lastRowId_onlyNum+i+1)).find("td[aria-describedby=list_asset_popup_NUM]").html(i+1+lastRowNum);
            }    		
        	
        	alertify.alert('Info','선택적용 되었습니다.',function(){
        		
        	});            
    });
}