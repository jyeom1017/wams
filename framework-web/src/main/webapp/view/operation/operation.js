angular.module('app.operation').controller('costManageController',costManageController);
function costManageController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval,$filter, ConfigService, YearSet, GridConfig, Message) {
	
	$scope.pageNo = 1;
	//$scope.totalCount = 1000000;
	$scope.SelectedAssetList = [];
	var strExpenseItemCd = '';
	var strManageItemCd = '';
	//불러오기 팝업 선택목록
	$scope.storeCdList = [{cd:'01',name:'조사및탐사'},{cd:'02',name:'개보수'}];
	
	$scope.loadStoreCd = $scope.storeCdList[0];
	
	$scope.UploadExcel = function(){
		$("#input_excel_file").trigger('click');
	}
	
	$scope.$on('$viewContentLoaded', function() {
		$("#tab1_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab2_btn, #tab3_btn").removeClass("active");
    		$("#tab1").show();
    		$("#tab2, #tab3").hide();
    	});
    	$("#tab2_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab1_btn, #tab3_btn").removeClass("active");
    		$("#tab2").show();
    		$("#tab1, #tab3").hide();
    	});
    	
    	mainDataService.getCommonCodeList({
            gcode : 'COST', //지출구분코드
            gname : ''
        }).success(function(data) {
            if (!common.isEmpty(data.errMessage)) {
                alertify.error('errMessage : ' + data.errMessage);
                strExpenseItemCd = '01:초기투자비;02:수리비;03:유지비;04:운영비;05:개선비;06:해체폐기비;';
            } else {
            	strExpenseItemCd = '';
                $.each(data,function(idx,Item){
                	//01:평균;02:구간;03:최소;04:최대
                	if(idx>0) strExpenseItemCd +=';';
                	strExpenseItemCd += Item.C_SCODE + ':'+ Item.C_NAME;
                });
            }
        	mainDataService.getCommonCodeList({
                gcode : 'OPRT', //운영비코드
                gname : ''
            }).success(function(data) {
            	if (!common.isEmpty(data.errMessage)) {
                    alertify.error('errMessage : ' + data.errMessage);
                    strManageItemCd = ':;01:운영비;02:기술진단비;03:안전진단비;04:검교정비;05:기타;';
                    
                } else {
                	strManageItemCd = '';
                    $.each(data,function(idx,Item){
                    	//01:평균;02:구간;03:최소;04:최대
                    	if(idx==0) strManageItemCd +=':;';
                    	if(idx>0) strManageItemCd +=';';
                    	strManageItemCd += Item.C_SCODE + ':'+ Item.C_NAME; 	
                    });
                }
            	setGrid();
            });
            
        });
    	
    	//setGrid();
    	setGrid2();
       	/*mainDataService.getCommonCodeList({
            gcode : 'YEAR',
            gname : ''
        }).success(function(data) {
            if (!common.isEmpty(data.errMessage)) {
                alertify.error('errMessage : ' + data.errMessage);
            } else {
            	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
                $scope.ComCodeYearList = data;
            }
        });*/
	});
	
	function removeGrid(gridId){
		$("#listHolder").html("");
		//$("#list").remove();
		//$("#listPager").remove();
		//$("#list_page").remove();
		
		$("#listHolder").append("<div class=\"grid table-responsive table_scroll\"><table id=\"list\" class=\"table\"></table></div>");
		//$("#listHolder").append("<div class=\"grid table-responsive  w_98 border_color\"><table id=\"list\" class=\"table\"></table></div>");
		$("#listHolder").append("<div id=\"listPager\" class=\"w_98\"></div>");
		$("#listHolder").append("<div id=\"list_page\" class=\"w_98\"></div>");

	}
	
	$scope.checkExpenseItem = function(gridId){
		
		//if($scope.lastEditCellId !=gridId) return true;
		var gridData = $("#list").jqGrid("getRowData",gridId);
		console.log(gridId);
		console.log(gridData.EXPENSE_ITEM);
			if(gridData.EXPENSE_ITEM=='04') return true;
			else return false;
		
	}
	
	$scope.totalCount = 0;
	function setGrid() {
    	var colModel = [{
            name : 'RNUM',
            sortable:false,
            width : 20
        }, {
            name : 'COST_SID',
            sortable:false,
            width : 20,
            hidden : true
        }, {
            name : 'ASSET_SID',
            width : 20,
            hidden : true
        }, {
            name : 'PATH_CD',
            width : 20,
            hidden : true
        }, {
            name : 'COST_DT',
            hidden : true
        }, {
            name : 'COST_DT_CONTROL',
            index : 'COST_DT',
            width : 50
            ,editable:true
            ,formatter : function(cellvalue, options, rowdata, action){ if(rowdata.RNUM==null) return ''; return rowdata.COST_DT;}
            ,editoptions:{size:20, dataInit:function(el,info){
            	//console.log(el);
            	//info.rowId;
            	var gridData = $(this).jqGrid('getRowData',info.rowId);
            	var value = str2Time2(gridData.COST_DT);
            	//alert(gridData.COST_DT);
            	//alert(value);
                $(el).datepicker({dateFormat:'yy-mm-dd'});
                $(el).datepicker("setDate",value);
                }, defaultValue: ''/*function(){ 
                                            var currentTime = new Date(); 
                                            var month = parseInt(currentTime.getMonth() + 1); 
                                            month = month <= 9 ? "0"+month : month; 
                                            var day = currentTime.getDate(); 
                                            day = day <= 9 ? "0"+day : day; 
                                            var year = currentTime.getFullYear(); 
                                            return year+"-"+month + "-"+day;}*/
            }

        }, {
            name : 'DAY',
            sortable:false,
            width : 20,
        }, {
            name : 'ASSET_NM',
            hidden: true
        }, {
            name : 'ASSET_NM_CONTROL',
            //cellattr: function () { return ' title=""'; },
            sortable:false,
            width : 80,
            formatter: function(cellvalue, options, rowdata, action) {
            	if (rowdata.RNUM==null) return "";
            	var shtml = "<div title=''>" + rowdata.ASSET_NM + "</div>";
            	if(rowdata.RNUM !=null){
	            	if($scope.EditGridMode){
	            		shtml = "<div title=''>" + rowdata.ASSET_NM  + "<span style=\"width:50px;\" id="+options.rowId+"_asset_nm"+" ></span></div>";	
	            		//console.log(rowdata);
	            		//console.log(options);
	            		//console.log(action);
	            		$timeout(function(){
	            			$("#"+options.rowId+"_asset_nm").html($compile("<button ng-click=\"showPopupLevelPath(3,'"+options.rowId+"')\" style=\"height:15px;width:15px; outline: none;\" class=\"search_btn\"><i class=\"fa fa-search\"></i></button>")($scope));
	            		},500);
	           		}
            	}
            	return shtml;
            }
        }, {
            name : 'ASSET_CD',
            sortable:false,
            width : "100px",
        }, {
            name : 'EXPENSE_ITEM'
           	,hidden : false
            ,edittype : 'select', formatter : 'select', editoptions : {value :strExpenseItemCd} , editrules : {required:true}
        	,width : 50
        }, {
            name : 'REPEAT_YN'
            ,hidden : true
        }, {
            name : 'REPEAT_YN_CONTROL',
            index : 'REPEAT_YN',
            //cellattr: function () { return ' title=""'; },
            sortable:true,
            width : 20,
            formatter :  function(cellvalue, options, rowdata, action) {
            	if (rowdata.RNUM==null) return "";
            	var shtml ="<div title=''><input disabled type=checkbox style=\"min-width:0px;height:15px;\" " + ((rowdata.REPEAT_YN=='Y')?"checked":"") +" /></div>";
            	if(rowdata.RNUM !=null){
	            	if($scope.EditGridMode ){
	            		shtml = "<div title=''  style=\"width:50px;\" id="+options.rowId+"_repeat_yn"+" ></div>";	            		
	            		//console.log(rowdata);
	            		//console.log(options);
	            		//console.log(action);
	            		$timeout(function(){
	            			$("#"+options.rowId+"_repeat_yn").html($compile("<input style=\"display:"+((rowdata.EXPENSE_ITEM=='04')?"auto":"none")+";min-width:0px;height:15px;\" ng-click=\"updateRepeatYn('"+options.rowId+"')\"  type=checkbox  " + ((rowdata.REPEAT_YN=='Y')?"checked":"") +" />")($scope));
	            		},500);
	           		}
            		
            	}
            	return shtml;            	

            }
        }, {
            name : 'PERIOD_MONTH'
            ,sortable:true
            ,formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":((rowdata.REPEAT_YN=='Y')?(parseInt(rowdata.PERIOD_MONTH)>0?rowdata.PERIOD_MONTH:1):"");}
            ,width : 20
        }, {
            name : 'MANAGE_ITEM'
           	,hidden : false
           	,sortable:true
           	,edittype : 'select', formatter : 'select', editoptions : {value :strManageItemCd}
        	,width : 70
        }, {
            name : 'COST_AMT',
            formatter:function(cellvalue,options,rowdata,action){
            	console.log(cellvalue);
            	return (rowdata.RNUM==null)?"":$filter('currency')(("" + cellvalue).replace(/,/gi,''),'',0);
            	}, 
            width : 70
        }, {
            name : 'COST_DESC',
            sortable:false,
            width : 0,
            formatter:function(cellvalue,options,rowdata,action){return xssFilter(cellvalue);},
        }, {
            name : 'L1',
            sortable:false,
            formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,1);},
            width : 60,
        }, {
            name : 'L2',
            sortable:false,
            formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,2);},
            width : 60,
        }, {
            name : 'L3',
            sortable:false,
            formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,3);},
            width : 60,
        }, {
            name : 'L4',
            sortable:false,
            formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,4);},
            width : 60,
        }, {
            name : 'L5',
            sortable:false,
            formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,5);},
            width : 60,
        }, {
            name : 'L6',
            sortable:false,
            formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,6);},
            width : 60,
        }, {
            name : 'L7',
            sortable:false,
            formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,7);},
            width : 60,
        }, {
            name : 'L8',
            sortable:false,
            formatter:function(cellvalue,options,rowdata,action){return (rowdata.PATH_CD==null)?"":getLevelName(rowdata.PATH_CD,8);},
            width : 60,
        }
        ];		
        return pagerJsonGrid({
            grid_id : 'list',
            pager_id : 'listPager',
            url : '/operation/getCostList.json',
            page : 1,
            rows : GridConfig.sizeXS,
        	'cellEdit' : $scope.EditGridMode,
        	'cellsubmit' : 'remote',
        	'cellurl' : '/operation/setCostItem.json',
            condition : {
                //TOTCOUNT : $scope.totalCount,
				TOTCOUNT : $scope.totalCount,
				ASSET_LIST : $scope.SelectedAssetList.toString(),
				ASSET_NM : $scope.SearchAssetNm,
				ASSET_CD : $scope.SearchAssetCd,
				EXPENSE_ITEM : $scope.SearchExpense,
				START_DT : $scope.StartDt,
				END_DT : $scope.EndDt
            },
            beforeSaveCell : function(rowid, name, value, iRow, iCol){
            	var gridData = $(this).jqGrid('getRowData',rowid);
            	console.log(gridData);
            	if(gridData.RNUM == "") return '';
            	
            	if(name=='PERIOD_MONTH'){
            		if(gridData.REPEAT_YN=='N')	return '';
            		return value.replace(/[^0-9]/g,''); 
            	}
            	if(name=='COST_AMT'){
            		return value.replace(/[^0-9]/g,''); 
            	}            	
            	if(name=='EXPENSE_ITEM'){
            		if((value!='01' && $scope.beforeEditCellExpenseItem =='01') || (value=='01' && $scope.beforeEditCellExpenseItem !='01')){
            			alertify.alert('info','초기투자비 항목은 항목변경불가',function(){});
            			return $scope.beforeEditCellExpenseItem;
            		}
            		
            		if(value!='04' && gridData.MANAGE_ITEM=='') $("#"+rowid+"_repeat_yn").find("input[type=checkbox]").hide();
            		else $("#"+rowid+"_repeat_yn").find("input[type=checkbox]").show();
            		
            		if(gridData.MANAGE_ITEM!='') 
            			return '04';
            	}
            	if(name=='MANAGE_ITEM'){
            		if(gridData.EXPENSE_ITEM!='04') 
            			return '';            		
            	}
            	
            	return value;
            },
            beforeEditCell : function(rowid, name, value, iRow, iCol){
            	$scope.lastEditCellId = rowid;
            	$scope.lastEditColumnNo = iCol;
            	var gridData = $(this).jqGrid('getRowData',rowid);
            	if(gridData.RNUM == "")
            		$("#list").jqGrid('reloadGrid');
            	$scope.beforeEditCellExpenseItem = gridData.EXPENSE_ITEM;
            },
            beforeSubmitCell: function (rowid, name, value, iRow, iCol) {
            	//console.log(rowid);
            	//console.log(name);
            	//console.log(value);
            	//console.log(iRow);
            	//console.log(iCol);
            	var gridData = $(this).jqGrid('getRowData',rowid);
            	if(gridData.COST_SID=="") return false;
            	//var gridData = {COST_SID:row}
            	var param  = {COST_SID:gridData.COST_SID} ;
            	if(name=='COST_DT_CONTROL') name=='COST_DT';
            	if(name=='COST_DESC') value=xssFilter(value);
            	param[name] = value;
                return param;
            },
            afterSubmitCell : function(grid,rowid, name, value, iRow, iCol){
            	var gridData = $(this).jqGrid('getRowData',rowid);
            	if(name == 'COST_DT_CONTROL'){
           		gridData.COST_DT = xssFilter(value);
        		gridData.DAY = $scope.calcYoil(gridData); 
        		$("#list").jqGrid('setRowData',rowid,gridData);
            	}
            	//alertify.success('저장했습니다.');
            },
            serializeCellData: function (postdata) {
                //return JSON.stringify(postdata);
            	return postdata;
            },            
            rowNum : 20,
            colNames : [ '순번','COST_SID','ASSET_SID','PATH_CD','일자','일자 *','요일','자산명','자산명 *','자산코드','지출항목 *','반복','반복','주기(월)','운영비항목','비용 *','내용','레벨1','레벨2','레벨3','레벨4','레벨5','레벨6','레벨7','레벨8'],
            colModel : colModel,
            mtype: "POST",       //요청방식 GET/POST
            onSelectRow : function(id) {
                $scope.CostInfo = $('#list').jqGrid('getRowData', id);
                console.log($scope.CostInfo);

            },
/* 			,cellattr : function(rowid, value, rawObject, cm, rdata){ 
            	//필요에 따라서 if 조건문을 걸어 조건에 맞는 cell만 편집 방지 
            	//return "class='not-editable-cell'";
            	return "class='editable-cell'"; 
            },*/
        	ondblClickRow	:
    			function(id,colId,val){
    				$("#list").setColProp('EXPENSE_ITEM',{editable:$scope.EditGridMode});		// 특정 cell 수정가능여부 바꿔주기 => $(#jpgrid_id).setColProp('data_fmt_cd'.{editable:true/false})
    				$("#list").setColProp('MANAGE_ITEM',{editable:$scope.EditGridMode});
    				$("#list").setColProp('PERIOD_MONTH',{editable:$scope.EditGridMode});		// 특정 cell 수정가능여부 바꿔주기 => $(#jpgrid_id).setColProp('data_fmt_cd'.{editable:true/false})
    				//$("#list").setColProp('REPEAT_YN',{editable:$scope.EditGridMode});
    				$("#list").setColProp('COST_AMT',{editable:$scope.EditGridMode});
    				$("#list").setColProp('COST_DESC',{editable:$scope.EditGridMode});
    				$("#list").setColProp('COST_DT_CONTROL',{editable:$scope.EditGridMode});
    				//$("#list").setColProp('ASSET_NM',{editable:$scope.EditGridMode});
					/*$("#list").jqGrid('setCell',id, 'EXPENSE_NM', '', 'editable-cell');
					$("#list").jqGrid('setCell',id, 'PERIOD_MONTH', '', 'editable-cell');
					$("#list").jqGrid('setCell',id, 'REPEAT_YN', '', 'editable-cell');
					$("#list").jqGrid('setCell',id, 'MANAGE_ITEM', '', 'editable-cell');
					$("#list").jqGrid('setCell',id, 'COST_AMT', '', 'editable-cell');
					$("#list").jqGrid('setCell',id, 'COST_DESC', '', 'editable-cell');
					*/
   			},            
            gridComplete : function() {
				var ids = $(this).jqGrid('getDataIDs');
				//alert(ids);
				//if ($.isEmptyObject($scope.AssetItemInfo)) {
					//$(this).setSelection(ids[0]);
				//}
				//alert($('#list').getGridParam('page'));
				$scope.currentPageNo=$('#list').getGridParam('page');
				$scope.totalCount = $('#list').getGridParam("records");
				$scope.$apply();
				// if ($.isEmptyObject($stateParams.info)) {
				// $('#facility').setSelection(ids[0]);
				// }
			}                  
        });
    }
	
	$scope.updateRepeatYn = function(gridId){
		var gridData = $("#list").jqGrid('getRowData',gridId);
		if(gridData.COST_SID=="") return;
		gridData.REPEAT_YN = (gridData.REPEAT_YN!='Y')?'Y':'N';
		$("#list").jqGrid('setRowData',gridId,gridData);
		var param = 'COST_SID=' + gridData.COST_SID +'&REPEAT_YN='+gridData.REPEAT_YN;
		mainDataService.updateCost(param)
		.success(function(data){
			//alertify.success('저장했습니다.');
		});		
	}
	
	var list2ColModel = [
        {
            name : 'RNUM',
            width : 20
        }, {
            name : 'REPAIR_NM',
            width : 50,
            hidden : true
        }, {
            name : 'INSPECT_NM',
            width : 50,
            hidden : false
        }, {
            name : 'PERIOD_DT',
            width : 20,
        }, {
            name : 'INSERT_ID',
            width : 50,
        }, {
            name : 'INSERT_DT',
            width : 50,
        }, {
            name : 'REPAIR_SID',
            width : 0,
            hidden : true
        }, {
            name : 'REPAIR_START_DT',
            width : 0,
            hidden : true
        }, {          	
            name : 'INSPECT_START_DT',
            width : 0,
            hidden : true
        }, {        	
            name : 'INSPECT_SID',
            width : 0,
            hidden : true
        }];
	
	$scope.searchList2 = function(){
		setDatePicker();
		$("#list2").setGridParam({
    		datatype : 'json',
			page : 1,
			rows : GridConfig.sizeM,
			postData : {
				START_DT : $scope.StartDt,
				END_DT : $scope.EndDt,
				OPTION : '2'
			}
        }).trigger('reloadGrid');		
	}
	
	$scope.changeGrid = function(){
		setDatePicker();
	var URL = '/operation/getINSPECT.json';
		if($scope.loadStoreCd.cd=='02')
			URL = '/operation/getREPAIR.json';
			
		$.each(list2ColModel,function(idx,Item){
			if(Item.name == 'INSPECT_NM' && $scope.loadStoreCd.cd=='02') Item.hidden = true;
			if(Item.name == 'INSPECT_NM' && $scope.loadStoreCd.cd=='01') Item.hidden = false;			
			if(Item.name == 'REPAIR_NM' && $scope.loadStoreCd.cd=='02')	Item.hidden = false;
			if(Item.name == 'REPAIR_NM' && $scope.loadStoreCd.cd=='01')	Item.hidden = true;			
		});
		
		$("#list2").setGridParam({
    		datatype : 'json',
    		url : URL,
    		colModel : list2ColModel,
			page : 1,
			rows : GridConfig.sizeM,
			postData : {
				START_DT : $scope.StartDt,
				END_DT : $scope.EndDt,
				OPTION : '2'
			}
        }).trigger('reloadGrid');	
	}
	
	function setGrid2() {
        return pagerGrid({
            grid_id : 'list2',
            pager_id : 'listPager2',
            url : '/operation/getINSPECT.json',
            condition : {
                page : 1,
                rows : GridConfig.sizeM
            },
            rowNum : 10,
            colNames : [ '순번','사업명','사업명','사업기간','작성자','작성일','REPAIR_SID','START_DT','START_DT','INSPECT_SID'],
            colModel : list2ColModel,
            mtype: "",       //요청방식 GET/POST
            onSelectRow : function(id) {
            	
                $scope.AssetItemInfo = $('#list2').jqGrid('getRowData', id);
                console.log($scope.AssetItemInfo);
            }
        });
    }
	
	$scope.searchAssetName = function(){
		var data = {};
		 
		data.idx = ($scope.selectedCostIdx!=null)?$scope.selectedCostIdx:0;
		data.opt = 2;
		data.ASSET_NM = $scope.SearchAssetNm;
		data.callbackId = 'costlist_selectedAssetList';
		$rootScope.$broadcast("SelectAssetListPopup",data);
	}
	$scope.showPopupLevelPath = function (opt,idx)
    {
    	//alert('showpopup')
    	$scope.level_path_nm = "";
    	$("#dialog-assetlevelpath").show();
    	//$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'multi'});
    	$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'costlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'path',opt:opt,idx:idx});
    }
    $scope.$on('costlist_selectAssetLevelPath',function(event,data){
    	console.log(data);
    	$scope.asset_path_sid = data.ASSET_PATH_SID;
    	$scope.level_path_cd = data.LEVEL_PATH_CD;
    	$scope.level_path_nm = data.LEVEL_PATH_NM;
    	
    	//$("#dialog-AssetListPopup").show();
    	if(data.ASSET_PATH_SID==0){
    		data.ASSET_PATH_CD = data.LEVEL_PATH_CD;
    	}
    	//if(data.opt==2){
    		data.callbackId = 'costlist_selectedAssetList';
    		$rootScope.$broadcast("SelectAssetListPopup",data);
    	//}
    });
    
    $scope.$on('costlist_selectedAssetList',function(event,data){
    	console.log(data);
    	if(data.opt==3){
    		var gridData = $("#list").jqGrid('getRowData',data.idx);
    		console.log(gridData);
    		console.log(data.list);
    		gridData.ASSET_SID = data.list[0].SID;
    		gridData.ASSET_NM = data.list[0].ASSET_NM;
    		gridData.ASSET_CD = data.list[0].ASSET_CD;
    		if(gridData.COST_SID=="") return;
    		$("#list").jqGrid('setRowData',data.idx,gridData);
    		var param = 'COST_SID=' + gridData.COST_SID +'&ASSET_SID='+gridData.ASSET_SID;
    		mainDataService.updateCost(param)
    		.success(function(data){
    			alertify.success('저장했습니다.');
    		});
    		return;
    	}else if(data.opt==2 || data.opt==1){
    		$.each(data.list,function(idx,Item){
    			if( idx >= $scope.CostList.length - data.idx){
    				$scope.CostList.push({COST_DT : '',COST_DAY:'', ASSET_SID:Item.SID,ASSET_NM:Item.ASSET_NM,ASSET_CD:Item.ASSET_CD});
    			}
    				//var cost_dt = $scope.CostList[data.idx].COST_DT;
    				//var cost_day = $scope.CostList[data.idx].COST_DAY;
    				//$scope.CostList.splice(data.idx,0,{COST_DT : cost_dt,COST_DAY : cost_day, ASSET_SID:Item.SID,ASSET_NM:Item.ASSET_NM,ASSET_CD:Item.ASSET_CD});
    				$scope.CostList[data.idx+idx].ASSET_SID = Item.SID;
    				$scope.CostList[data.idx+idx].ASSET_NM = Item.ASSET_NM;
    				$scope.CostList[data.idx+idx].ASSET_CD = Item.ASSET_CD;
    		});    	
            $timeout(function(){
            	setDatePicker();
        	},500);
            //data.idx = $scope.CostList.length; 
    		return;
    	}
    	$scope.SelectedAssetList = [];
    	$.each(data.list,function(idx,Item){
    		$scope.SelectedAssetList.push(Item.SID);	
    	});
    });
    
    $(".mode_title").after().on("click",function(){
    	//$(".gis_wrap").css("visibility","hidden");
    	//$("#dialog-AssetListPopup").hide();
    });
    
    $scope.selectedCostIdx  = null;
    $scope.CostList = [];
    $scope.selectCost = function(idx)
    {
    	$scope.selectedCostIdx = idx;	
    }
    
    for(var i = 0;i<10;i++){
    	$scope.CostList.push({ASSET_SID:0,ASSET_NM:'',ASSET_CD:'',COST_DT:'',DAY:'',COST_AMT:0,REPEAT_YN:'',PERIOD_MONTH:0,COST_DESC:'',EXPENSE_ITEM:'',MANAGE_ITEM:''});
    }
    $timeout(function(){
		setDatePicker();
	},500);
    
    $scope.insertCostList = function(){
    	$scope.CostList.push({ASSET_SID:0,ASSET_NM:'',ASSET_CD:'',COST_DT:'',DAY:'',COST_AMT:0,REPEAT_YN:'',PERIOD_MONTH:0,COST_DESC:'',EXPENSE_ITEM:'',MANAGE_ITEM:''});
    	
    	$timeout(function(){
    		setDatePicker();
    	},500);
    }
    $scope.loadCostList = function(){
    	//개보수/조사및탐사 목록창이 뜨고 목록을 선택하면 자산명, 일자{준공일자(개보수)/종료일자(조사및탐사)}, 비용, 내용{공사내용(개보수)/조사내용(조사및탐사)} 항목을 불러와 채운다.
    	//$scope.$broadcast("SelectOperationList",{});
    	$("#SelectOperationListPopup").show();
        $timeout(function(){
    		setDatePicker();
    	},500);    	
    	
    }
    $scope.closeSelectOperationListPopup= function(){
    	$("#SelectOperationListPopup").hide();
        $timeout(function(){
    		setDatePicker();
    	},500);    	
    }
    $scope.applySelectOperationList= function(){
    	$("#SelectOperationListPopup").hide();
    	
    	if($scope.loadStoreCd.cd=='02'){
    		var param = "REPAIR_SID=" + $scope.AssetItemInfo['REPAIR_SID'] ;
    		//param +="&page=1";
    		//param +="&rows=1000";    		
        	mainDataService.getREPAIR_ASSET(param)
        	.success(function(data){
        		console.log(data);
        		$scope.CostList = [];
        		$.each(data.rows,function(idx,Item){
						$scope.CostList.push({
							COST_DT:("" + $scope.AssetItemInfo['REPAIR_START_DT']).replace(/-/gi,'')
							,DAY:''
							,ASSET_NM : Item['ASSET_NM']
							,ASSET_CD : Item['ASSET_CD']
							,EXPENSE_ITEM : ("0" + Item['EXPENSE_TYPE']).substr(-2)
							,MANAGE_ITEM : ""
							,REPEAT_YN : "N"
							,PERIOD_MONTH : "0"
							,COST_AMT : Item['REPAIR_AMT']
							,COST_DESC : Item['REPAIR_DESC']
						});
        		});
				$.each($scope.CostList,function(idx,Item){
					Item.DAY = $scope.calcYoil(Item);
					Item.IsRepeat = (Item.REPEAT_YN=='Y')?(true):(false);
					//Item.EXPENSE_ITEM  
					//Item.MANANGE_ITEM  
				});
	        	$timeout(function(){
	        		setDatePicker();
	        	},500);        		
        		//ASSET_SID,ASSET_CD,ASSET_NM,REPAIR_AMT
        	});    		
    	}else{
    		var param = "INSPECT_SID=" + $scope.AssetItemInfo['INSPECT_SID'];
    		//param +="&page=1";
    		//param +="&rows=1000";
    		
        	mainDataService.getINSPECT_ASSET(param)
        	.success(function(data){
        		console.log(data);
        		$scope.CostList = [];
        		$.each(data.rows,function(idx,Item){
						$scope.CostList.push({
							COST_DT:("" + $scope.AssetItemInfo['INSPECT_START_DT']).replace(/-/gi,'')
							,DAY:''
							,ASSET_NM : Item['ASSET_NM']
							,ASSET_CD : Item['ASSET_CD']
							,EXPENSE_ITEM : ("0" + Item['EXPENSE_TYPE']).substr(-2)
							,MANAGE_ITEM : ""
							,REPEAT_YN : "N"
							,PERIOD_MONTH : "0"
							,COST_AMT : Item['INSPECT_AMT']
							,COST_DESC : Item['INSPECT_DESC']
						});
        		});
				$.each($scope.CostList,function(idx,Item){
					Item.DAY = $scope.calcYoil(Item);
					Item.IsRepeat = (Item.REPEAT_YN=='Y')?(true):(false);
					//Item.EXPENSE_ITEM  
					//Item.MANANGE_ITEM  
				});
	        	$timeout(function(){
	        		setDatePicker();
	        	},500);        		
        		//ASSET_SID,ASSET_CD,ASSET_NM,INSPECT_AMT
        	});    		
    	}
        $timeout(function(){
    		setDatePicker();
    	},500);

    }
    $scope.uploadCostList = function(){
    	
    }
    $scope.downloadCostList = function(){
    	//양식다운로드
    	var expense_item_array = {'01':'초기투자비','02':'수리비','03':'유지비','04':'운영비','05':'개선비','06':'해체폐기비'};
    	var manange_item_array = {'01':'운영비','02':'기술진단비','03':'안전진단비','04':'검교정비','05':'기타'};
        const jobType = $state.current.name;
        $('#exportForm').find('[name=jobType]').val(jobType);
        var PARAM  = [];
        $.each($scope.CostList,function(idx,Item){
        	if(Item.ASSET_CD != '' || Item.ASSET_CD != null )
        	PARAM.push({
        			RNUM:idx+1
        			,COST_DT:Item.COST_DT
        			,DAY:Item.DAY
        			,ASSET_NM:Item.ASSET_NM
        			,ASSET_CD:Item.ASSET_CD
        			,EXPENSE_ITEM:expense_item_array[Item.EXPENSE_ITEM]
        			,MANAGE_ITEM:manange_item_array[Item.MANAGE_ITEM]
        			,COST_AMT:Item.COST_AMT
        			,REPEAT_YN:(Item.IsRepeat)?'Y':'N'
        			,PERIOD_MONTH:Item.PERIOD_MONTH
        			,COST_DESC:Item.COST_DESC
        			});
        });
        $('#exportForm').find('#PARAM').val(JSON.stringify({list:PARAM}));
        $('#exportForm').submit();    	
    }
    $scope.deleteCostList = function(){
    	//if()
    	var list = [];
    	$.each($scope.CostList,function(idx,Item){
    		if($scope.selectedCostIdx==idx){
    			
    		}else list.push(angular.copy(Item));
    	});
    	$scope.CostList = [];
    	$scope.CostList = list;
    	$timeout(function(){
    		setDatePicker();
    	},1000);
    }
    
    $scope.saveCostList = function(){
    	var param = {ITEM_LIST : []};
    	var flag = false;
    	$.each($scope.CostList,function(idx,Item){
    		if(Item.IsRepeat==true){
    			Item.REPEAT_YN= "Y"; 
    		}else if(Item.IsRepeat==false){
    			Item.REPEAT_YN= "N";
    		}
    		
    		if(Item.COST_DT !='' || Item.ASSET_CD !='' || Item.COST_AMT !='' || Item.EXPENSE_ITEM !=''){
    			if(Item.COST_DT ==''){
    				alertify.alert('info','일자를 입력하세요.');
    				flag = true;
    			}	
    			else if(Item.ASSET_CD ==''){
    				alertify.alert('info','자산명을  입력하세요.');
    				flag = true;
    			}
    			else if(Item.COST_AMT ==''){
    				alertify.alert('info','비용을 입력하세요');
    				flag = true;
    			}
    			else if(Item.EXPENSE_ITEM ==''){
    				alertify.alert('info','지출항목을 입력하세요');
    				flag = true;
    			}
    			else if(Item.EXPENSE_ITEM =='04' && Item.MANAGE_ITEM=='') {
    				alertify.alert('info','운영비 항목을 선탁하세요.');
    				flag = true;
    			}    			
    			else if(parseInt(Item.PERIOD_MONTH)<=0 && Item.IsRepeat==true) {
    				alertify.alert('info','반복비용에서 반복주기 0은 입력할 수 없습니다.');
    				flag = true;
    			}
    			else if(parseInt(Item.COST_AMT)<=0) {
    				alertify.alert('info','비용은 0 보다 큰 숫자로 입력하세요.');
    				flag = true;
    			}
    			if(flag) return false;
    		}
    		
    	});
    	if(flag){
    		//alertify.alert('info','입력값을 확인하세요');
    		return; 
    	}
    	
    	$.each($scope.CostList,function(idx,Item){
    		if(Item.COST_DT !='' && Item.ASSET_CD !='' && Item.COST_AMT !='' && Item.EXPENSE_ITEM !='')
    		param.ITEM_LIST.push({
    				COST_DT:Item.COST_DT
    				,ASSET_SID:Item.ASSET_SID
    				,ASSET_CD:Item.ASSET_CD
    				,EXPENSE_ITEM:Item.EXPENSE_ITEM
    				,MANAGE_ITEM:Item.MANAGE_ITEM
    				,REPEAT_YN:Item.REPEAT_YN
    				,PERIOD_MONTH:Item.PERIOD_MONTH
    				,COST_AMT : Item.COST_AMT
    				,COST_DESC : xssFilter(Item.COST_DESC) 
    				});
    	});
    	
    	mainDataService.saveCostList(param)
    	.success(function(data){
    		$scope.CostList = [];
    		for(var i=0;i<10;i++)
    		$scope.CostList.push({ASSET_SID:0,ASSET_NM:'',ASSET_CD:'',COST_DT:'',DAY:'',COST_AMT:0,REPEAT_YN:'',PERIOD_MONTH:0,COST_DESC:'',EXPENSE_ITEM:'',MANAGE_ITEM:''});
        	$timeout(function(){
        		setDatePicker();
        	},500);    		
    	});
    }
    $scope.ExpenseCdList = [];
    $scope.ManageCdList = [];

    mainDataService.getCommonCodeList({
		gcode : 'COST',
		name : ''
	}).success(function(obj) {
		// console.log(obj);
		$scope.ExpenseCdList = obj;
	});
    
    mainDataService.getCommonCodeList({
		gcode : 'OPRT',
		name : ''
	}).success(function(obj) {
		// console.log(obj);
		$scope.ManageCdList = obj;
	});
    setDatePicker();

    
    function getLevelName(pathCd,level){
		//console.log($rootScope.LevelName);
		//console.log(pathCd,level);
		var LEVEL_CD = pathCd.split('_')[level-1];
		if(LEVEL_CD!=''){
			var LevelName = $rootScope.LevelName[''+(level -1) +'_'+ LEVEL_CD];
			return LevelName;
		}else{
			return "";
		}
	}
    
    //----목록 다운로드
    
    //var colIndex = ['NUM','COST_SID','COST_DT','ASSET_NM','ASSET_CD','EXPENSE_NM','REPEAT_YN','PERIOD_MONTH','MANAGE_NM','COST_AMT','COST_DESC'];
    var colIndex = [{name:'순번',id:'NUM'},{name:'일자',id:'COST_DT'},{name:'요일',id:'DAY'},{name:'자산명',id:'ASSET_NM'},{name:'자산코드',id:'ASSET_CD'}
    	,{name:'지출항목',id:'EXPENSE_NM'},{name:'반복',id:'REPEAT_YN'},{name:'주기(월)',id:'PERIOD_MONTH'},{name:'운영비항목',id:'MANAGE_NM'},{name:'비용',id:'COST_AMT'},{name:'내용',id:'COST_DESC'}
    	];
    var rows = [];
    function loadNext(param){
    	mainDataService.getCostListExcel(param)
    	.success(function(Obj){
    		
    		if(Obj.length>0 ){
        		$.each(Obj,function(idx,Item){
        			var collumns = [];
        			if(idx==0 && param.PAGE_NO== 0 ){
        				var headers = [];				
        				$.each(colIndex,function(idx1,item){
        						headers.push(item.name);
        				});
        				rows.push(headers);
        			}
        			$.each(colIndex,function(idx1,item){
        				collumns.push(Item[item.id]);
        			});
        			rows.push(collumns);
        		});    		
   			param.PAGE_NO = param.PAGE_NO +1;
   			$( "#progressbar" ).progressbar({value: param.PAGE_NO /( $scope.totalCount / 100) * 100 });
    		loadNext(param);
    		}else{
        		const workSheetData = rows;
        		const workSheet = XLSX.utils.aoa_to_sheet(workSheetData);
        		//workSheet['!autofilter'] = {ref : "A1:R11"};
        		const workBook = XLSX.utils.book_new();
        		XLSX.utils.book_append_sheet(workBook, workSheet, '자산비용목록');
        		XLSX.writeFile(workBook, "자산비용목록_"+ $scope.getToday()+".xlsx");  
        		$("#dialog-progressBar").hide();
    		}
    	});    	
    }
    
    $scope.downloadExcel = function(){
    	rows=[];
    	var param={PAGE_NO:0,ROW_CNT:100,
				ASSET_LIST : $scope.SelectedAssetList.toString(),
				ASSET_NM : $scope.SearchAssetNm,
				ASSET_CD : $scope.SearchAssetCd,
				EXPENSE_ITEM : $scope.SearchExpense,
				START_DT : $scope.StartDt,
				END_DT : $scope.EndDt 
    			};
    	loadNext(param);

    	
    	$rootScope.$broadcast('ShowCommonProgressBar',{callback_fnc:function(){
    		//alert('test');
    		$( "#progressbar" ).progressbar({value: 0});
    	},alertMessage : '다운로드중입니다.'
    	});
    	//$scope.progress(0);
    }
    //--------------------목록 다운로드 끝
    
    $scope.searchList= function(pageNo){
    	if($scope.SearchModeOpt==1){
    		if($scope.SearchYear==""){
    			//alert('기간을 입력하 세요');
    			//$("#selectYear").focus();
    			//return;
            	$scope.StartDt = ""; 
               	$scope.EndDt =  "";    			
    		}else{
            	$scope.StartDt = $scope.SearchYear + "0101"; 
               	$scope.EndDt =  $scope.SearchYear + "1231";    			
    		}
    	}else{
    		if($("#start_dt").val()==""){
    			alert('기간을 입력하 세요');
    			$("#start_dt").focus();
    			return;
    		}
    		if($("#end_dt").val()==""){
    			alert('기간을 입력하 세요');
    			$("#end_dt").focus();
    			return;
    		}
            // 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
            if (!checkDate($("#start_dt").val(), $("#end_dt").val(), '')) {
                return;
            }
        	$scope.StartDt = $("#start_dt").val().replace(/-/gi,''); 
           	$scope.EndDt =  $("#end_dt").val().replace(/-/gi,'');    		
    	}
    	
    	//$scope.totalCount = 0;
    	
    	$("#list").setGridParam({
    		datatype : 'json',
			page : (typeof pageNo!='undefined')?pageNo:$scope.currentPageNo,
			rows : GridConfig.sizeS,
			postData : {
				TOTCOUNT : $scope.totalCount,
				ASSET_LIST : $scope.SelectedAssetList.toString(),
				ASSET_NM : $scope.SearchAssetNm,
				ASSET_CD : $scope.SearchAssetCd,
				EXPENSE_ITEM : $scope.SearchExpense,
				START_DT : $scope.StartDt,
				END_DT : $scope.EndDt
			}
        }).trigger('reloadGrid');
    }
    
    $scope.EditGridMode = false;
    $scope.changeMode= function(){
    	if($scope.EditGridMode){
    		$scope.EditGridMode = false;
    		$("#editBtn").html("수정");
    		$("#list").jqGrid("saveCell", $scope.lastEditCellId, $scope.lastEditColumnNo);
    	}else{
    		$scope.EditGridMode = true;
    		$("#editBtn").html("수정완료");
    		
    	}
		removeGrid();
		setGrid();
		$timeout(function(){
			//setGrid();
		},500);
    }
    $scope.deleteCost= function(){
    	if($scope.EditGridMode) {
    		alert('수정중에는 삭제할수 없습니다.');
    		return ;
    	}
    	alertify.confirm('삭제하시겠습니까?',function(){
    		console.log($scope.CostInfo);
    		var param = {COST_SID : $scope.CostInfo.COST_SID};
        	mainDataService.deleteCost(param)
        	.success(function(data){
        		console.log(data);
        		//alert('삭제되었습니다.');
        		alertify.success('Ok');
        		$scope.searchList();
        	});    		
    	},function(){
    		//alert('취소되었습니다.');
    	})
    	
    }
    $scope.SearchModeOpt = 1;
    $scope.SearchMode = function(Opt){
    	$scope.SearchModeOpt = Opt;
    }
    var sday = new Date(); // 일주일전
    sday.setDate(sday.getDate() - 7); // 일주일전으로 set
    var today = new Date(); // 오늘
    
    
    $scope.SearchYear = "" + sday.getFullYear();
    $scope.SearchStartDt = '' + today.getFullYear() + '-'+ ('0' + (today.getMonth()+1)).substr(-2) + '-'+ ('0'+ today.getDate()).substr(-2);
    $scope.SearchEndDt = '' + today.getFullYear() + '-'+ ('0' + (today.getMonth()+1)).substr(-2) + '-'+ ('0'+ today.getDate()).substr(-2);
    $scope.StartDt = $scope.SearchYear + "0101";
    $scope.EndDt = $scope.SearchYear + "1231";
    
    var storeDay = ['일요일','월요일','화요일','수요일','목요일','금요일','토요일']
    $scope.calcYoil = function(cost){
    	cost.COST_DT = cost.COST_DT.replace(/-/gi,'');
    	console.log(cost);
    	var year = parseInt(cost.COST_DT.substring(0,4)); 
    	var month = parseInt(cost.COST_DT.substring(4,6));
    	var date = parseInt(cost.COST_DT.substring(6,8));
    	
    	console.log(year,month-1,date);
    	
    	cost.DAY = storeDay[(new Date(year,month-1,date)).getDay()];
    	return cost.DAY;
    }
    
    $scope.fileSelect = function($files, cmd, index){
   	var reader = new FileReader();
		reader.onload = function(e) {
			const workBook =XLSX.read(reader.result, {type :'binary'});
			workBook.SheetNames.forEach(sheetName => {
				const rows =XLSX.utils.sheet_to_json(workBook.Sheets[sheetName]);
				//rows.splice(0,1);
				//console.log(rows);
				console.log(rows);
				if(sheetName=='Sheet1'){
			    	var expense_item_array = {'초기투자비':'01','수리비':'02','유지비':'03','운영비':'04','개선비':'05','해체폐기비':'06'};
			    	var manange_item_array = {'운영비':'01','기술진단비':'02','안전진단비':'03','검교정비':'04','기타':'05'};					
					$scope.CostList = [];
					$.each(rows,function(idx,Item){
						$scope.CostList.push({
							COST_DT:Item['날짜']
							,DAY:''
							,ASSET_NM : Item['자산명']
							,ASSET_CD : Item['자산코드']
							,EXPENSE_ITEM : expense_item_array[Item['비용구분']]
							,MANAGE_ITEM : manange_item_array[Item['운영비항목']]
							,REPEAT_YN : ("" + Item['반복여부']).replace(/'/gi,'')
							,PERIOD_MONTH : Item['반복주기']
							,COST_AMT : Item['금액']
							,COST_DESC : Item['내용']
						});
					});
					$.each($scope.CostList,function(idx,Item){
						Item.DAY = $scope.calcYoil(Item);
						Item.IsRepeat = (Item.REPEAT_YN=='Y')?(true):(false);
						//Item.EXPENSE_ITEM  
						//Item.MANANGE_ITEM  
					});
		        	$timeout(function(){
		        		setDatePicker();
		        	},500);
					$scope.$apply();					
				}
				//mainDataService.CommunicationRelay(PopupView.URLS.InsertAsset, {List : rows, SID : PopupView.Info.Popup_SID}).success(function(sInfo) {
					//Common_Swap.B_A(sInfo, Model.condition);
					//$('#'+ Model.grid_id).jqGrid('setGridParam', {postData : Model.condition, page : 1}).trigger('reloadGrid');	
				//});
			});
		};
		reader.readAsBinaryString($files[0]);
	}
}

//angular.module('app.operation').controller('investigationController',investigationController);
angular.module('app.operation').controller('nfcController',nfcController);
// angular.module('app.operation').controller('minwonController',minwonController);

function nfcController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	
	$scope.nfc_app_url = "http://buansystem.weven.net/webadm/login";
	const hostname = location.hostname;
	
	if (hostname === 'localhost' || hostname === '127.0.0.1' || hostname === 'ams.weven.net') {  // 개발 PC
		$scope.nfc_app_url = "http://buansystem.weven.net/webadm/login";
	} else if (hostname === '14.35.198.58') { // 평촌 개발 서버 (외부)		
		$scope.nfc_app_url = "http://buansystem.weven.net/webadm/login";
	} else if (hostname === '192.168.1.80') {  // 평촌 개발 PC (내부)
		$scope.nfc_app_url = "http://buansystem.weven.net/webadm/login";
	} else if (hostname === '109.14.96.230') { // 부안군 행망에서 망연계
		$scope.nfc_app_url = "http://109.14.96.230:18080/webadm/login";
	} else { // 부안군 운영서버 폐쇄망
		$scope.nfc_app_url = "http://10.0.28.100:18080/webadm/login";
	}
			//http://buansystem.weven.net/webadm/login 
			//http://59.2.173.225:18080/webadm/login
			//http://10.0.28.100:18080/webadm/login
	
	$scope.$on('$viewContentLoaded', function () {
		console.log($scope.nfc_app_url);
		$("input[name=id]").val("admin");
		$("input[name=input_pass]").val("1234");
		$("#nfc_login").prop("action",$scope.nfc_app_url);
		$("#nfc_login").submit();
		$("input[name=id]").val("");
		$("input[name=input_pass]").val("");
	});
	
}

// function minwonController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
// 	 $scope.$on('$viewContentLoaded', function () {
// 	    	$("#tab1_btn").on("click", function() {
// 	    		$(this).addClass("active");
// 	    		$("#tab2_btn, #tab3_btn").removeClass("active");
// 	    		$("#tab1").show();
// 	    		$("#tab2, #tab3").hide();
// 	    	});
// 	    	$("#tab2_btn").on("click", function() {
// 	    		$(this).addClass("active");
// 	    		$("#tab1_btn, #tab3_btn").removeClass("active");
// 	    		$("#tab2").show();
// 	    		$("#tab1, #tab3").hide();
// 	    	});
// 	    	$("#tab3_btn").on("click", function() {
// 	    		$(this).addClass("active");
// 	    		$("#tab2_btn, #tab1_btn").removeClass("active");
// 	    		$("#tab3").show();
// 	    		$("#tab1, #tab2").hide();
// 	    	});
// 	    });
// }

