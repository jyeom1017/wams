angular.module('app.operation').controller('operationController',operationController);

// 상수운영관리
function operationController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {

	$scope.tabType = 'tab1';
	$scope.excel_date = [];

	$scope.$on('$viewContentLoaded', function () {
    	$("#tab1_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab2_btn, #tab3_btn, #tab4_btn").removeClass("active");
    		$("#tab1").show();
    		$("#tab2, #tab3, #tab4").hide();
    		$("#left_conts").show();
			// $scope.searchDateType = 'year';
			$scope.tabType = 'tab1';
			$("label[for='tab1_DATE_radio0']").text('최근 일년');
			$scope.setLastYear();
			$scope.changeRadio();
		});
    	$("#tab2_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab1_btn, #tab3_btn, #tab4_btn").removeClass("active");
    		$("#tab2").show();
    		$("#tab1, #tab3, #tab4").hide();
    		$("#left_conts").show();
			$scope.tabType = 'tab2';
			$("label[for='tab1_DATE_radio0']").text('최근 일주일');
			// $scope.searchDateType = 'week';
			$scope.setLastYear();
			$scope.changeRadio();
    	});
    	$("#tab3_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab2_btn, #tab1_btn, #tab4_btn").removeClass("active");
    		$("#tab3").show();
    		$("#tab1, #tab2, #tab4").hide();
    		$("#left_conts").show();
			$("label[for='tab1_DATE_radio0']").text('최근 일주일');
			$scope.tabType = 'tab3';
			// $scope.searchDateType = 'week';
			$scope.setLastYear();
			$scope.changeRadio();
    	});
    	$("#tab4_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab2_btn, #tab1_btn, #tab3_btn").removeClass("active");
    		$("#tab4").show();
    		$("#tab1, #tab2, #tab3").hide();
    		$("#left_conts").hide();
			$scope.tabType = 'tab4';
			// $scope.searchDateType = 'week';
			$scope.setLastYear();
			$scope.changeRadio();
			setDatePicker();
    	});

		setDatePickerToday();
		$scope.setLastYear();
		$scope.changeRadio();

		setGrid1();
		setGrid2();
		setGrid3();
		setGrid4();
		setGrid5();
		setGrid6();

	});

	$scope.Block_PO_PCODE = 'block1';
	$scope.blockList = [];
	$scope.tab1 = {};
	$scope.monthList = {}; // tab1 유량 및 수압 데이터 (월 단위 그래프 그리기 위함)
	$scope.hourList = {}; // tab2 유량 및 수압 데이터 (시간 단위 그래프 그리기 위함)
	$scope.waterhourList = {};

	// 차트 그래프 툴팁 함수
	function dataTipJsFunc (seriesId, seriesName, index, xName, yName, data, values) {
		console.log('data tip');

		if (seriesName == "ROI") {
			return data['Month'] + "ROI : " + "<b>" + data['roi'] + "%</b>";
		} else {
			return data['Month'] + "Sales : " + "<b>$" + data['sales'] + "M</b>";
		}
	}
	
	
	$scope.DownLoadExcel = function(){
		var colIndex = [];
		var rows = [];		
			colIndex.push({name : '년월',id:'C_YYYYMM'});
			$.each($scope.blockList,function(idx,Item){
				if(Item.IsValid){
				colIndex.push({name : Item.BL_NAME +'수용가수',id: '\'' +Item.BL_BCODE + '\'' +'_CNT'});
				colIndex.push({name : Item.BL_NAME +'사용량',id: '\'' +Item.BL_BCODE + '\'' +'_TOT'});
				}
			});

    		$.each($scope.ConsumerGroupList,function(idx,Item){
    			var collumns = [];
    			if(idx==0 ){
    				var headers = [];				
    				$.each(colIndex,function(idx1,item){
    						headers.push(item.name);
    				});
    				rows.push(headers);
    			}
    			$.each(colIndex,function(idx1,item){
    				collumns.push(Item[item.id]);
    			});
    			rows.push(collumns);
    		});    		
			
			
		
    		const workSheetData = rows;
    		const workSheet = XLSX.utils.aoa_to_sheet(workSheetData);
    		//workSheet['!autofilter'] = {ref : "A1:R11"};
    		const workBook = XLSX.utils.book_new();
    		XLSX.utils.book_append_sheet(workBook, workSheet, '요금정보');
    		XLSX.writeFile(workBook, "요금정보_"+ $scope.getToday()+".xlsx");  
	}
	
	$timeout(function(){
		$scope.searchConsumerGroup();
	},1000);
	
	var IsValidBlock = function(bl_bcode,valid_block){
		//console.log(valid_block);
		if($.inArray(bl_bcode,valid_block)<0) return false;
		return true;
	}
	
	$scope.searchDateType = "1";
	$scope.searchDate = {S_DATE : '20220101',E_DATE : '20221231'};
	
	$scope.searchConsumerGroup = function(){
		
		$scope.DaeBlockCnt = 0;
		$scope.JungBlockCnt = 0;
		$scope.SoBlockCnt = 0;
		
		$.each($scope.blockList,function(idx,Item){
			if(Item.BL_BCODE.substr(0,1)=='1') $scope.DaeBlockCnt++;
			if(Item.BL_BCODE.substr(0,1)=='2') $scope.JungBlockCnt++;
			if(Item.BL_BCODE.substr(0,1)=='3') $scope.SoBlockCnt++;
		});
		
		var param  = {
				searchDateType : $scope.searchDateType,
				start_dt : $scope.searchDate.S_DATE.replace(/-/gi,'').substr(0,6),
				end_dt : $scope.searchDate.E_DATE.replace(/-/gi,'').substr(0,6)
				};
		mainDataService.getConsumerGroup(param)
		.success(function(data){
			var valid_block = [];
			$scope.ConsumerGroupList = data;
			$.each(data,function(idx,Item){
				for(key in Item)
				{
					//console.log(Item[key]);
					//debugger;
					if(key.indexOf("_CNT")>0 && parseInt(Item[key]) >= 0){
						if($.inArray(key.substring(1,5),valid_block)<0)
						valid_block.push(key.substring(1,5));
					} 
						
				}
			});
			//$scope.valid_block = valid_block;
			$.each($scope.blockList,function(idx,Item){
				Item.IsValid = IsValidBlock(Item.BL_BCODE,valid_block);
			});
		});
	}

	$scope.getBlockList = function (block) {
		// console.log(block);
		// 아래 부분은 지워야 함.
		var PO_PCODE = '';

		switch (block) {
			case 'block1':
				PO_PCODE = '01';
				break;
			case 'block2':
				PO_PCODE = '31';
				break;
			case 'block3':
				PO_PCODE = '91'; //9x
				break;
			default :
				PO_PCODE = '01';
		}

		mainDataService.getBlockCodeList({
			// PO_PCODE: PO_PCODE // 블록, 배수지, 관리블록 라디오 버튼 없앰. 전체 리스트 불러오는 것으로 수정함.
		}).success(function (obj) {
			if (!common.isEmpty(obj.errorMessage)) {
				alertify.error('errMessage : ' + obj.errorMessage);
			} else {
				$scope.blockList = obj;
				mainDataService.getBlockList({})
				.success(function(data){
					$scope.subBlockList = data;
				});
				/*
				for (var i = 0; i < obj.length; i++){
					obj[i].subBlockList = [];
				}

				for (var i = 0; i < obj.length; i++) {
					obj[i].PO_PCODE_SUBSTR = obj[i].PO_PCODE.substr(4, 2);
					//obj[i].subBlockList = [];
					if (obj[i].PO_PCODE_SUBSTR === '01') {
						for (var j = 0; j < obj.length; j++) {
							obj[j].PO_PCODE_SUBSTR = obj[j].PO_PCODE.substr(4, 2);

							if ((obj[j].PO_PCODE_SUBSTR === '01') && (obj[i].BL_BCODE === obj[j].BL_PBCODE)) {
								obj[i].subBlockList.push(obj[j]);
							}
						}
					}

					for (var k = 0; k < obj.length; k++) {
						obj[k].PO_PCODE_SUBSTR = obj[k].PO_PCODE.substr(4, 2);

						if (obj[i].PO_PCODE_SUBSTR != '01') { // 관리블록이면 반복문 처음으로
							continue;
						}

						if ((obj[k].PO_PCODE_SUBSTR != '01') && (obj[i].PO_PCODE.substr(0, 4) === obj[k].PO_PCODE.substr(0, 4))
								// && (obj[i].PO_PCODE != obj[k].PO_PCODE)
								) {
							obj[i].subBlockList.push(obj[k]);
						}
					}
				}
				*/
				
			}
		});
	};

	// '최근 일년' 라디오 선택
	$scope.setLastYear = function () {
		var searchDateType = ($("label[for='tab1_DATE_radio0']").text() === '최근 일년')? 'year' : 'week';

		var sday = new Date(); // 일년전
		var today = new Date(); // 오늘

		if (searchDateType === 'year') {
			sday.setFullYear(sday.getFullYear() - 1); // 일년전으로 set

		} else if (searchDateType === 'week') {
			sday.setDate(sday.getDate() - 7); // 일주일전으로 set
		}

		var sYear = sday.getFullYear();
		var eYear = today.getFullYear();
		var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
		var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

		$scope.tab1.S_DATE = s_yyyymmdd;
		$('#tab1_S_DATE').val(s_yyyymmdd);
		$scope.tab1.E_DATE = e_yyyymmdd;
		$('#tab1_E_DATE').val(e_yyyymmdd);
	}

	// 조회 날짜 (S_DATE 또는 E_DATE) 변경 시 실행. 최근 일년이 아닌 날짜 선택 시 '사용자 선택'으로 변경됨.
	$scope.changeRadio = function (/*periodType*/) {
		var searchDateType = ($("label[for='tab1_DATE_radio0']").text() === '최근 일년')? 'year' : 'week';

		var sday = new Date(); // 일년전
		var today = new Date(); // 오늘

		if (searchDateType === 'year') {
			sday.setFullYear(sday.getFullYear() - 1); // 일년전으로 set

		} else if (searchDateType === 'week') {
			sday.setDate(sday.getDate() - 7); // 일주일전으로 set
		}

		var sYear = sday.getFullYear();
		var eYear = today.getFullYear();
		var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
		var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

		if ($('#tab1_S_DATE').val() === s_yyyymmdd && $('#tab1_E_DATE').val() === e_yyyymmdd) {
			$scope.tab1.DATE_radio = '0';
		} else {
			$scope.tab1.S_DATE = $('#tab1_S_DATE').val();
			$scope.tab1.E_DATE = $('#tab1_E_DATE').val();
			$scope.tab1.DATE_radio = '1';
		}
	};

	// 조회 버튼 클릭 시 tab에 따라 search 함수 연결
	$scope.search = function () {
		console.log($scope.tabType);

		if ($scope.tabType === 'tab1') { // 유량/수압 조회
			$scope.month_search();
		} else if ($scope.tabType === 'tab2') { // 유량/수압 상세현황
			$scope.hour_search();
		} else if ($scope.tabType === 'tab3') { // 수질 조회
			$scope.waterQuality_search();
		}

	};

	// tab1 조회
	$scope.month_search = function () {
		console.log($scope.tab1);
		$scope.tab1.ccode = '479';
		$scope.tab1.po_pcode = $("input:checkbox[name='tab1_blockChk']:checked").val();
		$scope.tab1.pname = $("input:checkbox[name='tab1_blockChk']:checked").next().find('span:eq(1)').text();
		console.log($scope.tab1.pname);
		// var po_pcode = $("input:checkbox[name='tab1_blockChk']:checked").val();
		// console.log(po_pcode);

		if ($scope.tab1.po_pcode === undefined) {
			alertify.alert('','블록명은 필수항목입니다.');
			return;
		}

		// 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
		if (!checkDate($scope.tab1.S_DATE, $scope.tab1.E_DATE, 365)) {
			return;
		}

		// flux : 유량
		// wp (water pressure) : 수압
		var param = {};
		param.crudURL = '/operation/getData_flux_wp_month.json';
		param.ccode = $scope.tab1.ccode;
		param.po_pcode = $scope.tab1.po_pcode;
		param.s_date = $scope.tab1.S_DATE;
		param.e_date = $scope.tab1.E_DATE;

		// 로딩바
		$('#isLoading').css('display', '');
		var target = document.getElementById('spinnerContainer');
		var spinner = new Spinner().spin(target);

		mainDataService.cmCRUD(param).success(function (obj) {
			$('#isLoading').css('display', 'none');
			spinner.stop();
			if (!common.isEmpty(obj.errMessage)) {
				alertify.alert('errMessage : ' + obj.errMessage);
			} else {
				$scope.monthList = obj; // 그래프에 사용됨
				$scope.month.clearGridData();
				$scope.month.setGridParam({
					datatype: 'local',
					data    : obj,
				}).trigger('reloadGrid');

				monthGraph(); // tab1 유량 및 수압 그래프 생성 함수

				$scope.day.clearGridData();
				$('#day>tbody').append("<tr><td align='center' colspan='3'>데이터가 없습니다.</td></tr>");
				// $scope.tenMinList = [];
			}
		});
		gridResize();
	};

	var monthGraph = function () {
		console.log($scope.monthList);

		// var dateData = []; // 측정일시 데이터
		// var fluxData = []; // 유량 데이터
		// var waterPressureData = []; // 수압 데이터

		var dataArray = [];

		if ($scope.monthList != undefined && $scope.monthList.length > 0) {

			$.each($scope.monthList, function(idx, Item) { // idx : 인덱스 숫자 / Item : 데이터
				// dateData.push(Item.TIME);
				// fluxData.push(Item.AVE_Q);
				// waterPressureData.push(Item.AVE_P);

				var dataObj = {};
				dataObj.TIME = Item.TIME;
				dataObj.AVE_Q = Item.AVE_Q;
				dataObj.AVE_P = Item.AVE_P;

				dataArray.push(dataObj);
			});
		}

		console.log(dataArray);




			// 차트 이름, 차트 영역 id
		rMateChartH5.create("chart1", "monthGraph", "", "100%", "100%");

// 스트링 형식으로 레이아웃 정의.
		var layoutStr =

		'<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
			+'<Options>'
			+'<Caption text="'+$scope.tab1.pname+'"/>' // 차트 제목 캡션
			+'<Legend useVisibleCheck="true"/>' // Legend : 범례 / 체크박스 선택 true
			+'</Options>'
			+'<NumberFormatter id="numFmt1"/>'
			+'<NumberFormatter id="numFmt2"/>'
			+'<Combination2DChart showDataTips="false" dataTipJsFunction="dataTipJsFunc">'
			/*
                 showDataTips : 데이터에 마우스를 가져갔을 때 나오는 Tip을 보이기/안보이기 속성입니다
            	true 설정했을 때 에러 발생해서 false 해둠.
            */
				+'<horizontalAxis>' // 가로축 설정
					+'<CategoryAxis categoryField="TIME" padding="0.5"/>' // 숫자가 아닌 '문자'가 축의 라벨에 표시
				+'</horizontalAxis>'
				+'<verticalAxis>' // 세로축 설정
			// 막대 (수압)
			// interval : 축 간격 설정. default는 NaN. (데이터에 따라 자동설정)
					+'<LinearAxis id="vAxis1" minimum="0" maximum="10" interval="NaN" title="수압(kgf/cm³)"/>'
				+'</verticalAxis>'

				+'<series>' // series => Column2DSeries(막대), Line2DSeries(라인)
			// 1. Column2DSeries : 차트에 설정된 데이터 처리, 세로 막대 표현
					+'<Column2DSeries labelPosition="outside" yField="AVE_P" displayName="수압">' // displayName : 시리즈 이름 설정, 툴팁에 보임
						+'<fill>'
							+'<SolidColor color="#41b2e6"/>'
						+'</fill>'
						+'<showDataEffect>' // 애니메이션 효과
							+'<SeriesInterpolate/>'
						+'</showDataEffect>'
					+'</Column2DSeries>'
			// 2. Line2DSeries : 차트에 설정된 데이터 처리, 라인 차트 표현
					+'<Line2DSeries form="curve" labelPosition="up" radius="5" yField="AVE_Q" displayName="유량" itemRenderer="CircleItemRenderer">'
						+'<verticalAxis>'
							+'<LinearAxis id="vAxis2" title="유량(m³)"/>'
						+'</verticalAxis>'
						+'<showDataEffect>'
							+'<SeriesInterpolate/>'
						+'</showDataEffect>'
						+'<lineStroke>'
							+'<Stroke color="#f9bd03" weight="4"/>'
						+'</lineStroke>'
						+'<stroke>'
							+'<Stroke color="#f9bd03" weight="3"/>'
						+'</stroke>'
					+'</Line2DSeries>'
				+'</series>'
			+'<verticalAxisRenderers>'
			+'<Axis2DRenderer axis="{vAxis1}" showLine="false"/>'
			+'<Axis2DRenderer axis="{vAxis2}" showLine="false"/>'
			+'</verticalAxisRenderers>'
			+'</Combination2DChart>'
			+'</rMateChart>';

// 차트 데이터
		var chartData = dataArray;

		rMateChartH5.calls("chart1", {
			"setLayout" : layoutStr,
			"setData" : chartData
		});
	};

	var hourGraph = function () {

		console.log($scope.hourList);

		var dataArray = [];

		if ($scope.hourList != undefined && $scope.hourList.length > 0) {

			$.each($scope.hourList, function(idx, Item) { // idx : 인덱스 숫자 / Item : 데이터
				// dateData.push(Item.TIME);
				// fluxData.push(Item.AVE_Q);
				// waterPressureData.push(Item.AVE_P);

				var dataObj = {};
				dataObj.DATETIME = Item.DATETIME; // 측정일시
				dataObj.H_AVEP = Item.H_AVEP; // 수압 (평균수압)
				dataObj.H_Q = Item.H_Q; // 유량 (시간 적산차 유량)

				dataArray.push(dataObj);
			});
		}
		console.log(dataArray);

		rMateChartH5.create("chart2", "hourGraph", "", "100%", "100%");

		var layoutStr =
			'<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
			+'<Options>'
			+'<Caption text="'+$scope.tab1.pname+'"/>' // 차트 제목 캡션
			+'<Legend useVisibleCheck="true"/>' // Legend : 범례 / 체크박스 선택 true
			+'</Options>'

			+'<Combination2DChart >'
			+'<horizontalAxis>'
			+'<CategoryAxis categoryField="DATETIME" padding="0.5"/>'
			+'</horizontalAxis>'
			+'<verticalAxis>'
			+'<LinearAxis id="vAxis1" title="수압(kgf/cm³)" interval="0.05" minimum="0" />'
			+'</verticalAxis>'

			+'<series>'
				+'<Area2DSeries yField="H_AVEP" form="curve" displayName="수압">'
					+'<areaStroke>'
						+'<Stroke color="#41b2e6" weight="1"/>' // Stroke : 선 표현. weight : 두께
					+'</areaStroke>'
					+'<areaFill>'
						+'<SolidColor color="#41b2e6" alpha="0.15"/>' // 영역 색칠하기. alpha : 투명도
					+'</areaFill>'
					// +'<showDataEffect>'
					// 	+'<SeriesInterpolate minimumElementDuration="0"/>'
					// +'</showDataEffect>'
				+'</Area2DSeries>'
				+'<Area2DSeries yField="H_Q" form="curve" displayName="유량">'
					+'<verticalAxis>'
						+'<LinearAxis id="vAxis2" title="유량(m³)" interval="5" minimum="0" />'
					+'</verticalAxis>'
					+'<areaStroke>'
						+'<Stroke color="#f9bd03" weight="1"/>'
					+'</areaStroke>'
					+'<areaFill>'
						+'<SolidColor color="#f9bd03" alpha="0.15"/>'
					+'</areaFill>'
					// +'<showDataEffect>'
					// 	+'<SeriesInterpolate minimumElementDuration="0"/>'
					// +'</showDataEffect>'
				+'</Area2DSeries>'
			+'</series>'
			+'<verticalAxisRenderers>'
				+'<Axis2DRenderer axis="{vAxis1}" showLine="false"/>'
				+'<Axis2DRenderer axis="{vAxis2}" showLine="false"/>'
			+'</verticalAxisRenderers>'
			+'</Combination2DChart>'
			+'</rMateChart>';



// 차트 데이터
		var chartData = dataArray;

		rMateChartH5.calls("chart2", {
			"setLayout" : layoutStr,
			"setData" : chartData
		});
	}


		// tab2 조회
	$scope.hour_search = function() {
		$scope.tab1.ccode = '479';
		$scope.tab1.po_pcode = $("input:checkbox[name='tab1_blockChk']:checked").val();
		$scope.tab1.pname = $("input:checkbox[name='tab1_blockChk']:checked").next().find('span:eq(1)').text();


		if ($scope.tab1.po_pcode === undefined) {
			alertify.alert('','블록명은 필수항목입니다.');
			return;
		}

		// 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
		if (!checkDate($scope.tab1.S_DATE, $scope.tab1.E_DATE, 365)) {
			return;
		}

		var param = {};
		param.crudURL = '/operation/getData_flux_wp_hour.json';
		param.ccode = $scope.tab1.ccode;
		param.po_pcode = $scope.tab1.po_pcode;
		param.s_date = $scope.tab1.S_DATE;
		param.e_date = $scope.tab1.E_DATE;

		// 로딩바
		$('#isLoading').css('display', '');
		var target = document.getElementById('spinnerContainer');
		var spinner = new Spinner().spin(target);

		console.log(param);
		mainDataService.cmCRUD(param).success(function(obj) {
			$('#isLoading').css('display', 'none');
			spinner.stop();
			if (!common.isEmpty(obj.errMessage)) {
				alertify.alert('errMessage : ' + obj.errMessage);
			} else {
				$scope.hourList = obj;
				$scope.hour.clearGridData();
				$scope.hour.setGridParam({
					datatype: 'local',
					data: obj,
				}).trigger('reloadGrid');

				hourGraph();
				$scope.min.clearGridData();
				$('#min>tbody').append("<tr><td align='center' colspan='7'>데이터가 없습니다.</td></tr>");
			}
		});
		gridResize();
	};

	
	var waterQualityGraph = function () {

		console.log($scope.waterhourList);

		var dataArray = [];

		if ($scope.waterhourList != undefined && $scope.waterhourList.length > 0) {
			//debugger;
			$.each($scope.waterhourList, function(idx, Item) { // idx : 인덱스 숫자 / Item : 데이터
				// dateData.push(Item.TIME);
				// fluxData.push(Item.AVE_Q);
				// waterPressureData.push(Item.AVE_P);

				var dataObj = {};
				dataObj.DATETIME = Item.DATETIME; // 측정일시
				dataObj.NTU = Item.WQ_NTU; // 탁도
				dataObj.PH = Item.WQ_PH; // ph
				dataObj.CL = Item.WQ_CL; // 잔류염소농도
				dataObj.EC = Item.WQ_EC; // 전기전도도
				dataObj.WC = Item.WQ_WC; // 수온

				dataArray.push(dataObj);
			});
		}
		console.log(dataArray);

		rMateChartH5.create("chart2", "waterQuality_hourGraph", "", "100%", "100%");

		var layoutStr =
			'<rMateChart backgroundColor="#FFFFFF" borderStyle="none">'
			+'<Options>'
			+'<Caption text="'+$scope.tab1.pname+'"/>' // 차트 제목 캡션
			+'<Legend useVisibleCheck="true"/>' // Legend : 범례 / 체크박스 선택 true
			+'</Options>'

			+'<Combination2DChart >'
			+'<horizontalAxis>'
			+'<CategoryAxis categoryField="DATETIME" padding="0.5"/>'
			+'</horizontalAxis>'
			+'<verticalAxis>'
				+'<LinearAxis id="vAxis1" title="탁도" />'
				//+'<LinearAxis id="vAxis2" title="PH"  />'
				//+'<LinearAxis id="vAxis3" title="잔류염소" />'
				//+'<LinearAxis id="vAxis4" title="전기전도도" />'
				//+'<LinearAxis id="vAxis5" title="수온" />'
			+'</verticalAxis>'
			+'<series>'
				+'<Area2DSeries yField="NTU" form="curve" displayName="탁도">'
					+'<areaStroke>'
						+'<Stroke color="#41b2e6" weight="1"/>' // Stroke : 선 표현. weight : 두께
					+'</areaStroke>'
					+'<areaFill>'
						+'<SolidColor color="#41b2e6" alpha="0.15"/>' // 영역 색칠하기. alpha : 투명도
					+'</areaFill>'
				+'</Area2DSeries>'
				+'<Area2DSeries yField="PH" form="curve" displayName="PH">'
					+'<verticalAxis>'
						+'<LinearAxis id="vAxis2" title="PH"  />'
					+'</verticalAxis>'
					+'<areaStroke>'
						+'<Stroke color="#f9bd03" weight="1"/>'
					+'</areaStroke>'
					+'<areaFill>'
						+'<SolidColor color="#f9bd03" alpha="0.15"/>'
					+'</areaFill>'
				+'</Area2DSeries>'
				+'<Area2DSeries yField="CL" form="curve" displayName="잔류염소">'
					+'<verticalAxis>'
						+'<LinearAxis id="vAxis3" title="잔류염소" />'
					+'</verticalAxis>'
					+'<areaStroke>'
						+'<Stroke color="#bdf903" weight="1"/>'
					+'</areaStroke>'
					+'<areaFill>'
						+'<SolidColor color="#bdf903" alpha="0.15"/>'
					+'</areaFill>'
				+'</Area2DSeries>'
				+'<Area2DSeries yField="EC" form="curve" displayName="전기전도도">'
					+'<verticalAxis>'
						+'<LinearAxis id="vAxis4" title="전기전도도" />'
					+'</verticalAxis>'
					+'<areaStroke>'
						+'<Stroke color="#bd03f9" weight="1"/>'
					+'</areaStroke>'
					+'<areaFill>'
						+'<SolidColor color="#bd03f9" alpha="0.15"/>'
					+'</areaFill>'
				+'</Area2DSeries>'
				+'<Area2DSeries yField="WC" form="curve" displayName="수온">'
					+'<verticalAxis>'
						+'<LinearAxis id="vAxis5" title="수온" />'
					+'</verticalAxis>'
					+'<areaStroke>'
						+'<Stroke color="#03f9bd" weight="1"/>'
					+'</areaStroke>'
					+'<areaFill>'
						+'<SolidColor color="#03f9bd" alpha="0.15"/>'
					+'</areaFill>'
				+'</Area2DSeries>'		
			+'</series>'
			+'<verticalAxisRenderers>'
				+'<Axis2DRenderer axis="{vAxis1}" showLine="false"/>'
				+'<Axis2DRenderer axis="{vAxis2}" showLine="false"/>'
				+'<Axis2DRenderer axis="{vAxis3}" showLine="false"/>'
				+'<Axis2DRenderer axis="{vAxis4}" showLine="false"/>'
				+'<Axis2DRenderer axis="{vAxis5}" showLine="false"/>'
			+'</verticalAxisRenderers>'
			+'</Combination2DChart>'
			+'</rMateChart>';



// 차트 데이터
		var chartData = dataArray;

		rMateChartH5.calls("chart2", {
			"setLayout" : layoutStr,
			"setData" : chartData
		});
	}	
	// tab3 조회
	$scope.waterQuality_search = function () {
		$scope.tab1.ccode = '479';
		$scope.tab1.pname = $("input:checkbox[name='tab1_blockChk']:checked").next().find('span:eq(1)').text();
		$scope.tab1.po_pcode = $("input:checkbox[name='tab1_blockChk']:checked").val();

		if ($scope.tab1.po_pcode === undefined) {
			alertify.alert('','블록명은 필수항목입니다.');
			return;
		}

		// 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
		if (!checkDate($scope.tab1.S_DATE, $scope.tab1.E_DATE, 365)) {
			return;
		}

		var param = {};
		param.crudURL = '/operation/getData_waterQuality_hour.json';
		param.ccode = $scope.tab1.ccode;
		param.po_pcode = $scope.tab1.po_pcode;
		param.s_date = $scope.tab1.S_DATE;
		param.e_date = $scope.tab1.E_DATE;

		// 로딩바
		$('#isLoading').css('display', '');
		var target = document.getElementById('spinnerContainer');
		var spinner = new Spinner().spin(target);

		mainDataService.cmCRUD(param).success(function(obj) {
			$('#isLoading').css('display', 'none');
			spinner.stop();
			if (!common.isEmpty(obj.errMessage)) {
				alertify.alert('errMessage : ' + obj.errMessage);
			} else {
				console.log(obj);
				$scope.waterhourList = obj;
				$scope.waterQuality_hour.clearGridData();
				$scope.waterQuality_hour.setGridParam({
					datatype: 'local',
					data: obj,
				}).trigger('reloadGrid');
				
				waterQualityGraph();
			}
		});
		gridResize();
	}
	
	$scope.waterQuality_min_search = function (s_date,s_hh) {
		$scope.tab1.ccode = '479';
		$scope.tab1.po_pcode = $("input:checkbox[name='tab1_blockChk']:checked").val();

		if ($scope.tab1.po_pcode === undefined) {
			alertify.alert('','블록명은 필수항목입니다.');
			return;
		}

		// 날짜 체크 (최대 1년 까지 조회, '시작 날짜 < 종료 날짜' 이어야 함)
		if (!checkDate($scope.tab1.S_DATE, $scope.tab1.E_DATE, 365)) {
			return;
		}

		var param = {};
		param.crudURL = '/operation/getData_waterQuality_min.json';
		param.ccode = $scope.tab1.ccode;
		param.po_pcode = $scope.tab1.po_pcode;
		param.s_date = s_date;
		param.s_hh = s_hh;

		// 로딩바
		$('#isLoading').css('display', '');
		var target = document.getElementById('spinnerContainer');
		var spinner = new Spinner().spin(target);

		mainDataService.cmCRUD(param).success(function(obj) {
			$('#isLoading').css('display', 'none');
			spinner.stop();
			if (!common.isEmpty(obj.errMessage)) {
				alertify.alert('errMessage : ' + obj.errMessage);
			} else {
				console.log(obj);
				$scope.waterQuality_min.clearGridData();
				$scope.waterQuality_min.setGridParam({
					datatype: 'local',
					data: obj,
				}).trigger('reloadGrid');
			}
		});
		gridResize();
	}

	// 좌측 블록 트리 리스트에서 체크박스 하나만 선택되게 함
	$scope.selectOneCheckbox = function (checked) {
		var obj = document.getElementsByName("tab1_blockChk");
		for (var i = 0; i < obj.length; i++) {
			if (obj[i] != checked.currentTarget) {
				obj[i].checked = false;
			}
		}
	};

	/******************** setGrid ********************/
	// tab1 > 유량 및 수압 데이터 (월 단위)
	var setGrid1 = function() {
		var gridInfo = {
			grid_id: 'month',
			autowidth: true,
			colNames: ['측정일시', "월평균<br>적산차 유량 <b class='unit'>(m³)</b>", "월평균<br>최대유량 <b class='unit'>(m³/h)</b>", "월평균<br>최소유량 <b class='unit'>(m³/h)</b>",
					"평균수압<br><b class='unit'>(kgf/cm³)</b>", "월평균<br>최대수압 <b class='unit'>(kgf/cm³)</b>", "월평균<br>최소수압 <b class='unit'>(kgf/cm³)</b>"],
			colModel: [{
				name: 'TIME',
				index: 'TIME',
				sorttype: 'string'
			}, {
				name: 'AVE_Q',
				index: 'AVE_Q',
				sorttype: 'float'
			}, {
				name: 'MAX_Q',
				index: 'MAX_Q',
				sorttype: 'float'
			}, {
				name: 'MIN_Q',
				index: 'MIN_Q',
				sorttype: 'float'
			}, {
				name: 'AVE_P',
				index: 'AVE_P',
				sorttype: 'float'
			}, {
				name: 'MAX_P',
				index: 'MAX_P',
				sorttype: 'float'
			}, {
				name: 'MIN_P',
				index: 'MIN_P',
				sorttype: 'float'
			}],
			onSelectRow: function(rowid, status, e) {
				var param = $(this).jqGrid('getRowData', rowid);
				day_search(param); // 행 클릭 시 일 단위 데이터 보여주기
			},
			loadComplete: function(data) {
				if (data.rows.length == 0) {
					// 행 개수 계산
					var rowCount = $('#month').getGridParam('reccount');
					// td의 개수 계산
					var colspanCount = $('#month tbody tr').children().length;

					if (rowCount == 0) {
						$('#month>tbody').append("<tr><td align='center' colspan='7'>데이터가 없습니다.</td></tr>");
					}
				}
				// 첫번째 행 선택
				var firstid = $('#month').jqGrid('getDataIDs');
				$('#month').setSelection(firstid[0]);
			}
		};
		$scope.month = nonPagerGrid(gridInfo);
	}

	// tab1 > 유량 및 수압 데이터 (일 단위)
	var setGrid2 = function() {
		var gridInfo2 = {
			grid_id: 'day',
			autowidth: true,
			colNames: ['측정일시', "유량<br><b class='unit'>(m³/h)</b>", "수압<br><b class='unit'>(kgf/cm³)</b>"],
			colModel: [{
				name: 'TIME',
				index: 'TIME',
				sorttype: 'string'
			}, {
				name: 'AVE_Q',
				index: 'AVE_Q',
				sorttype: 'float'
			}, {
				name: 'AVE_P',
				index: 'AVE_P',
				sorttype: 'float'
			}],
			loadComplete: function(data) {
				if (data.rows.length == 0) {
					// 행 개수 계산
					var rowCount = $('#day').getGridParam('reccount');
					// td의 개수 계산
					var colspanCount = $('#day tbody tr').children().length;

					if (rowCount == 0) {
						$('#day>tbody').append("<tr><td align='center' colspan='3'>데이터가 없습니다.</td></tr>");
					}
				} else {
					var firstid = $('#day').jqGrid('getDataIDs');
					$('#day').setSelection(firstid[0]);
				}
			}
		};
		$scope.day = nonPagerGrid(gridInfo2);
	}

	// tab2 > 유량 및 수압 데이터 (시간 단위)
	var setGrid3 = function() {
		var gridInfo3 = {
			grid_id: 'hour',
			autowidth: true,
			colNames: ['측정일시', "시간 적산차<br>유량 <b class='unit'>(m³)</b>", "적산유량<br><b class='unit'>(m³)</b>", "시간 평균<br>유량 <b class='unit'>(m³/h)</b>",
				"시간 최대<br>유량 <b class='unit'>(m³/h)</b>", "시간 최소<br>유량 <b class='unit'>(m³/h)</b>", "평균수압<br><b class='unit'>(kgf/cm³)</b>"],
			colModel: [{
				name: 'DATETIME',
				index: 'DATETIME',
				// sorttype: 'string'
				sorttype: 'date'
			}, {
				name: 'H_Q',
				index: 'H_Q',
				sorttype: 'float'
			}, {
				name: 'H_LASTAQ',
				index: 'H_LASTAQ',
				sorttype: 'float'
			}, {
				name: 'H_AVEQ',
				index: 'H_AVEQ',
				sorttype: 'float'
			}, {
				name: 'H_MAXQ',
				index: 'H_MAXQ',
				sorttype: 'float'
			}, {
				name: 'H_MINQ',
				index: 'H_MINQ',
				sorttype: 'float'
			}, {
				name: 'H_AVEP',
				index: 'H_AVEP',
				sorttype: 'float'
			}],
			onSelectRow: function(rowid, status, e) {
				var param = $(this).jqGrid('getRowData', rowid);
				min_search(param); // 행 클릭 시 분 단위 데이터 보여주기
			},
			loadComplete: function(data) {
				if (data.rows.length == 0) {
					// 행 개수 계산
					var rowCount = $('#hour').getGridParam('reccount');
					// td의 개수 계산
					var colspanCount = $('#hour tbody tr').children().length;

					if (rowCount == 0) {
						$('#hour>tbody').append("<tr><td align='center' colspan='7'>데이터가 없습니다.</td></tr>");
					}
				} else {
					// 첫번째 행 선택
					var firstid = $('#hour').jqGrid('getDataIDs');
					$('#hour').setSelection(firstid[0]);
				}
			}
		};
		$scope.hour = nonPagerGrid(gridInfo3);
	}

	// tab2 > 유량 및 수압 데이터 (분 단위)
	var setGrid4 = function() {
		var gridInfo4 = {
			grid_id: 'min',
			autowidth: true,
			colNames: ['측정일시', "적산유량<br><b class='unit'>(m³)</b>", "평균수압<br><b class='unit'>(kgf/cm³)</b>"],
			colModel: [{
				name: 'DATETIME',
				index: 'DATETIME',
				// sorttype: 'string'
				sorttype: 'date'
			}, {
				name: 'MA_Q',
				index: 'MA_Q',
				sorttype: 'float'
			}, {
				name: 'MA_P',
				index: 'MA_P',
				sorttype: 'float'
			}],
			loadComplete: function(data) {
				if (data.rows.length == 0) {
					// 행 개수 계산
					var rowCount = $('#min').getGridParam('reccount');
					// td의 개수 계산
					var colspanCount = $('#min tbody tr').children().length;

					if (rowCount == 0) {
						$('#min>tbody').append("<tr><td align='center' colspan='10'>데이터가 없습니다.</td></tr>");
					}
				} else {
					var firstid = $('#min').jqGrid('getDataIDs');
					$('#min').setSelection(firstid[0]);
				}
			}
		};
		$scope.min = nonPagerGrid(gridInfo4);
	}

	// tab3 > 수질 데이터 (시간 단위)
	var setGrid5 = function() {
		var gridInfo5 = {
			grid_id: 'waterQuality_hour',
			autowidth: true,
			colNames: ['측정일시', "탁도 <b class='unit'>(NTU)</b>", "pH",
				"잔류염소 <b class='unit'>(mg/L)</b>", "전기전도도 <b class='unit'>(µS/cm)</b>", "수온 <b class='unit'>(℃)</b>"],
			colModel: [{
				name: 'DATETIME',
				index: 'DATETIME',
				sorttype: 'string'
			}, {
				name: 'WQ_NTU',
				index: 'WQ_NTU',
				sorttype: 'float'
			}, {
				name: 'WQ_PH',
				index: 'WQ_PH',
				sorttype: 'float'
			}, {
				name: 'WQ_CL',
				index: 'WQ_CL',
				sorttype: 'float'
			}, {
				name: 'WQ_EC',
				index: 'WQ_EC',
				sorttype: 'float'
			}, {
				name: 'WQ_WC',
				index: 'WQ_WC',
				sorttype: 'float'
			}],
			onSelectRow: function(rowid, status, e) {
				var param = $(this).jqGrid('getRowData', rowid);
				//debugger;
				console.log(param);
				var date = param.DATETIME.substr(0,10).replace(/-/gi,'');
				var hh = param.DATETIME.substr(11,2);
				$scope.waterQuality_min_search(date,hh); // 행 클릭 시 분 단위 데이터 보여주기
			},			
			loadComplete: function(data) {
				// $(".ui-pg-selbox option[value='All']").val(1000000);
				console.log(data);
				if (data.rows.length == 0) {
					// 행 개수 계산
					var rowCount = $('#waterQuality').getGridParam('reccount');
					// td의 개수 계산
					var colspanCount = $('#waterQuality tbody tr').children().length;

					if (rowCount == 0) {
						$('#waterQuality>tbody').append("<tr><td align='center' colspan='6'>데이터가 없습니다.</td></tr>");
					}
				} else {
					// var firstid = $('#day').jqGrid('getDataIDs');
					// $('#day').setSelection(firstid[0]);
				}
			}
		};
		$scope.waterQuality_hour = nonPagerGrid(gridInfo5);
	}

	// tab3 > 수질 데이터 (분 단위)
	var setGrid6 = function() {
		var gridInfo6 = {
			grid_id: 'waterQuality_min',
			autowidth: true,
			colNames: ['측정일시', "탁도", "PH", "염소", "전기", "수온"],
			colModel: [{
				name: 'DATETIME',
				index: 'DATETIME',
				sorttype: 'string',
				formatter : function(cellvalue){
					if(cellvalue!=null) return cellvalue.substr(14,2);
					return '';
					}
			}, {
				name: 'WQ_NTU',
				index: 'WQ_NTU',
				sorttype: 'float'
			}, {
				name: 'WQ_PH',
				index: 'WQ_PH',
				sorttype: 'float'
			}, {
				name: 'WQ_CL',
				index: 'WQ_CL',
				sorttype: 'float'
			}, {
				name: 'WQ_EC',
				index: 'WQ_EC',
				sorttype: 'float'
			}, {
				name: 'WQ_WC',
				index: 'WQ_WC',
				sorttype: 'float'
			}],
			loadComplete: function(data) {
				if (data.rows.length == 0) {
					// 행 개수 계산
					var rowCount = $('#waterQuality_min').getGridParam('reccount');
					// td의 개수 계산
					var colspanCount = $('#waterQuality_min tbody tr').children().length;

					if (rowCount == 0) {
						$('#waterQuality_min>tbody').append("<tr><td align='center' colspan='10'>데이터가 없습니다.</td></tr>");
					}
				} else {
					var firstid = $('#waterQuality_min').jqGrid('getDataIDs');
					$('#waterQuality_min').setSelection(firstid[0]);
				}
			}
		};
		$scope.waterQuality_min = nonPagerGrid(gridInfo6);
	}
	/******************** setGrid 끝 ********************/

	// tab1의 grid1 셀 클릭 시 실행
	var day_search = function(parameter) {
		console.log(parameter);
		console.log($scope.tab1);
		var param = {};
		param.crudURL = '/operation/getData_flux_wp_day.json';
		param.ccode = $scope.tab1.ccode;
		param.po_pcode = $scope.tab1.po_pcode;

		var onlyNum = parameter.TIME.replace(/[^0-9]/g,'');
		var year = '20' + onlyNum.substr(0, 2);
		var month = onlyNum.substr(2, 2);

		var s_date_toDate = new Date(Number(year), Number(month) - 1, Number('01'));
		var e_date_toDate = new Date(s_date_toDate);
		e_date_toDate = new Date(e_date_toDate.setMonth(e_date_toDate.getMonth() + 1));

		var s_yyyymmdd = s_date_toDate.getFullYear() + '-' + ('0' + (s_date_toDate.getMonth() + 1)).substr(-2) + '-' + ('0' + s_date_toDate.getDate()).substr(-2);
		var e_yyyymmdd = e_date_toDate.getFullYear() + '-' + ('0' + (e_date_toDate.getMonth() + 1)).substr(-2) + '-' + ('0' + e_date_toDate.getDate()).substr(-2);

		param.s_date = s_yyyymmdd;
		param.e_date = e_yyyymmdd;

		$scope.excel_date.s_date = param.s_date;
		$scope.excel_date.e_date = param.e_date;

		console.log(param);
		mainDataService.cmCRUD(param).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.alert('errMessage : ' + obj.errMessage);
			} else {
				$scope.day.clearGridData();
				$scope.day.setGridParam({
					datatype: 'local',
					data: obj,
				}).trigger('reloadGrid');
				// $scope.tenMinList = obj; // 차트 그리기에 필요
				// initZoomChart();
			}
		});
		gridResize();
	};

	// tab2의 grid3 셀 클릭 시 실행
	var min_search = function(parameter) {
		console.log(parameter);
		console.log($scope.tab1);
		var param = {};
		param.crudURL = '/operation/getData_flux_wp_min.json';
		param.ccode = $scope.tab1.ccode;
		param.po_pcode = $scope.tab1.po_pcode;

		// var onlyNum = parameter.TIME.replace(/[^0-9]/g,'');
		// var year = '20' + onlyNum.substr(0, 2);
		// var month = onlyNum.substr(2, 2);

		var date_time = parameter.DATETIME.split(' ', 2);
		console.log(date_time);

		var s_yyyymmdd = date_time[0];
		var hour = date_time[1].substr(0, 2);

		// var s_date_toDate = new Date(Number(year), Number(month) - 1, Number('01'));
		// var e_date_toDate = new Date(s_date_toDate);
		// e_date_toDate = new Date(e_date_toDate.setMonth(e_date_toDate.getMonth() + 1));

		// var s_yyyymmdd = s_date_toDate.getFullYear() + '-' + ('0' + (s_date_toDate.getMonth() + 1)).substr(-2) + '-' + ('0' + s_date_toDate.getDate()).substr(-2);
		// var e_yyyymmdd = e_date_toDate.getFullYear() + '-' + ('0' + (e_date_toDate.getMonth() + 1)).substr(-2) + '-' + ('0' + e_date_toDate.getDate()).substr(-2);

		param.s_date = s_yyyymmdd;
		// param.e_date = e_yyyymmdd;
		param.hour = hour;

		$scope.excel_date.s_date = param.s_date;
		$scope.excel_date.hour = param.hour;

		console.log($scope.excel_date);

		console.log(param);
		// return;
		mainDataService.cmCRUD(param).success(function(obj) {
			if (!common.isEmpty(obj.errMessage)) {
				alertify.alert('errMessage : ' + obj.errMessage);
			} else {
				$scope.min.clearGridData();
				$scope.min.setGridParam({
					datatype: 'local',
					data: obj,
				}).trigger('reloadGrid');
				// $scope.tenMinList = obj; // 차트 그리기에 필요
				// initZoomChart();
			}
		});
		gridResize();
	};

	// 엑셀 양식 다운로드
	$scope.getExcelDownload = function(mode) {
		var jobType = null;
		var s_date = $scope.tab1.S_DATE;
		var e_date = $scope.tab1.E_DATE;
		var hour = '';
		var ccode = $scope.tab1.ccode;
		var po_pcode = $scope.tab1.po_pcode;
		var gridType = "";

		switch (mode) {
			case 1:
				gridType = "month";
				jobType = '010201_1';
				break;
			case 2:
				s_date = $scope.excel_date.s_date;
				e_date = $scope.excel_date.e_date;
				gridType = "day";
				jobType = '010201_2';
				break;
			case 3:
				gridType = "hour";
				jobType = '010201_3';
				break;
			case 4:
				s_date = $scope.excel_date.s_date;
				hour = $scope.excel_date.hour;
				gridType = "min";
				jobType = '010201_4';
				break;
			case 5:
				jobType = '010201_5';
				break;
			case 6:
				jobType = '010201_6';
				break;
			default:
				break;
		}

		var gridLength = $('#' + gridType).getGridParam("reccount");
		console.log(gridLength);
		console.log(po_pcode);

		if (gridLength === 0 || po_pcode === undefined) {
			alertify.alert('','데이터를 조회해주세요.');
			return;
		}

		$('#exportForm').find('[name=jobType]').val(jobType);
		$('#exportForm').find('#s_date').val(s_date);
		$('#exportForm').find('#e_date').val(e_date);
		$('#exportForm').find('#ccode').val(ccode);
		$('#exportForm').find('#po_pcode').val(po_pcode);
		$('#exportForm').find('#hour').val(hour);

		$('#exportForm').submit(); // index.html의 form에 보낸다.
	};

	$scope.getBlockList($scope.Block_PO_PCODE); // 처음 로딩 시 라디오버튼 '블록'으로 세팅
}