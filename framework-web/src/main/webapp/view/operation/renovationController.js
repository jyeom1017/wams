angular.module('app.operation').controller('renovationController',renovationController);
angular.module('app.operation').controller('renovationPopupController',renovationPopupController);
angular.module('app.operation').controller('image_PopupController',image_PopupController);
angular.module('app.operation').controller('New_And_ModifiedPopupController',New_And_ModifiedPopupController);
angular.module('app.operation').controller('renovation_PopupController',renovation_PopupController);
// 개보수
function renovationController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, $filter, MaxUploadSize) {	
	
	$scope.setWeek = function setLastWeek() {
        var sday = new Date(); // 6개월
        sday.setMonth(sday.getMonth() - 6); // 6개월전으로 set

        var today = new Date(); // 오늘

        var sYear = sday.getFullYear();
        var eYear = today.getFullYear();
        var s_yyyymmdd = sYear + '-' + ('0' + (sday.getMonth() + 1)).substr(-2) + '-' + ('0' + sday.getDate()).substr(-2);
        var e_yyyymmdd = eYear + '-' + ('0' + (today.getMonth() + 1)).substr(-2) + '-' + ('0' + today.getDate()).substr(-2);

        $scope.Serch.Info.START_DT = s_yyyymmdd;
        $('#start_dt').val(s_yyyymmdd);
        $scope.Serch.Info.END_DT = e_yyyymmdd;
        $('#end_dt').val(e_yyyymmdd);
    }
	
	
	$scope.MCODE = '0202';
	//온 로드
	$scope.$on('$viewContentLoaded', function() {
		$("#tab1_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab2_btn, #tab3_btn").removeClass("active");
    		$("#tab1").show();
    		$("#tab2, #tab3").hide();
    	});
    	$("#tab2_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab1_btn, #tab3_btn").removeClass("active");
    		$("#tab2").show();
    		$("#tab1, #tab3").hide();
    		
    	});
    	$("#tab3_btn").on("click", function() {
    		$(this).addClass("active");
    		$("#tab2_btn, #tab1_btn").removeClass("active");
    		$("#tab3").show();
    		$("#tab1, #tab2").hide();
    	});	
    	$rootScope.setBtnAuth($scope.MCODE);
    	$scope.setWeek();
    	View.Info = null;
    	setDatePicker();
    	Search.Init();
    	/*mainDataService
	    	.getCommonCodeList({gcode : 'YEAR', gname : ''})
	    	.success(function(data) {
	            if (!common.isEmpty(data.errMessage)) {
	                alertify.error('errMessage : ' + data.errMessage);
	            } else {
	            	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
	                $scope.ComCodeYearList = data;
	            }
	        });*/
    	
    	mainDataService
	    	.CommunicationRelay("/new_operation/getFormatTYPE.json")
			.success(function(array) {
				
	    		FormatTYPE.TYPE = {}
	    		for(i in array){
	    			if(array[i].C_SCODE!='01')
	    			FormatTYPE.TYPE[Number(i) + 1] = array[i].C_NAME;
	    		}
	    		
	    		FormatTYPE.PATH = 'COST';
	    		
	    		Grid = ModelInit.Grid();
	    		//수정
	    		let models = Grid.Models;
	    		pagerJsonGrid(models[0]);
	    		
	    		mainDataService
	    			.CommunicationRelay("/new_operation/getREPAIR.json", Grid.Models[0].condition)
		    		.success(function(obj) {
		        		let info = obj.rows[0];
		        		models[1].condition.REPAIR_SID = info.REPAIR_SID;
		        		models[2].condition.REPAIR_SID = info.REPAIR_SID;
		        		//Common_Picture.Init(pictureModel);
		        		View.Info = info;
		        		View.Info.BUSINESS_AMT =  $filter('number')(View.Info.BUSINESS_AMT,0);
		        		
		        		pagerJsonGrid(models[1]);
		        		pagerJsonGrid(models[2]);
		        		
		        		mainDataService
			        		.CommunicationRelay("/new_operation/getREPAIR_FILE.json", {REPAIR_SID : info.REPAIR_SID, PHOTO_YN : "N"})
			        		.success(function(array) {
			    				View.FileList = array;
			    			});
		    		});
	    	});
	});
	
	let Search = {
		Info : {
			OPTION : 1, 
			YEAR : "", 
			START_DT : "", 
			END_DT : "",
			REPAIR_NM : ''
		},
		Init : function() {
			this.Info.YEAR="";
			this.Info.OPTION = 1;
		},
		Select : function(option) {
			this.Info["OPTION"] = option;
		},
		Lookup : function() {
			if(this.Info.OPTION == 2 &&this.Info.START_DT > this.Info.END_DT){
				alertify.alert('Info','기간범위를  확인하세요.시작날짜가 종료날짜 보다 클수 없습니다.');
				return;
			}
			let model = Grid.Models[0];
			Common_Swap.A_B(model.condition, Search.Info);
			$('#'+ model.grid_id).jqGrid('setGridParam', {postData : model.condition, page : 1}).trigger('reloadGrid');	
		},
		Delete : function() {
			alertify.confirm('삭제 확인','작성하신 자료를 삭제 하시겠습니까?', function() {
				mainDataService
					.CommunicationRelay("/new_operation/updateREPAIR_DEL_YN.json",View.Info)
					.success(function(count) {
						if(Common_AlertStr(count)) return;
						
						let id = Grid.Models[0].grid_id;
						var ids = $('#'+ id).jqGrid('getDataIDs');
						$('#'+ id).jqGrid('setSelection', ids[0]);
						$('#'+ id).trigger('reloadGrid');	
					});
			}, function() {});
		},
		ExcelDown : function() {
			let jobType = $state.current.name+ "_" + 2
			Common_Excel.ExcelDown(jobType, Search.Info);
		}
	}
	
	let FormatTYPE = {};
	
	let format = {
			Number: function(cellvalue,options,rowdata,action) {
				if(!cellvalue) return ''
				
				cellvalue = cellvalue.toString();
				
				let temp = cellvalue.split(',');
				cellvalue = '';
				for(i in temp) {
					cellvalue += temp[i];
				}
				
				return $filter('number')(cellvalue, 0);
			},
			getLevelName:function(pathCd,level){
				var LEVEL_CD = pathCd.split('_')[level-1];
				if(LEVEL_CD!=''){
					var LevelName = $rootScope.LevelName[''+(level -1) +'_'+ LEVEL_CD];
					return LevelName;
				}else{
					return "";
				}
			}
	}

	let pictureModel = {
			getUrl: "/new_operation/getREPAIR_FILE.json",
			insertUrl: '/new_operation/insertREPAIR_FILE.json',
			updateUrl: '/new_operation/updateREPAIR_FILE.json',
			deleteUrl: '/new_operation/deleteREPAIR_FILE.json',
			size: 12,
			//인잇시 0번 인덱스에 기본이미지 자동으로 붙음 
			fileParam: ['F_PATH', 'F_DESC', 'F_NUM', 'F_TITLE', 'PHOTO_NUM'],
			valueation:[
				{name: 'REPAIR_SID', check: true, Default: ''},
				{name: 'ASSET_SID', check: true, Default: ''},
				{name: 'PHOTO_YN', check: false, Default: 'Y'},
			],
			Service: function(model){
				let list = model.List;
				//서버에서 이미지를 검색하여 가져온다
			  	mainDataService
			  		.CommunicationRelay(model.getUrl, model.param)
		    		//성공
		    		.success(function(array) {
						if(errMessageAlert(array)) return;
						//가져온 리스에서 파일 넘버에 맞는 곳에 정보를 입력한다.
						for (let i = 0; i < array.length; i++) {
							list[array[i].PHOTO_NUM] = array[i];
						}
					})
					//실패
					.error(function(error) {
						alertify.error('사진을 블러오기 중 문제가 생겼습니다.');
					});
			},
			Edit : function(index) {//수정화면을 호출한다
				alert('edit');
				//if(Common_Picture.Validation()) return;
				$rootScope.$broadcast("openImage_Popup", {Model : this, Info: this.List[index]});
			}
	}
	
	let Grid = null;
	//수정
	let ModelInit = {
			Grid : function() {
				return {
					Models : [
						{
							grid_id : 'list',
							pager_id : 'listPager',
							url : '/new_operation/getREPAIR.json',
							condition : {
								page : 1, 
								rows : GridConfig.sizeM,
								OPTION : $scope.Serch.Info.OPTION,
								YEAR : $scope.Serch.Info.YEAR								
							}, 
							rowNum : GridConfig.sizeM,
							colNames :
								[ "순번",'sid', '사업명', '사업 기간', '등록자', '등록일자', 
								//희든
								'조사탐사시작일', '조사탐사종료일', '계약일', '거래처명', '사업비용', '비고'],
							colModel : [
								{name : 'RNUM', width : "40px", index : 'RNUM', resizable: false}, 
								{name : 'REPAIR_SID', width : 0, index : 'REPAIR_SID', hidden : true}, 
								{name : 'REPAIR_NM', width : 150, index : 'REPAIR_NM', resizable: false}, 
								{name : 'PERIOD_DT', width : 100, index : 'PERIOD_DT', resizable: false}, 
								{name : 'INSERT_ID', width : 70, index : 'INSERT_ID', resizable: false}, 
								{name : 'INSERT_DT', width : 70, index : 'INSERT_DT', resizable: false}, 
								//히든
								{name : 'REPAIR_START_DT', width : 0, index : 'REPAIR_START_DT', hidden : true}, 
								{name : 'REPAIR_END_DT', width : 0, index : 'REPAIR_END_DT', hidden : true}, 
								{name : 'CONTRACT_DT', width : 0, index : 'CONTRACT_DT', hidden : true}, 
								{name : 'COMPANY_NM', width : 0, index : 'COMPANY_NM', hidden : true}, 
								{name : 'BUSINESS_AMT', width : 0, index : 'BUSINESS_AMT', hidden : true}, 
								{name : 'C_MEMO', width : 0, index : 'C_MEMO', hidden : true}
							],
							onSelectRow : function() {
								let id = $('#list').getGridParam( "selrow" );
								View.Info = jQuery("#list").jqGrid("getRowData",id);
								View.Info.BUSINESS_AMT =  $filter('number')(View.Info.BUSINESS_AMT,0);
								let info = {
										REPAIR_SID : View.Info.REPAIR_SID,
										PHOTO_YN : "N"
								}
								mainDataService
									.CommunicationRelay("/new_operation/getREPAIR_FILE.json",info)
									.success(function(array) {
										View.FileList = array;
									});
								
								let model = Grid.Models[1];
								model.condition.REPAIR_SID = View.Info.REPAIR_SID;
								$('#' + model.grid_id).jqGrid('setGridParam', {postData : model.condition, page : 1}).trigger('reloadGrid');	
								
								model = Grid.Models[2];
								model.condition.REPAIR_SID = View.Info.REPAIR_SID;
								$('#' + model.grid_id).jqGrid('setGridParam', {postData : model.condition, page : 1}).trigger('reloadGrid');
								
								pictiureInfo = {
						        		REPAIR_SID : View.Info.REPAIR_SID,
						        		ASSET_SID : ''
						        	}
					        	//storePicture.Init(pictiureInfo);
							},
							loadComplete: function() {
								var grid = $('#' + Grid.Models[0].grid_id);
								var ids = grid.jqGrid('getDataIDs');
								
								if(!View.Info)
									grid.jqGrid('setSelection', ids[0]);
								
								//선택 유지
								var rowData = grid.jqGrid('getRowData');
								for (var i = 0; i < rowData.length; i++) {
									if (rowData[i].REPAIR_SID == View.Info.REPAIR_SID) {
										grid.jqGrid('setSelection', ids[i]);
										break;
									}
								}
								$scope.totalCount = grid.getGridParam("records");
								if(grid.jqGrid('getRowData')[0].NUM=="") $scope.totalCount = 0;
							},
						},
						//1
						{
							grid_id : 'list2', 
							pager_id : 'listPager2', 
							url : '/new_operation/getREPAIR_ASSET.json', 
							//파라미터
							condition : {
								page : 1, 
								rows : GridConfig.sizeSS, 
								REPAIR_SID : 0, 
								PHOTO_YN: 'N', 
								Name : "2"
							}, 
							rowNum : 5,
						    colNames : [
						        	"순번","SID", "자산코드", '자산명', '보수내용', '지출항목구분', '비용', 
						        	'페스',
					        		'레벨1', '레벨2', '레벨3', '레벨4', '레벨5', '레벨6', '레벨7', '레벨8', 
				        	], 
						    colModel : [
						        //뷰 
						    	{name : 'NUM', width : 20, index : 'NUM', sorttype : 'int', resizable: false}, 
						    	{name : 'ASSET_SID', width : 40, index : 'ASSET_SID',hidden : true},
						    	{name : 'ASSET_CD', width : "80px", index : 'ASSET_CD', resizable: false}, 
						        {name : 'ASSET_NM', width : 40, index : 'ASSET_NM', resizable: false}, 
				 
						        {name : 'POPUP_DESC', width : 40, index : 'POPUP_DESC', resizable: false}, 
						        {name : 'EXPENSE_TYPE', hidden:true,width : 50, index : 'EXPENSE_TYPE', resizable: false}, 
						        {name : 'POPUP_AMT', hidden:true,width : 40, index : 'POPUP_AMT', resizable: false, formatter:  function ( cellvalue , options , rowdata )  { 
						        	return  cellvalue === null ? '' : $filter('number')(cellvalue,0);}}, 
						        {name : 'ASSET_PATH_CD', width : 0, index : 'ASSET_PATH_CD', hidden : true},
						        {name : 'LEVEL_1', width : 20, index : 'LEVEL_1', resizable: false}, 
						        {name : 'LEVEL_2', width : 20, index : 'LEVEL_2', resizable: false}, 
						        {name : 'LEVEL_3', width : 20, index : 'LEVEL_3', resizable: false}, 
						        {name : 'LEVEL_4', width : 20, index : 'LEVEL_4', resizable: false}, 
						        {name : 'LEVEL_5', width : 20, index : 'LEVEL_5', resizable: false}, 
						        {name : 'LEVEL_6', width : 20, index : 'LEVEL_6', resizable: false}, 
						        {name : 'LEVEL_7', width : 20, index : 'LEVEL_7', resizable: false}, 
						        {name : 'LEVEL_8', width : 20, index : 'LEVEL_8', resizable: false}, 
					        ],
							loadComplete: function(){
								Grid.formatLevel('#list2');
				            	$scope.total2Count = $('#' + Grid.Models[1].grid_id).getGridParam("records");
				            	if($('#' + Grid.Models[1].grid_id).jqGrid('getRowData')[0].NUM=="") $scope.total2Count = 0;								
							},
				            onSelectRow : function(rowid, status, e) {
				                var param = $('#list2').jqGrid('getRowData', rowid);
				                $scope.asset = param;
				            },
						},
						//2
						{
							grid_id : 'list4', 
							pager_id : 'listPager4', 
							url : '/new_operation/getREPAIR_ASSET.json', 
							//파라미터
							condition : {
								page : 1, 
								rows : GridConfig.sizeSS, 
								REPAIR_SID : 0, 
								PHOTO_YN: 'N', 
								Name : "2"
							}, 
							rowNum : GridConfig.sizeSS,
							colNames : [
					        	"순번","SID", "자산코드", '자산명', '보수내용', '지출항목구분', '비용', 
					        	'페스',
				        		'레벨1', '레벨2', '레벨3', '레벨4', '레벨5', '레벨6', '레벨7', '레벨8', 
				        	], 
				        	colModel : [
						        //뷰 
						    	{name : 'NUM', width : 20, index : 'NUM', sorttype : 'int', resizable: false}, 
						    	{name : 'ASSET_SID', width : 40, index : 'ASSET_SID',hidden : true},
						    	{name : 'ASSET_CD', width : "80px", index : 'ASSET_CD', resizable: false}, 
						        {name : 'ASSET_NM', width : 40, index : 'ASSET_NM', resizable: false}, 
				 
						        {name : 'POPUP_DESC', width : 40, index : 'POPUP_DESC', resizable: false}, 
						        {name : 'EXPENSE_TYPE',hidden:true, width : 50, index : 'EXPENSE_TYPE', resizable: false}, 
						        {name : 'POPUP_AMT',hidden:true, width : 40, index : 'POPUP_AMT', resizable: false, formatter:  function ( cellvalue , options , rowdata )  { 
						        	return  cellvalue === null ? '' : $filter('number')(cellvalue,0);}}, 
						        
						        {name : 'ASSET_PATH_CD', width : 0, index : 'ASSET_PATH_CD', hidden : true},
						        {name : 'LEVEL_1', width : 20, index : 'LEVEL_1', resizable: false}, 
						        {name : 'LEVEL_2', width : 20, index : 'LEVEL_2', resizable: false}, 
						        {name : 'LEVEL_3', width : 20, index : 'LEVEL_3', resizable: false}, 
						        {name : 'LEVEL_4', width : 20, index : 'LEVEL_4', resizable: false}, 
						        {name : 'LEVEL_5', width : 20, index : 'LEVEL_5', resizable: false}, 
						        {name : 'LEVEL_6', width : 20, index : 'LEVEL_6', resizable: false}, 
						        {name : 'LEVEL_7', width : 20, index : 'LEVEL_7', resizable: false}, 
						        {name : 'LEVEL_8', width : 20, index : 'LEVEL_8', resizable: false}, 
					        ],
					        onSelectRow : function(id) {
					        	let info = $('#list4').jqGrid('getRowData', id)
					        	pictiureInfo = {
					        		REPAIR_SID : View.Info.REPAIR_SID,
					        		ASSET_SID : info["ASSET_SID"],
					        		PHOTO_YN : "Y"
					        	}
								mainDataService
								.CommunicationRelay("/new_operation/getREPAIR_FILE.json",pictiureInfo)
								.success(function(array) {
									console.log(array);
									$scope.pic_list =  array;
								});					        	
					        	//Common_Picture.SetParam(pictiureInfo);
					        },
					        loadComplete: function(){
								Grid.formatLevel('#list4');
								var ids = $('#list4').jqGrid('getDataIDs');
								$('#list4').jqGrid('setSelection', ids[0]);
							}
						}
					],
					formatLevel : function(id) {
						let post = jQuery(id).jqGrid('getGridParam').postData;
						var ids = jQuery(id).getDataIDs();
						
						let type =  $rootScope.LevelName;
						for(var i=0; i < ids.length; i++){ 
							let rowObject = jQuery(id).jqGrid("getRowData",ids[i]);
							let info = {}
							if(rowObject.NUM == '')continue;
							
							for(let j =1; j < 9; j++){
								if(rowObject["LEVEL_" + j] == '') continue;
								info["LEVEL_" + j] = type[(j-1) + "_" + rowObject["LEVEL_" + j]];
							}
							
							if(rowObject.EXPENSE_TYPE != ''){
								info.EXPENSE_TYPE = FormatTYPE.TYPE[rowObject.EXPENSE_TYPE];
							}
							
							$(id).setRowData(ids[i],info);
						}
					}
				}
			}
	}	
	let papupModel = {
			colModel: [
		        {name : 'ASSET_CD', colName:'자산코드', width : '250px', type:'input',}, 
		        {name : 'ASSET_NM', colName:'자산명', width : '150px', type:'input',}, 
		        {name : 'POPUP_DESC', colName:'보수내용', width : '350px', type:'input', editable : true}, 
		        {name : 'EXPENSE_TYPE', colName:'지출항목구분',hidden:true, width : '93px', type:'select', editable : true,
		        	selectOptions: null, options: function(){ return FormatTYPE.TYPE; }}, 
		        {name : 'POPUP_AMT', colName:'비용',hidden:true, width : '300px', type:'int', editable : true, formatter: format.Number,}, 
        		{name : 'FIRST_PRICE', colName:'초기투자비용',hidden:true, width : '150px', type:'input'},
		        {name : 'LEVEL_1', colName:'레벨1', width : '150px', type:'input',
        			formatter:function(cellvalue,options,rowdata,action){return (rowdata.ASSET_PATH_CD==null)?"": format.getLevelName(rowdata.ASSET_PATH_CD,1);},}, 
		        {name : 'LEVEL_2', colName:'레벨2', width : '150px', type:'input',
    				formatter:function(cellvalue,options,rowdata,action){return (rowdata.ASSET_PATH_CD==null)?"": format.getLevelName(rowdata.ASSET_PATH_CD,2);},}, 
		        {name : 'LEVEL_3', colName:'레벨3', width : '150px', type:'input',
					formatter:function(cellvalue,options,rowdata,action){return (rowdata.ASSET_PATH_CD==null)?"": format.getLevelName(rowdata.ASSET_PATH_CD,3);},}, 
		        {name : 'LEVEL_4', colName:'레벨4', width : '150px', type:'input',
					formatter:function(cellvalue,options,rowdata,action){return (rowdata.ASSET_PATH_CD==null)?"": format.getLevelName(rowdata.ASSET_PATH_CD,4);},}, 
		        {name : 'LEVEL_5', colName:'레벨5', width : '150px', type:'input',
					formatter:function(cellvalue,options,rowdata,action){return (rowdata.ASSET_PATH_CD==null)?"": format.getLevelName(rowdata.ASSET_PATH_CD,5);},}, 
		        {name : 'LEVEL_6', colName:'레벨6', width : '150px', type:'input',
					formatter:function(cellvalue,options,rowdata,action){return (rowdata.ASSET_PATH_CD==null)?"": format.getLevelName(rowdata.ASSET_PATH_CD,6);},}, 
		        {name : 'LEVEL_7', colName:'레벨7', width : '150px', type:'input',
					formatter:function(cellvalue,options,rowdata,action){return (rowdata.ASSET_PATH_CD==null)?"": format.getLevelName(rowdata.ASSET_PATH_CD,7);},}, 
		        {name : 'LEVEL_8', colName:'레벨8', width : '150px', type:'input',
					formatter:function(cellvalue,options,rowdata,action){return (rowdata.ASSET_PATH_CD==null)?"": format.getLevelName(rowdata.ASSET_PATH_CD,8);},}, 
		        {name : 'ASSET_PATH_CD', colName:'페스', width : '0px', type:'input', hidden : true},
		        {name : 'ASSET_SID', colName:'SID', width : '0px',type:'input',hidden : true}, 
		        
			]
	}
	
	let View = {
		Info : {
				REPAIR_SID : '',
				POPUP_NM : '',
				REPAIR_START_DT : '',
				REPAIR_END_DT : '',
				CONTRACT_DT : '',
				COMPANY_NM : '',
				BUSINESS_AMT : '',
				C_MEMO : '',
				INSERT_ID : '',
		},
		FileList : [],
		New : function() {
			$rootScope
				.$broadcast("New_And_ModifiedPopup", {
					"URLS" : {
						insert:'/new_operation/insertREPAIR.json',
						save: '/new_operation/saveREPAIR.json',	
						insertFILE: "/new_operation/insertREPAIR_FILE.json",
						deleteFile:"/new_operation/deleteREPAIR_FILE.json"
					},
					"colModel": papupModel.colModel,
					"ID_1" : "개보수 사업 등록",
					"ID_2" : "개보수 사업 관리"
				});
		},
		Modified : function() {
			//수정
			$rootScope
				.$broadcast("New_And_ModifiedPopup", {
					"URLS" : {
						insert : "/new_operation/getREPAIR_MODIFIED.json",
						save : "/new_operation/saveREPAIR.json",
						insertFILE: "/new_operation/insertREPAIR_FILE.json",
						deleteFile:"/new_operation/deleteREPAIR_FILE.json"
					},
					"insertParam":{REPAIR_SID: this.Info.REPAIR_SID, PHOTO_YN : "N"},
					"colModel": papupModel.colModel,
					"ID_1" : "개보수 사업 수정",
					"ID_2" : "개보수 사업 관리"
				});
		},
		Report : function() {
			window.open('/excel/downloadExcel.do?EXCEL_ID=010202&REPAIR_SID=' + this.Info.REPAIR_SID,'_blank');
		}
	}
	
	$scope.ShowAssetGisInfoPopup = function() {
        var param = { ASSET_SID: $scope.asset.ASSET_SID };
        mainDataService.getAssetGisInfo(param).success(function(data) {
        	console.log(data);
        	if(data==null|| data==''){
        		alertify.alert('info','시설자산은 위치정보가 없습니다.');
        		return;
        	}
			if(data.GEOM_TEXT=="") {
				alertify.alert("Info","표시할 위치정보가 없습니다.",function(){});
				return;
			}
            $rootScope.$broadcast('showGisPopup', {
                init: function() {
                    var layer_name = data.LAYER;
                    var ftr_idn = '\'' + data.FTR_IDN + '\'';
                    showAsset(layer_name, ftr_idn);
                },
                callbackId: 'RegistAssetItem_selectGisInfo'
            });
        });
    };
    
	$scope.Serch = Search;
	$scope.View = View;
	$scope.storePicture = pictureModel;
	
    $scope.onKeyPress = function(event) {
        if (event.key === 'Enter') {
        	$scope.Serch.Lookup();
        }
    }
    
	$scope.uploadPic = function (picInfo){
        console.log(picInfo);
        $rootScope.$broadcast("showImageUploadPopup", {
            'picInfo' : picInfo //ASSET_SID, SEQ
            }
        );

        $rootScope.callback_savePicture = function(obj){
            console.log(picInfo);
            console.log(obj);
     
            var info = picInfo;
			if(errMessageAlert(obj)) return;
			
			info.F_NUM = obj.f_num;
			info.PHOTO_YN = "Y";
			info.PHOTO_NUM = picInfo.SEQ;
			info.F_DESC = obj.F_DESC;
			info.F_PATH = obj.F_PATH;
			mainDataService
				.CommunicationRelay('/new_operation/insertREPAIR_FILE.json', info)
				.success(function(obj) {
					//PicturePopup.Completed();
					})
				.error(function(error) {
					alertify.error('사진을 저장하던 중 문제가 생겼습니다.')
					});
			            
        }
        $scope.image_popupInfo = [];
        $('#dialog-imageUpload-popup2').show();
    }

    $scope.showPic = function(pic){
    	$rootScope.$broadcast("popupShowPicture",{URL:pic.F_PATH,DESC:pic.F_DESC,pic,
    		callbackDeletePic:function(data){
    		debugger;
            console.log(data);
            var picInfo = data.pic;
            
			if(!picInfo.F_NUM){
				return;
			}
			let Info = picInfo;
			Info.PHOTO_YN = 'Y';
			mainDataService
				.CommunicationRelay('/new_operation/deleteREPAIR_FILE.json', picInfo)
				.success(function(info) {
					picInfo.F_PATH = '';
					picInfo.F_DESC = '';					
					if(errMessageAlert(info)) return;
					mainDataService
						.fileDelete(picInfo.F_NUM)
						.success(function(obj) {
							if(errMessageAlert(obj)) return;
							alertify.success('삭제되었습니다.');
							picInfo.F_NUM = '';
						});
				});            
        },
        updateDlgId : '#dialog-imageUpload-popup2',
        callbackUpdatePic:function(obj){
        	console.log(obj);
        	var picInfo = obj;
        	debugger;
            var param = {
                REPAIR_SID: obj.pic.REPAIR_SID,
                ASSET_SID: obj.pic.ASSET_SID,
                PHOTO_NUM: obj.pic.SEQ,
                F_NUM: (obj.f_num>0 && typeof obj.pic !='undefined' )?obj.pic.F_NUM:obj.F_NUM,
            };
            if(obj.f_num>0 && typeof obj.pic !='undefined' ){
            	obj.ASSET_SID = obj.pic.ASSET_SID;
            	obj.SEQ = obj.pic.SEQ;
            }
            
			mainDataService
			.CommunicationRelay('/new_operation/deleteREPAIR_FILE.json', param)
			.success(function(obj1) {
				if(errMessageAlert(obj1)) return;
				
				mainDataService
				.fileDelete(param.F_NUM)
				.success(function(obj) {
					if(errMessageAlert(obj)) return;
				});
				
	            var info = picInfo;
				//if(errMessageAlert(obj)) return;
				info.REPAIR_SID = picInfo.pic.REPAIR_SID; 
				info.F_NUM = info.f_num;
				info.PHOTO_YN = "Y";
				info.PHOTO_NUM = picInfo.SEQ;
				info.F_DESC = info.F_DESC;
				info.F_PATH = info.F_PATH;
				mainDataService
					.CommunicationRelay('/new_operation/insertREPAIR_FILE.json', info)
					.success(function(obj) {
						picInfo.pic.F_PATH = info.F_PATH;
						picInfo.pic.F_DESC = info.F_DESC;
						picInfo.pic.F_NUM = info.F_NUM;
						alertify.success('수정되었습니다.');
						//PicturePopup.Completed();
						})
					.error(function(error) {
						alertify.error('사진을 저장하던 중 문제가 생겼습니다.')
						});
				
			});            
            
            $('#dialog-imageUpload-popup2').hide();
        }
      });
    }    
}
//신규 수정
function New_And_ModifiedPopupController($scope,$filter, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, MaxUploadSize) {
	$scope.$on("New_And_ModifiedPopup", function(e, data) {
		setDatePicker();
		PopupView.URLS = data.URLS;
		mainDataService
			.CommunicationRelay(PopupView.URLS.insert, data.insertParam)
			.success(function(obj) {
				console.log(obj)
				PopupView.Info = obj.Info;
				PopupView.Info.PHOTO_YN = 'N';
				PopupView.Info.BUSINESS_AMT = $filter('number')(PopupView.Info.BUSINESS_AMT, 0); 
				PopupView.FileList = obj.FileList;
				PopupView.colModel = data.colModel; //[{width:'50px',name:'xxx', editable : true, type:'input'},{width:'100px',name:'xxx2',editable : true, type:'input'},{width:'200px',name:'xxx3'},{width:'400px',name:'xxx4'}]
				ng_grid.LevelNames = ng_grid.SetLevelNames();
				PopupView.List = ng_grid.Init(PopupView.colModel, obj.List);
			});
		
		$('#New_And_ModifiedPopup_1').text(data.ID_1);
		$('#New_And_ModifiedPopup_2').text(data.ID_2);
		console.log(data.ID_1)
		console.log(data.ID_2)
		Model.colNames[3] =  data.ID_1 == "개보수 사업 수정" ? "보수내용" : "조사내용"
		console.log(Model)
		Model.SheetName = data.ID_2.replace(' ','').replace(' ','');
		data.ID_1 = data.ID_1.replace(' ','').replace(' ','');
		Model.Name = data.ID_1+"_양식_"+ $scope.getToday()+".xlsx";
		PopupView.File = null;
		
		$scope.isOperation_popup = true;
	});
	
	let New_And_ModifiedPopupITEM  = [
		{'name' : 'POPUP_NM', 'title' : '사업명', 'required' : true, 'maxlength' : 60}, 
		{'name' : 'INSERT_ID', 'title' : '등록자', 'required' : true, 'maxlength' : 10}, 
		{'name' : 'COMPANY_NM', 'title' : '거래처명', 'required' : true, 'maxlength' : 10},
		{'name' : 'BUSINESS_AMT', 'title' : '사업비용', 'required' : false, 'maxlength' : 100},
		{'name' : 'START_DT', 'title' : '착공일자','required' : true, 'maxlength' : 20},
		{'name' : 'CONTRACT_DT', 'title' : '계약일자', 'required' : true, 'maxlength' : 20}, 
		{'name' : 'END_DT', 'title' : '준공일자', 'required' : true, 'maxlength' : 20 }, 
		{'name' : 'C_MEMO', 'title' : '특이사항', 'required' : false, 'maxlength' : 400}
	]
	
	let BUSINESS_AMT_ITEM  = [ 
		{'name' : 'BUSINESS_AMT', 'title' : '사업비용', 'required' : false, 'maxlength' : 100}, 
	]
	
	let Model = {
			 ListName: 'ExcelList',
			 SheetName: $scope.ID_2,
			 Name: $scope.ID_1+"양식_"+ $scope.getToday()+".xlsx",
			 colNames: ["순번", "자산코드", "자산명",'', 
				 '지출항목구분','비용','초기투자비용','레벨1','레벨2','레벨3','레벨4','레벨5','레벨6','레벨7','레벨8'],
			 colModel:[
				{Name: "RNUM", wpx:50},
				{Name: "ASSET_CD", wpx:250},
				{Name: "ASSET_NM", wpx:100},
				{Name: "POPUP_DESC", wpx:250},
				{Name: "EXPENSE_TYPE", wpx:50},
				{Name: "POPUP_AMT", wpx:150},
				{Name: "FIRST_PRICE", wpx:100},
				{Name: "LEVEL_1", wpx:100},
				{Name: "LEVEL_2", wpx:100},
				{Name: "LEVEL_3", wpx:100},
				{Name: "LEVEL_4", wpx:100},
				{Name: "LEVEL_5", wpx:100},
				{Name: "LEVEL_6", wpx:100},
				{Name: "LEVEL_7", wpx:100},
				{Name: "LEVEL_8", wpx:100},
			 ]
		 }
	
	let ng_grid = {	
		colModel: null,
		LevelNames: null,
		SetList: new Set(),
		Size: 0,
		Init: function(colModel, List){
			this.colModel = colModel;
			this.SetList = new Set();
			this.Size = GridConfig.sizeSS
			return this.SetData(List);
		},
		Blur: function(row, col) {
			let map = this.colModel[col]
			let list = PopupView.List[row];
			if(!list[map.name]) return;
			
			let cellvalue = list[map.name].toString();
			list[map.name] = map.formatter(cellvalue,'',map,'');
		},
		SetData: function(data) {
			let SetList = this.SetList;
			if(SetList.size == 0) {
				this.List = [];
			}
			
			let model = this.colModel;
			let map = {};
			let bindex = SetList.size;
				
			if(bindex!=0 && bindex < this.Size) {
				let list = this.List;
				for(i in list) 
					if(!list[i].ASSET_CD) delete list[i];
				
				this.List = list.filter((element) => true);
			}
				
			for(i in data) {
				bindex = SetList.size;
				SetList.add(data[i].ASSET_CD);
				if(bindex != SetList.size) {
					map = {};
					for(j in model) {
						if(data[i][model[j].name])
							map[model[j].name] = data[i][model[j].name];
					}
					this.List.push(map);
				}
			}
			
			this.Update();
			
			return this.RemainRowMin(this.List, this.Size);
		},
		SetOnloadData: function(data) {
			let setlist = this.SetList = new Set();
			let list = new Array(data.length);
			
			colModel = Model.colModel;
			colNames = Model.colNames;
			let map = null;
			for(i in data) {
				map = {}
				for(j in colNames){
					if(colModel[j].Name != 'RNUM') 
						map[colModel[j].Name] = data[i][colNames[j]];
				}
				if(!map['ASSET_CD'])continue;
				
				if(map['EXPENSE_TYPE']) 
					map['EXPENSE_TYPE'] = map['EXPENSE_TYPE'].toString();
				
				if(map['POPUPR_AMT']) 
					map['POPUP_AMT'] = $filter('number')(map['POPUP_AMT'].toString(), 0);
				
				if(!map['ASSET_PATH_CD']) {
					let ASSET_PATH_CD = ''
					for(let j=1; j<15; j++) {
						ASSET_PATH_CD += this.GetLevelNum(j - 1, map['LEVEL_'+ j]) + '_' 
					}
					map['ASSET_PATH_CD'] = ASSET_PATH_CD;
				}
				
				list[i] = map
			}
			
			return this.SetData(list);
		},
		RemainRowMin: function(list, rowMin){
			let len = rowMin - list.length;
			for(let i =0; i < len;  i++) {
				list.push({});
			}
			return list;
		},
		Update: function() {
			let List = this.List;
			let maps = this.colModel;
			let map = null;
			let name = '';
			for(i in maps) {
				map = maps[i];
				if(map['formatter']) {
					for(j in this.List) {
						name = map['name'];
						List[j][name] = map.formatter(List[j][name], '', List[j], '');	
					}
				}
				
				if(map['type'] == 'select') {
					map['selectOptions'] = map.options();
				}
			}
		},
		Delete: function(list) {
			let SetList = this.SetList;
			for(i in list) {
				if(list[i].isCheckbox){
					SetList.delete(list[i].ASSET_CD);
					delete list[i];
				}
			}
			
			this.List = list.filter((element) => true);
			
			return this.RemainRowMin(this.List, this.Size);
		},
		SetLevelNames: function() {
			let List = $rootScope.LevelName;
			let map = {};
			for(key in List){
				let strA = key.split('_');
				
				if(!map[strA[0]])
					map[strA[0]] = {}
				
				map[strA[0]][List[key]] = strA[1];
			}
			
			return map;
		},
		GetLevelNum:function(level, str){
			if(!str)return '';
			return this.LevelNames[level][str];
		}
	}
	
	let PopupView = {
			Info : {
				POPUP_SID : '',
				POPUP_NM : '',
				START_DT : '',
				END_DT : '',
				CONTRACT_DT : '',
				COMPANY_NM : '',
				BUSINESS_AMT : '',
				C_MEMO : '',
				INSERT_ID : '',
				PHOTO_YN:'N'
			},
			URLS : {},
			List: null,
			FileList : [],
			File: null,
			CMD: null,
			isCheckBoxAll: false,
			Cancel : function() {// 취소
				// alert("test");
				// $scope.isOperation_popup = false;
				alertify.confirm('Info','작성 중인 내용이 저장되지 않았습니다. 정말 취소하시겠습니까?'
					,function(){
						//$scope.userModalSelet =false;
						$scope.isOperation_popup = false;
						alertify.alert('Info','취소되었습니다.',function(){
							return;
						});
						return;
					}
					,function(){
						return;
					});
			},
			Distribution : function() {//비용 처리
				if(!ItemValidation(this.Info, BUSINESS_AMT_ITEM)) return;
				
				let cost = this.Info.BUSINESS_AMT;
				
				if(cost == '' || cost == undefined || cost == null || cost == '0') {
					alertify.alert("Info","비용을 입력하세요.");
					return;
				}
				
				let list = PopupView.List;
				if(!list[0].ASSET_CD) {
					alertify.alert("Info","자산을 입력하세요.");
					return;
				}
				
				$rootScope
					.$broadcast("openDistribution_Popup", {
						COST : this.Info.BUSINESS_AMT,
						List : PopupView.List,
						Size : ng_grid.Size
					});
			},
			Save : function() {//저장
				if(!ItemValidation(this.Info, New_And_ModifiedPopupITEM)) return;
				
				let str = this.Info.BUSINESS_AMT;
				let BUSINESS_AMT = Number(Common_sts(str));
				
				if(this.Info.CONTRACT_DT > this.Info.START_DT){
					alertify.alert("Info","계약일자은 착공일자보다 클 수 없습니다.");
					return;
				}
				
				if(this.Info.START_DT > this.Info.END_DT) {
					alertify.alert("Info","착공일자은 준공일자보다 클 수 없습니다.");
					return;
				}
				/*
				if(ng_grid.SetList.size == 0) {
					alertify.alert("","자산을 선택해 주세요.");
					return;
				}
				*/
				this.Info.COMPANY_NM = xssFilter(this.Info.COMPANY_NM);
				this.Info.POPUP_NM = xssFilter(this.Info.POPUP_NM);
				
				let list = this.List;
				let sum = 0;
				/*
				for (i in list){
					if(list[i].POPUP_AMT)
						sum += Number(Common_sts(list[i].POPUP_AMT));
					
					if(list[i].ASSET_CD && $(".selectTest").eq(i).val() == '') {
						alertify.alert((Number(i)+1)+ '번의 ' + "지출항목을 선택하세요.");
						return;
					}
				}
				
				if(sum != BUSINESS_AMT) {
					alertify.alert("자산의 총 비용: " +  $filter('number')(sum, 0) + ", 사업비용: " +  $filter('number')(BUSINESS_AMT, 0) + "<br> 자산의 총 비용과 사업비용이 다릅니다.");
					return;
				}
				*/
				this.Info.BUSINESS_AMT = BUSINESS_AMT;
				mainDataService
					.CommunicationRelay(PopupView.URLS.save, {List : this.List, FileList: this.FileList, Info : this.Info})
					.success(function(sInfo) {
						if(Common_AlertStr(sInfo)) return;
						
						$('#list').trigger('reloadGrid');
						
						$scope.isOperation_popup = false;
					});
				
			},
			Delete : function() {//자산 삭제
				PopupView.List = ng_grid.Delete(PopupView.List);
				this.isCheckBoxAll = false;
			},
			AssetSelection : function() {//자산 받기
				$rootScope
					.$broadcast('showGisPopup',{callbackId:'assetAddFromSelectGisInfo'});
			},
			DiagnosticArea : function() {
			},
			FormOutput : function() {//양식 출력
				Common_workSheetLoading.Init(Model,null);
				
				 let list = angular.copy(PopupView.List);
				 
				 for(let i = 0; i<list.length; i++){
					 list[i]["RNUM"] = i + 1
					 delete list[i].ASSET_SID
					 delete list[i].ASSET_PATH_CD
				 }
				 
				 Common_workSheetLoading.workBookDownload(Model, list);
			},
			OnFileSelect : function($files, cmd, index) {//파일 받기
				if(FileCheck($files, cmd, MaxUploadSize)){
					PopupView.File = null;
					return;
				}
				
				switch (index) {
					case 1:
						PopupView.File = $files;
						PopupView.CMD = cmd;
					break;
					case 2 : 
						var reader = new FileReader();
						reader.onload = function(e) {
							const workBook =XLSX.read(reader.result, {type :'binary'});
							workBook.SheetNames.forEach(sheetName => {
								const rows =XLSX.utils.sheet_to_json(workBook.Sheets[sheetName]);
								
								PopupView.List = ng_grid.SetOnloadData(rows);
								$scope.$apply();
								alertify.success('성공');
							});
						};
						
						reader.readAsBinaryString($files[0]);
					break;
				}
			},
			FileUpload:function() {//파일 업로드
				let files = PopupView.File;
				//if(FileCheck(files, PopupView.CMD, MaxUploadSize)) return;
				if(files==null || files.length==0  ){
					alertify.alert('Info','파일을 선택하세요.',function(){});
				}
				if(this.FileList.length >= 5){
					alertify.alert('Info','첨부파일은 최대 5개 까지입니다.',function(){});
					 return;	
				}

				if (files[0].size === 0) {
					alertify.alert('Info','사이즈가 0인 파일은 업로드 할 수 없습니다.');
					return;
				}

				let fileDesc = files[0].name;
				let path = 'new_operation';
				let dirType = fileDesc.split('.')[1];
				let bookOpt = '';
				let list = this.FileList;
				let info = this.Info;
				mainDataService
					.fileUpload(fileDesc, path, files, dirType, bookOpt)
					.success(function(obj) {
						if(errMessageAlert(obj)) return;
						
						let file = {}
					   	file.files = files;
					   	file.RNUM = list.length + 1;
					   	file.F_TITLE = files[0].name;
					    file.F_SIZE = files[0].size;
					   	file.F_NUM = obj.f_num;
					   	file.PHOTO_YN = 'N';
						list.push(file);
						PopupView.File = null;
						
						let fileInfo = {
								SID : 	info.POPUP_SID,
								ASSET_SID : 0,
								F_NUM : file.F_NUM,
								F_DESC : '',
								PHOTO_YN : 'N',
								PHOTO_NUM : 999
						}
						mainDataService
							.CommunicationRelay(PopupView.URLS.insertFILE, fileInfo)
							.success(function(sInfo) {
								alertify.success('파일업로드성공');
							});
					});
			},
			FileListDelete : function(index) {//파일 삭제
				alertify.confirm('삭제 확인','자료를 삭제 하시겠습니까?', function() {
					let list = PopupView.FileList;
					//첨부 삭제
					mainDataService
						.CommunicationRelay(PopupView.URLS.deleteFile, {F_NUM: list[index].F_NUM, SID: PopupView.Info.POPUP_SID, PHOTO_YN: 'N'})
						.success(function(int) {
							if(errMessageAlert(int)) return;
							//파일 삭제
							mainDataService
								.fileDelete(list[index].F_NUM)
								.success(function(obj) {
									if(errMessageAlert(obj)) return;
									
									list.splice(index, 1); 
									for(let i = index; i < list.length; i++) {
										list[i].RNUM = i + 1;
									}
									
									alertify.success('삭제되었습니다.');
								});
						});
				}, function() {});
			},
			NumFormat : function(key) {//문자열 , 제거 
				let str = this.Info[key].toString();
				let temp = str.split(',');
				str = '';
				for(i in temp){
					str += temp[i];
				}
				
				this.Info[key] = $filter('number')(str,0);
			},
			Blur: function(row, col) {//값이 변경된 경우
				ng_grid.Blur(row, col);
			},
			SelectAll: function(){//자산 전체 체크
				let list = this.List;
				for(i in list){
					list[i]['isCheckbox'] = this.isCheckBoxAll;	
				}
				
				this.List = list;
			}
		}
		//지도에서 정보 받기
		$scope.$on('assetAddFromSelectGisInfo',function(event, data) {
				var str = ''
				let list = data.data;
				for(i in list) {
					if(list[i].ftr_idn)
						str += "'" + list[i].ftr_idn + "'" + ",";
				}
				str = str.length >= 1 ? str.substr(0, str.length -1) : '';
				
				var param = {FTR_IDN : str, LAYER_CD: data.data[0].fid};
				mainDataService
					.getAssetFromGisInfo(param)
					.success(function(data2) {
						if(Common_AlertStr(data2)) return;
						
						PopupView.List = ng_grid.SetData(data2);
					});
			});
	
	$scope.showPopupLevelPath = function (opt,idx)
    {
    	$scope.level_path_nm = "";
    	$("#dialog-assetlevelpath").show();
    	//$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'multi'});
    	$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'costlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'path',opt:opt,idx:idx});
    }
    $scope.$on('costlist_selectAssetLevelPath',function(event,data){
    	console.log(data);
    	$scope.asset_path_sid = data.ASSET_PATH_SID;
    	$scope.level_path_cd = data.LEVEL_PATH_CD;
    	$scope.level_path_nm = data.LEVEL_PATH_NM;
    	
    	//$("#dialog-AssetListPopup").show();
    	if(data.ASSET_PATH_SID==0){
    		data.ASSET_PATH_CD = data.LEVEL_PATH_CD;
    	}
    	//if(data.opt==2){
    		data.callbackId = 'costlist_selectedAssetList';
    		$rootScope.$broadcast("SelectAssetListPopup",data);
    	//}
    });
    
    $scope.$on('costlist_selectedAssetList',function(event,data){
    	console.log(data);
   		//var lastRowNum = $("#list_asset_popup").getGridParam("reccount");
   		//var lastRowId = $("#list_asset_popup").find('tbody tr:last').get(0).id;
    	var cnt = $scope.PopupView.List.length;
    	/*
		{Name: "RNUM", wpx:50},
		{Name: "ASSET_CD", wpx:250},
		{Name: "ASSET_NM", wpx:100},
		{Name: "POPUP_DESC", wpx:250},
		{Name: "EXPENSE_TYPE", wpx:50},
		{Name: "POPUP_AMT", wpx:150},
		{Name: "FIRST_PRICE", wpx:100},
		{Name: "LEVEL_1", wpx:100},
		{Name: "LEVEL_2", wpx:100},
		{Name: "LEVEL_3", wpx:100},
		{Name: "LEVEL_4", wpx:100},
		{Name: "LEVEL_5", wpx:100},
		{Name: "LEVEL_6", wpx:100},
		{Name: "LEVEL_7", wpx:100},
		{Name: "LEVEL_8", wpx:100},
   		*/
		var list = [];
    		
    		$.each(data.list,function(idx,Item){
    			var FN_CD = Item.ASSET_CD.substring(11,12);
    			var ASSET_PATH_CD = Item.ASSET_CD.substring(0,4) + '_'
						+ Item.ASSET_CD.substring(4,11) +'_'
						+ Item.ASSET_CD.substring(11,12) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(12,15):Item.ASSET_CD.substring(12,13)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(15,18):Item.ASSET_CD.substring(13,14)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(18,19):Item.ASSET_CD.substring(14,17)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(19,22):Item.ASSET_CD.substring(17,22)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(22,27):'_')+'_______';
					
    			list.push({RNUM:cnt + 1 + idx,ASSET_SID:Item.SID,ASSET_CD:Item.ASSET_CD,ASSET_NM:Item.ASSET_NM
    				,ASSET_PATH_CD: ASSET_PATH_CD  					
   					});
    		});
    		/*
            $("#list_asset_popup").jqGrid("addRowData", lastRowNum+1, list, 'last');
            var lastRowId_onlyNum = Number(lastRowId.replace(/[^0-9]/g,''));
            for (var i = 0; i < data.length; i++) {
                $("#list_asset_popup").find("#jqg"+(lastRowId_onlyNum+i+1)).find("td[aria-describedby=list_asset_popup_NUM]").html(i+1+lastRowNum);
            }
          	*/
    	$scope.PopupView.List = ng_grid.SetData(list);
    	alertify.alert('Info','선택적용 되었습니다.',function(){
    		
    	});
    });
	
		$scope.PopupView = PopupView;
}
//배분 팝업
function renovation_PopupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message, $filter) {
	$scope.$on("openDistribution_Popup", function(e, data) {
		$scope.distributionInfo = {};
		$scope.distributionInfo.OPTION = 0;
		
		Cost = data.COST;
		List = data.List;
		Size = data.Size;
		//ID = data.ID;
		
		$scope.isRenovation_popup = true;
	});
	
	let Cost = null;
	let List = null;
	let Size = null;
	let ID = null;
	$scope.close = function() {
		$scope.isRenovation_popup = false;
	}
	
	$scope.radioClick = function(option) {
		$scope.distributionInfo.OPTION = option;
	}
	
	$scope.execution = function(option = $scope.distributionInfo.OPTION) {
		Cost = Number(Common_sts(Cost));
		
		switch(option) {
		case 0 :
			{
				let len = 0;
				for(i in List) {
					if(List[i]['ASSET_CD'])
						len++;
				}
				
				let fraction = Cost/len; 
				let sum = 0;
				let fraction0 = 0;
				for(i in List) {
					if(List[i]['ASSET_CD']) {
						List[i]['POPUP_AMT'] = $filter('number')(fraction, 0);
						sum += Number(Common_sts(List[i]['POPUP_AMT']));
					}
				}
			}
		break;
		case 1 :
			{
				let totalFIRST_AMT = 0;
				let sum = 0;
				for(i in List) {
					if(List[i]['FIRST_PRICE'])
						totalFIRST_AMT += Number(Common_sts(List[i]['FIRST_PRICE']));
				}
				totalFIRST_AMT = totalFIRST_AMT == 0 ? 1: totalFIRST_AMT;
						
				let  FIRST_AMT = 0;
				for(i in List) {
					FIRST_AMT = List[i]['FIRST_PRICE'] ? Number(Common_sts(List[i]['FIRST_PRICE'])) : 0;
					List[i]['POPUP_AMT'] =  Cost * (FIRST_AMT / totalFIRST_AMT);
					List[i]['POPUP_AMT'] = $filter('number')(List[i]['POPUP_AMT'], 0);
					sum += Number(Common_sts(List[i]['POPUP_AMT']));
				}
			}
		break;
		}
		$scope.isRenovation_popup = false;
	}
	
	$scope.alert = function(str) {
		 alertify.confirm(str); 
	}
}
//수정화면
function image_PopupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	$scope.$on("openImage_Popup", function(e, data) {
		Model = data.Model;
		photo.Info = angular.copy(Model.param);
		
		let fileParamlist = Model.fileParam;
		for(i in fileParamlist) {
			photo.Info[fileParamlist[i]] = data.Info[fileParamlist[i]];
		}
		
		$scope.isImage_popup = true;
	});	
	let data = null;
	let Model = null;
	let photo = {
		Cancel : function() {
			$scope.isImage_popup = false;
		},
		Remove : function() {
			if(!this.Info.F_NUM){
				$scope.isImage_popup = false;
				return;
			}
			let Info = this.Info;
			Info.PHOTO_YN = 'Y';
			mainDataService
				.CommunicationRelay(Model.deleteUrl, this.Info)
				.success(function(info) {
					if(errMessageAlert(info)) return;
					
					mainDataService
						.fileDelete(Info.F_NUM)
						.success(function(obj) {
							if(errMessageAlert(obj)) return;
							Model.parents.Init();
							$scope.isImage_popup = false;
							alertify.success('삭제되었습니다.');
						});
				});
		},
		Correction : function() {
			$rootScope
				.$broadcast("openSys_Popup4", {Model : Model, Info: this.Info});
			$scope.isImage_popup = false;
		}
	}
	
	//사진 수정 취소
	$scope.photo = photo;
}
//이미지 저장 팝업
function renovationPopupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService, YearSet, GridConfig, Message) {
	$scope.$on("openSys_Popup4", function(e, data) {
		
		PicturePopup.Info = data.Info;
		Model = data.Model;
		
		files = null;
		Data = angular.copy(data);
		
		$scope.isSys_popup4 = true;
	});	
	
	let Model = null;
	let Data = null;
	let files = null;
	
	let Sys_Popup4Item = [ 
		{'name' : 'F_TITLE', 'title' : '사진', 'required' : true, 'maxlength' : 50}, 
		{'name' : 'F_DESC', 'title' : '내용', 'required' : true, 'maxlength' : 50}, 
	]
	
	let PicturePopup = {
			Info : {},
			Cancel : function() {
				$rootScope.$broadcast("openImage_Popup", Data);
				$scope.isSys_popup4 = false;
			},
			Confirm : function() {
				if(!ItemValidation(this.Info, Sys_Popup4Item)) return;
				
				this.Info.F_NUM ? this.Update(files, this.Info) : this.Insert(files, this.Info);
			},
			Insert : function(file, info) {
				info.PHOTO_YN = "Y";
				var fileDesc = file[0].name;
				var path = 'new_operation';
				var dirType = fileDesc.split('.')[1];
				var bookOpt = '';
				mainDataService
				.fileUpload(fileDesc, path, file, dirType, bookOpt)
					.success(function(obj) {
						if(errMessageAlert(obj)) return;
						
						info.F_NUM = obj.f_num;
						
						mainDataService
							.CommunicationRelay(Model.insertUrl, info)
							.success(function(obj) {PicturePopup.Completed();})
							.error(function(error) {alertify.error('사진을 저장하던 중 문제가 생겼습니다.')});
					});
			},
			Update : function(files, info) {
				info.BF_NUM = info.F_NUM;
				
				files ? PicturePopup.updateFileService(files, info) : PicturePopup.updateService(info);
			},
			updateFileService: function(files, info) {
				var fileDesc = files[0].name;
				var path = 'new_operation';
				var dirType = fileDesc.split('.')[1];
				var bookOpt = '';
				mainDataService
					.fileUpload(fileDesc, path, files, dirType, bookOpt)
					.success(function(obj) {
						if(errMessageAlert(obj)) return;
						
						info.F_NUM = obj.f_num;
						PicturePopup.updateService(info);
					});
			},
			updateService: function(info) {
				mainDataService
					.CommunicationRelay(Model.updateUrl, info)
					.success(function(obj) {PicturePopup.Completed();})
					.error(function(error) {alertify.error('사진을 저장하던 중 문제가 생겼습니다.')});
			},
			Completed: function(){
				Model.parents.Init();
				
				alertify.success('변경완료.');
				
				$scope.isSys_popup4 = false;
			},
			OnFileSelect : function($files, cmb) {
				if(extensionCheck($files, cmb)) return;
				
				this.Info.F_TITLE = $files[0].name;
				
				files = $files;
			},
	}
	
	$scope.PicturePopup = PicturePopup;
	
	function extensionCheck(files,cmd) {
		if(files.length == 0) return true;
		
        var str = '\.(' + cmd + ')$';
        var FileFilter = new RegExp(str);
        var ext = cmd.split('|');
        if(!files[0].name.toLowerCase().match(FileFilter)) {
        	alertify.alert('Info', '확장자가 ' + ext + ' 인 파일만 선택가능합니다.');
        	return true;
        }
        
        return false;
	}
	

    
	/*
    $scope.showPopupLevelPath = function (opt,idx)
    {
    	alert('showpopup')
    	$scope.level_path_nm = "";
    	$("#dialog-assetlevelpath").show();
    	//$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'itemlist_selectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'multi'});
    	$rootScope.$broadcast('loadAssetLevelPathList',{callbackId:'PopupViewSelectAssetLevelPath',namefilter:$scope.level_path_nm,returnType:'path',opt:opt,idx:idx});
    }
    $scope.$on('PopupViewSelectAssetLevelPath',function(event,data){
    	console.log(data);
    	$scope.asset_path_sid = data.ASSET_PATH_SID;
    	$scope.level_path_cd = data.LEVEL_PATH_CD;
    	$scope.level_path_nm = data.LEVEL_PATH_NM;
    	
    	//$("#dialog-AssetListPopup").show();
    	if(data.ASSET_PATH_SID==0){
    		data.ASSET_PATH_CD = data.LEVEL_PATH_CD;
    	}
    	//if(data.opt==2){
    		data.callbackId = 'PopupViewSelectedAssetList';
    		$rootScope.$broadcast("SelectAssetListPopup",data);
    	//}
    });
    
    $scope.$on('PopupViewSelectedAssetList',function(event,data){
    	console.log(data);
   		//var lastRowNum = $("#list_asset_popup").getGridParam("reccount");
   		//var lastRowId = $("#list_asset_popup").find('tbody tr:last').get(0).id;
   		var list = [];
    		
    		$.each(data.list,function(idx,Item){
    			var FN_CD = Item.ASSET_CD.substring(11,12);
    			var ASSET_PATH_CD = Item.ASSET_CD.substring(0,4) + '_'
						+ Item.ASSET_CD.substring(4,11) +'_'
						+ Item.ASSET_CD.substring(11,12) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(12,15):Item.ASSET_CD.substring(12,13)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(15,18):Item.ASSET_CD.substring(13,14)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(18,19):Item.ASSET_CD.substring(14,17)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(19,22):Item.ASSET_CD.substring(17,22)) +'_'
						+((FN_CD=='F')?Item.ASSET_CD.substring(22,27):'_')+'_______';
					
    			list.push({NUM:lastRowNum +1+idx,ASSET_SID:Item.SID,ASSET_CD:Item.ASSET_CD,ASSET_NM:Item.ASSET_NM
    				,ASSET_PATH_CD: ASSET_PATH_CD  					
   					});
    		});
    		/ *
            $("#list_asset_popup").jqGrid("addRowData", lastRowNum+1, list, 'last');
            var lastRowId_onlyNum = Number(lastRowId.replace(/[^0-9]/g,''));
            for (var i = 0; i < data.length; i++) {
                $("#list_asset_popup").find("#jqg"+(lastRowId_onlyNum+i+1)).find("td[aria-describedby=list_asset_popup_NUM]").html(i+1+lastRowNum);
            }* /
            
    });
    */
}

