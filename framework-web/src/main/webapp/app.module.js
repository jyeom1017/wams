//var app = angular.module('app', [ 'ui.router', 'angularUtils.directives.dirPagination', 'angularFileUpload', 'app.etc', 'app.login', 'app.sample', 'app.facility', 'app.edu', 'app.book', 'app.claim', 'app.importantFc', 'app.facilityCheck', 'app.gis', 'app.system', 'app.sewerLine', 'app.monitor', 'app.dataExport', 'app.statistics', 'app.sample', 'app.admin', 'app.common', 'app.gate', 'app.asset', 'app.bbs', 'app.item' ]);
var app = angular.module('app', [ 'ui.router', 'angularUtils.directives.dirPagination', 'angularFileUpload',  'app.login', 'app.sample',  'app.gis', 'app.system',  'app.admin', 'app.common', 'app.asset', 'app.bbs', 'app.item','app.diagnosis','app.finance','app.operation','app.statistics','app.inventory','app.mypage','app.main' ]);

angular.module('app.constant', []);
angular.module('app.mypage', []);
angular.module('app.main', []);
angular.module('app.etc', []);
angular.module('app.login', [ 'app.constant' ]);
angular.module('app.sample', []);
angular.module('app.diagnosis', []);
angular.module('app.finance', []);
angular.module('app.operation', []);
angular.module('app.statistics', []);
angular.module('app.inventory', []);

//angular.module('app.facility', []);
//angular.module('app.edu', []);
//angular.module('app.book', []);
//angular.module('app.job', []);
//angular.module('app.claim', []);
//angular.module('app.importantFc', []);
//angular.module('app.facilityCheck', []);
angular.module('app.gis', []);
angular.module('app.system', []);
//angular.module('app.sewerLine', []);
//angular.module('app.monitor', []);
//angular.module('app.dataExport', []);
//angular.module('app.statistics', []);
angular.module('app.admin', []);
angular.module('app.common', []);
//angular.module('app.gate', []);
angular.module('app.bbs', []);
angular.module('app.asset', []);
angular.module('app.item', []);

angular.module('app.common').directive('krInput', [ '$parse', function($parse) {
	return {
		priority : 2,
		restrict : 'A',
		compile : function(element) {
			element.on('compositionstart', function(e) {
				e.stopImmediatePropagation();
			});
		},
	};
} ]);

angular.module('app.common').directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
					text = text.toString();
					var transformedInput = text.replace(/[^0-9.]/g, '');
					var orValue = ngModelCtrl.$$rawModelValue;

					if(orValue)orValue = orValue.toString();
					if (transformedInput !== text) {
						ngModelCtrl.$setViewValue(transformedInput);
						ngModelCtrl.$render();
					}

					let num = Number(transformedInput);
					if (isNaN(num)) {
						ngModelCtrl.$setViewValue(orValue);
						ngModelCtrl.$render();
					}

					return num;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

angular.module('app.common').directive('realNumbersOnly', function () {
	return {
		require: 'ngModel',
		link: function (scope, element, attr, ngModelCtrl) {
			function fromUser(text) {
				if (text) {
					text = text.toString();
					var transformedInput = text.replace(/[^-0-9.]/gi, '');
					if (transformedInput !== text) {
						ngModelCtrl.$setViewValue(transformedInput);
						ngModelCtrl.$render();
					}

					var count = transformedInput.split('-').length - 1;
					if(transformedInput.indexOf("-") > 0 || count > 1) { //중간에 -가 있다면 replace
						ngModelCtrl.$setViewValue(transformedInput.substring(0, transformedInput.length-1));
						ngModelCtrl.$render();
					}

					return transformedInput;
				}
				return undefined;
			}
			ngModelCtrl.$parsers.push(fromUser);
		}
	};
});

app.controller('MainController', MainController);

app.run(function($rootScope, $state, $location, runtimeStates, $document, $injector, mainDataService, $timeout, $interval, ConfigService) {
	// console.log(sessionStorage.getItem('MenuList'));
	/*
	alert(sessionStorage.getItem('loginId'));
	if(sessionStorage.getItem('loginId')=='' || sessionStorage.getItem('loginId')==undefined) {
		location.href='/login';
		return;
	}
	*/
		
	runtimeStates.userDefaultState();
	
	$rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
		
		// console.log('stateChanged :');
		// console.log(toState);
		
		$rootScope.requiredLogin = sessionStorage.getItem('loginId') ? false : true;
		if (toState && toState.requireLogin) {
			if ($rootScope.requiredLogin) {
				$state.go('login');
				e.preventDefault();
			} else if (toState.url == '/') {
				// var url = sessionStorage.getItem('lastMenu') || '010202';
				var url = '010601';
				$timeout(function() {
					$state.go("010601");
				}, 100);
			} else {
				
			}
		} else {
			// 로그인 주소 직접 칠 경우
			if (toState.name.indexOf('login') > -1) {
				$rootScope.requiredLogin = true;
			}
		}
		
		var MainPageScope = angular.element(document.getElementById('MainController')).scope();

		// console.log(MainPageScope);
		
		MainPageScope.menuPath = [];
		if (MainPageScope.MenuList == null || typeof MainPageScope.MenuList == 'undefined' || MainPageScope.MenuList.length == 0) {
			MainPageScope.MenuList = JSON.parse(sessionStorage.getItem('MenuList'));
		}
		$.each(MainPageScope.MenuList, function(index, menuItem) {
			if (menuItem.m_PG_FILE_NAME != '')
				if (toState.url == '/' + menuItem.m_PG_FILE_NAME) {
					MainPageScope.getMenuPath(menuItem);
					MainPageScope.helpPage = menuItem.m_HELP_PAGE;
					MainPageScope.useBookMark = menuItem.USE_BOOKMARK;
					//document.title = '부안 상수시설 생애주기 관리시스템(' + menuItem.m_NAME + ')';
					return false;
				}
			MainPageScope.useBookMark = '';
		});
		// console.log(MainPageScope.MenuList);
		if(MainPageScope.MenuList !=null)
		if (toState.requireLogin && MainPageScope.MenuList.length > 0) {
			//로그인 여부 채크
			/*
			 mainDataService.getHasSession()
				.success(function(data) {
					console.log(data);
					
					if (data !== true) {
						sessionStorage.clear();
						MainPageScope.logout();
					}
				});
			*/
			
			// 권한없는 메뉴에 접근시 차단 -- 가능하려면 사용하는 모든 State URL을 메뉴에 등록 해야함
			var authMenu = false;
			$.each(MainPageScope.MenuList, function(index, menuItem) {
				if (menuItem.m_PG_FILE_NAME != '') {
					if (toState.url == '/' + menuItem.m_PG_FILE_NAME) {
						authMenu = true;
						$timeout(function() {
							$rootScope.setBtnAuth(menuItem.m_MCODE);
						}, 300);
						return false;
					}
				}
			});
			
			if (authMenu == false) {
				// console.log('메뉴 권한이 없습니다.');
				// alertify.alert('메뉴 권한이 없습니다.');
				$.each(MainPageScope.MenuList, function(idx, menuItem) {
					if (menuItem.m_PG_FILE_NAME != '') {
						url = menuItem.m_PG_FILE_NAME;
						// url = sessionStorage.getItem('lastMenu') || '010202';
						// console.log(url);
						e.preventDefault();
						$timeout(function() {
							//$state.go(url);
							$state.go("010601");
						}, 0);
						
						return false;
					}
				});
				
			} else {
				// console.log('localStorage.setItem=========='+toState.url);
				/*if ('010202' != toState.url) {
					// localStorage.setItem('lastUsedMenu', toState.url);
					sessionStorage.setItem('lastMenu', (toState.url.substr(1)));
					var item = {};
					item.E_ID = sessionStorage.getItem('loginId');
					item.LAST_MENU = toState.url;

					//메뉴 사용 기록 저장
					//mainDataService.updateEmpLastMenu(item)
					
					// .success(function(obj) {
					// console.log(obj);
					// });
					
				}*/
				
			}
		}


		// console.log('stateChangeStart_end');
	}); // end $stateChangeStart
	
	$rootScope.setBtnAuth = function(MCODE) {
		if (sessionStorage.getItem(MCODE + 'E') != 'true') {
			$('.btnEdit').addClass('disabled').prop('disabled', true);
			$('.btnEdit').addClass('disable');
		}
		if (sessionStorage.getItem(MCODE + 'V') != 'true') {
			$('.btnView').addClass('disabled').prop('disabled', true);
			$('.btnView').addClass('disable');
		}
	}

});

function MainController($rootScope, $document, $timeout, $state, $q, $location, $scope, $compile, runtimeStates, ConfigService, mainDataService, Message, $filter, LastMenuUseYN) {
	var bodyElement = angular.element($document);

	$scope.setLogout = function(callback){
		mainDataService.setLogout({})
		.success(function(data){
			callback();
		});
	}

	angular.forEach(['click'],
//	angular.forEach(['keydown', 'keyup', 'click', 'mousemove', 'DOMMouseScroll', 'mousewheel', 'mousedown', 'touchstart', 'touchmove', 'scroll', 'focus'], 
	  function(EventName) {
		bodyElement.bind(EventName, function(e) {
			hasSession(e)
		});
	});

		
	$scope.loadItemInfo = function(param,SCOPE,callback){
		//alert(JSON.stringify(param));
        mainDataService.getAssetItemInfo(param)
        .success(function(data){
        	var list = [];
        	//console.log(data);
        	$.each(data,function(idx,Item){
        		Item.REQUIRED = Item.REQUIRED === 'Y';
        		var dtype = Item.DATA_TYPE.split(':');
            		if(dtype.length>=2){ 
            			Item.maxLen = parseInt(dtype[1]);
                		if(dtype[1].indexOf(".") >= 0 ){
                			var floatLen = dtype[1].split(".");
                			Item.maxLen += parseInt(floatLen[1]) + 1;
                			Item.floatLen = parseInt(floatLen[1]); 
                		}
            		}
        		
        			switch(dtype[0]){
        			case 'number':
        			case 'Num' :
        				if(Item.ITEM_VAL == null) Item.ITEM_VAL="";
        				if(dtype[1].indexOf(".") >= 0 ){
        					Item.ITEM_VAL = (new Intl.NumberFormat('ko-KR', { minimumFractionDigits: Item.floatLen }).format(parseFloat(Item.ITEM_VAL.replace(/,/gi,''))));
        					if(Item.ITEM_VAL=='NaN') Item.ITEM_VAL="";
        					//alert('test1');
        				}else{
        					//
        					Item.ITEM_VAL = parseInt(Item.ITEM_VAL.replace(/,/gi,''));
        					if(Item.ITEM_VAL=='NaN') Item.ITEM_VAL="";
        				}
        				break;
        			case 'select':
        				Item.REF_CODE_DATA = Item.REF_CODE_DATA.split('/'); 
        				break;
        			case 'select2':
        				// console.log(Item.REF_CODE_DATA);
        				// console.log($scope.ComCodeList);
        				if(Item.REF_CODE_DATA.indexOf("::")>0){
        					var REF_CD = Item.REF_CODE_DATA.split("::")[0];
        					var filterObj = {C_MEMO : Item.REF_CODE_DATA.split("::")[1] };
        					Item.REF_CODE_DATA = $filter('filter')($scope.ComCodeList[REF_CD],filterObj);
        				}else{
        					Item.REF_CODE_DATA = $scope.ComCodeList[Item.REF_CODE_DATA];	
        				}
        				break;        				
        			case 'check':
        				var checkedVal = (Item.ITEM_VAL===''||Item.ITEM_VAL==null)?[]:Item.ITEM_VAL.split(',');
        				Item.CheckedItem = [];
        				Item.REF_CODE_DATA = Item.REF_CODE_DATA.split('/');
        				$.each(Item.REF_CODE_DATA,function(idx2,checked){
        					Item.CheckedItem.push(($.inArray(checked, checkedVal) >= 0));
        				});
        				break;
        			case 'check2':
        				Item.CheckedItem = [false];
        				Item.CheckedItem[0] = !(Item.ITEM_VAL === '' || Item.ITEM_VAL == null);
        				break;
        			case 'readonly':
        				if(Item.ITEM_CD=='10003'){
        					Item.ITEM_VAL=SCOPE.fn_name;
        				}
        				break;
        			case 'string' :
        				if(Item.ITEM_VAL == null) Item.ITEM_VAL="";
        				break;
        			case 'date' :
        				if(Item.ITEM_VAL == null || Item.ITEM_VAL==='0000-00-00' || Item.ITEM_VAL==='00000000') Item.ITEM_VAL="";
        				break;
        			default : 
        				break;
        			}
        		//console.log(Item);
        		list.push(Item);
        	});
        	SCOPE.ItemList = list;
        	if(typeof callback =='function'){
        		callback(SCOPE);
        	} 
        		
        });
	}
	
	
	
	
	function hasSession(e){
	 mainDataService.getHasSession()
		.success(function(data) {
//			console.log(data);
			
			if (data !== true) {
				alertify.alert('Info','로그아웃 되었습니다.\n다시 로그인해주세요.',function(){
					$scope.logout();
					sessionStorage.clear();
					top.location.href="/login";
				});
			}
		});
	}

	$scope.firstSelect = Message;
	
	$scope.Sample = [ {
		url : 'jqgrid'
	}, {
		url : 'test'
	}, {
		url : 'popup'
	}, {
		url : 'chart'
	}, {
		url : 'input'
	}, {
		url : 'map'
	}, {
		url : 'CRUD'
	}, {
		url : 'cctv'
	}, {
		url : 'fileupload'
	} ];
	
	$scope.menuIcon = {
		'1' : 'sysInfo',
		'2' : 'Gis',
		'3' : 'facility',
		'4' : 'focus',
		'5' : 'research',
		'6' : 'repair',
		'7' : 'complaints',
		'8' : 'diagnosis',
		'9' : 'educations',
		'10' : 'books',
		'11' : 'monitor',
		'12' : 'statistics',
		'13' : 'integration'
	};
	
	$scope.menuAdminSelected = false;
	$scope.menuPartSelected = '01';
	$scope.menuPath = [];
	$scope.MenuList = [];
	$scope.isLogin = false;
	
	$scope.getMenuPath = function(item) {
		$scope.menuPath.unshift(item.m_NAME);
		// $scope.menuPath.push(item.m_NAME);
		//document.title = '울진 하수관로 운영관리 시스템(' + item.m_NAME + ')';
		
		$.each($scope.MenuList, function(index, menuItem) {
			
			if (item.m_PARENT_MCODE == '')
				return -1;
			
			if (item.m_PARENT_MCODE == menuItem.a_MCODE) {
				return $scope.getMenuPath(menuItem) + '&gt;' + item.m_NAME + '';
			} else {
				return '';
			}
			
		});

	}

	$scope.logout = function() {
		$scope.isLogin = false;
		$scope.loginNm = null;
		// sessionStorage.setItem('loginId','');
		// sessionStorage.setItem('loginNm','');
		// sessionStorage.setItem('MenuList','[]');
		sessionStorage.clear();
		// $state.go('login');
		$scope.setLogout(function(){
			location.href = './logout';	
		});
	}

	$rootScope.$on('loginSuccess', function(event, data) {
		console.log(data);
		console.log($scope.MenuList);
		
		$scope.isLogin = data.res;
		$scope.loginNm = data.loginNm;
		$scope.loginId = data.loginId; // 0818 수정
		$scope.isAdmin = ('G001' == sessionStorage.getItem('groupCd')) ? true : false;

		if ($scope.isLogin)
			$scope.getMenuList(function() {
				// var url = localStorage.getItem('lastUsedMenu') || '010202';
				// var url = sessionStorage.getItem('lastMenu') || '010202';

				//var url = sessionStorage.getItem('lastMenu') || '010202';
				var url = "010601";
				
				url = url.replace('\/', '');
				// alert(url);
				var findUrl = false;
				$.each($scope.MenuList, function(idx, menuItem) {
					if (menuItem.m_PG_FILE_NAME != '') {
						// alert(menuItem.m_PG_FILE_NAME);
						if (url == menuItem.m_PG_FILE_NAME) {
							findUrl = true;
							return false;
						}
					}
				});
				
				if (findUrl == false) {
					url = $scope.MenuList[0].m_PG_FILE_NAME;
				}
				
				$timeout(function() {
					$state.go(url);
				}, 100);
				
				sessionStorage.setItem('MenuList', JSON.stringify($scope.MenuList));
				$.each($scope.MenuList, function(idx, menuItem) {
					sessionStorage.setItem(menuItem.m_MCODE + 'V', (menuItem.a_VIEW == 'Y' ? true : false));
					sessionStorage.setItem(menuItem.m_MCODE + 'E', (menuItem.a_MANG == 'Y' ? true : false));
				});
				
			});
		
		
			$scope.reloadComCodeYearList();
		 	$scope.reloadComCodeYearList2();
		 	$scope.ComCodeList = {};
		 	$scope.reloadComcodeList();
		
	});

	$scope.reloadComCodeYearList = function(){
	 	mainDataService.getCommonCodeList({
	        gcode : 'YEAR',
	        gname : ''
	    }).success(function(data) {
	        if (!common.isEmpty(data.errMessage)) {
	            alertify.error('errMessage : ' + data.errMessage);
	        } else {
	        	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
	            $scope.ComCodeYearList =  $filter('filter')(data,{C_USEGBN:'Y'});
	        }
	    });
	}
	
 	$scope.reloadComCodeYearList2 = function(){
 	 	mainDataService.getCommonCodeList({
 	        gcode : 'YR2',
 	        gname : ''
 	    }).success(function(data) {
 	        if (!common.isEmpty(data.errMessage)) {
 	            alertify.error('errMessage : ' + data.errMessage);
 	        } else {
 	        	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
 	            $scope.ComCodeYearList2 = $filter('filter')(data,{C_USEGBN:'Y'});
 	        }
 	    }); 		
 	}
 	
 	$scope.reloadComcodeList = function(){
	 	//관종
	 	mainDataService.getCommonCodeList({
	        gcode : 'MOP',
	        gname : ''
	    }).success(function(data) {
	        if (!common.isEmpty(data.errMessage)) {
	            alertify.error('errMessage : ' + data.errMessage);
	        } else {
	        	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
	            $scope.ComCodeList['MOP'] = data;
	        }
	    });	
	 	//토양종류
	 	mainDataService.getCommonCodeList({
	        gcode : 'TOS',
	        gname : ''
	    }).success(function(data) {
	        if (!common.isEmpty(data.errMessage)) {
	            alertify.error('errMessage : ' + data.errMessage);
	        } else {
	        	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
	            $scope.ComCodeList['TOS'] = data;
	        }
	    });
	 	//도로종류
	 	mainDataService.getCommonCodeList({
	        gcode : 'RDF',
	        gname : ''
	    }).success(function(data) {
	        if (!common.isEmpty(data.errMessage)) {
	            alertify.error('errMessage : ' + data.errMessage);
	        } else {
	        	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
	            $scope.ComCodeList['RDF'] = data;
	        }
	    });
	 	//관경
	 	mainDataService.getCommonCodeList({
	        gcode : 'CN21',
	        gname : ''
	    }).success(function(data) {
	        if (!common.isEmpty(data.errMessage)) {
	            alertify.error('errMessage : ' + data.errMessage);
	        } else {
	        	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
	            $scope.ComCodeList['CN21'] = data;
	        }
	    }); 	
	 	//관로계통
	 	mainDataService.getCommonCodeList({
	        gcode : 'SAA',
	        gname : ''
	    }).success(function(data) {
	        if (!common.isEmpty(data.errMessage)) {
	            alertify.error('errMessage : ' + data.errMessage);
	        } else {
	        	data.sort(function(a,b){return (a.C_SCODE<b.C_SCODE)?1:-1;});
	            $scope.ComCodeList['SAA'] = data;
	        }
	    });	
		mainDataService.getBlockCodeList({
		}).success(function (obj) {
			if (!common.isEmpty(obj.errorMessage)) {
				alertify.error('errMessage : ' + obj.errorMessage);
			} else {
				$scope.ComCodeList['BLOC'] = [];
				$.each(obj,function(idx,Item){
					$scope.ComCodeList['BLOC'].push({C_SCODE:Item.BL_BCODE,C_NAME:Item.BL_NAME,C_GCODE:Item.BL_PBCODE,C_MEMO:((Item.M=="1")?"M":Item.LEV)})	 
				})
				
				//console.log($scope.blockList);
			}
		});
	 	}
 	
 	//sessionStorage.getItem('loginId')
 	if(sessionStorage.getItem('loginId')!='' && sessionStorage.getItem('loginId') ){
		$scope.reloadComCodeYearList();
	 	$scope.reloadComCodeYearList2();
	 	$scope.ComCodeList = {};
	 	$scope.reloadComcodeList();
 	}
 	
	$scope.goMenu = function(menuItem, event) {
		// console.log(menuItem);
		// event.preventDefault();
		 //console.log(event);
		
		if (menuItem.m_PG_FILE_NAME != '') {
			console.log($state);
			console.log($state.current.name);
			console.log(menuItem.m_PG_FILE_NAME);
			if($state.current.name == menuItem.m_PG_FILE_NAME){
				//location.href="/#/" + $state.current.url;
				location.reload();
				return;
			}
			$state.go(menuItem.m_PG_FILE_NAME, {});
		}
		event.stopPropagation();
		
	}

	$scope.goURL = function(url, tabId) {
		$state.go(url, {
			tabid : tabId
		});
	}

	$scope.goURL_info = function(url, tabId, Info) {
		$state.go(url, {
			tabid : tabId,
			info : Info
		});
	}

	$scope.goURL_sew = function(url, tabId, Info) {
		var param1 = {
			'url' : url,
			'tabId' : tabId,
			'Info' : Info
		};
		var url1 = '#/gate/' + encodeURI(JSON.stringify(param1));
		
		window.open(url1, '_blank');
	}

	// 팝업창 여부를 확인하기 위한 변수
	var popFlag;
	$scope.goURL_pop = function(page) {
		// 팝업 사이즈
		var nWidth = 1600;
		var nHeight = 900;
		
		var winWidth = document.body.clientWidth;
		var winHeight = document.body.clientHeight;
		var winX = window.screenX || window.screenLeft || 0;
		var winY = window.screenY || window.screenTop || 0;
		var nLeft = winX + (winWidth - nWidth) / 2;
		var nTop = winY + (winHeight - nHeight) / 2;
		
		var strOption = '';
		strOption += 'left=' + nLeft + 'px,';
		strOption += 'top=' + nTop + 'px,';
		strOption += 'width=' + nWidth + 'px,';
		strOption += 'height=' + nHeight + 'px,';
		strOption += 'toolbar=no,menubar=no,location=no,';
		strOption += 'resizable=yes,status=yes';
		
		var url = '/assets/lib/pdfjs-2.0.943-dist/web/viewer.html?file=/resources/upload/file/help/help.pdf#page=' + page;
		// 최초 클릭이면 팝업을 띄운다.
		if (!popFlag) {
			popUp = window.open(url, '', strOption);
			popFlag = true;
		} else {
			// 팝업창 존재 여부를 확인하여 팝업창이 이미 떠 있으면 주소 이동
			if (!popUp.closed && popUp) {
				popUp.location.href = url;
				// 없으면 팝업을 다시 띄울 수 있게 한다.
			} else {
				popUp = window.open(url, '', strOption);
				popFlag = true;
			}
		}
		
	}

	$scope.menuAdmin = function() {
		$scope.menuAdminSelected = true;
	}

	$scope.menuUser = function(menuPart, event) {
		$scope.menuAdminSelected = false;
		$scope.menuPartSelected = menuPart;
		// console.log('test');
		// console.log(event);
		
		var navCaption = $('.nav_title li');
		
		$(navCaption).click(function() {
			$(navCaption).removeClass('selected');
			$(this).addClass('selected');
		});
		
	}

	$scope.getMenuList = function(callback) {
		$scope.afterMenuLoad = ($scope.afterMenuLoad == null) ? true : false;
		$scope.menuPath = [];
		$scope.MenuList = [];
		
		// console.log(sessionStorage.getItem('loginId'));

		if (sessionStorage.getItem('loginId') == '' || sessionStorage.getItem('loginId') == null)
			return;
		
		$scope.isLogin = true;
		$scope.loginNm = sessionStorage.getItem('loginNm');
		$scope.loginId = sessionStorage.getItem('loginId');

		
		
		mainDataService.getMenuList().success(function(data) {
			// console.log(data);
			if (data.errName != null || data.length == 0) {
				$scope.MenuList = [];
				$scope.logout();
				return;
			}
			
			for (var i = 0; i < data.length; i++) {
				data[i].MenuList = [];
				for (var j = 0; j < data.length; j++) {
					if (data[i].a_MCODE == data[j].m_PARENT_MCODE)
						data[i].MenuList.push(data[j]);
				}
			}
			
			$scope.MenuList = data;
			// console.log($scope.MenuList);

			if (typeof callback == 'function') {
				callback();
			}
			$scope.afterMenuLoad = true;
			$timeout(function() {
				
				/*
				 * //메뉴 클릭시 $('.gnb-1>li').click(function(e){ //not(this) == 클릭된 li가 아닌 것 //선택하지 않은 ul(현재 열려있는 ul)은 slideUp으로 메뉴 올려줌 // $('.gnb-1>li').not(this).find('ul').stop(true,true).slideUp(200); //this = .gnb>li $(this).find('ul').stop(true,true).slideToggle(200); $(this).toggleClass('on'); }); $('.gnb-2 .gnb-sub > li').click(function(e){ $(this).find('ul').stop(true,true).slideToggle(200); $(this).toggleClass('on'); });
				 */
				// left 3depth menu
				var lnbUI = {
					click : function(target, speed) {
						var _self = this, $target = $(target);
						_self.speed = speed || 10;
						
						$target.each(function() {
							if (findChildren($(this))) {
								return;
							}
							$(this).addClass('noDepth');
						});
						
						function findChildren(obj) {
							return obj.find('> ul').length > 0;
						}
						
						$target.on('click', 'button', function(e) {
							// e.stopPropagation();
							var $this = $(this), $depthTarget = $this.next(), $siblings = $this.parent().siblings();
							
							$this.parent('li').find('ul li').removeClass('on');
							$siblings.removeClass('on');
							$siblings.find('ul').slideUp(100);
							if (!$this.parent().hasClass('on')) {
								_self.activeOn($this);
								$depthTarget.slideDown(_self.speed);
							} else {
								$depthTarget.slideUp(_self.speed);
								_self.activeOff($this);
							}
						})
					},
					activeOff : function($target) {
						$target.parent().removeClass('on');
					},
					activeOn : function($target) {
						$target.parent().addClass('on');
					}
				}; // Call lnbUI
				$(function() {
					lnbUI.click('#nlnb li', 1);
				});
				
				/*
				 * $.each($scope.MenuList,function(index,menuItem){ if(menuItem.m_PG_FILE_NAME!='') if( $state.current.url == '/'+menuItem.m_PG_FILE_NAME ){ return $scope.getMenuPath(menuItem); } });
				 */
				var menuid = '#menu' + $state.current.name;
				if (menuid != '#menu' && menuid != '#menu^')
					$(menuid).addClass('on').parent().show().parent().addClass('on')
			}, 300);
		});
	}
	// console.log($scope.isLogin);
	if ($scope.MenuList.length == 0 )
		$scope.getMenuList();
	
	$scope.isAdmin = ('G001' == sessionStorage.getItem('groupCd')) ? true : false;
	
	// $scope.storeEmployeeList = [];
	
	$scope.showPicture = {};
	
	/*
	 * $scope.showPicture = function(pic){ $rootScope.$broadcast('popupShowPicture', {URL:pic.F_PATH,DESC:pic.F_TITLE}); }
	 */

	$rootScope.$on('popupShowPicture', function(event, data) {
		$('#dialog_commonImagPopup').dialog({
			title : '사진보기',
			autoOpen : true,
			modal : true,
			height : 700,
			width : 900,
			resizable : false
		});
		$scope.showPicture = data;
	});
	
	/*
	 * $rootScope.$on('popupEmployeeList', function (event, data) { $( '#dialog_commonUserList' ).dialog({title: '담당자 선택',autoOpen: true, height: 450, width: 500, modal: true}); console.log(event); $scope.storeEmployeeList = data.rows; $scope.callbackId = data.callbackId;//'selectUserID_1' }); $scope.selectUser = function(){ var ID = $('input[name='selectUserID']:checked').val(); var name = ''; if(!ID) { alertify.alert('담당자를 선택하십시오'); return; } $.each($scope.storeEmployeeList,function(idx,Item){ if(Item['E_ID']==ID){ name = Item['E_NAME']; return false; } }); $( '#dialog_commonUserList' ).dialog('close'); $rootScope.$broadcast($scope.callbackId, {E_ID:ID, E_NAME:name}); } //직급코드 로딩 var posCodeList =[]; mainDataService.getCommonCodeList({gcode:'501',gname:''}) .success(function(data){ posCodeList = data; }); $scope.UserPOS = function (val){ var ret=val; $.each(posCodeList, function(i, item){ if(item.C_SCODE == val){ ret=item.C_NAME; return false; } }); return ret; } $scope.selectUser1 =
	 * function(index){ $('#radio' + index).prop('checked', true); } $scope.canclePop = function(){ $( '#dialog_commonUserList' ).dialog('close'); }
	 */

	/*
	 * $scope.savePicture = function(){ //사진정보 저장 후 파일 넘버 콜백 var fileDesc = $scope.picture_desc; var path = ($rootScope.uploadPath || 'temp'); if($scope.file != null) mainDataService.fileUpload(fileDesc, path, $scope.file) .success(function(obj){ $scope.file = null; $scope.picture_desc = ''; $scope.picLoaded = false; $scope.previewImg0 = null; $rootScope.callback_savePicture(obj); $( '#dialog_commonImageFileDrag2' ).dialog('close'); }); else{ $scope.file = null; } } $scope.closePicture = function(){ $( '#dialog_commonImageFileDrag2' ).dialog('close'); $scope.file = null; $scope.picLoaded = false; $scope.previewImg0 = null; $scope.file = null; $scope.picture_desc = ''; } function readURL(file) { var reader = new FileReader(); reader.onload = function (e) { $scope.previewImg0 = e.target.result; $scope.picLoaded=true; $scope.$apply(); }; reader.readAsDataURL(file); } $scope.onFileSelect1 = function ($files, cmd) { console.log($files); if ($files.length === 0) { return; } var str = '\.(' +
	 * cmd + ')$'; var FileFilter = new RegExp(str) ; var ext = cmd.split('|'); if($files[0].name.toLowerCase().match(FileFilter)){ readURL($files[0]); }else{ alertify.alert('확장자가 ' + ext + ' 인 파일만 선택가능합니다.'); $scope.file = null; } }
	 */

	/*
	 * function loadHisPictureList(item){ if($scope.Info.FTR_CDE == null || $scope.Info.FTR_IDN == null){ $scope.storeHisPictureList=[]; return; } var param = 'FTR_CDE='+$scope.Info.FTR_CDE+'&FTR_IDN='+$scope.Info.FTR_IDN+'&FH_SEQ='+item.FH_SEQ; console.log(param); mainDataService.getHisPictureList(param) .success(function(data){ $scope.storeHisPictureList = data; while($scope.storeHisPictureList.length<2){ $scope.storeHisPictureList.push({F_NUM:0,F_PATH:'',F_TITLE:''}); } }); }
	 */
	// 사용자메뉴추가
	$scope.UserMenuAdd = function() {
		
		$.each($scope.MenuList, function(index, menuItem) {
			if (menuItem.m_PG_FILE_NAME != '')
				if ($state.current.url == '/' + menuItem.m_PG_FILE_NAME) {
					var m_part = menuItem.m_PART;
					var m_code = menuItem.a_MCODE;
					mainDataService.setUserMenu('&M_PART=' + m_part + '&A_MCODE=' + m_code + '&USE_BOOKMARK=N').success(function(data) {
						// console.log(data);
					});
					menuItem.USE_BOOKMARK = 'Y';
					$scope.useBookMark = 'Y';
					return false;
				}
		});
		
	}
	// 사용자메뉴삭제
	$scope.UserMenuDel = function() {
		$.each($scope.MenuList, function(index, menuItem) {
			if (menuItem.m_PG_FILE_NAME != '')
				if ($state.current.url == '/' + menuItem.m_PG_FILE_NAME) {
					var m_part = menuItem.m_PART;
					var m_code = menuItem.a_MCODE;
					mainDataService.setUserMenu('&M_PART=' + m_part + '&A_MCODE=' + m_code + '&USE_BOOKMARK=Y').success(function(data) {
						// console.log(data);
					});
					menuItem.USE_BOOKMARK = 'N';
					$scope.useBookMark = 'N';
					return false;
				}
		});
	}
	$scope.GetMenuName = function(MCODE) {
		var rtnVal = MCODE;
		$.each($scope.MenuList, function(idx, Item) {
			if (Item.m_MCODE == MCODE) {
				rtnVal = Item.m_NAME;
				return false;
			}
		});
		return rtnVal;
	}
	$scope.useBookMark = '';
	$scope.helpPage = '';
	
	// 파일 다운로드
	$scope.getCmFileDown = function(file) {
		
		var MCODE = null;
		var f_num = null;
		
		$.each($scope.MenuList, function(index, menuItem) {
			if (menuItem.m_PG_FILE_NAME != '') {
				if ($state.current.url == '/' + menuItem.m_PG_FILE_NAME) {
					console.log($state.current.url);
					MCODE = menuItem.m_MCODE;
					console.log(MCODE);

					return false;
				}
			}
		});

		console.log(sessionStorage);
		
		// if (sessionStorage.getItem(MCODE + 'E') != 'true' || sessionStorage.getItem(MCODE + 'V') != 'true') {
		if (sessionStorage.getItem(MCODE + 'V') != 'true') {
			alertify.alert('권한이 없습니다.');
			return;
		}
		
		if (typeof file.F_NUM != 'undefined' && file.F_NUM != null) {
			f_num = file.F_NUM;
		} else if (typeof file.JF_FNUM != 'undefined' && file.JF_FNUM != null) {
			f_num = file.JF_FNUM;
		} else {
			alertify.error('no direct');
			return;
		}
		$('#fileDownForm').find('#f_num').val(f_num);
		$('#fileDownForm').submit();
	}

	// 처리구역/처리분구 UPDATE
	$scope.treatmentUpdate = function() {
		treatmentUpdate();
	}
	// 관망도 캡쳐 및 스케치
	$scope.Sketch = function() {
		Sketch();
	}
	// 공간데이터
	$scope.GisData = function() {
		GisData();
	}
	
	$scope.myPasswordChangePopupShow = function(){
		$scope.$broadcast('popupMyPasswordChange', {});
	}
	
	// include 될 떄 마다 동작..
	// $rootScope.$on('$includeContentLoaded', function(MCODE) {
	// if (sessionStorage.getItem(MCODE + 'E') != 'true') {
	// $('.btnEdit').addClass('disabled').prop('disabled', true);
	// $('.btnEdit').addClass('disable');
	// }
	// if (sessionStorage.getItem(MCODE + 'V') != 'true') {
	// $('.btnView').addClass('disabled').prop('disabled', true);
	// $('.btnView').addClass('disable');
	// }
	// });
	
	$scope.getToday =function(){
        var today = new Date(); // 오늘
        var Year = today.getFullYear();
        return Year +  ('0' + (today.getMonth() + 1)).substr(-2) + ('0' + today.getDate()).substr(-2);
	}
}

angular.module('app.common').controller('picturePopupController', picturePopupController);

function picturePopupController($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService) {
	

	$scope.savePicture = function() {
		
		var Scope = angular.element(document.getElementById('dialog_commonImageFileDrag2')).scope();
		Scope.category = '';		
		
		$.each($scope.MenuList, function(index, menuItem) {
			if (menuItem.m_PG_FILE_NAME != '') {
				if ($state.current.url == '/' + menuItem.m_PG_FILE_NAME) {
					MCODE = menuItem.m_MCODE;
					return false;
				}
			}
		});
		
		if (sessionStorage.getItem(MCODE + 'E') != 'true' || sessionStorage.getItem(MCODE + 'V') != 'true') {
			alertify.alert('권한이 없습니다.');
			return;
		}
		
		// 사진정보 저장 후 파일 넘버 콜백
		var validationItem = [ {
			'name' : 'picture_desc',
			'title' : '설명',
			'name2' : 'fileDesc',
			'required' : true
		} ];
		
		if (!ItemValidation({
			picture_desc : Scope.picture_desc
		}, validationItem))
			return;
		
		// 사진정보 저장 후 파일 넘버 콜백
		var fileDesc = ((Scope.category != undefined) ? Scope.category : '') + Scope.picture_desc;
		var path = ($rootScope.uploadPath || 'temp');
		var dirType = 'img';
		var bookOpt = $rootScope.bookOpt;
		if (Scope.file != null)
			mainDataService.fileUpload(fileDesc, path, Scope.file, dirType, bookOpt)
			.success(function(obj) {
				if (!common.isEmpty(obj.errMessage)) {
					alertify.error('errMessage : ' + obj.errMessage);
				} else {
					Scope.file = null;
					Scope.picture_desc = '';
					Scope.picLoaded = false;
					Scope.previewImg0 = null;
					
					
					$rootScope.callback_savePicture(obj);
					delete $rootScope.uploadPath;
					delete $rootScope.bookOpt;
					$('#dialog_commonImageFileDrag2').dialog('close');
				}
			});
		else {
			if($scope.showPicture.pic==undefined || $scope.showPicture.pic=={}){
				alertify.alert('Error','사진 파일을 등록해주세요.',function(){});
				return;
			}
			//설명만 수정할때
			if($scope.showPicture.pic.F_NUM > 0){
				//alert('설명만 수정할때');
				
				$scope.showPicture.DESC = $scope.picture_desc;
				$scope.showPicture.pic.DESCRIPTION = $scope.picture_desc;
				$scope.showPicture.pic.f_num = $scope.showPicture.pic.F_NUM; 
				$scope.showPicture.pic.F_TITLE = $scope.picture_desc;
				$rootScope.callback_savePicture($scope.showPicture.pic);
				$('#dialog_commonImageFileDrag2').dialog('close');
    			$scope.file = null;
    			$scope.picLoaded = false;
    			$scope.previewImg0 = null;
    			$scope.picture_desc = '';				
			}else{
			Scope.file = null;
			alertify.error('파일을 선택해주세요')
			}
		}
	}

	$scope.closePicture = function() {
		$('#dialog_commonImageFileDrag2').dialog('close');
		$scope.file = null;
		$scope.picLoaded = false;
		$scope.previewImg0 = null;
		$scope.file = null;
		$scope.picture_desc = '';
		
	}

	function readURL(file) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$scope.previewImg0 = e.target.result;
			$scope.picLoaded = true;
			$scope.$apply();
		};
		reader.readAsDataURL(file);
	}
	
	$scope.onFileSelect1 = function($files, cmd) {
		var Scope = angular.element(document.getElementById('dialog_commonImageFileDrag2')).scope();
		// console.log($files);
		
		if ($files.length === 0) {
			return;
		}
		var str = '\.(' + cmd + ')$';
		var FileFilter = new RegExp(str);
		var ext = cmd.split('|');
		if ($files[0].name.toLowerCase().match(FileFilter)) {
			readURL($files[0]);
		} else {
			alertify.alert('Error','확장자가 ' + ext + ' 인 파일만 선택가능합니다.',function(){});
			Scope.file = null;
			Scope.showPicture.pic.F_TITLE = '';			
		}
	}

	$rootScope.getTitle = function(id) {
		return $('#' + id + ' option:selected').text();
	}

	$rootScope.deletePic = function(){
		//alert('deletePic')
		console.log($scope.showPicture);
		/*if(confirm('사진을 삭제하시겠습니까?')){
			$('#dialog_commonImagPopup').dialog('close');
			alert('삭제되었습니다.');
			$scope.showPicture.callbackDeletePic($scope.showPicture);
		}*/
		
		alertify.confirm('사진삭제','사진을 삭제하시겠습니까?', function(){ 
			//alertify.success('삭제되었습니다.');	
			$('#dialog_commonImagPopup').dialog('close');
			$scope.showPicture.callbackDeletePic($scope.showPicture); 
		},function(){}).set('basic', false);
	}
	
	$rootScope.cancelUpdatePic= function(){
		var Scope =	 angular.element(document.getElementById('dialog_commonImageFileDrag2')).scope();
		Scope.closePicture();
	}
	$rootScope.updatePic = function(){
		if(typeof $scope.showPicture.callbackUpdatePic != 'function'){
			alert('오류:callback Function이 정의되지 않음');
			return;
		}
		console.log($scope.showPicture);
		var picInfo = $scope.showPicture.pic;
		$scope.picLoaded = true;
		$scope.previewImg0 = $scope.showPicture.URL;
		$scope.picture_desc = $scope.showPicture.DESC;
		
		if($scope.showPicture.updateDlgId=='#dialog_commonImageFileDrag2')
			$($scope.showPicture.updateDlgId).dialog();
		else{
			$('#dialog_commonImagPopup').dialog('close');
			$($scope.showPicture.updateDlgId).show();
		}
		
		$rootScope.callback_savePicture = $scope.showPicture.callbackUpdatePic; 
	}


	/*$rootScope.updatePic2 = function(){
		//alert('updatePic')
		console.log($scope.showPicture);
		var picInfo = $scope.showPicture.pic;
		$scope.picLoaded = true;
		$scope.previewImg0 = $scope.showPicture.URL;
		$scope.picture_desc = $scope.showPicture.DESC;
		$rootScope.callback_savePicture = function(obj){
			console.log(picInfo);
			console.log(obj);
			var param = {AssetSid: picInfo.ASSET_SID,F_NUM:obj.f_num,PHOTO_NUM : picInfo.SEQ, DESCRIPTION : obj.F_TITLE };
			mainDataService.updateAssetPic(param)
				.success(function(data){
					console.log(data);
				});
		}
		$('#dialog_commonImageFileDrag2').dialog();
	}*/

	$rootScope.$on('popupShowPicture2', function(event, data) {
		console.log('show pic 2');
		console.log(data);

		$("#dialog-image-popup2").show();

		$scope.showPicture = data;
	});
	
	$rootScope.displayDateFormat = function(str){
		if(str=='' || str==null) return '';
		var rstr = (str.replace(/-/gi,'') + '0101').substring(0,8);
		return rstr.substring(0,4) + '-' + rstr.substring(4,6) + '-' + rstr.substring(6,8);
	}
}



/*
$rootScope.updatePic = function(){
	//alert('updatePic')
	console.log($scope.showPicture);
	var picInfo = $scope.showPicture.pic;
	$scope.picLoaded = true;
	$scope.previewImg0 = $scope.showPicture.URL;
	$scope.picture_desc = $scope.showPicture.DESC;
	$rootScope.callback_savePicture = function(obj){
		console.log(picInfo);
		console.log(obj);
		var param = {AssetSid: picInfo.ASSET_SID,F_NUM:obj.f_num,PHOTO_NUM : picInfo.SEQ, DESCRIPTION : obj.F_TITLE }; 
		mainDataService.updateAssetPic(param)
		.success(function(data){
			console.log(data);
		});
	}
	$('#dialog_commonImageFileDrag2').dialog();		
}
 */

angular.module('app.common').controller('picturePopupController2', picturePopupController2);

function picturePopupController2($scope, $state, $stateParams, mainDataService, $rootScope, $compile, $timeout, $interval, ConfigService) {

	$scope.image_popupInfo = [];

	$scope.$on("showImageUploadPopup", function (e, data) {
		// console.log(e);
		console.log(data);

		// $('#dialog-imageUploadPopup').show();
	});

/*	$scope.uploadPicPopup = function (pic) {
		console.log(pic);


		$scope.image_popupInfo = pic.pic;
		$scope.image_popupInfo.F_TITLE = pic.pic.F_TITLE;
		$scope.image_popupInfo.F_DESC = pic.pic.F_DESC;
		console.log($scope.image_popupInfo);
		$('#dialog-image-popup2').hide();
		$('#dialog-imageUpload-popup2').show();


	};*/

	$scope.savePicture = function() {

		console.log($scope.image_popupInfo);
		console.log($scope.file);

		$.each($scope.MenuList, function(index, menuItem) {
			if (menuItem.m_PG_FILE_NAME != '') {
				if ($state.current.url == '/' + menuItem.m_PG_FILE_NAME) {
					MCODE = menuItem.m_MCODE;
					return false;
				}
			}
		});

		if (sessionStorage.getItem(MCODE + 'E') != 'true' || sessionStorage.getItem(MCODE + 'V') != 'true') {
			alertify.alert('권한이 없습니다.');
			return;
		}

		// 사진정보 저장 후 파일 넘버 콜백
		var validationItem = [ {
			'name' : 'picture_desc',
			'title' : '설명',
			'name2' : 'fileDesc',
			'required' : true
		} ];

		if (!ItemValidation({
			picture_desc : $scope.image_popupInfo.F_DESC
		}, validationItem))
			return;

		var Scope = angular.element(document.getElementById('dialog_commonImageFileDrag2')).scope();

		//if ($scope.file[0] != null)
		if ($scope.file != null){
			// 사진정보 저장 후 파일 넘버 콜백
			var fileDesc = $scope.file[0].name;
			var path = 'new_operation';
			var dirType = fileDesc.split('.')[1];
			var bookOpt = '';			
			mainDataService.fileUpload(fileDesc, path, $scope.file, dirType, bookOpt)
			.success(function(obj) {
				if (!common.isEmpty(obj.errMessage)) {
					alertify.error('errMessage : ' + obj.errMessage);
				} else {
					$scope.file = null;
					// $scope.image_popupInfo = [];
					// Scope.picture_desc = '';
					// Scope.picLoaded = false;
					// Scope.previewImg0 = null;

					obj.F_DESC = $scope.image_popupInfo.F_DESC;
					obj.pic = $scope.showPicture.pic;
					console.log(obj);
					
					$rootScope.callback_savePicture(obj);
					delete $rootScope.uploadPath;
					delete $rootScope.bookOpt;
					// $('#dialog_commonImageFileDrag2').dialog('close');
					$('#dialog-imageUpload-popup2').hide();
					$scope.image_popupInfo = [];
				}
			});
		}
		else {
			if($scope.showPicture.pic==undefined || $scope.showPicture.pic=={}){
				alertify.alert('Error','사진 파일을 등록해주세요.',function(){});
				return;
			}			
			//설명만 수정할때
			if($scope.showPicture.pic.F_NUM > 0){
				//alert('설명만 수정할때');
				$scope.showPicture.DESC = $scope.picture_desc;
				$scope.showPicture.pic.DESCRIPTION = $scope.picture_desc;
				$scope.showPicture.pic.f_num = $scope.showPicture.pic.F_NUM;
				$scope.showPicture.pic.F_TITLE = $scope.picture_desc;
				$scope.showPicture.pic.F_DESC = $scope.image_popupInfo.F_DESC;
				$rootScope.callback_savePicture($scope.showPicture.pic);
				//$('#dialog_commonImageFileDrag2').dialog('close');
				$scope.file = null;
				$scope.picLoaded = false;
				$scope.previewImg0 = null;
				$scope.file = null;
				$scope.picture_desc = '';
				$scope.image_popupInfo = [];
			}else{
				Scope.file = null;
				alertify.error('파일을 선택해주세요')
			}
		}
	}

	$scope.closePicture = function() {
		console.log('close');
		$('#dialog-imageUpload-popup2').hide();


		$scope.file = null;
		$scope.picLoaded = false;
		$scope.previewImg0 = null;
		$scope.file = null;
		$scope.picture_desc = '';
		$scope.image_popupInfo = [];

	}

/*	$scope.closePicture2 = function() {
		console.log('close');
		$('#dialog-image-popup2').hide();


		$scope.file = null;
		$scope.picLoaded = false;
		$scope.previewImg0 = null;
		$scope.file = null;
		$scope.picture_desc = '';

	}*/

	function readURL(file) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$scope.previewImg0 = e.target.result;
			$scope.picLoaded = true;
			$scope.$apply();
		};
		reader.readAsDataURL(file);
	}

	$scope.onFileSelect1 = function($files, cmd) {
		console.log($files);
		console.log(cmd);

		$scope.image_popupInfo.F_TITLE = $files[0].name;
		$scope.file = $files;

		if ($files.length === 0) {
			return;
		}
		var str = '\.(' + cmd + ')$';
		var FileFilter = new RegExp(str);
		var ext = cmd.split('|');
		if ($files[0].name.toLowerCase().match(FileFilter)) {
			readURL($files[0]);
		} else {
			alertify.alert('Error','확장자가 ' + ext + ' 인 파일만 선택가능합니다.',function(){});
			$scope.file = null;
			$scope.image_popupInfo.F_TITLE = '';			
		}
	}

}
