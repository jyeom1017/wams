angular.module('app').config(function($httpProvider) {
	$httpProvider.interceptors.push(function($q, $rootScope, $location, $injector) {
		
		return {
			'response' : function(response) {
				/*var messageBox = $injector.get('messageBox'); // what is messagebox??
				// messagebox를 가져오지 못해 undefined 따라서 아래 if문을 안탐
				console.log('app.config response.data.message == ' + response.data.message);
				console.log('app.config reponseCode == ' + responseCode);
				if (response.data.message) {
					var responseCode = response.data.responseCode, textMsg = response.data.message;
					
					if (responseCode === '403') {
						$rootScope.accessToken = null;
						sessionStorage.removeItem('accessToken');
						sessionStorage.removeItem('userName');
						sessionStorage.removeItem('menuList');
						$location.path('/auth/login');
					} else if (responseCode === 'NG') {
						// alert(textMsg);
						messageBox.open(textMsg, {
							type : 'info'
						});
					}
				}*/
				var responseCode = response.data.responseCode;
				var textMsg = response.data.message;
				
				if (responseCode === '403') {
					$rootScope.accessToken = null;
					sessionStorage.removeItem('accessToken');
					sessionStorage.removeItem('userName');
					sessionStorage.removeItem('menuList');
					// $location.path('/login');
					location.href = '/login';
				} else if (responseCode === 'NG') {
					alertify.alert(textMsg);
				}
				// console.log(response);
				return response;
			},
			'responseError' : function(rejection) {
				return $q.reject(rejection);
			}
		};
	});
});