angular.module('app.constant', [])
.constant('ConfigService', {
	'VMS_VER' : 'V100',
	'VMS_CLIENT_TYPE' : 'PC'
}).constant('Message', {
	'firstSelect' : '- 선택 -',
	'selectSmall' : '선택',
	'fGroup' : '- Group을 선택  -',
	'whole' : '전체'
}).constant('Version', {
	'V1' : '/api',
	'V2' : '/service2.php'
}).constant('YearSet', {
	'baseYear' : 2014,
	'plus' : 2,
	'minus' : 5
}).constant('GridConfig', {
	'sizeSSS' : 2,
	'sizeSS' : 5,
	'sizeT' : 10,
	'sizeXS' : 16,
	'sizeS' : 17,
	'sizeM' : 18,
	'sizeL' : 19,
	'sizeXL' : 20,
	'PageListCnt' : 10
}).constant('LastMenuUseYN', {
	'USE' : 'N'
}).constant('MaxUploadSize', {
	// 'size' : 2147483648 // 2GB            2147483648/1024/1024 ==> 2048MB
	'size' : 20971520 * 15// 20MB
}).constant('FixedAssetItem', {
	'list' : ['10001','10002','10003','10004','10005','10006','10007','10008']   
}).constant('MOP_INDIR_EST', {
	//송수 : SP
	'0' : ['MOP002','MOP003','MOP006','MOP014','MOP019','MOP021','MOP024','MOP030','MOP031','MOP037','MOP039','MOP042','MOP101','MOP102','MOP104','MOP105','MOP106','MOP111','MOP203','MOP204','MOP998']
	//송수 : CIP/DCIP
	,'1' : ['MOP001','MOP010','MOP015']
	//송수 : CML/DCIP
	,'2' : ['MOP027']
	//송수 : 비금속관
	,'3' : ['MOP004','MOP005','MOP008','MOP009','MOP012','MOP013','MOP016','MOP020','MOP025','MOP026','MOP028','MOP029','MOP032','MOP033','MOP034','MOP040','MOP041','MOP103','MOP201','MOP202','MOP205']
	//배수 : 강관
	,'4' : ['MOP002','MOP003','MOP006','MOP014','MOP019','MOP021','MOP024','MOP030','MOP031','MOP037','MOP039','MOP042','MOP101','MOP102','MOP104','MOP105','MOP106','MOP111','MOP203','MOP204','MOP998']
	//배수 : 주철관종
	,'5' : ['MOP001','MOP010','MOP015','MOP027']
	//배수 : 플라스틱관
	,'6' : ['MOP004','MOP005','MOP016','MOP026','MOP028','MOP032','MOP041','MOP103','MOP202','MOP205']
}).constant('MOP_DIR_EST', {
	// 직접평가 SP
	'0' : ['MOP002','MOP003','MOP006','MOP014','MOP019','MOP021','MOP024','MOP030','MOP031','MOP037','MOP039','MOP042','MOP101','MOP102','MOP104','MOP105','MOP106','MOP111','MOP203','MOP204','MOP998']
	// 직접평가 CML-DIP
	,'1' : ['MOP027']
	// 직접평가 CIP/DCIP
	,'2' : ['MOP001','MOP010','MOP015']
	// 직접평가 비금속관
	,'3' : ['MOP004','MOP005','MOP016','MOP026','MOP028','MOP032','MOP103','MOP202']
	//GRP
	,'4' : ['MOP041','MOP205']
	//콘크리트관
	,'5' : ['MOP008','MOP009','MOP012','MOP013','MOP020','MOP025','MOP029','MOP033','MOP034','MOP040','MOP201']
});
